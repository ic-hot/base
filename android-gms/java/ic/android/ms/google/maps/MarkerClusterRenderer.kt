package ic.android.ms.google.maps


import android.content.Context

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions

import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer


internal class MarkerClusterRenderer (

	context : Context,
	googleMap : GoogleMap,
	clusterManager : ClusterManager<MarkerClusterItem>

) : DefaultClusterRenderer<MarkerClusterItem> (context, googleMap, clusterManager) {

	override fun shouldRenderAsCluster (cluster: Cluster<MarkerClusterItem>) : Boolean {
		return cluster.size > 1
	}

	override fun onBeforeClusterItemRendered (
		item: MarkerClusterItem, markerOptions: MarkerOptions
	) {
		markerOptions.icon(BitmapDescriptorFactory.fromBitmap(item.marker.icon))
		markerOptions.anchor(.5F, .5F)
	}

}
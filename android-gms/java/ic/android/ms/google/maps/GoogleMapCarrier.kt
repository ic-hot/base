package ic.android.ms.google.maps


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.struct.map.editable.EditableMap
import ic.struct.collection.Collection
import ic.util.geo.Location

import android.content.Context
import android.view.View

import ic.android.ms.maps.MapMarker
import ic.android.ms.maps.MapCarrier
import ic.android.ms.maps.MapPolyline

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.ClusterManager

import ic.android.util.handler.post
import ic.struct.collection.ext.foreach.breakableForEach


class GoogleMapCarrier : MapCarrier {


	private var context : Context? = null

	private var mapView : MapView? = null


	private var googleMap : GoogleMap? = null

	private var clusterManager : ClusterManager<MarkerClusterItem>? = null


	override fun initView (context: Context) : View {
		this.context = context
		mapView = MapView(context)
		return mapView!!
	}


	override fun open (

		styleResId : Int32,

		onReady : () -> Unit

	) {

		val mapView = mapView!!
		mapView.onCreate(null)
		mapView.onStart()
		mapView.onResume()
		mapView.getMapAsync { googleMap ->

			this.googleMap = googleMap

			googleMap.setPadding(leftPx, topPx, rightPx, bottomPx)

			googleMap.uiSettings.isMapToolbarEnabled = false

			if (styleResId != 0) {
				googleMap.setMapStyle(
					MapStyleOptions.loadRawResourceStyle(mapView.context, styleResId)
				)
			}

			googleMap.setOnCameraMoveListener {
				onCameraMoveAction(
					Location(googleMap.cameraPosition.target.latitude, googleMap.cameraPosition.target.longitude),
					googleMap.cameraPosition.zoom
				)
			}

			clusterManager = ClusterManager<MarkerClusterItem>(context, googleMap).apply {
				renderer = MarkerClusterRenderer(context!!, googleMap, this).apply {
					setAnimation(false)
				}
				setOnClusterItemClickListener { clusterItem ->
					clusterItem.marker.onClick()
					true
				}
			}

			googleMap.setOnCameraIdleListener(clusterManager)

			post {
				googleMap.setOnMarkerClickListener { gMarker ->
					val mapMarker = mapMarkersByGMarker[gMarker]
					if (mapMarker == null) {
						clusterManager?.onMarkerClick(gMarker)
					} else {
						mapMarker.onClick()
					}
					true
				}
			}

			onReady()

		}
	}


	// Padding:

	private var leftPx		: Int32 = 0
	private var topPx 		: Int32 = 0
	private var rightPx		: Int32 = 0
	private var bottomPx	: Int32 = 0

	override fun setMapPadding (leftPx: Int32, topPx: Int32, rightPx: Int32, bottomPx: Int32) {
		this.leftPx = leftPx
		this.topPx = topPx
		this.rightPx = rightPx
		this.bottomPx = bottomPx
		googleMap?.setPadding(leftPx, topPx, rightPx, bottomPx)
	}


	// Markers:

	private val gMarkersByMapMarker = EditableMap<MapMarker, Marker>()

	private val mapMarkersByGMarker = EditableMap<Marker, MapMarker>()

	private val clusterItemsByMapMarker = EditableMap<MapMarker, MarkerClusterItem>()

	override fun addMarker (marker: MapMarker) {
		if (marker.toCluster) {
			val markerClusterItem = MarkerClusterItem(marker)
			clusterItemsByMapMarker[marker] = markerClusterItem
			clusterManager?.addItem(markerClusterItem)
			clusterManager?.cluster()
		} else {
			val gMarker = googleMap!!.addMarker(
				MarkerOptions().apply {
					position(LatLng(marker.position.latitude, marker.position.longitude))
					icon(BitmapDescriptorFactory.fromBitmap(marker.icon))
					anchor(.5F, .5F)
				}
			) ?: return
			gMarkersByMapMarker[marker] = gMarker
			mapMarkersByGMarker[gMarker] = marker
		}
	}

	override fun removeMarker (marker: MapMarker) {
		if (marker.toCluster) {
			val markerClusterItem = clusterItemsByMapMarker[marker] ?: return
			clusterManager?.removeItem(markerClusterItem)
			clusterManager?.cluster()
			clusterItemsByMapMarker[marker] = null
		} else {
			val gMarker = gMarkersByMapMarker[marker] ?: return
			gMarker.remove()
			gMarkersByMapMarker[marker] = null
			mapMarkersByGMarker[gMarker] = null
		}
	}


	// Polylines:

	private val polylines = EditableMap<MapPolyline, Polyline>()

	override fun addPolyline (polyline: MapPolyline) {
		val polylineOptions = PolylineOptions()
		polylineOptions.width(polyline.width)
		polylineOptions.color(polyline.color)
		polyline.points.forEach { point -> polylineOptions.add(LatLng(point.latitude, point.longitude)) }
		val gPolyline = googleMap!!.addPolyline(polylineOptions)
		polylines[polyline] = gPolyline
	}

	override fun removePolyline (polyline: MapPolyline) {
		val gPolyline = polylines[polyline] ?: return
		gPolyline.remove()
		polylines[polyline] = null
	}


	override fun clear() {
		clusterManager?.clearItems()
		googleMap!!.clear()
		gMarkersByMapMarker.empty()
		mapMarkersByGMarker.empty()
		clusterItemsByMapMarker.empty()
		polylines.empty()
	}


	// Camera state:

	override fun moveCamera (position: Location, zoom: Float32, toAnimate: Boolean) {
		if (googleMap == null) return
		val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(position.latitude, position.longitude), zoom)
		if (toAnimate) {
			googleMap!!.animateCamera(cameraUpdate, 512, null)
		} else {
			googleMap!!.moveCamera(cameraUpdate)
		}
	}

	override fun moveCameraToBounds (points: Collection<Location>, toAnimate: Boolean) {
		if (googleMap == null) return
		val cameraUpdate = CameraUpdateFactory.newLatLngBounds(
			LatLngBounds.builder().apply { points.breakableForEach { include(LatLng(it.latitude, it.longitude)) } }.build(),
			0
		)
		if (toAnimate) {
			googleMap!!.animateCamera(cameraUpdate, 512, null)
		} else {
			googleMap!!.moveCamera(cameraUpdate)
		}
	}

	private var onCameraMoveAction : (Location, Float32) -> Unit = { _, _ -> }

	override fun setOnCameraMoveAction(onCameraMoveAction: (Location, Float32) -> Unit) {
		this.onCameraMoveAction = onCameraMoveAction
	}


	override fun close() {
		gMarkersByMapMarker.empty()
		mapMarkersByGMarker.empty()
		polylines.empty()
		clusterManager = null
		googleMap = null
		mapView?.onPause()
		mapView?.onStop()
		mapView?.onDestroy()
		mapView = null
		context = null
	}


}
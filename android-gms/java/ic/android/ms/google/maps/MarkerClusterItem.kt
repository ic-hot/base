package ic.android.ms.google.maps


import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

import ic.android.ms.maps.MapMarker


internal class MarkerClusterItem (val marker: MapMarker) : ClusterItem {

	override fun getSnippet() = null

	override fun getTitle() = null

	override fun getPosition() = LatLng(marker.position.latitude, marker.position.longitude)

}
package ic.android.ms.google.messaging


import com.google.firebase.messaging.RemoteMessage

import ic.android.ms.messaging.notifyMessageReceived
import ic.android.ms.messaging.notifyMessagingTokenUpdated
import ic.struct.map.finite.fromkmap.FiniteMapFromKotlinMap
import ic.util.log.logD


class FirebaseMessagingService : com.google.firebase.messaging.FirebaseMessagingService() {


	override fun onNewToken (newToken: String) {
		logD("FirebaseMessagingService") { "onNewToken $newToken" }
		savedFirebaseInstanceIdToken = newToken
		notifyMessagingTokenUpdated(newToken)
	}


	override fun onMessageReceived (remoteMessage: RemoteMessage) {
		logD("FirebaseMessagingService") { "onMessageReceived remoteMessage $remoteMessage" }
		notifyMessageReceived(
			from = remoteMessage.from,
			notificationTitle = remoteMessage.notification?.title,
			notificationBody = remoteMessage.notification?.body,
			data = FiniteMapFromKotlinMap(remoteMessage.data)
		)
	}


}
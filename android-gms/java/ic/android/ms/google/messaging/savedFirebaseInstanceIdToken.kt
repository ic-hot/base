package ic.android.ms.google.messaging


import ic.storage.prefs.Prefs


private val prefs = Prefs("ic.android.ms.google")


internal var savedFirebaseInstanceIdToken : String?

	get() = prefs.getStringOrNull("firebaseInstanceId")

	set (value) = prefs.set("firebaseInstanceId", value)

;
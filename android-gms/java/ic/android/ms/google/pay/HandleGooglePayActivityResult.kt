package ic.android.ms.google.pay


import ic.util.code.json.JsonObject
import ic.util.code.json.ext.parse

import android.os.Bundle

import ic.android.util.bundle.ext.getAsString
import ic.android.util.bundle.ext.getAsStringOrNull


inline fun handleGooglePayActivityResult (
	result : Bundle,
	onCanceled : () -> Unit,
	onError : (message: String?) -> Unit,
	onSuccess : (response: JsonObject) -> Unit
) {
	val status = result.getAsString("status")
	when (status) {
		"Canceled" -> onCanceled()
		"Error" -> {
			val message = result.getAsStringOrNull("message")
			onError(message)
		}
		"Success" -> {
			val paymentDataJson = result.getAsString("paymentDataJson").let {
				JsonObject.parse(it)
			}
			onSuccess(paymentDataJson)
		}
	}
}
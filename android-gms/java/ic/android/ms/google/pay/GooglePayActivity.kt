package ic.android.ms.google.pay


import android.content.Intent

import com.google.android.gms.wallet.*

import ic.android.ms.google.pay.invoice.GooglePayInvoice
import ic.android.ms.google.pay.invoice.ext.fromBundle
import ic.android.ui.activity.BaseActivity
import ic.android.ui.activity.ext.extras
import ic.android.ui.activity.ext.nav.finishWithResult
import ic.android.util.bundle.ext.getAsBundle
import ic.base.BuildConfig
import ic.util.code.json.JsonArray
import ic.util.code.json.JsonObject
import ic.util.log.logI
import ic.util.log.logW


class GooglePayActivity : BaseActivity() {


	private val loadPaymentDataRequestCode = 991


	private var isGooglePayOpen : Boolean = false

	private var isOnActivityResultCalled : Boolean = false


	override fun onResume() {

		super.onResume()

		if (isGooglePayOpen) {

			if (!isOnActivityResultCalled) {
				finishWithResult(
					"status" to "Canceled"
				)
			}

		} else {

			val invoice = GooglePayInvoice.fromBundle(extras.getAsBundle("invoice"))

			val paymentsClient = Wallet.getPaymentsClient(
				this,
				Wallet.WalletOptions.Builder().apply {
					if (invoice.useTestEnvironment) {
						setEnvironment(WalletConstants.ENVIRONMENT_TEST)
					} else {
						setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
					}
				}.build()
			)

			val requestJson = JsonObject(
				"apiVersion" to 2,
				"apiVersionMinor" to 0,
				"allowedPaymentMethods" to JsonArray(
					JsonObject(
						"type" to "CARD",
						"parameters" to JsonObject(
							"allowedAuthMethods" to JsonArray("PAN_ONLY", "CRYPTOGRAM_3DS"),
							"allowedCardNetworks" to JsonArray(
								"AMEX",
								"DISCOVER",
								"JCB",
								"MASTERCARD",
								"VISA"
							)
						),
						"tokenizationSpecification" to JsonObject(
							"type" to invoice.tokenizationType,
							"parameters" to JsonObject(
								"gateway" to invoice.gateway,
								"gatewayMerchantId" to invoice.gatewayMerchantId
							)
						)
					)
				),
				"transactionInfo" to JsonObject(
					"totalPrice" to invoice.amountUah.toString(),
					"totalPriceStatus" to "FINAL",
					"countryCode" to "UA",
					"currencyCode" to "UAH"
				),
				"merchantInfo" to JsonObject(
					"merchantName" to invoice.gateway
				)
			)

			val request = PaymentDataRequest.fromJson(requestJson.toString())

			AutoResolveHelper.resolveTask(
				paymentsClient.loadPaymentData(request), this, loadPaymentDataRequestCode
			)

			isGooglePayOpen = true
		}

	}


	override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
		if (data == null) return
		when (requestCode) {
			loadPaymentDataRequestCode -> {
				when (resultCode) {
					RESULT_OK -> {
						val paymentData = PaymentData.getFromIntent(data)!!
						isOnActivityResultCalled = true
						finishWithResult(
							"status" to "Success",
							"paymentDataJson" to paymentData.toJson()
						)
					}
					RESULT_CANCELED -> {
						isOnActivityResultCalled = true
						finishWithResult(
							"status" to "Canceled"
						)
					}
					AutoResolveHelper.RESULT_ERROR -> {
						isOnActivityResultCalled = true
						val message = AutoResolveHelper.getStatusFromIntent(data)?.statusMessage
						logW("GooglePay") {
							"Payment error\n" +
							"message: $message\n" +
							"data: $data"
						}
						finishWithResult(
							"status" to "Error",
							"message" to message
						)
					}
				}
			}
		}
	}


	override fun onBackButton() {
		finishWithResult(
			"status" to "Canceled"
		)
	}


}
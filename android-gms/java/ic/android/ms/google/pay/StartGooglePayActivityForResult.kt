package ic.android.ms.google.pay


import android.app.Activity

import ic.android.ms.google.pay.invoice.GooglePayInvoice
import ic.android.ms.google.pay.invoice.ext.toBundle
import ic.android.ui.activity.ext.nav.startActivityForResult


fun Activity.startGooglePayActivityForResult (invoice: GooglePayInvoice) {

	startActivityForResult<GooglePayActivity>(
		"invoice" to invoice.toBundle()
	)

}
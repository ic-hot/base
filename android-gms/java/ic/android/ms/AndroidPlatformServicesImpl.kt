package ic.android.ms


import android.app.Activity
import android.content.Context
import android.os.Bundle

import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging

import ic.android.app.thisApp
import ic.android.ms.google.maps.GoogleMapCarrier
import ic.android.ms.google.messaging.savedFirebaseInstanceIdToken
import ic.android.ms.maps.MapCarrier
import ic.android.system.deviceId
import ic.base.annotations.UsedByReflect


@UsedByReflect
object AndroidPlatformServicesImpl : AndroidPlatformServices {


	override val type get() = AndroidPlatformServicesType.Google


	override fun init() {

		FirebaseApp.initializeApp(thisApp)

		FirebaseAnalytics.getInstance(thisApp).setUserId(deviceId)

	}


	override fun trackEvent (eventName: String, vararg params: Pair<String, String?>) {

		FirebaseAnalytics.getInstance(thisApp).logEvent(
			eventName,
			Bundle().apply {
				params.forEach { (key, value) ->
					if (value != null) putString(key, value)
				}
			}
		)

	}


	override fun trackScreenStart (activity: Activity, screenName: String) {

		@Suppress("DEPRECATION")
		FirebaseAnalytics.getInstance(thisApp).setCurrentScreen(activity, screenName, null)

	}

	override fun trackScreenEnd (activity: Activity, screenName: String) {}


	override val messagingToken get() = savedFirebaseInstanceIdToken


	override fun subscribeToTopic (topicId: String) {

		FirebaseMessaging.getInstance().subscribeToTopic(topicId)

	}

	
	override fun createMapCarrier (context: Context) : MapCarrier {
		return GoogleMapCarrier()
	}


}
package ic.storage.prefs


import android.content.Context

import ic.android.app.thisApp
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.rawBitsAsInt64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.rawBitsAsFloat64
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


internal class AndroidPrefs (

	private val prefsName : String

) : Prefs {

	private val sharedPreferences by lazy {
		thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
	}

	private val sharedPreferencesEditor by lazy {
		sharedPreferences.edit()
	}

	override fun set (key: String, value: Any?) {
		synchronized(this) {
			when (value) {
				null -> {
					sharedPreferencesEditor.remove(key)
					sharedPreferencesEditor.commit()
				}
				is Boolean -> {
					sharedPreferencesEditor.putBoolean(key, value)
					sharedPreferencesEditor.commit()
				}
				is String -> {
					sharedPreferencesEditor.putString(key, value)
					sharedPreferencesEditor.commit()
				}
				is Int32 -> {
					sharedPreferencesEditor.putInt(key, value)
					sharedPreferencesEditor.commit()
				}
				is Int64 -> {
					sharedPreferencesEditor.putLong(key, value)
					sharedPreferencesEditor.commit()
				}
				is Float64 -> {
					sharedPreferencesEditor.putLong(key, value.rawBitsAsInt64)
					sharedPreferencesEditor.commit()
				}
				else -> {
					sharedPreferencesEditor.putString(key, value.toString())
					sharedPreferencesEditor.commit()
				}
			}
		}
	}

	override fun getBooleanOrNull (key: String) : Boolean? = synchronized(this) {
		if (sharedPreferences.contains(key)) sharedPreferences.getBoolean(key, false) else null
	}

	override fun getInt32OrNull (key: String) : Int32? = synchronized(this) {
		if (sharedPreferences.contains(key)) sharedPreferences.getInt(key, 0) else null
	}

	override fun getInt64OrNull (key: String) : Int64? = synchronized(this) {
		if (sharedPreferences.contains(key)) sharedPreferences.getLong(key, 0) else null
	}

	override fun getFloat64OrNull (key: String) : Float64? = synchronized(this) {
		if (sharedPreferences.contains(key)) sharedPreferences.getLong(key, 0).rawBitsAsFloat64 else null
	}

	override fun getStringOrNull (key: String) : String? = synchronized(this) {
		sharedPreferences.getString(key, null)
	}

}
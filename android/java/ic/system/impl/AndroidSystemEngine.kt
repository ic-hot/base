package ic.system.impl


import ic.system.ostype.OsType.Android


internal object AndroidSystemEngine : JvmSystemEngine() {


	override fun getOsType() = Android


}
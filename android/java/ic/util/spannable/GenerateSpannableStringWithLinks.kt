package ic.util.spannable


import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View

import ic.base.loop.breakableRepeat
import ic.base.escape.breakable.Break
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.replaceAll
import ic.struct.list.List
import ic.ifaces.action.Action


fun generateSpannableStringWithLinks (

	markedString : String,

	linkStartMark : String,
	linkEndMark : String,

	linkColorArgb : Int32,

	linksActions : List<Action>

) : SpannableString {

	val visibleString = markedString.replaceAll(linkStartMark to "", linkEndMark to "")

	val spannableString = SpannableString.valueOf(visibleString)

	var charIndex = 0
	var removedTagsLength : Int32 = 0

	breakableRepeat(linksActions.length) { linkIndex ->

		val rawStart = markedString.indexOf(linkStartMark, startIndex = charIndex)

		if (rawStart < 0) Break()

		val start = rawStart - removedTagsLength

		removedTagsLength += linkStartMark.length

		charIndex = rawStart + linkStartMark.length

		val rawEnd = markedString.indexOf(linkEndMark, startIndex = charIndex)

		val end = rawEnd - removedTagsLength

		removedTagsLength += linkEndMark.length

		charIndex = rawEnd + linkEndMark.length

		val clickableSpan = object : ClickableSpan() {

			override fun onClick(widget: View) {
				linksActions[linkIndex].run()
			}

			override fun updateDrawState(drawState: TextPaint) {
				drawState.isUnderlineText = false
			}
		}

		spannableString.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

		val colorSpan = ForegroundColorSpan(linkColorArgb)

		spannableString.setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

	}

	return spannableString

}
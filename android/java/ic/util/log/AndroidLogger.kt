package ic.util.log


import android.util.Log


object AndroidLogger : Logger() {


	override fun implementLog (level: LogLevel, tag: String, value: String) {
		when (level) {
			LogLevel.None 		-> {}
			LogLevel.Verbose 	-> Log.v(tag, value)
			LogLevel.Debug 		-> Log.d(tag, value)
			LogLevel.Info		-> Log.i(tag, value)
			LogLevel.Warning	-> Log.w(tag, value)
			LogLevel.Error		-> Log.e(tag, value)
			LogLevel.Wtf		-> Log.wtf(tag, value)
		}
	}


}
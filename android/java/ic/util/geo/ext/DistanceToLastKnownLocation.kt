package ic.util.geo.ext


import ic.util.geo.lastKnownLocation
import ic.util.geo.Location
import ic.util.geo.getDistanceInKm
import ic.util.geo.getDistanceInM


inline val Location.distanceToLastKnownLocationInM get() = getDistanceInM(lastKnownLocation, this)

inline val Location.distanceToLastKnownLocationInKm get() = getDistanceInKm(lastKnownLocation, this)



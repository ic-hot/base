package ic.util.geo.ext


import android.location.LocationManager

import ic.util.geo.Location


fun Location.toAndroidLocation() : android.location.Location {
	val androidLocation = android.location.Location(LocationManager.GPS_PROVIDER)
	androidLocation.latitude = latitude
	androidLocation.longitude = longitude
	return androidLocation
}
package ic.util.analytics.impl


import ic.android.ms.androidPlatformServices
import ic.android.ui.activity.all.lastForegroundActivity


internal object AndroidAnalyticsEngine : AnalyticsEngine {

	override fun trackEvent (eventName: String, params: Array<out Pair<String, String?>>) {
		androidPlatformServices.trackEvent(
			eventName = eventName,
			params = params
		)
	}

	override fun trackScreenStart (screenName: String) {
		androidPlatformServices.trackScreenStart(
			activity = lastForegroundActivity ?: return,
			screenName = screenName
		)
	}

	override fun trackScreenEnd (screenName: String) {
		androidPlatformServices.trackScreenEnd(
			activity = lastForegroundActivity ?: return,
			screenName = screenName
		)
	}

}
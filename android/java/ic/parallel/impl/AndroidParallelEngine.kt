package ic.parallel.impl


import ic.android.util.handler.executor.UiThreadExecutor
import ic.android.util.handler.post
import ic.android.util.handler.postDelayed

import ic.base.primitives.int64.Int64
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable


internal object AndroidParallelEngine : JvmParallelEngine() {

	@Deprecated("Use doInUiThread")
	override val defaultCallbackExecutor get() = UiThreadExecutor

	override fun doInUiThread (action: Action) : Cancelable {
		return post(action)
	}

	override fun schedule (action: Action, delayMs: Int64) = postDelayed(delayMs) { action.run() }

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.context.ext


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


inline fun Context.inflateView (layoutResId: Int, parentView: ViewGroup?) : View {

	return LayoutInflater.from(this).inflate(layoutResId, parentView, false)

}
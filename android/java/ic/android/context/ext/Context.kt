package ic.android.context.ext


import android.content.Context


inline val Context.context get() = this
package ic.android.context.ext


import android.content.Context
import android.content.Intent

import ic.android.util.bundle.Bundle


@Suppress("NOTHING_TO_INLINE")
inline fun Context.sendBroadcast (
	action : String?,
	vararg extras : Pair<String, Any?>
) {

	sendBroadcast(
		Intent().apply {
			if (action != null) {
				this.action = action
			}
			putExtras(
				Bundle(*extras)
			)
		}
	)

}
package ic.android.ui.view.edittext.ext


import android.text.InputType
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import ic.android.ui.view.textinput.TextInputMode


var EditText.inputMode : TextInputMode
	get() {
		throw NotImplementedError()
	}
	set(value) {
		when (value) {
			TextInputMode.Normal 		-> inputType = InputType.TYPE_CLASS_TEXT
			TextInputMode.Number 		-> inputType = InputType.TYPE_CLASS_NUMBER
			TextInputMode.PersonName 	-> inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PERSON_NAME
			TextInputMode.Email -> {
				inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
				imeOptions = imeOptions or EditorInfo.IME_FLAG_FORCE_ASCII
			}
		}
	}
;
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.edittext

import ic.android.ui.view.textinput.textfilters.CompositeTextFilter


inline fun EditText.setTextFilters (vararg textFilters: (oldValue: String, newValue: String) -> String) {

	setTextFilter(CompositeTextFilter(*textFilters))

}
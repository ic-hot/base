package ic.android.ui.view.edittext


import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo.*
import ic.base.R
import ic.android.storage.assets.getFontFromAssetsOrNull
import ic.math.ext.clamp


@SuppressLint("AppCompatCustomView")
class EditText : android.widget.EditText {


	init {
		setTextIsSelectable(true)
	}


	private var textFilter : ((oldValue: String, newValue: String) -> String)? = null

	fun setTextFilter (textFilter: (oldValue: String, newValue: String) -> String) {
		this.textFilter = textFilter
		onValueChanged (text.toString())
	}


	private var onValueChangedAction
		: ((oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit)?
		= null
	;

	fun setOnValueChangedAction (
		toCallAtOnce : Boolean = false,
		valueChangedAction
			: (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit
		,
	) {
		this.onValueChangedAction = valueChangedAction
		if (toCallAtOnce) {
			valueChangedAction(text.toString(), text.toString(), false)
		}
	}

	private var onValueChangedActions = HashSet<(oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit>()

	fun addOnValueChangedAction (onValueChangedAction: (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit) {
		onValueChangedActions.add(onValueChangedAction)
	}

	private var onValueChangedActionsByKey = HashMap<String, (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit>()

	fun setOnValueChangedAction (key: String, valueChangedAction: (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit) {
		onValueChangedActionsByKey.put(key, valueChangedAction)
	}

	private fun invokeValueChangedActions (oldValue: String, newValue: String, invokedByUserInput: Boolean) {
		onValueChangedAction?.invoke(oldValue, newValue, invokedByUserInput)
		onValueChangedActions.forEach { it(oldValue, newValue, invokedByUserInput) }
		onValueChangedActionsByKey.values.forEach { it(oldValue, newValue, invokedByUserInput) }
	}


	var isLocked : Boolean = false

	private var oldValue : String = text.toString()

	private var valueChangedProgrammatically : Boolean = false

	private fun onValueChanged (newValue: String) {
		if (newValue != oldValue) {
			if (isLocked) {
				if (newValue != oldValue) {
					setText(oldValue)
					setSelection(oldValue.length, oldValue.length)
				}
			} else {
				val oldValue = oldValue
				this.oldValue = newValue
				if (textFilter == null) {
					invokeValueChangedActions(oldValue, newValue, !valueChangedProgrammatically)
				} else {
					val filteredText = textFilter!!.invoke(oldValue, newValue)
					if (filteredText == newValue) {
						invokeValueChangedActions(oldValue, newValue, !valueChangedProgrammatically)
					} else {
						val selection = (selectionEnd + (filteredText.length - newValue.length)).clamp(0, filteredText.length)
						setText(filteredText)
						if (text.toString() == filteredText) {
							setSelection(selection, selection)
						}
					}
				}
			}
		}
		valueChangedProgrammatically = false
	}

	init {
		addTextChangedListener(
			object : TextWatcher {
				override fun beforeTextChanged (newText: CharSequence, start: Int, count: Int, after: Int) {}
				override fun onTextChanged (newText: CharSequence, start: Int, before: Int, count: Int) {}
				override fun afterTextChanged (newEditable: Editable) {
					onValueChanged(newEditable.toString())
				}
			}
		)
	}

	var value : String
		get() = text.toString()
		set(value) {
			valueChangedProgrammatically = true
			setText(value)
		}
	;


	fun setOkayAction (okayAction: (text: String) -> Unit) {
		setOnEditorActionListener { _, actionId, _ ->
			if (actionId == IME_ACTION_DONE || actionId == IME_ACTION_NEXT || actionId == IME_ACTION_GO || actionId == IME_ACTION_SEARCH) {
				okayAction(text.toString())
				true
			} else false
		}
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super (context, attrs, defStyleAttr)
	{
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.TextInputView)
			val fontName = styledAttributes.getString(R.styleable.TextInputView_textFont)
			if (fontName != null) {
				typeface = getFontFromAssetsOrNull(fontName)
			}
			styledAttributes.recycle()
		}
	}


}
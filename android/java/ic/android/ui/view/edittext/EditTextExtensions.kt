package ic.android.ui.view.edittext


fun android.widget.EditText.setValue (value: String, selection: Int) {
	setText(value)
	setSelection(selection)
}

var android.widget.EditText.value : String
	get() = text.toString()
	set(value) {
		setText(value)
		setSelection(value.length)
	}
;
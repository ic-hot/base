package ic.android.ui.view.gen


import android.view.View

import ic.android.ui.view.scope.AndroidViewScope


inline fun AndroidViewGenerator (

	crossinline generateView : AndroidViewScope.() -> View

) : AndroidViewGenerator {

	return object : AndroidViewGenerator {

		override fun AndroidViewScope.generateView(): View {
			return generateView()
		}

	}

}
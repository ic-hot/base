package ic.android.ui.view.gen


import android.view.View

import ic.android.ui.view.scope.AndroidViewScope


interface AndroidViewGenerator {

	fun AndroidViewScope.generateView() : View

}
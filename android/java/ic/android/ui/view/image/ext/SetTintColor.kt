package ic.android.ui.view.image.ext


import ic.base.primitives.int32.Int32

import android.widget.ImageView


fun ImageView.setTintColor (tintColorArgb: Int32?) {
	if (tintColorArgb == null) {
		clearColorFilter()
	} else {
		setColorFilter(tintColorArgb)
	}
}
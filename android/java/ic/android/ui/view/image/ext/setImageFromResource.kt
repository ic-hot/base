@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.image.ext


import android.widget.ImageView


inline fun ImageView.setImageFromResource (resId: Int) {

	setImageResource(resId)

}
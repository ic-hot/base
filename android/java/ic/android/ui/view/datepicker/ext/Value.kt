package ic.android.ui.view.datepicker.ext


import android.widget.DatePicker

import ic.util.time.Time
import ic.util.time.ext.*
import ic.util.time.funs.now


var DatePicker.value : Time

	get() {
		return Time(
			initialTime = now,
			year = year,
			month = month,
			dayOfMonth = dayOfMonth
		)
	}

	set (value) {
		updateDate(
			value.year,
			value.month,
			value.dayOfMonth
		)
	}

;
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.impl.measure


import ic.base.primitives.int32.Int32

import android.view.View


inline fun View.measureSimpleViewDimension (measureSpec: Int32) : Int32 {

	return when (View.MeasureSpec.getMode(measureSpec)) {
		View.MeasureSpec.EXACTLY -> View.MeasureSpec.getSize(measureSpec)
		View.MeasureSpec.AT_MOST -> 0
		else -> 0
	}

}
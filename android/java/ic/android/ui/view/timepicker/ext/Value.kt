package ic.android.ui.view.timepicker.ext


import ic.util.time.ext.hourOfDay
import ic.util.time.ext.minute
import ic.util.time.funs.now
import ic.util.time.Time

import android.widget.TimePicker


@Suppress("DEPRECATION")
var TimePicker.value : Time

	get() {
		return Time(
			initialTime = now,
			hourOfDay = currentHour,
			minute = currentMinute
		)
	}

	set (value) {
		currentHour 	= value.hourOfDay
		currentMinute 	= value.minute
	}

;
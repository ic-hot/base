package ic.android.ui.view.group.ext


import android.view.View
import android.view.ViewGroup


inline fun ViewGroup.noChild (condition : (View) -> Boolean) : Boolean {

	return !atLeastOneChild(condition = condition)

}
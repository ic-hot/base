package ic.android.ui.view.group.ext


import ic.base.primitives.int32.Int32

import android.view.View
import android.view.ViewGroup


inline fun ViewGroup.atLeastOneChild (condition : (View) -> Boolean) : Boolean {

	val childCount = this.childCount

	var childIndex : Int32 = 0

	while (childIndex < childCount) {

		val child = getChildAt(childIndex)

		if (condition(child)) return true

		childIndex++

	}

	return false

}
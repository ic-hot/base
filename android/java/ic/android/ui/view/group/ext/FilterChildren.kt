package ic.android.ui.view.group.ext


import android.view.View
import android.view.ViewGroup

import ic.base.primitives.int32.Int32


inline fun ViewGroup.filterChildren (

	condition : (View) -> Boolean

) {

	var index : Int32 = 0

	while (index < childCount) {

		val child = getChildAt(index)

		if (condition(child)) {

			index++

		} else {

			removeViewAt(index)

		}

	}

}
package ic.android.ui.view.group.ext


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


fun ViewGroup.inflateContent (layoutResId: Int) {
	LayoutInflater.from(context).inflate(layoutResId, this)!!
}

fun ViewGroup.inflateChild (layoutResId: Int) : View {
	return LayoutInflater.from(context).inflate(layoutResId, this, false)!!
}
package ic.android.ui.view.group.ext


import android.view.View
import android.view.ViewGroup

import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.editable.BaseEditableList
import ic.struct.list.editable.EditableList


val ViewGroup.editableChildren : EditableList<View> get() {

	return object : BaseEditableList<View>() { // TODO shouldn't be freezable

		override val length get() = childCount.asInt64

		override fun implementGet (index: Int64) = getChildAt(index.asInt32)

		override fun implementSet (index: Int64, item: View) {
			removeViewAt(index.asInt32)
			addView(item, index.asInt32)
		}

		override fun implementInsert (index: Int64, item: View) {
			addView(item, index.asInt32)
		}

		override fun implementRemove (index: Int64) : View {
			val view = getChildAt(index.asInt32)
			removeViewAt(index.asInt32)
			return view
		}

		override fun empty() {
			removeAllViews()
		}

	}

}
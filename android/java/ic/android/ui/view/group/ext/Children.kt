package ic.android.ui.view.group.ext


import ic.base.primitives.int32.ext.asInt64
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach

import android.view.View
import android.view.ViewGroup


var ViewGroup.children : List<View>

	get() {
		return List(childCount) { this[it] }
	}

	set (value) {
		fun areIdentical() : Boolean {
			if (childCount.asInt64 != value.length) return false
			forEachChildIndexed { childIndex, child ->
				if (value[childIndex] !== child) return false
			}
			return true
		}
		if (areIdentical()) return
		removeAllViews()
		value.forEach { addView(it) }
	}

;
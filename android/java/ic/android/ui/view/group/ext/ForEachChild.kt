package ic.android.ui.view.group.ext


import ic.base.loop.breakableRepeat
import ic.base.primitives.int32.Int32

import android.view.View
import android.view.ViewGroup


inline fun ViewGroup.forEachChildIndexed (action : (childIndex: Int32, child: View) -> Unit) {
	breakableRepeat(childCount) { index -> action(index, getChildAt(index)) }
}


inline fun ViewGroup.forEachChild (action: (View) -> Unit) {
	forEachChildIndexed { _, child -> action(child) }
}
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.group.ext


import ic.base.primitives.int32.Int32

import android.view.View
import android.view.ViewGroup


inline operator fun <ChildView: View> ViewGroup.get (index: Int32) : ChildView {

	@Suppress("UNCHECKED_CAST")
	return getChildAt(index)!! as ChildView

}


inline fun <ChildView: View> ViewGroup.getOrNull (index: Int32) : ChildView? {

	if (index < 0) 				return null
	if (index >= childCount) 	return null

	return get(index)

}
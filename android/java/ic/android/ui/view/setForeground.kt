package ic.android.ui.view


import java.util.*

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View

import ic.android.storage.res.getResDrawable


fun View.setForegroundFromDrawable (foregroundDrawable: Drawable?) {
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
		foreground = foregroundDrawable
		viewForegroundResIds.remove(this)
	}
}


private val viewForegroundResIds = WeakHashMap<View, Int>()

fun View.setForegroundFromResource (drawableResId: Int) {
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
		if (viewForegroundResIds[this] == drawableResId) return
		foreground = context.getResDrawable(drawableResId)
		viewForegroundResIds[this] = drawableResId
	}
}
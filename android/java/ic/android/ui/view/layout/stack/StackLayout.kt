package ic.android.ui.view.layout.stack


import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.View.MeasureSpec.AT_MOST
import android.view.View.MeasureSpec.EXACTLY
import android.view.View.MeasureSpec.makeMeasureSpec
import android.view.View.MeasureSpec.UNSPECIFIED
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout

import ic.base.loop.breakableRepeat
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.math.denormalize
import ic.math.funs.max
import ic.math.funs.min


open class StackLayout : FrameLayout {


	// TODO
	/*override fun onMeasure (widthMeasureSpec: Int32, heightMeasureSpec: Int32) {

		var maxChildWidthPx : Int32 = 0
		var maxChildHeightPx : Int32 = 0

		breakableRepeat(childCount) { childIndex ->

			val child = getChildAt(childIndex)
			val childLayoutParams = child.layoutParams

			fun getChildMeasureSpec (measureSpec: Int32) : Int32 {
				val measureSpecSize = MeasureSpec.getSize(measureSpec)
				return when (MeasureSpec.getMode(measureSpec)) {
					EXACTLY -> makeMeasureSpec(measureSpecSize, AT_MOST)
					AT_MOST -> makeMeasureSpec(measureSpecSize, AT_MOST)
					else 	-> makeMeasureSpec(0, UNSPECIFIED)
				}
			}

			child.measure(
				getChildMeasureSpec(measureSpec = widthMeasureSpec),
				getChildMeasureSpec(measureSpec = heightMeasureSpec)
			)

			fun getNewMaxChildSizePx (oldMaxChildSizePx: Int32, childLayoutParam: Int32, childMeasuredSizePx: Int32) = max(
				oldMaxChildSizePx,
				when (childLayoutParam) {
					WRAP_CONTENT -> childMeasuredSizePx
					MATCH_PARENT -> childMeasuredSizePx
					else -> childLayoutParam
				}
			)

			maxChildWidthPx = getNewMaxChildSizePx(
				oldMaxChildSizePx = maxChildWidthPx,
				childLayoutParam = childLayoutParams.width,
				childMeasuredSizePx = child.measuredWidth
			)

			maxChildHeightPx = getNewMaxChildSizePx(
				oldMaxChildSizePx = maxChildHeightPx,
				childLayoutParam = childLayoutParams.height,
				childMeasuredSizePx = child.measuredHeight
			)

		}

		fun getMeasuredSizePx (measureSpec: Int32, maxChildSizePx: Int32) : Int32 {
			val measureSpecSize = MeasureSpec.getSize(measureSpec)
			return when (MeasureSpec.getMode(measureSpec)) {
				EXACTLY -> measureSpecSize
				AT_MOST -> min(measureSpecSize, maxChildSizePx)
				else -> maxChildSizePx
			}
		}

		val measuredWidthPx  = getMeasuredSizePx(measureSpec = widthMeasureSpec,  maxChildSizePx = maxChildWidthPx)
		val measuredHeightPx = getMeasuredSizePx(measureSpec = heightMeasureSpec, maxChildSizePx = maxChildHeightPx)

		breakableRepeat(childCount) { childIndex ->

			val child = getChildAt(childIndex)
			val childLayoutParams = child.layoutParams

			fun getChildMeasureSpec (childLayoutParam: Int32, measuredSizePx: Int32) = when (childLayoutParam) {
				WRAP_CONTENT 	-> makeMeasureSpec(measuredSizePx, AT_MOST)
				MATCH_PARENT 	-> makeMeasureSpec(measuredSizePx, EXACTLY)
				else 			-> makeMeasureSpec(childLayoutParam, EXACTLY)
			}

			child.measure(
				getChildMeasureSpec(childLayoutParam = childLayoutParams.width, measuredSizePx = measuredWidthPx),
				getChildMeasureSpec(childLayoutParam = childLayoutParams.height, measuredSizePx = measuredHeightPx)
			)

		}

		setMeasuredDimension(measuredWidthPx, measuredHeightPx)

	}*/


	@SuppressLint("RtlHardcoded")
	protected open fun getChildHorizontalAlign (child: View, childLayoutParams: FrameLayout.LayoutParams) : Float32 {
		return when (childLayoutParams.gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
			Gravity.LEFT 	-> 0F
			Gravity.RIGHT 	-> 1F
			else 			-> .5F
		}
	}

	@SuppressLint("RtlHardcoded")
	protected open fun getChildVerticalAlign (child: View, childLayoutParams: FrameLayout.LayoutParams) : Float32 {
		return when (childLayoutParams.gravity and Gravity.VERTICAL_GRAVITY_MASK) {
			Gravity.TOP 	-> 0F
			Gravity.BOTTOM 	-> 1F
			else 			-> .5F
		}
	}


	override fun onLayout (changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {

		val widthPx  = right - left
		val heightPx = bottom - top

		breakableRepeat(childCount) { childIndex ->

			val child = getChildAt(childIndex)
			val childLayoutParams = child.layoutParams as FrameLayout.LayoutParams

			fun getChildSizePx (sizePx: Int32, childLayoutParam: Int32, childMeasuredSizePx: Int32) = when (childLayoutParam) {
				MATCH_PARENT -> sizePx
				WRAP_CONTENT -> childMeasuredSizePx
				else -> childLayoutParam
			}

			val childWidthPx = getChildSizePx(
				sizePx = widthPx, childLayoutParam = childLayoutParams.width, childMeasuredSizePx = child.measuredWidth
			)
			val childHeightPx = getChildSizePx(
				sizePx = heightPx, childLayoutParam = childLayoutParams.height, childMeasuredSizePx = child.measuredHeight
			)

			val childHorizontalAlign = getChildHorizontalAlign(child, childLayoutParams)
			val childVerticalAlign = getChildVerticalAlign(child, childLayoutParams)

			val childXPositionPx = childHorizontalAlign.denormalize(0, widthPx - childWidthPx).asInt32
			val childYPositionPx = childVerticalAlign.denormalize(0, heightPx - childHeightPx).asInt32

			child.layout(
				childXPositionPx,
				childYPositionPx,
				childXPositionPx + childWidthPx,
				childYPositionPx + childHeightPx
			)

		}

	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)


}
package ic.android.ui.view.textinput.textfilters


import ic.struct.list.List
import ic.struct.list.ext.get


abstract class CompositeTextFilter : (String, String) -> String {

	protected abstract val children : List<(oldValue: String, newValue: String) -> String>

	override fun invoke (oldValue: String, newValue: String) : String {

		var value = newValue

		for (i in 0 until children.length) {
			val child = children[i]
			value = child(oldValue, value)
		}

		return value

	}

}
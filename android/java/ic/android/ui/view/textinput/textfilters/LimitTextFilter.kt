package ic.android.ui.view.textinput.textfilters


abstract class LimitTextFilter : (String, String) -> String {

	protected abstract val limit : Int

	override fun invoke (oldValue: String, newValue: String) : String {

		if (newValue.length > limit) return oldValue

		return newValue

	}

}
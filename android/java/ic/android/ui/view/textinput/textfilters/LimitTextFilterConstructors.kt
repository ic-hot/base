@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.android.ui.view.textinput.textfilters


inline fun LimitTextFilter (limit: Int) : LimitTextFilter {
	return object : LimitTextFilter() {
		override val limit get() = limit
	}
}
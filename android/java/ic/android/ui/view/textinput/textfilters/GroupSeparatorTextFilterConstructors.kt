@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.textinput.textfilters


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.struct.list.List


inline fun GroupSeparatorTextFilter (

	separator : Character,

	groupSizes : List<Int32>

) : GroupSeparatorTextFilter {

	return object : GroupSeparatorTextFilter() {

		override val separator get() = separator

		override val groupSizes get() = groupSizes

	}

}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.android.ui.view.textinput.textfilters

import ic.base.arrays.ext.asList
import ic.struct.list.List


inline fun CompositeTextFilter (
	children: List<(oldValue: String, newValue: String) -> String>
) : CompositeTextFilter {
	return object : CompositeTextFilter() {
		override val children get() = children
	}
}

inline fun CompositeTextFilter (vararg children: (oldValue: String, newValue: String) -> String) : CompositeTextFilter {
	return CompositeTextFilter(children.asList(isArrayImmutable = true))
}
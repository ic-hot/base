@file:Suppress("ConvertToStringTemplate")


package ic.android.ui.view.textinput.textfilters


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.filter
import ic.ifaces.getter.getter2.Getter2
import ic.struct.list.List
import ic.struct.list.ext.get
import ic.struct.list.ext.reduce.sum.sum

import ic.util.log.logD


abstract class GroupSeparatorTextFilter : Getter2<String, String, String> {


	protected abstract val groupSizes : List<Int32>

	protected abstract val separator : Character


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun get (oldValue: String, newValue: String) : String {

		val totalTargetLength = groupSizes.sum

		val newValueWithoutSeparators = newValue.filter { it != separator }

		if (newValueWithoutSeparators.length > totalTargetLength) {
			return oldValue
		}

		val buffer = StringBuilder()

		var currentGroupIndex : Int32 = 0
		var indexInGroup : Int32 = 0

		newValueWithoutSeparators.forEach { character ->

			if (indexInGroup >= groupSizes[currentGroupIndex]) {
				buffer.append(separator)
				currentGroupIndex++
				indexInGroup = 0
			}

			buffer.append(character)

			indexInGroup++

		}

		logD("GroupSeparatorTextFilter") {
			"oldValue: $oldValue, newValue: $newValue, result: $buffer"
		}

		return buffer.toString()

	}

}
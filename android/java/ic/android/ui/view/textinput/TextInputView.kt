package ic.android.ui.view.textinput


import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.inputmethod.EditorInfo.*

import ic.base.R
import ic.android.storage.assets.getFontFromAssetsOrNull
import ic.math.ext.clamp


open class TextInputView : android.widget.EditText {


	init {
		@Suppress("LeakingThis")
		setTextIsSelectable(true)
	}


	private var textFilter: ((oldValue: String, newValue: String) -> String) = { _, newValue -> newValue }

	fun setTextFilter (textFilter: (oldValue: String, newValue: String) -> String) {
		this.textFilter = textFilter
		onValueChanged(text.toString())
	}


	private var onValueChangedAction
		: ((oldValue: String, newValue: String, isInvokedByUserInteraction: Boolean) -> Unit)? = null
	;

	fun setOnValueChangedAction(
		toCallAtOnce : Boolean = false,
		onValueChanged : (oldValue: String, newValue: String, isInvokedByUserInteraction: Boolean) -> Unit
	) {
		this.onValueChangedAction = onValueChanged
		if (toCallAtOnce) {
			onValueChanged(text.toString(), text.toString(), false)
		}
	}

	private var onValueChangedActions = HashSet<(oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit>()

	fun addOnValueChangedAction(onValueChangedAction: (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit) {
		onValueChangedActions.add(onValueChangedAction)
	}

	private var onValueChangedActionsByKey =
		HashMap<String, (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit>()
	;

	fun setOnValueChangedAction (
		key: String,
		valueChangedAction: (oldValue: String, newValue: String, invokedByUserInput: Boolean) -> Unit
	) {
		onValueChangedActionsByKey.put(key, valueChangedAction)
	}

	private fun invokeOnValueChangedActions (oldValue: String, newValue: String) {
		val isValueChangedByUserInteraction = this.isValueChangedByUserInteraction
		this.isValueChangedByUserInteraction = true
		onValueChangedAction?.invoke(oldValue, newValue, isValueChangedByUserInteraction)
		onValueChangedActions.forEach { it(oldValue, newValue, isValueChangedByUserInteraction) }
		onValueChangedActionsByKey.values.forEach {
			it(
				oldValue,
				newValue,
				isValueChangedByUserInteraction
			)
		}
	}


	var isLocked: Boolean = false

	private var oldValue : String = text.toString()

	private var isValueChangedByUserInteraction: Boolean = true

	private var isOnValueChangedRecursion = false

	private fun filterValue (oldValue: String, newValue: String) : String {
		var filteredValue = newValue
		while (true) {
			val newFilteredValue = textFilter.invoke(oldValue, filteredValue)
			if (newFilteredValue == filteredValue) return filteredValue
			filteredValue = newFilteredValue
		}
	}

	private fun implementSetText (oldValue: String, newValue: String) {
		val oldCursorPosition = selectionEnd
		var newCursorPosition = (
			if (newValue.length > oldValue.length) {
				oldCursorPosition + (newValue.length - oldValue.length)
			} else {
				oldCursorPosition
			}
		)
		newCursorPosition = newCursorPosition.clamp(0, newValue.length)
		isOnValueChangedRecursion = true
		setText(newValue)
		isOnValueChangedRecursion = false
		//if (newCursorPosition >= oldCursorPosition) {
			setSelection(newCursorPosition, newCursorPosition)
		//}
	}

	private fun onValueChanged (newValue: String) {
		if (isOnValueChangedRecursion) return
		val oldValue = this.oldValue
		if (newValue == oldValue) {
			return
		}
		if (isLocked) {
			setText(oldValue)
			setSelection(oldValue.length, oldValue.length)
			return
		}
		val filteredValue = filterValue(oldValue, newValue)
		if (filteredValue != newValue) {
			implementSetText(oldValue, filteredValue)
		}
		this.oldValue = filteredValue
		requestLayout()
		invokeOnValueChangedActions(oldValue, filteredValue)
	}

	init {
		@Suppress("LeakingThis")
		addTextChangedListener(
			object : TextWatcher {
				override fun beforeTextChanged (newText: CharSequence, start: Int, count: Int, after: Int) {}
				override fun onTextChanged (newText: CharSequence, start: Int, before: Int, count: Int) {
					onValueChanged(newText.toString())
				}
				override fun afterTextChanged (newText: Editable) {}
			}
		)
	}

	open var value : String
		get() = text.toString()
		set(value) {
			isValueChangedByUserInteraction = false
			val oldValue = text.toString()
			this.oldValue = oldValue
			val filteredValue = filterValue(oldValue, value)
			if (filteredValue != oldValue) {
				implementSetText(oldValue, filteredValue)
			}
			this.oldValue = filteredValue
			invokeOnValueChangedActions(oldValue, filteredValue)
		}
	;


	override fun dispatchTouchEvent (event: MotionEvent?) : Boolean {
		if (isLocked) return false
		return super.dispatchTouchEvent(event)
	}


	fun setOkayAction (okayAction: (text: String) -> Unit) {
		setOnEditorActionListener { _, actionId, _ ->
			if (
				actionId == IME_ACTION_DONE ||
				actionId == IME_ACTION_NEXT ||
				actionId == IME_ACTION_GO ||
				actionId == IME_ACTION_SEARCH
			) {
				okayAction(text.toString())
				true
			} else false
		}
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.TextInputView)
			val fontName = styledAttributes.getString(R.styleable.TextInputView_textFont)
			if (fontName != null) {
				typeface = getFontFromAssetsOrNull(fontName)
			}
			styledAttributes.recycle()
		}
	}


}
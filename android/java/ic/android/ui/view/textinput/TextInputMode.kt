package ic.android.ui.view.textinput


import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.ext.reduce.find.find


sealed class TextInputMode {

	abstract val id : Int32

	object Normal : TextInputMode() {
		override val id get() = 0
	}

	object Number : TextInputMode() {
		override val id get() = 1
	}

	object PersonName : TextInputMode() {
		override val id get() = 2
	}

	object Email : TextInputMode() {
		override val id get() = 3
	}

	companion object {

		val all get() = List(Normal, Number, PersonName, Email)

		fun byId (id: Int32) = all.find { it.id == id }

	}

}
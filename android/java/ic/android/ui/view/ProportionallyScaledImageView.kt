package ic.android.ui.view


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.View.MeasureSpec.*
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import ic.base.primitives.float32.Float32

import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.math.ext.clamp


@SuppressLint("AppCompatCustomView")


open class ProportionallyScaledImageView

	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)

	: ImageView (context, attrs, defStyleAttr)

{


	init {
		scaleType = ScaleType.FIT_CENTER
	}


	var minAspectRatio : Float32 = 0F
		set(value) {
			field = value
			requestLayout()
		}
	;


	private fun getDimension (layoutParam: Int32, measureSpec: Int32) : Int32 {
		return when (layoutParam) {
			MATCH_PARENT -> {
				val mode = getMode(measureSpec)
				when (mode) {
					EXACTLY -> getSize(measureSpec)
					AT_MOST -> 0
					else -> 0
				}
			}
			WRAP_CONTENT -> -1
			else -> layoutParam
		}
	}

	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		if (drawable !is BitmapDrawable) {
			return super.onMeasure(widthMeasureSpec, heightMeasureSpec)
		}
		val layoutParams = this.layoutParams
		val width  = getDimension(layoutParams.width,  widthMeasureSpec)
		val height = getDimension(layoutParams.height, heightMeasureSpec)
		val aspectRatio = run {
			super.onMeasure(makeMeasureSpec(0, UNSPECIFIED), makeMeasureSpec(0, UNSPECIFIED))
			measuredWidth.asFloat32 / measuredHeight.asFloat32
		}.clamp(min = minAspectRatio)
		when {
			width >= 0 && height < 0 -> {
				setMeasuredDimension(width, (width / aspectRatio).asInt32)
			}
			width < 0 && height >= 0 -> {
				setMeasuredDimension((height * aspectRatio).asInt32, height)
			}
			else -> {
				super.onMeasure(widthMeasureSpec, heightMeasureSpec)
			}
		}
	}


}
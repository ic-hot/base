package ic.android.ui.view


import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.graphics.Canvas
import android.graphics.Path

import kotlin.math.min


class CircleCroppedView

	@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)

	: FrameLayout(context, attrs, defStyleAttr)

{

	private var circleScaleValue : Float = 1F
	var circleScale : Float
		get() = circleScaleValue
		set(value) {
			circleScaleValue = value
			invalidate()
		}
	;

	override fun dispatchDraw (canvas: Canvas) {
		canvas.clipPath(Path().apply {
			addCircle(
				width.toFloat() / 2,
				height.toFloat() / 2,
				min(width, height).toFloat() / 2 * circleScaleValue,
				Path.Direction.CW
			)
		})
		super.dispatchDraw(canvas)
	}

}
package ic.android.ui.view


import android.content.Context
import android.util.AttributeSet
import android.view.View.MeasureSpec.*
import android.widget.FrameLayout

import ic.base.R
import ic.android.ui.view.group.ext.forEachChild


class ProportionallyScaledView

	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)

	: FrameLayout (context, attrs, defStyleAttr)

{


	private var intrinsicSizeValue : Int = 0

	var intrinsicSize : Int
		get() = intrinsicSizeValue
		set(value) {
			intrinsicSizeValue = value
			requestLayout()
		}
	;


	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val width 	= if (getMode(widthMeasureSpec) == EXACTLY) 	getSize(widthMeasureSpec) 	else null
		val height 	= if (getMode(heightMeasureSpec) == EXACTLY) 	getSize(heightMeasureSpec) 	else null
		val scale : Float
		when {
			width != null && height == null -> {
				var childrenMaxHeight = 0
				forEachChild { child ->
					child.measure(makeMeasureSpec(intrinsicSize, EXACTLY), makeMeasureSpec(0, UNSPECIFIED))
					val childHeight = child.measuredHeight
					if (childHeight > childrenMaxHeight) childrenMaxHeight = childHeight
				}
				scale = width.toFloat() / intrinsicSizeValue.toFloat()
				setMeasuredDimension(width, (childrenMaxHeight * scale).toInt())
			}
			width == null && height != null -> {
				var childrenMaxWidth = 0
				forEachChild { child ->
					child.measure(makeMeasureSpec(0, UNSPECIFIED), makeMeasureSpec(intrinsicSize, EXACTLY))
					val childWidth = child.measuredWidth
					if (childWidth > childrenMaxWidth) childrenMaxWidth = childWidth
				}
				scale = height.toFloat() / intrinsicSizeValue.toFloat()
				setMeasuredDimension((childrenMaxWidth * scale).toInt(), height)
			}
			else -> throw IllegalStateException("width: $width, height: $height")
		}
		forEachChild { child ->
			child.pivotX = 0F
			child.pivotY = 0F
			child.scaleX = scale
			child.scaleY = scale
		}
	}


	init {
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.ProportionallyScaledView)
			intrinsicSizeValue = styledAttributes.getDimensionPixelSize(R.styleable.ProportionallyScaledView_intrinsicSize, 0)
			styledAttributes.recycle()
		}
	}


}
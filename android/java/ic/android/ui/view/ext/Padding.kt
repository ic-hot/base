package ic.android.ui.view.ext


import android.view.View
import ic.android.util.units.dpToPx
import ic.android.util.units.pxToDp
import ic.base.primitives.float32.Float32


inline var View.leftPaddingPx : Int
	get() = paddingLeft
	set(value) {
		setPadding(
			value, paddingTop, paddingRight, paddingBottom
		)
	}
;


inline var View.rightPaddingPx : Int
	get() = paddingRight
	set(value) {
		setPadding(
			paddingLeft, paddingTop, value, paddingBottom
		)
	}
;


inline var View.topPaddingPx : Int
	get() = paddingTop
	set(value) {
		setPadding(
			paddingLeft, value, paddingRight, paddingBottom
		)
	}
;


inline var View.topPaddingDp : Float32
	get() = pxToDp(topPaddingPx)
	set(value) { topPaddingPx = dpToPx(value) }
;


inline var View.bottomPaddingPx : Int
	get() = paddingBottom
	set(value) {
		setPadding(
			paddingLeft, paddingTop, paddingRight, value
		)
	}
;
package ic.android.ui.view.ext


import android.view.View
import android.view.ViewGroup

import ic.android.util.units.pxToDp
import ic.android.util.units.dpToPx
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


inline var View.leftMarginPx : Int32
	get() {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			return layoutParams.leftMargin
		} else {
			return 0
		}
	}
	set(value) {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			layoutParams.leftMargin = value
			this.layoutParams = layoutParams
		}
	}
;

inline var View.leftMarginDp : Float32
	get() = pxToDp(leftMarginPx)
	set(value) { leftMarginPx = dpToPx(value) }
;


inline var View.rightMarginPx : Int32
	get() {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			return layoutParams.rightMargin
		} else {
			return 0
		}
	}
	set(value) {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			layoutParams.rightMargin = value
			this.layoutParams = layoutParams
		}
	}
;

inline var View.rightMarginDp : Float32
	get() = pxToDp(rightMarginPx)
	set(value) { rightMarginPx = dpToPx(value) }
;


inline var View.topMarginPx : Int32
	get() {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			return layoutParams.topMargin
		} else {
			return 0
		}
	}
	set(value) {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			layoutParams.topMargin = value
			this.layoutParams = layoutParams
		}
	}
;

inline var View.topMarginDp : Float32
	get() = pxToDp(topMarginPx)
	set(value) { topMarginPx = dpToPx(value) }
;


inline var View.bottomMarginPx : Int32
	get() {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			return layoutParams.bottomMargin
		} else {
			return 0
		}
	}
	set(value) {
		val layoutParams = layoutParams
		if (layoutParams is ViewGroup.MarginLayoutParams) {
			layoutParams.bottomMargin = value
			this.layoutParams = layoutParams
		}
	}
;

inline var View.bottomMarginDp : Float32
	get() = pxToDp(bottomMarginPx)
	set(value) { bottomMarginPx = dpToPx(value) }
;
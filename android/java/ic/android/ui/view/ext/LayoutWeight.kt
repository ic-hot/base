package ic.android.util


import android.view.View
import android.widget.LinearLayout


var View.layoutWeight : Float

	get() {
		val layoutParams = layoutParams as LinearLayout.LayoutParams
		return layoutParams.weight
	}

	set(value) {
		val layoutParams = layoutParams as LinearLayout.LayoutParams
		layoutParams.weight = value
		this.layoutParams = layoutParams
	}

;
package ic.android.ui.view.ext


import android.view.View


@Deprecated("Use platform call setOnLongClickListener")
inline fun View.setOnLongClickAction (crossinline onLongClickAction: () -> Unit) {
	setOnLongClickListener {
		onLongClickAction()
		true
	}
}
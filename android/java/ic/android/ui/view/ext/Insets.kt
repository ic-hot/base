package ic.android.ui.view.ext


import ic.math.funs.max

import android.view.View

import ic.android.ui.activity.ext.heightPx
import ic.android.ui.activity.ext.insets.bottomInsetPx
import ic.android.ui.activity.ext.insets.topInsetPx


val View.topInsetPx : Int get() {

	return max(
		0,
		activity.topInsetPx - globalYWithoutTranslationInPx
	)

}


val View.bottomInsetPx : Int get() {

	return max(0, activity.bottomInsetPx - (activity.heightPx - (globalYWithoutTranslationInPx + measuredHeight)))

}
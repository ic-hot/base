@file:Suppress("NOTHING_TO_INLINE", "PROTECTED_CALL_FROM_PUBLIC_INLINE")


package ic.android.ui.view.ext


import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.float32.Float32

import android.graphics.Canvas
import android.graphics.drawable.GradientDrawable
import android.view.View
import ic.base.primitives.int32.Int32


inline fun View.setBackgroundCircle (

	colorArgb: Int,

	strokeWidthPx : Float32 = Float32(0),
	strokeColorArgb : Int32 = 0,

	crossinline beforeDraw : () -> Unit = {}

) {

	background = object : GradientDrawable() {

		init {
			shape = OVAL
			setColor(colorArgb)
			setStroke(strokeWidthPx.asInt32, strokeColorArgb)
		}

		override fun draw (canvas: Canvas) {
			beforeDraw()
			super.draw(canvas)
		}

	}

}
package ic.android.ui.view.ext


import android.view.View


inline val View.heightPx : Int get() = height
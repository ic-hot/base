package ic.android.ui.view.ext


import android.view.View
import ic.android.util.units.pxToDp
import ic.base.primitives.float32.Float32


@Deprecated("Do not use")
inline val View.widthPx : Int get() = width

@Suppress("DEPRECATION")
@Deprecated("Do not use")
inline val View.widthDp : Float32 get() = pxToDp(widthPx)
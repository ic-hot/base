package ic.android.ui.view.ext


import android.view.View
import android.view.ViewGroup
import android.view.ViewParent


inline fun View.hasParent (predicate: (ViewGroup) -> Boolean) : Boolean {

	var parent : ViewParent? = parent

	while (true) {

		if (parent !is ViewGroup) return false

		if (predicate(parent)) return true

		parent = parent.parent

	}

}
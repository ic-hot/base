@file:Suppress("NOTHING_TO_INLINE", "PROTECTED_CALL_FROM_PUBLIC_INLINE")


package ic.android.ui.view.ext


import ic.base.primitives.int32.Int32

import android.graphics.Canvas
import android.graphics.drawable.GradientDrawable
import android.view.View
import ic.android.util.units.dpToPx
import ic.base.kfunctions.DoNothing
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asFloat32


inline fun View.setBackgroundRoundCorners (

	color : Int32 = 0,

	topLeftCornerRadiusPx 		: Int32 = 0,
	topRightCornerRadiusPx 		: Int32 = 0,
	bottomLeftCornerRadiusPx 	: Int32 = 0,
	bottomRightCornerRadiusPx 	: Int32 = 0,

	strokeWidthPx : Int32 = 0,
	strokeColorArgb : Int32 = 0x00000000,

	crossinline beforeDraw : () -> Unit = {}

) {

	background = object : GradientDrawable() {

		init {
			shape = RECTANGLE
			cornerRadii = floatArrayOf(
				topLeftCornerRadiusPx.asFloat32, 		topLeftCornerRadiusPx.asFloat32,
				topRightCornerRadiusPx.asFloat32, 		topRightCornerRadiusPx.asFloat32,
				bottomRightCornerRadiusPx.asFloat32,	bottomRightCornerRadiusPx.asFloat32,
				bottomLeftCornerRadiusPx.asFloat32, 	bottomLeftCornerRadiusPx.asFloat32
			)
			setColor(color)
			setStroke(strokeWidthPx, strokeColorArgb)
		}

		override fun draw (canvas: Canvas) {
			beforeDraw()
			super.draw(canvas)
		}

	}

}


inline fun View.setBackgroundRoundCorners (
	color : Int32 = 0,
	cornersRadius : Int32,
	strokeWidth : Int32 = 0,
	strokeColorArgb : Int32 = 0,
	crossinline beforeDraw : () -> Unit = {}
) = setBackgroundRoundCorners(
	color = color,
	topLeftCornerRadiusPx 	= cornersRadius,
	topRightCornerRadiusPx 	= cornersRadius,
	bottomLeftCornerRadiusPx 	= cornersRadius,
	bottomRightCornerRadiusPx = cornersRadius,
	strokeWidthPx = strokeWidth,
	strokeColorArgb = strokeColorArgb,
	beforeDraw = beforeDraw
)


fun View.setBackgroundRoundCorners (
	color : Int32 = 0,
	cornersRadius : Float32,
	strokeWidth : Int32 = 0,
	strokeColorArgb : Int32 = 0,
	beforeDraw : () -> Unit = DoNothing
) = setBackgroundRoundCorners(
	color = color,
	topLeftCornerRadiusPx 		= dpToPx(cornersRadius),
	topRightCornerRadiusPx 		= dpToPx(cornersRadius),
	bottomLeftCornerRadiusPx 	= dpToPx(cornersRadius),
	bottomRightCornerRadiusPx 	= dpToPx(cornersRadius),
	strokeWidthPx = strokeWidth,
	strokeColorArgb = strokeColorArgb,
	beforeDraw = beforeDraw
)

inline fun View.setBackgroundRoundCorners (
	color : Int32 = 0,
	cornersRadius : Float32,
	strokeWidth : Float32,
	strokeColor : Int32 = 0,
	crossinline beforeDraw : () -> Unit = {}
) = setBackgroundRoundCorners(
	color 					= color,
	topLeftCornerRadiusPx 		= dpToPx(cornersRadius),
	topRightCornerRadiusPx 		= dpToPx(cornersRadius),
	bottomLeftCornerRadiusPx 	= dpToPx(cornersRadius),
	bottomRightCornerRadiusPx 	= dpToPx(cornersRadius),
	strokeWidthPx 				= dpToPx(strokeWidth),
	strokeColorArgb 			= strokeColor,
	beforeDraw 					= beforeDraw
)


inline fun View.setBackgroundRoundCorners (
	color : Int32 = 0,
	topCornersRadius 		: Float32 = Float32(0),
	bottomCornersRadius 	: Float32 = Float32(0),
	strokeWidth : Int32 = 0,
	strokeColorArgb : Int32 = 0,
	crossinline beforeDraw : () -> Unit = {}
) = setBackgroundRoundCorners(
	color = color,
	topLeftCornerRadiusPx 		= dpToPx(topCornersRadius),
	topRightCornerRadiusPx 		= dpToPx(topCornersRadius),
	bottomLeftCornerRadiusPx 	= dpToPx(bottomCornersRadius),
	bottomRightCornerRadiusPx 	= dpToPx(bottomCornersRadius),
	strokeWidthPx = strokeWidth,
	strokeColorArgb = strokeColorArgb,
	beforeDraw = beforeDraw
)
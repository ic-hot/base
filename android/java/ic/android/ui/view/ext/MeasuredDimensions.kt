package ic.android.ui.view.ext


import android.view.View

import ic.android.util.units.pxToDp
import ic.base.primitives.int32.Int32


inline val View.measuredWidthPx : Int32 get() = measuredWidth

inline val View.measuredHeightPx : Int32 get() = measuredHeight

inline val View.measuredWidthDp get() = pxToDp(measuredWidthPx)

inline val View.measuredHeightDp get() = pxToDp(measuredHeightPx)
package ic.android.ui.view.ext


import android.view.View


inline val View.activity get() = context as android.app.Activity
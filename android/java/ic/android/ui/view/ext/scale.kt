package ic.android.ui.view.ext


import ic.base.primitives.float32.Float32

import android.view.View


var View.scale : Float32
	get() = scaleX
	set(value) {
		scaleX = value
		scaleY = value
	}
;
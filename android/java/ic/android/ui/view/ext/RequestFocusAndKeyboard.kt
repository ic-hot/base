package ic.android.ui.view.ext


import android.view.View

import ic.android.ui.activity.ext.keyboard.showKeyboard


fun View.requestFocusAndKeyboard() {

	requestFocus()

	activity.showKeyboard(view = this)

}
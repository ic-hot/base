package ic.android.ui.view.ext


import android.view.View


inline fun View.setOnClickAction (crossinline onClickAction: () -> Unit) {
	setOnClickListener { onClickAction() }
}
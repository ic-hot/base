package ic.android.ui.view.ext


import ic.base.primitives.int32.Int32

import android.view.View


val View.globalXWithoutTranslationInPx : Int32 get() {
	val parent = (this.parent as? View) ?: return left
	return parent.globalXWithoutTranslationInPx + left
}

val View.globalYWithoutTranslationInPx : Int32 get() {
	val parent = (this.parent as? View) ?: return top
	return parent.globalYWithoutTranslationInPx + top
}
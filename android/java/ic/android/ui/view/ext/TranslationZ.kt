package ic.android.ui.view.ext


import ic.base.primitives.float32.Float32

import android.view.View


inline var View.translationZPx : Float32
	get() {
		return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			translationZ
		} else {
			Float32(0)
		}
	}
	set(value) {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			translationZ = value
		}
	}
;
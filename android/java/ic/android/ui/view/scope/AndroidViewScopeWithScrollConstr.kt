package ic.android.ui.view.scope


import android.content.Context
import android.view.ViewGroup

import ic.android.util.units.dpToPx
import ic.android.util.units.pxToDp
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


@JvmName("AndroidViewScopeWithScrollPx")
inline fun AndroidViewScopeWithScroll (

	context 		: Context,
	containerView 	: ViewGroup?,

	crossinline onScroll : (scrollPositionPx: Int32) -> Unit

) : AndroidViewScopeWithScroll {

	return object : AndroidViewScopeWithScroll {

		override val context get() 			= context
		override val parentView get() 	= containerView

		override fun notifyScroll (scrollPositionPx: Int32) = onScroll(scrollPositionPx)

		override fun notifyScroll (scrollPositionDp: Float32) {
			onScroll(
				dpToPx(scrollPositionDp)
			)
		}

	}

}


@JvmName("AndroidViewScopeWithScrollDp")
inline fun AndroidViewScopeWithScroll (

	context 		: Context,
	containerView 	: ViewGroup?,

	crossinline onScroll : (scrollPositionDp: Float32) -> Unit

) : AndroidViewScopeWithScroll {

	return object : AndroidViewScopeWithScroll {

		override val context get() 			= context
		override val parentView get() 	= containerView

		override fun notifyScroll (scrollPositionPx: Int32) {
			onScroll(
				pxToDp(scrollPositionPx)
			)
		}

		override fun notifyScroll (scrollPositionDp: Float32) = onScroll(scrollPositionDp)

	}

}
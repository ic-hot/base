package ic.android.ui.view.scope


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


interface AndroidViewScopeWithScroll : AndroidViewScope {

	val topPaddingPx : Int32	get() = 0
	val topPaddingDp : Float32	get() = Float32(0)

	fun notifyScroll (scrollPositionPx: Int32)
	fun notifyScroll (scrollPositionDp: Float32)

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.scope


import android.content.Context
import android.view.ViewGroup


inline fun AndroidViewScope (

	context : Context,

	parentView : ViewGroup? = null

) : AndroidViewScope {

	return object : AndroidViewScope {

		override val context get() = context

		override val parentView get() = parentView

	}

}
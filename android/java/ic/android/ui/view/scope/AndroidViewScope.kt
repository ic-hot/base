package ic.android.ui.view.scope


import android.content.Context
import android.view.ViewGroup

import ic.android.ui.activity.ext.triggerBackButton
import ic.android.ui.view.scope.ext.activity


interface AndroidViewScope {


	val context : Context

	val parentView : ViewGroup?


	fun notifyClickBack() = activity.triggerBackButton()


}
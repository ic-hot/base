package ic.android.ui.view.scope.ext


import android.app.Activity

import ic.android.ui.view.scope.AndroidViewScope


inline val AndroidViewScope.activity get() = context as Activity
package ic.android.ui.notif


sealed class NotificationImportance {

	object Default : NotificationImportance()

	object Low : NotificationImportance()

}
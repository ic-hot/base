package ic.android.ui.notif


import android.app.*
import android.content.Context

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32


fun showNotification (
	channelId   : String,
	channelName	: String,
	groupId	: String,
	notificationId: Int,
	isOngoing : Boolean = false,
	smallIconResId : Int32,
	title : String,
	body : String,
	activityToStart : ActivityToStart? = null
) {

	val context = thisApp

	val notificationManager = (
		context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
	)

	notificationManager.notify(
		notificationId,
		Notification(
			channelId = channelId,
			channelName = channelName,
			groupId = groupId,
			isOngoing = isOngoing,
			smallIconResId = smallIconResId,
			title = title,
			message = body,
			activityToStart = activityToStart
		)
	)

}
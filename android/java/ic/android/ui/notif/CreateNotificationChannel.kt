package ic.android.ui.notif


import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32


fun createNotificationChannel (

	channelId : String,

	channelName : String,

	importance : NotificationImportance = NotificationImportance.Default

) {

	if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) return

	val notificationManager = (
		thisApp.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
	)

	val importanceId : Int32 = when (importance) {
		NotificationImportance.Default -> NotificationManager.IMPORTANCE_DEFAULT
		NotificationImportance.Low -> NotificationManager.IMPORTANCE_LOW
	}

	val notificationChannel = NotificationChannel(channelId, channelName, importanceId)

	notificationManager.createNotificationChannel(notificationChannel)

}
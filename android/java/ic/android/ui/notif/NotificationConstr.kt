package ic.android.ui.notif


import android.app.*

import android.content.Context
import android.content.Intent
import android.os.Build

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32


fun Notification (

	channelId   : String,
	channelName	: String,
	isSoundEnabled : Boolean = true,
	importance : NotificationImportance = NotificationImportance.Default,

	groupId : String? = null,

	isOngoing : Boolean,

	smallIconResId : Int,

	title : String? = null,
	message : String? = null,

	activityToStart : ActivityToStart? = null

) : Notification {

	val context = thisApp

	val notificationManager = (
		context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
	)

	val importanceId : Int32 = (
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			when (importance) {
				NotificationImportance.Default -> NotificationManager.IMPORTANCE_DEFAULT
				NotificationImportance.Low     -> NotificationManager.IMPORTANCE_LOW
			}
		} else {
			0
		}
	)

	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
		if (channelId !in channelsRegistered) {
			notificationManager.createNotificationChannel(
				NotificationChannel(
					channelId,
					channelName,
					importanceId
				).apply {
					if (!isSoundEnabled) {
						setSound(null, null)
					}
				}
			)
			channelsRegistered.add(channelId)
		}
	}

	val builder = (
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			@Suppress("DEPRECATION")
			Notification.Builder(thisApp)
		} else {
			Notification.Builder(thisApp, channelId)
		}
	)
	builder.setOngoing(isOngoing)
	builder.setSmallIcon(smallIconResId)
	if (title != null) {
		builder.setContentTitle(title)
	}
	if (message != null) {
		builder.setContentText(message)
	}
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
		builder.setChannelId(channelId)
	}
	if (groupId != null) {
		builder.setGroup(groupId)
	}
	builder.setAutoCancel(true)
	if (activityToStart != null) {
		val activityIntent = Intent(context, activityToStart.activityClass).apply {
			putExtras(activityToStart.extras)
		}
		var flags = PendingIntent.FLAG_UPDATE_CURRENT
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			flags = flags or PendingIntent.FLAG_IMMUTABLE
		}
		builder.setContentIntent(
			PendingIntent.getActivity(
				context,
				(Math.random() * 4096).toInt(),
				activityIntent,
				flags
			)
		)
	}
	return builder.build()

}
package ic.android.ui.notif


import ic.struct.set.editable.EditableSet


internal val channelsRegistered = EditableSet<String>()
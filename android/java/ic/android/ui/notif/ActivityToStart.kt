package ic.android.ui.notif


import android.app.Activity
import android.os.Bundle


data class ActivityToStart (

	val activityClass  : Class<out Activity>,

	val extras : Bundle

)
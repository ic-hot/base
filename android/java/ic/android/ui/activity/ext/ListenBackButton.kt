package ic.android.ui.activity.ext


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.base.throwables.NotSupportedException
import ic.design.task.Task
import ic.ifaces.cancelable.Cancelable


fun Activity.listenBackButtonBlocking (onBackButton: () -> Unit) : Cancelable {

	if (this !is AbstractActivity) throw NotSupportedException.Runtime()

	impl.run {

		onBackButtonBlockingActionsStack.add(onBackButton)
		return Task(
			cancel = {
				onBackButtonBlockingActionsStack.remove(onBackButton)
			}
		)

	}

}
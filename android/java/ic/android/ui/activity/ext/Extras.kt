package ic.android.ui.activity.ext


import android.app.Activity
import android.os.Bundle


inline val Activity.extras : Bundle get() = intent.extras ?: Bundle()
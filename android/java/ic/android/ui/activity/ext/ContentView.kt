package ic.android.ui.activity.ext


import android.app.Activity
import android.view.View

import ic.android.ui.activity.AbstractActivity


val Activity.contentViewOrNull : View? get() {

	if (this is AbstractActivity) {
		return this.contentViewOrNull
	} else {
		return findViewById(android.R.id.content)
	}

}


inline val Activity.contentView get() = contentViewOrNull!!
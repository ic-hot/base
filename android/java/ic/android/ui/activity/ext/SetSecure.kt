package ic.android.ui.activity.ext


import android.app.Activity
import android.view.WindowManager.LayoutParams.FLAG_SECURE


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.setSecure (isSecure: Boolean) {

	if (isSecure) {

		window.setFlags(FLAG_SECURE, FLAG_SECURE)

	} else {

		window.setFlags(0, FLAG_SECURE)

	}

}
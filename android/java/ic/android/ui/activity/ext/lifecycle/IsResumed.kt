package ic.android.ui.activity.ext.lifecycle


import android.app.Activity

import ic.android.ui.activity.AbstractActivity


val Activity.isResumed : Boolean get() {

	if (this is AbstractActivity) {

		return impl.isResumed

	} else {

		return false

	}

}
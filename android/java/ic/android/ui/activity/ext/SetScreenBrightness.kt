package ic.android.ui.activity.ext


import android.app.Activity

import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.isNaN
import ic.math.ext.clamp


fun Activity.setScreenBrightness (brightness: Float32) {

	val layoutParams = window.attributes

	layoutParams.screenBrightness = (
		if (brightness.isNaN) {
			-1F
		} else {
			brightness.clamp()
		}
	)

	window.attributes = layoutParams

}
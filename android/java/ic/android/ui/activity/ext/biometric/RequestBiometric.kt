package ic.android.ui.activity.ext.biometric


import android.annotation.SuppressLint
import android.app.Activity
import android.hardware.biometrics.BiometricPrompt
import android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import android.os.CancellationSignal

import ic.android.ui.activity.AbstractActivity
import ic.android.ui.activity.ext.biometric.old.requestBiometricBeforeAndroidP
import ic.android.ui.toast.showToast
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@SuppressLint("MissingPermission")
@Suppress("DEPRECATION", "OVERRIDE_DEPRECATION")
fun Activity.requestBiometric (titleText: String, cancelText: String) {

	if (SDK_INT < VERSION_CODES.P) {
		return requestBiometricBeforeAndroidP()
	}

	if (this !is AbstractActivity) throw NotSupportedException.Runtime("className: $className")

	if (!isBiometricSupported) {
		onBiometricError()
		return
	}

	val biometricPrompt = (
		BiometricPrompt.Builder(this).apply {
			setTitle(titleText)
			setNegativeButton(cancelText, mainExecutor) { _, _ ->
				onBiometricCanceled()
			}
		}.build()
	)

	val cancellationSignal = CancellationSignal().apply {
		setOnCancelListener {
			onBiometricCanceled()
		}
	}

	val callback = (
		@SuppressLint("NewApi")
		object : AuthenticationCallback() {
			override fun onAuthenticationError (errorCode: Int, errString: CharSequence?) {
				if (errorCode == 10) {
					onBiometricCanceled()
				} else {
					errString?.toString()?.let { showToast(it) }
					onBiometricError()
				}
			}
			override fun onAuthenticationSucceeded (result: BiometricPrompt.AuthenticationResult?) {
				onBiometricSuccess()
			}
		}
	)

	biometricPrompt.authenticate(cancellationSignal, mainExecutor, callback)

}
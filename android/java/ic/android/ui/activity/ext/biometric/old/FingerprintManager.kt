@file:Suppress("DEPRECATION")


package ic.android.ui.activity.ext.biometric.old


import android.app.Activity
import android.content.Context.FINGERPRINT_SERVICE
import android.hardware.fingerprint.FingerprintManager
import android.os.Build


val Activity.fingerprintManager : FingerprintManager?

	get() {

		return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return null
		} else {
			getSystemService(FINGERPRINT_SERVICE) as FingerprintManager
		}

	}

;
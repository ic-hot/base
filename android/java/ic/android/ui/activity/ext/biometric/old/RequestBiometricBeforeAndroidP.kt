@file:Suppress("DEPRECATION", "OVERRIDE_DEPRECATION")


package ic.android.ui.activity.ext.biometric.old


import android.annotation.SuppressLint
import android.app.Activity
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import android.os.CancellationSignal

import ic.android.ui.activity.AbstractActivity
import ic.android.ui.activity.ext.biometric.isBiometricSupported
import ic.android.ui.toast.showToast
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@SuppressLint("MissingPermission")
internal fun Activity.requestBiometricBeforeAndroidP() {

	if (this !is AbstractActivity) throw NotSupportedException.Runtime("className: $className")

	if (SDK_INT < VERSION_CODES.M) {
		onBiometricError()
		return
	}

	if (!isBiometricSupported) {
		onBiometricError()
		return
	}

	val fingerprintManager = this.fingerprintManager

	val cryptoObject = null

	val cancellationSignal = CancellationSignal().apply {
		setOnCancelListener {
			onBiometricCanceled()
		}
	}

	val callback = (
		@SuppressLint("NewApi")
		object : AuthenticationCallback() {
			override fun onAuthenticationError (errorCode: Int, errString: CharSequence?) {
				if (errorCode == 5) {
					onBiometricCanceled()
				} else {
					errString?.toString()?.let { showToast(it) }
					onBiometricError()
				}
			}
			override fun onAuthenticationSucceeded (result: AuthenticationResult?) {
				onBiometricSuccess()
			}
		}
	)

	fingerprintManager!!.authenticate(cryptoObject, cancellationSignal, 0, callback, null)

}
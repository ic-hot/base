package ic.android.ui.activity.ext.biometric


import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import ic.android.ui.activity.ext.biometric.old.fingerprintManager


val Activity.isBiometricSupported : Boolean

	@SuppressLint("MissingPermission")
	get() {

		if (SDK_INT < VERSION_CODES.M) return false

		val fingerprintManager = this.fingerprintManager ?: return false

		@Suppress("DEPRECATION")
		if (!fingerprintManager.isHardwareDetected) return false

		@Suppress("DEPRECATION")
		if (!fingerprintManager.hasEnrolledFingerprints()) return false

		else return true

	}

;
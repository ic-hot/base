package ic.android.ui.activity.ext.keyboard


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.ifaces.action.Action


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.addOnKeyboardStateChangedAction (action: Action) {

	if (this !is AbstractActivity) return

	impl.onKeyboardStateChangedActions.addIfNotExists(action)

}


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.removeOnKeyboardStateChangedAction (action: Action) {

	if (this !is AbstractActivity) return

	impl.onKeyboardStateChangedActions.removeIfExists(action)

}
package ic.android.ui.activity.ext.keyboard


import android.app.Activity

import ic.android.ui.activity.AbstractActivity


inline val Activity.isKeyboardShown : Boolean get() {
	if (this is AbstractActivity) {
		return impl.isKeyboardShown
	} else {
		return false
	}
}
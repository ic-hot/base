package ic.android.ui.activity.ext.keyboard


import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager

import ic.android.ui.activity.AbstractActivity
import ic.util.log.logD


fun Activity.showKeyboard() {
	if (this is AbstractActivity) {
		if (isKeyboardShown) {
			logD("ActivityImpl") {
				"$this showKeyboard Keyboard is already shown."
			}
			return
		} else {
			logD("ActivityImpl") {
				"$this showKeyboard Keyboard is hidden. Showing keyboard."
			}
		}
	} else {
		logD("ActivityImpl") {
			"$this showKeyboard Previous keyboard state is unknown. Showing keyboard."
		}
	}
	val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
	@Suppress("DEPRECATION")
	imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
	@Suppress("DEPRECATION")
	imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
}


fun Activity.showKeyboard (view: View) {
	if (this is AbstractActivity) {
		if (isKeyboardShown) {
			logD("ActivityImpl") {
				"$this showKeyboard Keyboard is already shown."
			}
			return
		} else {
			logD("ActivityImpl") {
				"$this showKeyboard Keyboard is hidden. Showing keyboard."
			}
		}
	} else {
		logD("ActivityImpl") {
			"$this showKeyboard Previous keyboard state is unknown. Showing keyboard."
		}
	}
	val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
	imm.showSoftInput(view, 0)
}
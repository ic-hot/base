package ic.android.ui.activity.ext.keyboard


import android.app.Activity

import ic.design.task.Task
import ic.ifaces.action.Action


inline fun Activity.listenKeyboardState (

	crossinline onKeyboardStateChanged : () -> Unit

) : Task {

	onKeyboardStateChanged()

	val action = Action {
		onKeyboardStateChanged()
	}

	addOnKeyboardStateChangedAction(action)

	return Task(
		cancel = {
			removeOnKeyboardStateChangedAction(action)
		}
	)

}
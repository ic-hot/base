package ic.android.ui.activity.ext.keyboard


import android.app.Activity
import android.view.inputmethod.InputMethodManager

import ic.android.ui.activity.AbstractActivity
import ic.android.ui.activity.ext.contentView
import ic.android.util.handler.post
import ic.util.log.logD


fun Activity.hideKeyboard() {

	if (this is AbstractActivity) {
		if (impl.keyboardHidingTask == null) {
			if (impl.isKeyboardShown) {
				logD("ActivityImpl") { "$this hideKeyboard Keyboard shown. Hiding keyboard." }
				impl.keyboardHidingTask = post {
					impl.keyboardHidingTask = null
				}
			} else {
				logD("ActivityImpl") { "$this hideKeyboard Keyboard is already hidden." }
				return
			}
		} else {
			logD("ActivityImpl") { "$this hideKeyboard Keyboard hiding is already in action." }
		}
	} else {
		logD("ActivityImpl") {
			"$this hideKeyboard Previous keyboard state is unknown. Hiding keyboard."
		}
	}

	val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
	@Suppress("DEPRECATION")
	imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
	@Suppress("DEPRECATION")
	imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY)

	imm.hideSoftInputFromWindow(contentView.windowToken, 0)

}
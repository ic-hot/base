package ic.android.ui.activity.ext.selectcontent


import android.app.Activity
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import ic.android.ui.activity.impl.REQUEST_CODE_SELECT_FILE


fun Activity.openFileSelector() {

	val getContentIntent = Intent().apply {
		type = "*/*"
		action = ACTION_GET_CONTENT
	}

	val chooserIntent = Intent.createChooser(getContentIntent, null)

	startActivityForResult(
		chooserIntent,
		REQUEST_CODE_SELECT_FILE
	)

}
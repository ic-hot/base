package ic.android.ui.activity.ext.selectcontent


import android.app.Activity
import android.content.Intent
import android.content.Intent.*

import ic.android.ui.activity.impl.REQUEST_CODE_SELECT_IMAGE


fun Activity.openImageSelector (titleText: String) {

	val getIntent = Intent(ACTION_GET_CONTENT).apply { type = "image/*" }

	/*val pickIntent = Intent(ACTION_PICK, EXTERNAL_CONTENT_URI).apply { type = "image/*" }

	val chooserIntent = createChooser(getIntent, titleText).apply {
		putExtra(
			EXTRA_INITIAL_INTENTS,
			arrayOf(pickIntent)
		)
	}

	startActivityForResult(chooserIntent, REQUEST_CODE_SELECT_IMAGE)
	*/*/

	startActivityForResult(getIntent, REQUEST_CODE_SELECT_IMAGE)

}
package ic.android.ui.activity.ext


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.base.primitives.int32.Int32


val Activity.widthPx : Int32 get() {
	if (this is AbstractActivity) {
		return impl.widthPx
	} else {
		return 0 // TODO
	}
}

val Activity.heightPx : Int32 get() {
	if (this is AbstractActivity) {
		return impl.heightPx
	} else {
		return 0 // TODO
	}
}
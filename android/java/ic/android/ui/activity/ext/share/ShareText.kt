package ic.android.ui.activity.ext.share


import android.app.Activity
import android.content.Intent


fun Activity.shareText (text: String, subject: String? = null, chooserTitle: String? = null) {
	val sharingIntent = Intent(Intent.ACTION_SEND)
	sharingIntent.type = "text/plain"
	if (subject != null) {
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
	}
	sharingIntent.putExtra(Intent.EXTRA_TEXT, text)
	startActivity(
		Intent.createChooser(
			sharingIntent,
			if (chooserTitle == null) "" else chooserTitle
		)
	)
}
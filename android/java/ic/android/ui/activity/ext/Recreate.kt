package ic.android.ui.activity.ext


import android.app.Activity
import android.content.Intent

import ic.android.system.isXiaomi
import ic.android.ui.activity.AbstractActivity


fun Activity.safeRecreate() {
	if (isXiaomi) {
		val intent = Intent(this, this.javaClass)
		finish()
		startActivity(intent)
	} else {
		recreate()
	}
}


internal fun Activity.recreateOrScheduleForRecreating() {
	if (this is AbstractActivity) {
		impl.run {
			if (isResumed) {
				safeRecreate()
			} else {
				isScheduledToRecreate = true
			}
		}
	} else {
		safeRecreate()
	}
}
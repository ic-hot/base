package ic.android.ui.activity.ext


import android.app.Activity


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.triggerBackButton() {

	@Suppress("DEPRECATION")
	onBackPressed()

}
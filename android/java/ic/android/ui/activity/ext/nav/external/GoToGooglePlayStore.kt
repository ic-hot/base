package ic.android.util.intent


import android.content.ActivityNotFoundException

import android.content.Context
import android.content.Intent
import android.net.Uri


fun Context.goToGooglePlayStore (applicationId: String) {

	try {

		startActivity(
			Intent(
				Intent.ACTION_VIEW,
				Uri.parse("market://details?id=$applicationId")
			)
		)

	} catch (e: ActivityNotFoundException) {

		startActivity(
			Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/details?id=$applicationId")
			)
		)

	}

}
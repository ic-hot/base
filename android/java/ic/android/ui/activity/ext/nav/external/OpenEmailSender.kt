@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.activity.ext.nav.external


import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.toArray

import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.net.Uri


fun Context.openEmailSender (emailAddresses: Collection<String>, subject: String = "", text: String = "") {

	val intent = Intent(
		ACTION_SENDTO,
		Uri.parse("mailto:")
	)

	intent.putExtra(EXTRA_EMAIL, emailAddresses.toArray())
	intent.putExtra(EXTRA_SUBJECT, subject)
	intent.putExtra(EXTRA_TEXT, text)

	startActivity(intent)

}


inline fun Context.openEmailSender (emailAddress: String, subject: String = "", text: String = "") {
	openEmailSender(
		emailAddresses = Collection(emailAddress),
		subject = subject,
		text = text
	)
}
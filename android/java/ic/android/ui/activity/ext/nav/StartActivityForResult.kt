@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.activity.ext.nav


import kotlin.reflect.KClass

import android.app.Activity
import android.content.Intent
import android.os.Bundle

import ic.android.util.bundle.Bundle
import ic.base.primitives.int32.Int32
import ic.base.reflect.ext.className
import ic.util.log.logD


fun Activity.startActivityForResult (

	activityClass : Class<out Activity>,

	requestCode : Int,

	extras : Bundle = Bundle()

) {

	logD("ActivityNavigation") {
		"startActivityForResult $className " +
		"activityClass: ${ activityClass.name }, " +
		"requestCode: $requestCode, " +
		"extras: $extras"
	}

	val intent = Intent(this, activityClass)

	intent.putExtras(extras)

	startActivityForResult(intent, requestCode)

}


inline fun Activity.startActivityForResult (
	activityClass : KClass<out Activity>,
	requestCode : Int,
	extras : Bundle = Bundle()
) {
	startActivityForResult(activityClass = activityClass.java, requestCode = requestCode, extras = extras)
}


inline fun Activity.startActivityForResult (
	activityClass : KClass<out Activity>,
	requestCode : Int,
	vararg extras : Pair<String, Any?>
) {
	startActivityForResult(activityClass = activityClass.java, requestCode = requestCode, extras = Bundle(*extras))
}


inline fun Activity.startActivityForResult (
	activityClass : KClass<out Activity>,
	vararg extras : Pair<String, Any?>
) {
	startActivityForResult(activityClass = activityClass.java, requestCode = 0, extras = Bundle(*extras))
}


inline fun <reified ActivityType: Activity> Activity.startActivityForResult (
	vararg extras : Pair<String, Any?>
) {
	startActivityForResult(
		activityClass = ActivityType::class.java, requestCode = 0, extras = Bundle(*extras)
	)
}


inline fun <reified ActivityType: Activity> Activity.startActivityForResult (
	requestCode: Int32,
	vararg extras : Pair<String, Any?>
) {
	startActivityForResult(
		activityClass = ActivityType::class.java, requestCode = requestCode, extras = Bundle(*extras)
	)
}
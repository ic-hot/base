package ic.android.util.intent


import android.app.Activity
import android.content.Intent
import android.net.Uri


fun Activity.startPhoneCall (phoneNumber: String) {

	val intent = Intent(
		Intent.ACTION_CALL,
		Uri.fromParts("tel", phoneNumber, null)
	)
	
	startActivity(intent)

}
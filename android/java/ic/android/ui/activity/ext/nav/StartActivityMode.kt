package ic.android.ui.activity.ext.nav


sealed class StartActivityMode {

	object AddToCurrentTask : StartActivityMode()

	object StartInNewTask : StartActivityMode()

}
package ic.android.ui.activity.ext.nav.external


import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri

import ic.base.throwables.NotSupportedException


@Throws(NotSupportedException::class)
fun Activity.goToTelegramOtThrowNotSupported (telegramChannel: String) {

	run checkingIfTelegramAppInstalled@{

		try {
			@Suppress("DEPRECATION")
			packageManager.getPackageInfo("org.telegram.messenger", 0)
			return@checkingIfTelegramAppInstalled
		} catch (e: PackageManager.NameNotFoundException) {}

		try {
			@Suppress("DEPRECATION")
			packageManager.getPackageInfo("org.thunderdog.challegram", 0)
			return@checkingIfTelegramAppInstalled
		} catch (e: PackageManager.NameNotFoundException) {}

		throw NotSupportedException

	}

	startActivity(
		Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=$telegramChannel"))
	)

}


fun Activity.goToTelegramOrWeb (telegramChannel: String) {

	try {
		goToTelegramOtThrowNotSupported(telegramChannel = telegramChannel)
	} catch (t: NotSupportedException) {

		startActivity(
			Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telegram.me/$telegramChannel"))
		)

	}

}
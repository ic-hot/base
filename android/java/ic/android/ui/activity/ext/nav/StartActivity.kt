@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.activity.ext.nav


import kotlin.reflect.KClass

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.os.Bundle

import ic.android.ui.activity.ext.nav.StartActivityMode.*
import ic.android.util.bundle.Bundle
import ic.util.log.logD


inline fun Context.startActivity (
	activityClass : Class<out Activity>,
	mode : StartActivityMode,
	extras : Bundle = Bundle()
) {
	logD("ActivityNavigation") { "startActivity ${ activityClass.name } mode: $mode, extras: $extras" }
	val intent = Intent(this, activityClass)
	when (mode) {
		AddToCurrentTask -> {}
		StartInNewTask -> {
			intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
			intent.addFlags(FLAG_ACTIVITY_MULTIPLE_TASK)
		}
	}
	intent.putExtras(extras)
	startActivity(intent)
}


inline fun Context.startActivity (activityClass: Class<out Activity>, extras: Bundle) {
	startActivity(activityClass = activityClass, mode = AddToCurrentTask, extras = extras)
}


inline fun Context.startActivity (activityClass: Class<out Activity>) {
	startActivity(activityClass = activityClass, mode = AddToCurrentTask, extras = Bundle())
}



inline fun Context.startActivity (
	activityClass : KClass<out Activity>,
	mode : StartActivityMode = AddToCurrentTask,
	extras : Bundle = Bundle()
) {
	startActivity(activityClass = activityClass.java, mode = mode, extras = extras)
}

inline fun Context.startActivity (activityClass : KClass<out Activity>, extras : Bundle = Bundle()) {
	startActivity(activityClass = activityClass.java, mode = AddToCurrentTask, extras = extras)
}


inline fun Context.startActivity (
	activityClass : KClass<out Activity>,
	mode : StartActivityMode = AddToCurrentTask,
	vararg extras : Pair<String, Any?>
) {
	startActivity(activityClass = activityClass.java, mode = mode, extras = Bundle(*extras))
}

inline fun Context.startActivity (
	activityClass : KClass<out Activity>,
	vararg extras : Pair<String, Any?>
) {
	startActivity(activityClass = activityClass.java, extras = Bundle(*extras))
}

inline fun Context.startActivity (activityClass : KClass<out Activity>) {
	startActivity(activityClass = activityClass.java, mode = AddToCurrentTask, extras = Bundle())
}

inline fun <reified ActivityType: Activity> Context.startActivity() {
	startActivity(activityClass = ActivityType::class.java, mode = AddToCurrentTask, extras = Bundle())
}

inline fun <reified ActivityType: Activity> Context.startActivity (vararg extras : Pair<String, Any?>) {
	startActivity(activityClass = ActivityType::class.java, mode = AddToCurrentTask, extras = Bundle(*extras))
}
package ic.android.ui.activity.ext.nav.external


import ic.struct.list.List

import android.content.Context
import android.content.Intent
import android.net.Uri
import ic.struct.list.ext.concat


fun Context.goToSendSms (phoneNumbers: List<String>, message: String) {

	val intent = Intent(
		Intent.ACTION_SENDTO,
		Uri.parse(
			"smsto:" + phoneNumbers.concat(separator = ';')
		)
	)

	intent.putExtra("sms_body", message)

	startActivity(intent)

}
package ic.android.ui.activity.ext.nav.external


import android.app.Activity
import android.content.Intent
import android.net.Uri
import ic.util.log.logW


fun Activity.openPhoneDialer (phoneNumber: String) {

	try {

		val intent = Intent(
			Intent.ACTION_DIAL,
			Uri.fromParts("tel", phoneNumber, null)
		)

		startActivity(intent)

	} catch (e: Exception) {

		logW("Uncaught") { e }

	}

}
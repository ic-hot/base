package ic.android.ui.activity.ext.nav.external


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS


fun Activity.goToAppSettings() {

	val intent = Intent(ACTION_APPLICATION_DETAILS_SETTINGS).apply {
		data = Uri.fromParts("package", packageName, null)
	}

	startActivity(intent)

}
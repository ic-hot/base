package ic.android.ui.activity.ext.nav.external


import java.util.Locale

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

import ic.android.ui.toast.showToast
import ic.util.geo.lastKnownLocation
import ic.util.geo.Location


fun Context.goToMapsNavigator (destination: Location, origin: Location = lastKnownLocation) {

	var uri = String.format(
		Locale.UK,
		"google.navigation:%f,%f?z=%d&q=%f,%f",
		origin.latitude,
		origin.longitude,
		16,
		destination.latitude,
		destination.longitude
	)

	//Try for google Maps 1st.
	val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
	intent.setPackage("com.google.android.apps.maps")
	try {
		startActivity(intent)
	} catch (ex: ActivityNotFoundException) {
		try { //Now try for GO MAPS / Browser
			uri = "http://maps.google.com/maps?daddr=" + destination.latitude + "," + destination.longitude
			val unrestrictedIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
			startActivity(unrestrictedIntent)
		} catch (innerEx: ActivityNotFoundException) { //Finally show a toast.
			showToast("Install Google maps")
		}
	}

}
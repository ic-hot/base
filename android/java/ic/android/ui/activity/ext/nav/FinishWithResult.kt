@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.activity.ext.nav


import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle

import ic.android.util.bundle.Bundle
import ic.base.reflect.ext.className
import ic.util.log.logD


fun Activity.finishWithResult (

	resultCode : Int,

	result : Bundle

) {

	logD("ActivityNavigation") { "finishWithResult $className resultCode: $resultCode, result: $result" }

	val intent = Intent()

	intent.putExtra("activityClassName", javaClass.name)

	intent.putExtras(result)

	setResult(resultCode, intent)

	finish()

}


inline fun Activity.finishWithResult (result : Bundle) {

	finishWithResult(
		resultCode = RESULT_OK,
		result = result
	)

}


inline fun Activity.finishWithResult (vararg result : Pair<String, Any?>) {
	finishWithResult(
		resultCode = RESULT_OK,
		result = Bundle(*result)
	)
}
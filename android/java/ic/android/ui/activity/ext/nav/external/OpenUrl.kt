package ic.android.ui.activity.ext.nav.external


import android.app.Activity
import android.content.Intent
import android.net.Uri

import ic.util.log.logW


fun Activity.openUrl (urlString: String) {

	try {
		val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
		startActivity(intent)
	} catch (e: Exception) {
		logW("Uncaught") { e }
	}

}
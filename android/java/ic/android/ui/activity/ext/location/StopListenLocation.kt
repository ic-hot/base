package ic.android.ui.activity.ext.location


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.stopListenLocation() {

	if (this is AbstractActivity) {
		stopListenLocation()
	} else {
		throw NotSupportedException.Runtime("className: $className")
	}

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.activity.ext


import android.app.Activity

import ic.android.ui.activity.AbstractActivity


inline fun Activity.deferFinish() {
	if (this is AbstractActivity) {
		deferFinish()
	}
}

inline fun Activity.undeferFinish() {
	if (this is AbstractActivity) {
		undeferFinish()
	}
}
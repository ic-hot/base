package ic.android.ui.activity.ext.insets


import android.app.Activity

import ic.design.task.Task
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable


inline fun Activity.listenInsets (

	crossinline onInsetsChanged : () -> Unit

) : Cancelable {

	onInsetsChanged()

	val action = Action {
		onInsetsChanged()
	}

	addOnInsetsChangedAction(action)

	return Task(
		cancel = {
			removeOnInsetsChangedAction(action)
		}
	)

}
package ic.android.ui.activity.ext.insets


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.ifaces.action.Action


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.addOnInsetsChangedAction (action: Action) {

	if (this !is AbstractActivity) return

	impl.onInsetsChangedActions.addIfNotExists(action)

}


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.removeOnInsetsChangedAction (action: Action) {

	if (this !is AbstractActivity) return

	impl.onInsetsChangedActions.removeIfExists(action)

}
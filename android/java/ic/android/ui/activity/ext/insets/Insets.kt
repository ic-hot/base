package ic.android.ui.activity.ext.insets


import android.app.Activity

import ic.android.ui.activity.AbstractActivity
import ic.android.util.units.pxToDp
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


inline val Activity.topInsetPx : Int32 get() {
	return if (this is AbstractActivity) {
		return impl.topInsetPx
	} else {
		0
	}
}

inline val Activity.bottomInsetPx : Int32 get() {
	return if (this is AbstractActivity) {
		return impl.bottomInsetPx
	} else {
		0
	}
}

inline val Activity.topInsetDp : Float32 get() = pxToDp(topInsetPx)

inline val Activity.bottomInsetDp : Float32 get() = pxToDp(bottomInsetPx)
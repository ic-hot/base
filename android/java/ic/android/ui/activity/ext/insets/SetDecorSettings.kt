package ic.android.ui.activity.ext.insets


import android.app.Activity
import android.os.Build
import android.view.View.*


fun Activity.setDecorSettings (

	isStatusBarLight : Boolean,

	isNavigationBarLight : Boolean

) {

	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return

	@Suppress("DEPRECATION")
	run {

		var systemUiVisibility = window.decorView.systemUiVisibility

		if (isStatusBarLight) {
			systemUiVisibility = systemUiVisibility or SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
		} else {
			systemUiVisibility = systemUiVisibility and SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
		}

		if (isNavigationBarLight) {
			systemUiVisibility = systemUiVisibility or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
		} else {
			systemUiVisibility = systemUiVisibility and SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR.inv()
		}

		window.decorView.systemUiVisibility = systemUiVisibility

	}

}
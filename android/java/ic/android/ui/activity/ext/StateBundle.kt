package ic.android.ui.activity.ext


import android.app.Activity
import android.os.Bundle
import android.os.PersistableBundle


val Activity.stateBundle : Bundle? get() {

	if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

		val stateBundle = Bundle()

		val persistableBundle = PersistableBundle()

		onSaveInstanceState(stateBundle, persistableBundle)

		return stateBundle

	} else {

		return null

	}

}
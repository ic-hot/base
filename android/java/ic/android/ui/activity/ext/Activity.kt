package ic.android.ui.activity.ext


import android.app.Activity


inline val Activity.activity get() = this
package ic.android.ui.activity.ext.permissions


import android.app.Activity
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import ic.android.ui.activity.impl.permissions.isPermissionAlwaysGranted


fun Activity.isPermissionGranted (permission: String) : Boolean {
	if (SDK_INT < VERSION_CODES.M) return true
	if (isPermissionAlwaysGranted(permission)) return true
	return checkSelfPermission(permission) == PERMISSION_GRANTED
}


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.isPermissionGranted (vararg permissions: String) : Boolean {
	return permissions.all { isPermissionGranted(it) }
}


fun Activity.isPermissionGranted (permissions: Iterable<String>) : Boolean {
	return permissions.all { isPermissionGranted(it) }
}
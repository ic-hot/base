package ic.android.ui.activity.ext.permissions


import android.app.Activity


fun Activity.getPermissionStatus (permission: String) : Boolean? {
	return when {
		isPermissionGranted(permission) -> true
		isPermissionDenied(permission)  -> false
		else -> null
	}
}
package ic.android.ui.activity.ext.permissions


import android.app.Activity
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES

import ic.android.ui.activity.impl.permissions.isPermissionAlwaysGranted
import ic.base.arrays.ext.atLeastOne


fun Activity.isPermissionDenied (permission: String) : Boolean {
	if (SDK_INT < VERSION_CODES.M) return false
	if (isPermissionAlwaysGranted(permission)) return false
	return shouldShowRequestPermissionRationale(permission)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.isPermissionDenied (vararg permissions: String) : Boolean {
	return permissions.atLeastOne { isPermissionDenied(it) }
}
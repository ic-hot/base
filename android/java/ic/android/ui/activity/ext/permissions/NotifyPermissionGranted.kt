package ic.android.ui.activity.ext.permissions


import android.app.Activity

import ic.android.ui.activity.AbstractActivity


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.notifyPermissionGranted() {

	this as AbstractActivity

	onPermissionGranted()

}
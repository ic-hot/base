package ic.android.ui.activity.ext.permissions


import android.app.Activity
import android.os.Build

import ic.android.ui.activity.impl.REQUEST_PERMISSIONS_CODE
import ic.android.ui.activity.impl.permissions.isPermissionAlwaysGranted
import ic.base.arrays.ext.copyFilter


fun Activity.requestPermission (vararg permissions : String) {

	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
		notifyPermissionGranted()
		return
	}

	if (isPermissionGranted(*permissions)) {

		notifyPermissionGranted()

	} else {

		requestPermissions(
			permissions.copyFilter { !isPermissionAlwaysGranted(it) },
			REQUEST_PERMISSIONS_CODE
		)

	}

}
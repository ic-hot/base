package ic.android.ui.activity.impl


import android.app.Application
import android.content.Context

import ic.android.app.thisAppOrNull
import ic.android.util.locale.createLocalizedContext


internal fun ActivityImpl.attachBaseContext (newBase: Context) {

	if (thisAppOrNull == null) thisAppOrNull = newBase.applicationContext as Application

	superAttachBaseContext(
		createLocalizedContext(newBase)
	)

}
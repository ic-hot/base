package ic.android.ui.activity.impl.permissions


import android.Manifest.permission.POST_NOTIFICATIONS
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES


fun isPermissionAlwaysGranted (permission: String) : Boolean {

	if (SDK_INT < VERSION_CODES.M) return true

	if (SDK_INT < 33 && permission == POST_NOTIFICATIONS) return true

	else return false

}
package ic.android.ui.activity.impl.permissions


import android.content.pm.PackageManager

import ic.android.ui.activity.impl.ActivityImpl
import ic.android.ui.activity.impl.REQUEST_PERMISSIONS_CODE
import ic.base.arrays.Int32Array
import ic.base.primitives.int32.Int32


internal fun ActivityImpl.onRequestPermissionsResult (

	requestCode : Int32,
	permissions : Array<out String>,
	grantResults : Int32Array

) {

	if (requestCode != REQUEST_PERMISSIONS_CODE) {
		return superOnRequestPermissionsResult(requestCode, permissions, grantResults)
	}

	if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {

		abstractActivity.onPermissionGranted()

	} else {

		abstractActivity.onPermissionDenied()

	}



}
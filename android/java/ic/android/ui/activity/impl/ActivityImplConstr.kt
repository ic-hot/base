package ic.android.ui.activity.impl


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View


inline fun ActivityImpl (

	activity : Activity,

	crossinline superAttachBaseContext : (Context) -> Unit,

	crossinline superOnCreate : (Bundle?) -> Unit,
	crossinline superOnStart : () -> Unit,
	crossinline superOnResume : () -> Unit,
	crossinline superOnPause : () -> Unit,
	crossinline superOnStop : () -> Unit,
	crossinline superOnDestroy : () -> Unit,

	crossinline superOnBackPressed : () -> Unit,

	crossinline superStartActivityForResult : (intent: Intent, requestCode: Int) -> Unit,

	crossinline superStartActivityForResultWithOptions : (intent: Intent, requestCode: Int, options: Bundle?) -> Unit,

	crossinline superOnActivityResult : (requestCode: Int, resultCode: Int, data: Intent?) -> Unit,

	crossinline superOnRequestPermissionsResult
		: (requestCode: Int, permissions: Array<out String>, grantResults: IntArray) -> Unit
	,

	crossinline superFinish : () -> Unit,

	crossinline superOnSaveInstanceState : (Bundle) -> Unit,

	crossinline superSetContentView : (View) -> Unit

) : ActivityImpl {

	return object : ActivityImpl() {

		override val activity get() = activity

		override fun superAttachBaseContext (newBase: Context) {
			superAttachBaseContext(newBase)
		}

		override fun superOnCreate (stateBundle: Bundle?) = superOnCreate(stateBundle)

		override fun superOnStart() = superOnStart()
		override fun superOnResume() = superOnResume()
		override fun superOnPause() = superOnPause()
		override fun superOnStop() = superOnStop()

		override fun superOnBackPressed() = superOnBackPressed()

		override fun superOnRequestPermissionsResult (
			requestCode: Int, permissions: Array<out String>, grantResults: IntArray
		) {
			superOnRequestPermissionsResult(requestCode, permissions, grantResults)
		}

		override fun superFinish() = superFinish()

		override fun superStartActivityForResult (intent: Intent, requestCode: Int) {
			superStartActivityForResult(intent, requestCode)
		}

		override fun superStartActivityForResult (intent: Intent, requestCode: Int, options: Bundle?) {
			superStartActivityForResultWithOptions(intent, requestCode, options)
		}

		override fun superOnActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
			superOnActivityResult(requestCode, resultCode, data)
		}

		override fun superOnSaveInstanceState (stateBundle: Bundle) {
			superOnSaveInstanceState(stateBundle)
		}

		override fun superSetContentView (view: View) {
			superSetContentView(view)
		}

		override fun superOnDestroy() = superOnDestroy()

	}

}
package ic.android.ui.activity.impl


import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import ic.android.app.thisAppOrNull

import ic.android.ui.activity.AbstractActivity
import ic.android.ui.activity.all.allActivities
import ic.android.ui.activity.all.lastForegroundActivity
import ic.android.ui.activity.ext.keyboard.hideKeyboard
import ic.android.ui.activity.ext.safeRecreate
import ic.android.ui.activity.ext.keyboard.showKeyboard
import ic.android.ui.activity.impl.location.startListenLocation
import ic.android.ui.activity.impl.location.stopListenLocation
import ic.android.util.bundle.ext.getAsStringOrNull
import ic.base.primitives.int32.Int32
import ic.base.reflect.ext.className
import ic.base.reflect.extends
import ic.base.reflect.getClassByNameOrNull
import ic.design.task.scope.BaseTaskScope
import ic.design.task.scope.ext.postDelayed
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.struct.set.editable.EditableSet
import ic.util.log.logD
import ic.util.log.logW


abstract class ActivityImpl : BaseTaskScope() {


	abstract val activity : Activity

	inline val abstractActivity get() = activity as AbstractActivity


	abstract fun superAttachBaseContext (newBase: Context)
	abstract fun superOnCreate (stateBundle: Bundle?)
	abstract fun superOnStart()
	abstract fun superOnResume()
	abstract fun superOnPause()
	abstract fun superOnStop()
	abstract fun superOnDestroy()

	abstract fun superOnBackPressed()

	abstract fun superOnRequestPermissionsResult (
		requestCode: Int, permissions: Array<out String>, grantResults: IntArray
	)

	abstract fun superFinish()

	abstract fun superStartActivityForResult (intent: Intent, requestCode: Int)

	abstract fun superStartActivityForResult (intent: Intent, requestCode: Int, options: Bundle?)

	abstract fun superOnActivityResult (requestCode: Int, resultCode: Int, data: Intent?)

	abstract fun superOnSaveInstanceState (stateBundle: Bundle)

	abstract fun superSetContentView (view: View)


	// Lifecycle:

	internal var isResumed : Boolean = false

	internal var isScheduledToRecreate : Boolean = false

	fun onCreate (stateBundle: Bundle?) {
		if (thisAppOrNull == null) thisAppOrNull = activity.application
		allActivities.add(activity)
		superOnCreate(stateBundle)
		isOpen = true
	}

	fun onStart() {
		superOnStart()
	}

	fun onResume() {
		lastForegroundActivity = activity
		isResumed = true
		try {
			superOnResume()
		} catch (e: Exception) {
			// Xiaomi recreate workaround
			logW("Uncaught") { e }
		}
		if (isScheduledToRecreate) {
			activity.safeRecreate()
		} else {
			onResumeKeyboard()
			if (wasListeningLocationWhileResumed) {
				startListenLocation()
			}
		}
	}

	fun onPause() {
		wasListeningLocationWhileResumed = isListeningLocation
		if (isListeningLocation) {
			stopListenLocation()
		}
		superOnPause()
		onPauseKeyboard()
		isResumed = false
	}

	fun onStop() {
		superOnStop()
	}

	fun onDestroy() {
		if (lastForegroundActivity === activity) {
			lastForegroundActivity = null
		}
		isOpen = false
		cancelTasks()
		superOnDestroy()
		allActivities.remove(activity)
	}


	// Size:

	var widthPx  : Int32 = 0
	var heightPx : Int32 = 0


	// Insets:

	var topInsetPx    : Int32 = 0
	var bottomInsetPx : Int32 = 0

	val onInsetsChangedActions = EditableSet<Action>()


	// Back button:

	internal val onBackButtonBlockingActionsStack : MutableList<() -> Unit> = ArrayList()

	fun onBackPressed() {
		activity.hideKeyboard()
		if (onBackButtonBlockingActionsStack.isEmpty()) {
			abstractActivity.onBackButton()
		} else {
			onBackButtonBlockingActionsStack.last()()
		}
	}


	// Defer finish:

	private var finishDeferCount : Int32 = 0
	private var isFinishPending : Boolean = false

	fun deferFinish() {
		finishDeferCount++
	}

	fun undeferFinish() {
		finishDeferCount--
		if (!isFinishPending) return
		if (finishDeferCount > 0) return
		implementFinish()
	}

	private fun implementFinish() {
		if (isKeyboardShown) {
			activity.hideKeyboard()
			postDelayed(256) {
				superFinish()
			}
		} else {
			superFinish()
		}
	}

	fun finish() {
		isFinishPending = true
		if (finishDeferCount > 0) return
		implementFinish()
	}


	// Activity result:

	fun startActivityForResult (intent: Intent, requestCode: Int) {
		isOriginalActivity = true
		superStartActivityForResult(intent, requestCode)
	}

	fun startActivityForResult (intent: Intent, requestCode: Int, options: Bundle?) {
		isOriginalActivity = true
		superStartActivityForResult(intent, requestCode, options)
	}

	private var isOriginalActivity : Boolean = false

	fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
		superOnActivityResult(requestCode, resultCode, data)
		if (resultCode == RESULT_CANCELED) return
		if (data == null) return
		when (requestCode) {
			REQUEST_CODE_SELECT_FILE -> {
				if (resultCode != Activity.RESULT_OK) return
				val uri = data.data ?: return
				abstractActivity.onFileSelected(
					fileInput = ByteInputFromInputStream(
						activity.contentResolver.openInputStream(uri) ?: return
					)
				)
				return
			}
			REQUEST_CODE_SELECT_IMAGE -> {
				if (resultCode != Activity.RESULT_OK) return
				val uri = data.data ?: return
				try {
					val inputStream = activity.contentResolver.openInputStream(uri) ?: return
					val bitmap = BitmapFactory.decodeStream(inputStream)
					if (bitmap == null) {
						abstractActivity.onUnableToParseSelectedImage()
					} else {
						abstractActivity.onImageSelected(bitmap = bitmap)
					}
				} catch (_: java.io.FileNotFoundException) {
					abstractActivity.onSelectedImageNotExists()
				}
				return
			}
		}
		val result = data.extras
		val activityClassName = result?.getAsStringOrNull("activityClassName")
		if (activityClassName != null) {
			val activityClass : Class<out Activity>? = getClassByNameOrNull(activityClassName)
			if (activityClass != null) {
				if (activityClass extends Activity::class) {
					logD("ActivityNavigation") {
						"onActivityResult ${ activity.className } " +
						"activityClass: $activityClassName, " +
						"result: $result"
					}
					abstractActivity.onActivityResult(
						activityClass = activityClass.kotlin,
						result = result
					)
					@Suppress("DEPRECATION")
					abstractActivity.onActivityResult(
						activityClass = activityClass,
						result = result,
						isOriginalActivity = isOriginalActivity
					)
					isOriginalActivity = false
				} else {
					logW("Activity.onActivityResult") {
						"Class is not Activity. " +
						"activityClassName: $activityClassName"
					}
				}
			}
		}
	}


	// Keyboard:

	var isKeyboardShown : Boolean = false; internal set

	var keyboardHidingTask : Cancelable? = null

	private var isKeyboardShownWhenPaused : Boolean = false

	val onKeyboardStateChangedActions = EditableSet<Action>()

	private fun onResumeKeyboard() {
		if (isKeyboardShownWhenPaused) {
			logD("ActivityImpl") {
				"$activity Resuming Activity. Keyboard was shown when paused. Showing keyboard"
			}
			activity.showKeyboard()
		}
	}

	private fun onPauseKeyboard() {
		isKeyboardShownWhenPaused = isKeyboardShown
		if (isKeyboardShownWhenPaused) {
			logD("ActivityImpl") {
				"$activity Pausing Activity. Keyboard has been shown."
			}
		}
		isKeyboardShown = false
	}


	// Location:

	private var locationManagerField : LocationManager? = null

	internal val locationManager get() = locationManagerField ?: (
		(activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager).also {
			locationManagerField = it
		}
	)

	internal var isListeningLocation : Boolean = false

	internal var wasListeningLocationWhileResumed : Boolean = false

	internal var locationListener : LocationListener? = null

	internal var delayedListenLocationTask : Cancelable? = null


	// State:

	internal fun onSaveInstanceState (stateBundle: Bundle) {
		superOnSaveInstanceState(stateBundle)
		abstractActivity.saveStateToBundle(stateBundle)
	}


	// TaskScope

	final override var isOpen : Boolean = false
		private set
	;


}
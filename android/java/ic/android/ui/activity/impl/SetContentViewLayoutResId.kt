package ic.android.ui.activity.impl


import ic.base.primitives.int32.Int32


fun ActivityImpl.setContentView (layoutResId: Int32) {

	setContentView(

		activity.layoutInflater.inflate(layoutResId, null)

	)

}
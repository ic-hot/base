package ic.android.ui.activity.impl


import android.graphics.Rect
import android.view.View

import ic.struct.collection.ext.copy.copy
import ic.struct.collection.ext.foreach.forEach
import ic.util.log.logD


fun ActivityImpl.setContentView (view: View) {

	logD("ActivityImpl") { "$activity setContentView" }

	superSetContentView(view)

	view.setOnApplyWindowInsetsListener { _, insets ->
		logD("ActivityImpl") { "$activity onApplyWindowInsetsListener" }
		@Suppress("DEPRECATION")
		if (
			insets.systemWindowInsetTop    != topInsetPx    ||
			insets.systemWindowInsetBottom != bottomInsetPx
		) {
			topInsetPx    = insets.systemWindowInsetTop
			bottomInsetPx = insets.systemWindowInsetBottom
			logD("ActivityImpl") { "$activity onInsetsChanged $topInsetPx $bottomInsetPx" }
			abstractActivity.onInsetsChanged()
			onInsetsChangedActions.copy().forEach { it.run() }
		}
		insets
	}

	val r = Rect()

	view.viewTreeObserver.addOnGlobalLayoutListener {
		view.getWindowVisibleDisplayFrame(r)
		val widthPx  = view.rootView.width
		val heightPx = view.rootView.height
		// r.bottom is the position above soft keypad or device button.
		// if keypad is shown, the r.bottom is smaller than that before.
		val keypadHeight = heightPx - r.bottom;
		if (keypadHeight > heightPx * 0.15) {
			// 0.15 ratio is perhaps enough to determine keypad height.
			// keyboard is opened
			if (!isKeyboardShown) {
				logD("ActivityImpl") { "$activity Keyboard show detected" }
				isKeyboardShown = true
				abstractActivity.onKeyboardShown()
				onKeyboardStateChangedActions.copy().forEach { it.run() }
			}
		}
		else {
			// keyboard is closed
			if (isKeyboardShown) {
				logD("ActivityImpl") { "$activity Keyboard hide detected" }
				isKeyboardShown = false
				abstractActivity.onKeyboardHidden()
				onKeyboardStateChangedActions.copy().forEach { it.run() }
			}
		}
		if (widthPx != this.widthPx || heightPx != this.heightPx) {
			this.widthPx  = widthPx
			this.heightPx = heightPx
			logD("ActivityImpl") { "$activity onSizeChanged $widthPx $heightPx" }
			abstractActivity.onSizeChanged()
		}
	}

}
package ic.android.ui.activity.impl.location


import android.annotation.SuppressLint

import ic.android.ui.activity.impl.ActivityImpl


@Suppress("NOTHING_TO_INLINE")
@SuppressLint("MissingPermission")
internal inline fun ActivityImpl.stopListenLocation() {


	if (!isListeningLocation) return
	isListeningLocation = false


	val delayedListenLocationTask = this.delayedListenLocationTask

	if (delayedListenLocationTask != null) {

		delayedListenLocationTask.cancel()

		this.delayedListenLocationTask = null

	}


	val locationListener = this.locationListener

	if (locationListener != null) {

		try {
			locationManager.removeUpdates(locationListener)
		} catch (t: Throwable) {}

		this.locationListener = null

	}


}
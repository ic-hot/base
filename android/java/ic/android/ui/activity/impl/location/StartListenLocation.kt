package ic.android.ui.activity.impl.location


import android.annotation.SuppressLint
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

import ic.android.ui.activity.impl.ActivityImpl
import ic.util.geo.lastKnownLocation
import ic.util.geo.lastKnownLocationTime
import ic.android.util.handler.post
import ic.util.geo.Location
import ic.util.log.logW
import ic.util.time.ext.compareTo
import ic.util.time.funs.now
import ic.util.time.plus


@Suppress("NOTHING_TO_INLINE")
@SuppressLint("MissingPermission")
internal inline fun ActivityImpl.startListenLocation() {

	val now = now

	if (isListeningLocation) {

		if (now < lastKnownLocationTime + 1000L * 60) {
			abstractActivity.onLocationUpdated(lastKnownLocation)
		}

		return

	}

	isListeningLocation = true

	if (now < lastKnownLocationTime + 1000L * 60) {
		abstractActivity.onLocationUpdated(lastKnownLocation)
	}

	delayedListenLocationTask = post {

		delayedListenLocationTask = null

		val locationListener = object : LocationListener {

			override fun onLocationChanged (androidLocation: android.location.Location) {

				val location = Location(
					latitude 	= androidLocation.latitude,
					longitude 	= androidLocation.longitude
				)

				lastKnownLocation = location

				abstractActivity.onLocationUpdated(location)

			}

			@Deprecated("Deprecated in Java")
			override fun onStatusChanged (provider: String, status: Int, extras: Bundle?) {}
			override fun onProviderEnabled (provider: String) {}
			override fun onProviderDisabled (provider: String) {}

		}

		val accuracyInM = 256F

		try {

			locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 1024L, accuracyInM, locationListener
			)

			locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 1024L, accuracyInM, locationListener
			)

			this.locationListener = locationListener

		} catch (t: Throwable) {

			try {
				locationManager.removeUpdates(locationListener)
			} catch (e: Throwable) {
				logW("Uncaught") { e }
			}

			abstractActivity.onRequestLocationFailed()

		}

	}

}
package ic.android.ui.activity


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View

import ic.android.system.scaledDensityFactor
import ic.android.system.screenDensityFactor
import ic.android.ui.activity.impl.ActivityImpl
import ic.util.log.logD


@SuppressLint("MissingSuperCall")


abstract class BaseActivity : Activity(), AbstractActivity {


	override val impl = ActivityImpl(
		this,
		superAttachBaseContext = { newBase -> super<Activity>.attachBaseContext(newBase) },
		superOnCreate = { stateBundle -> super<Activity>.onCreate(stateBundle) },
		superOnStart = { super<Activity>.onStart() },
		superOnResume = { super<Activity>.onResume() },
		superOnPause = { super<Activity>.onPause() },
		superOnStop = { super<Activity>.onStop() },
		superOnDestroy = { super<Activity>.onDestroy() },
		superOnBackPressed = {
			@Suppress("DEPRECATION")
			super<Activity>.onBackPressed()
		},
		superStartActivityForResult = { intent, requestCode -> super<Activity>.startActivityForResult(intent, requestCode) },
		superStartActivityForResultWithOptions = { intent, requestCode, options ->
			super<Activity>.startActivityForResult(intent, requestCode, options)
		},
		superOnActivityResult = { requestCode, resultCode, data ->
			super<Activity>.onActivityResult(requestCode, resultCode, data)
		},
		superOnRequestPermissionsResult = { requestCode, permissions, grantResults ->
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				super<Activity>.onRequestPermissionsResult(requestCode, permissions, grantResults)
			}
		},
		superFinish = { super<Activity>.finish() },
		superOnSaveInstanceState = { stateBundle -> super<Activity>.onSaveInstanceState(stateBundle) },
		superSetContentView = { view ->
			super<Activity>.setContentView(view)
		}
	)


	protected open fun initTargetThemeResourceId() : Int? = null

	private var themeResourceId : Int? = null


	override fun attachBaseContext (newBase: Context) {
		super<AbstractActivity>.attachBaseContext(newBase)
	}


	override fun onCreate (stateBundle: Bundle?) {
		super<AbstractActivity>.onCreate(stateBundle)
		screenDensityFactor = resources.displayMetrics.density
		scaledDensityFactor = resources.displayMetrics.scaledDensity
		val targetThemeResourceId = initTargetThemeResourceId()
		if (targetThemeResourceId != null && targetThemeResourceId != themeResourceId) {
			themeResourceId = targetThemeResourceId
			setTheme(targetThemeResourceId)
		}
	}

	override fun onStart()   = super<AbstractActivity>.onStart()
	override fun onResume()  = super<AbstractActivity>.onResume()
	override fun onPause()   = super<AbstractActivity>.onPause()
	override fun onStop()    = super<AbstractActivity>.onStop()
	override fun onDestroy() = super<AbstractActivity>.onDestroy()


	override fun startActivityForResult (intent: Intent, requestCode: Int) {
		super<AbstractActivity>.startActivityForResult(intent, requestCode)
	}

	override fun startActivityForResult (intent: Intent, requestCode: Int, options: Bundle?) {
		super<AbstractActivity>.startActivityForResult(intent, requestCode, options)
	}

	override fun onRequestPermissionsResult (requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super<AbstractActivity>.onRequestPermissionsResult(requestCode, permissions, grantResults)
	}

	override fun finish() = super<AbstractActivity>.finish()

	override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
		super<AbstractActivity>.onActivityResult(requestCode, resultCode, data)
	}


	final override fun onBackPressed() = super<AbstractActivity>.onBackPressed()


	final override fun onSaveInstanceState (stateBundle: Bundle) = super<AbstractActivity>.onSaveInstanceState(stateBundle)

	override fun setContentView (view: View) {
		super<AbstractActivity>.setContentView(view)
	}

	override fun setContentView (layoutResId: Int) {
		super<AbstractActivity>.setContentView(layoutResId)
	}


}
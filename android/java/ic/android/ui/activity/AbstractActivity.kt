package ic.android.ui.activity


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout

import ic.android.ui.activity.impl.ActivityImpl
import ic.android.ui.activity.impl.attachBaseContext
import ic.android.ui.activity.impl.location.startListenLocation
import ic.android.ui.activity.impl.location.stopListenLocation
import ic.android.ui.activity.impl.permissions.onRequestPermissionsResult
import ic.android.ui.activity.impl.setContentView
import ic.base.primitives.int32.Int32
import ic.design.task.scope.TaskScope
import ic.ifaces.cancelable.Cancelable
import ic.stream.input.ByteInput
import ic.util.geo.Location
import kotlin.reflect.KClass


interface AbstractActivity : TaskScope {


	val impl : ActivityImpl

	val contentViewOrNull : View? get() = (this as Activity).findViewById(android.R.id.content)

	val containerView : View? get() = (this as Activity).findViewById(android.R.id.content)

	val overlayContainerView : FrameLayout get() = contentViewOrNull as FrameLayout

	fun onBackButton() = impl.superOnBackPressed()

	fun onBackPressed() = impl.onBackPressed()

	fun onPermissionDenied() {}
	fun onPermissionGranted() {}

	fun deferFinish() 	= impl.deferFinish()
	fun undeferFinish() = impl.undeferFinish()
	fun finish()		= impl.finish()


	fun attachBaseContext (newBase: Context) = impl.attachBaseContext(newBase)

	fun onCreate (stateBundle: Bundle?) = impl.onCreate(stateBundle)
	fun onStart()   = impl.onStart()
	fun onResume()  = impl.onResume()
	fun onPause()   = impl.onPause()
	fun onStop()    = impl.onStop()
	fun onDestroy() = impl.onDestroy()


	fun onRequestPermissionsResult (
		requestCode: Int32, permissions: Array<out String>, grantResults: IntArray
	) {
		impl.onRequestPermissionsResult(
			requestCode = requestCode,
			permissions = permissions,
			grantResults = grantResults
		)
	}


	fun startActivityForResult (intent: Intent, requestCode: Int32) {
		impl.startActivityForResult(intent, requestCode)
	}

	fun startActivityForResult (intent: Intent, requestCode: Int32, options: Bundle?) {
		impl.startActivityForResult(intent, requestCode, options)
	}

	fun onActivityResult (
		activityClass : KClass<out Activity>,
		result : Bundle
	) {}

	fun onActivityResult (
		activityClass : Class<out Activity>,
		result : Bundle,
		isOriginalActivity : Boolean
	) {}

	@Deprecated("To remove")
	fun onImageSelected (bitmap: Bitmap) {}

	@Deprecated("To remove")
	fun onUnableToParseSelectedImage() {}

	@Deprecated("To remove")
	fun onSelectedImageNotExists() {}

	fun onActivityResult (requestCode: Int32, resultCode: Int32, data: Intent?) {
		impl.onActivityResult(requestCode, resultCode, data)
	}

	fun onSaveInstanceState (stateBundle: Bundle) {
		impl.onSaveInstanceState(stateBundle)
	}

	fun setContentView (view: View) {
		impl.setContentView(view)
	}

	fun setContentView (layoutResId: Int32) {
		impl.setContentView(layoutResId)
	}


	// Size:

	fun onSizeChanged() {}


	// Insets:

	fun onInsetsChanged() {}


	// Keyboard:

	fun onKeyboardShown() {}
	fun onKeyboardHidden() {}


	fun saveStateToBundle (stateBundle: Bundle) {}


	// Location:

	fun startListenLocation() = impl.startListenLocation()
	fun stopListenLocation()  = impl.stopListenLocation()

	fun onLocationUpdated (location: Location) {}

	fun onRequestLocationFailed() {}


	// Biometric:

	fun onBiometricCanceled() {}

	fun onBiometricError() {}

	fun onBiometricSuccess() {}


	// Select contentL

	@Deprecated("To remove")
	fun onFileSelected (fileInput: ByteInput) {}


	// TaskScope:

	override fun notifyTaskStarted (task: Cancelable) = impl.notifyTaskStarted(task)
	override fun notifyTaskFinished (task: Cancelable) = impl.notifyTaskFinished(task)
	override fun cancelTasks() = impl.cancelTasks()
	override val isOpen get() = impl.isOpen


}
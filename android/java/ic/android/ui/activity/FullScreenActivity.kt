package ic.android.ui.activity


import android.os.Build
import android.os.Bundle
import android.view.View.*
import android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE


abstract class FullScreenActivity : BaseActivity() {


	override fun onCreate (stateBundle: Bundle?) {

		super.onCreate(stateBundle)

		actionBar?.hide()

		@Suppress("DEPRECATION")
		window.run {

			statusBarColor     = 0x00000000
			navigationBarColor = 0x00000000

			setSoftInputMode(SOFT_INPUT_ADJUST_RESIZE)

			decorView.systemUiVisibility = decorView.systemUiVisibility or (
				SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
				SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
			)

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
				isStatusBarContrastEnforced = false
				isNavigationBarContrastEnforced = false
			}

		}

	}


}
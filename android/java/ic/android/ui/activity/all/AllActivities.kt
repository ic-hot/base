package ic.android.ui.activity.all


import android.app.Activity

import ic.struct.set.editable.EditableSet


internal val allActivities = EditableSet<Activity>()
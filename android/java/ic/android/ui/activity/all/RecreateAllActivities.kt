package ic.android.ui.activity.all


import ic.android.ui.activity.ext.recreateOrScheduleForRecreating
import ic.struct.collection.ext.copy.copy
import ic.struct.list.ext.foreach.forEach


fun recreateAllActivities() {

	allActivities.copy().forEach { activity ->

		activity.recreateOrScheduleForRecreating()

	}

}
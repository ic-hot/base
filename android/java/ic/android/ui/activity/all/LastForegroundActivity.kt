package ic.android.ui.activity.all


import android.annotation.SuppressLint
import android.app.Activity


@SuppressLint("StaticFieldLeak")
var lastForegroundActivity : Activity? = null
	internal set
;
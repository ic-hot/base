@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.alert


import android.app.Activity
import android.app.AlertDialog

import ic.android.ui.activity.ext.deferFinish
import ic.android.ui.activity.ext.undeferFinish


inline fun Activity.showAlert (

	title : String? = null,

	message : String,

	buttonText : String,
	crossinline onClickButton : () -> Unit

) {

	val alertDialog = AlertDialog.Builder(this).apply {
		if (title != null) {
			setTitle(title)
		}
		setMessage(message)
		setCancelable(false)
		setPositiveButton(buttonText) { dialog, _ ->
			dialog.cancel()
			onClickButton()
		}
		setOnCancelListener {
			undeferFinish()
		}
	}.create()

	deferFinish()
	alertDialog.show()

}
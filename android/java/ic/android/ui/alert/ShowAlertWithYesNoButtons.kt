@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.alert


import android.app.Activity
import android.app.AlertDialog

import ic.android.ui.activity.ext.deferFinish
import ic.android.ui.activity.ext.undeferFinish


inline fun Activity.showAlert (

	title : String? = null,

	message : String,

	yesButtonText : String,
	crossinline onClickYes : () -> Unit,

	noButtonText : String,
	crossinline onClickNo : () -> Unit

) {

	val alertDialog = AlertDialog.Builder(this).apply {
		if (title != null) {
			setTitle(title)
		}
		setMessage(message)
		setCancelable(false)
		setPositiveButton(yesButtonText) { dialog, _ ->
			dialog.cancel()
			onClickYes()
		}
		setNegativeButton(noButtonText) { dialog, _ ->
			dialog.cancel()
			onClickNo()
		}
		setOnCancelListener {
			undeferFinish()
		}
	}.create()

	deferFinish()
	alertDialog.show()

}
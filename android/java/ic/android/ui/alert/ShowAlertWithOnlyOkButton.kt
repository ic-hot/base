package ic.android.ui.alert


import android.app.Activity
import android.app.AlertDialog

import ic.android.storage.res.getResString
import ic.android.ui.activity.ext.deferFinish
import ic.android.ui.activity.ext.undeferFinish
import ic.base.primitives.int32.Int32


fun Activity.showAlert (message: String) {

	val alertDialog = AlertDialog.Builder(this).apply {
		setMessage(message)
		setCancelable(false)
		setPositiveButton(
			resources.getString(android.R.string.ok)
		) { dialog, _ ->
			dialog.cancel()
		}
		setOnCancelListener {
			undeferFinish()
		}
	}.create()

	deferFinish()
	alertDialog.show()

}


@Suppress("NOTHING_TO_INLINE")
inline fun Activity.showAlert (messageResId: Int32) = showAlert(
	message = getResString(messageResId)
)
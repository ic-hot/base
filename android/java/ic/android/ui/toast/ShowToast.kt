@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.toast


import ic.util.log.logD

import android.widget.Toast

import ic.android.storage.res.getResString
import ic.android.app.thisApp


inline fun showToast (text: String) {
	logD("showToast") { "text: \"$text\"" }
	Toast.makeText(thisApp, text, Toast.LENGTH_LONG).show()
}

inline fun showToast (textResId : Int) = showToast(getResString(textResId))
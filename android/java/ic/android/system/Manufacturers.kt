package ic.android.system


import android.annotation.SuppressLint
import android.os.Build

import ic.base.strings.inLowerCase


@SuppressLint("DefaultLocale")
val isXiaomi = "xiaomi" in Build.MANUFACTURER.inLowerCase

@SuppressLint("DefaultLocale")
val isHuawei = "huawei" in Build.MANUFACTURER.inLowerCase

@SuppressLint("DefaultLocale")
val isSamsung = "samsung" in Build.MANUFACTURER.inLowerCase
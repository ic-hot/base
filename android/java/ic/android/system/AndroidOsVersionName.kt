package ic.android.system


import android.os.Build

import ic.util.log.logE


val androidOsVersionName : String = try {
	Build.VERSION.RELEASE
} catch (t: Throwable) {
	logE("ic.android.util.androidOsVersionName") { t }
	"UNKNOWN"
}
package ic.android.util


import android.content.Context
import android.os.Build
import android.os.PowerManager

import ic.android.app.thisApp


val isScreenOn : Boolean get() {

	val powerManager = thisApp.getSystemService(Context.POWER_SERVICE) as PowerManager
	return if (Build.VERSION.SDK_INT < 20) {
		@Suppress("DEPRECATION")
		powerManager.isScreenOn
	} else {
		powerManager.isInteractive
	}

}


val isScreenOff get() = !isScreenOn
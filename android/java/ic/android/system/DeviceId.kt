package ic.android.system


import android.annotation.SuppressLint
import android.provider.Settings

import ic.android.app.thisApp
import ic.util.log.logE


val deviceId : String
	@SuppressLint("HardwareIds")
	get() {
		try {
			return Settings.Secure.getString(
				thisApp.contentResolver, Settings.Secure.ANDROID_ID
			)
		} catch (t: Throwable) {
			logE("ic.android.util.deviceId") { t }
			return ""
		}
	}
;
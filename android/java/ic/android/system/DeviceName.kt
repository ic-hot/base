package ic.android.system


import android.annotation.SuppressLint
import android.os.Build

import ic.base.strings.ext.capitalized
import ic.util.log.logE


val deviceName : String
	@SuppressLint("DefaultLocale")
	get() = try {
		if (Build.MODEL.startsWith(Build.MANUFACTURER)) {
			Build.MODEL.capitalized
		} else {
			Build.MANUFACTURER.capitalized + " " + Build.MODEL
		}
	} catch (t: Throwable) {
		logE("ic.android.system.deviceName") { t }
		""
	}
;
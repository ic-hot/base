package ic.android.system.memusage


import android.app.ActivityManager
import android.content.Context
import android.os.Environment
import android.os.StatFs

import ic.android.app.thisApp


class MemoryUsage (
	val totalBytesCount 	: Long,
	val availableBytesCount : Long
) {

	val usedBytesCount : Long get() = totalBytesCount - availableBytesCount

	val usedGbCount 	: Int get() = (usedBytesCount 	/ 1024 / 1024 / 1024).toInt()
	val totalGbCount 	: Int get() = (totalBytesCount 	/ 1024 / 1024 / 1024).toInt()

	val usage : Float get() = if (totalBytesCount == 0L) 0F else usedBytesCount.toFloat() / totalBytesCount.toFloat()

	operator fun plus (other: MemoryUsage) = MemoryUsage(
		totalBytesCount 	= totalBytesCount 		+ other.totalBytesCount,
		availableBytesCount = availableBytesCount 	+ other.availableBytesCount
	)

	companion object {

		val ZERO = MemoryUsage(0L, 0L)

	}

}


private fun getStorageUsage (statFs: StatFs) = MemoryUsage(
	totalBytesCount 	= statFs.blockCountLong 		* statFs.blockSizeLong,
	availableBytesCount = statFs.availableBlocksLong 	* statFs.blockSizeLong
)

fun getInternalStorageUsage() = getStorageUsage(
	StatFs(Environment.getRootDirectory().path)
)

fun getExternalStorageUsage() : MemoryUsage {
	if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
		@Suppress("DEPRECATION")
		return getStorageUsage(
			StatFs(Environment.getExternalStorageDirectory().path)
		)
	} else {
		return MemoryUsage(0, 0)
	}
}

fun getStorageUsage() = getInternalStorageUsage() + getExternalStorageUsage()


fun getRamUsage () : MemoryUsage {
	val activityManager = thisApp.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
	val memoryInfo = ActivityManager.MemoryInfo()
	activityManager.getMemoryInfo(memoryInfo)
	return MemoryUsage(
		totalBytesCount 	= memoryInfo.totalMem,
		availableBytesCount = memoryInfo.availMem
	)
}
package ic.android.system


import ic.util.text.uuid.generateUuid


val deviceGuid : String get() = generateUuid(deviceId)
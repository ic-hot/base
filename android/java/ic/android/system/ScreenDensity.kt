package ic.android.system


import ic.android.app.thisApp
import ic.base.kfunctions.ext.getValue
import ic.base.primitives.float32.Float32
import ic.struct.value.cached.Cached


private val displayMetrics by Cached { thisApp.resources.displayMetrics }

var screenDensityFactor : Float32 = displayMetrics.density

@Suppress("DEPRECATION")
var scaledDensityFactor : Float32 = displayMetrics.scaledDensity
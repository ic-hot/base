package ic.android.service.commands


import ic.android.util.bundle.Bundle

import android.content.Context
import android.content.Intent
import android.os.Bundle


inline fun <reified ServiceClass: android.app.Service> Context.startService (
	extras : Bundle
) {
	startService(
		Intent(this, ServiceClass::class.java).apply {
			putExtras(extras)
		}
	)
}


inline fun <reified ServiceClass: android.app.Service> Context.startService (
	vararg extras : Pair<String, Any?>
) {
	startService<ServiceClass>(
		extras = Bundle(*extras)
	)
}
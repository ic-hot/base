@file:Suppress("NOTHING_TO_INLINE")


package ic.android.service.binding


import ic.ifaces.cancelable.Cancelable

import kotlin.reflect.KClass

import android.content.Context
import android.content.Intent

import ic.android.service.BaseService


fun <ServiceType: BaseService> Context.bindService (
	serviceClass : Class<ServiceType>,
	callback : BindServiceCallback<ServiceType>
) : Cancelable {

	val context = this

	val intent = Intent(this, serviceClass)

	var isCanceled : Boolean = false

	val connection = object : BaseServiceConnection() {

		override fun onBind (service: BaseService) {
			if (isCanceled) return
			callback.onFinish()
			@Suppress("UNCHECKED_CAST")
			callback.onSuccess(
				ServiceBinding(
					context = context,
					connection = this,
					service = service as ServiceType
				)
			)
		}

	}

	bindService(intent, connection, Context.BIND_AUTO_CREATE)

	return Cancelable {
		isCanceled = true
	}

}


inline fun <ServiceType: BaseService> Context.bindService (
	serviceClass : KClass<ServiceType>,
	crossinline onFinish : () -> Unit,
	crossinline onSuccess : (ServiceBinding<ServiceType>) -> Unit
) : Cancelable {
	return bindService(
		serviceClass = serviceClass.java,
		callback = BindServiceCallback(
			onFinish = onFinish,
			onSuccess = onSuccess
		)
	)
}
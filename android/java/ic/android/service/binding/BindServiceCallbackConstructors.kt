package ic.android.service.binding


import ic.android.service.BaseService


inline fun <ServiceType: BaseService> BindServiceCallback (

	crossinline onFinish : () -> Unit,
	crossinline onSuccess : (ServiceBinding<ServiceType>) -> Unit

) : BindServiceCallback<ServiceType> {

	return object : BindServiceCallback<ServiceType> {

		override fun onFinish() {
			onFinish()
		}

		override fun onSuccess (binding: ServiceBinding<ServiceType>) {
			onSuccess(binding)
		}

	}

}
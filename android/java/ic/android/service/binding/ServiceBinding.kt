package ic.android.service.binding


import android.content.Context

import ic.android.service.BaseService
import ic.ifaces.lifecycle.closeable.Closeable


class ServiceBinding<ServiceType: BaseService>

	internal constructor (

		context : Context,

		connection : BaseServiceConnection,

		service : ServiceType

	)

: Closeable {


	private var context : Context? = context

	private var connection : BaseServiceConnection? = connection

	private var serviceField : ServiceType? = service


	val service : ServiceType get() = serviceField!!


	override fun close() {
		val context = this.context
		val connection = this.connection
		if (context != null && connection != null) {
			context.unbindService(connection)
		}
		this.context = null
		this.connection = null
	}


}
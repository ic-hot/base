package ic.android.service.binding


import ic.android.service.BaseService


interface BindServiceCallback<ServiceType: BaseService> {

	fun onFinish()

	fun onSuccess (binding: ServiceBinding<ServiceType>)

}
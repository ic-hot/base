package ic.android.service.binding


import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder

import ic.android.service.BaseService


internal abstract class BaseServiceConnection : ServiceConnection {


	protected abstract fun onBind (service: BaseService)


	override fun onServiceConnected (name: ComponentName?, serviceBinder: IBinder?) {
		if (serviceBinder is BaseService.Binder) {
			onBind(serviceBinder.serviceInstance)
		}
	}


	override fun onServiceDisconnected (name: ComponentName?) {}


}
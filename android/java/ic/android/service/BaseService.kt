package ic.android.service


import ic.base.primitives.int32.Int32

import android.app.Service
import android.content.Intent
import android.os.Bundle
import android.os.IBinder


abstract class BaseService : Service() {


	internal class Binder (

		val serviceInstance : BaseService

	) : android.os.Binder()

	final override fun onBind (intent: Intent) : IBinder {
		return Binder(
			serviceInstance = this
		)
	}


	final override fun onStartCommand (intent: Intent?, flags: Int32, startId: Int32) : Int32 {
		val result = super.onStartCommand(intent, flags, startId)
		val extras = intent?.extras
		if (extras != null) {
			onStartCommand(extras)
		}
		return result
	}

	protected open fun onStartCommand (extras: Bundle) {}


}
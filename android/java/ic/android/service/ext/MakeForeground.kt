package ic.android.service.ext


import android.annotation.SuppressLint
import android.app.Service
import android.content.pm.ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
import android.os.Build
import android.os.Build.VERSION_CODES.TIRAMISU

import ic.android.ui.notif.ActivityToStart
import ic.android.ui.notif.Notification
import ic.base.primitives.int32.Int32


@SuppressLint("ForegroundServiceType")
fun Service.makeForeground (

	channelId		: String,
	channelName		: String,
	groupId			: String? = null,
	notificationId : Int32,

	smallIconResId	: Int32,
	title			: String? = null,
	message			: String? = null,

	activityToStart : ActivityToStart? = null

) {

	val notification = Notification(
		channelId = channelId,
		channelName = channelName,
		isSoundEnabled = false,
		groupId = groupId,
		smallIconResId = smallIconResId,
		title = title,
		message = message,
		isOngoing = true,
		activityToStart = activityToStart
	)

	if (Build.VERSION.SDK_INT < TIRAMISU) {
		startForeground(notificationId, notification)
	} else {
		startForeground(notificationId, notification, FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK)
	}

}
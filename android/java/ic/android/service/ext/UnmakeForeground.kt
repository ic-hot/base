@file:Suppress("NOTHING_TO_INLINE")


package ic.android.service.ext


import android.app.Service


inline fun Service.unmakeForeground() {

	@Suppress("DEPRECATION")
	stopForeground(true)

}
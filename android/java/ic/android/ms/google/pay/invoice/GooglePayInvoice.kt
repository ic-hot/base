package ic.android.ms.google.pay.invoice


import java.math.BigDecimal


data class GooglePayInvoice (

	val useTestEnvironment : Boolean,

	val tokenizationType : String,

	val gateway : String,
	val gatewayMerchantId : String,

	val amountUah : BigDecimal

) {

	companion object

}
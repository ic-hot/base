package ic.android.ms.google.pay.invoice.ext


import android.os.Bundle

import ic.android.ms.google.pay.invoice.GooglePayInvoice
import ic.android.util.bundle.Bundle
import ic.android.util.bundle.ext.getAsBoolean
import ic.android.util.bundle.ext.getAsString
import ic.android.util.bundle.getAsBigDecimal


fun GooglePayInvoice.toBundle() = Bundle(
	"useTestEnvironment" to useTestEnvironment,
	"tokenizationType"   to tokenizationType,
	"gateway"            to gateway,
	"gatewayMerchantId"  to gatewayMerchantId,
	"amountUah"          to amountUah
)


fun GooglePayInvoice.Companion.fromBundle (bundle: Bundle) = GooglePayInvoice(
	useTestEnvironment = bundle.getAsBoolean("useTestEnvironment"),
	tokenizationType   = bundle.getAsString("tokenizationType"),
	gateway            = bundle.getAsString("gateway"),
	gatewayMerchantId  = bundle.getAsString("gatewayMerchantId"),
	amountUah          = bundle.getAsBigDecimal("amountUah")
)
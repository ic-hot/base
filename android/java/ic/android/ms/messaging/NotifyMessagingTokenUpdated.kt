package ic.android.ms.messaging


import ic.android.app.BaseApplication
import ic.android.app.thisApp


fun notifyMessagingTokenUpdated (token: String) {

	(thisApp as BaseApplication).notifyMessagingTokenUpdated(token)

}
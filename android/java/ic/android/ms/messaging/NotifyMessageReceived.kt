package ic.android.ms.messaging


import ic.android.app.BaseApplication
import ic.android.app.thisApp
import ic.struct.map.finite.FiniteMap


fun notifyMessageReceived (

	from: String?,
	notificationTitle: String?,
	notificationBody: String?,
	data: FiniteMap<String, String>

) {

	(thisApp as BaseApplication).notifyMessageReceived(from, notificationTitle, notificationBody, data)

}
package ic.android.ms.messaging


import ic.android.ms.androidPlatformServices


fun subscribeToTopic (topicId: String) {

	androidPlatformServices.subscribeToTopic(topicId)

}
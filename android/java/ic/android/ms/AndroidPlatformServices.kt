package ic.android.ms


import android.app.Activity
import android.content.Context

import ic.android.ms.maps.MapCarrier


interface AndroidPlatformServices {


	val type : AndroidPlatformServicesType


	fun init()


	fun trackEvent (eventName: String, params: Array<out Pair<String, String?>>)

	fun trackScreenStart (activity: Activity, screenName: String)

	fun trackScreenEnd (activity: Activity, screenName: String)


	val messagingToken : String?

	fun subscribeToTopic (topicId: String)


	fun createMapCarrier (context: Context) : MapCarrier


}
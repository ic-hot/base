package ic.android.ms


sealed class AndroidPlatformServicesType {

	object Google : AndroidPlatformServicesType()

	object Huawei : AndroidPlatformServicesType()

}
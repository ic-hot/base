package ic.android.ms.maps


import ic.util.geo.Location

import android.graphics.Bitmap


abstract class MapMarker (

	val position : Location,

	val icon : Bitmap,

	val toCluster : Boolean = false

) {

	abstract fun onClick()

}
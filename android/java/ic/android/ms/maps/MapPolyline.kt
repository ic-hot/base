package ic.android.ms.maps


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.util.geo.Location


class MapPolyline (

	val width : Float32,

	val color : Int32,

	val points : List<Location>

)
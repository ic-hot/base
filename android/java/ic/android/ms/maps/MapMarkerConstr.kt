package ic.android.ms.maps


import ic.util.geo.Location

import android.graphics.Bitmap


inline fun MapMarker (

	position : Location,

	icon : Bitmap,

	toCluster : Boolean = false,

	crossinline onClick : MapMarker.() -> Unit

) : MapMarker {

	return object : MapMarker(
		position = position,
		icon = icon,
		toCluster = toCluster
	) {

		override fun onClick() = onClick(this)

	}

}
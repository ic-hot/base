package ic.android.ms.maps


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.struct.collection.Collection
import ic.util.geo.Location

import android.content.Context
import android.view.View


interface MapCarrier {


	fun initView (context: Context) : View


	fun open (
		styleResId : Int32,
		onReady : () -> Unit
	)


	fun setMapPadding (leftPx: Int32, topPx: Int32, rightPx: Int32, bottomPx: Int32)

	fun setOnCameraMoveAction (onCameraMoveAction : (Location, zoom: Float32) -> Unit)

	fun moveCamera (position: Location, zoom: Float32, toAnimate : Boolean)

	fun moveCameraToBounds (points: Collection<Location>, toAnimate: Boolean)


	fun addMarker (marker: MapMarker)

	fun removeMarker (marker: MapMarker)


	fun addPolyline (polyline: MapPolyline)

	fun removePolyline (polyline: MapPolyline)


	fun clear()


	fun close()


}
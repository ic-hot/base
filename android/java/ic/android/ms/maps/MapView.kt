package ic.android.ms.maps


import ic.android.ui.view.group.ext.get
import ic.android.ms.androidPlatformServices
import ic.android.ui.view.ext.activity
import ic.android.util.handler.postDelayed
import ic.base.R
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.ifaces.cancelable.Cancelable
import ic.struct.set.editable.EditableSet
import ic.struct.collection.Collection
import ic.util.geo.Location
import ic.util.geo.lastKnownLocation

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import ic.android.ui.activity.ext.heightPx
import ic.struct.collection.ext.foreach.forEach


open class MapView : ViewGroup {


	private var initialDelayJob : Cancelable? = null


	private var mapCarrier : MapCarrier? = null


	private val styleResId : Int32


	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		initialDelayJob = postDelayed(256L) {
			val mapCarrier = androidPlatformServices.createMapCarrier(context)
			val view = mapCarrier.initView(context = context)
			addView(view, MATCH_PARENT, MATCH_PARENT)
			mapCarrier.open(
				styleResId = styleResId,
				onReady = {
					this.mapCarrier = mapCarrier
					mapCarrier.setMapPadding(leftPx, topPx, rightPx, bottomPx)
					if (cameraBoundsPoints == null) {
						mapCarrier.moveCamera(cameraPosition, zoom, toAnimate = false)
					} else {
						mapCarrier.moveCameraToBounds(cameraBoundsPoints!!, toAnimate = false)
					}
					mapCarrier.setOnCameraMoveAction { cameraPosition, zoom ->
						this.cameraPosition = cameraPosition
						this.zoom = zoom
						onCameraMoveAction(cameraPosition, zoom)
					}
					markers.forEach { mapCarrier.addMarker(it) }
					polylines.forEach { mapCarrier.addPolyline(it) }
				}
			)
		}
	}


	private var maximumHeightPx = activity.heightPx

	private var isChildMeasured : Boolean = false

	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val widthPx 	= MeasureSpec.getSize(widthMeasureSpec)
		val heightPx 	= MeasureSpec.getSize(heightMeasureSpec)
		if (childCount > 0 && !isChildMeasured) {
			val childView : View = this[0]
			childView.measure(
				MeasureSpec.makeMeasureSpec(maximumHeightPx, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(maximumHeightPx, MeasureSpec.EXACTLY)
			)
			isChildMeasured = true
		}
		setMeasuredDimension(widthPx, heightPx)
	}

	override fun onLayout (changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		if (childCount > 0) {
			val childView : View = this[0]
			val width = right - left
			val height = bottom - top
			val mapWidth = childView.measuredWidth
			val mapHeight = childView.measuredHeight
			val x = (width - mapWidth) / 2
			val y = (height - mapHeight) / 2
			childView.layout(x, y, x + mapWidth, y + mapHeight)
		}
	}


	var cameraPosition : Location = lastKnownLocation; private set

	var zoom : Float32 = Float32(12); private set

	private var cameraBoundsPoints : Collection<Location>? = null

	private var onCameraMoveAction : (Location, zoom: Float32) -> Unit = { _, _ -> }

	fun setOnCameraMoveAction (onCameraMoveAction : (Location, zoom: Float32) -> Unit) {
		this.onCameraMoveAction = onCameraMoveAction
	}

	fun moveCamera (position: Location, zoom: Float32 = this.zoom, toAnimate: Boolean) {
		this.cameraPosition = position
		this.zoom = zoom
		this.cameraBoundsPoints = null
		mapCarrier?.moveCamera(position, zoom, toAnimate)
	}

	fun moveCameraToBounds (points: Collection<Location>, toAnimate: Boolean) {
		this.cameraBoundsPoints = points
		mapCarrier?.moveCameraToBounds(points, toAnimate)
	}


	private var leftPx		: Int32 = 0
	private var topPx 		: Int32 = 0
	private var rightPx		: Int32 = 0
	private var bottomPx	: Int32 = 0

	fun setMapPadding (
		leftPx: Int32 = 0, topPx: Int32 = 0, rightPx: Int32 = 0, bottomPx: Int32 = 0
	) {
		this.leftPx = leftPx
		this.topPx = topPx
		this.rightPx = rightPx
		this.bottomPx = bottomPx
		mapCarrier?.setMapPadding(leftPx, topPx, rightPx, bottomPx)
	}


	private val markers = EditableSet<MapMarker>()

	fun addMarker (marker: MapMarker) {
		markers.add(marker)
		mapCarrier?.addMarker(marker)
	}

	fun removeMarker (marker: MapMarker) {
		markers.remove(marker)
		mapCarrier?.removeMarker(marker)
	}


	private val polylines = EditableSet<MapPolyline>()

	fun addPolyline (polyline: MapPolyline) {
		polylines.add(polyline)
		mapCarrier?.addPolyline(polyline)
	}

	fun removePolyline (polyline: MapPolyline) {
		polylines.remove(polyline)
		mapCarrier?.removePolyline(polyline)
	}


	fun clear() {
		markers.empty()
		polylines.empty()
		mapCarrier?.clear()
	}


	var isTouchEnabled : Boolean = true

	override fun dispatchTouchEvent (event: MotionEvent) : Boolean {
		if (!isTouchEnabled) return false
		return super.dispatchTouchEvent(event)
	}


	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		initialDelayJob?.cancel()
		initialDelayJob = null
		mapCarrier?.close()
		mapCarrier = null
		removeAllViews()
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super (context, attrs, defStyleAttr)
	{
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MapView)
			styleResId = styledAttributes.getResourceId(R.styleable.MapView_mapStyle, 0)
			styledAttributes.recycle()
		} else {
			styleResId = 0
		}
	}


}
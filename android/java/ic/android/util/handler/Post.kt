package ic.android.util.handler


import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable


inline fun post (crossinline action: () -> Unit) : Cancelable {
	var isCanceled : Boolean = false
	globalHandler.post {
		if (!isCanceled) {
			action()
		}
	}
	return Cancelable { isCanceled = true }
}


fun post (action: Action) : Cancelable {
	return post {
		action.run()
	}
}
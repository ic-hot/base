@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.handler


import android.os.Handler

import ic.android.app.thisApp


val globalHandler : Handler by lazy { Handler(thisApp.mainLooper) }
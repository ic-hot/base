package ic.android.util.handler


import ic.base.primitives.int64.Int64
import ic.design.task.Task
import ic.struct.list.List


inline fun <Item> List<Item>.postForEach (
	delayMs : Int64 = 256,
	toCallAtOnce : Boolean = false,
	crossinline onFinish : () -> Unit = {},
	crossinline action: (item: Item) -> Unit
) : Task? {

	return postForEachIndexed (delayMs = delayMs, toCallAtOnce = toCallAtOnce, onFinish = onFinish) { _, item ->
		action(item)
	}

}
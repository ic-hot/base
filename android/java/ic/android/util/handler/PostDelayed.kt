package ic.android.util.handler


import ic.base.primitives.int64.Int64
import ic.ifaces.cancelable.Cancelable
import ic.util.time.duration.Duration


inline fun postDelayed (
	delayInMs : Int64,
	crossinline onCancel : () -> Unit = {},
	crossinline action : () -> Unit
) : Cancelable? {
	if (delayInMs == Long.MAX_VALUE) return null
	if (delayInMs <= 0) return post(action)
	var isCanceled : Boolean = false
	val runnable = Runnable {
		if (!isCanceled) {
			action()
		}
	}
	globalHandler.postDelayed(runnable, delayInMs)
	return Cancelable(
		cancel = {
			isCanceled = true
			globalHandler.removeCallbacks(runnable)
			onCancel()
		}
	)
}


inline fun postDelayed (
	delay: Duration,
	crossinline onCancel : () -> Unit = {},
	crossinline action : () -> Unit
) : Cancelable? {
	return postDelayed(
		delayInMs = delay.inMs,
		onCancel = onCancel,
		action = action
	)
}
package ic.android.util.handler


import ic.base.escape.breakable.Break
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.design.task.Task
import ic.struct.list.ext.length.isEmpty


inline fun <Item> List<Item>.postForEachIndexed (
	delayMs : Int64 = 256,
	toCallAtOnce : Boolean = false,
	batchSize : Int32 = 1,
	crossinline onFinish : () -> Unit = {},
	crossinline action: (index: Int, item: Item) -> Unit
) : Task? {

	if (isEmpty) return null

	var index : Int32 = 0

	return postLoop (
		delayMs = delayMs,
		toCallAtOnce = toCallAtOnce,
		onFinish = onFinish
	) {

		var indexInBatch : Int32 = 0

		while (indexInBatch < batchSize) {

			if (index >= length) Break()

			action(index, this[index])

			indexInBatch++
			index++

		}

	}

}
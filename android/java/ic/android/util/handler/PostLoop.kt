package ic.android.util.handler


import ic.base.primitives.int64.Int64
import ic.base.throwables.Break
import ic.design.task.Task


inline fun postLoop (
	delayMs : Int64 = 0L,
	toCallAtOnce : Boolean = false,
	crossinline onFinish : () -> Unit = {},
	crossinline action : () -> Unit
) : Task? {
	var isCanceled : Boolean = false
	var isFinished : Boolean = false
	lateinit var iteration : () -> Unit
	iteration = iterating@{
		if (isCanceled) return@iterating
		try {
			action()
		} catch (t: Break) {
			isFinished = true
			onFinish()
			return@iterating
		}
		postDelayed(delayMs) {
			iteration()
		}
	}
	if (toCallAtOnce) {
		iteration()
	} else {
		post(iteration)
	}
	if (isFinished) return null
	return Task(
		cancel = { isCanceled = true }
	)
}
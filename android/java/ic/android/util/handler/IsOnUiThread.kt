package ic.android.util.handler


import ic.android.app.thisApp


inline val isOnUiThread : Boolean get() {

	return thisApp.mainLooper.thread == Thread.currentThread()

}
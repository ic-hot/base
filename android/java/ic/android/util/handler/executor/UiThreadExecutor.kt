package ic.android.util.handler.executor


import ic.base.throwables.NotNeededException
import ic.android.util.handler.globalHandler
import ic.android.util.handler.isOnUiThread
import ic.ifaces.action.Action
import ic.ifaces.executor.Executor
import ic.ifaces.stoppable.Stoppable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


object UiThreadExecutor : Executor {


	override fun execute (action: Action) : Stoppable? {

		if (isOnUiThread) {

			action.run()
			return null

		} else {

			val mutex = Mutex()

			var isCanceled : Boolean = false

			globalHandler.post {
				mutex.synchronized {
					if (!isCanceled) {
						action.run()
					}
				}
			}

			return Stoppable(
				stopNonBlockingOrThrowNotNeeded = {
					mutex.synchronized {
						if (isCanceled) throw NotNeededException
						isCanceled = true
					}
				},
				waitFor = {}
			)

		}
	}


}
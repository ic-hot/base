package ic.android.util.bundle.serial


import ic.base.throwables.NotSupportedException

import android.os.Bundle


interface BundleSerializer {

	@Throws(NotSupportedException::class)
	fun serializeToBundle (bundle: Bundle, key: String, value: Any?)

}
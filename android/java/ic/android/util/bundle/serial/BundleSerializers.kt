package ic.android.util.bundle.serial


import ic.struct.list.editable.EditableList


internal val bundleSerializers = EditableList<BundleSerializer>()

fun addBundleSerializer (bundleSerializer: BundleSerializer) {
	bundleSerializers.add(bundleSerializer)
}
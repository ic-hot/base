@file:Suppress("FunctionName")


package ic.android.util.bundle


import android.os.Bundle

import ic.android.util.bundle.ext.set


fun Bundle (vararg entries: Pair<String, Any?>) : Bundle {

	val bundle = Bundle()

	entries.forEach {
		bundle[it.first] = it.second
	}

	return bundle

}
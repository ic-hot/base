@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle


inline fun Bundle.getAsInt32OrNull (key: String) : Int? {
	return if (containsKey(key)) {
		getInt(key)
	} else {
		null
	}
}


inline fun Bundle.getAsInt32 (key: String) = getAsInt32OrNull(key)!!
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle
import ic.base.arrays.ext.asList

import ic.struct.list.List


inline fun Bundle.getAsListOfStringsOrNull (key: String) : List<String>? {

	return getStringArray(key)?.asList(isArrayImmutable = true)

}


inline fun Bundle.getAsListOfStrings (key: String) = getAsListOfStringsOrNull(key) ?: List()
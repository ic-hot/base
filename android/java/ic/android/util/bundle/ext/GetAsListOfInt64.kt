package ic.android.util.bundle.ext


import android.os.Bundle

import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfInt64OrNull (key: String) : List<Int64>? {

	return getLongArray(key)?.let { ListFromArray(it, isArrayImmutable = true) }

}


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfInt64 (key: String) = getAsListOfInt64OrNull(key) ?: List()
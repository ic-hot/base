@file:Suppress("NOTHING_TO_INLINE", "FoldInitializerAndIfToElvis")


package ic.android.util.bundle


import java.math.BigDecimal
import android.os.Bundle


inline fun Bundle.getAsBigDecimalOrNull (key: String) : BigDecimal? {

	val string = getString(key)

	if (string == null) return null

	return BigDecimal(string)

}

inline fun Bundle.getAsBigDecimal (key: String) = getAsBigDecimalOrNull(key)!!
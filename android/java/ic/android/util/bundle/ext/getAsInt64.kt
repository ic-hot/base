@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle


inline fun Bundle.getAsInt64OrNull (key: String) : Long? {
	return if (containsKey(key)) {
		getLong(key)
	} else {
		null
	}
}


inline fun Bundle.getAsInt64 (key: String) = getAsInt64OrNull(key)!!
package ic.android.util.bundle.ext


import android.os.Bundle


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsByteArrayOrNull (key: String) : ByteArray? {
	return getByteArray(key)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsByteArray (key: String) = getAsByteArrayOrNull(key)!!
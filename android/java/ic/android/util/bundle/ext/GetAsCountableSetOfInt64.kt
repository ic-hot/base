package ic.android.util.bundle.ext


import android.os.Bundle

import ic.base.arrays.ext.toFiniteSet
import ic.base.primitives.int64.Int64
import ic.struct.set.finite.FiniteSet


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsFiniteSetOfInt64OrNull (key: String) : FiniteSet<Int64>? {

	return getLongArray(key)?.toFiniteSet()

}


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsFiniteSetOfInt64 (key: String) : FiniteSet<Int64> {
	return getAsFiniteSetOfInt64OrNull(key) ?: FiniteSet()
}
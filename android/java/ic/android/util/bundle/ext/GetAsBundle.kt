@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle


import android.os.Bundle
import android.os.Parcelable


inline fun <Type: Parcelable> Bundle.getAsParcelableOrNull (key: String) : Type? {
	@Suppress("DEPRECATION")
	return getParcelable(key)
}

inline fun <Type: Parcelable> Bundle.getAsParcelable (key: String) : Type {
	return getAsParcelableOrNull(key)!!
}
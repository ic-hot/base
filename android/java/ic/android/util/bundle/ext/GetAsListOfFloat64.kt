package ic.android.util.bundle.ext


import android.os.Bundle

import ic.base.primitives.float64.Float64

import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfFloat64OrNull (key: String) : List<Float64>? {

	return getDoubleArray(key)?.let { ListFromArray(it, isArrayImmutable = true) }

}


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfFloat64 (key: String) = getAsListOfFloat64OrNull(key) ?: List()
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import ic.base.primitives.float64.Float64

import android.os.Bundle


inline fun Bundle.getAsFloat64OrNull (key: String) : Float64? {
	return if (containsKey(key)) {
		getDouble(key)
	} else {
		null
	}
}


inline fun Bundle.getAsFloat64 (key: String) : Float64 = getAsFloat64OrNull(key)!!
package ic.android.util.bundle.ext


import java.math.BigDecimal

import android.os.Bundle
import android.os.Parcelable

import ic.android.util.bundle.serial.bundleSerializers
import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.float64.Float64
import ic.base.throwables.NotExistsException
import ic.funs.effector.find.all
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.toArray
import ic.struct.list.ext.foreach.forEach


operator fun Bundle.set (key: String, value: Any?) {

	var isBundleSerializersSuccess : Boolean = false

	skippable {
		bundleSerializers.forEach tryingBundleSerializer@{ bundleSerializer ->
			try {
				bundleSerializer.serializeToBundle(this, key, value)
			} catch (t: NotExistsException) {
				return@tryingBundleSerializer
			}
			isBundleSerializersSuccess = true
			skip
		}
	}

	if (isBundleSerializersSuccess) return

	when {

		value == null 		-> remove(key)
		value is Int32 		-> putInt(key, value)
		value is Int64 		-> putLong(key, value)
		value is Float32	-> putFloat(key, value)
		value is Float64	-> putDouble(key, value)
		value is Boolean 	-> putBoolean(key, value)
		value is String 	-> putString(key, value)

		value is Bundle 	-> putBundle(key, value)

		value is Parcelable -> putParcelable(key, value)

		value is BigDecimal	-> putString(key, value.toString())

		value is ByteArray  -> putByteArray(key, value)
		value is LongArray	-> putLongArray(key, value)

		value is Array<*> -> {
			if (value.all { it is Parcelable }) {
				@Suppress("UNCHECKED_CAST")
				putParcelableArray(key, value as Array<out Parcelable>)
			}
		}

		value is Collection<*> -> {

			when {
				value.all { it is Parcelable } -> {
					@Suppress("UNCHECKED_CAST")
					val iterableOfParcelables = value as Collection<Parcelable>
					putParcelableArray(key, iterableOfParcelables.toArray())
				}
				value.all { it is Int64 } -> {
					@Suppress("UNCHECKED_CAST")
					val iterableOfInt64 = value as Collection<Int64>
					putLongArray(key, iterableOfInt64.toArray())
				}
				value.all { it is Float32 } -> {
					@Suppress("UNCHECKED_CAST")
					val iterableOfFloat32 = value as Collection<Float32>
					putFloatArray(key, iterableOfFloat32.toArray())
				}
				value.all { it is Float64 } -> {
					@Suppress("UNCHECKED_CAST")
					val iterableOfFloat64 = value as Collection<Float64>
					putDoubleArray(key, iterableOfFloat64.toArray())
				}
				value.all { it is String } -> {
					@Suppress("UNCHECKED_CAST")
					val iterableOfStrings = value as Collection<String>
					putStringArray(key, iterableOfStrings.toArray())
				}
				else -> throw RuntimeException("value: $value")
			}

		}

		else -> putString(key, value.toString())

	}

}
package ic.android.util.bundle.ext


import ic.base.arrays.ext.copy.convert.copyConvertToList
import ic.struct.list.List

import android.os.Bundle
import android.os.Parcelable


inline fun
	<reified Item: Parcelable>
	Bundle.getAsListOfParcelablesOrNull (key: String) : List<Item>?
{

	@Suppress("DEPRECATION")
	return getParcelableArray(key)?.copyConvertToList { it as Item }

}


inline fun
	<reified Item: Parcelable>
	Bundle.getAsListOfParcelables (key: String) : List<Item>
{
	return getAsListOfParcelablesOrNull(key)!!
}
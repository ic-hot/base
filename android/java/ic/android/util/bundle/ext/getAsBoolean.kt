@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle


inline fun Bundle.getAsBooleanOrNull (key: String) : Boolean? {
	return if (containsKey(key)) {
		getBoolean(key)
	} else {
		null
	}
}


inline fun Bundle.getAsBoolean (key: String) = getAsBooleanOrNull(key)!!

inline fun Bundle.getAsBoolean (
	key : String,
	or : Boolean
) : Boolean {
	return getAsBooleanOrNull(key) ?: or
}

inline fun Bundle.getAsBoolean (
	key : String,
	or : () -> Boolean
) : Boolean {
	return getAsBooleanOrNull(key) ?: or()
}
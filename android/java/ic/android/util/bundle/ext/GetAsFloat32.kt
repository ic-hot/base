@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import ic.base.primitives.float32.Float32

import android.os.Bundle


inline fun Bundle.getAsFloat32OrNull (key: String) : Float32? {
	return if (containsKey(key)) {
		getFloat(key)
	} else {
		null
	}
}


inline fun Bundle.getAsFloat32 (key: String) : Float32 = getAsFloat32OrNull(key)!!
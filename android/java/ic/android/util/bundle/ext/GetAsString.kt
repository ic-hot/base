@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle


inline fun Bundle.getAsStringOrNull (key: String) : String? = getString(key)

inline fun Bundle.getAsString (key: String) = getAsStringOrNull(key)!!
package ic.android.util.bundle.ext


import android.os.Bundle

import ic.base.primitives.float32.Float32
import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfFloat32OrNull (key: String) : List<Float32>? {

	return getFloatArray(key)?.let { ListFromArray(it, isArrayImmutable = true) }

}


@Suppress("NOTHING_TO_INLINE")
inline fun Bundle.getAsListOfFloat32 (key: String) = getAsListOfFloat32OrNull(key) ?: List()
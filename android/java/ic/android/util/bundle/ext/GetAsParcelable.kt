@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.bundle.ext


import android.os.Bundle


inline fun Bundle.getAsBundleOrNull (key: String) : Bundle? = getBundle(key)

inline fun Bundle.getAsBundle (key: String) = getAsBundleOrNull(key)!!
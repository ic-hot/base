@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.units


import ic.android.system.scaledDensityFactor
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32


inline fun spToPx (sp: Float32) : Int32 = (sp * scaledDensityFactor).asInt32

inline fun spToPx (sp: Int32) : Int32 = spToPx(sp.asFloat32)


@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.units


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32

import ic.android.system.screenDensityFactor


inline fun pxToDp (px: Int32) : Float32 = px / screenDensityFactor

inline fun pxToDp (px: Float32) : Float32 = px / screenDensityFactor
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.units


import ic.android.system.screenDensityFactor
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32


inline fun dpToPx (dp: Float32) : Int32 = (dp * screenDensityFactor).asInt32

inline fun dpToPx (dp: Int32) : Int32 = dpToPx(dp.asFloat32)

inline fun dpToPx (dp: Float64) : Int32 = (dp * screenDensityFactor).asInt32


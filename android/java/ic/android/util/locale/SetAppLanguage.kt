package ic.android.util.locale


import ic.android.app.thisApp
import ic.android.ui.activity.all.recreateAllActivities


fun setAppLanguage (languageCode: String?, silently: Boolean = false) {

	savedAppLanguageCode = languageCode

	globalLocalizedContext = createLocalizedContext(thisApp)

	if (!silently) {

		recreateAllActivities()

	}

}
package ic.android.util.locale


import android.annotation.SuppressLint
import android.content.Context

import ic.util.locale.currentLocale
import ic.util.locale.impl.LocaleFromJavaLocale


@SuppressLint("StaticFieldLeak")
private var globalLocalizedContextField : Context? = null


var globalLocalizedContext : Context

	get() = globalLocalizedContextField!!

	set(value) {
		globalLocalizedContextField = value
		currentLocale = LocaleFromJavaLocale(
			@Suppress("DEPRECATION")
			value.resources.configuration.locale
		)
	}

;
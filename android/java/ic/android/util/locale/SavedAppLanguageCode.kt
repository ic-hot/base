package ic.android.util.locale


import ic.storage.prefs.Prefs


private val prefs = Prefs("ic.android.util.locale")


internal var savedAppLanguageCode : String?

	get() = prefs.getStringOrNull("appLanguageCode")

	set(value) = prefs.set("appLanguageCode", value)

;
package ic.android.util.locale


import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics


fun createLocalizedContext (sourceContext: Context) : Context {

	val resources = sourceContext.resources

	@Suppress("DEPRECATION")
	val sourceLanguageCode = resources.configuration.locale.language

	@Suppress("DEPRECATION")
	val systemLanguageCode = Resources.getSystem().configuration.locale.language

	val targetLanguageCode = savedAppLanguageCode ?: systemLanguageCode

	if (targetLanguageCode == sourceLanguageCode) return sourceContext

	val javaLocale = java.util.Locale(targetLanguageCode)

	val configuration : Configuration = resources.configuration

	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

		val displayMetrics : DisplayMetrics = resources.displayMetrics

		@Suppress("DEPRECATION")
		configuration.locale = javaLocale

		@Suppress("DEPRECATION")
		resources.updateConfiguration(configuration, displayMetrics)

		return sourceContext

	} else {

		configuration.setLocale(javaLocale)

		return sourceContext.createConfigurationContext(configuration)

	}

}
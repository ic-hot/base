@file:Suppress("DEPRECATION")

package ic.android.util.geo


import ic.android.app.thisApp
import ic.base.primitives.float32.Float32
import ic.design.task.Task
import ic.design.task.scope.TaskScope
import ic.util.geo.Location
import ic.util.time.ext.compareTo
import ic.util.time.funs.now
import ic.util.time.plus

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import ic.util.geo.lastKnownLocation
import ic.util.geo.lastKnownLocationTime


@SuppressLint("MissingPermission")


/**
 * @return null if can't start listen
 */
@Deprecated("Use Activity.startListenLocation()")
inline fun implementListenLocation (

	accuracyM : Float32 = Float32(256),

	crossinline onLocationUpdate : (Location) -> Unit

) : Task? {

	if (now < lastKnownLocationTime + 1000L * 60 * 4) {
		onLocationUpdate(lastKnownLocation)
	}

	val locationManager = thisApp.getSystemService(Context.LOCATION_SERVICE) as LocationManager

	val locationListener = object : LocationListener {
		override fun onLocationChanged (androidLocation: android.location.Location) {
			val location = Location(
				latitude 	= androidLocation.latitude,
				longitude 	= androidLocation.longitude
			)
			lastKnownLocation = location
			onLocationUpdate(location)
		}
		@Deprecated("Deprecated in Java")
		override fun onStatusChanged (provider: String, status: Int, extras: Bundle?) {}
		override fun onProviderEnabled (provider: String) {}
		override fun onProviderDisabled (provider: String) {}
	}

	try {

		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1024L, accuracyM, locationListener)
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 	 1024L, accuracyM, locationListener)

		return Task {
			locationManager.removeUpdates(locationListener)
		}

	} catch (e: Throwable) {

		return null

	}

}


@Deprecated("Use Activity.startListenLocation()")
inline fun listenLocation (
	accuracyM : Float32 = Float32(256),
	crossinline onLocationUpdate : (Location) -> Unit
) : Task? {
	return implementListenLocation(accuracyM = accuracyM, onLocationUpdate = onLocationUpdate)
}


@Deprecated("Use Activity.startListenLocation()")
inline fun TaskScope.listenLocation (
	accuracyM : Float32 = Float32(256),
	crossinline onLocationUpdate : (Location) -> Unit
) {
	implementListenLocation(accuracyM = accuracyM) { location ->
		if (isOpen) {
			onLocationUpdate(location)
		}
	}?.let { notifyTaskStarted(it) }
}
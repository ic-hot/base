@file:Suppress("DEPRECATION")


package ic.android.util.geo


import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

import ic.android.app.thisApp
import ic.android.util.handler.postDelayed
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.base.throwables.AccessException
import ic.design.task.Task
import ic.design.task.scope.TaskScope
import ic.ifaces.cancelable.Cancelable
import ic.util.geo.Location
import ic.util.geo.lastKnownLocation
import ic.util.geo.lastKnownLocationTime
import ic.util.time.ext.compareTo
import ic.util.time.funs.now
import ic.util.time.plus


@SuppressLint("MissingPermission")


@Deprecated("Use Activity.startListenLocation()")
abstract class GetLocationTask : Task {


	protected abstract fun onFinish()

	protected abstract fun onTimeout()

	protected abstract fun onSuccess (location: Location)


	private val locationManager = (
		thisApp.getSystemService(Context.LOCATION_SERVICE) as LocationManager
	)

	private var locationListener : LocationListener? = object : LocationListener {

		override fun onLocationChanged (androidLocation: android.location.Location) {
			val location = Location(
				latitude 	= androidLocation.latitude,
				longitude 	= androidLocation.longitude
			)
			lastKnownLocation = location
			cancel()
			onFinish()
			onSuccess(location)
		}

		@Deprecated("Deprecated in Java")
		override fun onStatusChanged (provider: String, status: Int, extras: Bundle?) {}
		override fun onProviderEnabled (provider: String) {}
		override fun onProviderDisabled (provider: String) {}

	}

	private var timeoutWaiter : Cancelable? = null


	override fun cancel() {
		if (locationListener != null) {
			locationManager.removeUpdates(locationListener!!)
			locationListener = null
		}
		timeoutWaiter?.cancel()
		timeoutWaiter = null
	}


	@Throws(AccessException::class)
	constructor (accuracyM: Float32, timeoutMs: Int64) {

		try {

			locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 1024L, accuracyM, locationListener!!
			)

			locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 1024L, accuracyM, locationListener!!
			)

		} catch (e: SecurityException) { throw AccessException }

		timeoutWaiter = postDelayed(timeoutMs) {
			cancel()
			onFinish()
			onTimeout()
		}

	}


}


@Deprecated("Use Activity.startListenLocation()")
inline fun implementGetCurrentLocation (

	accuracyM : Float32 = Float32(256),
	timeoutMs : Int64 = Int64(1000) * 16,

	crossinline onFinish : () -> Unit,

	onPermissionDenied : () -> Unit,
	crossinline onTimeout : () -> Unit,

	crossinline onSuccess : (Location) -> Unit

) : Task? {

	if (now < lastKnownLocationTime + 1000L * 60 * 4) {

		onFinish()
		onSuccess(lastKnownLocation)
		return null

	}

	try {

		return object : GetLocationTask(
			accuracyM = accuracyM,
			timeoutMs = timeoutMs
		) {

			override fun onFinish() = onFinish()

			override fun onTimeout() = onTimeout()

			override fun onSuccess (location: Location) = onSuccess(location)

		}

	} catch (t: AccessException) {

		onFinish()
		onPermissionDenied()
		return null

	}

}


@Deprecated("Use Activity.startListenLocation()")
inline fun getCurrentLocation (

	accuracyM : Float32 = Float32(256),
	timeoutMs : Int64 = Int64(1000) * 16,

	crossinline onFinish : () -> Unit,

	onPermissionDenied : () -> Unit,
	crossinline onTimeout : () -> Unit,

	crossinline onSuccess : (Location) -> Unit

) : Task? {

	return implementGetCurrentLocation(
		accuracyM = accuracyM,
		timeoutMs = timeoutMs,
		onFinish = onFinish,
		onPermissionDenied = onPermissionDenied,
		onTimeout = onTimeout,
		onSuccess = onSuccess
	)

}


@Deprecated("Use Activity.startListenLocation()")
inline fun TaskScope.getCurrentLocation (

	accuracyM : Float32 = Float32(256),
	timeoutMs : Int64 = Int64(1000) * 16,

	crossinline onFinish : () -> Unit = {},

	onPermissionDenied : () -> Unit,
	crossinline onTimeout : () -> Unit,

	crossinline onSuccess : (Location) -> Unit

) {

	var task : Task? = null

	task = implementGetCurrentLocation(
		accuracyM = accuracyM,
		timeoutMs = timeoutMs,
		onFinish = {
			task?.let { notifyTaskFinished(it) }
			task = null
			onFinish()
		},
		onPermissionDenied = onPermissionDenied,
		onTimeout = onTimeout,
		onSuccess = onSuccess
	)

	task?.let { notifyTaskStarted(it) }

}

package ic.android.util.geo


import ic.ifaces.cancelable.Cancelable

import android.annotation.SuppressLint
import android.content.Context.SENSOR_SERVICE
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

import ic.android.app.thisApp


@SuppressLint("MissingPermission")
fun listenOrientation (

	onOrientationUpdate : (Float) -> Unit

) : Cancelable {

	val sensorManager = thisApp.getSystemService(SENSOR_SERVICE) as SensorManager

	val gSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
	val mSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

	val sensorEventListener = object : SensorEventListener {
		override fun onAccuracyChanged (sensor: Sensor, accuracy: Int) {}
		override fun onSensorChanged (event: SensorEvent) {
			val alpha = 0.97F
			val mGravity = FloatArray(3)
			if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
				mGravity[0] = alpha * mGravity[0] + (1 - alpha) * event.values[0]
				mGravity[1] = alpha * mGravity[1] + (1 - alpha) * event.values[1]
				mGravity[2] = alpha * mGravity[2] + (1 - alpha) * event.values[2]
			}
			val mGeomagnetic = FloatArray(3)
			if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
				mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha) * event.values[0]
				mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha) * event.values[1]
				mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha) * event.values[2]
			}
			val R = FloatArray(9)
			val I = FloatArray(9)
			val success = SensorManager.getRotationMatrix(
				R, I, mGravity,
				mGeomagnetic
			)
			if (success) {
				val orientation = FloatArray(3)
				SensorManager.getOrientation(R, orientation)
				var azimuth = Math.toDegrees(orientation[0].toDouble()).toFloat()
				azimuth = (azimuth + 360) % 360
				onOrientationUpdate(azimuth)
			}
		}
	}

	sensorManager.registerListener(sensorEventListener, gSensor, SensorManager.SENSOR_DELAY_NORMAL)
	sensorManager.registerListener(sensorEventListener, mSensor, SensorManager.SENSOR_DELAY_NORMAL)

	return object : Cancelable {
		override fun cancel() {
			sensorManager.unregisterListener(sensorEventListener)
		}
	}

}
package ic.android.util.geo.ext


import ic.util.geo.Location


@Suppress("NOTHING_TO_INLINE")
inline fun android.location.Location.toLocation() = Location(
	latitude = latitude,
	longitude = longitude
)
package ic.android.util.geo.ext


import android.os.Bundle

import ic.android.util.bundle.Bundle
import ic.android.util.bundle.ext.getAsFloat64
import ic.util.geo.Location


fun Location.toBundle() = Bundle(
	"latitude"  to latitude,
	"longitude" to longitude
)

fun Location.Companion.fromBundle (bundle: Bundle) = Location(
	latitude  = bundle.getAsFloat64("latitude"),
	longitude = bundle.getAsFloat64("longitude")
)
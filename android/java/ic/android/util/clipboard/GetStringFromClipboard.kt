package ic.android.util.clipboard


import android.content.ClipboardManager
import android.content.Context

import ic.android.app.thisApp


fun getStringFromClipboard() : String? {

	val clipboard = thisApp.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
	return clipboard.primaryClip?.getItemAt(0)?.text?.toString()

}
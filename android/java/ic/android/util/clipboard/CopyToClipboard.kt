package ic.android.util.clipboard


import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context

import ic.android.app.thisApp


fun copyToClipboard (text: String) {

	val clipboard = thisApp.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
	val clip = ClipData.newPlainText("text", text)
	clipboard.setPrimaryClip(clip)

}
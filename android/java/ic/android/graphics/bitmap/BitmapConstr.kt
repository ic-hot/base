package ic.android.graphics.bitmap


import android.graphics.Bitmap


@Suppress("NOTHING_TO_INLINE")
inline fun Bitmap (width: Int, height: Int) : Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.graphics.bitmap


import android.graphics.Bitmap


inline fun Bitmap.copyCropSymmetrically (edgeSize: Int) : Bitmap {
	return copyCrop(
		left = edgeSize,
		top = edgeSize,
		newWidth = width - edgeSize * 2,
		newHeight = height - edgeSize * 2
	)
}
@file:Suppress("NOTHING_TO_INLINE")


package ic.android.graphics.bitmap


import android.graphics.Bitmap


fun Bitmap.copyCrop (left: Int, top: Int, newWidth: Int, newHeight: Int) : Bitmap {
	return Bitmap.createBitmap(this, left, top, newWidth, newHeight)
}
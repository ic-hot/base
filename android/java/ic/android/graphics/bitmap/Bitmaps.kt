@file:Suppress("NOTHING_TO_INLINE")


package ic.android.graphics.bitmap


import java.io.ByteArrayOutputStream

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import ic.base.throwables.UnableToParseException

import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray


// To png:

fun pngToBitmapOrThrowUnableToParse (byteArray: ByteArray) : Bitmap {
	val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
	if (bitmap == null) throw UnableToParseException()
	return bitmap
}

inline fun pngToBitmap (byteArray: ByteArray) : Bitmap {
	try {
		return pngToBitmapOrThrowUnableToParse(byteArray)
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime(t)
	}
}

inline fun pngToBitmap (byteSequence: ByteSequence) = pngToBitmap(byteSequence.toByteArray())

inline fun pngToBitmapOrThrowUnableToParse (byteSequence: ByteSequence) : Bitmap {
	return pngToBitmapOrThrowUnableToParse(byteSequence.toByteArray())
}

inline fun pngToBitmapOrNull (byteSequence: ByteSequence) : Bitmap? {
	try {
		return pngToBitmapOrThrowUnableToParse(byteSequence.toByteArray())
	} catch (t: UnableToParseException) {
		return null
	}
}

inline fun nullablePngToBitmap (png: ByteSequence?) : Bitmap? {
	if (png == null) return null
	return pngToBitmap(png)
}

inline fun nullablePngToBitmapOrNull (png: ByteSequence?) : Bitmap? {
	if (png == null) return null
	try {
		return pngToBitmapOrThrowUnableToParse(png.toByteArray())
	} catch (t: UnableToParseException) {
		return null
	}
}
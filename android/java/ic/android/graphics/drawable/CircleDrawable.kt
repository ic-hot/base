@file:Suppress("FunctionName")


package ic.android.ui.drawable


import ic.base.primitives.int32.Int32

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape


fun CircleDrawable (

	colorArgb : Int32 = Int32(0xffffffff)

) : ShapeDrawable {

	return ShapeDrawable(
		OvalShape()
	).apply {
		paint.color = colorArgb
	}

}
@file:Suppress("FunctionName")


package ic.android.ui.drawable


import ic.base.primitives.int32.Int32

import android.graphics.Paint
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import ic.base.primitives.int32.ext.asFloat32


fun RingDrawable (

	thicknessPx : Int32,

	colorArgb : Int32 = Int32(0xffffffff)

) : ShapeDrawable {
	return ShapeDrawable(
		OvalShape()
	).apply {
		paint.color = colorArgb
		paint.strokeWidth = thicknessPx.asFloat32
		paint.style = Paint.Style.STROKE
	}
}
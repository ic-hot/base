@file:Suppress("FunctionName")


package ic.android.ui.drawable


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape


fun RoundCornersRectDrawable (

	colorArgb : Int32 = Int32(0xffffffff),

	leftTopCornerRadiusPx 		: Float32 = Float32(0),
	rightTopCornerRadiusPx 		: Float32 = Float32(0),
	leftBottomCornerRadiusPx 	: Float32 = Float32(0),
	rightBottomCornerRadiusPx 	: Float32 = Float32(0)

) : ShapeDrawable {

	return ShapeDrawable(

		RoundRectShape(
			floatArrayOf(
				leftTopCornerRadiusPx,
				leftTopCornerRadiusPx,
				rightTopCornerRadiusPx,
				rightTopCornerRadiusPx,
				rightBottomCornerRadiusPx,
				rightBottomCornerRadiusPx,
				leftBottomCornerRadiusPx,
				leftBottomCornerRadiusPx
			),
			null, null
		)

	).apply {

		paint.color = colorArgb

	}

}
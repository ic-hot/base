package ic.android.storage.prefs


import android.content.Context

import ic.android.app.thisApp


fun getPrefBoolean (prefsName : String, prefKey : String, defaultValue : Boolean = false) : Boolean {

	val sharedPreferences = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

	val value = sharedPreferences.getBoolean(prefKey, defaultValue)

	return value

}
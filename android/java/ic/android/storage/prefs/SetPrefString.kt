package ic.android.storage.prefs


import android.content.Context

import ic.android.app.thisApp


@Deprecated("Use Prefs(prefsName)[prefKey]")
fun getPrefStringOrNull (prefsName : String, prefKey : String) : String? {
	val sharedPreferences = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
	return sharedPreferences.getString(prefKey, null)
}

@Deprecated("Use Prefs(prefName)[prefKey] = value")
fun setPrefString (prefsName: String, prefKey : String, value : String?) {
	val sharedPreferencesEditor = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit()
	sharedPreferencesEditor.putString(prefKey, value)
	sharedPreferencesEditor.commit()
}
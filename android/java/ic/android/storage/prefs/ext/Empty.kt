package ic.android.storage.prefs.ext


import ic.android.storage.prefs.Prefs


fun Prefs.empty() {

	synchronized(this) {
		sharedPreferencesEditor.clear()
		sharedPreferencesEditor.commit()
	}

}
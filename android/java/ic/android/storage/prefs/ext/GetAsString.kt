package ic.android.storage.prefs.ext


import ic.android.storage.prefs.Prefs


fun Prefs.getAsStringOrNull (name: String) : String? {

	synchronized(this) {
		return sharedPreferences.getString(name, null)
	}

}
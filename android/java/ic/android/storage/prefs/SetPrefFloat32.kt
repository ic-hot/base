package ic.android.storage.prefs


import ic.base.primitives.float32.Float32
import ic.util.log.logD

import android.annotation.SuppressLint
import android.content.Context

import ic.android.app.thisApp


@SuppressLint("ApplySharedPref")
fun setPrefFloat32 (prefsName: String, prefKey: String, value: Float32) {

	logD("prefs.$prefsName") { "setPrefBoolean prefKey: $prefKey value: $value" }

	val sharedPreferencesEditor = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit()

	sharedPreferencesEditor.putFloat(prefKey, value)

	sharedPreferencesEditor.commit()

}
package ic.android.storage.prefs


import android.annotation.SuppressLint
import android.content.Context

import ic.android.app.thisApp


@SuppressLint("ApplySharedPref")
fun setPrefBoolean (prefsName: String, prefKey: String, value: Boolean) {

	val sharedPreferencesEditor = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit()

	sharedPreferencesEditor.putBoolean(prefKey, value)

	sharedPreferencesEditor.commit()

}
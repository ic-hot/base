package ic.android.storage.prefs


import ic.base.primitives.int64.Int64
import ic.util.log.logD

import android.annotation.SuppressLint
import android.content.Context

import ic.android.app.thisApp


@SuppressLint("ApplySharedPref")
fun setPrefInt64 (prefsName: String, prefKey: String, value: Int64) {

	logD("prefs.$prefsName") { "setPrefBoolean prefKey: $prefKey value: $value" }

	val sharedPreferencesEditor = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE).edit()

	sharedPreferencesEditor.putLong(prefKey, value)

	sharedPreferencesEditor.commit()

}
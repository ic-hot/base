package ic.android.storage.prefs


import ic.base.primitives.float32.Float32

import android.content.Context

import ic.android.app.thisApp
import ic.util.log.logD


fun getPrefFloat32Or (
	prefsName : String,
	prefKey : String,
	defaultValue : Float32
) : Float32 {

	val sharedPreferences = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

	val value = try {
		sharedPreferences.getFloat(prefKey, defaultValue)
	} catch (t: ClassCastException) {
		defaultValue
	}

	logD("prefs.$prefsName") {
		"getPrefFloat32Or prefKey: $prefKey defaultValue: $defaultValue, value: $value"
	}

	return value

}
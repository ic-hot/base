@file:Suppress("NOTHING_TO_INLINE")


package ic.android.storage.prefs


inline fun Prefs (name: String) : Prefs {

	return object : Prefs() {

		override val name get() = name

	}

}
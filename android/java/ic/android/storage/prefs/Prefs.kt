package ic.android.storage.prefs


import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

import ic.android.app.thisApp


@SuppressLint("CommitPrefEdits")
abstract class Prefs {


	protected abstract val name : String


	private var sharedPreferencesField : SharedPreferences? = null

	internal val sharedPreferences : SharedPreferences get() {
		synchronized(this) {
			if (sharedPreferencesField == null) {
				sharedPreferencesField = thisApp.getSharedPreferences(name, Context.MODE_PRIVATE)
			}
			return sharedPreferencesField!!
		}
	}


	private var sharedPreferencesEditorField : SharedPreferences.Editor? = null

	internal val sharedPreferencesEditor : SharedPreferences.Editor get() {
		synchronized(this) {
			if (sharedPreferencesEditorField == null) {
				sharedPreferencesEditorField = sharedPreferences.edit()
			}
			return sharedPreferencesEditorField!!
		}
	}


}
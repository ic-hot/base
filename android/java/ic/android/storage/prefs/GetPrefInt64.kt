package ic.android.storage.prefs


import android.content.Context

import ic.android.app.thisApp
import ic.base.primitives.int64.Int64
import ic.util.log.logD


fun getPrefInt64Or (
	prefsName : String,
	prefKey : String,
	defaultValue : Int64
) : Int64 {

	val sharedPreferences = thisApp.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

	val value = sharedPreferences.getLong(prefKey, defaultValue)

	logD("prefs.$prefsName") {
		"getPrefInt64Or prefKey: $prefKey defaultValue: $defaultValue, value: $value"
	}

	return value

}
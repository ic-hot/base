@file:Suppress("NOTHING_TO_INLINE")


package ic.android.storage.prefs


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32


inline fun setPrefFloat64 (prefsName: String, prefKey: String, value: Float64) {

	setPrefFloat32(
		prefsName = prefsName,
		prefKey = prefKey,
		value = value.asFloat32
	)

}
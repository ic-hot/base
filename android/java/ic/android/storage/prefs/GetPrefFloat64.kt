@file:Suppress("NOTHING_TO_INLINE")


package ic.android.storage.prefs


import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32


inline fun getPrefFloat64Or (
	prefsName : String, prefKey : String, defaultValue : Float64
) : Float64 {

	return getPrefFloat32Or(
		prefsName = prefsName,
		prefKey = prefKey,
		defaultValue = defaultValue.asFloat32
	).asFloat64

}
package ic.android.storage.res


import android.graphics.Bitmap
import android.graphics.BitmapFactory

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32
import ic.base.throwables.NotExistsException
import ic.util.cache.Cache
import ic.util.cache.RamCache


private val bitmapsByResId : Cache<Int32, Bitmap> = RamCache()

fun getResBitmap (resId: Int32) = bitmapsByResId.getOr(resId) {
	BitmapFactory.decodeResource(thisApp.resources, resId)
}


private val bitmapsByName : Cache<String, Bitmap> = RamCache()

@Throws(NotExistsException::class)
fun getResBitmapOrThrow (resName: String) : Bitmap {
	return bitmapsByName.getOr(resName) {
		try {
			getResBitmap(
				getResIdOrThrowNotExists("drawable", resName)
			)
		} catch (_: Exception) {
			throw NotExistsException
		}
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun getResBitmapOrNull (resName: String) : Bitmap? {
	try {
		return getResBitmapOrThrow(resName)
	} catch (_: NotExistsException) {
		return null
	}
}
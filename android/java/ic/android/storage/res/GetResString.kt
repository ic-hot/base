@file:Suppress("NOTHING_TO_INLINE")


package ic.android.storage.res


import ic.base.strings.ext.replaceAll

import ic.android.util.locale.globalLocalizedContext
import ic.base.throwables.NotExistsException


fun getResString (resId : Int) = globalLocalizedContext.resources.getString(resId)

@Throws(NotExistsException::class)
fun getResStringOrThrowNotExists (resName : String) : String {
	return getResString(
		getResIdOrThrowNotExists("string", resName)
	)
}

inline fun getResString (resName: String) : String {
	try {
		return getResStringOrThrowNotExists(resName)
	} catch (_: NotExistsException) {
		throw RuntimeException()
	}
}


fun getResString (resId : Int, vararg replace : Pair<String, String>) : String {
	return getResString(resId).replaceAll(*replace)
}

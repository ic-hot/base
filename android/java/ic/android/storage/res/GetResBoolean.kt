@file:Suppress("NOTHING_TO_INLINE")


package ic.android.util.res


import ic.android.app.thisApp


inline fun getResBoolean (resId : Int) : Boolean = thisApp.resources.getBoolean(resId)
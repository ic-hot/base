package ic.android.storage.res


import android.content.Context
import android.graphics.drawable.Drawable

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32


@Suppress("DEPRECATION")
fun Context.getResDrawable (resId : Int32): Drawable = resources.getDrawable(resId)


fun getResDrawable (resId : Int32): Drawable = thisApp.getResDrawable(resId)



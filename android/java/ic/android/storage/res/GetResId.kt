package ic.android.storage.res


import android.annotation.SuppressLint

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32
import ic.base.throwables.NotExistsException


@SuppressLint("DiscouragedApi")
@Throws(NotExistsException::class)
fun getResIdOrThrowNotExists (type: String, resName: String) : Int32 {
	try {
		val resId = thisApp.resources.getIdentifier(resName, type, thisApp.packageName)
		if (resId == 0) throw NotExistsException
		return resId
	} catch (_: Exception) {
		throw NotExistsException
	}
}
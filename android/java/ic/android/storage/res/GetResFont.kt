package ic.android.storage.res


import android.graphics.Typeface
import android.os.Build.VERSION_CODES
import android.os.Build.VERSION.SDK_INT

import ic.android.app.thisApp
import ic.base.primitives.int32.Int32
import ic.base.throwables.NotExistsException


@Suppress("NOTHING_TO_INLINE")
inline fun getResFontOrNull (fontResId: Int32) : Typeface? {
	if (SDK_INT < VERSION_CODES.O) return null
	return thisApp.resources.getFont(fontResId)
}


@Suppress("NOTHING_TO_INLINE")
inline fun getResFont (fontResId: Int32) = getResFontOrNull(fontResId)!!


@Suppress("NOTHING_TO_INLINE")
inline fun getResFontOrNull (fontResName: String) : Typeface? {
	try {
		val resId = getResIdOrThrowNotExists("font", fontResName)
		return getResFontOrNull(resId)
	} catch (_: NotExistsException) {
		return null
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun getResFont (fontResName: String) = getResFontOrNull(fontResName)!!
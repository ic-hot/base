package ic.android.storage.res.attrs


import android.content.Context
import android.util.TypedValue

import ic.base.primitives.int32.Int32


fun Context.getAttrResId (attrId : Int32) : Int32 {
	val typedValue = TypedValue()
	theme.resolveAttribute(attrId, typedValue, true)
	return typedValue.resourceId
}
package ic.android.storage.assets


import android.graphics.Typeface

import ic.android.app.thisApp
import ic.base.strings.ext.endsWith
import ic.util.cache.RamCache


private val cache = RamCache<String, Typeface?>()


private fun implGetFontFromAssetsOrNull (name: String) : Typeface? {
	val assets = thisApp.assets
	when {
		name.endsWith(".ttf", ignoreCase = true) -> {
			return Typeface.createFromAsset(assets, "fonts/$name")
		}
		name.endsWith(".otf", ignoreCase = true) -> {
			return Typeface.createFromAsset(assets, "fonts/$name")
		}
		else -> {
			try {
				return Typeface.createFromAsset(assets, "fonts/$name.ttf")
			} catch (_: Exception) {}
			try {
				return Typeface.createFromAsset(assets, "fonts/$name.otf")
			} catch (_: Exception) {}
			return null
		}
	}
}


fun getFontFromAssetsOrNull (name: String) : Typeface? {
	return cache.getOr(key = name) {
		implGetFontFromAssetsOrNull(name)
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun getFontFromAssets (name: String) = getFontFromAssetsOrNull(name)!!
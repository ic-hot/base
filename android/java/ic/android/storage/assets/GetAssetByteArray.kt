package ic.android.storage.assets


import ic.base.throwables.NotExistsException
import ic.stream.sequence.ext.toByteArray

import kotlin.jvm.Throws


@Throws(NotExistsException::class)
fun getAssetByteArrayOrThrow (assetFilePath: String) : ByteArray {

	val byteSequence = getAssetFileOrThrow(assetFilePath)

	return byteSequence.toByteArray()

}
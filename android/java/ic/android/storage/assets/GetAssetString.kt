@file:Suppress("NOTHING_TO_INLINE")


package ic.android.storage.assets


import ic.base.throwables.NotExistsException
import ic.stream.sequence.ext.toString
import ic.util.text.charset.Charset
import ic.util.text.charset.Charset.Companion.Utf8


@Throws(NotExistsException::class)
inline fun getAssetStringOrThrow (assetFilePath: String, charset: Charset = Utf8) : String {
	return getAssetFileOrThrow("$assetFilePath.txt").toString(charset)
}


inline fun getAssetStringOrNull (assetFilePath: String, charset: Charset = Utf8) : String? {
	try {
		return getAssetStringOrThrow(assetFilePath, charset = charset)
	} catch (_: NotExistsException) {
		return null
	}
}



inline fun getAssetString (assetFilePath: String, charset: Charset = Utf8) : String {
	try {
		return getAssetStringOrThrow(assetFilePath, charset = charset)
	} catch (_: NotExistsException) {
		throw RuntimeException()
	}
}
package ic.android.storage.assets


import java.io.IOException

import ic.base.jstream.ext.readToByteSequence
import ic.base.throwables.NotExistsException
import ic.stream.sequence.ByteSequence
import ic.android.app.thisApp


@Throws(NotExistsException::class)
fun getAssetFileOrThrow (assetFilePath: String) : ByteSequence {

	val inputStream = try {
		thisApp.assets.open(assetFilePath)
	} catch (t: IOException) {
		throw NotExistsException
	}

	return inputStream.readToByteSequence()

}
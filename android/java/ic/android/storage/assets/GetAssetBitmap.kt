package ic.android.storage.assets


import java.io.IOException

import android.graphics.Bitmap
import android.graphics.BitmapFactory

import ic.android.app.thisApp


fun getAssetBitmapOrNull (assetFilePath: String) : Bitmap? {

	val inputStream = try {
		thisApp.assets.open(assetFilePath)
	} catch (t: IOException) {
		return null
	}

	return BitmapFactory.decodeStream(inputStream)

}
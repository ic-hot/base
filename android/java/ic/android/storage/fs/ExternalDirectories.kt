package ic.android.storage.fs


import android.os.Environment
import android.os.Environment.DIRECTORY_DOWNLOADS

import ic.android.app.thisApp
import ic.storage.fs.Directory
import ic.storage.fs.local.impl.JvmDirectory


val externalFilesDirectory : Directory
	get() {
	val externalFilesDir = thisApp.getExternalFilesDir(null)!!
	return JvmDirectory(
		javaFile = externalFilesDir.parentFile!!.parentFile!!.parentFile!!.parentFile!!
	)
}

val downloadDirectory : Directory get() {
	return JvmDirectory(
		javaFile = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS)
	)
}
package ic.android.app.ext


import java.util.*

import android.app.Application
import android.content.pm.PackageManager

import ic.util.log.logE


val Application.installTime get() = Date(
	try {
		@Suppress("DEPRECATION")
		packageManager.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS).firstInstallTime
	} catch (nameNotFoundException : PackageManager.NameNotFoundException) {
		logE("Application.installTime") { nameNotFoundException }
		0L
	}
)
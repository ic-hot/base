package ic.android.app.ext


import android.app.Application

import ic.util.log.logE


val Application.versionName : String get() {
	try {
		return packageManager.getPackageInfo(packageName, 0).versionName!!
	} catch (t: Throwable) {
		logE("ic.android.app.ext") { t }
		return ""
	}
}
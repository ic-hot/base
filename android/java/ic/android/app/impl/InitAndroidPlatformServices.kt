package ic.android.app.impl


import ic.android.app.BaseApplication
import ic.android.ms.AndroidPlatformServices
import ic.android.ms.androidPlatformServices
import ic.base.reflect.ext.getStaticFieldValue
import ic.base.reflect.getClassByNameOrNull


internal fun BaseApplication.initAndroidPlatformServices() {

	val platformServicesClass = getClassByNameOrNull<AndroidPlatformServices>(
		"ic.android.ms.AndroidPlatformServicesImpl"
	)

	if (platformServicesClass == null) return

	androidPlatformServices = platformServicesClass.getStaticFieldValue("INSTANCE")

	androidPlatformServices.init()

}
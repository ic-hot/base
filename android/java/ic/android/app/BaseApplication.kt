package ic.android.app


import androidx.multidex.MultiDexApplication

import ic.android.app.impl.initAndroidPlatformServices
import ic.android.storage.prefs.getPrefInt64Or
import ic.android.storage.prefs.setPrefInt64
import ic.android.util.locale.createLocalizedContext
import ic.android.util.locale.globalLocalizedContext
import ic.app.tier.Tier
import ic.base.primitives.int64.Int64
import ic.network.http.cookies.enableCookies
import ic.network.http.cookies.AndroidPersistentCookieStore
import ic.struct.map.finite.FiniteMap
import ic.util.geo.Location
import ic.util.geo.defaultLocation
import ic.util.time.Time
import ic.util.time.funs.now


abstract class BaseApplication : MultiDexApplication() {


	protected abstract val tier : Tier?


	protected open fun onLaunch() {}


	protected open fun onMessageReceived (
		from: String?,
		notificationTitle: String?,
		notificationBody: String?,
		data: FiniteMap<String, String>
	) {}

	internal fun notifyMessageReceived (
		from: String?,
		notificationTitle: String?,
		notificationBody: String?,
		data: FiniteMap<String, String>
	) {
		onMessageReceived(
			from = from,
			notificationTitle = notificationTitle,
			notificationBody = notificationBody,
			data = data
		)
	}


	protected open fun onMessagingTokenUpdated (token: String) {}

	internal fun notifyMessagingTokenUpdated (token: String) {
		onMessagingTokenUpdated(token)
	}


	var firstLaunchTime : Time
		get() {
			return Time(getPrefInt64Or("BaseApplication", "firstLaunchTimeMillis", Int64(0)))
		}
		private set(value) {
			setPrefInt64("BaseApplication", "firstLaunchTimeMillis", value.epochMs)
		}
	;


	protected open fun initDefaultLocation() : Location = Location(0.0, 0.0)


	protected open fun beforeFirstLaunch() {}

	protected open fun afterFirstLaunch() {}

	protected open val toEnableCookies : Boolean get() = false


	final override fun onCreate() {

		super.onCreate()

		thisAppOrNull = this

		val isFirstLaunch : Boolean = (
			if (firstLaunchTime.epochMs == 0L) {
				firstLaunchTime = now
				true
			} else {
				false
			}
		)

		tier?.let { ic.app.tier.tier = it }

		if (toEnableCookies) enableCookies(cookieStore = AndroidPersistentCookieStore(this))

		defaultLocation = initDefaultLocation()

		globalLocalizedContext = createLocalizedContext(this)
		
		initAndroidPlatformServices()

		if (isFirstLaunch) {
			beforeFirstLaunch()
		}

		onLaunch()

		if (isFirstLaunch) {
			afterFirstLaunch()
		}

	}


}
package ic.android.app


import kotlin.reflect.KClass

import ic.app.tier.tierMayChange
import ic.base.reflect.ext.getStaticFieldValue


private var buildConfigClass : KClass<*>? = null

fun setAndroidBuildConfigClass (c: KClass<*>?) {
	buildConfigClass = c
	tierMayChange
}

fun <R> getBuildConfigValue (name: String) : R {
	val c = buildConfigClass?.java
	if (c == null) {
		@Suppress("UNCHECKED_CAST")
		return null as R
	}
	return c.getStaticFieldValue(name)
}
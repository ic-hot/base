package ic.design.task.scope.ext


import ic.base.primitives.int64.Int64
import ic.design.task.scope.TaskScope
import ic.ifaces.cancelable.Cancelable
import ic.util.time.duration.Duration


inline fun TaskScope.postDelayed (delayInMs: Int64, crossinline action: () -> Unit) {

	var task : Cancelable? = null

	task = ic.android.util.handler.postDelayed(delayInMs) {

		task?.let { notifyTaskFinished(it) }

		action()

	}

	task?.let { notifyTaskStarted(it) }

}


inline fun TaskScope.postDelayed (delay: Duration, crossinline action: () -> Unit) {

	return postDelayed(delayInMs = delay.inMs, action = action)

}
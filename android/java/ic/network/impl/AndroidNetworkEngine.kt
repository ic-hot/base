package ic.network.impl


import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager

import ic.android.app.thisApp
import ic.design.task.Task
import ic.network.avail.NetworkAvailabilityCallback
import ic.network.avail.NetworkType
import ic.util.log.logD


internal object AndroidNetworkEngine : JvmNetworkEngine() {


	override val networkType : NetworkType?
		@Suppress("DEPRECATION")
		@SuppressLint("MissingPermission")
		get() {
			val connectivityManager = thisApp.getSystemService(CONNECTIVITY_SERVICE)
			connectivityManager ?: return null
			connectivityManager as ConnectivityManager
			val networkInfo = connectivityManager.activeNetworkInfo ?: return null
			return when (networkInfo.type) {
				ConnectivityManager.TYPE_MOBILE -> NetworkType.Mobile
				ConnectivityManager.TYPE_WIFI   -> NetworkType.WiFi
				else                            -> NetworkType.Wired
			}
		}
	;


	@Suppress("DEPRECATION")
	override fun listenNetworkAvailability (
		toCallAtOnce : Boolean,
		callback : NetworkAvailabilityCallback
	) : Task {
		var oldNetworkType = networkType
		if (toCallAtOnce) {
			if (oldNetworkType != null) {
				callback.onConnectionEstablished(oldNetworkType)
			} else {
				callback.onConnectionLost()
			}
		}
		val receiver = object : BroadcastReceiver() {
			override fun onReceive (context: Context, intent: Intent) {
				val networkType = networkType
				if (networkType == oldNetworkType) return
				if (networkType == null) {
					callback.onConnectionLost()
				} else {
					if (oldNetworkType != null) {
						callback.onConnectionLost()
					}
					callback.onConnectionEstablished(networkType)
				}
				oldNetworkType = networkType
			}
		}
		val intentFilter = IntentFilter().apply {
			addAction(ConnectivityManager.CONNECTIVITY_ACTION)
		}
		thisApp.registerReceiver(receiver, intentFilter)
		return Task(
			cancel = { thisApp.unregisterReceiver(receiver) }
		)
	}


}
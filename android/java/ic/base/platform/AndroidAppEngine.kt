package ic.base.platform


import ic.android.app.isDebug
import ic.android.app.thisApp
import ic.app.impl.AppEngine
import ic.app.tier.Tier
import ic.storage.fs.Directory
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.impl.JvmDirectory
import ic.storage.prefs.AndroidPrefs
import ic.app.res.impl.AndroidResources
import ic.struct.value.ext.getValue
import ic.struct.value.cached.Cached


internal object AndroidAppEngine : AppEngine() {

	override val resources get() = AndroidResources

	override fun getTierString() : String? = null

	override fun getDefaultTier() : Tier {
		if (isDebug) {
			return Tier.Debug
		} else {
			return Tier.Production
		}
	}

	override fun initPrivateFolder() : Directory {
		return JvmDirectory(
			javaFile = thisApp.filesDir
		)
	}

	private val externalFilesFolder by Cached {
		JvmDirectory(
			javaFile = thisApp.getExternalFilesDir(null)!!
		)
	}

	override fun initCommonDataDirectory() : Directory {
		return externalFilesFolder.createFolderIfNotExists("common")
	}

	override fun initCommonPublicFolder() : Directory {
		return externalFilesFolder
			.createFolderIfNotExists("public")
			.createFolderIfNotExists("common")
		;
	}

	override fun createPrefs (prefsName: String) = AndroidPrefs(prefsName = prefsName)

}
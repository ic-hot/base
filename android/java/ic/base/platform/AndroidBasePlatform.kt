package ic.base.platform


import ic.network.impl.AndroidNetworkEngine
import ic.parallel.impl.AndroidParallelEngine
import ic.system.impl.AndroidSystemEngine
import ic.util.analytics.impl.AndroidAnalyticsEngine
import ic.util.log.AndroidLogger


internal object AndroidBasePlatform : JvmBasePlatform() {

	override val parallelEngine get() = AndroidParallelEngine

	override val systemEngine get() = AndroidSystemEngine

	override val globalLogger get() = AndroidLogger

	override val networkEngine get() = AndroidNetworkEngine

	override val analyticsEngine get() = AndroidAnalyticsEngine

	override val platformType get() = PlatformType.Jvm.Android

	override val appEngine get() = AndroidAppEngine

}
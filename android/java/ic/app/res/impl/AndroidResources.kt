package ic.app.res.impl


import ic.android.storage.assets.getAssetFileOrThrow
import ic.base.throwables.NotExistsException
import ic.stream.sequence.ByteSequence


internal object AndroidResources : Resources {

	@Throws(NotExistsException::class)
	override fun getFileOrThrow (path: String) : ByteSequence {
		return getAssetFileOrThrow(path)
	}

	override fun getStringOrNull (path: String) : String? {
		TODO("Not yet implemented")
	}

}
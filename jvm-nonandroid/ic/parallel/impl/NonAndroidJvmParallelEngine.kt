package ic.parallel.impl


import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.executor.DirectExecutor


object NonAndroidJvmParallelEngine : JvmParallelEngine() {

	override val defaultCallbackExecutor get() = DirectExecutor

	override fun doInUiThread (action: Action) : Cancelable {
		action.run()
		return Cancelable(
			cancel = {}
		)
	}

}
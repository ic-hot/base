package ic.base.platform


import ic.network.impl.NonAndroidJvmNetworkEngine
import ic.parallel.impl.NonAndroidJvmParallelEngine
import ic.system.impl.NonAndroidJvmSystemEngine


internal abstract class NonAndroidJvmBasePlatform : JvmBasePlatform() {


	override val networkEngine get() = NonAndroidJvmNetworkEngine

	override val parallelEngine get() = NonAndroidJvmParallelEngine

	override val systemEngine get() = NonAndroidJvmSystemEngine


}
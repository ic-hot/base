package ic.network.impl


import ic.design.task.Task
import ic.network.avail.NetworkAvailabilityCallback
import ic.network.avail.NetworkType


internal object NonAndroidJvmNetworkEngine : JvmNetworkEngine() {


	// TODO
	override val networkType: NetworkType? get() = NetworkType.Wired


	// TODO
	override fun listenNetworkAvailability (
		toCallAtOnce : Boolean,
		callback : NetworkAvailabilityCallback
	) : Task {
		if (toCallAtOnce) {
			callback.onConnectionEstablished(NetworkType.Wired)
		}
		return Task(
			cancel = {}
		)
	}


}
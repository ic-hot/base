package ic.app


import ic.base.arrays.ext.join

import ic.app.launch.launch


abstract class App : AApp() {


	companion object {

		@JvmStatic
		fun main (args: Array<String>) {
			appArgs = args.join(separator = ' ')
			launch()
		}

	}


}
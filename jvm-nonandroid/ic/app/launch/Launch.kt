package ic.app.launch


import kotlin.system.exitProcess

import ic.app.App
import ic.base.reflect.extends
import ic.base.reflect.getClassByName
import ic.base.reflect.instantiate
import ic.ifaces.stoppable.ext.stopBlockingIfNeeded
import ic.system.SystemOut
import ic.test.Test


fun launch() {

	val mainClass : Class<out Any> = getClassByName(mainClassName)

	when {

		mainClass extends App::class -> {

			val app = mainClass.instantiate() as App

			Runtime.getRuntime().addShutdownHook(
				java.lang.Thread {
					try {
						app.stopBlockingIfNeeded()
					} catch (any: Throwable) {
						any.printStackTrace()
						exitProcess(1)
					}
				}
			)

			try {
				app.run()
			} catch (t: Throwable) {
				t.printStackTrace()
				exitProcess(1)
			}

		}

		mainClass extends Test::class -> {

			val test = mainClass.instantiate() as Test

			test.run(SystemOut)

			println()
			println("All tests passed.")

		}

		else -> throw Error(
			"mainClass: ${ mainClass.name }"
		)

	}

}
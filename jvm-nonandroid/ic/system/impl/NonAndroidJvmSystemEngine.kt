package ic.system.impl


import ic.storage.fs.FsEntry
import ic.storage.fs.local.ext.exists
import ic.system.ostype.OsType.*


internal object NonAndroidJvmSystemEngine : JvmSystemEngine() {


	override fun getOsType() = when {

		FsEntry.exists("/etc/redhat-release") -> Linux.RedHatBased

		FsEntry.exists("/etc/apt/source.list") -> Linux.DebianBased

		FsEntry.exists("/etc/arch-release") -> Linux.ArchBased

		else -> Linux.Other

	}


}
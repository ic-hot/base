package ic.base.arrays.ext


import kotlinx.cinterop.allocArrayOf
import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.NativePlacement

import platform.Foundation.NSData
import platform.Foundation.create

import ic.base.primitives.int32.ext.asUInt64


@BetaInteropApi
@ExperimentalForeignApi
inline fun NativePlacement.byteArrayToNsData (byteArray: ByteArray) : NSData {
	return NSData.create(
		bytes = allocArrayOf(byteArray),
		length = byteArray.length.asUInt64
	)
}
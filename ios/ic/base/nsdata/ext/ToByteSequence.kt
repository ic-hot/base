package ic.base.nsdata.ext


import platform.Foundation.NSData

import ic.base.arrays.bytes.ext.asByteSequence
import ic.stream.sequence.ByteSequence


inline fun NSData.toByteSequence() : ByteSequence {

	return toByteArray().asByteSequence

}
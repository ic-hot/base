package ic.base.nsdata.ext


import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.usePinned

import platform.Foundation.NSData
import platform.posix.memcpy


@OptIn(ExperimentalForeignApi::class)
fun NSData.toByteArray() : ByteArray {
	val size = length.toInt()
	val bytes = ByteArray(size)
	if (size > 0) {
		bytes.usePinned { pinned ->
			memcpy(pinned.addressOf(0), this.bytes, this.length)
		}
	}
	return bytes
}
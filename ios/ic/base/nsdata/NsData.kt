package ic.base.nsdata


import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.nativeHeap

import platform.Foundation.NSData

import ic.base.arrays.bytes.ByteArray
import ic.base.arrays.ext.byteArrayToNsData


@OptIn(ExperimentalForeignApi::class, BetaInteropApi::class)
private val emptyNsData : NSData = run {
	nativeHeap.run {
		byteArrayToNsData(ByteArray())
	}
}

@Suppress("FunctionName")
fun NsData() : NSData {
	return emptyNsData
}
package ic.base.platform


import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi

import ic.app.impl.IosAppEngine
import ic.network.impl.IosNetworkEngine
import ic.parallel.impl.IosParallelEngine
import ic.storage.IosStorageEngine
import ic.struct.value.ephemeral.impl.IosEphemeralEngine
import ic.system.impl.IosSystemEngine
import ic.util.analytics.impl.IosAnalyticsEngine
import ic.util.crypto.impl.IosCryptoEngine
import ic.util.locale.impl.IosLocaleEngine
import ic.util.log.IosLogger
import ic.util.log.Logger
import ic.util.text.charset.impl.IosCharsetsEngine


@OptIn(ExperimentalForeignApi::class, BetaInteropApi::class)
internal object IosBasePlatform : NativeBasePlatform() {

	override val platformType get() = PlatformType.Ios

	override val analyticsEngine get() = IosAnalyticsEngine

	override val parallelEngine get() = IosParallelEngine

	override val appEngine get() = IosAppEngine

	override val systemEngine get() = IosSystemEngine

	override val ephemeralEngine get() = IosEphemeralEngine

	override val charsetsEngine get() = IosCharsetsEngine

	override val localeEngine get() = IosLocaleEngine

	override val networkEngine get() = IosNetworkEngine

	override val storageEngine get() = IosStorageEngine

	override val cryptoEngine get() = IosCryptoEngine

	override val globalLogger get() = IosLogger

}
package ic.struct.value.ephemeral.impl


import kotlin.native.ref.WeakReference

import ic.struct.value.ephemeral.Ephemeral
import kotlin.experimental.ExperimentalNativeApi


@OptIn(ExperimentalNativeApi::class)
class IosSoftReference<Value: Any> (value: Value) : Ephemeral<Value> {

	private val nativeWeakReference = WeakReference(value)

	override val value get() = nativeWeakReference.value

}
package ic.struct.value.ephemeral.impl


import ic.struct.value.ephemeral.Ephemeral


object IosEphemeralEngine : EphemeralEngine {

	override fun <Value : Any> createSoftReference (value: Value) : Ephemeral<Value> {
		return IosSoftReference(value)
	}

	override fun <Value : Any> createWeakReference (value: Value) : Ephemeral<Value> {
		return IosWeakReference(value)
	}

}
package ic.storage


import ic.storage.impl.StorageEngine


internal object IosStorageEngine : StorageEngine {

	override val localFsEngine get() = throw NotImplementedError()

}
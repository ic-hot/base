package ic.storage.prefs


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import platform.Foundation.NSUserDefaults

import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.util.code.json.JsonArray
import ic.util.code.json.JsonObject


class IosPrefs (private val prefsName: String) : Prefs {

	private fun getComplexKey (key: String) = "$prefsName.$key"

	override fun set (key: String, value: Any?) {
		when (value) {
			null -> {
				NSUserDefaults.standardUserDefaults.removeObjectForKey(
					getComplexKey(key)
				)
			}
			is Boolean -> {
				NSUserDefaults.standardUserDefaults.setBool(
					value = value,
					forKey = getComplexKey(key)
				)
			}
			is Int32 -> {
				NSUserDefaults.standardUserDefaults.setInteger(
					value = value.asInt64,
					forKey = getComplexKey(key)
				)
			}
			is Int64 -> {
				NSUserDefaults.standardUserDefaults.setInteger(
					value = value,
					forKey = getComplexKey(key)
				)
			}
			is Float64 -> {
				NSUserDefaults.standardUserDefaults.setDouble(
					value = value,
					forKey = getComplexKey(key)
				)
			}
			is String -> {
				NSUserDefaults.standardUserDefaults.setObject(
					value = value,
					forKey = getComplexKey(key)
				)
			}
			is JsonObject -> {
				val string = value.toString()
				NSUserDefaults.standardUserDefaults.setObject(
					value = string,
					forKey = getComplexKey(key)
				)
			}
			is JsonArray -> {
				val string = value.toString()
				NSUserDefaults.standardUserDefaults.setObject(
					value = string,
					forKey = getComplexKey(key)
				)
			}
			else -> throw NotImplementedError("value: $value")
		}
	}

	override fun getBooleanOrNull (key: String): Boolean? {
		val obj : Any? = NSUserDefaults.standardUserDefaults.objectForKey(
			defaultName = getComplexKey(key)
		)
		return obj as? Boolean
	}

	override fun getInt32OrNull (key: String) : Int32? {
		val obj : Any? = NSUserDefaults.standardUserDefaults.objectForKey(
			defaultName = getComplexKey(key)
		)
		return (obj as? Int64)?.asInt32
	}

	override fun getInt64OrNull (key: String) : Int64? {
		val obj : Any? = NSUserDefaults.standardUserDefaults.objectForKey(
			defaultName = getComplexKey(key)
		)
		return obj as? Int64
	}

	override fun getFloat64OrNull (key: String) : Float64? {
		val obj : Any? = NSUserDefaults.standardUserDefaults.objectForKey(
			defaultName = getComplexKey(key)
		)
		return obj as? Float64
	}

	override fun getStringOrNull (key: String) : String? {
		return NSUserDefaults.standardUserDefaults.stringForKey(
			defaultName = getComplexKey(key)
		)
	}

}
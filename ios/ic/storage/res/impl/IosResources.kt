package ic.storage.res.impl


import platform.Foundation.create
import platform.Foundation.NSBundle
import platform.Foundation.NSData

import ic.app.res.impl.Resources
import ic.base.nsdata.ext.toByteSequence
import ic.base.throwables.NotExistsException
import ic.stream.sequence.ByteSequence


internal object IosResources : Resources {


	@Throws(NotExistsException::class)
	override fun getFileOrThrow (path: String) : ByteSequence {
		val filePath = NSBundle.mainBundle.pathForResource(
			name = "IcResources/$path",
			ofType = null
		)
		if (filePath == null) throw NotExistsException
		val nsData = NSData.create(contentsOfFile = filePath)
		if (nsData == null) throw NotExistsException
		return nsData.toByteSequence()
	}


	override fun getStringOrNull (path: String) : String? {
		TODO("Not yet implemented")
	}


}
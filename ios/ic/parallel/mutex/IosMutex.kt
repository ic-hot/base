package ic.parallel.mutex


import platform.Foundation.NSRecursiveLock


internal class IosMutex : Mutex {

	private val nsRecursiveLock = NSRecursiveLock()

	override fun seize() {
		nsRecursiveLock.lock()
	}

	override fun release() {
		nsRecursiveLock.unlock()
	}

}
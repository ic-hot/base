package ic.parallel.thread


import ic.parallel.funs.sleepWhile
import ic.parallel.thread.impl.ThreadImplementation


import platform.Foundation.NSThread


internal class IosThreadImplementation (

	thread : Thread

) : ThreadImplementation(thread) {

	private var isRunning : Boolean = false

	private val nsThread = NSThread {
		try {
			thread.runBlocking()
		} finally {
			isRunning = false
		}
	}

	override val isCurrentThread get() = nsThread == NSThread.currentThread

	override fun startNonBlocking() {
		isRunning = true
		nsThread.start()
	}

	override fun stopNonBlocking() {
		if (!isRunning) return
		nsThread.cancel()
	}

	override fun waitFor() {
		sleepWhile { isRunning }
	}

}
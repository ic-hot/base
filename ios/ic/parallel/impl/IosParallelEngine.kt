package ic.parallel.impl


import platform.Foundation.NSThread
import platform.darwin.dispatch_async
import platform.darwin.dispatch_get_main_queue

import ic.base.primitives.int64.Int64
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.executor.IosMainThreadExecutor
import ic.parallel.mutex.IosMutex
import ic.parallel.mutex.Mutex
import ic.parallel.thread.IosThreadImplementation
import ic.parallel.thread.Thread
import ic.parallel.thread.funs.StopThread
import ic.parallel.thread.impl.ThreadImplementation


internal object IosParallelEngine : NativeParallelEngine() {

	override fun sleep (durationMs: Int64) {
		platform.posix.usleep(
			(durationMs * 1000).toUInt()
		)
	}

	override fun createThreadImplementation (thread: Thread) : ThreadImplementation {
		return IosThreadImplementation(thread)
	}

	override fun createMutex() : Mutex = IosMutex()

	override val defaultCallbackExecutor get() = IosMainThreadExecutor

	override fun doInUiThread (action: Action) : Cancelable {
		var isCanceled : Boolean = false
		dispatch_async(dispatch_get_main_queue()) {
			if (!isCanceled) {
				action.run()
			}
		}
		return Cancelable {
			isCanceled = true
		}
	}

	override fun canBeStoppedHere() {
		if (NSThread.currentThread.isCancelled()) StopThread()
	}

}
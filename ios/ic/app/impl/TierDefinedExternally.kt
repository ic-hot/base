package ic.app.impl


import ic.app.tier.Tier
import ic.base.annotations.UsedExternally


@UsedExternally
lateinit var tierDefinedExternally : Tier
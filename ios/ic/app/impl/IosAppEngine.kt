package ic.app.impl


import ic.storage.fs.Directory
import ic.storage.prefs.IosPrefs
import ic.storage.prefs.Prefs
import ic.storage.res.impl.IosResources


internal object IosAppEngine : AppEngine() {

	override val resources get() = IosResources

	override fun initPrivateFolder(): Directory {
		TODO("Not yet implemented")
	}

	override fun initCommonDataDirectory(): Directory {
		TODO("Not yet implemented")
	}

	override fun initCommonPublicFolder(): Directory {
		TODO("Not yet implemented")
	}

	override fun createPrefs (prefsName: String) : Prefs {
		return IosPrefs(prefsName)
	}

	override fun getTierString() = null

	override fun getDefaultTier() = tierDefinedExternally

}
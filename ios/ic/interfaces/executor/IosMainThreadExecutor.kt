package ic.ifaces.executor


import ic.ifaces.action.Action
import ic.ifaces.executor.Executor
import ic.ifaces.stoppable.Stoppable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized

import platform.Foundation.NSRunLoop
import platform.Foundation.performBlock


object IosMainThreadExecutor : Executor {


	override fun execute (action: Action) : Stoppable {

		val mutex = Mutex()

		var isStopped : Boolean = false

		NSRunLoop.mainRunLoop.performBlock {
			mutex.synchronized {
				if (!isStopped) {
					action.run()
				}
			}
		}

		return Stoppable(
			stopNonBlockingOrThrowNotNeeded = {
				mutex.synchronized {
					isStopped = true
				}
			},
			waitFor = {}
		)

	}


}
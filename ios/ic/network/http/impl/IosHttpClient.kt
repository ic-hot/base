package ic.network.http.impl


import kotlinx.cinterop.*

import platform.Foundation.*

import ic.base.arrays.ext.byteArrayToNsData
import ic.base.kcollections.ext.toFiniteMap
import ic.base.nsdata.ext.toByteArray
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asInt32
import ic.base.strings.ext.replaceAll
import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse
import ic.stream.sequence.empty.EmptyByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray
import ic.struct.map.finite.ext.toKotlinMap
import ic.util.mimetype.MimeType
import ic.util.text.charset.Charset


@ExperimentalForeignApi
@BetaInteropApi
object IosHttpClient : HttpClient {

	@Throws(IoException::class, HttpException::class)
	override fun sendHttpRequest (
		request : HttpRequest,
		connectTimeoutMs : Int32,
		readTimeoutMs : Int32,
		toIgnoreTrustCertificate : Boolean
	) : HttpResponse {

		memScoped {

			val nsRequest = NSMutableURLRequest().apply {
				setURL(
					NSURL(string = request.urlString)
				)
				setHTTPMethod(request.method)
				@Suppress("UNCHECKED_CAST")
				setAllHTTPHeaderFields(
					request.headers.toKotlinMap() as kotlin.collections.Map<Any?, *>
				)
				setHTTPBody(
					byteArrayToNsData(request.body.toByteArray())
				)
			}

			val (nsUrlResponse, nsError, responseNsData) = run {
				val responsePointer : ObjCObjectVar<NSURLResponse?> = alloc()
				val errorPointer : ObjCObjectVar<NSError?> = alloc()
				val responseNsData = NSURLConnection.sendSynchronousRequest(
					request = nsRequest,
					returningResponse = responsePointer.ptr,
					error = errorPointer.ptr
				)
				Triple(responsePointer.value, errorPointer.value, responseNsData)
			}

			if (nsUrlResponse !is NSHTTPURLResponse) {
				val formattedRequest = request.toString().replaceAll("\n" to "\n    ")
				throw HttpException(
					request = request,
					response = HttpResponse(
						statusCode = when (nsError?.code) {
							-1008L -> 404
							-1011L -> 500
							-1012L, -1013L -> 401
							else -> {
								throw IoException
							}
						}
					)
				)
			}

			val statusCode = nsUrlResponse.statusCode.asInt32

			@Suppress("UNCHECKED_CAST")
			val responseHeaders = (
				nsUrlResponse.allHeaderFields as kotlin.collections.Map<String, String>
			).toFiniteMap()

			val body = (
				if (responseNsData == null) {
					EmptyByteSequence
				} else {
					ByteSequenceFromByteArray(
						responseNsData.toByteArray()
					)
				}
			)

			val charset = (
				Charset.byHttpNameOrNull(nsUrlResponse.textEncodingName) ?: Charset.defaultHttp
			)

			val response = HttpResponse(
				statusCode = statusCode,
				contentType = MimeType.byNameOrNull(nsUrlResponse.MIMEType),
				charset = charset,
				headers = responseHeaders,
				body = body
			)

			val isSuccess = statusCode in 200..204

			if (isSuccess) {
				return response
			} else {
				throw HttpException(request, response)
			}

		}

	}


}
package ic.network.impl


import ic.design.task.Task
import ic.network.avail.NetworkAvailabilityCallback
import ic.network.avail.NetworkType
import ic.network.http.impl.IosHttpClient
import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi


@BetaInteropApi
@ExperimentalForeignApi
internal object IosNetworkEngine : NetworkEngine {

	override val networkType: NetworkType?
		get() = TODO("Not yet implemented")

	override fun listenNetworkAvailability(toCallAtOnce: Boolean, callback: NetworkAvailabilityCallback): Task {
		TODO("Not yet implemented")
	}

	override val httpClient get() = IosHttpClient

}
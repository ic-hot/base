package ic.util.locale.impl


import ic.util.locale.Locale

import platform.Foundation.NSLocale
import platform.Foundation.languageCode


internal class LocaleFromNsLocale (val nsLocale: NSLocale) : Locale {


	override val languageCode get() = nsLocale.languageCode


}
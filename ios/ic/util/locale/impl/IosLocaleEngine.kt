package ic.util.locale.impl


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int64.ext.asFloat64
import ic.base.throwables.UnableToParseException
import ic.util.locale.Locale
import ic.util.time.Time

import platform.Foundation.*
import kotlin.native.concurrent.freeze


internal object IosLocaleEngine : LocaleEngine {

	override fun getDefaultLocale() : Locale = LocaleFromNsLocale(
		NSLocale(NSLocale.preferredLanguages[0] as String)
	)

	override fun getLocale (languageCode: String, countryCode: String) : Locale = LocaleFromNsLocale(
		NSLocale("$languageCode-$countryCode")
	)

	override fun formatFloat64 (float64: Float64, pattern: String, locale: Locale, groupingSeparator: Char) : String {
		val nsNumberFormatter = NSNumberFormatter()
		nsNumberFormatter.positiveFormat = pattern
		nsNumberFormatter.negativeFormat = "-" + pattern
		nsNumberFormatter.setGroupingSeparator(groupingSeparator.toString())
		return nsNumberFormatter.stringFromNumber(NSNumber(float64))!!
	}

	override fun formatTime (
		time : Time,
		pattern : String,
		locale : Locale,
		toUseStandaloneMonth : Boolean
	) : String {
		val nsDateFormatter = NSDateFormatter()
		nsDateFormatter.dateFormat = pattern
		nsDateFormatter.locale = (locale as LocaleFromNsLocale).nsLocale
		val nsDate = NSDate.create(
			timeIntervalSince1970 = time.epochMs.asFloat64 / 1000
		)
		return nsDateFormatter.stringFromDate(nsDate)
	}

	@Throws(UnableToParseException::class)
	override fun parseTimeOrThrowUnableToParse(
		string : String,
		pattern : String,
		locale : Locale
	) : Time {
		val nsDateFormatter = NSDateFormatter()
		nsDateFormatter.dateFormat = pattern
		nsDateFormatter.locale = (locale as LocaleFromNsLocale).nsLocale
		val nsDate = nsDateFormatter.dateFromString(string) ?: throw UnableToParseException
		return Time(
			epochMs = (nsDate.timeIntervalSince1970 * 1000).asInt64
		)
	}

}
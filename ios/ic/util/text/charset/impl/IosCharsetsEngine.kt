package ic.util.text.charset.impl


import ic.base.throwables.UnableToParseException
import ic.util.text.charset.Charset


object IosCharsetsEngine : CharsetsEngine {

	override fun stringToByteArray (string: String, charset: Charset) : ByteArray {
		return when (charset) {
			Charset.Utf8 -> string.encodeToByteArray()
			else -> throw NotImplementedError("charset: $charset")
		}
	}

	override fun byteArrayToString (byteArray: ByteArray, charset: Charset) : String {
		return when (charset) {
			Charset.Utf8 -> byteArray.decodeToString()
			else -> throw NotImplementedError("charset: $charset")
		}
	}

	override fun encodeUrl (string: String, charset: Charset) : String {
		return string // TODO Implement
	}

	@Throws(UnableToParseException::class)
	override fun decodeUrlOrThrowUnableToParse (string: String, charset: Charset) : String {
		return string // TODO Implement
	}

}
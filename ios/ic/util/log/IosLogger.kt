package ic.util.log


import platform.Foundation.NSLog

import ic.base.strings.ext.limit
import ic.text.TextBuffer


object IosLogger : Logger() {

	override fun implementLog (level: LogLevel, tag: String, value: String) {
		try {
			NSLog(
				TextBuffer().apply {
					write(tag)
					putCharacter(' ')
					write(value)
					writeNewLine()
				}.toString()
			)
		} catch (e: Exception) {
			NSLog("Logger error")
		}
	}

}
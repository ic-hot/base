package ic.util.crypto.impl


import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.convert
import kotlinx.cinterop.usePinned

import platform.CoreCrypto.CC_SHA256
import platform.CoreCrypto.CC_SHA256_DIGEST_LENGTH


internal object IosCryptoEngine : CryptoEngine() {


	@OptIn(ExperimentalForeignApi::class)
	override fun sha256 (data: ByteArray) : ByteArray {
		val digest = UByteArray(CC_SHA256_DIGEST_LENGTH)
		data.usePinned { inputPinned ->
			digest.usePinned { digestPinned ->
				CC_SHA256(inputPinned.addressOf(0), data.size.convert(), digestPinned.addressOf(0))
			}
		}
		return digest.toByteArray()
	}


}
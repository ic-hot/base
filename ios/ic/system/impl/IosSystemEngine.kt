package ic.system.impl


import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.system.CpuArchitecture
import ic.system.ostype.OsType

import platform.Foundation.NSDate
import platform.Foundation.timeIntervalSince1970


internal object IosSystemEngine : SystemEngine() {

	override fun getCpuArchitecture() : CpuArchitecture {
		return CpuArchitecture.X86_64
	}

	override fun getOsType(): OsType {
		return OsType.Ios
	}

	override fun getCurrentEpochTimeMs() : Int64 {
		return (NSDate().timeIntervalSince1970() * 1000).asInt64
	}

	override fun getCpuCoresCount() : Int32 = 4

}
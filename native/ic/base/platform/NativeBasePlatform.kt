package ic.base.platform


import ic.parallel.impl.NativeParallelEngine


internal abstract class NativeBasePlatform : BasePlatform() {

	abstract override val parallelEngine : NativeParallelEngine

}
#ifndef BASE_INCLUDED
#define BASE_INCLUDED


#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


typedef char          byte;
typedef char          bit;
typedef int           int32;
typedef unsigned int  uint32;
typedef long          int64;
typedef unsigned long uint64;
typedef float         float32;
typedef double        float64;
typedef char*         string;


const float64 Inf = INFINITY;


static inline void error() {
  printf("error\n");
  exit(1);
}

static inline void debug1 (string String, int64 Arg) {
  printf(String, Arg);
  printf("\n");
}

static inline void debug (string String) {
  debug1(String, 0);
}

static inline void assert (bit Condition) {
  if (!Condition) {
    error();
  }
}

static inline void assertNotNan (float64 X) {
  assert(!isnan(X));
}

static inline void assert1 (bit Condition, string String, int64 Number) {
  if (!Condition) {
    debug1(String, Number);
    error();
  }
}


static inline float32 clampFloat32 (float32 N) {
  if (N < 0) return 0;
  if (N > 1) return 1;
  return N;
}


static inline int64 divCeil (int64 A, int64 B) {
  //assert(A > 0);
  //assert(B > 0);
  int64 C = A / B;
  if (C * B == A) {
    return C;
  } else {
    return C + 1;
  }
}

static inline float64 absFloat64 (float64 A) {
  if (A < 0) {
    return -A;
  } else {
    return A;
  }
}


typedef struct thread {
  pthread_t Tid;
} thread;

thread doInBackground (void* function, void* Arg) {
  thread Thread;
  pthread_create(&(Thread.Tid), 0, function, Arg);
  return Thread;
}


#endif

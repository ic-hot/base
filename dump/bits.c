#ifndef BITS_INCLUDED
#define BITS_INCLUDED


#include "base.c"
#include "memory.c"


static inline bit byteGetBit (byte Byte, uint32 Index) {
  return (Byte >> (7 - Index)) & 1;
}

static inline void byteSetBit (byte* Ref, uint32 Index, bit Bit) {
  byte Byte = *Ref;
  byte ShiftedOne = 1 << (7 - Index);
  if (Bit) {
    Byte = Byte | ShiftedOne;
  } else {
    Byte = Byte & (~ShiftedOne);
  }
  *Ref = Byte;
}

static inline bit int32GetBit (int32 Int32, uint32 Index) {
  return (Int32 >> (31 - Index)) & 1;
}

static inline void int32SetBit (int32* Ref, uint32 Index, bit Bit) {
  int32 Int32 = *Ref;
  byte ShiftedOne = 1 << (31 - Index);
  if (Bit) {
    Int32 = Int32 | ShiftedOne;
  } else {
    Int32 = Int32 & (~ShiftedOne);
  }
  *Ref = Int32;
}

static inline bit uint32GetBit (uint32 Uint32, uint32 Index) {
  return (Uint32 >> (31 - Index)) & 1;
}

static inline bit byteXor (byte Byte) {
  return (
    Byte ^
    (Byte >> 1) ^
    (Byte >> 2) ^
    (Byte >> 3) ^
    (Byte >> 4) ^
    (Byte >> 5) ^
    (Byte >> 6) ^
    (Byte >> 7)
  ) & 1;
}

static inline int32 byteCountOnes (byte Byte) {
  int32 Result = 0;
  for (int32 I = 0; I < 8; I++) {
    Result += byteGetBit(Byte, I);
  }
  return Result;
}


typedef struct bits {
  byte* Ref;
  int64 C;
} bits;

static inline bits createBits (uint64 C) {
  bits Bits;
  Bits.C = C;
  if (C > 0) {
    Bits.Ref = allocateClear(divCeil(C, 8));
  }
  return Bits;
}

static inline void destroyBits (bits Bits) {
  deallocate(Bits.Ref);
}

static inline bit bitsGet (bits Bits, int64 Index) {
  return byteGetBit(Bits.Ref[Index / 8], Index % 8);
}

static inline void bitsSet (bits Bits, int64 Index, bit Bit) {
  byteSetBit(Bits.Ref + (Index / 8), Index % 8, Bit);
}

static inline bit areBitsEqual (bits A, bits B) {
  int64 LengthInBytes = divCeil(A.C, 8);
  for (int64 I = 0; I < LengthInBytes; I++) {
    if (A.Ref[I] != B.Ref[I]) return 0;
  }
  return 1;
}

static inline bits andBitsToBits (bits A, bits B, bits C) {
  int64 LengthInBytes = divCeil(A.C, 8);
  for (int64 I = 0; I < LengthInBytes; I++) {
    C.Ref[I] = A.Ref[I] & B.Ref[I];
  }
  return C;
}

static inline bits and3BitsToBits (bits A, bits B, bits C, bits D) {
  int64 LengthInBytes = divCeil(A.C, 8);
  for (int64 I = 0; I < LengthInBytes; I++) {
    D.Ref[I] = A.Ref[I] & B.Ref[I] & C.Ref[I];
  }
  return D;
}

static inline bit bitsXor (bits Bits) {
  int64 LengthInBytes = divCeil(Bits.C, 8);
  byte ResultByte = 0;
  for (int64 I = 0; I < LengthInBytes; I++) {
    ResultByte = ResultByte ^ Bits.Ref[I];
  }
  return byteXor(ResultByte);
}

void clearBits (bits Bits) {
  clear(Bits.Ref, divCeil(Bits.C, 8));
}

static inline int64 bitsCountOnes (bits Bits) {
  int64 LengthInBytes = divCeil(Bits.C, 8);
  int64 Result = 0;
  for (int64 I = 0; I < LengthInBytes; I++) {
    Result += byteCountOnes(Bits.Ref[I]);
  }
  return Result;
}


void printBit (bit Bit) {
  if (Bit) printf("1"); else printf("0");
}

void printBitsOfByte (byte Byte) {
  for (int32 I = 0; I < 8; I++) {
    bit Bit = byteGetBit(Byte, I);
    printBit(Bit);
  }
}

void printBitsOfUint32 (uint32 Uint32) {
  for (int32 I = 0; I < 32; I++) {
    bit Bit = uint32GetBit(Uint32, I);
    printBit(Bit);
  }
}

void printBits (bits Bits) {
  for (int32 I = 0; I < Bits.C; I++) {
    bit Bit = bitsGet(Bits, I);
    printBit(Bit);
  }
}


#endif

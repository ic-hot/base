#ifndef MINMAX_INCLUDED
#define MINMAX_INCLUDED


#include "base.c"


static inline uint32 maxUint32 (uint32 A, uint32 B) {
  if (A < B) return B; else return A;
}


static inline uint64 minUint64 (uint64 A, uint64 B) {
  if (A > B) return B; else return A;
}


#endif

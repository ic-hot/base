#ifndef RANDOM_INCLUDED
#define RANDOM_INCLUDED


#include "base.c"


//uint32 Seed = 0;
    
/*
uint32 randomUint32 () {
  //Seed = 214013 * Seed + 2531011;
  return rand();
}
*/

uint32 randomUint32 () {
  return (
    (rand() << 16) | 
    (rand() << 0 )
  );
}

float32 randomFloat32Below1 () {
  return (float32) rand() / RAND_MAX;
}

bit randomBitProb (float32 Probability) {
  return randomFloat32Below1() < Probability;
}

byte randomByte () {
  return (
    (((uint64) rand()) << 48) |
    (((uint64) rand()) << 32) |
    (((uint64) rand()) << 16) |
    (((uint64) rand()) << 0 )
  );
}

uint64 randomUint64 () {
  return (rand() << 16) | rand();
}

uint32 randomUint32Below (int32 Limit) {
  return randomUint32() % Limit;
}

uint64 randomUint64Below (int64 Limit) {
  return randomUint64() % Limit;
}

uint32 randomUint32InRange (uint32 From, uint32 To) {
  return From + randomUint32Below(To - From);
}

bit randomBit () {
  return rand() & 1;
}


#endif

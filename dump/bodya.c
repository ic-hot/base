#include "ai.c"

#include <jpeglib.h>
#include <gtk/gtk.h>

int32 CoordSize = 8;
int32 ImgSize = 256;

typedef struct color {
  byte R;
  byte G;
  byte B;
} color;

color* OriginalImage;
color* PredictedImage;

GtkWidget* ImageWidget;

data Data;

ai Ai;

gboolean updateImage() {
  GdkPixbuf* pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, ImgSize, ImgSize);
  guchar* pixels_data = gdk_pixbuf_get_pixels(pixbuf);
  for (int y = 0; y < ImgSize; y++) for (int x = 0; x < ImgSize; x++) {
    int index = (y * ImgSize + x) * 3;
    pixels_data[index + 0] = PredictedImage[y * ImgSize + x].R;
    pixels_data[index + 1] = PredictedImage[y * ImgSize + x].G;
    pixels_data[index + 2] = PredictedImage[y * ImgSize + x].B;
  }
  gtk_image_set_from_pixbuf(GTK_IMAGE(ImageWidget), pixbuf);
  gtk_widget_queue_draw(ImageWidget);
  g_object_unref(pixbuf);
}

const int32 ThreadsCount = 6;

uint32* Seeds;

void train (int32* ThreadIndexRef) {
  int32 ThreadIndex = *ThreadIndexRef;
  while (1) {
    for (int32 P = CoordSize * 2 + ThreadIndex; P < CoordSize * 2 + 8 * 3; P += ThreadsCount) {
      aiTrainBatch(Ai, P, Data, 256, Seeds + ThreadIndex);
    }
  }
}

void updateImagePeriodically() {
  while (1) {
    sleep(16);
    g_idle_add(updateImage, 0);
  }
}

void predict () {
  bits Sample = allocateBits(CoordSize * 2 + 8 * 3);
  bits Palette = allocateBits(CoordSize * 2 + 8 * 3);
  while (1) {
    int32 BitsLost = 0;
    for (int32 PixelIndex = 0; PixelIndex < ImgSize * ImgSize; PixelIndex++) {
      int32 X = PixelIndex % ImgSize;
      int32 Y = PixelIndex / ImgSize;
      for (int32 P = 0; P < CoordSize; P++) {
        bitsSet(Sample, P, int32GetBit(X, 32 - CoordSize + P));
      }
      for (int32 P = 0; P < CoordSize; P++) {
        bitsSet(Sample, CoordSize + P, int32GetBit(Y, 32 - CoordSize + P));
      }
      for (int32 I = 0; I < 8; I++) {
        bit Bit = aiPredictBit(Ai, CoordSize * 2 + I, Sample, Palette);
        byteSetBit(&(PredictedImage[PixelIndex].R), I, Bit);
        if (byteGetBit(OriginalImage[PixelIndex].R, I) != Bit) BitsLost++;
      }
      for (int32 I = 0; I < 8; I++) {
        bit Bit = aiPredictBit(Ai, CoordSize * 2 + 8 + I, Sample, Palette);
        byteSetBit(&(PredictedImage[PixelIndex].G), I, Bit);
        if (byteGetBit(OriginalImage[PixelIndex].G, I) != Bit) BitsLost++;
      }
      for (int32 I = 0; I < 8; I++) {
        bit Bit = aiPredictBit(Ai, CoordSize * 2 + 8 * 2 + I, Sample, Palette);
        byteSetBit(&(PredictedImage[PixelIndex].B), I, Bit);
        if (byteGetBit(OriginalImage[PixelIndex].B, I) != Bit) BitsLost++;
      }
    }
    printf("Loss: %d\n", BitsLost);
  }
}

int32 main (int argc, char *argv[]) {

  printf("Reading from file...\n");
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);
  FILE *infile = fopen("image.jpg", "rb");
  if (!infile) {
    fprintf(stderr, "Error opening input file\n");
    return 1;
  }
  jpeg_stdio_src(&cinfo, infile);
  jpeg_read_header(&cinfo, TRUE);
  jpeg_start_decompress(&cinfo);
  assert(cinfo.output_width == ImgSize);
  assert(cinfo.output_height == ImgSize);
  JSAMPARRAY buffer = (*cinfo.mem->alloc_sarray) ((j_common_ptr) &cinfo, JPOOL_IMAGE, cinfo.output_width * cinfo.num_components, 1);
  OriginalImage = malloc(cinfo.output_width * cinfo.output_height * sizeof(color));
  PredictedImage = malloc(cinfo.output_width * cinfo.output_height * sizeof(color));
  int pixel_index = 0;
  while (cinfo.output_scanline < cinfo.output_height) {
    jpeg_read_scanlines(&cinfo, buffer, 1);
    for (int x = 0; x < cinfo.output_width; x++) {
      if (cinfo.num_components == 1) {
        OriginalImage[pixel_index].R = buffer[0][cinfo.output_components * x];
        OriginalImage[pixel_index].G = buffer[0][cinfo.output_components * x];
        OriginalImage[pixel_index].B = buffer[0][cinfo.output_components * x];
      }
      else if (cinfo.num_components == 3) {
        OriginalImage[pixel_index].R = buffer[0][cinfo.output_components * x];
        OriginalImage[pixel_index].G = buffer[0][cinfo.output_components * x + 1];
        OriginalImage[pixel_index].B = buffer[0][cinfo.output_components * x + 2];
      }
      else error();
      pixel_index++;
    }
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  fclose(infile);
  
  printf("Populating data...\n");
  Data = allocateData(CoordSize * 2 + 8 * 3, ImgSize * ImgSize, 1024 * 64, 1.0 / 10);
  bits Sample = allocateBits(CoordSize * 2 + 8 * 3);
  for (int32 Y = 0; Y < ImgSize; Y++) for (int32 X = 0; X < ImgSize; X++) {
    color Pixel = OriginalImage[Y * ImgSize + X];
    for (int32 P = 0; P < CoordSize; P++) {
      bitsSet(Sample, P, int32GetBit(X, 32 - CoordSize + P));
    }
    for (int32 P = 0; P < CoordSize; P++) {
      bitsSet(Sample, CoordSize + P, int32GetBit(Y, 32 - CoordSize + P));
    }
    for (int32 P = 0; P < 8; P++) {
      bitsSet(Sample, CoordSize * 2 + P, byteGetBit(Pixel.R, P));
    }
    for (int32 P = 0; P < 8; P++) {
      bitsSet(Sample, CoordSize * 2 + 8 + P, byteGetBit(Pixel.G, P));
    }
    for (int32 P = 0; P < 8; P++) {
      bitsSet(Sample, CoordSize * 2 + 8 * 2 + P, byteGetBit(Pixel.B, P));
    }
    dataAddSample(Data, Sample);
  }
  bitsFree(Sample);
  
  printf("Initializing predictor...\n");
  Ai = makeAi(CoordSize * 2 + 8 * 3);
  for (int32 P = CoordSize * 2; P < CoordSize * 2 + 8 * 3; P++) {
    for (int32 I = 0; I < CoordSize * 2; I++) {
      aiSetADependsOnB(Ai, P, I, 1);
    }
    aiSetRelationsCount(Ai, P, 1024);
  }
  
  printf("Opening window...\n");
  gtk_init(&argc, &argv);
  GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "Bodya");
  gtk_window_set_default_size(GTK_WINDOW(window), ImgSize, ImgSize);
  GdkPixbuf* pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, ImgSize, ImgSize);
  guchar* pixels_data = gdk_pixbuf_get_pixels(pixbuf);
  for (int y = 0; y < ImgSize; y++) for (int x = 0; x < ImgSize; x++) {
    int index = (y * ImgSize + x) * 3;
    pixels_data[index + 0] = OriginalImage[y * ImgSize + x].R;
    pixels_data[index + 1] = OriginalImage[y * ImgSize + x].G;
    pixels_data[index + 2] = OriginalImage[y * ImgSize + x].B;
  }
  ImageWidget = gtk_image_new_from_pixbuf(pixbuf);
  gtk_container_add(GTK_CONTAINER(window), ImageWidget);
  gtk_widget_show_all(window);
  
  Seeds = malloc(sizeof(uint32) * ThreadsCount);
  
  int32 ThreadsIndexes [ThreadsCount];
  for (int32 I = 0; I < ThreadsCount; I++) {
    ThreadsIndexes[I] = I;
    doInBackground1(train, &(ThreadsIndexes[I]));
  }
  doInBackground(predict);
  doInBackground(updateImagePeriodically);
  
  gtk_main();
  g_object_unref(pixbuf);
  
  free(Seeds);
  free(OriginalImage);
  free(PredictedImage);
  
  return 0;
  
}

#ifndef MEMORY_INCLUDED
#define MEMORY_INCLUDED


#include <stdlib.h>

#include "base.c"


void* allocate (uint64 LengthInBytes) {
  return malloc(LengthInBytes);
}

void clear (void* Block, uint64 LengthInBytes) {
  memset(Block, 0, LengthInBytes);
}

void* allocateClear (uint64 LengthInBytes) {
  void* Block = malloc(LengthInBytes);
  clear(Block, LengthInBytes);
  return Block;
}

void deallocate (void* Block) {
  free(Block);
}


void memCopy (void* From, void* To, uint64 LengthInBits) {
  memcpy(To, From, LengthInBits);
}


#endif

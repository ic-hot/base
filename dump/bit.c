#ifndef BIT_INCLUDED
#define BIT_INCLUDED


#include "base.c"


static inline bit bitAssertValid (bit Bit) {
  assert1(
    (Bit & 0b11111110) == 0, 
    "bitAssertValid Bit: %d", Bit
  );
}

static inline int32 bitAsSignum (bit Bit) {
  if (Bit) {
    return -1;
  } else {
    return 1;
  }
}

#endif

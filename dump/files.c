#ifndef FILES_INCLUDED
#define FILES_INCLUDED

#include "base.c"

string readFileAsString (string FileName) {
  FILE* File = fopen(FileName, "r");
  if (!File) error();
  fseek(File, 0, SEEK_END);
  int64 Length = ftell(File);
  fseek(File, 0, SEEK_SET);
  string String = malloc(Length + 1);
  fread(String, 1, Length, File);
  fclose(File);
  String[Length] = 0;
  return String;
}

#endif

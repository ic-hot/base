package ic.android.ms.huawei.maps


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.ifaces.cancelable.Cancelable
import ic.struct.collection.Collection
import ic.struct.map.editable.EditableMap
import ic.util.geo.Location

import android.content.Context
import android.view.View

import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap

import ic.android.ms.maps.MapMarker
import ic.android.ms.maps.MapCarrier
import ic.android.ms.maps.MapPolyline

import com.huawei.hms.maps.MapView
import com.huawei.hms.maps.model.*
import ic.struct.collection.ext.foreach.breakableForEach


class HuaweiMapCarrier : MapCarrier {


	private var mapView : MapView? = null


	private var huaweiMap : HuaweiMap? = null


	override fun initView (context : Context) : View {
		mapView = MapView(context)
		return mapView!!
	}


	override fun open (

		styleResId : Int32,

		onReady : () -> Unit

	) {

		val mapView = mapView!!

		mapView.onCreate(null)
		mapView.onStart()
		mapView.onResume()
		mapView.getMapAsync { huaweiMap ->

			this.huaweiMap = huaweiMap

			huaweiMap.uiSettings.isMapToolbarEnabled = false
			huaweiMap.uiSettings.isZoomGesturesEnabled = true
			huaweiMap.uiSettings.isScrollGesturesEnabled = true

			huaweiMap.setPadding(leftPx, topPx, rightPx, bottomPx)

			if (styleResId != 0) {
				huaweiMap.setMapStyle(
					MapStyleOptions.loadRawResourceStyle(mapView.context, styleResId)
				)
			}

			huaweiMap.setOnCameraMoveListener {
				onCameraMoveAction(
					Location(huaweiMap.cameraPosition.target.latitude, huaweiMap.cameraPosition.target.longitude),
					huaweiMap.cameraPosition.zoom
				)
			}

			// TODO Doesn't work with onMarkerClick
			/*clusterManager = ClusterManager<MarkerClusterItem>(context, googleMap).apply {
				renderer = MarkerClusterRenderer(context, googleMap, this)
				setOnClusterItemClickListener { clusterItem ->
					clusterItem.marker.onClick()
					false
				}
			}
			googleMap.setOnCameraIdleListener(clusterManager)
			googleMap.setOnMarkerClickListener(clusterManager)*/

			huaweiMap.setOnMarkerClickListener { hMarker ->
				val mapMarker = mapMarkersByHMarkerId[hMarker.id]
				if (mapMarker != null) {
					mapMarker.onClick()
				}
				true
			}

			onReady()

		}

	}


	// Markers:

	private val gMarkersByMapMarker = EditableMap<MapMarker, Marker>()

	private val mapMarkersByHMarkerId = EditableMap<String, MapMarker>()

	private var clusterPendingJob : Cancelable? = null

	override fun addMarker (marker: MapMarker) {
		val hMarker = huaweiMap!!.addMarker(
			MarkerOptions().apply {
				position(LatLng(marker.position.latitude, marker.position.longitude))
				icon(BitmapDescriptorFactory.fromBitmap(marker.icon))
				anchor(.5F, .5F)
			}
		)
		gMarkersByMapMarker[marker] = hMarker
		mapMarkersByHMarkerId[hMarker.id] = marker
	}

	override fun removeMarker (marker: MapMarker) {
		val hMarker = gMarkersByMapMarker[marker] ?: return
		gMarkersByMapMarker[marker] = null
		mapMarkersByHMarkerId[hMarker.id] = null
		hMarker.remove()
	}


	// Padding:

	private var leftPx		: Int32 = 0
	private var topPx 		: Int32 = 0
	private var rightPx		: Int32 = 0
	private var bottomPx	: Int32 = 0

	override fun setMapPadding (leftPx: Int32, topPx: Int32, rightPx: Int32, bottomPx: Int32) {
		this.leftPx = leftPx
		this.topPx = topPx
		this.rightPx = rightPx
		this.bottomPx = bottomPx
		huaweiMap?.setPadding(leftPx, topPx, rightPx, bottomPx)
	}


	// Polylines:

	private val polylines = EditableMap<MapPolyline, Polyline>()

	override fun addPolyline (polyline: MapPolyline) {
		val polylineOptions = PolylineOptions()
		polylineOptions.width(polyline.width)
		polylineOptions.color(polyline.color)
		polyline.points.forEach { point -> polylineOptions.add(LatLng(point.latitude, point.longitude)) }
		val gPolyline = huaweiMap!!.addPolyline(polylineOptions)
		polylines[polyline] = gPolyline
	}

	override fun removePolyline (polyline: MapPolyline) {
		val gPolyline = polylines[polyline] ?: return
		gPolyline.remove()
		polylines[polyline] = null
	}


	override fun clear() {
		huaweiMap!!.clear()
		gMarkersByMapMarker.empty()
		mapMarkersByHMarkerId.empty()
		polylines.empty()
	}


	// Camera state:

	override fun moveCamera (position: Location, zoom: Float32, toAnimate: Boolean) {
		if (huaweiMap == null) return
		val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(position.latitude, position.longitude), zoom)
		if (toAnimate) {
			huaweiMap!!.animateCamera(cameraUpdate, 512, null)
		} else {
			huaweiMap!!.moveCamera(cameraUpdate)
		}
	}

	override fun moveCameraToBounds (points: Collection<Location>, toAnimate: Boolean) {
		if (huaweiMap == null) return
		val cameraUpdate = CameraUpdateFactory.newLatLngBounds(
			LatLngBounds.builder().apply { points.breakableForEach { include(LatLng(it.latitude, it.longitude)) } }.build(),
			0
		)
		if (toAnimate) {
			huaweiMap!!.animateCamera(cameraUpdate, 512, null)
		} else {
			huaweiMap!!.moveCamera(cameraUpdate)
		}
	}

	private var onCameraMoveAction : (Location, Float32) -> Unit = { _, _ -> }

	override fun setOnCameraMoveAction(onCameraMoveAction: (Location, Float32) -> Unit) {
		this.onCameraMoveAction = onCameraMoveAction
	}


	override fun close() {
		gMarkersByMapMarker.empty()
		mapMarkersByHMarkerId.empty()
		polylines.empty()
		clusterPendingJob?.cancel()
		clusterPendingJob = null
		huaweiMap = null
		mapView?.onPause()
		mapView?.onStop()
		mapView?.onDestroy()
		mapView = null
	}


}
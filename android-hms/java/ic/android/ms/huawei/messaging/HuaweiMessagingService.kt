package ic.android.ms.huawei.messaging


import ic.struct.map.finite.FiniteMap

import ic.android.ms.messaging.notifyMessageReceived
import ic.android.ms.messaging.notifyMessagingTokenUpdated
import ic.android.util.handler.post

import com.huawei.hms.push.HmsMessageService
import com.huawei.hms.push.RemoteMessage
import ic.struct.map.finite.fromkmap.FiniteMapFromKotlinMap
import ic.util.log.logD
import ic.util.log.logW


class HuaweiMessagingService : HmsMessageService() {


	override fun onNewToken (newToken: String) {

		logD("HuaweiMessagingService") { "onNewToken $newToken" }

		savedHmsInstanceIdToken = newToken

		post {
			notifyMessagingTokenUpdated(newToken)
		}

	}


	override fun onTokenError (error: Exception) {
		logW("HuaweiMessagingService") { error }
	}


	override fun onMessageReceived (remoteMessage: RemoteMessage) {

		val data = FiniteMapFromKotlinMap(remoteMessage.dataOfMap)

		logD("HuaweiMessagingService") { "onMessageReceived $data" }

		post {
			notifyMessageReceived(
				from = remoteMessage.from,
				notificationTitle = remoteMessage.notification?.title,
				notificationBody = remoteMessage.notification?.body,
	 			data = data
			)
		}

	}


}
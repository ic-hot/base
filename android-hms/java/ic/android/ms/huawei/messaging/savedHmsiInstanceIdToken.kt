package ic.android.ms.huawei.messaging


import ic.android.storage.prefs.getPrefStringOrNull
import ic.android.storage.prefs.setPrefString


internal var savedHmsInstanceIdToken : String?

	get() = getPrefStringOrNull("ic.android.ms.huawei", "hmsInstanceId")

	set (value) = setPrefString("ic.android.ms.huawei", "hmsInstanceId", value)

;
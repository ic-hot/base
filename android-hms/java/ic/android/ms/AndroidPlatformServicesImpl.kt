package ic.android.ms


import android.app.Activity
import android.content.Context
import android.os.Bundle

import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.aaid.HmsInstanceId
import com.huawei.hms.analytics.HiAnalytics
import com.huawei.hms.analytics.HiAnalyticsTools
import com.huawei.hms.push.HmsMessaging

import ic.android.app.thisApp
import ic.android.util.handler.post
import ic.android.ms.maps.MapCarrier
import ic.android.ms.messaging.notifyMessagingTokenUpdated
import ic.android.ms.huawei.maps.HuaweiMapCarrier
import ic.android.ms.huawei.messaging.savedHmsInstanceIdToken
import ic.base.annotations.UsedByReflect
import ic.base.reflect.ext.className
import ic.parallel.funs.doInBackground


@UsedByReflect
object AndroidPlatformServicesImpl : AndroidPlatformServices {


	override val type get() = AndroidPlatformServicesType.Huawei


	override fun init() {

		HiAnalyticsTools.enableLog()

		doInBackground {
			val oldToken = savedHmsInstanceIdToken
			val appId = AGConnectServicesConfig.fromContext(thisApp).getString("client/app_id")
			val tokenScope = "HCM"
			val newToken = HmsInstanceId.getInstance(thisApp).getToken(appId, tokenScope)
			if (newToken != oldToken) {
				savedHmsInstanceIdToken = newToken
				post {
					notifyMessagingTokenUpdated(newToken)
				}
			}
		}

	}

	override fun trackEvent(eventName: String, vararg params: Pair<String, String?>) {

		HiAnalytics.getInstance(thisApp).onEvent(
			eventName,
			Bundle().apply {
				params.forEach { (key, value) ->
					if (value != null) putString(key, value)
				}
			}
		)

	}


	override fun trackScreenStart(activity: Activity, screenName: String) {
		HiAnalytics.getInstance(thisApp).pageStart(screenName, activity.className)
	}

	override fun trackScreenEnd(activity: Activity, screenName: String) {
		HiAnalytics.getInstance(thisApp).pageEnd(screenName)
	}


	override val messagingToken get() = savedHmsInstanceIdToken


	override fun subscribeToTopic(topicId: String) {
		HmsMessaging.getInstance(thisApp).subscribe(topicId)
	}

	
	override fun createMapCarrier(context: Context) : MapCarrier {
		return HuaweiMapCarrier()
	}


}
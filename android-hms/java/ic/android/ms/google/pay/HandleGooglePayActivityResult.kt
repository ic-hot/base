package ic.android.ms.google.pay


import android.os.Bundle

import ic.base.throwables.WrongCallException
import ic.util.code.json.JsonObject


/**
 * Stub
 */
inline fun handleGooglePayActivityResult (
	result : Bundle,
	onCanceled : () -> Unit,
	onError : (message: String?) -> Unit,
	onSuccess : (response: JsonObject) -> Unit
) {
	throw WrongCallException.Runtime()
}
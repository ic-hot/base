package ic.android.ms.google.pay


import android.app.Activity

import ic.android.ms.google.pay.invoice.GooglePayInvoice
import ic.android.ui.toast.showToast


/**
 * Stub
 */
fun Activity.startGooglePayActivityForResult (invoice: GooglePayInvoice) {

	showToast("Google Pay is not supported on this device")

}
package ic.base.platform


import ic.app.impl.IcAppEngine
import ic.base.platform.PlatformType.*
import ic.util.analytics.impl.AnalyticsEngine


internal object IcBasePlatform : NonAndroidJvmBasePlatform() {


	override val platformType get() = Jvm.Ic

	override val analyticsEngine: AnalyticsEngine
		get() = TODO("Not yet implemented")


	override val appEngine get() = IcAppEngine


}
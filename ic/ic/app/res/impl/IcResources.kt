package ic.app.res.impl


import ic.app.srcDirectory
import ic.base.kfunctions.ext.getValue
import ic.base.throwables.NotExistsException
import ic.storage.fs.File
import ic.storage.fs.ext.getFolder
import ic.storage.fs.ext.read
import ic.storage.fs.local.ext.getExistingOrThrow
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toString
import ic.struct.value.cached.Cached
import ic.util.text.charset.Charset.Companion.Utf8


object IcResources : Resources {

	private val resDir by Cached {
		srcDirectory.getFolder("res")
	}

	@Throws(NotExistsException::class)
	override fun getFileOrThrow (path: String) : ByteSequence {
		return File.getExistingOrThrow(resDir, path).read()
	}

	override fun getStringOrNull (path: String) : String? {
		try {
			return getFileOrThrow("txt/$path.txt").toString(charset = Utf8)
		} catch (_: NotExistsException) {
			return null
		}
	}

}
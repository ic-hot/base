package ic.app.impl


import ic.app.res.impl.IcResources
import ic.app.runtimeAppPackageName
import ic.app.tier.Tier.Production
import ic.storage.fs.Directory
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.publicHomeDirectory
import ic.storage.fs.local.userHomeDirectory
import ic.storage.prefs.Prefs


internal object IcAppEngine : NonAndroidJvmAppEngine() {


	override fun getTierString() : String? = System.getenv("IC_TIER")


	override fun getDefaultTier() = Production


	override val resources get() = IcResources


	override fun initPrivateFolder() : Directory {
		return userHomeDirectory
		.createFolderIfNotExists(".ic")
		.createFolderIfNotExists("data")
		.createFolderIfNotExists(runtimeAppPackageName)
	}


	override fun initCommonDataDirectory(): Directory {
		return userHomeDirectory
		.createFolderIfNotExists(".ic")
		.createFolderIfNotExists("data")
		.createFolderIfNotExists("common")
	}


	override fun initCommonPublicFolder(): Directory {
		return publicHomeDirectory
		.createFolderIfNotExists(".ic")
		.createFolderIfNotExists("data")
		.createFolderIfNotExists("common")
	}


	override fun createPrefs (prefsName: String) : Prefs {
		throw NotImplementedError()
	}


}
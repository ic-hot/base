package ic.app


import ic.base.kfunctions.ext.getValue
import ic.storage.fs.Directory
import ic.storage.fs.local.ext.getExisting
import ic.struct.value.cached.Cached


internal val distributionDirectory by Cached {

	Directory.getExisting(
		path = System.getenv("IC_DISTRIBUTION_PATH")!!
	)

}
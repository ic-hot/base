package ic.app


import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


internal val runtimeAppPackageName by Cached {

	System.getenv("IC_APP_PACKAGE_NAME")!!

}
package ic.app


import ic.base.kfunctions.ext.getValue
import ic.base.strings.ext.split
import ic.storage.fs.Directory
import ic.storage.fs.local.ext.getExisting
import ic.storage.fs.merge.MergeFolder
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.value.cached.Cached


internal val srcDirectory by Cached {
	MergeFolder(
		children = (
			System.getenv("IC_SRC_DIRS")!!
			.split(separator = ':')
			.copyConvert { relativePath ->
				Directory.getExisting(distributionDirectory, relativePath)
			}
		)
	)
}
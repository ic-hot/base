package ic.base.platform


import ic.network.impl.JvmNetworkEngine
import ic.parallel.impl.JvmParallelEngine
import ic.storage.impl.JvmStorageEngine
import ic.struct.value.ephemeral.impl.JvmEphemeralEngine
import ic.system.impl.JvmSystemEngine
import ic.util.crypto.impl.CryptoEngine
import ic.util.crypto.impl.JvmCryptoEngine
import ic.util.locale.impl.JvmLocaleEngine
import ic.util.text.charset.impl.JvmCharsetsEngine


internal abstract class JvmBasePlatform : BasePlatform() {


	abstract override val platformType : PlatformType.Jvm


	abstract override val parallelEngine : JvmParallelEngine


	override abstract val systemEngine : JvmSystemEngine

	final override val ephemeralEngine get() = JvmEphemeralEngine
	final override val charsetsEngine  get() = JvmCharsetsEngine
	final override val localeEngine    get() = JvmLocaleEngine

	override abstract val networkEngine : JvmNetworkEngine

	final override val storageEngine get() = JvmStorageEngine

	final override val cryptoEngine get() = JvmCryptoEngine


}
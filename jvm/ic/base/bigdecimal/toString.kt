@file:Suppress("NOTHING_TO_INLINE")


package ic.base.bigdecimal


import java.math.BigDecimal
import java.util.Locale

import ic.util.text.numbers.formatNumber


inline fun BigDecimal.toString (

	pattern : String,

	locale : Locale = Locale.getDefault(),

	groupingSeparator : Char = ' '

) : String = formatNumber(

	number = this,

	pattern = pattern,
	locale = locale,
	groupingSeparator = groupingSeparator

)
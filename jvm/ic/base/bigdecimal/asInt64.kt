package ic.base.bigdecimal


import java.math.BigDecimal

import ic.base.primitives.int64.Int64


inline val BigDecimal.asInt64 : Int64 get() = toLong()
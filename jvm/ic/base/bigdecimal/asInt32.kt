package ic.base.bigdecimal


import java.math.BigDecimal

import ic.base.primitives.int32.Int32


inline val BigDecimal.asInt32 : Int32 get() = toInt()
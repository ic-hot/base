package ic.base.bigdecimal.ext


import java.math.BigDecimal
import java.math.RoundingMode


inline val BigDecimal.rounded : BigDecimal get() = setScale(0, RoundingMode.HALF_UP)
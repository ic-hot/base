package ic.base.bigdecimal.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asBigDecimal

import java.math.BigDecimal


@Suppress("NOTHING_TO_INLINE")
inline operator fun BigDecimal.plus (float64: Float64) : BigDecimal {

	return this + float64.asBigDecimal

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.bigdecimal.ext


import java.math.BigDecimal

import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asBigDecimal
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asBigDecimal


inline operator fun BigDecimal.compareTo (float64: Float64) = compareTo(float64.asBigDecimal)

inline operator fun BigDecimal.compareTo (int32: Int32) = compareTo(int32.asBigDecimal)
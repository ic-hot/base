package ic.base.bigdecimal.ext


import java.math.BigDecimal

import ic.base.primitives.float32.Float32


inline val BigDecimal.asFloat32 : Float32 get() = toFloat()
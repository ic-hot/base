package ic.base.bigdecimal.ext


import java.math.BigDecimal

import ic.base.primitives.float64.Float64


inline val BigDecimal.asFloat64 : Float64 get() = toDouble()
package ic.base.bigdecimal.ext


import java.math.BigDecimal

import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asBigDecimal


@Suppress("NOTHING_TO_INLINE")
inline operator fun BigDecimal.times (int32: Int32) : BigDecimal {

	return this * int32.asBigDecimal

}
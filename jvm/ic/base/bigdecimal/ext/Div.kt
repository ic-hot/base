package ic.base.bigdecimal.ext


import java.math.BigDecimal

import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asBigDecimal


@Suppress("NOTHING_TO_INLINE")
inline operator fun BigDecimal.div (int32: Int32) : BigDecimal {

	return this.divide(int32.asBigDecimal)

}
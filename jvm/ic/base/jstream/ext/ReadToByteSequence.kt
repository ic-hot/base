@file:Suppress("NOTHING_TO_INLINE")


package ic.base.jstream.ext


import java.io.InputStream

import ic.stream.input.ext.read


inline fun InputStream.readToByteSequence() = asByteInput.read()
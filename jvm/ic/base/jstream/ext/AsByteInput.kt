package ic.base.jstream.ext


import java.io.InputStream

import ic.stream.input.ByteInput
import ic.stream.input.fromis.ByteInputFromInputStream


inline val InputStream.asByteInput : ByteInput get() = ByteInputFromInputStream(this)
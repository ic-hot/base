package ic.base.jstream.ext


import java.io.OutputStream

import ic.stream.output.ByteOutput
import ic.stream.output.fromos.ByteOutputFromOutputStream


inline val OutputStream.asByteOutput : ByteOutput get() = ByteOutputFromOutputStream(this)
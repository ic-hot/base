@file:Suppress("NOTHING_TO_INLINE")


package ic.base.jstream.ext


import java.io.InputStream

import ic.stream.input.ext.read
import ic.stream.sequence.ext.toByteArray


inline fun InputStream.readToByteArray() : ByteArray = asByteInput.read().toByteArray()
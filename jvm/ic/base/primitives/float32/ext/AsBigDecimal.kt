package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32

import java.math.BigDecimal


inline val Float32.asBigDecimal : BigDecimal get() = toBigDecimal()
package ic.base.primitives.int64.ext


import java.math.BigDecimal

import ic.base.primitives.int64.Int64


val Int64.asBigDecimal : BigDecimal get() = BigDecimal(this)
package ic.base.primitives.int64.ext


import java.math.BigInteger

import ic.base.primitives.int64.Int64



inline val Int64.asBigInteger : BigInteger get() = toBigInteger()
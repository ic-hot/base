package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64

import java.math.BigDecimal


val Float64.asBigDecimalOrNull : BigDecimal? get() {

	if (this.isNaN) {

		return null

	} else {

		try {
			return toBigDecimal()
		} catch (_: Exception) {
			return null
		}

	}

}


inline val Float64.asBigDecimal get() = asBigDecimalOrNull!!
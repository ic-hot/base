package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64
import java.math.BigInteger


inline val Float64.asBigInteger : BigInteger get() = toBigDecimal().toBigInteger()
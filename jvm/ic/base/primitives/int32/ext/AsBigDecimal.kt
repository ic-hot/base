package ic.base.primitives.int32.ext


import java.math.BigDecimal


import ic.base.primitives.int32.Int32


val Int32.asBigDecimal : BigDecimal get() = BigDecimal(this)
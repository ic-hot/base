@file:Suppress("NOTHING_TO_INLINE")


package ic.base.primitives.int32.ext


import java.math.BigDecimal

import ic.base.primitives.int32.Int32


inline operator fun Int32.times (bigDecimal: BigDecimal) : BigDecimal {
	return this.asBigDecimal * bigDecimal
}
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext


import java.math.BigDecimal

import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
inline fun String.parseBigDecimalOrThrowUnableToParse() : BigDecimal {
	return ic.util.text.numbers.parseBigDecimalOrThrowUnableToParse(this)
}


inline fun String.parseBigDecimal() : BigDecimal {
	try {
		return parseBigDecimalOrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("string: $this")
	}
}


inline fun String.parseBigDecimalOrNull() : BigDecimal? {
	try {
		return parseBigDecimalOrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		return null
	}
}



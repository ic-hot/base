package ic.base.arrays.ext


inline val <reified Item> Array<Item>.itemClass : Class<Item> get() = Item::class.java
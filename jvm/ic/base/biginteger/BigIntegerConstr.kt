@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.biginteger


import java.math.BigInteger

import ic.base.primitives.int64.Int64


inline fun BigInteger (int64: Int64) : BigInteger = BigInteger.valueOf(int64)
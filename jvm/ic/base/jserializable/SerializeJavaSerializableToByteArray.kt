package ic.base.jserializable


import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream


fun serializeJavaSerializableToByteArray (javaSerializable: java.io.Serializable) : ByteArray {

	val byteArrayOutputStream = ByteArrayOutputStream()

	val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)

	try {

		objectOutputStream.writeObject(javaSerializable)

		objectOutputStream.flush()

		return byteArrayOutputStream.toByteArray()

	} finally {

		byteArrayOutputStream.close()

	}

}
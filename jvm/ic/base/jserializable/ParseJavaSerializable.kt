package ic.base.jserializable


import java.io.ByteArrayInputStream
import java.io.ObjectInputStream


fun <Value: java.io.Serializable> parseJavaSerializable (byteArray: ByteArray) : Value {

	val byteArrayInputStream = ByteArrayInputStream(byteArray)

	val objectInputStream = ObjectInputStream(byteArrayInputStream)

	try {

		@Suppress("UNCHECKED_CAST")
		return objectInputStream.readObject() as Value

	} finally {

		objectInputStream.close()

	}

}
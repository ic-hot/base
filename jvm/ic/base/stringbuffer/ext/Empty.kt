@file:Suppress("NOTHING_TO_INLINE")


package ic.base.stringbuffer.ext


inline fun StringBuffer.empty() {

	setLength(0)

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.stringbuffer


import ic.base.primitives.character.Character


inline fun StringBuffer.putCharacter (character: Character) {
	append(character)
}
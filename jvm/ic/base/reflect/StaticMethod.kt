@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")


package ic.base.reflect


import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

import ic.base.throwables.AccessException



class StaticMethod

	internal constructor (private val javaMethod : Method)

{

	operator fun <Result> invoke (vararg args: Any?) : Result {

		return try {

			@Suppress("UNCHECKED_CAST")
			javaMethod.invoke(null, *args) as Result

		} catch (e: IllegalAccessException) {
			throw AccessException.Runtime(e)
		} catch (e: InvocationTargetException) 	{
			throw e.targetException
		}

	}

}
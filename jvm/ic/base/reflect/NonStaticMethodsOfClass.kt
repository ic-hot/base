package ic.base.reflect


import java.lang.reflect.Modifier

import ic.base.arrays.ext.toFiniteSet
import ic.base.escape.skip.skip
import ic.struct.collection.ext.count.count
import ic.struct.list.ext.copy.toArray
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.finite.BaseFiniteMap


class NonStaticMethodsOfClass internal constructor (

	private val javaClass: Class<*>

) : BaseFiniteMap<MethodSignature, NonStaticMethodOfClass>() {

	override val keys get() = javaClass.methods.toFiniteSet { method ->
		if (Modifier.isStatic(method.modifiers)) skip
		MethodSignature(
			name = method.name,
			argClasses = ListFromArray(method.parameterTypes, isArrayImmutable = true)
		)
	}

	override val count get() = keys.count

	override fun get (key: MethodSignature) : NonStaticMethodOfClass? {
		return try {
			NonStaticMethodOfClass(
				javaMethod = javaClass.getMethod(
					key.name,
					*key.argClasses.toArray()
				)
			)
		} catch (e: NoSuchMethodException) { null }
	}

}
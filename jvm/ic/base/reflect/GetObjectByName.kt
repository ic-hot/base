@file:Suppress("NOTHING_TO_INLINE")


package ic.base.reflect


import ic.base.throwables.NotExistsException


fun <Type: Any> getObjectByNameOrNull (className : String) : Type? {
	val objectClass = getClassByNameOrNull<Type>(className) ?: return null
	@Suppress("UNCHECKED_CAST")
	return objectClass.staticFields["INSTANCE"]?.get() as Type?
}


@Throws(NotExistsException::class)
inline fun <Type: Any> getObjectByNameOrThrowNotExists (className : String) : Type {
	return getObjectByNameOrNull(className) ?: throw NotExistsException
}


inline fun <Type: Any> getObjectByName (className : String) : Type {
	return getObjectByNameOrNull(className) ?: throw RuntimeException("className: $className")
}
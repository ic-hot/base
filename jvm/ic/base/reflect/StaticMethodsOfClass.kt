@file:Suppress("NOTHING_TO_INLINE")


package ic.base.reflect


import java.lang.reflect.Modifier.*

import ic.base.arrays.ext.copy.convert.copyConvertToFiniteSet
import ic.base.escape.skip.skip
import ic.base.reflect.ext.getStaticMethodOrNull
import ic.struct.collection.ext.count.count
import ic.struct.list.List
import ic.struct.list.ext.copy.toArray
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.finite.BaseFiniteMap
import ic.struct.set.finite.FiniteSet


class StaticMethodsOfClass

	internal constructor (
		private val javaClass: Class<*>
	)

	: BaseFiniteMap<MethodSignature, StaticMethod>()

{


	override val keys : FiniteSet<MethodSignature> get() = javaClass.methods.copyConvertToFiniteSet { javaMethod ->
		if (!isStatic(javaMethod.modifiers)) skip
		MethodSignature(
			name = javaMethod.name,
			argClasses = ListFromArray(javaMethod.parameterTypes, isArrayImmutable = true)
		)
	}

	override val count get() = keys.count


	operator fun get (name: String, vararg argClasses: Class<*>) : StaticMethod? {
		return javaClass.getStaticMethodOrNull(name, *argClasses)
	}


	inline operator fun get (name: String, argClasses: List<Class<*>>) = get(name, *argClasses.toArray())

	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun get (methodSignature: MethodSignature) : StaticMethod? {
		return get(methodSignature.name, methodSignature.argClasses)
	}


}
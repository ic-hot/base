package ic.base.reflect


import ic.base.throwables.NotExistsException


fun <Type: Any> getClassByNameOrNull (className : String) : Class<Type>? {
	return try {
		@Suppress("UNCHECKED_CAST")
		Class.forName(className) as Class<Type>
	} catch (e: ClassNotFoundException) { null }
}


@Throws(NotExistsException::class)
fun <Type: Any> getClassByNameOrThrowNotExists (className : String) : Class<Type> {
	return getClassByNameOrNull(className) ?: throw NotExistsException
}


fun <Type: Any> getClassByName (className: String) : Class<Type> {
	return getClassByNameOrNull(className) ?: throw RuntimeException("className: $className")
}



package ic.base.reflect


import ic.ifaces.gettersetter.gettersetter1.GetterSetter1

import java.lang.reflect.Field


class NonStaticField internal constructor (

	private val javaField : Field

) : GetterSetter1<Any?, Any?> {


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun get (obj: Any?) : Any? {
		return javaField.get(obj)
	}


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	operator override fun set (obj: Any?, value: Any?) {
		javaField.set(obj, value)
	}


}
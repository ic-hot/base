package ic.base.reflect


import java.lang.reflect.Field
import java.lang.reflect.Modifier

import ic.base.throwables.Skip
import ic.struct.collection.convert.ConvertCollection
import ic.struct.collection.ext.copy.copyToFiniteSet
import ic.struct.collection.ext.count.count
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.finite.BaseFiniteMap


class StaticFields internal constructor (

	private val javaClass: Class<*>

) : BaseFiniteMap<String, StaticField>() {


	override val keys get() = (
		ConvertCollection<String, Field>(
			ListFromArray(javaClass.declaredFields, isArrayImmutable = true)
		) { field ->
			if (!Modifier.isStatic(field.modifiers)) throw Skip
			field.name
		}.copyToFiniteSet()
	)

	override val count get() = keys.count


	override fun get (key: String) : StaticField? {
		return try {
			StaticField(javaClass.getField(key))
		} catch (e: NoSuchFieldException) { null }
	}


}
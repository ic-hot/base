package ic.base.reflect


import ic.ifaces.gettersetter.GetterSetter

import java.lang.reflect.Field



class StaticField internal constructor (private val javaField: Field) : GetterSetter<Any?> {


	override fun get() : Any? {
		return javaField.get(null)
	}


	override fun set (value: Any?) {
		javaField.set(null, value)
	}


}
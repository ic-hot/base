@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")


package ic.base.reflect


import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

import ic.base.throwables.AccessException
import ic.struct.list.List
import ic.struct.list.ext.copy.toUntypedArray


class NonStaticMethodOfClass internal constructor (private val javaMethod : Method) {


	operator fun <Result> invoke (receiver: Any, vararg args: Any?) : Result {
		@Suppress("UNCHECKED_CAST")
		return javaMethod.invoke(receiver, *args) as Result
	}


	operator fun <Result> invoke (receiver: Any, args: List<Any?>) : Result {
		return try {
			invoke(receiver, *args.toUntypedArray())
		} catch (e: IllegalAccessException) 	{ throw AccessException.Runtime(e)
		} catch (e: InvocationTargetException) 	{ throw e.targetException }
	}


}
@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")


package ic.base.reflect


val Class<*>.nonStaticMethods 	get() = NonStaticMethodsOfClass(this)

val Class<*>.staticMethods 		get() = StaticMethodsOfClass(this)

val Any.methods get() = MethodsOfObject(this)
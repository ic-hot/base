@file:Suppress("NOTHING_TO_INLINE")


package ic.base.reflect.ext


import ic.base.throwables.AccessException
import ic.base.throwables.NotExistsException
import ic.base.throwables.WrongValueException


@Throws(NotExistsException::class)
fun <Receiver, Value> Class<Receiver>.getStaticFieldValueOrThrowNotExists (

	staticFieldName : String

) : Value {

	val staticField = try {
		getField(staticFieldName)
	} catch (t: NoSuchFieldException) {
		throw NotExistsException
	} catch (t: SecurityException) {
		throw AccessException.Runtime(
			"Field $name.$staticFieldName is not visible"
		)
	}

	try {
		@Suppress("UNCHECKED_CAST")
		return staticField.get(null) as Value
	} catch (t: IllegalArgumentException) {
		throw WrongValueException.Runtime(
			"Field $name.$staticFieldName is not static"
		)
	} catch (t: IllegalAccessException) {
		throw AccessException.Runtime(
			"Field $name.$staticFieldName is not visible"
		)
	}

}


inline fun <Receiver, Value> Class<Receiver>.getStaticFieldValue (
	staticFieldName : String
) : Value {
	try {
		return getStaticFieldValueOrThrowNotExists(staticFieldName)
	} catch (t: NotExistsException) {
		throw RuntimeException(
			"No such field $name.$staticFieldName"
		)
	}
}
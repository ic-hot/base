package ic.base.reflect.ext


import ic.base.reflect.MethodSignature
import ic.base.reflect.StaticMethod
import ic.struct.list.ext.copy.toArray


fun Class<*>.getStaticMethodOrNull (
	name : String,
	vararg argsClasses : Class<*>
) : StaticMethod? {
	try {
		return StaticMethod(
			javaMethod = getMethod(name, *argsClasses)
		)
	} catch (e: NoSuchMethodException) {
		return null
	}
}

fun Class<*>.getStaticMethod (
	name : String,
	vararg argsClasses : Class<*>
) : StaticMethod {
	return getStaticMethodOrNull(name, *argsClasses)!!
}

fun Class<*>.getStaticMethodOrNull(methodSignature : MethodSignature) : StaticMethod? {
	return getStaticMethodOrNull(
		name = methodSignature.name,
		argsClasses = methodSignature.argClasses.toArray()
	)
}
package ic.base.reflect


import java.lang.reflect.InvocationTargetException

import ic.struct.list.List
import ic.struct.list.ext.copy.toArray
import ic.base.throwables.NotExistsException


@Throws(NotExistsException::class)
fun <Type> Class<Type>.instantiateOrThrowNotExists (argClasses: List<Class<*>>, args: List<Any?>) : Type {
	try {
		return getConstructor(*argClasses.toArray()).newInstance(*args.toArray())
	} catch (e: NoSuchMethodException) 		{ throw NotExistsException
	} catch (e: InstantiationException) 	{ throw NotExistsException
	} catch (e: InvocationTargetException) 	{ throw e.targetException
	} catch (e: Throwable) {
		throw RuntimeException(
			"Can't instantiate $name with args ($argClasses) $args",
			e
		)
	}
}


fun <Type> Class<Type>.instantiate (
	argClasses : List<Class<*>>,
	args : List<Any?>
) : Type {
	try {
		return instantiateOrThrowNotExists(argClasses, args)
	} catch (noSuchConstructor: NotExistsException) {
		throw RuntimeException("${ this.name }: No such constructor ($argClasses) $args")
	}
}


fun <Type> Class<Type>.instantiate() : Type {
	return instantiate(argClasses = List(), args = List())
}
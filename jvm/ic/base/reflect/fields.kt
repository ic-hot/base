package ic.base.reflect


val Class<*>.nonStaticFields 	get() = NonStaticFields(this)
val Class<*>.staticFields 		get() = StaticFields(this)
package ic.base.reflect


import java.lang.reflect.Modifier

import ic.base.arrays.ext.toFiniteSet
import ic.base.throwables.Skip
import ic.struct.collection.ext.count.count
import ic.struct.list.List
import ic.struct.list.ext.copy.toArray
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.finite.BaseFiniteMap


class MethodsOfObject internal constructor (

	private val receiver : Any

) : BaseFiniteMap<MethodSignature, MethodOfObject>() {

	override val keys get() = receiver.javaClass.methods.toFiniteSet { method ->
		if (Modifier.isStatic(method.modifiers)) throw Skip
		MethodSignature(
			name = method.name,
			argClasses = ListFromArray(method.parameterTypes, isArrayImmutable = true)
		)
	}

	override val count get() = keys.count

	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun get (signature: MethodSignature): MethodOfObject? {
		return try {
			MethodOfObject(
				receiver = receiver,
				signature = signature,
				javaMethod = receiver.javaClass.getMethod(
					signature.name,
					*signature.argClasses.toArray()
				)
			)
		} catch (e: NoSuchMethodException) { null }
	}

	operator fun get (methodName: String, argClasses: List<Class<*>>) : MethodOfObject? {
		return get(
			MethodSignature(
				name = methodName,
				argClasses = argClasses
			)
		)
	}

}
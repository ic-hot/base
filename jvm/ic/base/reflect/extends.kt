package ic.base.reflect


import kotlin.reflect.KClass


infix fun Class<*>?.extends (otherClass: Class<*>) : Boolean {

	if (this == null) return false

	return otherClass.isAssignableFrom(this)

}


infix fun Class<*>?.extends (otherClass: KClass<*>) : Boolean {

	return this extends otherClass.java

}
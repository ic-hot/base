package ic.base.reflect


@Suppress("NOTHING_TO_INLINE")
inline infix fun Any?.isInstanceOf (type: Class<*>) : Boolean {

	if (this == null) return false

	return type.isInstance(this)

}
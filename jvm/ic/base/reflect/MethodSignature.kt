package ic.base.reflect


import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.ext.concat
import ic.struct.list.ext.copy.convert.copyConvert


class MethodSignature

	internal constructor (

		val name : String,

		val argClasses : List<Class<*>>

	)

{

	override fun equals (other: Any?) : Boolean {
		if (other is MethodSignature) {
			if (other.name != name) return false
			if (other.argClasses != argClasses) return false
			return true
		} else {
			return false
		}
	}

	override fun hashCode() : Int32 {
		return name.hashCode() + argClasses.hashCode()
	}

	override fun toString() = "$name(${ 
		argClasses.copyConvert { it.name }.concat(separator = ", ") 
	})"

}
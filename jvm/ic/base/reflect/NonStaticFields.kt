package ic.base.reflect


import java.lang.reflect.Modifier

import ic.struct.collection.ext.count.count
import ic.struct.map.finite.BaseFiniteMap
import ic.struct.set.editable.EditableSet


class NonStaticFields
	internal constructor (private val javaClass: Class<*>)
	: BaseFiniteMap<String, NonStaticField>()
{


	override val keys = EditableSet<String>().apply {
		javaClass.declaredFields.forEach {
			if (!Modifier.isStatic(it.modifiers)) {
				addIfNotExists(it.name)
			}
		}
	}

	override val count get() = keys.count


	override fun get (key: String) : NonStaticField? {
		try {
			return NonStaticField(
				javaClass.getField(key)
			)
		} catch (e: NoSuchMethodException) {
			return null
		}
	}


}
package ic.base.reflect


import java.lang.reflect.InvocationTargetException

import ic.base.reflect.ext.className
import ic.base.throwables.AccessException
import ic.base.throwables.NotSupportedException
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.fromarray.ListFromArray


class MethodOfObject

	internal constructor (

		val receiver: Any,

		private val javaMethod : java.lang.reflect.Method,

		val signature: MethodSignature

	)

{

	operator fun <Result> invoke (vararg args: Any?) : Result {

		try {

			@Suppress("UNCHECKED_CAST")
			return javaMethod.invoke(receiver, *args) as Result

		} catch (e: IllegalAccessException) { throw AccessException.Runtime(e)
		} catch (e: InvocationTargetException) { throw e.targetException
		} catch (e: IllegalArgumentException) {

			throw NotSupportedException.Runtime(
				"methodName: ${ signature.name }, " +
				"expectedArgClasses: ${ signature.argClasses.copyConvert { it.name } }, " +
				"actualArgClasses: ${ ListFromArray<Any?>(args, isArrayImmutable = true).copyConvert { it.className } }"
			)

		}

	}

}
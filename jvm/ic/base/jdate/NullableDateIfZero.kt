@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.jdate


import java.util.Date

import ic.base.primitives.int64.Int64


inline fun NullableDateIfZero (epochMs : Int64?) : Date? {

	if (epochMs == null) return null

	if (epochMs == Int64(0)) return null

	return Date(epochMs)

}
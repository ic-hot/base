package ic.base.jdate


import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar


fun areSameMonth (a: Date, b: Date) : Boolean {

	val aCalendar = GregorianCalendar()
	aCalendar.time = a

	val bCalendar = GregorianCalendar()
	aCalendar.time = b

	if (aCalendar[Calendar.YEAR] != bCalendar[Calendar.YEAR]) return false

	return aCalendar[Calendar.MONTH] == bCalendar[Calendar.MONTH]

}
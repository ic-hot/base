package ic.base.jdate


import java.util.Date

import ic.base.primitives.int64.Int64


inline val Date?.epochMsOrZero : Int64 get() {

	if (this == null) return 0

	return time

}
package ic.base.jdate


import java.util.Date

import ic.util.time.Time


inline val Date.asTime : Time get() = Time(epochMs = time)
package ic.util.locale.impl


import ic.util.locale.Locale


class LocaleFromJavaLocale (internal val javaLocale: java.util.Locale) : Locale {

	override val languageCode : String get() = javaLocale.language

}
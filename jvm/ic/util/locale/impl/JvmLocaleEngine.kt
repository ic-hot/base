package ic.util.locale.impl


import java.util.Calendar
import java.util.GregorianCalendar

import ic.base.jdate.asTime
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asBigDecimalOrNull
import ic.base.primitives.int32.Int32
import ic.util.locale.Locale
import ic.util.text.numbers.formatNumber
import ic.util.text.time.jdate.formatDate
import ic.util.text.time.jdate.parseDateOrThrowUnableToParse
import ic.util.time.Time
import ic.util.time.ext.asJavaDate


object JvmLocaleEngine : LocaleEngine {

	override fun getDefaultLocale() = LocaleFromJavaLocale(java.util.Locale.getDefault())

	override fun getLocale (languageCode: String, countryCode: String) = LocaleFromJavaLocale(
		java.util.Locale(languageCode, countryCode)
	)

	override fun formatFloat64 (
		float64 : Float64,
		pattern : String,
		locale : Locale,
		groupingSeparator : Char
	) : String {
		val bigDecimal = float64.asBigDecimalOrNull
		if (bigDecimal == null) return float64.toString()
		return formatNumber(
			number = bigDecimal,
			pattern = pattern,
			locale = locale,
			groupingSeparator = groupingSeparator
		)
	}

	override fun formatTime(
		time : Time,
		pattern : String,
		locale : Locale,
		toUseStandaloneMonth : Boolean
	) = formatDate(
		date = time.asJavaDate,
		pattern = pattern,
		locale = locale,
		toUseStandaloneMonth = toUseStandaloneMonth
	)

	override fun parseTimeOrThrowUnableToParse (
		string : String,
		pattern : String,
		locale : Locale
	) = parseDateOrThrowUnableToParse(
		string = string,
		pattern = pattern,
		locale = locale
	).asTime


	override fun getYear (time: Time, locale: Locale) : Int32 {
		val calendar = GregorianCalendar()
		calendar.timeInMillis = time.epochMs
		return calendar[Calendar.YEAR]
	}

	override fun getMonth (time: Time, locale: Locale) : Int32 {
		val calendar = GregorianCalendar()
		calendar.timeInMillis = time.epochMs
		return calendar[Calendar.MONTH]
	}

	override fun getDayOfMonth (time: Time, locale: Locale) : Int32 {
		val calendar = GregorianCalendar()
		calendar.timeInMillis = time.epochMs
		return calendar[Calendar.DAY_OF_MONTH]
	}

}
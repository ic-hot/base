package ic.util.locale.ext


import ic.util.locale.Locale
import ic.util.locale.impl.LocaleFromJavaLocale


val Locale.asJavaLocale get() = (this as LocaleFromJavaLocale).javaLocale
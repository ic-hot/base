package ic.util.time.funs


import ic.util.time.ext.hourOfDay


val isNight : Boolean get() {
	return now.hourOfDay !in 6 until 18
}
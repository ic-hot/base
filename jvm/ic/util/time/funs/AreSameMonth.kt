@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.funs


import java.util.*

import ic.util.time.Time


fun areSameMonth (a: Time, b: Time) : Boolean {

	val aCalendar = GregorianCalendar()
	aCalendar.timeInMillis = a.epochMs

	val bCalendar = GregorianCalendar()
	aCalendar.timeInMillis = b.epochMs

	if (aCalendar[Calendar.YEAR] != bCalendar[Calendar.YEAR]) return false

	return aCalendar[Calendar.MONTH] == bCalendar[Calendar.MONTH]

}
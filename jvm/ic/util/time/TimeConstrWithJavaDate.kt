@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time


import java.util.Date


inline fun Time (javaDate: Date) = Time(epochMs = javaDate.time)
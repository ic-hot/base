package ic.util.time.impl


import java.util.GregorianCalendar


@JvmField
internal val gregorianCalendar = GregorianCalendar()
package ic.util.time.impl


import java.util.Calendar.*

import ic.base.primitives.int64.Int64


fun calendarValuesToEpochMs (

	year 		: Int = 0,
	month 		: Int = 0,
	dayOfMonth 	: Int = 0,
	hourOfDay 	: Int = 0,
	minute 		: Int = 0,
	second 		: Int = 0,
	millisecond : Int = 0

) : Int64 {

	synchronized (gregorianCalendar) {

		gregorianCalendar[YEAR] 		= year
		gregorianCalendar[MONTH] 		= month
		gregorianCalendar[DAY_OF_MONTH] = dayOfMonth
		gregorianCalendar[HOUR_OF_DAY]	= hourOfDay
		gregorianCalendar[MINUTE]		= minute
		gregorianCalendar[SECOND]		= second
		gregorianCalendar[MILLISECOND]	= millisecond

		return gregorianCalendar.timeInMillis

	}

}

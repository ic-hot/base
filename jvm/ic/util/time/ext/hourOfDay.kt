package ic.util.time.ext


import ic.util.time.Time
import ic.util.time.impl.gregorianCalendar
import java.util.Calendar.HOUR_OF_DAY


val Time.hourOfDay : Int get() {

	synchronized (gregorianCalendar) {
		gregorianCalendar.timeInMillis = epochMs
		return gregorianCalendar[HOUR_OF_DAY]
	}

}
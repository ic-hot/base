package ic.util.time.ext


import ic.util.time.Time
import ic.util.time.impl.gregorianCalendar
import java.util.Calendar.MILLISECOND


val Time.millisecond : Int get() {

	synchronized (gregorianCalendar) {
		gregorianCalendar.timeInMillis = epochMs
		return gregorianCalendar[MILLISECOND]
	}

}
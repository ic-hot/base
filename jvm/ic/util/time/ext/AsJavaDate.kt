package ic.util.time.ext


import java.util.Date

import ic.util.time.Time


inline val Time.asJavaDate get() = Date(epochMs)
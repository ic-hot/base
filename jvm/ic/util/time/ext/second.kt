package ic.util.time.ext


import ic.util.time.Time
import ic.util.time.impl.gregorianCalendar
import java.util.Calendar.SECOND


val Time.second : Int get() {

	synchronized (gregorianCalendar) {
		gregorianCalendar.timeInMillis = epochMs
		return gregorianCalendar[SECOND]
	}

}
package ic.util.time.ext


import java.util.Calendar.*
import java.util.GregorianCalendar
import java.util.Locale

import ic.util.time.Time


fun Time.getMonthStart (locale: Locale) : Time {

	val calendar = GregorianCalendar(locale)

	calendar.timeInMillis = epochMs

	calendar[DAY_OF_MONTH] 	= 1
	calendar[HOUR_OF_DAY] 	= 0
	calendar[MINUTE] 		= 0
	calendar[SECOND] 		= 0
	calendar[MILLISECOND]	= 0

	return Time(epochMs = calendar.timeInMillis)

}
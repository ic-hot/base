package ic.util.time


import ic.util.time.ext.dayOfMonth
import ic.util.time.ext.hourOfDay
import ic.util.time.ext.millisecond
import ic.util.time.ext.minute
import ic.util.time.ext.month
import ic.util.time.ext.second
import ic.util.time.ext.year
import ic.util.time.impl.calendarValuesToEpochMs


@Suppress("NOTHING_TO_INLINE")
inline fun Time (

	initialTime : Time = Time.EpochStart,

	year 		: Int = initialTime.year,
	month 		: Int = initialTime.month,
	dayOfMonth 	: Int = initialTime.dayOfMonth,
	hourOfDay 	: Int = initialTime.hourOfDay,
	minute 		: Int = initialTime.minute,
	second 		: Int = initialTime.second,
	millisecond : Int = initialTime.millisecond

) : Time {

	return Time(

		calendarValuesToEpochMs(

			year = year,
			month = month,
			dayOfMonth = dayOfMonth,
			hourOfDay = hourOfDay,
			minute = minute,
			second = second,
			millisecond = millisecond

		)

	)

}
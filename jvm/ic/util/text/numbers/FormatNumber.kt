package ic.util.text.numbers


import ic.util.locale.Locale
import ic.util.locale.currentLocale
import ic.util.locale.ext.asJavaLocale

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols


fun formatNumber (

	number : BigDecimal,

	pattern : String,

	locale : java.util.Locale,

	groupingSeparator : Char = ' '

) : String {

	val decimalFormatSymbols = DecimalFormatSymbols(locale)
	decimalFormatSymbols.groupingSeparator = groupingSeparator

	val decimalFormat = DecimalFormat(pattern, decimalFormatSymbols)

	return decimalFormat.format(number)

}


fun formatNumber (

	number : BigDecimal,

	pattern : String,

	locale : Locale = currentLocale,

	groupingSeparator : Char = ' '

) = formatNumber(
	number = number,
	pattern = pattern,
	locale = locale.asJavaLocale,
	groupingSeparator = groupingSeparator
)
package ic.util.text.numbers


import java.math.BigDecimal

import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun parseBigDecimalOrThrowUnableToParse (string: String) : BigDecimal {

	try {

		return BigDecimal(string)

	} catch (numberFormatException: NumberFormatException) {

		throw UnableToParseException()

	}

}
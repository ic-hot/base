package ic.util.text.base64


fun base64StringToByteArray (base64String: String) : ByteArray {

	return java.util.Base64.getDecoder().decode(base64String)

}
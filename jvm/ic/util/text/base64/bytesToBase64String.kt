@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.base64


inline fun bytesToBase64String (byteArray: ByteArray) : String {

	return java.util.Base64.getEncoder().encodeToString(byteArray)

}
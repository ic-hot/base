@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.base64


import ic.text.Text


inline fun base64TextToByteArray (base64Text: Text) : ByteArray {

	return base64StringToByteArray(base64Text.toString())

}
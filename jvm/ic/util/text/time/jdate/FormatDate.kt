@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.time.jdate


import java.util.Date

import ic.util.locale.Locale
import ic.util.locale.ext.asJavaLocale


inline fun formatDate (

	date : Date,
	pattern : String,
	locale : Locale = Locale.current,
	toUseStandaloneMonth : Boolean = false

) : String {

	return formatDate(
		date = date,
		pattern = pattern,
		locale = locale.asJavaLocale,
		toUseStandaloneMonth = toUseStandaloneMonth
	)

}
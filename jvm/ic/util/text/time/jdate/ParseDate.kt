package ic.util.text.time.jdate


import ic.base.throwables.UnableToParseException
import ic.util.locale.Locale
import ic.util.locale.currentLocale
import ic.util.locale.ext.asJavaLocale
import java.util.Date


@Throws(UnableToParseException::class)
fun parseDateOrThrowUnableToParse (

	string : String,

	pattern : String,
	locale : Locale = currentLocale

) : Date {

	return parseDateOrThrowUnableToParse(
		string = string,
		pattern = pattern,
		locale = locale.asJavaLocale
	)

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.time.jdate


import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun parseDateOrThrowUnableToParse (

	string : String,

	pattern : String,
	locale : Locale = Locale.getDefault()

) : Date {

	try {
		return SimpleDateFormat(pattern, locale).parse(string)!!
	} catch (parseException: ParseException) {
		throw UnableToParseException()
	}

}


fun parseDate (

	dateString : String,

	pattern : String,
	locale : Locale = Locale.getDefault()

) : Date {

	try {

		return parseDateOrThrowUnableToParse(
			string = dateString,
			pattern = pattern,
			locale = locale
		)

	} catch (t: UnableToParseException) { throw UnableToParseException.Runtime(t) }

}


@JvmName("parseNullableDate")
inline fun parseDate (

	dateString : String?,

	pattern : String,
	locale : Locale = Locale.getDefault()

) : Date? {

	if (dateString == null) return null

	return parseDate(dateString = dateString, pattern = pattern, locale = locale)

}
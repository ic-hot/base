package ic.util.text.time.jdate


import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


fun formatDate (

	date: Date,

	pattern: String,
	locale: Locale = Locale.getDefault(),
	toUseStandaloneMonth : Boolean = false

) : String {

	val simpleDateFormat = SimpleDateFormat(pattern, locale)

	if (toUseStandaloneMonth) {
		simpleDateFormat.dateFormatSymbols = simpleDateFormat.dateFormatSymbols.apply {
			when (locale.language) {
				"uk" -> months = arrayOf(
					"Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень",
					"Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"
				)
				"ru" -> months = arrayOf(
					"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
					"Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
				)
			}
		}
	}

	return simpleDateFormat.format(date)

}
package ic.util.text.charset.impl


import ic.base.throwables.UnableToParseException
import ic.util.text.charset.Charset

import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder


internal object JvmCharsetsEngine : CharsetsEngine {

	override fun stringToByteArray (string: String, charset: Charset) : ByteArray {
		return string.toByteArray(
			kotlin.text.charset(charset.jvmName)
		)
	}

	override fun byteArrayToString (byteArray: ByteArray, charset: Charset): String {
		return byteArray.toString(
			kotlin.text.charset(charset.jvmName)
		)
	}

	override fun encodeUrl (string: String, charset: Charset) : String {
		return try {
			URLEncoder.encode(string, charset.jvmName)
		} catch (e: UnsupportedEncodingException) { throw Error(e) }
	}

	@Throws(UnableToParseException::class)
	override fun decodeUrlOrThrowUnableToParse (string: String, charset: Charset) : String {
		try {
			return URLDecoder.decode(string, charset.jvmName)
		} catch (_: Exception) {
			throw UnableToParseException()
		}
	}

}
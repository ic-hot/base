package ic.util.text.distance


import ic.base.primitives.float32.ext.toString
import ic.util.locale.Locale
import ic.util.locale.currentLocale


fun formatDistance (

	distanceM : Float,

	locale : Locale = currentLocale,

	unitSystem : DistanceUnitSystem = DistanceUnitSystem.Metric

) = when (unitSystem) {

	is DistanceUnitSystem.Metric -> when {

		distanceM < 1000 -> "${ distanceM.toInt() } ${
			when (locale.languageCode) {
				"uk" -> "м"
				"ru" -> "м"
				else -> "m"
			}
		}"

		else -> "${ 
			(distanceM / 1000).toString(pattern = "##0.#", locale = locale) 
		} ${
			when (locale.languageCode) {
				"uk" -> "км"
				"ru" -> "км"
				else -> "km"
			}
		}"

	}

	is DistanceUnitSystem.Imperial -> {

		"${ (distanceM / 1000).toString(pattern = "##0.#", locale = locale) } mi"

	}

}

package ic.util.text.uuid


import java.util.UUID

import ic.base.strings.ext.toByteArray
import ic.math.rand.getRandomByteArray
import ic.util.text.charset.Charset.Companion.Utf8


fun generateUuid (seed: ByteArray = getRandomByteArray(16)) : String {

	return UUID.nameUUIDFromBytes(seed).toString()

}


fun generateUuid (seed: String) = generateUuid(seed = seed.toByteArray(charset = Utf8))
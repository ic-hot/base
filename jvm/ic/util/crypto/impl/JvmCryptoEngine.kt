package ic.util.crypto.impl


import java.security.MessageDigest

import ic.base.assert.assert
import ic.base.arrays.ext.length


internal object JvmCryptoEngine : CryptoEngine() {


	override fun sha256 (data: ByteArray) : ByteArray {
		return MessageDigest.getInstance("SHA-256").digest(data).also {
			assert { it.length == 256 / 8 }
		}
	}


}
package ic.util.hash


import java.security.MessageDigest


fun md5 (data: ByteArray) : ByteArray {

	return MessageDigest.getInstance("MD5").digest(data)

}
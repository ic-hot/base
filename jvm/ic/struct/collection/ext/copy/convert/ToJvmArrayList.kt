package ic.struct.collection.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach


inline fun <Item, NewItem> Collection<Item>.copyConvertToJvmArrayList (

	@Skippable crossinline convertItem : (Item) -> NewItem

) : kotlin.collections.ArrayList<NewItem> {

	val result = ArrayList<NewItem>()

	breakableForEach { item ->

		skippable {

			result.add(
				convertItem(item)
			)

		}

	}

	return result

}
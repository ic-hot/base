package ic.struct.collection.ext.reduce.sum


import java.math.BigDecimal

import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach


inline fun <Item> Collection<Item>.sumByBigDecimal (

	crossinline itemToBigDecimal : (Item) -> BigDecimal

) : BigDecimal {
	var sum : BigDecimal = BigDecimal.ZERO
	breakableForEach { item ->
		sum += itemToBigDecimal(item)
	}
	return sum
}
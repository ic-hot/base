@file:OptIn(ExperimentalTypeInference::class)


package ic.struct.list.ext.reduce.sum


import kotlin.experimental.ExperimentalTypeInference

import java.math.BigDecimal

import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.sumOf (

	itemToBigDecimal : (Item) -> BigDecimal

) : BigDecimal {
	var sum : BigDecimal = BigDecimal.ZERO
	forEach { item ->
		sum += itemToBigDecimal(item)
	}
	return sum
}
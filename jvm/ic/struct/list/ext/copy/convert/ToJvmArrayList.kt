package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


inline fun <Item, NewItem> List<Item>.copyConvertToJvmArrayList (

	@Skippable convertItem : (Item) -> NewItem

) : kotlin.collections.ArrayList<NewItem> {

	val result = ArrayList<NewItem>()

	breakableForEach { item ->

		skippable {

			result.add(
				convertItem(item)
			)

		}

	}

	return result

}
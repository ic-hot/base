package ic.struct.value.ephemeral.impl


import ic.struct.value.ephemeral.Ephemeral
import java.lang.ref.SoftReference


@Suppress("EqualsOrHashCode")
class JvmSoftReference<Value: Any> (

	value : Value

) : SoftReference<Value>(value), Ephemeral<Value> {


	override val value get() = get()

	override val valueOrNull get() = get()


	private val hashCode = value.hashCode()

	override fun hashCode() = hashCode


}
package ic.struct.value.ephemeral.impl


import ic.struct.value.ephemeral.Ephemeral


object JvmEphemeralEngine : EphemeralEngine {

	override fun <Value : Any> createSoftReference (value: Value) : Ephemeral<Value> {
		return JvmSoftReference(value)
	}

	override fun <Value : Any> createWeakReference (value: Value) : Ephemeral<Value> {
		return JvmWeakReference(value)
	}

}
package ic.struct.value.ephemeral.impl


import ic.struct.value.ephemeral.Ephemeral
import java.lang.ref.WeakReference


@Suppress("EqualsOrHashCode")
class JvmWeakReference<Value: Any> (

	value : Value

) : WeakReference<Value>(value), Ephemeral<Value> {


	override val value get() = get()

	override val valueOrNull get() = get()


	private val hashCode = value.hashCode()

	override fun hashCode() = hashCode


}
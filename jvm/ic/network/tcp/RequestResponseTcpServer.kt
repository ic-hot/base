package ic.network.tcp


import ic.base.throwables.IoException
import ic.network.socket.SocketAddress
import ic.parallel.funs.sleep
import ic.stream.input.ByteInput
import ic.stream.input.cache.CacheByteInput
import ic.stream.sequence.ByteSequence
import ic.util.text.charset.Charset
import ic.util.log.log
import ic.util.text.charset.ext.bytesToString


abstract class RequestResponseTcpServer : TcpServer() {


	protected abstract fun handleThrowable (request: ByteSequence, throwable: Throwable) : ByteSequence?

	@Throws(IncompleteRequestException::class)
	protected abstract fun getResponse (socketAddress: SocketAddress, input: ByteInput) : ByteSequence


	@Throws(IoException.Runtime::class)
	override fun handleConnection (connection: TcpConnection) {
		try {
			for (i in 0..64) {
				sleep(64)
				val requestCache = CacheByteInput(connection.input)
				var response: ByteSequence?
				try {
					response = getResponse(connection.address, requestCache.newIterator())
					requestCache.close()
					synchronized(logger) {
						logger.log(successLogLevel, "SocketRRServer") {
							"request: ${ Charset.defaultHttp.bytesToString(requestCache) }"
						}
						logger.log(successLogLevel, "SocketRRServer") {
							"response: ${ Charset.defaultHttp.bytesToString(response!!) }"
						}
					}
				} catch (ioException: IoException.Runtime) {
					continue
				} catch (ioException: IncompleteRequestException) {
					requestCache.close()
					continue
				} catch (throwable: Throwable) {
					requestCache.close()
					synchronized(logger) {
						log(successLogLevel, "SocketRRServer") {
							"request: ${ Charset.defaultHttp.bytesToString(requestCache) }"
						}
						log(successLogLevel, "SocketRRServer") { throwable }
					}
					response = handleThrowable(requestCache, throwable)
				}
				connection.output.write(response!!)
				break
			}
		} finally {
			connection.close()
		}
	}


}
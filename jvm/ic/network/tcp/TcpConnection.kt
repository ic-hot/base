package ic.network.tcp


import java.net.InetSocketAddress

import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.ifaces.lifecycle.closeable.Closeable
import ic.network.socket.SocketAddress
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.stream.output.fromos.ByteOutputFromOutputStream


class TcpConnection

	private constructor(
		val address : SocketAddress,
		private val javaSocket : java.net.Socket
	)

: Closeable {

	override fun close() {
		try {
			javaSocket.close()
		} catch (_: java.io.IOException) {}
	}

	val input  : ByteInput
	val output : ByteOutput

	init {
		try {
			input = ByteInputFromInputStream(
				javaSocket.getInputStream()
			)
			output = ByteOutputFromOutputStream(
				javaSocket.getOutputStream()
			)
		} catch (e: java.io.IOException) {
			throw IoException
		}
	}

	@Throws(IoException::class)
	constructor (
		address : SocketAddress,
		timeoutMs : Int32 = 16384
	) : this (
		address = address,
		javaSocket = createJavaSocket(address, timeoutMs = timeoutMs)
	)

	constructor (javaSocket: java.net.Socket) : this (
		address = SocketAddress(
			host = javaSocket.inetAddress.hostAddress!!,
			port = javaSocket.port
		),
		javaSocket = javaSocket
	)

	companion object {

		@Throws(IoException::class)
		private fun createJavaSocket (
			address : SocketAddress,
			timeoutMs : Int32
		) : java.net.Socket {
			return try {
				java.net.Socket().apply {
					soTimeout = timeoutMs
					connect(
						InetSocketAddress(address.host, address.port),
						timeoutMs
					)
				}
			} catch (_: java.io.IOException) {
				throw IoException
			}
		}

	}

}
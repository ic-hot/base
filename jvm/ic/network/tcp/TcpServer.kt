package ic.network.tcp


import ic.base.annotations.Blocking
import ic.math.annotations.BetweenInt
import ic.base.assert.assert
import ic.base.escape.breakable.Break
import ic.base.loop.breakableLoop
import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.base.throwables.ext.stackTraceAsString
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.network.socket.SocketAddress
import ic.parallel.funs.doInBackground
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.parallel.thread.Thread
import ic.parallel.thread.impl.StopThread
import ic.service.blockingstart.BlockingStartService
import ic.util.log.LogLevel
import ic.util.log.Logger
import ic.util.log.globalLogger
import ic.util.log.logD


abstract class TcpServer : BlockingStartService(isReusable = true) {


	private inline val thisSocketServer get() = this


	/**
	 * @return TCP port to listen
	 */
	@BetweenInt(0, 65537)
	protected abstract val port : Int32


	protected open val logger : Logger get() = globalLogger

	protected open val successLogLevel : LogLevel get() = LogLevel.Debug

	protected open val failureLogLevel : LogLevel get() = LogLevel.Debug


	@Throws(IoException::class)
	protected abstract fun handleConnection (connection: TcpConnection)

	protected open fun handleIOException (ioException: IoException) {}

	protected open fun handleThrowable (address: SocketAddress, throwable: Throwable) {
		logger.log(failureLogLevel, "SocketServer") {
			"$address ${ throwable.stackTraceAsString }"
		}
	}


	private val threadMutex = Mutex()

	private var serverSocket : java.net.ServerSocket? = null

	private var thread : Thread? = null


	@Blocking
	public override fun implementStartBlocking() {
		val port = port
		logD("SocketServer") { "start port: $port" }
		assert { port >= 0	  }
		assert { port < 65537 }
		assert { serverSocket == null }
		try {
			serverSocket = java.net.ServerSocket(port)
		} catch (e: java.io.IOException) {
			throw IoException.Runtime(e)
		} catch (e: InterruptedException) {
			throw StopThread
		}
		assert { thread == null }
		threadMutex.synchronized {
			thread = doInBackground {
				try {
					breakableLoop {
						try {
							val connection = TcpConnection(serverSocket!!.accept())
							doInBackground handlingConnection@ {
								try {
									handleConnection(connection)
								} catch (ioException: IoException.Runtime) {
									handleIOException(IoException)
								} catch (ioException: IoException) {
									handleIOException(ioException)
								} catch (throwable: Throwable) {
									handleThrowable(
										address = connection.address,
										throwable = throwable
									)
								}
							}
						} catch (socketClosed: java.net.SocketException) {
							Break()
						} catch (ioException: java.io.IOException) {
							handleIOException(IoException)
						} catch (e: InterruptedException) {
							throw StopThread
						}
					}
				} finally {
					threadMutex.synchronized {
						thread = null
					}
					thisSocketServer.notifyFinished()
				}
			}
		}
		logD("SocketServer") { "start port: $port Success" }
	}


	public override fun implementStopNonBlocking() {

		logD("SocketServer") { "stop" }

		val thread = threadMutex.synchronized { this.thread }
		thread?.stopNonBlockingIfNeeded()

		val serverSocket = this.serverSocket
		this.serverSocket = null
		try {
			serverSocket?.close()
		} catch (_: java.io.IOException) {}

		logD("SocketServer") { "stop Success" }

	}


	override fun implementWaitFor() {
		threadMutex.synchronized {
			thread?.waitFor()
		}
	}


}

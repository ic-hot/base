package ic.network.impl


import ic.network.http.impl.JvmHttpClient


internal abstract class JvmNetworkEngine : NetworkEngine {

	override val httpClient get() = JvmHttpClient

}
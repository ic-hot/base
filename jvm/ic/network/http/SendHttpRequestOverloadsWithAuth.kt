package ic.network.http


import ic.base.throwables.IoException
import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse
import ic.stream.sequence.ByteSequence
import ic.struct.map.finite.FiniteMap
import ic.base.throwables.TimeoutException
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.ext.Empty
import ic.util.url.Url


@Throws(IoException::class, HttpException::class, TimeoutException::class)
fun sendHttpRequest (

	method : String = "GET",

	url : Url,

	authorization: ic.network.http.auth.HttpAuthorization,

	headers : FiniteMap<String, String> = FiniteMap.Empty(),

	body : ByteSequence = EmptyByteSequence,

	connectTimeoutMs 	: Int = 16000,
	readTimeoutMs 		: Int = 16000,

	toIgnoreTrustCertificate : Boolean = false

) : HttpResponse {

	return sendHttpRequest(
		request = HttpRequest(
			method = method,
			url = url,
			authorization = authorization,
			headers = headers,
			body = body
		),
		connectTimeoutMs = connectTimeoutMs,
		readTimeoutMs = readTimeoutMs,
		toIgnoreTrustCertificate = toIgnoreTrustCertificate
	)

}


@Throws(IoException::class, HttpException::class, TimeoutException::class)
fun sendHttpRequest (

	method : String = "GET",

	url : String,

	authorization: ic.network.http.auth.HttpAuthorization,

	headers : FiniteMap<String, String> = FiniteMap.Empty(),

	body : ByteSequence = EmptyByteSequence,

	connectTimeoutMs 	: Int = 16000,
	readTimeoutMs 		: Int = 16000,

	toIgnoreTrustCertificate : Boolean = false

) : HttpResponse {

	return sendHttpRequest(
		request = HttpRequest(
			method = method,
			url = Url.parse(url),
			authorization = authorization,
			headers = headers,
			body = body
		),
		connectTimeoutMs = connectTimeoutMs,
		readTimeoutMs = readTimeoutMs,
		toIgnoreTrustCertificate = toIgnoreTrustCertificate
	)

}
package ic.network.http.cookies


import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.CookieStore


fun enableCookies (cookieStore: CookieStore) {

	val cookieManager = CookieManager(
		cookieStore,
		CookiePolicy.ACCEPT_ALL
	)

	CookieHandler.setDefault(cookieManager)

}
package ic.network.http.request.ext


import ic.base.throwables.UnableToParseException
import ic.network.http.auth.HttpAuthorization
import ic.network.http.auth.ext.parseOrThrowUnableToParse
import ic.network.http.request.HttpRequest


inline val HttpRequest.authorization : HttpAuthorization? get() {

	var authorizationValue = headers["Authorization"]
	if (authorizationValue == null) authorizationValue = headers["authorization"]

	if (authorizationValue == null) return null

	try {
		return HttpAuthorization.parseOrThrowUnableToParse(authorizationValue)
	} catch (unableToParseException: UnableToParseException) {
		throw UnableToParseException.Runtime(unableToParseException)
	}

}
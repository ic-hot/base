package ic.network.http.request


import ic.network.http.auth.HttpAuthorization
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.foreach.breakableForEach
import ic.util.url.Url


fun HttpRequest (

	method : String = "GET",

	url : Url,

	authorization : HttpAuthorization,

	headers : FiniteMap<String, String> = FiniteMap(),

	body : ByteSequence = EmptyByteSequence

) : HttpRequest {

	return HttpRequest(

		method = method,
		url = url,

		headers = run {

			val headersMap = EditableMap<String, String>()

			headers.breakableForEach { key, value ->
				headersMap[key] = value
			}

			headersMap["Authorization"] = authorization.asHeaderString

			headersMap

		},

		body = body

	)

}
package ic.network.http.impl


import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

import ic.base.annotations.Blocking
import ic.base.arrays.ext.isNotEmpty
import ic.base.primitives.int32.Int32
import ic.base.jstream.ext.readToByteSequence
import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse
import ic.network.ssl.TrustAllSslSocketFactory
import ic.stream.sequence.empty.EmptyByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.foreach.forEach
import ic.util.text.charset.Charset


object JvmHttpClient : HttpClient {


	@Blocking
	@Throws(IoException::class, HttpException::class)
	override fun sendHttpRequest (
		request : HttpRequest,
		connectTimeoutMs : Int32, readTimeoutMs : Int32,
		toIgnoreTrustCertificate : Boolean
	) : HttpResponse {

		try {

			val connection = URL(request.urlString).openConnection() as HttpURLConnection

			try {

				if (toIgnoreTrustCertificate) {
					if (connection is HttpsURLConnection) {
						connection.sslSocketFactory = TrustAllSslSocketFactory
					}
				}

				connection.connectTimeout 	= connectTimeoutMs
				connection.readTimeout 		= readTimeoutMs

				connection.requestMethod = request.method

				request.headers.forEach { key, value ->
					connection.setRequestProperty(key, value)
				}

				val requestBodyByteArray = request.body.toByteArray()
				if (requestBodyByteArray.isNotEmpty) {
					connection.doOutput = true
					connection.outputStream.write(requestBodyByteArray)
				}

				val status = connection.responseCode

				val headers = EditableMap<String, String>()

				connection.headerFields.forEach { (key, values) ->
					if (key != null) headers[key] = values[0]
				}

				val isSuccess = status in 200..204

				val inputStream : InputStream? = if (isSuccess) {
					connection.inputStream
				} else {
					connection.errorStream
				}

				val responseBody = (
					if (inputStream == null) {
						EmptyByteSequence
					} else {
						inputStream.readToByteSequence()
					}
				)

				val response = HttpResponse(
					statusCode = status,
					charset = Charset.defaultHttp,
					headers = headers,
					body = responseBody
				)

				if (isSuccess) {
					return response
				} else {
					throw HttpException(request, response)
				}

			} finally {

				connection.disconnect()

			}

		} catch (ioException: IOException) 			{ throw IoException
		} catch (ioException: IoException.Runtime) 	{ throw IoException }

	}


}
package ic.network.http.auth.oauth1


import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.struct.map.finite.FiniteMap
import ic.util.url.Url


@Throws(IoException::class, HttpException::class)
fun requestOAuth1AccessToken (

	url : String,

	consumerKey : OAuth1ConsumerKey,

	temporaryOAuthToken : OAuth1Token,

	oAuthVerifier : String

) : OAuth1Token {

	val response = sendHttpRequestWithOAuth1Authorization(
		method = "POST",
		url = url,
		body = FiniteMap(
			"oauth_token" to temporaryOAuthToken.token,
			"oauth_verifier" to oAuthVerifier
		),
		consumerKey = consumerKey,
		oAuthToken = temporaryOAuthToken
	)

	val responseParams = Url.parseParams(response.bodyAsString)

	return OAuth1Token(
		token = responseParams["oauth_token"]!!,
		secret = responseParams["oauth_token_secret"]!!
	)

}
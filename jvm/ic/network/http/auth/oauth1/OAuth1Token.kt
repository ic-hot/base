package ic.network.http.auth.oauth1


data class OAuth1Token (

	val token : String,

	val secret : String

)
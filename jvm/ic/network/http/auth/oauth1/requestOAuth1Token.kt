package ic.network.http.auth.oauth1


import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.util.url.Url


@Throws(IoException::class, HttpException::class)
fun requestOAuth1Token (

	url : String,

	consumerKey : OAuth1ConsumerKey,

	callbackUrl : String

) : OAuth1Token {

	val response = sendHttpRequestWithOAuth1Authorization(
		url = url,
		consumerKey = consumerKey,
		oAuthToken = null,
		callback = callbackUrl
	)

	val responseParams = Url.parseParams(response.bodyAsString)

	return OAuth1Token(
		token = responseParams["oauth_token"]!!,
		secret = responseParams["oauth_token_secret"]!!
	)

}
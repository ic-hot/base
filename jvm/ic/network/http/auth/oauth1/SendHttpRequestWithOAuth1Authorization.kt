package ic.network.http.auth.oauth1


import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.network.http.response.HttpResponse
import ic.network.http.sendHttpRequest
import ic.struct.map.finite.FiniteMap
import ic.util.text.charset.Charset
import ic.base.throwables.TimeoutException
import ic.ifaces.hascount.ext.isEmpty
import ic.stream.sequence.empty.EmptyByteSequence
import ic.util.text.charset.ext.stringToByteSequence
import ic.util.url.Url


@Throws(IoException::class, HttpException::class, TimeoutException::class)
fun sendHttpRequestWithOAuth1Authorization (

	method : String = "GET",

	url : Url,

	consumerKey : OAuth1ConsumerKey,

	oAuthToken : OAuth1Token?,

	callback : String? = null,

	headers : FiniteMap<String, String> = FiniteMap(),

	body : FiniteMap<String, String> = FiniteMap(),

	connectTimeoutMs 	: Int = 16000,
	readTimeoutMs 		: Int = 16000,

	toIgnoreTrustCertificate : Boolean = false

) : HttpResponse {

	return sendHttpRequest(
		method = method,
		url = url,
		authorization = OAuth1Authorization.generateDefault(
			consumerKey = consumerKey,
			oAuthToken = oAuthToken,
			callback = callback,
			method = method,
			baseUrl = url.baseUrlString,
			urlParams = url.params,
			urlParamsFromBody = body
		),
		headers = headers,
		body = (
			if (body.isEmpty) {
				EmptyByteSequence
			} else {
				Charset.defaultHttp.stringToByteSequence(Url.formatParams(body))
			}
		),
		connectTimeoutMs = connectTimeoutMs,
		readTimeoutMs = readTimeoutMs,
		toIgnoreTrustCertificate = toIgnoreTrustCertificate
	)

}


@Throws(IoException::class, HttpException::class, TimeoutException::class)
fun sendHttpRequestWithOAuth1Authorization (

	method : String = "GET",

	url : String,

	consumerKey : OAuth1ConsumerKey,

	oAuthToken : OAuth1Token?,

	callback : String? = null,

	headers : FiniteMap<String, String> = FiniteMap(),

	body : FiniteMap<String, String> = FiniteMap(),

	connectTimeoutMs 	: Int = 16000,
	readTimeoutMs 		: Int = 16000,

	toIgnoreTrustCertificate : Boolean = false

) : HttpResponse {

	return sendHttpRequestWithOAuth1Authorization(
		method = method,
		url = Url.parse(url),
		consumerKey = consumerKey,
		oAuthToken = oAuthToken,
		callback = callback,
		headers = headers,
		body = body,
		connectTimeoutMs = connectTimeoutMs,
		readTimeoutMs = readTimeoutMs,
		toIgnoreTrustCertificate = toIgnoreTrustCertificate
	)

}


@Throws(IoException::class, HttpException::class, TimeoutException::class)
fun sendHttpRequestWithOAuth1Authorization (

	method : String = "GET",

	baseUrl : String,
	urlParams : FiniteMap<String, String>,

	consumerKey : OAuth1ConsumerKey,

	oAuthToken : OAuth1Token?,

	callback : String? = null,

	headers : FiniteMap<String, String> = FiniteMap(),

	body : FiniteMap<String, String> = FiniteMap(),

	connectTimeoutMs 	: Int = 16000,
	readTimeoutMs 		: Int = 16000,

	toIgnoreTrustCertificate : Boolean = false

) : HttpResponse {

	return sendHttpRequestWithOAuth1Authorization(
		method = method,
		url = Url.fromBaseUrlAndParams(baseUrl = baseUrl, params = urlParams),
		consumerKey = consumerKey,
		oAuthToken = oAuthToken,
		callback = callback,
		headers = headers,
		body = body,
		connectTimeoutMs = connectTimeoutMs,
		readTimeoutMs = readTimeoutMs,
		toIgnoreTrustCertificate = toIgnoreTrustCertificate
	)

}
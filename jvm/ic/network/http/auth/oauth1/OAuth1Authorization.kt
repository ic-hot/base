package ic.network.http.auth.oauth1


import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import ic.base.strings.inUpperCase
import ic.math.rand.getRandomByteArray
import ic.network.http.auth.HttpAuthorization
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.foreach.breakableForEach
import ic.util.text.charset.Charset.Companion.Utf8
import ic.util.text.base64.bytesToBase64String
import ic.util.text.charset.ext.stringToByteArray
import ic.util.text.charset.url.encodeUrl
import ic.system.funs.getCurrentEpochTimeS
import ic.util.url.Url


class OAuth1Authorization(

	val consumerKey : String,

	val oAuthToken : String?,

	val nonce: String,
	val timestamp: String,

	val signatureMethod: String,
	val signature: String,

	val callback: String?

) : HttpAuthorization {


	override val asHeaderString = (
		"OAuth " +
		"oauth_nonce=\"${ encodeUrl(nonce) }\", " +
		(
			if (callback == null) {
				""
			} else {
				"oauth_callback=\"${ encodeUrl(callback) }\", "
			}
		) +
		(
			if (oAuthToken == null) {
				""
			} else {
				"oauth_token=\"${ encodeUrl(oAuthToken) }\", "
			}
		) +
		"oauth_signature_method=\"${ encodeUrl(signatureMethod) }\", " +
		"oauth_timestamp=\"${ encodeUrl(timestamp) }\", " +
		"oauth_consumer_key=\"${ encodeUrl(consumerKey) }\", " +
		"oauth_signature=\"${ encodeUrl(signature) }\", " +
		"oauth_version=\"1.0\""
	)


	companion object {


		private fun generateSignature(

			method : String,
			baseUrl : String,
			urlParams : FiniteMap<String, *>,
			urlParamsFromBody : FiniteMap<String, *>,

			oAuthConsumerKey : OAuth1ConsumerKey,
			oAuthToken : OAuth1Token?,
			oAuthNonce : String,
			oAuthTimestamp : String,
			oAuthCallback : String?

		) : String {

			val allParams = EditableMap<String, Any>().apply {
				urlParams.breakableForEach { key, value ->
					this[key] = value
				}
				urlParamsFromBody.breakableForEach { key, value ->
					this[key] = value
				}
				this["oauth_consumer_key"] 		= oAuthConsumerKey.key
				this["oauth_token"] 			= oAuthToken?.token
				this["oauth_version"]			= "1.0"
				this["oauth_nonce"]				= oAuthNonce
				this["oauth_timestamp"]			= oAuthTimestamp
				this["oauth_signature_method"] 	= "HMAC-SHA1"
				this["oauth_callback"]			= oAuthCallback
			}

			val paramsString = Url.formatParams(allParams)

			val baseStringBuffer = StringBuffer()
			baseStringBuffer.append(method.inUpperCase)
			baseStringBuffer.append('&')
			baseStringBuffer.append(encodeUrl(baseUrl))
			baseStringBuffer.append('&')
			baseStringBuffer.append(encodeUrl(paramsString))

			val baseString = baseStringBuffer.toString()

			val keyBytes = (
				Utf8.stringToByteArray(
					oAuthConsumerKey.secret + '&' + (oAuthToken?.secret ?: "")
				)
			)
			val key = SecretKeySpec(keyBytes, "HmacSHA1")
			val mac = Mac.getInstance("HmacSHA1")
			mac.init(key)
			return bytesToBase64String(mac.doFinal(Utf8.stringToByteArray(baseString)))

		}


		fun generateDefault (

			consumerKey : OAuth1ConsumerKey,
			oAuthToken 	: OAuth1Token?,

			callback : String?,

			method : String,
			baseUrl : String,
			urlParams : FiniteMap<String, *>,
			urlParamsFromBody : FiniteMap<String, *>

		) : OAuth1Authorization {

			val nonce = bytesToBase64String(getRandomByteArray(24))

			val timestamp = getCurrentEpochTimeS().toString()

			return OAuth1Authorization(

				consumerKey = consumerKey.key,
				oAuthToken = oAuthToken?.token,

				callback = callback,

				nonce = nonce,

				timestamp = timestamp,

				signatureMethod = "HMAC-SHA1",

				signature = generateSignature(
					method = method,
					baseUrl = baseUrl,
					urlParams = urlParams,
					urlParamsFromBody = urlParamsFromBody,
					oAuthConsumerKey = consumerKey,
					oAuthToken = oAuthToken,
					oAuthNonce = nonce,
					oAuthTimestamp = timestamp,
					oAuthCallback = callback
				)

			)

		}


	}


}
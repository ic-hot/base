package ic.network.http.auth.oauth1


data class OAuth1ConsumerKey (

	val key 	: String,
	val secret 	: String

)
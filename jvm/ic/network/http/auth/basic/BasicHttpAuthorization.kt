package ic.network.http.auth.basic


import ic.text.input.TextInput
import ic.util.text.charset.Charset
import ic.base.throwables.UnableToParseException
import ic.network.http.auth.HttpAuthorization
import ic.util.text.base64.base64StringToByteArray
import ic.util.text.base64.bytesToBase64String
import ic.util.text.charset.ext.bytesToString
import ic.util.text.charset.ext.stringToByteArray


class BasicHttpAuthorization (

	val username : String,
	val password : String

) : HttpAuthorization {


	override val asHeaderString : String get() {
		return "Basic ${ 
			bytesToBase64String(
				Charset.defaultHttp.stringToByteArray("$username:$password")
			) 
		}"
	}


	companion object {

		@Throws(UnableToParseException::class)
		internal fun parseOrThrowUnableToParse (iterator: TextInput) : BasicHttpAuthorization {
			val base64Credentials = iterator.readToString()
			val decoded : String = Charset.defaultHttp.bytesToString(
				base64StringToByteArray(base64Credentials)
			)
			val split = decoded.split(':')
			if (split.size != 2) throw UnableToParseException()
			return BasicHttpAuthorization(split[0], split[1])
		}

	}


}
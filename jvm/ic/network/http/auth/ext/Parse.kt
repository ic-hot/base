package ic.network.http.auth.ext


import ic.base.throwables.NotExistsException
import ic.base.throwables.UnableToParseException
import ic.network.http.auth.HttpAuthorization
import ic.network.http.auth.basic.BasicHttpAuthorization
import ic.network.http.auth.bearer.BearerHttpAuthorization
import ic.text.Text
import ic.text.ext.FromString
import ic.text.input.TextInput


@Throws(UnableToParseException::class)
fun HttpAuthorization.Companion.parseOrThrowUnableToParse (

	header : String

) : HttpAuthorization? {
	val iterator : TextInput = Text.FromString(header).newIterator()
	return try {
		val type = iterator.safeReadTill(' ').toString()
		if (type == "Basic") {
			BasicHttpAuthorization.parseOrThrowUnableToParse(iterator)
		} else if (type == "Bearer") {
			BearerHttpAuthorization.parseOrThrowUnableToParse(iterator)
		} else throw UnableToParseException()
	} catch (notFound: NotExistsException) {
		throw UnableToParseException()
	}
}
package ic.network.ssl


import java.security.SecureRandom
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory


val TrustAllSslSocketFactory : SSLSocketFactory = (

	SSLContext.getInstance("SSL").apply {
		init(null, arrayOf(TrustAllTrustManager), SecureRandom())
	}

	.socketFactory

)
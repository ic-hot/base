package ic.network.ssl


import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager


@Suppress("TrustAllX509TrustManager")
object TrustAllTrustManager : X509TrustManager {

	override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}

	override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}

	override fun getAcceptedIssuers() = arrayOf<X509Certificate>()

}
package ic.ifaces.getter.getter1.selector


import ic.base.reflect.isInstanceOf
import ic.base.throwables.NotSupportedException


abstract class TypeSelectGetter1 <Value, GeneralArg, SpecificArg>

	: SelectGetter1<Value, GeneralArg>

{


	protected abstract val argType : Class<*>


	protected abstract fun getIfRightType (arg: SpecificArg) : Value


	@Throws(NotSupportedException::class)
	override fun getOrThrowNotSupported (arg: GeneralArg) : Value {

		if (arg isInstanceOf argType) {
			@Suppress("UNCHECKED_CAST")
			return getIfRightType(arg as SpecificArg)
		} else {
			throw NotSupportedException
		}

	}


}
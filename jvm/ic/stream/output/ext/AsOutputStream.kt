package ic.stream.output.ext


import java.io.OutputStream

import ic.base.primitives.int32.ext.asByte
import ic.stream.output.ByteOutput
import ic.stream.output.fromos.ByteOutputFromOutputStream


val ByteOutput.asOutputStream : OutputStream
	get() {
		if (this is ByteOutputFromOutputStream) {
			return internalOutputStream
		}
		val thisByteOutput = this
		return object : OutputStream() {
			override fun write(i: Int) {
				putByte(i.asByte)
			}
			override fun write(byteArray: ByteArray) {
				thisByteOutput.write(byteArray)
			}
			private var isFlushed : Boolean = false
			override fun flush() {
				isFlushed = true
			}
			override fun close() {
				if (isFlushed) {
					thisByteOutput.close()
				} else {
					thisByteOutput.cancel()
				}
			}
		}
	}
;
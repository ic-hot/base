package ic.stream.output.fromos


import java.io.OutputStream

import ic.base.primitives.bytes.ext.asInt32
import ic.base.throwables.IoException
import ic.stream.output.ByteOutput


abstract class ByteOutputFromOutputStream : ByteOutput {


	protected abstract val outputStream : OutputStream

	internal val internalOutputStream get() = outputStream


	@Throws(IoException::class)
	override fun putByte (byte: Byte) {
		try {
			outputStream.write(byte.asInt32)
		} catch (_: java.io.IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	override fun close() {
		try {
			outputStream.flush()
			outputStream.close()
		} catch (_: java.io.IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	override fun cancel() {
		try {
			outputStream.close()
		} catch (_: java.io.IOException) {
			throw IoException
		}
	}


}
@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.output.fromos


import java.io.OutputStream


inline fun ByteOutputFromOutputStream (

	outputStream : OutputStream

) : ByteOutputFromOutputStream {

	return object : ByteOutputFromOutputStream() {

		override val outputStream get() = outputStream

	}

}
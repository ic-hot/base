package ic.stream.input.fromis


import java.io.InputStream
import java.io.IOException

import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.End
import ic.base.throwables.End.End
import ic.base.throwables.IoException
import ic.stream.input.ByteInput


abstract class ByteInputFromInputStream : ByteInput {


	protected abstract val inputStream : InputStream

	private var isClosed : Boolean = false


	@Throws(IoException::class, End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		if (isClosed) End()
		try {
			val i = inputStream.read()
			if (i == -1) {
				inputStream.close()
				isClosed = true
				End()
			}
			return i.asByte
		} catch (_: IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	override fun close() {
		try {
			inputStream.close()
			isClosed = true
		} catch (_: IOException) {
			throw IoException
		}
	}


}
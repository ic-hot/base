@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.input.fromis


import java.io.InputStream


inline fun ByteInputFromInputStream (

	inputStream: InputStream

) : ByteInputFromInputStream {

	return object : ByteInputFromInputStream() {

		override val inputStream get() = inputStream

	}

}
package ic.stream.input.ext


import java.io.InputStream
import java.io.IOException

import ic.base.primitives.bytes.ext.asInt32
import ic.base.throwables.End

import ic.stream.input.ByteInput


inline val ByteInput.asInputStream : InputStream get() {

	return object : InputStream() {

		override fun read(): Int {

			return try {
				getNextByteOrThrowEnd().asInt32
			} catch (end: End) {
				-1
			} catch (throwable: Throwable) {
				throw IOException(throwable)
			}

		}

	}

}
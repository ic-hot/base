package ic.text.input.ext


import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.text.input.TextInput


val TextInput.asJavaReader : java.io.Reader get() {

	return object : java.io.Reader() {

		override fun read() : Int32 {
			try {
				return getNextCharacterOrThrowEnd().code
			} catch (end: End) {
				return -1
			}
		}

		override fun read (chars: CharArray, offset: Int, length: Int) : Int {
			for (i in offset until offset + length) {
				try {
					chars[i] = getNextCharacterOrThrowEnd()
				} catch (end: End) {
					return if (i == 0) -1 else i
				}
			}; return length
		}

		override fun ready() = true

		override fun close() {}

	}

}
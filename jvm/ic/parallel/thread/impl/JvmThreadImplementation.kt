package ic.parallel.thread.impl


import ic.base.primitives.int64.Int64
import ic.parallel.thread.Thread


internal class JvmThreadImplementation  (

	thread : Thread

) : ThreadImplementation(thread = thread) {


	private val javaThread = java.lang.Thread {
		try {
			thread.runBlocking()
		} catch (_: InterruptedException) {
		} catch (_: StopThread) {}
	}


	override val isCurrentThread get() = java.lang.Thread.currentThread() === javaThread


	override fun startNonBlocking() {
		javaThread.start()
	}


	override fun stopNonBlocking() {
		if (!javaThread.isAlive) return
		javaThread.interrupt()
	}


	override fun waitFor() {
		try {
			javaThread.join(Int64.MAX_VALUE)
		} catch (_: InterruptedException) {
			throw StopThread
		}
	}


}
package ic.parallel.impl


import ic.base.primitives.int64.Int64
import ic.parallel.mutex.JvmMutex
import ic.parallel.thread.Thread
import ic.parallel.thread.impl.JvmThreadImplementation
import ic.parallel.thread.impl.StopThread
import ic.parallel.thread.impl.ThreadImplementation


abstract class JvmParallelEngine : ParallelEngine {

	override fun createThreadImplementation (thread: Thread) : ThreadImplementation {
		return JvmThreadImplementation(thread)
	}

	override fun sleep (durationMs: Int64) {
		if (durationMs > 0) {
			try {
				java.lang.Thread.sleep(durationMs)
			} catch (t: InterruptedException) {
				throw StopThread
			}
		} else {
			java.lang.Thread.yield()
			if (java.lang.Thread.interrupted()) throw StopThread
		}
	}

	override fun createMutex() = JvmMutex()

	override fun canBeStoppedHere() {
		if (java.lang.Thread.interrupted()) throw StopThread
	}

}
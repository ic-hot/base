package ic.parallel.mutex


import java.util.concurrent.locks.ReentrantLock

import ic.parallel.thread.funs.StopThread


class JvmMutex : Mutex {

	private val reentrantLock = ReentrantLock()

	override fun seize() {
		try {
			reentrantLock.lock()
		} catch (_: InterruptedException) {
			StopThread()
		}
	}

	override fun release() {
		try {
			reentrantLock.unlock()
		} catch (_: InterruptedException) {
			StopThread()
		}
	}

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.math.funs


import java.math.BigDecimal


inline fun max (a: BigDecimal, b: BigDecimal) = if (a > b) a else b
package ic.storage.fs.local


import ic.base.throwables.AccessException
import ic.storage.fs.Directory
import ic.storage.fs.local.ext.createIfNotExists
import ic.storage.fs.local.ext.getExisting
import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


val userHomeDirectory : Directory by Cached {
	Directory.getExisting(
		System.getProperty("user.home")!!
	)
}


@Throws(AccessException::class)
fun getUserHomeDirectory (userName: String) : Directory {
	// Throw AccessDeniedException if not root
	return Directory.createIfNotExists(
		if (userName == "root") {
			"/root"
		} else {
			"/home/$userName"
		}
	)
}


val publicHomeDirectory : Directory by Cached {
	getUserHomeDirectory("public")
}
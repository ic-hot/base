package ic.storage.fs.local


import ic.storage.fs.Directory
import ic.storage.fs.local.ext.getExisting


val workdir = Directory.getExisting(
	path = System.getProperty("user.dir")!!
)
package ic.storage.fs.local.impl


import java.io.BufferedInputStream
import java.io.FileInputStream

import ic.base.annotations.Valid
import ic.base.assert.assert

import ic.storage.fs.File
import ic.storage.impl.toUseTemporaryFiles
import ic.stream.input.ByteInput
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.stream.output.ByteOutput


internal class JvmFile (

	@Valid
	javaFile : java.io.File

) : JvmFsEntry(javaFile = javaFile), File {


	init {
		assert { javaFile.exists() }
		assert { !javaFile.isDirectory }
	}

	override val length get() = javaFile.length()


	override fun openInput() : ByteInput {
		return ByteInputFromInputStream(
			BufferedInputStream(
				FileInputStream(javaFile)
			)
		)
	}


	private var parentJavaFileField : java.io.File? = parentJavaFile

	private val parentJavaFile get() = (
		parentJavaFileField ?: javaFile.parentFile.also { parentJavaFileField = it }
	)


	override fun openOutput() : ByteOutput {
		return JvmFileOutput(
			parentJavaFile = parentJavaFile,
			javaFileName = javaFile.name,
			toUseTemporaryFiles = toUseTemporaryFiles
		)
	}


}
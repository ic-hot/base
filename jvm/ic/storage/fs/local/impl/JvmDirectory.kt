package ic.storage.fs.local.impl


import java.nio.file.Paths

import ic.base.annotations.Valid
import ic.base.arrays.ext.copy.convert.copyConvertToCollection
import ic.base.arrays.ext.copy.convert.copyConvertToFiniteSet
import ic.base.assert.assert
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.cmd.bash.bashPureSingleQuote
import ic.storage.fs.FsEntry
import ic.storage.fs.Directory
import ic.storage.fs.File
import ic.storage.fs.local.impl.funs.decodeFileName
import ic.storage.fs.local.impl.funs.encodeFileName
import ic.storage.fs.local.impl.funs.resolveJvmFsEntry
import ic.storage.fs.local.impl.funs.setFilePermissions
import ic.struct.collection.Collection
import ic.struct.set.finite.FiniteSet
import ic.system.localcmd.localBashSession


internal class JvmDirectory (

	@Valid
	javaFile : java.io.File,

) : JvmFsEntry(javaFile), Directory {


	init {
		assert({ "${ javaFile.absolutePath } not exists" }) { javaFile.exists() }
		assert { javaFile.isDirectory }
	}


	override fun getItemsNames() : FiniteSet<String> {
		return javaFile.list()!!.copyConvertToFiniteSet { decodeFileName(it) }
	}

	override fun getItems() : Collection<JvmFsEntry> {
		return javaFile.listFiles()!!.copyConvertToCollection { resolveJvmFsEntry(it) }
	}

	@Throws(NotExistsException::class)
	override fun getItemOrThrowNotExists (name: String) : FsEntry {
		val childJavaFile = java.io.File(javaFile, encodeFileName(name))
		if (!childJavaFile.exists()) throw NotExistsException
		return resolveJvmFsEntry(childJavaFile)
	}


	@Throws(AlreadyExistsException::class)
	override fun createFileOrThrowAlreadyExists (name: String) : File {
		val childJavaFile = java.io.File(javaFile, encodeFileName(name))
		if (childJavaFile.exists()) throw AlreadyExistsException
		childJavaFile.createNewFile()
		val childAbsolutePath = childJavaFile.absolutePath
		setFilePermissions(absolutePath = childAbsolutePath, isRecursive = false)
		return JvmFile(childJavaFile)
	}


	@Throws(AlreadyExistsException::class)
	override fun createFolderOrThrowAlreadyExists (name: String) : Directory {
		val childJavaFile = java.io.File(javaFile, encodeFileName(name))
		if (childJavaFile.exists()) throw AlreadyExistsException
		childJavaFile.mkdir()
		val childAbsolutePath = childJavaFile.absolutePath
		setFilePermissions(absolutePath = childAbsolutePath, isRecursive = true)
		return JvmDirectory(childJavaFile)
	}


	override fun copyHereReplacing (name: String, item: FsEntry) {
		if (item is JvmFsEntry) {
			localBashSession.executeCommand(
				"cp -rf ${ item.absolutePath } ${ javaFile.absolutePath }/${ encodeFileName(name) }"
			)
		} else {
			throw NotImplementedError()
		}
	}

	override fun moveHereReplacing (name: String, item: FsEntry) {
		if (item is JvmFsEntry) {
			// TODO Make safe
			val script = (
				"rm -rf ${ bashPureSingleQuote(javaFile.absolutePath + "/" + encodeFileName(name)) };" +
				(
					"mv -f " +
					"${ bashPureSingleQuote(item.absolutePath) } " +
					bashPureSingleQuote(javaFile.absolutePath + "/" + encodeFileName(name))
				)
			)
			val response = localBashSession.executeScript(script)
			assert(
				getMessage = { "$script\n$response" }
			) { response.isEmpty }
		} else {
			throw NotImplementedError()
		}
	}


	override fun absolutizePath (relativePath: String) : String {
		if (relativePath.startsWith("/")) return relativePath
		if (relativePath.startsWith("~")) return relativePath
		return "${ javaFile.absolutePath }/$relativePath"
	}


	override fun relativizePath (absolutePath: String) : String {
		return Paths.get(javaFile.absolutePath).relativize(Paths.get(absolutePath)).toString()
	}


	@Throws(AlreadyExistsException::class)
	override fun createSymbolicLinkOrThrowAlreadyExists (name: String, linkPath: String) {
		val childJavaFile = java.io.File(javaFile, encodeFileName(name))
		if (childJavaFile.exists()) throw AlreadyExistsException
		val absolutePath = javaFile.absolutePath + '/' + encodeFileName(name)
		localBashSession.executeCommand(
			"ln -s $linkPath $absolutePath"
		)
		setFilePermissions(absolutePath, isRecursive = false)
	}


	override fun empty() {
		localBashSession.executeCommand(
			"rm -rf ${ bashPureSingleQuote(javaFile.absolutePath) + "/*" }"
		)
	}


	override fun toString() = absolutePath


}
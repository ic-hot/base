package ic.storage.fs.local.impl


import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.File
import ic.storage.fs.FsEntry
import ic.storage.fs.local.impl.funs.resolveJvmFsEntry
import ic.storage.fs.local.impl.funs.setFilePermissions
import ic.storage.fs.local.workdir


object JvmLocalFsEngine : LocalFsEngine {


	@Throws(NotExistsException::class)
	override fun getEntryOrThrowNotExists (path: String) : FsEntry {
		val javaFile = java.io.File(path)
		if (!javaFile.exists()) throw NotExistsException
		return resolveJvmFsEntry(javaFile)
	}


	@Throws(AlreadyExistsException::class)
	override fun createFolderOrThrowAlreadyExists (path: String) : Directory {
		val absolutePath = workdir.absolutizePath(path)
		val javaFile = java.io.File(absolutePath)
		if (javaFile.exists()) throw AlreadyExistsException
		javaFile.mkdirs()
		setFilePermissions(absolutePath = absolutePath, isRecursive = true)
		return JvmDirectory(
			javaFile = javaFile
		)
	}


	@Throws(AlreadyExistsException::class)
	override fun createFileOrThrowAlreadyExists (path: String) : File {
		val absolutePath = workdir.absolutizePath(path)
		val javaFile = java.io.File(absolutePath)
		if (javaFile.exists()) throw AlreadyExistsException
		val parentJavaFile = javaFile.parentFile ?: throw RuntimeException(
			"Can't get parent directories for $absolutePath"
		)
		parentJavaFile.mkdirs()
		javaFile.createNewFile()
		setFilePermissions(absolutePath = absolutePath, isRecursive = false)
		return JvmFile(javaFile)
	}


}
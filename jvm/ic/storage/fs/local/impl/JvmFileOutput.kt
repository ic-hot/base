package ic.storage.fs.local.impl


import java.io.BufferedOutputStream
import java.io.FileOutputStream

import ic.base.primitives.bytes.ext.asInt32
import ic.base.throwables.IoException
import ic.storage.fs.local.impl.funs.makeTmpFileName
import ic.stream.output.ByteOutput
import ic.system.localcmd.LocalShSession


internal class JvmFileOutput (

	private val parentJavaFile : java.io.File,

	private val javaFileName : String,

	private val toUseTemporaryFiles : Boolean

) : ByteOutput {


	private val javaFile = (
		if (toUseTemporaryFiles) {
			java.io.File(parentJavaFile, makeTmpFileName())
		} else {
			java.io.File(parentJavaFile, javaFileName)
		}
	)

	private val javaOutputStream = BufferedOutputStream(
		FileOutputStream(javaFile)
	)


	@Throws(IoException::class)
	override fun putByte (byte: Byte) {
		try {
			try {
				javaOutputStream.write(byte.asInt32)
			} catch (e: java.io.IOException) {
				throw IoException.Runtime(e)
			}
		} catch (t: Throwable) {
			javaFile.delete()
			throw t
		}
	}


	@Throws(IoException::class)
	override fun close() {
		try {
			try {
				javaOutputStream.flush()
				javaOutputStream.close()
			} catch (e: java.io.IOException) {
				throw IoException.Runtime(e)
			}
			if (toUseTemporaryFiles) {
				LocalShSession(
					workdirPath = parentJavaFile.absolutePath
				).run {
					val command = "mv -f ${ javaFile.name } $javaFileName"
					val response = executeCommand(command)
					if (response.isNotEmpty) {
						throw RuntimeException("$command\n$response")
					}
				}
			}
		} catch (t: Throwable) {
			javaFile.delete()
			throw t
		}
	}


	override fun cancel() {
		try {
			javaOutputStream.close()
		} catch (_: java.io.IOException) {}
		javaFile.delete()
	}


}
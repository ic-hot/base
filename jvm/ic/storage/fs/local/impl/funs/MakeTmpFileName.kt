package ic.storage.fs.local.impl.funs


import ic.base.primitives.int64.ext.asFixedSizeHexString
import ic.math.rand.randomInt64
import ic.system.funs.getCurrentEpochTimeMs


internal fun makeTmpFileName() = (
	".tmp" +
	getCurrentEpochTimeMs().asFixedSizeHexString + randomInt64().asFixedSizeHexString
)
package ic.storage.fs.local.impl.funs


import ic.util.text.charset.url.decodeUrl
import ic.util.text.charset.url.encodeUrl
import ic.util.text.path.isValidFileName


private const val URL_ENCODED_PREFIX = "URL_ENCODED_UTF_8."


// For some reason it doesn't work in some cases. More robust solution needed
internal fun encodeFileName (key: String) : String {
	if (key.isValidFileName) {
		return key
	} else {
		return URL_ENCODED_PREFIX + encodeUrl(key)
	}
}


// For some reason it doesn't work in some cases. More robust solution needed
internal fun decodeFileName (fileName: String) : String {
	if (fileName.startsWith(URL_ENCODED_PREFIX)) {
		return decodeUrl(fileName.substring(URL_ENCODED_PREFIX.length))
	} else {
		return fileName
	}
}
package ic.storage.fs.local.impl.funs


import ic.base.primitives.int64.Int64


internal fun getJavaFileSizeInBytes (javaFile: java.io.File) : Int64 {

	if (javaFile.isDirectory) {

		return (javaFile.listFiles() ?: return 0).sumOf { getJavaFileSizeInBytes(it) }

	} else {

		return javaFile.length()

	}

}
package ic.storage.fs.local.impl.funs


import ic.base.platform.PlatformType
import ic.base.platform.platformType
import ic.system.isRoot
import ic.system.localcmd.localShSession


fun setFilePermissions (absolutePath: String, isRecursive: Boolean) {

	if (platformType == PlatformType.Jvm.Android) return

	if (absolutePath.startsWith("/home/public/")) {

		localShSession.executeCommand(
			"chmod " +
			(if (isRecursive) "-R " else "") +
			"777 " +
			absolutePath
		)

	} else {

		if (isRoot) {

			if (!absolutePath.startsWith("/home/")) {
				localShSession.executeCommand(
					"chmod " +
					(if (isRecursive) "-R " else "") +
					"755 " +
					absolutePath
				)
			}

		}

	}

}
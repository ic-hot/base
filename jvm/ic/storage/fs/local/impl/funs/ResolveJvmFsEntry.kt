package ic.storage.fs.local.impl.funs


import ic.base.annotations.Valid
import ic.storage.fs.local.impl.JvmDirectory
import ic.storage.fs.local.impl.JvmFile
import ic.storage.fs.local.impl.JvmFsEntry


internal fun resolveJvmFsEntry (

	@Valid javaFile : java.io.File

) : JvmFsEntry {

	if (javaFile.isDirectory) {

		return JvmDirectory(
			javaFile = javaFile
		)

	} else {

		return JvmFile(
			javaFile = javaFile
		)

	}

}
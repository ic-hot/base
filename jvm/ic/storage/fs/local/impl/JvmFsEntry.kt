package ic.storage.fs.local.impl


import ic.base.primitives.int64.Int64
import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.Mutex
import ic.storage.fs.Directory
import ic.storage.fs.FsEntry
import ic.storage.fs.local.impl.funs.decodeFileName
import ic.storage.fs.local.impl.funs.getJavaFileSizeInBytes
import ic.struct.value.ext.getValue
import ic.struct.value.cached.Cached
import ic.util.time.Time


internal abstract class JvmFsEntry (

	val javaFile : java.io.File,

) : FsEntry, Mutable {


	override val mutex = Mutex()


	override val modifiedAt : Time by Cached {
		Time(epochMs = javaFile.lastModified())
	}


	override val sizeInBytes : Int64 get() = getJavaFileSizeInBytes(javaFile)


	override val name : String by Cached {
		decodeFileName(javaFile.name)
	}


	override val absolutePath : String by Cached {
		var absolutePath = javaFile.absolutePath
		if (absolutePath.endsWith("/")) {
			absolutePath = absolutePath.substring(0, absolutePath.length - "/".length)
		}
		absolutePath
	}


	override val parent : Directory? by Cached {
		javaFile.parentFile?.let { JvmDirectory(it) }
	}


	override val freeSpaceInBytes get() = javaFile.freeSpace


	override fun remove() {
		javaFile.deleteRecursively()
	}


}
package ic.storage.impl


import ic.storage.fs.local.impl.JvmLocalFsEngine


object JvmStorageEngine : StorageEngine {


	override val localFsEngine get() = JvmLocalFsEngine


}
package ic.storage.impl


import ic.base.platform.PlatformType
import ic.base.platform.platformType


val toUseTemporaryFiles : Boolean get() = (
	if (platformType == PlatformType.Jvm.Android) {
		false
	} else {
		true
	}
)
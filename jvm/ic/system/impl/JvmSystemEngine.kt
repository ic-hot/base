package ic.system.impl


import ic.base.throwables.NotExistsException
import ic.system.CpuArchitecture
import ic.system.CpuCoresCount


internal abstract class JvmSystemEngine : SystemEngine() {


	override fun getCurrentEpochTimeMs() = System.currentTimeMillis()


	override fun getCpuArchitecture() : CpuArchitecture {
		val unixName = System.getProperty("os.arch")
		return when (unixName) {
			"x86_64" -> CpuArchitecture.X86_64
			"amd64"  -> CpuArchitecture.X86_64
			else -> throw RuntimeException("unixName: $unixName")
		}
	}


	override fun getCpuCoresCount() = CpuCoresCount.cpuCoresCount


}
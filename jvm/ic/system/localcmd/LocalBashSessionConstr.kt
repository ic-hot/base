@file:Suppress("NOTHING_TO_INLINE")


package ic.system.localcmd


import ic.storage.fs.Directory


@Suppress("FunctionName")
inline fun LocalBashSession (

	workdirPath : String

) : LocalCmdSession {

	return LocalCmdSession(
		shellExecutablePath = "/bin/bash",
		workdirPath = workdirPath
	)

}


@Suppress("FunctionName")
inline fun LocalBashSession (

	workdir : Directory

) : LocalCmdSession {

	return LocalCmdSession(
		shellExecutablePath = "/bin/bash",
		workdirPath = workdir.absolutePath
	)

}
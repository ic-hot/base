@file:Suppress("NOTHING_TO_INLINE")


package ic.system.localcmd


import ic.storage.fs.Directory


@Suppress("FunctionName")
inline fun LocalShSession (

	workdirPath : String

) : LocalCmdSession {

	return LocalCmdSession(
		shellExecutablePath = "/bin/sh",
		workdirPath = workdirPath
	)

}


@Suppress("FunctionName")
inline fun LocalShSession (

	workdir : Directory

) : LocalCmdSession {

	return LocalCmdSession(
		shellExecutablePath = "/bin/sh",
		workdirPath = workdir.absolutePath
	)

}
package ic.system.localcmd


import ic.base.strings.ext.asText
import ic.base.strings.ext.replaceAll
import ic.base.strings.ext.trimmed
import ic.cmd.bash.BashSession
import ic.stream.input.ext.read
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.text.*
import ic.util.log.logD
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.bytesToText


class LocalCmdSession (

	private val shellExecutablePath : String,

	override var workdirPath : String

) : BashSession() {


	override fun implementExecuteScript (script: String) : Text {
		val processBuilder = ProcessBuilder(shellExecutablePath, "-c", script)
		processBuilder.redirectErrorStream(true)
		val process = processBuilder.start()
		val output = ByteInputFromInputStream(process.inputStream)
		var outputText = Charset.defaultUnix.bytesToText(output.read())
		output.close()
		outputText = outputText.toString().replaceAll("\r" to "").trimmed.asText
		return outputText
	}


	fun launchCommand (command: String) {
		logD("LocalBashSession") { "launchScript $command" }
		var processBuilder = ProcessBuilder(
			shellExecutablePath, "-c", "cd $workdirPath; $command"
		)
		processBuilder = processBuilder.inheritIO()
		val process = processBuilder.start()
		process.waitFor()
	}


	override fun close() {}


}
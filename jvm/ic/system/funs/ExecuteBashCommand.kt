@file:Suppress("NOTHING_TO_INLINE")


package ic.system.funs


import ic.system.localcmd.localBashSession
import ic.text.Text


inline fun executeBashCommand (command: String) = localBashSession.executeCommand(command)

inline fun executeBashScript (script: String) = localBashSession.executeScript(script)

inline fun executeBashScript (script: Text) = executeBashScript(script.toString())
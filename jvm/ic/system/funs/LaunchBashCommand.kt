@file:Suppress("NOTHING_TO_INLINE")


package ic.system.funs


import ic.system.localcmd.localBashSession


inline fun launchBashCommand (command: String) = localBashSession.launchCommand(command)
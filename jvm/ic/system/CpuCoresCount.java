package ic.system;


public class CpuCoresCount {

	private CpuCoresCount() {}

	public static int cpuCoresCount = Runtime.getRuntime().availableProcessors();

}

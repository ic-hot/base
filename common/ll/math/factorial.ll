

factorial
	in positive integer
	out branch
		if equals in 1
		then 1
		else factorial
			out minus in 1


assert equals 1
	out factorial 1

assert equals 2
	out factorial 2

assert equals 3
	out factorial 6

assert equals 4
	out factorial 24

assert equals 5
	out factorial 120

assert equals 6
    out factorial 720

assert equals 7
	out factorial 5040

assert equals 8
	out factorial 40320


out factorial
@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.sequence.ext


import ic.stream.sequence.ByteSequence
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.bytesToText


inline fun ByteSequence.toText (charset: Charset = Charset.Utf8) = charset.bytesToText(this)
package ic.stream.sequence.ext


import ic.stream.buffer.ByteBuffer
import ic.stream.input.ext.readToByteBuffer
import ic.stream.sequence.ByteSequence


fun ByteSequence.toByteArray() : ByteArray {
	if (this is ByteBuffer) return toByteArray()
	return newIterator().readToByteBuffer().toByteArray()
}
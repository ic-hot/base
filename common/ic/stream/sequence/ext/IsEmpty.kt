package ic.stream.sequence.ext


import ic.stream.sequence.ByteSequence
import ic.base.throwables.End


inline val ByteSequence.isEmpty : Boolean get() {

	val iterator = newIterator()

	try {
		iterator.getNextByteOrThrowEnd()
		return false
	} catch (t: End) {
		return true
	}

}
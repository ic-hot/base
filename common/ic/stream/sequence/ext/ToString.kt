@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.sequence.ext


import ic.stream.sequence.ByteSequence
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.bytesToString


inline fun ByteSequence.toString (charset: Charset) : String {

	return charset.bytesToString(this)

}
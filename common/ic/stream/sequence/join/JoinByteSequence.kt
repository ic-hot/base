package ic.stream.sequence.join


import ic.base.annotations.Throws
import ic.base.throwables.End
import ic.stream.input.ByteInput
import ic.stream.sequence.BaseByteSequence
import ic.stream.sequence.ByteSequence
import ic.struct.sequence.Sequence


abstract class JoinByteSequence : BaseByteSequence() {

	protected abstract val byteSequences: Sequence<ByteSequence>

	override fun newIterator() : ByteInput {
		return object : ByteInput {

			private val byteSequenceIterator = byteSequences.newIterator()

			private var itemIterator: ByteInput? = null

			@Throws(End::class)
			override fun getNextByteOrThrowEnd() : Byte {
				while (true) {
					if (itemIterator == null) {
						itemIterator = byteSequenceIterator.getNextOrThrowEnd().newIterator()
					}
					try {
						return itemIterator!!.getNextByteOrThrowEnd()
					} catch (endOfItem: End) {
						itemIterator = null
					}
				}
			}

			override fun close() {}

		}
	}

}
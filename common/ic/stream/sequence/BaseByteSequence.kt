package ic.stream.sequence


import ic.stream.sequence.ext.toString
import ic.struct.sequence.BaseSequence
import ic.util.text.charset.Charset.Companion.Utf8


abstract class BaseByteSequence : BaseSequence<Byte>(), ByteSequence {

	override fun toString() : String {
		return toString(charset = Utf8)
	}

}
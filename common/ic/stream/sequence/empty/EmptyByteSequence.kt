package ic.stream.sequence.empty


import ic.stream.input.empty.EmptyByteInput
import ic.stream.sequence.BaseByteSequence


object EmptyByteSequence : BaseByteSequence() {

	override fun newIterator() = EmptyByteInput

}
package ic.stream.sequence


import ic.base.annotations.Narrowing
import ic.stream.input.ByteInput
import ic.struct.sequence.Sequence


interface ByteSequence : Sequence<Byte> {


	@Narrowing
	override fun newIterator() : ByteInput
	

}
package ic.stream.sequence.frombytearray


import ic.base.annotations.Throws
import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.stream.input.ByteInput
import ic.stream.sequence.BaseByteSequence


abstract class ByteSequenceFromByteArray : BaseByteSequence() {


	protected abstract val byteArray : ByteArray


	override fun newIterator() = object : ByteInput {

		private var index : Int32 = 0

		private var isClosed : Boolean = false

		@Throws(End::class)
		override fun getNextByteOrThrowEnd() : Byte {
			if (isClosed) throw End
			if (index < byteArray.length) {
				return byteArray[index++]
			} else {
				throw End
			}
		}

		override fun close() {
			isClosed = true
		}

	}


}
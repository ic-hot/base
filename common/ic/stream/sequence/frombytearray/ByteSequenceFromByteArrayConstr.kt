@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.sequence.frombytearray


inline fun ByteSequenceFromByteArray (

	byteArray : ByteArray

) : ByteSequenceFromByteArray {

	return object : ByteSequenceFromByteArray() {

		override val byteArray get() = byteArray

	}

}
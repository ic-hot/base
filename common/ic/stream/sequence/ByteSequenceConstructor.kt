@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.stream.sequence


import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray


inline fun ByteSequence (bytes: ByteArray) = ByteSequenceFromByteArray(bytes)
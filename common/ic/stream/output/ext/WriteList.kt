package ic.stream.output.ext


import ic.base.primitives.int64.ext.asInt32
import ic.stream.output.ByteOutput
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


inline fun <Item> ByteOutput.writeListWithInt32Length (
	list : List<Item>,
	writeItem : ByteOutput.(Item) -> Unit
) {
	writeInt32(list.length.asInt32)
	list.forEach { item ->
		writeItem(item)
	}
}


inline fun <Item> ByteOutput.writeListWithInt64Length (
	list : List<Item>,
	writeItem : ByteOutput.(Item) -> Unit
) {
	writeInt64(list.length)
	list.forEach { item ->
		writeItem(item)
	}
}
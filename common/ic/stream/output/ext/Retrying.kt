package ic.stream.output.ext


import ic.base.primitives.int32.Int32
import ic.stream.output.ByteOutput
import ic.stream.output.retry.RetryingByteOutput
import ic.util.time.duration.Duration


@Suppress("NOTHING_TO_INLINE")
inline fun ByteOutput.retrying (

	maxRetriesCount : Int32,

	delay : Duration

) : ByteOutput {

	return RetryingByteOutput(
		sourceByteOutput = this,
		maxRetriesCount = maxRetriesCount,
		delay = delay
	)

}
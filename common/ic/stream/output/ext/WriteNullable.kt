package ic.stream.output.ext


import ic.base.throwables.IoException
import ic.stream.output.ByteOutput


@Throws(IoException::class)
inline fun <T: Any> ByteOutput.writeNullable (obj: T?, write: ByteOutput.(T) -> Unit) {

	if (obj == null) {

		writeBoolean(false)

	} else {

		writeBoolean(true)
		write(obj)

	}

}
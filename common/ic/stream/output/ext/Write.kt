package ic.stream.output.ext


import ic.base.loop.loop
import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun ByteOutput.write (bytes: ByteArray) {
	bytes.forEach { putByte(it) }
}


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun ByteOutput.write (input: ByteInput) {
	input.runAndCloseOrCancel {
		try {
			loop {
				putByte(
					getNextByteOrThrowEnd()
				)
			}
		} catch (_: End) {}
	}
}
package ic.stream.output.ext


import ic.base.arrays.ext.length
import ic.stream.output.ByteOutput


fun ByteOutput.writeByteArray (byteArray: ByteArray) {

	writeInt32(byteArray.length)

	write(byteArray)

}
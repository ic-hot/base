package ic.stream.output.ext


import ic.stream.input.ByteInput
import ic.stream.input.empty.EmptyByteInput


inline val ByteInput.Companion.Empty get() = EmptyByteInput
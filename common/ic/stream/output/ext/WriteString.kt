package ic.stream.output.ext


import ic.base.arrays.ext.length
import ic.base.strings.ext.toByteArray
import ic.stream.output.ByteOutput
import ic.util.text.charset.Charset
import ic.util.text.charset.Charset.Companion.Utf8


fun ByteOutput.writeString (string: String, charset: Charset = Utf8) {
	val bytes = string.toByteArray(charset = charset)
	writeInt32(bytes.length)
	write(bytes)
}


fun ByteOutput.writeNullableString (string: String?, charset: Charset = Utf8) {
	if (string == null) {
		writeInt32(-1)
	} else {
		writeString(string, charset = charset)
	}
}
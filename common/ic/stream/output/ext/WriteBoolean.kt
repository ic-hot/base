package ic.stream.output.ext


import ic.base.throwables.IoException
import ic.stream.output.ByteOutput


@Throws(IoException::class)
@Suppress("NOTHING_TO_INLINE")
inline fun ByteOutput.writeBoolean (boolean: Boolean) {

	putByte(
		if (boolean) {
			1
		} else {
			0
		}
	)

}
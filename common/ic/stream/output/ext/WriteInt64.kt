package ic.stream.output.ext


import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.toByteArray
import ic.stream.output.ByteOutput


inline fun ByteOutput.writeInt64 (int64: Int64) {

	write(
		int64.toByteArray()
	)

}
package ic.stream.output.ext


import ic.base.primitives.int64.ext.asInt32
import ic.stream.output.ByteOutput
import ic.struct.collection.ext.count.count
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach


inline fun <Key, Value: Any> ByteOutput.writeFiniteMapWithInt32Length (

	finiteMap : FiniteMap<Key, Value>,

	crossinline writeKey : ByteOutput.(Key) -> Unit,

	crossinline writeValue : ByteOutput.(Value) -> Unit

) {

	writeInt32(finiteMap.keys.count.asInt32)

	finiteMap.forEach { key, value ->

		writeKey(key)

		writeValue(value)

	}

}
package ic.stream.output.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.toByteArray
import ic.base.throwables.IoException
import ic.stream.output.ByteOutput


@Throws(IoException::class)
inline fun ByteOutput.writeInt32 (int32: Int32) {

	write(
		int32.toByteArray()
	)

}
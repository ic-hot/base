package ic.stream.output


import ic.base.annotations.Throws
import ic.base.loop.loop
import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.writer.Writer
import ic.stream.sequence.ByteSequence


interface ByteOutput : Closeable, Writer<ByteSequence>, Cancelable {


	@Throws(IoException::class)
	fun putByte (byte: Byte)


	@Throws(IoException::class)
	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun write (bytes: ByteSequence) {
		try {
			val input = bytes.newIterator()
			loop {
				putByte(
					input.getNextByteOrThrowEnd()
				)
			}
		} catch (_: End) {}
	}


	@Throws(IoException::class)
	override fun close()


	@Throws(IoException::class)
	override fun cancel()


}

package ic.stream.output.retry


import ic.base.annotations.Throws
import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.parallel.funs.sleep
import ic.stream.output.ByteOutput
import ic.util.time.duration.Duration


class RetryingByteOutput (

	private val sourceByteOutput : ByteOutput,

	private val maxRetriesCount : Int32,

	private val delay : Duration

) : ByteOutput {


	override fun putByte (byte: Byte) {
		var retryIndex : Int32 = 0
		loop {
			try {
				return sourceByteOutput.putByte(byte)
			} catch (t: IoException) {
				if (retryIndex < maxRetriesCount) {
					sleep(delay)
					retryIndex++
				} else {
					throw t
				}
			}
		}
	}


	@Throws(IoException::class)
	override fun close() {
		var retryIndex : Int32 = 0
		loop {
			try {
				return sourceByteOutput.close()
			} catch (t: IoException) {
				if (retryIndex < maxRetriesCount) {
					sleep(delay)
					retryIndex++
				} else {
					throw t
				}
			}
		}
	}


	@Throws(IoException::class)
	override fun cancel() {
		sourceByteOutput.cancel()
	}


}
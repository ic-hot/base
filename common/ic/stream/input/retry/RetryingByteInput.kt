package ic.stream.input.retry


import ic.base.annotations.Throws
import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.parallel.funs.sleep
import ic.stream.input.ByteInput
import ic.util.time.duration.Duration


class RetryingByteInput (

	private val sourceByteInput : ByteInput,

	private val maxRetriesCount : Int32,

	private val delay : Duration

) : ByteInput {


	@Throws(IoException::class, End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		var retryIndex : Int32 = 0
		loop {
			try {
				return sourceByteInput.getNextByteOrThrowEnd()
			} catch (_: IoException) {
				if (retryIndex < maxRetriesCount) {
					sleep(delay)
					retryIndex++
				} else {
					throw IoException
				}
			}
		}
	}


	@Throws(IoException::class)
	override fun close() {
		sourceByteInput.close()
	}


}
package ic.stream.input


import ic.base.throwables.End
import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.iterator.Iterator


interface ByteInput : Iterator<Byte>, Closeable {


	@Throws(End::class)
	fun getNextByteOrThrowEnd() : Byte


	@Throws(End::class)
	override fun getNextOrThrowEnd() : Byte = getNextByteOrThrowEnd()


	override fun close()


	companion object


}
package ic.stream.input.empty


import ic.base.throwables.End
import ic.stream.input.ByteInput


object EmptyByteInput : ByteInput {

	override fun getNextByteOrThrowEnd() = throw End

	override fun close() {}

}
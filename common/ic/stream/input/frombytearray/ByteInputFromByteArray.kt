package ic.stream.input.frombytearray


import ic.base.annotations.Throws
import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.stream.input.ByteInput


class ByteInputFromByteArray (

	private val byteArray : ByteArray

) : ByteInput {

	var index : Int32 = 0

	@Throws(End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		if (index < byteArray.length) {
			return byteArray[index++]
		} else {
			throw End
		}
	}

	override fun close() {}

}
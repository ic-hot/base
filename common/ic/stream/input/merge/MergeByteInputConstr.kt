package ic.stream.input.merge


import ic.base.arrays.ext.asList
import ic.ifaces.iterator.Iterator
import ic.stream.input.ByteInput
import ic.struct.sequence.Sequence


inline fun MergeByteInput (sourceInputs: Iterator<ByteInput>) : MergeByteInput {

	return object : MergeByteInput() {

		override val sourceInputs get() = sourceInputs

	}

}


inline fun MergeByteInput (sourceInputs: Sequence<ByteInput>) : MergeByteInput {
	return MergeByteInput(
		sourceInputs = sourceInputs.newIterator()
	)
}


inline fun MergeByteInput (vararg sourceInputs: ByteInput) : MergeByteInput {
	return MergeByteInput(sourceInputs = sourceInputs.asList)
}
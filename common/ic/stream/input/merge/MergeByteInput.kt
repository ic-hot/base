package ic.stream.input.merge


import ic.base.loop.loop
import ic.base.throwables.End
import ic.ifaces.iterator.Iterator
import ic.stream.input.ByteInput


abstract class MergeByteInput : ByteInput {

	protected abstract val sourceInputs : Iterator<ByteInput>

	private var input : ByteInput? = null

	@Throws(End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		if (input == null) {
			input = sourceInputs.getNextOrThrowEnd()
		}
		loop {
			try {
				return input!!.getNextByteOrThrowEnd()
			} catch (_: End) {
				input = sourceInputs.getNextOrThrowEnd()
			}
		}
	}

	override fun close() {
		try {
			loop {
				input?.close()
				input = sourceInputs.getNextOrThrowEnd()
			}
		} catch (_: End) {}
	}

}
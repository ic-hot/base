@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.input.cache


import ic.stream.input.ByteInput


inline fun CacheByteInput (

	sourceByteInput : ByteInput

) : CacheByteInput {

	return object : CacheByteInput() {

		override val sourceByteInput get() = sourceByteInput

	}

}
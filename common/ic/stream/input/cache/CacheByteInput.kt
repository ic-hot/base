package ic.stream.input.cache


import ic.base.throwables.End
import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.mutable.ext.synchronized
import ic.stream.buffer.ByteBuffer
import ic.stream.input.ByteInput
import ic.stream.sequence.BaseByteSequence


abstract class CacheByteInput : BaseByteSequence(), Closeable {


	protected abstract val sourceByteInput : ByteInput


	private val buffer = ByteBuffer()

	private var isClosed = false


	override fun newIterator(): ByteInput {

		return object : ByteInput {

			var index = 0L

			override fun getNextByteOrThrowEnd(): Byte {
				try {
					buffer.synchronized {
						if (index < buffer.count) {
							return buffer.getByteOrThrowIndexOutOfBounds(index)
						}
					}
					if (isClosed) throw End
					val byte: Byte = sourceByteInput.getNextByteOrThrowEnd()
					buffer.synchronized { buffer.putByte(byte) }
					return byte
				} finally {
					index++
				}
			}

			override fun close() {}

		}

	}


	override fun close() {
		isClosed = true
	}


}
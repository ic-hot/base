package ic.stream.input.ext


import ic.stream.input.ByteInput
import ic.struct.map.finite.FiniteMap


inline fun <Key, Value: Any> ByteInput.readFiniteMapWithInt32Length (

	readKey : ByteInput.() -> Key,

	readValue : ByteInput.() -> Value

) : FiniteMap<Key, Value> {

	return readEditableMapWithInt32Length(
		readKey = readKey,
		readValue = readValue
	).apply {
		// TODO freeze()
	}

}
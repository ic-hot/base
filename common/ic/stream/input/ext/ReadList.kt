package ic.stream.input.ext


import ic.base.primitives.int32.Int32
import ic.stream.input.ByteInput
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item> ByteInput.readEditableListWithInt32Length (
	readItem : ByteInput.() -> Item
) : EditableList<Item> {
	val editableList = EditableList<Item>()
	repeat(readInt32()) {
		editableList.add(readItem())
	}
	return editableList
}

inline fun <Item> ByteInput.readListWithInt32Length (
	readItem : ByteInput.() -> Item
) : List<Item> {
	return readEditableListWithInt32Length(readItem).apply { freeze() }
}

inline fun <Item> ByteInput.readListWithInt32LengthIndexed (
	readItem : ByteInput.(index: Int32) -> Item
) : List<Item> {
	return List(length = readInt32()) { index ->  readItem(index) }
}


inline fun <Item> ByteInput.readListWithInt64Length (
	readItem : ByteInput.() -> Item
) : List<Item> {
	return List(length = readInt64()) { readItem() }
}
package ic.stream.input.ext


import ic.base.arrays.bytes.ByteArray
import ic.base.primitives.int32.Int32
import ic.stream.input.ByteInput


fun ByteInput.readToByteArray (amount: Int32) : ByteArray {
	val byteArray = ByteArray(length = amount)
	repeat(amount) { index ->
		byteArray[index] = getNextByte()
	}
	return byteArray
}
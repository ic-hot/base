package ic.stream.input.ext


import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.IoException
import ic.base.throwables.UnableToParseException
import ic.stream.input.ByteInput


@Throws(IoException::class)
@Suppress("NOTHING_TO_INLINE")
inline fun ByteInput.readBoolean() : Boolean {

	val byte = getNextByte()

	when (byte) {

		0.asByte -> return false
		1.asByte -> return true

		else -> throw UnableToParseException.Runtime()

	}

}
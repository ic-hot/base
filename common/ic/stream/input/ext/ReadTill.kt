package ic.stream.input.ext


import ic.base.loop.breakableRepeat
import ic.stream.buffer.ByteBuffer
import ic.base.throwables.End
import ic.base.throwables.NotExistsException
import ic.stream.input.ByteInput


private inline fun <Result> ByteInput.implementReadToByteBufferTill (
	byteArrayToFind : ByteArray,
	onNotFound : (ByteBuffer) -> Result,
	onSuccess : (ByteBuffer) -> Result
) : Result {

	val buffer = ByteBuffer()

	var arrayIndex = 0

	try {

		while (true) {

			val byte : Byte = getNextByteOrThrowEnd()

			if (byte == byteArrayToFind[arrayIndex]) {

				arrayIndex++
				if (arrayIndex >= byteArrayToFind.size) {
					return onSuccess(buffer)
				}

			} else {

				breakableRepeat(arrayIndex) { i ->
					buffer.putByte(byteArrayToFind[i])
				}

				arrayIndex = 0
				buffer.putByte(byte)

			}

		}

	} catch (end: End) {

		breakableRepeat(arrayIndex) { i ->
			buffer.putByte(byteArrayToFind[i])
		}

		return onNotFound(buffer)

	}

}


@Throws(NotExistsException::class)
fun ByteInput.readToByteBufferTillOrThrowNotExists (byteArrayToFind : ByteArray) : ByteBuffer {
	return implementReadToByteBufferTill(
		byteArrayToFind,
		onNotFound = { throw NotExistsException },
		onSuccess = { it }
	)
}

fun ByteInput.readToByteBufferTill (byteArrayToFind : ByteArray) : ByteBuffer {
	return implementReadToByteBufferTill(
		byteArrayToFind,
		onNotFound = { throw RuntimeException() },
		onSuccess = { it }
	)
}

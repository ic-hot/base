@file:Suppress("NOTHING_TO_INLINE")


package ic.stream.input.ext


import ic.base.primitives.int64.Int64
import ic.base.throwables.End
import ic.stream.input.ByteInput


inline fun ByteInput.readInt64OrThrowEnd() : Int64 {
	return Int64(
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd()
	)
}

inline fun ByteInput.readInt64() : Int64 {
	try {
		return readInt64OrThrowEnd()
	} catch (t: End) {
		throw End.Runtime()
	}
}
package ic.stream.input.ext


import ic.base.throwables.IoException
import ic.stream.input.ByteInput


@Throws(IoException::class)
inline fun <T: Any?> ByteInput.readNullable (read: ByteInput.() -> T) : T? {

	val isNotNull = readBoolean()

	if (isNotNull) {

		return read()

	} else {

		return null

	}

}
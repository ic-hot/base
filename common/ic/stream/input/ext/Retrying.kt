package ic.stream.input.ext


import ic.base.primitives.int32.Int32
import ic.stream.input.ByteInput
import ic.stream.input.retry.RetryingByteInput
import ic.util.time.duration.Duration


fun ByteInput.retrying (

	maxRetriesCount : Int32,

	delay : Duration

) : ByteInput {

	return RetryingByteInput(
		sourceByteInput = this,
		maxRetriesCount = maxRetriesCount,
		delay = delay
	)

}
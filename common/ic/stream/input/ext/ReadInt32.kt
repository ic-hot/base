package ic.stream.input.ext


import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.stream.input.ByteInput


fun ByteInput.readInt32OrThrowEnd() : Int32 {
	return Int32(
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd(),
		getNextByteOrThrowEnd()
	)
}

fun ByteInput.readInt32() : Int32 {
	try {
		return readInt32OrThrowEnd()
	} catch (_: End) {
		throw RuntimeException()
	}
}
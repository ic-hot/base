package ic.stream.input.ext


import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.stream.input.ByteInput


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun ByteInput.getNextByte() : Byte {
	try {
		return getNextByteOrThrowEnd()
	} catch (_: End) {
		throw RuntimeException()
	}
}
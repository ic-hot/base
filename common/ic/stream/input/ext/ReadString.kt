package ic.stream.input.ext


import ic.stream.input.ByteInput
import ic.stream.sequence.ext.toString
import ic.util.text.charset.Charset
import ic.util.text.charset.Charset.Companion.Utf8


fun ByteInput.readNullableString (charset: Charset = Utf8) : String? {

	val lengthInBytes = readInt32()

	if (lengthInBytes == -1) return null

	val bytes = read(lengthInBytes)

	return bytes.toString(charset = charset)

}


fun ByteInput.readString (charset: Charset = Utf8) : String {

	return readNullableString(charset = charset)!!

}
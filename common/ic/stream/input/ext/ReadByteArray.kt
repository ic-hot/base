package ic.stream.input.ext


import ic.stream.input.ByteInput


fun ByteInput.readByteArray () : ByteArray {

	val length = readInt32()

	return readToByteArray(amount = length)

}
package ic.stream.input.ext


import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.stream.buffer.ByteBuffer
import ic.stream.input.ByteInput
import ic.stream.sequence.ByteSequence


fun ByteInput.readToByteBuffer() : ByteBuffer {
	val byteBuffer = ByteBuffer()
	try {
		loop {
			byteBuffer.putByte(
				getNextByteOrThrowEnd()
			)
		}
	} catch (_: End) {}
	return byteBuffer
}


@Throws(End::class)
fun ByteInput.readToByteBufferOrThrowEnd (amount: Int32) : ByteBuffer {
	val byteBuffer = ByteBuffer()
	repeat (amount) {
		byteBuffer.putByte(
			getNextByteOrThrowEnd()
		)
	}
	return byteBuffer
}

inline fun ByteInput.readToByteBuffer (amount: Int32) : ByteBuffer {
	try {
		return readToByteBufferOrThrowEnd(amount)
	} catch (_: End) {
		throw End.Runtime()
	}
}


inline fun ByteInput.read() : ByteSequence = readToByteBuffer()

inline fun ByteInput.read (amount: Int32) : ByteSequence = readToByteBuffer(amount)

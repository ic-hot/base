package ic.stream.input.ext


import ic.stream.input.ByteInput
import ic.struct.map.editable.EditableMap


inline fun <Key, Value: Any> ByteInput.readEditableMapWithInt32Length (

	readKey : ByteInput.() -> Key,

	readValue : ByteInput.() -> Value

) : EditableMap<Key, Value> {

	val map = EditableMap<Key, Value>()

	repeat(readInt32()) {

		val key = readKey()

		val value = readValue()

		map[key] = value

	}

	return map

}
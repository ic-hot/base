package ic.stream.buffer.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.throwables.IndexOutOfBoundsException
import ic.stream.buffer.ByteBuffer


@Throws(IndexOutOfBoundsException::class)
@Suppress("NOTHING_TO_INLINE")
inline fun ByteBuffer.getByteOrThrowIndexOutOfBounds (index: Int32) : Byte {
	return getByteOrThrowIndexOutOfBounds(index = index.asInt64)
}
package ic.stream.buffer


inline fun ByteBuffer (

	crossinline onClose : ByteBuffer.() -> Unit

) : ByteBuffer {

	return object : ByteBuffer() {

		override fun onClose() {
			onClose(this)
		}

	}

}
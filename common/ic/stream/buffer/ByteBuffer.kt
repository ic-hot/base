package ic.stream.buffer


import ic.math.annotations.PositiveOrZero
import ic.base.annotations.Valid
import ic.base.arrays.bytes.ByteArray
import ic.base.arrays.ext.copy
import ic.base.arrays.bytes.ext.get
import ic.base.arrays.ext.length
import ic.base.arrays.ext.set
import ic.base.primitives.int64.Int64
import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray
import ic.base.throwables.IndexOutOfBoundsException
import ic.stream.sequence.BaseByteSequence
import ic.struct.list.List


open class ByteBuffer : BaseByteSequence(), List<Byte>, ByteOutput, Mutable {


	protected open fun onClose() {}


	override val mutex = Mutex()

	private var array = ByteArray(64)

	final override var length = 0L; private set

	private var isClosed = false

	override fun newIterator() : ByteInput {
		return ByteSequenceFromByteArray(toByteArray()).newIterator()
	}

	fun toByteArray() : ByteArray = mutex.synchronized {
		return array.copy(length)
	}

	override fun putByte (byte: Byte) {
		mutex.synchronized {
			if (isClosed) throw RuntimeException("Closed")
			if (length >= array.size) {
				array = array.copy(length = array.length * 2)
			}
			array[length] = byte
			length++
		}
	}

	override fun close() {
		mutex.synchronized {
			if (isClosed) throw RuntimeException("Already closed")
			isClosed = true
		}
		onClose()
	}

	override fun cancel() {}

	private inline fun getByte (@Valid @PositiveOrZero index: Int64) = array[index]

	@Throws(IndexOutOfBoundsException::class)
	fun getByteOrThrowIndexOutOfBounds (index: Int64) : Byte {
		mutex.synchronized {
			if (index < 0) throw IndexOutOfBoundsException
			if (index >= length) throw IndexOutOfBoundsException
			return getByte(index)
		}
	}


	override val count get() = length

	override fun getOrThrowIndexOutOfBounds (index: Int64): Byte {
		if (index < 0 || index >= length) throw IndexOutOfBoundsException
		return array[index]
	}


}
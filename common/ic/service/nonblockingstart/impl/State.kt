package ic.service.nonblockingstart.impl


internal sealed class State {


	abstract val isWorking : Boolean

	abstract val isOpen : Boolean


	object ReadyToStart : State() {
		override val isOpen get() = false
		override val isWorking get() = false
	}

	object Starting : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object FinishedWhileStarting : State() {
		override val isOpen get() = false
		override val isWorking get() = false
	}

	object Running : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object Stopping : State() {
		override val isOpen get() = false
		override val isWorking get() = true
	}

	object StoppingToStartAgain : State() {
		override val isOpen get() = false
		override val isWorking get() = true
	}

	object NotUsableAnymore : State() {
		override val isOpen get() = false
		override val isWorking get() = false
	}


}
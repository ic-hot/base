package ic.service.nonblockingstart


import ic.base.assert.assert
import ic.base.throwables.NotNeededException
import ic.parallel.mutex.Synchronized
import ic.parallel.mutex.synchronized
import ic.service.BaseService
import ic.service.impl.*
import ic.service.impl.Scheduled
import ic.service.impl.isWaitingForStop
import ic.service.impl.throwNotReusable
import ic.service.impl.throwWrongStateRuntime
import ic.service.nonblockingstart.impl.State
import ic.service.nonblockingstart.impl.State.*


abstract class NonBlockingStartService (isReusable: Boolean)
	: BaseService(isReusable = isReusable)
{


	internal var state : State = ReadyToStart

	final override val isOpen get() = state.isOpen

	final override val isWorking get() = state.isWorking


	protected abstract fun implementStartNonBlocking()


	@Throws(NotNeededException::class)
	override fun startBlockingOrThrowNotNeeded() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> {
					state = Starting
					implementStartNonBlocking()
					when (state) {
						Starting -> {
							state = Running
							onStarted()
						}
						FinishedWhileStarting -> {
							state = if (isReusable) ReadyToStart else NotUsableAnymore
						}
						else -> throwWrongStateRuntime(state)
					}
				}
				Starting -> throw NotNeededException
				FinishedWhileStarting -> throwWrongStateRuntime(state)
				Running -> throw NotNeededException
				Stopping, StoppingToStartAgain -> throwWrongStateRuntime(state)
				NotUsableAnymore -> throwNotReusable()
			}
		}
	}


	@Throws(NotNeededException::class)
	override fun startNonBlockingOrThrowNotNeeded() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> {
					state = Starting
					implementStartNonBlocking()
					when (state) {
						Starting -> {
							state = Running
							onStarted()
						}
						FinishedWhileStarting -> {
							state = if (isReusable) ReadyToStart else NotUsableAnymore
						}
						else -> throwWrongStateRuntime(state)
					}
				}
				Starting -> throw NotNeededException
				FinishedWhileStarting -> throwWrongStateRuntime(state)
				Running -> throw NotNeededException
				Stopping -> {
					if (!isReusable) throwNotReusable()
					if (isWaitingForStop) throwWaitingForStop()
					state = StoppingToStartAgain
					return
				}
				StoppingToStartAgain -> throw NotNeededException
				NotUsableAnymore -> throwNotReusable()
			}
		}
	}


	override fun notifyFinished() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> throwWrongStateRuntime(state)
				Starting -> {
					state = FinishedWhileStarting
					onFinished()
				}
				FinishedWhileStarting -> throwWrongStateRuntime(state)
				Running, Stopping -> {
					state = if (isReusable) ReadyToStart else NotUsableAnymore
					onFinished()
				}
				StoppingToStartAgain -> {
					assert { isReusable }
					implementStartNonBlocking()
					state = Running
					onStarted()
				}
				NotUsableAnymore -> throwWrongStateRuntime(state)
			}
		}
	}


	@Synchronized
	@Throws(NotNeededException::class, Scheduled::class)
	final override fun onStopNonBlockingOrThrow() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> throw NotNeededException
				Starting -> throwWrongStateRuntime(state)
				FinishedWhileStarting -> throwWrongStateRuntime(state)
				Running -> {
					state = Stopping
					callOnStopping()
				}
				Stopping -> throw NotNeededException
				StoppingToStartAgain -> {
					state = Stopping
				}
				NotUsableAnymore -> throw NotNeededException
			}
		}
	}


	@Synchronized
	final override fun onStartWaitingOrThrowNotNeeded() {
		when (state) {
			ReadyToStart -> throw NotNeededException
			Starting -> {}
			FinishedWhileStarting -> throwWrongStateRuntime(state)
			Running -> {}
			Stopping -> {}
			StoppingToStartAgain -> throwCantWait()
			NotUsableAnymore -> throw NotNeededException
		}
	}


}
package ic.service.blockingstart


import ic.base.assert.assert
import ic.base.annotations.Blocking
import ic.base.throwables.NotNeededException
import ic.parallel.mutex.Synchronized
import ic.parallel.mutex.synchronized
import ic.service.BaseService
import ic.service.blockingstart.impl.State
import ic.service.blockingstart.impl.State.*
import ic.service.blockingstart.impl.implementStartNonBlocking
import ic.service.impl.*
import ic.service.impl.isWaitingForStop
import ic.service.impl.throwNotReusable
import ic.service.impl.throwWrongStateRuntime


abstract class BlockingStartService (isReusable: Boolean)
	: BaseService(isReusable = isReusable)
{


	internal var state : State = ReadyToStart

	final override val isOpen get() = state.isOpen

	final override val isWorking get() = state.isWorking


	@Blocking
	protected abstract fun implementStartBlocking()

	@Blocking
	internal fun callImplementStartBlocking() {
		implementStartBlocking()
	}

	@Synchronized
	protected open fun onStarting() {}

	@Synchronized
	internal fun callOnStarting() {
		onStarting()
	}

	@Synchronized
	protected open fun onFailedToStart (exception: Exception) {}

	@Synchronized
	internal fun callOnFailedToStart (exception: Exception) {
		onFailedToStart(exception)
	}

	@Blocking
	@Throws(NotNeededException::class)
	final override fun startBlockingOrThrowNotNeeded() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> {
					state = StartingBlocking
					onStarting()
				}
				StartingBlocking -> throwWrongStateRuntime(state)
				StartingNonBlocking -> throwWrongStateRuntime(state)
				StartingToStopAgain -> throwWrongStateRuntime(state)
				StartingToStopAndStartAgain -> throwWrongStateRuntime(state)
				Running -> throw NotNeededException
				StoppingToStartAgain -> throwWrongStateRuntime(state)
				Stopping -> throwWrongStateRuntime(state)
				NotUsableAnymore -> throwNotReusable()
			}
		}
		try {
			implementStartBlocking()
			stateMutex.synchronized {
				when (state) {
					ReadyToStart -> {}
					StartingBlocking -> {
						state = Running
						onStarted()
					}
					NotUsableAnymore -> {}
					else -> throwWrongStateError(state)
				}
			}
		} catch (t: Throwable) {
			stateMutex.synchronized {
				state = ReadyToStart
			}
			if (t is Exception) {
				onFailedToStart(t)
			}
			throw t
		}
	}

	@Throws(NotNeededException::class)
	final override fun startNonBlockingOrThrowNotNeeded() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> {
					state = StartingNonBlocking
					implementStartNonBlocking()
				}
				StartingBlocking -> throw NotNeededException
				StartingNonBlocking -> throw NotNeededException
				StartingToStopAgain -> {
					if (!isReusable) throwNotReusable()
					if (isWaitingForStop) throwWaitingForStop()
					state = StartingToStopAndStartAgain
				}
				StartingToStopAndStartAgain -> throw NotNeededException
				Running -> throw NotNeededException
				Stopping -> {
					if (!isReusable) throwNotReusable()
					if (isWaitingForStop) throwWaitingForStop()
					state = StoppingToStartAgain
				}
				StoppingToStartAgain -> throw NotNeededException
				NotUsableAnymore -> throwNotReusable()
			}
		}
	}


	override fun notifyFinished() {
		stateMutex.synchronized {
			when (state) {
				ReadyToStart -> throwWrongStateRuntime(state)
				StartingBlocking, StartingNonBlocking, StartingToStopAgain -> {
					state = if (isReusable) ReadyToStart else NotUsableAnymore
					onFinished()
				}
				StartingToStopAndStartAgain -> {
					assert { isReusable }
					state = StartingNonBlocking
					implementStartNonBlocking()
				}
				Running, Stopping -> {
					state = if (isReusable) ReadyToStart else NotUsableAnymore
					onFinished()
				}
				StoppingToStartAgain -> {
					assert { isReusable }
					state = StartingNonBlocking
					implementStartNonBlocking()
				}
				NotUsableAnymore -> throwWrongStateRuntime(state)
			}
		}
	}


	@Synchronized
	@Throws(NotNeededException::class, Scheduled::class)
	final override fun onStopNonBlockingOrThrow() {
		when (state) {
			ReadyToStart -> throw NotNeededException
			StartingBlocking -> throwWaitingForStart()
			StartingNonBlocking -> {
				state = StartingToStopAgain
			}
			StartingToStopAgain -> throw NotNeededException
			StartingToStopAndStartAgain -> {
				state = StartingToStopAgain
				throw Scheduled
			}
			Running -> {
				state = Stopping
				callOnStopping()
			}
			Stopping -> throw NotNeededException
			StoppingToStartAgain -> {
				state = Stopping
			}
			NotUsableAnymore -> throw NotNeededException
		}
	}


	@Synchronized
	final override fun onStartWaitingOrThrowNotNeeded() {
		when (state) {
			ReadyToStart -> throw NotNeededException
			StartingBlocking -> {}
			StartingNonBlocking -> {}
			StartingToStopAgain -> {}
			StartingToStopAndStartAgain -> throwCantWait()
			Running -> {}
			Stopping -> {}
			StoppingToStartAgain -> throwCantWait()
			NotUsableAnymore -> throw NotNeededException
		}
	}


}
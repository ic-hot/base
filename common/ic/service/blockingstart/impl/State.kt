package ic.service.blockingstart.impl


internal sealed class State {


	abstract val isWorking : Boolean

	abstract val isOpen : Boolean


	object ReadyToStart : State() {
		override val isOpen get() = false
		override val isWorking get() = false
	}

	object StartingBlocking : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object StartingNonBlocking : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object StartingToStopAgain : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object StartingToStopAndStartAgain : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object Running : State() {
		override val isOpen get() = true
		override val isWorking get() = true
	}

	object Stopping : State() {
		override val isOpen get() = false
		override val isWorking get() = true
	}

	object StoppingToStartAgain : State() {
		override val isOpen get() = false
		override val isWorking get() = true
	}

	object NotUsableAnymore : State() {
		override val isOpen get() = false
		override val isWorking get() = false
	}


}
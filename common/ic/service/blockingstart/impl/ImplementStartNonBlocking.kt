package ic.service.blockingstart.impl


import ic.parallel.funs.doInBackground
import ic.parallel.mutex.Synchronized
import ic.parallel.mutex.synchronized
import ic.service.blockingstart.BlockingStartService
import ic.service.blockingstart.impl.State.*
import ic.service.impl.throwWrongStateError


@Synchronized
internal fun BlockingStartService.implementStartNonBlocking() {

	callOnStarting()

	doInBackground {

		try {

			callImplementStartBlocking()

			stateMutex.synchronized {
				when (state) {
					ReadyToStart -> {}
					StartingBlocking -> throwWrongStateError(state)
					StartingNonBlocking -> {
						state = Running
						callOnStarted()
					}
					StartingToStopAgain -> {
						state = Stopping
						callOnStopping()
						callImplementStopNonBlocking()
					}
					StartingToStopAndStartAgain -> {
						state = StoppingToStartAgain
						callOnStopping()
						callImplementStopNonBlocking()
					}
					Running -> throwWrongStateError(state)
					Stopping -> throwWrongStateError(state)
					StoppingToStartAgain -> throwWrongStateError(state)
					NotUsableAnymore -> {}
				}
			}

		} catch (t: Throwable) {

			state = ReadyToStart

			if (t is Exception) {
				callOnFailedToStart(t)
			}

		}

	}

}
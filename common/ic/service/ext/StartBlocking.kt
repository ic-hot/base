package ic.service.ext


import ic.base.throwables.NotNeededException
import ic.service.Service


@Suppress("NOTHING_TO_INLINE")
inline fun Service.startBlocking() {
	try {
		startBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {
		throw RuntimeException("Service is already started")
	}
}
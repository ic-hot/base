package ic.service.ext


import ic.base.throwables.NotNeededException
import ic.service.Service


fun Service.startNonBlocking() {
	try {
		startNonBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {
		throw RuntimeException("Service is already started")
	}
}


fun Service.startNonBlockingIfNeeded() {
	try {
		startNonBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {}
}
package ic.service


import ic.base.annotations.Blocking
import ic.base.annotations.ToOverride
import ic.base.escape.breakable.Breakable
import ic.base.primitives.int32.Int32
import ic.base.throwables.NotNeededException
import ic.design.task.scope.BaseTaskScope
import ic.parallel.funs.sleepWhile
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.Synchronized
import ic.parallel.mutex.synchronized
import ic.service.impl.Scheduled


abstract class BaseService (

	val isReusable : Boolean

) : BaseTaskScope(), Service {


	internal val stateMutex = Mutex()

	internal var waitingThreadsCount : Int32 = 0


	@Synchronized
	protected open fun onStarted() {}

	@Synchronized
	internal fun callOnStarted() {
		onStarted()
	}


	protected open fun onFinished() {}

	/**
	 * Called by Service itself when it has finished or has been stopped
	 */
	protected abstract fun notifyFinished()


	protected abstract fun implementStopNonBlocking()

	internal fun callImplementStopNonBlocking() {
		implementStopNonBlocking()
	}

	@Synchronized
	@Breakable
	@Throws(NotNeededException::class, Scheduled::class)
	internal abstract fun onStopNonBlockingOrThrow()

	@Throws(NotNeededException::class)
	final override fun stopNonBlockingOrThrowNotNeeded() {
		try {
			stateMutex.synchronized {
				onStopNonBlockingOrThrow()
			}
			implementStopNonBlocking()
		} catch (_: Scheduled) {}
	}

	@Synchronized
	protected open fun onStopping() {}

	@Synchronized
	internal fun callOnStopping() {
		onStopping()
	}


	@ToOverride
	@Blocking
	protected open fun implementWaitFor() {
		sleepWhile { isWorking }
	}

	@Synchronized
	@Throws(NotNeededException::class)
	internal abstract fun onStartWaitingOrThrowNotNeeded()

	final override fun waitFor() {
		stateMutex.synchronized {
			try {
				onStartWaitingOrThrowNotNeeded()
			} catch (_: NotNeededException) {
				return
			}
			waitingThreadsCount++
		}
		try {
			implementWaitFor()
		} finally {
			waitingThreadsCount--
		}
	}


}

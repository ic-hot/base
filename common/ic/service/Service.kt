package ic.service


import ic.base.annotations.Blocking
import ic.base.throwables.NotNeededException
import ic.ifaces.stoppable.Stoppable


interface Service : Stoppable {


	val isWorking : Boolean


	@Blocking
	@Throws(NotNeededException::class)
	fun startBlockingOrThrowNotNeeded()

	@Throws(NotNeededException::class)
	fun startNonBlockingOrThrowNotNeeded()


}
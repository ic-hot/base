package ic.service.impl


import ic.base.reflect.ext.className
import ic.service.BaseService


internal fun BaseService.throwNotReusable() : Nothing {

	throw RuntimeException(
		"Service $className is not reusable"
	)

}
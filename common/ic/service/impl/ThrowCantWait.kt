package ic.service.impl


internal fun throwCantWait() : Nothing {

	throw RuntimeException(
		"The service is scheduled to restart. " +
		"Waiting for it now may cause inconsistencies."
	)

}
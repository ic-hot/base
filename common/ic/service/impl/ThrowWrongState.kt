package ic.service.impl


import ic.base.reflect.ext.simpleClassName


internal fun throwWrongStateRuntime (state: Any) : Nothing {

	throw RuntimeException(
		"state: ${ state.simpleClassName }"
	)

}


internal fun throwWrongStateError (state: Any) : Nothing {

	throw Error(
		"state: ${ state.simpleClassName }"
	)

}
package ic.service.impl


internal fun throwWaitingForStart() {

	throw RuntimeException(
		"There is at least one thread waiting for the service to start. " +
		"Stopping it now may cause state inconsistencies."
	)

}


internal fun throwWaitingForStop() {

	throw RuntimeException(
		"There is at least one thread waiting for the service to stop. " +
		"Restarting it now may cause state inconsistencies."
	)

}
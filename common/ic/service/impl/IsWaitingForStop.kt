package ic.service.impl


import ic.service.BaseService


internal inline val BaseService.isWaitingForStop get() = waitingThreadsCount > 0
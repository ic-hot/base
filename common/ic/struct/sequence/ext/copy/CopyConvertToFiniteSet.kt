package ic.struct.sequence.ext.copy


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.sequence.Sequence
import ic.struct.sequence.ext.foreach.forEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


inline fun <Item, NewItem> Sequence<Item>.copyConvertToFiniteSet (

	@Skippable convertItem: (Item) -> NewItem

) : FiniteSet<NewItem> {

	val set = EditableSet<NewItem>()

	forEach { item ->
		skippable {
			set.addIfNotExists(
				convertItem(item)
			)
		}
	}

	return set

}
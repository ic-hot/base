package ic.struct.sequence.ext.cast


import ic.base.kcollections.KotlinIterableFromSequence
import ic.struct.sequence.Sequence


inline val <Item> Sequence<Item>.asKotlinIterable : kotlin.collections.Iterable<Item> get() {
	return KotlinIterableFromSequence(this)
}
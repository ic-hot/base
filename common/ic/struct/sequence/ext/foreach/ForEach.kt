package ic.struct.sequence.ext.foreach


import ic.base.loop.breakableLoop
import ic.base.throwables.End
import ic.ifaces.action.action1.Action1
import ic.struct.sequence.Sequence


inline fun <Item> Sequence<Item>.forEach (

	onBreak : () -> Unit = {},

	action : (Item) -> Unit

) {
	val iterator = newIterator()
	try {
		breakableLoop(onBreak = onBreak) {
			val item = iterator.getNextOrThrowEnd()
			action(item)
		}
	} catch (_: End) {}
}


inline fun <Item> Sequence<Item>.forEach (

	onBreak : () -> Unit = {},

	action : Action1<Item>

) {

	forEach(onBreak = onBreak) { action.run(it) }

}
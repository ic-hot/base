package ic.struct.sequence


import ic.ifaces.iterator.Iterator
import ic.struct.collection.Collection


/**
 * Sequence is a data structure that mainly implements newIterator() method.
 * Sequence is strictly ordered.
 * Can be infinite or finite.
 */
interface Sequence<out Item> : Collection<Item> {


	fun newIterator() : Iterator<Item>


}

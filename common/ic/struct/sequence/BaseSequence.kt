package ic.struct.sequence


import ic.base.escape.breakable.Breakable
import ic.base.loop.loop
import ic.base.throwables.End
import ic.struct.collection.BaseCollection


abstract class BaseSequence<Item> : BaseCollection<Item>(), Sequence<Item> {


	@Breakable
	override fun invoke (action: (Item) -> Unit) {
		val iterator = newIterator()
		try {
			loop {
				val item = iterator.getNextOrThrowEnd()
				action(item)
			}
		} catch (_: End) {}
	}


}
package ic.struct.sequence.from.array


import ic.base.annotations.Throws
import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.ifaces.iterator.Iterator
import ic.struct.sequence.BaseSequence


class SequenceFromArray<Item> (

	private val array : Array<*>

) : BaseSequence<Item>() {

	override fun newIterator() : Iterator<Item> {
		return object : Iterator<Item> {
			var index : Int32 = 0
			@Throws(End::class)
			override fun getNextOrThrowEnd() : Item {
				if (index < array.length) {
					@Suppress("UNCHECKED_CAST")
					return array[index++] as Item
				} else {
					throw End
				}
			}
		}
	}

}
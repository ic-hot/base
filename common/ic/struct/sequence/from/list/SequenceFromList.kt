package ic.struct.sequence.from.list


import ic.base.annotations.Throws
import ic.base.primitives.int64.Int64
import ic.base.throwables.End
import ic.ifaces.iterator.Iterator
import ic.struct.list.List
import ic.struct.sequence.BaseSequence


class SequenceFromList<Item> (

	private val list : List<*>

) : BaseSequence<Item>() {

	override fun newIterator() : Iterator<Item> {
		return object : Iterator<Item> {
			var index : Int64 = 0
			@Throws(End::class)
			override fun getNextOrThrowEnd() : Item {
				if (index < list.length) {
					@Suppress("UNCHECKED_CAST")
					return list[index++] as Item
				} else {
					throw End
				}
			}
		}
	}

}
@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
interface Order2<Item> {

	fun compare (a: Item, b: Item) : OrderRelation

	val asJavaComparator : kotlin.Comparator<Item> get() = object : kotlin.Comparator<Item> {
		override fun compare (a: Item, b: Item) = when (this@Order2.compare(a, b)) {
			OrderRelation.Less 	  -> -1
			OrderRelation.Greater -> +1
			OrderRelation.Equals  -> 0
		}
	}

}
@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
class ReverseOrder2<Item> (

	private val sourceOrder2 : Order2<Item>

) : Order2<Item> {

	override fun compare (a : Item, b : Item) : OrderRelation {

		return sourceOrder2.compare(b, a)

	}

}
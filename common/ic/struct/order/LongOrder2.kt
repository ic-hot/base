@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
object LongOrder2 : Order2<Long> {

	@Suppress("KotlinConstantConditions")
	override fun compare (a: Long, b: Long) = when {
		a < b 	-> OrderRelation.Less
		a == b 	-> OrderRelation.Equals
		a > b	-> OrderRelation.Greater
		else	-> throw Error("a: $a, b: $b")
	}

}
@file:Suppress("DEPRECATION")


package ic.struct.order


import ic.base.primitives.int32.Int32


@Deprecated("Use normal compare Int32 values")
sealed class OrderRelation {


	abstract val inverse : OrderRelation


	abstract val asInt32 : Int32


	object Less : OrderRelation() {

		override val inverse get() = Greater

		override val asInt32 get() = -1

	}

	object Equals : OrderRelation() {

		override val inverse get() = this

		override val asInt32 get() = 0

	}

	object Greater : OrderRelation() {

		override val inverse get() = Less

		override val asInt32 get() = 1

	}


}
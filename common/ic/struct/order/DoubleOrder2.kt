@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
object DoubleOrder2 : Order2<Double> {

	override fun compare (a: Double, b: Double) = when {
		a < b 	-> OrderRelation.Less
		a == b 	-> OrderRelation.Equals
		a > b	-> OrderRelation.Greater
		else	-> throw Error("a: $a, b: $b")
	}

}
@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
class ConvertOrder2<Item, SourceItem> (

	private val sourceOrder2 : Order2<SourceItem>,

	private val itemConverter : (Item) -> SourceItem

) : Order2<Item> {

	override fun compare (a: Item, b: Item) : OrderRelation {

		return sourceOrder2.compare(itemConverter(a), itemConverter(b))

	}

}
@file:Suppress("DEPRECATION", "KotlinConstantConditions")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
object AlphabeticStringOrder2 : Order2<String> {

	override fun compare (a: String, b: String) = when {
		a < b 	-> OrderRelation.Less
		a == b 	-> OrderRelation.Equals
		a > b 	-> OrderRelation.Greater
		else	-> throw Error("a: $a, b: $b")
	}

}
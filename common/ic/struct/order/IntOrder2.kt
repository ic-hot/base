@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
object IntOrder2 : Order2<Int> {

	@Suppress("KotlinConstantConditions")
	override fun compare (a: Int, b: Int) = when {
		a < b 	-> OrderRelation.Less
		a == b 	-> OrderRelation.Equals
		a > b	-> OrderRelation.Greater
		else	-> throw Error("a: $a, b: $b")
	}

}
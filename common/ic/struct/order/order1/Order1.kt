@file:Suppress("DEPRECATION")


package ic.struct.order.order1


import ic.struct.order.Order2
import ic.struct.order.OrderRelation


@Deprecated("Use normal Int32 comparison")
interface Order1<Item> {

	fun compare (b: Item) : OrderRelation

	class FromOrder2AndFirstArg<Item> (private val order2: Order2<Item>, private val a: Item) :
		Order1<Item> {
		override fun compare (b: Item) : OrderRelation {
			return order2.compare(a, b)
		}
	}

	companion object

}
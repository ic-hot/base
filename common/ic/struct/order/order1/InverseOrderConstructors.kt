@file:Suppress("FunctionName", "NOTHING_TO_INLINE", "DEPRECATION")


package ic.struct.order.order1


@Deprecated("Use normal Int32 comparison")
inline fun <Item> InverseOrder1 (sourceOrder1: Order1<Item>) : InverseOrder1<Item> = object : InverseOrder1<Item>() {

	override val sourceOrder1 get() = sourceOrder1

}
@file:Suppress("DEPRECATION")


package ic.struct.order.order1


@Deprecated("Use normal Int32 comparison")
inline val <Item> Order1<Item>.inverse : Order1<Item> get() = InverseOrder1(this)
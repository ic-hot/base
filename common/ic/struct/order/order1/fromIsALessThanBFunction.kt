@file:Suppress("DEPRECATION")

package ic.struct.order.order1


import ic.base.primitives.bool.ext.then
import ic.struct.order.OrderRelation


@Deprecated("Use normal Int32 comparison")
inline fun <Item> Order1.Companion.fromIsALessThanBFunction (

	crossinline isALessThanB : (b: Item) -> Boolean

) : Order1<Item> = object : Order1<Item> {

	override fun compare (b: Item) = isALessThanB(b).then(OrderRelation.Less, OrderRelation.Greater)

}
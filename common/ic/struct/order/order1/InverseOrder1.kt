@file:Suppress("DEPRECATION")


package ic.struct.order.order1


import ic.struct.order.OrderRelation


@Deprecated("Use normal Int32 comparison")
abstract class InverseOrder1<Item> : Order1<Item> {

	protected abstract val sourceOrder1 : Order1<Item>

	override fun compare (b: Item) : OrderRelation {

		return sourceOrder1.compare(b).inverse

	}

}
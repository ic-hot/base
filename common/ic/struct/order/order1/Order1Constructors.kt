@file:Suppress("FunctionName", "DEPRECATION")


package ic.struct.order.order1


import ic.struct.order.OrderRelation


@Deprecated("Use normal Int32 comparison")
inline fun <Item> Order1 (crossinline lambda: (b: Item) -> OrderRelation) : Order1<Item> = object : Order1<Item> {

	override fun compare (b: Item) = lambda(b)

}
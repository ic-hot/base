package ic.struct.order.set.default.impl


import ic.base.primitives.int64.Int64


data class FoundIndex (

	val index : Int64,

	val hasItem : Boolean

)
package ic.struct.order.set.default.impl


import ic.base.loop.loop
import ic.base.primitives.int64.Int64
import ic.struct.order.set.default.DefaultOrderedEditableSet


/**
 * Returns (index * 2 + hasItem).
 * Yes, this is crazy and stupid, but we want maximum performance of this thing.
 */
internal fun <Item> DefaultOrderedEditableSet<Item>.findIndexAndHasItem (

	target : Item

) : Int64 {

	var intervalStart = 0L
	var intervalEnd = list.length

	loop {

		val intervalLength = intervalEnd - intervalStart

		val middleIndex = when {
			intervalLength < 0 -> {
				throw Error("intervalLength == $intervalLength")
			}
			intervalLength == 0L -> {
				return intervalStart * 2 // index: intervalStart, hasItem: false
			}
			else -> {
				intervalStart + intervalLength / 2
			}
		}

		val middleItem = list[middleIndex]

		val targetToMiddleItem = compareItems(target, middleItem)

		when {
			targetToMiddleItem < 0 -> {
				intervalEnd = middleIndex
			}
			targetToMiddleItem > 0 -> {
				intervalStart = middleIndex + 1
			}
			else -> {
				return middleIndex * 2 + 1 // index: middleIndex, hasItem: true
			}
		}

	}

}
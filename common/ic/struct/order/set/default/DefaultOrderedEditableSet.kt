package ic.struct.order.set.default


import ic.base.compare.Comparison
import ic.base.escape.breakable.Break
import ic.base.escape.breakable.Breakable
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.ifaces.action.action1.Action1
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.list.ext.foreach.forEach
import ic.struct.order.set.default.impl.findIndexAndHasItem
import ic.struct.set.editable.BaseEditableSet


abstract class DefaultOrderedEditableSet<Item> : BaseEditableSet<Item>() {


	abstract fun compareItems (a: Item, b: Item) : Comparison


	internal val list = EditableList<Item>()


	// Non-trivial:

	override fun contains (item: Item) : Boolean {
		val indexAndHasItem = findIndexAndHasItem(item)
		val hasItem = indexAndHasItem % 2 != 0L
		return hasItem
	}

	@Throws(AlreadyExistsException::class)
	override fun addOrThrowAlreadyExists (item: Item) {
		val indexAndHasItem = findIndexAndHasItem(item)
		val index = indexAndHasItem / 2
		val hasItem = indexAndHasItem % 2 != 0L
		if (hasItem) {
			throw AlreadyExistsException
		} else {
			list.insert(index, item)
		}
	}

	@Throws(NotExistsException::class)
	override fun removeOrThrowNotExists(item: Item) {
		val indexAndHasItem = findIndexAndHasItem(item)
		val index = indexAndHasItem / 2
		val hasItem = indexAndHasItem % 2 != 0L
		if (hasItem) {
			list.remove(index)
		} else {
			throw NotExistsException
		}
	}


	// Trivial:

	@Breakable
	override fun invoke (action: (Item) -> Unit) {
		list.forEach(action)
	}

	override fun empty() {
		list.empty()
	}


}
package ic.struct.order.set


import ic.struct.set.editable.EditableSet


interface OrderedEditableSet<Item> : OrderedFiniteSet<Item>, EditableSet<Item> {



}
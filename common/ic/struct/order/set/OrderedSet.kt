package ic.struct.order.set


import ic.struct.set.Set


interface OrderedSet<Item> : Set<Item>
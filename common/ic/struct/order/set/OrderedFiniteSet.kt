package ic.struct.order.set


import ic.struct.set.finite.FiniteSet
import ic.struct.list.List


interface OrderedFiniteSet<Item> : OrderedSet<Item>, FiniteSet<Item>, List<Item> {



}
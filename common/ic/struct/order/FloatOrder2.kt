@file:Suppress("DEPRECATION")


package ic.struct.order


@Deprecated("Use normal Int32 comparison")
object FloatOrder2 : Order2<Float> {

	override fun compare (a: Float, b: Float) = when {
		a < b 	-> OrderRelation.Less
		a == b 	-> OrderRelation.Equals
		a > b	-> OrderRelation.Greater
		else	-> throw Error("a: $a, b: $b")
	}

}
package ic.struct.grid.grid2d.cached


import ic.base.primitives.int64.Int64
import ic.struct.grid.grid2d.Grid2


inline fun <Item: Any> CachedGrid2 (

	crossinline initItem : (x: Int64, y: Int64) -> Item

) : Grid2<Item> {

	return object : CachedGrid2<Item>() {

		override fun initItem (x: Int64, y: Int64) : Item {
			return initItem(x, y)
		}

	}

}
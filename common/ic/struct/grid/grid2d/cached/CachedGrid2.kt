package ic.struct.grid.grid2d.cached


import ic.struct.grid.grid2d.editable.default.DefaultEditableGrid2WithGeneratingItems


abstract class CachedGrid2 <Item: Any> : DefaultEditableGrid2WithGeneratingItems<Item>() {

	override val toCacheGeneratedItems get() = true

}
package ic.struct.grid.grid2d


import ic.base.primitives.int64.Int64


interface Grid2<Item> {

	operator fun get (x: Int64, y: Int64) : Item

}
package ic.struct.grid.grid2d.editable


import ic.base.primitives.int64.Int64
import ic.struct.grid.grid2d.Grid2


interface EditableGrid2<Item> : Grid2<Item> {

	operator fun set (x: Int64, y: Int64, item: Item)

}
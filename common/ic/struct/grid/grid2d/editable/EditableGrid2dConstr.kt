package ic.struct.grid.grid2d.editable


import ic.base.primitives.int32.Int32
import ic.struct.grid.grid2d.editable.default.DefaultEditableGrid2InitializedWithNulls


@Suppress("FunctionName", "NOTHING_TO_INLINE")
inline fun <Item: Any> EditableGrid2d (
	chunkArrayLength : Int32 = 256
) : EditableGrid2<Item?> {
	return DefaultEditableGrid2InitializedWithNulls(
		chunkArrayLength = chunkArrayLength
	)
}
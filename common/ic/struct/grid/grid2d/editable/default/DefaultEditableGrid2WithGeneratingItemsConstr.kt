package ic.struct.grid.grid2d.editable.default


import ic.base.primitives.int64.Int64


@Suppress("FunctionName")
inline fun <Item: Any> DefaultEditableGrid2WithGeneratingItems (

	toCacheGeneratedItems : Boolean,

	crossinline initItem : (x: Int64, y: Int64) -> Item

) : DefaultEditableGrid2WithGeneratingItems<Item> {

	return object : DefaultEditableGrid2WithGeneratingItems<Item>() {

		override val toCacheGeneratedItems get() = toCacheGeneratedItems

		override fun initItem (x: Int64, y: Int64) : Item {
			return initItem(x, y)
		}

	}

}
package ic.struct.grid.grid2d.editable.default


import ic.base.primitives.int64.Int64


abstract class DefaultEditableGrid2WithGeneratingItems<Item: Any>

	: BaseDefaultEditableGrid2<Item>()

{


	protected abstract val toCacheGeneratedItems : Boolean

	protected abstract fun initItem (x: Int64, y: Int64) : Item


	override fun get (x: Int64, y: Int64) : Item {
		val existingItem = internalGet(x, y)
		if (existingItem == null) {
			val generatedItem = initItem(x, y)
			if (toCacheGeneratedItems) {
				set(x, y, generatedItem)
			}
			return generatedItem
		} else {
			@Suppress("UNCHECKED_CAST")
			return existingItem as Item
		}
	}


}
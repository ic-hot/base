package ic.struct.grid.grid2d.editable.default.impl


import ic.base.arrays.ext.get.get
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.grid.grid2d.editable.default.BaseDefaultEditableGrid2


internal fun BaseDefaultEditableGrid2<*>.implementGet (

	array : Array<Any?>,

	capacity : Int64,

	index : Int64

) : Any? {

	if (capacity == chunkArrayLength.asInt64) {
		@Suppress("UNCHECKED_CAST")
		return array[index]
	}

	val childCapacity = capacity / chunkArrayLength

	val childIndex = index / childCapacity

	val childArray = array[childIndex] ?: return null

	@Suppress("UNCHECKED_CAST")
	childArray as Array<Any?>

	val indexInChild = index % childCapacity

	return implementGet(
		array = childArray,
		capacity = childCapacity,
		index = indexInChild
	)

}
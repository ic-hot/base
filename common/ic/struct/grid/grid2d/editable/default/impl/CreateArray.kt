package ic.struct.grid.grid2d.editable.default.impl


import ic.base.arrays.Array
import ic.struct.grid.grid2d.editable.default.BaseDefaultEditableGrid2


@Suppress("NOTHING_TO_INLINE")
internal inline fun BaseDefaultEditableGrid2<*>.createArray() : Array<Any?> = (

	Array(length = chunkArrayLength)

)
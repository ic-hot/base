package ic.struct.grid.grid2d.editable.default.impl


import ic.struct.grid.grid2d.editable.default.BaseDefaultEditableGrid2


internal fun BaseDefaultEditableGrid2<*>.expand() {

	val newArray = createArray()
	newArray[0] = this.array
	this.array = newArray

	capacity *= chunkArrayLength

}
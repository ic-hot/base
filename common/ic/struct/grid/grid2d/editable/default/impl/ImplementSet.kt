package ic.struct.grid.grid2d.editable.default.impl


import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.set
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.grid.grid2d.editable.default.BaseDefaultEditableGrid2


internal fun BaseDefaultEditableGrid2<*>.implementSet (

	array : Array<Any?>,

	capacity : Int64,

	index : Int64,

	item : Any?

) {

	if (capacity == chunkArrayLength.asInt64) {
		array[index] = item
		return
	}

	val childCapacity = capacity / chunkArrayLength

	val childIndex = index / childCapacity

	val childArray = array[childIndex] ?: createArray().also { array[childIndex] = it }

	@Suppress("UNCHECKED_CAST")
	childArray as Array<Any?>

	val indexInChild = index % childCapacity

	implementSet(
		array = childArray,
		capacity = childCapacity,
		index = indexInChild,
		item = item
	)

}
package ic.struct.grid.grid2d.editable.default.impl


import ic.base.primitives.int64.Int64
import ic.base.primitives.uint64.ext.asInt64
import ic.math.map.map2dTo1d
import ic.math.map.mapSignedToUnsigned
import ic.struct.grid.grid2d.editable.default.BaseDefaultEditableGrid2


fun BaseDefaultEditableGrid2<*>.getIndex (x: Int64, y: Int64) : Int64 {

	return map2dTo1d(
		x = mapSignedToUnsigned(x),
		y = mapSignedToUnsigned(y)
	).asInt64

}
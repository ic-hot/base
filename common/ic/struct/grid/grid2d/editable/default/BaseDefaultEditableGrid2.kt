package ic.struct.grid.grid2d.editable.default


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.grid.grid2d.editable.EditableGrid2
import ic.struct.grid.grid2d.editable.default.impl.*
import ic.struct.grid.grid2d.editable.default.impl.expand
import ic.struct.grid.grid2d.editable.default.impl.implementGet
import ic.struct.grid.grid2d.editable.default.impl.implementSet


abstract class BaseDefaultEditableGrid2<Item> (

	internal val chunkArrayLength : Int32 = 256

) : EditableGrid2<Item> {


	internal var capacity : Int64 = chunkArrayLength.asInt64

	internal var array : Array<Any?>? = null


	protected fun internalGet (x: Int64, y: Int64) : Any? {
		val array = this.array ?: return null
		val index = getIndex(x, y)
		if (index >= capacity) return null
		return implementGet(
			array = array,
			capacity = capacity,
			index = index
		)
	}


	override fun set (x: Int64, y: Int64, item: Item) {
		if (item != null && array == null) {
			array = createArray()
		}
		val index = getIndex(x, y)
		while (capacity <= index) expand()
		implementSet(
			array = array!!,
			capacity = capacity,
			index = index,
			item = item
		)
	}


}
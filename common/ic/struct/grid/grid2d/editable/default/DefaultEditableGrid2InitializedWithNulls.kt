package ic.struct.grid.grid2d.editable.default


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


class DefaultEditableGrid2InitializedWithNulls<Item: Any>

	(
		chunkArrayLength : Int32 = 256
	)

	: BaseDefaultEditableGrid2<Item?>(chunkArrayLength = chunkArrayLength)

{

	override fun get (x: Int64, y: Int64) : Item? {
		@Suppress("UNCHECKED_CAST")
		return internalGet(x, y) as Item?
	}

}
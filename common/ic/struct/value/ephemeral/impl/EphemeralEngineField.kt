package ic.struct.value.ephemeral.impl


import ic.base.platform.basePlatform


internal inline val ephemeralEngine get() = basePlatform.ephemeralEngine
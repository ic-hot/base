package ic.struct.value.ephemeral.impl


import ic.struct.value.ephemeral.Ephemeral


internal interface EphemeralEngine {

	fun <Value: Any> createSoftReference (value: Value) : Ephemeral<Value>
	fun <Value: Any> createWeakReference (value: Value) : Ephemeral<Value>

}
package ic.struct.value.ephemeral


import ic.struct.value.ephemeral.impl.ephemeralEngine


@Suppress("FunctionName")
fun <Value: Any> Soft (value: Value) : Ephemeral<Value> {
	return ephemeralEngine.createSoftReference(value)
}
package ic.struct.value.ephemeral


import ic.struct.value.Val


interface Ephemeral<Value: Any> : Val<Value?>
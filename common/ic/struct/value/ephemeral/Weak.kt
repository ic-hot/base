package ic.struct.value.ephemeral


import ic.struct.value.ephemeral.impl.ephemeralEngine


@Suppress("FunctionName")
fun <Value: Any> Weak (value: Value) : Ephemeral<Value> {
	return ephemeralEngine.createWeakReference(value)
}
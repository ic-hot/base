package ic.struct.value.ext


import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.value.lateinit.LateInitVar


inline fun <Value> LateInitVar<Value>.getValueOrSetIfNull (

	getValueIfNull : () -> Value

) : Value {

	synchronizedIfMutable {

		return valueOrNull ?: getValueIfNull().also { value = it }

	}

}
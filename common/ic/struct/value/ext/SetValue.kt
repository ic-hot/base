@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.value.ext


import kotlin.reflect.KProperty

import ic.struct.value.Var


inline operator fun <Value> Var<Value>.setValue (
	thisRef: Any?, property: KProperty<*>, value: Value
) {
	this.value = value
}
package ic.struct.value.ext


import kotlin.reflect.KProperty

import ic.struct.value.Val


inline operator fun <Value> Val<Value>.getValue (
	thisRef: Any?, property: KProperty<*>
) : Value {
	return value
}
package ic.struct.value.ext


import ic.base.primitives.int64.Int64
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.value.Var


fun Var<Int64>.postIncrement() = synchronizedIfMutable { value++ }
package ic.struct.value


class DefaultVar<Value> (override var value: Value) : Var<Value> {

	override val valueOrNull get() = value

}
package ic.struct.value


interface Val<Value> : Function0<Value> {


	val value : Value

	val valueOrNull : Value? get() = value


	override fun invoke() = value


}
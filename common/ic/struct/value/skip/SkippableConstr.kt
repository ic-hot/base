package ic.struct.value.skip


import ic.base.escape.skip.skip
import ic.struct.value.Val


inline fun <Value> Skippable (

	crossinline isExists : () -> Boolean,

	sourceVal : Val<Value>

) : Skippable<Value> {

	return object : Skippable<Value>() {

		override val value : Value get() {
			if (isExists()) {
				return sourceVal.value
			} else {
				skip
			}
		}

	}

}
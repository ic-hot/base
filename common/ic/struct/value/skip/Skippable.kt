package ic.struct.value.skip


import ic.base.escape.skip.skippable
import ic.struct.value.Val


abstract class Skippable<Value> : Val<Value> {


	override val valueOrNull : Value? get() {
		skippable(
			onSkip = { return null }
		) {
			return value
		}
	}


}
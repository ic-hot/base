package ic.struct.value


class ConstantVal<Value> (

	override val value : Value

) : Val<Value> {

	override val valueOrNull get() = value
}
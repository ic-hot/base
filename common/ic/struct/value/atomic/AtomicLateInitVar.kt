package ic.struct.value.atomic


import ic.base.throwables.NotExistsException
import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.struct.value.lateinit.LateInitVar


class AtomicLateInitVar<Value> : LateInitVar<Value>, Mutable {


	override val mutex = Mutex()

	private var isInitialized : Boolean = false

	private var valueField : Value? = null


	override val valueOrNull : Value?
		get() {
			mutex.synchronized {
				if (isInitialized) {
					@Suppress("UNCHECKED_CAST")
					return valueField as Value
				} else {
					return null
				}
			}
		}
	;


	override var value : Value
		get() {
			mutex.synchronized {
				if (isInitialized) {
					@Suppress("UNCHECKED_CAST")
					return valueField as Value
				} else {
					throw RuntimeException()
				}
			}
		}
		set(value) {
			mutex.synchronized {
				valueField = value
				isInitialized = true
			}
		}
	;


}
package ic.struct.value.atomic


import ic.struct.value.lateinit.LateInitVar


@Suppress("NOTHING_TO_INLINE")
inline fun <Value> Atomic() : LateInitVar<Value> {

	return AtomicLateInitVar()

}
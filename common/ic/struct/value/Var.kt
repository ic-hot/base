package ic.struct.value


import ic.ifaces.mutable.ext.synchronizedIfMutable


interface Var<Value> : Val<Value> {


	override var value : Value


	fun setValueIfNull (value: Value) {
		synchronizedIfMutable {
			if (this.value == null) {
				this.value = value
			}
		}
	}


}

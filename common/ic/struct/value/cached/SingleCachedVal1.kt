package ic.struct.value.cached


import ic.struct.value.Val


abstract class SingleCachedVal1<Key, Value> : Val<Value> {


	protected abstract fun getKey() : Key

	protected abstract fun initValue (key: Key) : Value


	private var isKeySet : Boolean = false

	private var cacheKey : Key? = null

	private var cacheValue : Value? = null


	override val value : Value get() {
		val key = getKey()
		if (isKeySet) {
			if (cacheKey == key) {
				@Suppress("UNCHECKED_CAST")
				return cacheValue as Value
			} else {
				cacheKey = key
				val value = initValue(key)
				cacheValue = value
				return value
			}
		} else {
			isKeySet = true
			cacheKey = key
			val value = initValue(key)
			cacheValue = value
			return value
		}
	}


}
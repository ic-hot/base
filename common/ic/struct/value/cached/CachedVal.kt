package ic.struct.value.cached


import ic.struct.value.Val


abstract class CachedVal<Value> : Val<Value> {


	protected abstract fun initValue() : Value


	private var valueField : Value? = null

	private var isInitialized : Boolean = false


	override val value : Value get() {
		if (!isInitialized) {
			valueField = initValue()
			isInitialized = true
		}
		@Suppress("UNCHECKED_CAST")
		return valueField as Value
	}


	override val valueOrNull : Value? get() {
		return valueField
	}


}
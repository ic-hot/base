package ic.struct.value.cached


inline fun <Value: Any, Key> Cached (

	crossinline getKey : () -> Key,

	crossinline initValue : (Key) -> Value

) : CachedVal1<Value, Key> {

	return object : CachedVal1<Value, Key>() {

		override fun getKey() = getKey()

		override fun initValue (key: Key) = initValue(key)

	}

}


inline fun <Key, Value> SingleCached (

	crossinline getKey : () -> Key,

	crossinline initValue : (Key) -> Value

) : SingleCachedVal1<Key, Value> {

	return object : SingleCachedVal1<Key, Value>() {

		override fun getKey() = getKey()

		override fun initValue (key: Key) = initValue(key)

	}

}
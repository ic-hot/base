package ic.struct.value.cached


import ic.ifaces.gettersetter.gettersetter1.ext.getOrCreateIfNull
import ic.ifaces.mutable.Mutable
import ic.ifaces.mutable.ext.synchronized
import ic.parallel.mutex.Mutex
import ic.struct.map.editable.EditableMap
import ic.struct.value.Val


abstract class CachedVal1<Value: Any, Key> : Val<Value>, Mutable {


	protected abstract fun getKey() : Key

	protected abstract fun initValue (key: Key) : Value


	override val mutex = Mutex()

	private val cache = EditableMap<Key, Value>()


	override val value : Value get() {
		val key = getKey()
		synchronized {
			return cache.getOrCreateIfNull(key) {
				initValue(key)
			}
		}
	}

	override val valueOrNull : Value? get() {
		val key = getKey()
		synchronized {
			return cache[key]
		}
	}


}
package ic.struct.value.cached


import ic.struct.value.Val


@Suppress("FunctionName")
inline fun <Value> Cached (crossinline initValue : () -> Value) : Val<Value> {

	return object : CachedVal<Value>() {

		override fun initValue() = initValue()

	}

}
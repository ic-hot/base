package ic.struct.value


interface FloatVal : Val<Float> {


	val floatValue : Float

	override val value
		get() = floatValue
	;


}

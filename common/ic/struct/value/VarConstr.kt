@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.value

import ic.struct.value.lateinit.DefaultLateInitVar


@Suppress("FunctionName")
inline fun <Value> Var (value: Value) : Var<Value> = DefaultVar(value)

@Suppress("FunctionName")
inline fun <Value> Var() : Var<Value> = DefaultLateInitVar()
@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.value


inline fun <Value> Val (value: Value) : Val<Value> = ConstantVal(value)

inline fun <Value> Val (
	crossinline getValue : () -> Value
) : Val<Value> {
	return object : Val<Value> {
		override val value get() = getValue()
		override val valueOrNull get() = getValue()
	}
}
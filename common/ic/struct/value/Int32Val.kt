package ic.struct.value


interface Int32Val : Val<Int> {


	val intValue : Int

	
	override val value get() = intValue


	class Constant (override val intValue : Int) : Int32Val {
		override val valueOrNull get() = intValue
	}


}

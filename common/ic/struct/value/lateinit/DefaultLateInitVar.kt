package ic.struct.value.lateinit


import ic.base.throwables.NotExistsException


class DefaultLateInitVar<Value> : LateInitVar<Value> {


	private var isInitialized : Boolean = false

	private var valueField : Value? = null


	override val valueOrNull: Value?
		get() {
			if (isInitialized) {
				@Suppress("UNCHECKED_CAST")
				return valueField as Value
			} else {
				return null
			}
		}
	;


	override var value: Value
		get() {
			if (isInitialized) {
				@Suppress("UNCHECKED_CAST")
				return valueField as Value
			} else {
				throw RuntimeException()
			}
		}
		set(value) {
			valueField = value
			isInitialized = true
		}
	;


}
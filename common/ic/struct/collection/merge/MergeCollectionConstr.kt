@file:Suppress("FunctionName")


package ic.struct.collection.merge


import ic.struct.collection.Collection


inline fun <Item> MergeCollection (

	children : Collection<Collection<Item>>,

	crossinline areItemsEqual : (Item, Item) -> Boolean

) : MergeCollection<Item> {

	return object : MergeCollection<Item>() {

		override val children get() = children

		override fun areItemsEqual (a: Item, b: Item) : Boolean = areItemsEqual(a, b)

	}

}
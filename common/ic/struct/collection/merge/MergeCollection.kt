package ic.struct.collection.merge


import ic.ifaces.action.action1.Action1
import ic.struct.collection.BaseCollection
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.reduce.find.atLeastOne


abstract class MergeCollection<Item> : BaseCollection<Item>() {


	protected abstract val children : Collection<Collection<Item>>

	protected abstract fun areItemsEqual (a: Item, b: Item) : Boolean


	override fun invoke (action: (Item) -> Unit) {

		val alreadyPassedItems = EditableList<Item>()

		children.forEach { child ->

			child.forEach { item ->

				val isItemAlreadyPassed = alreadyPassedItems.atLeastOne { areItemsEqual(it, item) }

				if (!isItemAlreadyPassed) {

					action(item)

					alreadyPassedItems.add(item)

				}

			}

		}

	}


}
package ic.struct.collection


import ic.struct.collection.empty.EmptyCollection
import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection() : Collection<Item> = EmptyCollection


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection (vararg items: Item) : Collection<Item> {
	return List(*items)
}
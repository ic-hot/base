package ic.struct.collection.ext.reduce.minmax


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.int32.Int32
import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@Deprecated("Use ic.funs.effector.minmax")
@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> Collection<Item>.maxByOrThrow (
	@Skippable crossinline itemToValue: (Item) -> Int32
) : Item {
	var maxValue : Int32 = Int32.MIN_VALUE
	var maxItem : Item? = null
	var atLeastOneItem : Boolean = false
	forEach { item ->
		skippable {
			val value = itemToValue(item)
			if (atLeastOneItem) {
				if (value > maxValue) {
					maxItem = item
					maxValue = value
				}
			} else {
				atLeastOneItem = true
				maxItem = item
				maxValue = value
			}
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return maxItem as Item
	} else {
		throw EmptyException
	}
}


@Deprecated("Use ic.funs.effector.minmax")
@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxBy (
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Item {
	try {
		return maxByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}


@Deprecated("Use ic.funs.effector.minmax")
@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxByOrNull (
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Item? {
	try {
		return maxByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		return null
	}
}


@Deprecated("Use ic.funs.effector.minmax")
@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxBy (
	or : Item,
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Item {
	try {
		return maxByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		return or
	}
}
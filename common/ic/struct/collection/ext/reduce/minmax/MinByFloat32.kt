package ic.struct.collection.ext.reduce.minmax


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float32.Float32
import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> Collection<Item>.minByOrThrow (
	crossinline itemToValue: (Item) -> Float32
) : Item {
	var minValue : Float32 = Float32.POSITIVE_INFINITY
	var minItem : Item? = null
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val value = itemToValue(item)
		if (atLeastOneItem) {
			if (value < minValue) {
				minItem = item
				minValue = value
			}
		} else {
			atLeastOneItem = true
			minItem = item
			minValue = value
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return minItem as Item
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.minBy (
	crossinline itemToValue : (Item) -> Float32
) : Item {
	try {
		return minByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
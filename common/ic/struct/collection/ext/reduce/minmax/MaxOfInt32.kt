package ic.struct.collection.ext.reduce.minmax


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.int32.Int32
import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> Collection<Item>.maxOfOrThrow (
	@Skippable crossinline itemToValue: (Item) -> Int32
) : Int32 {
	var maxValue : Int32 = Int32.MIN_VALUE
	var atLeastOneItem : Boolean = false
	forEach { item ->
		skippable {
			val value = itemToValue(item)
			if (atLeastOneItem) {
				if (value > maxValue) {
					maxValue = value
				}
			} else {
				atLeastOneItem = true
				maxValue = value
			}
		}
	}
	if (atLeastOneItem) {
		return maxValue
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxOf (
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Int32 {
	try {
		return maxOfOrThrow(itemToValue)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxOfOrNull (
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Int32? {
	try {
		return maxOfOrThrow(itemToValue)
	} catch (_: EmptyException) {
		return null
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Collection<Item>.maxOf (
	or : Int32,
	@Skippable crossinline itemToValue : (Item) -> Int32
) : Int32 {
	try {
		return maxOfOrThrow(itemToValue)
	} catch (_: EmptyException) {
		return or
	}
}
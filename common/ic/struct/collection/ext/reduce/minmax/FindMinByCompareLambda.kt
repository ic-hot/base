package ic.struct.collection.ext.reduce.minmax


import ic.base.compare.Comparison
import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item> Collection<Item>.findMinOrThrowEmpty (
	crossinline compare: (Item, Item) -> Comparison
) : Item {
	var minItem : Item? = null
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		if (atLeastOneItem) {
			@Suppress("UNCHECKED_CAST")
			if (compare(item, minItem as Item) < 0) {
				minItem = item
			}
		} else {
			atLeastOneItem = true
			minItem = item
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return minItem as Item
	} else {
		throw EmptyException
	}
}


inline fun <Item> Collection<Item>.findMin (
	crossinline compare: (Item, Item) -> Comparison
) : Item {
	try {
		return findMinOrThrowEmpty(compare)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
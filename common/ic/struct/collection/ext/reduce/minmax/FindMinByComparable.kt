package ic.struct.collection.ext.reduce.minmax


import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item, C: Comparable<C>> Collection<Item>.findMinOrThrowEmpty (
	crossinline itemToComparable: (Item) -> C
) : Item {
	lateinit var minComparable : C
	var minItem : Item? = null
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		val comparable = itemToComparable(item)
		if (atLeastOneItem) {
			if (comparable < minComparable) {
				minItem = item
				minComparable = comparable
			}
		} else {
			atLeastOneItem = true
			minItem = item
			minComparable = comparable
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return minItem as Item
	} else {
		throw EmptyException
	}
}


inline fun <Item, C: Comparable<C>> Collection<Item>.findMin (
	crossinline itemToComparable: (Item) -> C
) : Item {
	try {
		return findMinOrThrowEmpty(itemToComparable)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
package ic.struct.collection.ext.reduce


import ic.base.annotations.Throws
import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.throwables.EmptyException
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@Throws(EmptyException::class)
val <Item> Collection<Item>.anyOrThrow : Item
	get() {
		var result : Item? = null
		var isFound : Boolean = false
		skippable {
			forEach { item ->
				isFound = true
				result = item
				skip
			}
		}
		if (isFound) {
			@Suppress("UNCHECKED_CAST")
			return result as Item
		} else {
			throw EmptyException
		}
	}
;


inline val <Item> Collection<Item>.any : Item get() {
	try {
		return anyOrThrow
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

inline val <Item> Collection<Item>.anyOrNull : Item? get() {
	try {
		return anyOrThrow
	} catch (_: EmptyException) {
		return null
	}
}
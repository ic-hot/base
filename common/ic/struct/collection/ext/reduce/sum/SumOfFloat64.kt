package ic.struct.collection.ext.reduce.sum


import kotlin.experimental.ExperimentalTypeInference
import kotlin.jvm.JvmName

import ic.base.primitives.float64.Float64
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@JvmName("sumOfFloat64")
inline fun <Item> Collection<Item>.sumOf (
	crossinline itemToFloat64: (Item) -> Float64
) : Float64 {
	var sum : Float64 = 0.0
	forEach { item ->
		sum += itemToFloat64(item)
	}
	return sum
}
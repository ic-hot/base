package ic.struct.collection.ext.reduce.sum


import ic.base.primitives.int32.Int32
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach


inline fun <Item> Collection<Item>.sumByInt32 (
	crossinline itemToInt32: (Item) -> Int32
) : Int32 {
	var sum : Int32 = 0
	breakableForEach { item ->
		sum += itemToInt32(item)
	}
	return sum
}
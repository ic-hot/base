package ic.struct.collection.ext.reduce.sum


import ic.base.primitives.float32.Float32
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


inline fun <Item> Collection<Item>.sumByFloat32 (
	crossinline itemToFloat32: (Item) -> Float32
) : Float32 {
	var sum : Float32 = 0F
	forEach { item ->
		sum += itemToFloat32(item)
	}
	return sum
}
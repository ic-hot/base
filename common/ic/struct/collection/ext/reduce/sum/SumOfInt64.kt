package ic.struct.collection.ext.reduce.sum


import kotlin.experimental.ExperimentalTypeInference
import kotlin.jvm.JvmName

import ic.base.primitives.int64.Int64
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@JvmName("sumOfInt64")
inline fun <Item> Collection<Item>.sumOf (
	crossinline itemToInt64 : (Item) -> Int64
) : Int64 {
	var sum : Int64 = 0
	forEach { item ->
		sum += itemToInt64(item)
	}
	return sum
}
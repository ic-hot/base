package ic.struct.collection.ext


import ic.struct.collection.Collection
import ic.struct.collection.ext.count.isEmpty
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList


operator fun <Item> Collection<Item>.plus (other: Collection<Item>) : Collection<Item> {
	if (other.isEmpty) return this
	if (this.isEmpty)  return other
	val list = EditableList<Item>()
	this.forEach { list.add(it) }
	other.forEach { list.add(it) }
	return list
}


operator fun
	<Item, ItemSubClass1: Item, ItemSubClass2: Item>
	Collection<ItemSubClass1>.plus (item: ItemSubClass2) : Collection<Item>
{
	val list = EditableList<Item>()
	this.forEach { list.add(it) }
	list.add(item)
	return list
}
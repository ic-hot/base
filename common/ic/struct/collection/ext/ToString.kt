package ic.struct.collection.ext


import ic.base.primitives.int32.Int32
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


fun Collection<*>.toString (indent: Int32) : String {

	val stringBuilder = StringBuilder()

	stringBuilder.append("[")

	var atLeastOne : Boolean = false

	forEach { item ->

		if (atLeastOne) {
			stringBuilder.append(',')
		} else {
			atLeastOne = true
		}

		stringBuilder.append('\n')
		repeat(indent + 1) { stringBuilder.append("    ") }

		stringBuilder.append(item)

	}

	if (atLeastOne) {
		stringBuilder.append('\n')
		repeat(indent) { stringBuilder.append("    ") }
	}

	stringBuilder.append("]")

	return stringBuilder.toString()

}
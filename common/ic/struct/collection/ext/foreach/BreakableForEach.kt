package ic.struct.collection.ext.foreach


import ic.base.escape.breakable.breakable
import ic.base.kfunctions.DoNothing
import ic.ifaces.action.action1.Action1
import ic.struct.collection.Collection


@Deprecated("To remove")
inline fun <Item> Collection<Item>.breakableForEach (

	noinline onBreak : () -> Unit = DoNothing,

	crossinline action : (Item) -> Unit

) {

	breakable(onBreak = onBreak) {
		forEach { action(it) }
	}

}
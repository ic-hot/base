package ic.struct.collection.ext.foreach


import ic.base.primitives.int32.Int32
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyToList
import ic.struct.list.ext.foreach.forEachInParallel


inline fun <Item> Collection<Item>.forEachInParallel (
	maxThreadsCount : Int32 = Int32.MAX_VALUE,
	crossinline action : (Item) -> Unit
) {
	this.copyToList().forEachInParallel(
		maxThreadsCount = maxThreadsCount,
		action = action
	)
}
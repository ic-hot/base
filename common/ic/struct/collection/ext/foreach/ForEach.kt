package ic.struct.collection.ext.foreach


import ic.funs.effect.Effect
import ic.struct.collection.Collection


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection<Item>.forEach (

	noinline action : Effect<Item>

) {

	invoke(action)

}

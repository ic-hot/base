package ic.struct.collection.ext.foreach


import ic.base.kfunctions.DoNothing
import ic.struct.collection.Collection


@Deprecated("To remove")
inline fun <Item> Collection<Item>.breakableForEach (

	noinline onBreak : () -> Unit = DoNothing,

	crossinline doInBetween : () -> Unit,

	crossinline action : (Item) -> Unit

) {

	var atLeastOne : Boolean = false

	breakableForEach(onBreak = onBreak) { item ->

		if (atLeastOne) {
			doInBetween()
		} else {
			atLeastOne = true
		}

		action(item)

	}

}
package ic.struct.collection.ext.count


import ic.struct.collection.Collection


inline val <Item> Collection<Item>.isEmpty : Boolean get() = count == 0L


inline val <Item> Collection<Item>?.isNullOrEmpty : Boolean get() = this == null || isEmpty


inline val <Item> Collection<Item>?.isNotEmpty : Boolean get() = !isNullOrEmpty
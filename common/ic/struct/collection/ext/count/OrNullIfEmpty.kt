package ic.struct.collection.ext.count


import ic.struct.collection.Collection


inline val <Item> Collection<Item>.orNullIfEmpty get() = if (isEmpty) null else this
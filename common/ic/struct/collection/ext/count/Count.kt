package ic.struct.collection.ext.count


import ic.base.primitives.int64.Int64
import ic.ifaces.hascount.HasCount
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


inline fun <Item> Collection<Item>.count (crossinline condition: (Item) -> Boolean) : Int64 {
	var result : Int64 = 0
	forEach { item ->
		if (condition(item)) {
			result++
		}
	}
	return result
}


inline val <Item> Collection<Item>.count : Int64 get() {
	if (this is HasCount) {
		return count
	} else {
		return count { true }
	}
}
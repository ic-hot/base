package ic.struct.collection.ext


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList


fun <Item> Collection<Item>.copyAdd (item: Item) : List<Item> {
	val editableList = EditableList<Item>()
	forEach {
		editableList.add(it)
	}
	editableList.add(item)
	return editableList
}
package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


fun <Item> Collection<Item>.copyToEditableSet() : EditableSet<Item> {
	val result = EditableSet<Item>()
	forEach { item ->
		result.addIfNotExists(item)
	}
	return result
}
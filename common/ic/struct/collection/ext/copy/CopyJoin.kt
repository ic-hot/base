package ic.struct.collection.ext.copy


import kotlin.jvm.JvmName

import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList


@JvmName("copyJoinCollectionOfCollections")
fun <Item> Collection<Collection<Item>>.copyJoin() : List<Item> {
	val result = EditableList<Item>()
	forEach { child ->
		child.forEach { item ->
			result.add(item)
		}
	}
	return result
}


@JvmName("copyJoinCollectionOfLists")
fun <Item> Collection<List<Item>>.copyJoin() : List<Item> {
	val result = EditableList<Item>()
	forEach { child ->
		child.forEach { item ->
			result.add(item)
		}
	}
	return result
}
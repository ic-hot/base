package ic.struct.collection.ext.copy


import ic.struct.list.List
import ic.struct.collection.Collection


fun <Item> Collection<Item>.toListIfNotAlready() : List<Item> {

	if (this is List) {

		return this

	} else {

		return copyToList()

	}

}
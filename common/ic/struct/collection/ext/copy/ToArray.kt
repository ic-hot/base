package ic.struct.collection.ext.copy


import ic.base.arrays.Float32Array
import ic.base.arrays.Float64Array
import ic.base.arrays.Int32Array
import ic.base.arrays.Int64Array
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.collection.Collection
import ic.struct.list.ext.copy.toArray


inline fun <reified Item> Collection<Item>.toArray() : Array<Item> {
	return copyToList().toArray()
}

fun Collection<Int32>.toArray() : Int32Array {
	return copyToList().toArray()
}

fun Collection<Int64>.toArray() : Int64Array {
	return copyToList().toArray()
}

fun Collection<Float32>.toArray() : Float32Array {
	return copyToList().toArray()
}

fun Collection<Float64>.toArray() : Float64Array {
	return copyToList().toArray()
}
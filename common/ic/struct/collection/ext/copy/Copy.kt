package ic.struct.collection.ext.copy


import ic.struct.collection.Collection


inline fun <Item> Collection<Item>.copy() = copyToList()
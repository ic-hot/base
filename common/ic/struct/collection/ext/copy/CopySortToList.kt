package ic.struct.collection.ext.copy


import ic.base.compare.Comparison
import ic.base.kcollections.ext.copy.copyToList
import ic.struct.collection.Collection
import ic.struct.list.List


fun <Item: Comparable<Item>> Collection<Item>.copySortToList() : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sort()
	return kotlinList.copyToList()
}


inline fun <Item> Collection<Item>.copySortToList (
	crossinline compareItems : (Item, Item) -> Comparison
) : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sortWith { a, b -> compareItems(a, b) }
	return kotlinList.copyToList()
}
package ic.struct.collection.ext.copy


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList


inline fun <Item, NewItem> Collection<Item>.copyConvertIndexed (

	@Skippable crossinline convertItem : (index: Int64, Item) -> NewItem

) : Collection<NewItem> {

	val list = EditableList<NewItem>()

	forEach { item ->
		val index = list.length
		skippable {
			list.add(
				convertItem(index, item)
			)
		}
	}

	return list

}
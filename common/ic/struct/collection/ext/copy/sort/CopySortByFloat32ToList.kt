package ic.struct.collection.ext.copy.sort


import ic.base.kcollections.ext.copy.copyToList
import ic.base.primitives.float32.Float32
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyToKotlinMutableList
import ic.struct.list.List


inline fun <Item> Collection<Item>.copySortByFloat32ToList (
	crossinline itemToFloat32 : (Item) -> Float32
) : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sortWith { a, b -> itemToFloat32(a) compareTo itemToFloat32(b) }
	return kotlinList.copyToList()
}
package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.set.finite.FiniteSet


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection<Item>.copyToFiniteSet() : FiniteSet<Item> {
	return copyToEditableSet()
}
package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList


fun <Item> Collection<Item>.copyToEditableList() : EditableList<Item> {
	val list = EditableList<Item>()
	forEach { item ->
		list.add(item)
	}
	return list
}
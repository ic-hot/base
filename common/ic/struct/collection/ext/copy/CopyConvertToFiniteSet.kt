package ic.struct.collection.ext.copy


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


inline fun <Item, NewItem> Collection<Item>.copyConvertToFiniteSet (
	@Skippable crossinline convertItem : (Item) -> NewItem
) : FiniteSet<NewItem> {
	val result = EditableSet<NewItem>()
	forEach { item ->
		skippable {
			result.addIfNotExists(
				convertItem(item)
			)
		}
	}
	return result
}
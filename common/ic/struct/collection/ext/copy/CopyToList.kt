package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.copy.copy


fun <Item> Collection<Item>.copyToList() : List<Item> {
	if (this is List) {
		return copy()
	} else {
		val list = EditableList<Item>()
		forEach { item ->
			list.add(item)
		}
		return list
	}
}
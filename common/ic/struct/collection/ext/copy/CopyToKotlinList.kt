package ic.struct.collection.ext.copy


import ic.struct.collection.Collection


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection<Item>.copyToKotlinList() : kotlin.collections.List<Item> {
	return copyToKotlinMutableList()
}
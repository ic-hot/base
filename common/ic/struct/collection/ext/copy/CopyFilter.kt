package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item> Collection<Item>.copyFilter (

	crossinline condition : (Item) -> Boolean

) : List<Item> {

	val list = EditableList<Item>()

	forEach { item ->

		if (condition(item)) {

			list.add(item)

		}

	}

	return list

}
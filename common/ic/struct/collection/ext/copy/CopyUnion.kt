package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


fun <Item> Collection<FiniteSet<out Item>>.copyUnion() : FiniteSet<Item> {

	val result = EditableSet<Item>()

	forEach { set ->

		set.forEach { item ->

			result.addIfNotExists(item)

		}

	}

	return result

}
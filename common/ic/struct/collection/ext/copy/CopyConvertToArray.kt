package ic.struct.collection.ext.copy


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList


inline fun <Item, reified NewItem> Collection<Item>.copyConvertToArray (

	@Skippable crossinline convertItem : (Item) -> NewItem

) : Array<NewItem> {

	val result = EditableList<NewItem>()

	forEach { item ->
		skippable {
			result.add(
				convertItem(item)
			)
		}
	}

	return result.toArray()

}
package ic.struct.collection.ext.copy


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


fun <Item> Collection<Item>.copyToKotlinMutableList() : kotlin.collections.MutableList<Item> {

	val result = mutableListOf<Item>()
	forEach { item ->
		result.add(item)
	}
	return result

}
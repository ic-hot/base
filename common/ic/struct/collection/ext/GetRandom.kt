package ic.struct.collection.ext


import ic.base.throwables.EmptyException
import ic.funs.fun0.Fun0
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.toListIfNotAlready
import ic.struct.list.ext.getRandomOrThrowEmpty


@Throws(EmptyException::class)
fun <Item> Collection<Item>.randomOrThrowEmpty () : Item {

	return this.toListIfNotAlready().getRandomOrThrowEmpty()

}


inline fun <Item> Collection<Item>.random (
	or : Fun0<Item> = { throw RuntimeException() }
) : Item {
	try {
		return randomOrThrowEmpty()
	} catch (_: EmptyException) {
		return or()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Collection<Item>.randomOrNull () : Item {
	try {
		return randomOrThrowEmpty()
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
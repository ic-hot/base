package ic.struct.collection


import ic.funs.effect.andDoInBetween
import ic.struct.collection.ext.foreach.forEach
import ic.text.TextBuffer


abstract class BaseCollection<Item> : Collection<Item> {


	override fun toString() : String {
		return TextBuffer().apply {
			write("[ ")
			forEach(
				{ item : Item ->
					write(item.toString())
				}
				.andDoInBetween {
					write(", ")
				}
			)
			write(" ]")
		}.toString()
	}


}
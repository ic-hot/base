package ic.struct.collection


import ic.base.arrays.ext.asList
import ic.ifaces.action.action1.Action1
import ic.struct.collection.ext.foreach.forEach


class JoinCollection<Item> (

	private val parts : Collection<Collection<Item>>

) : BaseCollection<Item>() {

	override fun invoke (action: (Item) -> Unit) {
		parts.forEach { part -> part.forEach(action) }
	}

	constructor (vararg parts: Collection<Item>)
		: this(parts.asList(isArrayImmutable = true))
	;

}

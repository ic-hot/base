package ic.struct.collection.mapvalues


import ic.struct.collection.BaseCollection
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach


abstract class MapValues<Value: Any> : BaseCollection<Value>() {


	protected abstract val sourceMap : FiniteMap<*, Value>


	override fun invoke (action: (Value) -> Unit) {
		sourceMap.forEach { _, value ->
			action(value)
		}
	}


}
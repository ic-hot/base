package ic.struct.collection.mapvalues


import ic.struct.map.finite.FiniteMap


@Suppress("NOTHING_TO_INLINE")
inline fun <Value: Any> MapValues (

	sourceMap: FiniteMap<*, Value>

) : MapValues<Value> {

	return object : MapValues<Value>() {

		override val sourceMap get() = sourceMap

	}

}
package ic.struct.collection


import ic.funs.effector.Effector


/**
 * Collection get a data structure that mainly implements forEach.
 * Collections are finite and countable.
 * In general Collection is unordered, however its implementations can be ordered.
 * In general forEach method does not guarantee same invocation order.
 */
interface Collection<out Item> : Effector<Item> {



}

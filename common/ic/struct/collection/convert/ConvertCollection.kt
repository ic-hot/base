package ic.struct.collection.convert


import ic.struct.collection.BaseCollection
import ic.struct.collection.Collection
import ic.base.throwables.Skip
import ic.struct.collection.ext.foreach.forEach


abstract class ConvertCollection<Item, SourceItem> : BaseCollection<Item>() {


	protected abstract val sourceCollection : Collection<SourceItem>

	protected abstract fun convertItem (sourceItem: SourceItem) : Item


	override fun invoke (action: (Item) -> Unit) {
		sourceCollection.forEach iteration@{ sourceItem ->
			val item = try {
				convertItem(sourceItem)
			} catch (skip: Skip) {
				return@iteration
			}
			action(item)
		}
	}


}

@file:Suppress("FunctionName")


package ic.struct.collection.convert

import ic.struct.collection.Collection


inline fun <NewItem, SourceItem> ConvertCollection (

	crossinline getSourceCollection : () -> Collection<SourceItem>,

	crossinline convertItemOrSkip : (SourceItem) -> NewItem

) : Collection<NewItem> = object : ConvertCollection<NewItem, SourceItem>() {

	override val sourceCollection get() = getSourceCollection()

	override fun convertItem (sourceItem: SourceItem) = convertItemOrSkip(sourceItem)

}


inline fun <NewItem, SourceItem> ConvertCollection (

	sourceCollection : Collection<SourceItem>,

	crossinline convertItem : (SourceItem) -> NewItem

) = ConvertCollection(
	getSourceCollection = { sourceCollection },
	convertItemOrSkip = convertItem
)
package ic.struct.collection.filter


import ic.struct.collection.BaseCollection
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach


abstract class FilterCollection<Item> : BaseCollection<Item>() {

	protected abstract val sourceCollection: Collection<Item>

	protected abstract fun filter (item: Item) : Boolean

	override fun invoke (action: (Item) -> Unit) {
		sourceCollection.forEach { item: Item ->
			if (filter(item)) {
				action(item)
			}
		}
	}

}
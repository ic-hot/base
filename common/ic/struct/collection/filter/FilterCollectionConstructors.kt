@file:Suppress("FunctionName")


package ic.struct.collection.filter


import ic.struct.collection.Collection


inline fun <Item> FilterCollection (

	sourceCollection : Collection<Item>,

	crossinline filter : (Item) -> Boolean

) : FilterCollection<Item> {

	return object : FilterCollection<Item>() {

		override val sourceCollection get() = sourceCollection

		override fun filter (item: Item) = filter(item)

	}

}
package ic.struct.collection.cacheconvert


import ic.struct.collection.Collection


inline fun <Item: Any, SourceItem> CacheConvertCollection (

	crossinline getSourceCollection : () -> Collection<SourceItem>,

	crossinline convertItemOrSkip : (SourceItem) -> Item

) : CacheConvertCollection<Item, SourceItem> {

	return object : CacheConvertCollection<Item, SourceItem>() {

		override val sourceCollection get() = getSourceCollection()

		override fun convertItemOrSkip (sourceItem: SourceItem) = convertItemOrSkip(sourceItem)
		
	}
	
}


inline fun <Item: Any, SourceItem> CacheConvertCollection (

	sourceCollection : Collection<SourceItem>,

	crossinline convertItemOrSkip : (SourceItem) -> Item
	
) = CacheConvertCollection(
	getSourceCollection = { sourceCollection },
	convertItemOrSkip = convertItemOrSkip
)
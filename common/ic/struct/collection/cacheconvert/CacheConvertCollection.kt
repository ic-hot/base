package ic.struct.collection.cacheconvert


import ic.base.throwables.Skip
import ic.ifaces.action.action1.Action1
import ic.ifaces.gettersetter.gettersetter1.ext.getOrCreateIfNull
import ic.struct.collection.BaseCollection
import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.editable.EditableMap
import ic.struct.set.editable.EditableSet


abstract class CacheConvertCollection<Item: Any, SourceItem> : BaseCollection<Item>() {


	protected abstract val sourceCollection : Collection<SourceItem>

	protected abstract fun convertItemOrSkip (sourceItem: SourceItem) : Item


	private val cache = EditableMap<SourceItem, Item>()

	private val skippedSourceItems = EditableSet<SourceItem>()


	override fun invoke (action: (Item) -> Unit) {
		sourceCollection.breakableForEach { sourceItem ->
			if (sourceItem !in skippedSourceItems) {
				try {
					action(
						cache.getOrCreateIfNull(sourceItem) {
							convertItemOrSkip(sourceItem)
						}
					)
				} catch (e: Skip) {
					skippedSourceItems.add(sourceItem)
				}	
			}
		}
	}
	
	
}
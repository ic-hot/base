package ic.struct.collection.empty


import ic.ifaces.hascount.HasCount
import ic.struct.collection.BaseCollection


object EmptyCollection : BaseCollection<Nothing>(), HasCount {


	override val count get() = 0L


	override fun invoke (action: (Nothing) -> Unit) {}


}
package ic.struct.list.test


import ic.struct.list.List
import ic.struct.list.editable.test.EditableListTests
import ic.test.TestSuite


internal class ListTests : TestSuite() {

	override fun initTests() = List(

		EditableListTests()

	)

}
package ic.struct.list.cycle


import ic.base.primitives.int64.Int64
import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE", "FunctionName")
inline fun <Item> CycleList (

	sourceList : List<Item>,

	loopsCount : Int64

) : CycleList<Item> {

	return object : CycleList<Item>() {

		override val sourceList get() = sourceList

		override val loopsCount get() = loopsCount

		override val isListImmutable get() = true

	}

}
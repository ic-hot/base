package ic.struct.list.cycle


import ic.base.primitives.int64.Int64

import ic.struct.list.List
import ic.struct.list.BaseList


abstract class CycleList<Item> : BaseList<Item>() {


	abstract val sourceList : List<Item>

	abstract val loopsCount : Int64


	override val length get() = sourceList.length * loopsCount

	override fun implementGet (index: Int64) : Item {
		return sourceList[index % sourceList.length]
	}




}
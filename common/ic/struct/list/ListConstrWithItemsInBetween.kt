package ic.struct.list


import ic.base.primitives.int32.Int32


inline fun <Item> List (

	length : Int32,

	initItemInBetween : () -> Item,

	initItem : (index: Int32) -> Item

) : List<Item> {

	return List(
		length = (
			if (length == 0) {
				0
			} else {
				length + length - 1
			}
		)
	) { index: Int32 ->

		if (index % 2 == 0) {

			initItem(index / 2)

		} else {

			initItemInBetween()

		}

	}

}
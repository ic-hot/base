package ic.struct.list


import ic.base.annotations.Throws
import ic.base.annotations.Valid
import ic.base.objects.ext.isImmutable
import ic.base.primitives.int64.Int64
import ic.base.throwables.End
import ic.base.throwables.IndexOutOfBoundsException
import ic.ifaces.immut.MaybeImmutable
import ic.ifaces.iterator.Iterator
import ic.math.annotations.PositiveOrZero
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.reduce.find.all
import ic.struct.list.ext.reduce.sum.sumOf
import ic.struct.list.funs.areListsEqual
import ic.struct.sequence.BaseSequence


abstract class BaseList<Item> : BaseSequence<Item>(), List<Item>, MaybeImmutable {


	protected abstract val isListImmutable : Boolean

	final override val isImmutable get() = isListImmutable && all { it.isImmutable }


	protected abstract fun implementGet (@Valid @PositiveOrZero index: Int64) : Item

	@Throws(IndexOutOfBoundsException::class)
	protected fun throwIndexOutOfBoundsIfIndexIsInvalid (index: Int64) {
		val isIndexValid = when {
			index < 0 -> false
			index < length -> true
			else -> false
		}
		if (!isIndexValid) throw IndexOutOfBoundsException
	}

	@Throws(IndexOutOfBoundsException::class)
	override fun getOrThrowIndexOutOfBounds (index: Int64) : Item {
		throwIndexOutOfBoundsIfIndexIsInvalid(index)
		return implementGet(index)
	}


	override fun hashCode(): Int {
		return sumOf { it.hashCode() }
	}

	override fun equals (other: Any?) : Boolean {
		if (other is List<*>) {
			return areListsEqual(this, other)
		} else {
			return false
		}
	}


	override fun invoke (action: (Item) -> Unit) {
		forEach(action)
	}

	override fun newIterator() : Iterator<Item> {
		val list = this
		val length = this.length
		return object : Iterator<Item> {
			var index : Int64 = 0
			@Throws(End::class)
			override fun getNextOrThrowEnd() : Item {
				if (index < length) {
					return list[index++]
				} else {
					throw End
				}
			}
		}
	}


	override val count get() = length


}
@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.list.reverse


import ic.struct.list.List


inline fun <Item> ReverseList (

	sourceList : List<Item>

) : ReverseList<Item> {

	return object : ReverseList<Item>() {

		override val sourceList get() = sourceList

		override val isSourceListImmutable get() = true

	}

}
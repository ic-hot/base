package ic.struct.list.reverse;


import ic.base.objects.ext.isImmutable
import ic.base.primitives.int64.Int64
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.list.BaseList
import ic.struct.list.List


abstract class ReverseList<Item> : BaseList<Item>() {

	protected abstract val sourceList : List<Item>

	protected abstract val isSourceListImmutable : Boolean

	override val isListImmutable get() = isSourceListImmutable && sourceList.isImmutable

	override val length get() = sourceList.length

	override fun implementGet (index: Int64) : Item {
		synchronizedIfMutable {
			return sourceList[sourceList.length - 1 - index]
		}
	}

}

package ic.struct.list.single


import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList


abstract class SingleItemList<Item> : BaseList<Item>() {


	protected abstract val item : Item

	protected abstract val isItemImmutable : Boolean


	override val isListImmutable get() = isItemImmutable


	override val length get() = 1L


	override fun implementGet (index: Int64) : Item {
		return item
	}


}
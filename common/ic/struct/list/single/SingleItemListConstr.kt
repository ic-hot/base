package ic.struct.list.single


inline fun <Item> SingleItemList (

	crossinline getItem : () -> Item

) : SingleItemList<Item> {

	return object : SingleItemList<Item>() {

		override val item get() = getItem()

		override val isItemImmutable get() = false

	}

}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> SingleItemList (

	item : Item

) : SingleItemList<Item> {

	return object : SingleItemList<Item>() {

		override val item get() = item

		override val isItemImmutable get() = true

	}

}
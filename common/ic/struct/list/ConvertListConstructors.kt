@file:Suppress("FunctionName")


package ic.struct.list

import ic.base.kcollections.ext.copy.copyToList
import ic.base.primitives.int64.Int64


inline fun <Item, SourceItem> ConvertList (

	crossinline getSourceList : () -> List<SourceItem>,

	crossinline convertItem : (index: Int64, SourceItem) -> Item

) : List<Item> {

	return object : ConvertList<Item, SourceItem>() {

		override val sourceList get() = getSourceList()

		override fun convertItem (index: Int64, sourceItem: SourceItem) : Item {
			return convertItem.invoke(index, sourceItem)
		}

	}

}


inline fun <Item, SourceItem> ConvertList (

	sourceList : List<SourceItem>,

	crossinline convertItem : (index: Int64, SourceItem) -> Item

) = ConvertList(
	getSourceList = { sourceList },
	convertItem = convertItem
)


inline fun <Item, SourceItem> ConvertList (

	sourceList : List<SourceItem>,

	crossinline convertItem : (SourceItem) -> Item

) = ConvertList(
	getSourceList = { sourceList },
	convertItem = { _, sourceItem -> convertItem(sourceItem) }
)


inline fun <Item, SourceItem> ConvertList (

	sourceList : kotlin.collections.List<SourceItem>,

	crossinline convertItem : (index: Int64, SourceItem) -> Item

) = ConvertList(
	sourceList = sourceList.copyToList(),
	convertItem = convertItem
)
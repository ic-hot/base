package ic.struct.list.empty


import ic.base.primitives.int64.Int64
import ic.base.throwables.WrongCallException
import ic.struct.list.BaseList


object EmptyList : BaseList<Nothing>() {

	override val length get() = 0L

	override val isListImmutable get() = true

	override fun implementGet (index: Int64) = throw WrongCallException.Error()

}
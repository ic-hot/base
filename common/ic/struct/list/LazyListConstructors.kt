package ic.struct.list


import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException


inline fun <Item> List.Companion.Lazy (

	crossinline getLength : () -> Int64,

	crossinline getItem : (index: Int64) -> Item

) : List<Item> {

	return object : BaseList<Item>() {

		override val length get() = getLength()

		override val isListImmutable get() = false

		override fun implementGet (index: Int64) = getItem(index)

	}

}


inline fun <Item> List.Companion.Lazy (

	crossinline getItem0 : () -> Item

) : List<Item> {

	return object : BaseList<Item>() {

		override val isListImmutable get() = false

		override val length get() = 1L

		override fun implementGet (index: Int64): Item {
			return when (index) {
				0L -> getItem0()
				else -> throw IndexOutOfBoundsException.Runtime(length = 2, index = index)
			}
		}

	}

}


inline fun <Item> List.Companion.Lazy (

	crossinline getFirstItem 	: () -> Item,
	crossinline getSecondItem 	: () -> Item

) : List<Item> {

	return object : BaseList<Item>() {

		override val isListImmutable get() = false

		override val length get() = 2L
		
		override fun implementGet (index: Int64): Item {
			return when (index) {
				0L -> getFirstItem()
				1L -> getSecondItem()
				else -> throw IndexOutOfBoundsException.Runtime(length = 2, index = index)
			}
		}

	}

}


inline fun <Item> List.Companion.Lazy (

	crossinline getFirstItem 	: () -> Item,
	crossinline getSecondItem 	: () -> Item,
	crossinline getThirdItem 	: () -> Item,
	crossinline getFourthItem 	: () -> Item

) : List<Item> {

	return object : BaseList<Item>() {

		override val isListImmutable get() = false

		override val length get() = 4L

		override fun implementGet (index: Int64): Item {
			return when (index) {
				0L -> getFirstItem()
				1L -> getSecondItem()
				2L -> getThirdItem()
				3L -> getFourthItem()
				else -> throw IndexOutOfBoundsException.Runtime(length = 4, index = index)
			}
		}

	}

}
package ic.struct.list


import ic.math.annotations.NotZero
import ic.math.annotations.Positive
import ic.base.primitives.int64.Int64


abstract class SplitIntoPortionsList<Item> : BaseList<List<Item>>() {

	@NotZero @Positive
	protected abstract val portionSize : Int64

	protected abstract val sourceList : List<Item>

	override val length : Int64 get() {
		val sourceListSize = sourceList.length
		var result = sourceListSize / portionSize
		if (sourceListSize % portionSize != 0L) result++
		return result
	}

	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun implementGet (portionIndex: Int64) : List<Item> {

		return object : BaseList<Item>() {

			override val length : Int64 get() {
				val sourceListSize = sourceList.length
				val remainder = sourceListSize % portionSize
				var portionsCount = sourceListSize / portionSize
				return if (remainder == 0L) {
					portionSize
				} else {
					portionsCount++
					if (portionIndex < portionsCount - 1) {
						portionSize
					} else {
						remainder
					}
				}
			}

			override val isListImmutable get() = false

			override fun implementGet (index: Int64) : Item {
				return sourceList[portionIndex * portionSize + index]
			}

		}

	}

}
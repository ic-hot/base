package ic.struct.list.fromklist


import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.BaseList


class ListFromKotlinList<Item> (

	private val kotlinList : kotlin.collections.List<Item>,

	private val isKotlinListImmutable : Boolean

) : BaseList<Item>() {

	override val length get() = kotlinList.size.asInt64

	override val isListImmutable get() = isKotlinListImmutable

	override fun implementGet (index: Int64) = kotlinList[index.asInt32]

}
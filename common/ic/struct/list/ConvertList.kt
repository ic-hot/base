package ic.struct.list


import ic.base.annotations.Valid
import ic.base.primitives.int64.Int64


abstract class ConvertList<Item, SourceItem> : BaseList<Item>() {


	override val isListImmutable get() = false


	protected abstract val sourceList: List<SourceItem>

	protected abstract fun convertItem (index: Int64, sourceItem: SourceItem) : Item


	override val length get() = sourceList.length


	override fun implementGet (@Valid index: Int64): Item {
		return convertItem(index, sourceList[index])
	}


}

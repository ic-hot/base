package ic.struct.list


import ic.base.arrays.ext.asList
import ic.base.arrays.ext.copy.convert.copyConvertToList
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.loop.repeat
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.list.editable.EditableList
import ic.struct.list.empty.EmptyList
import ic.struct.list.ext.length.isEmpty
import ic.struct.list.single.SingleItemList


fun List() = EmptyList


fun <Item> List (item1: Item) : List<Item> {
	return SingleItemList(item1)
}


fun <Item> List (vararg items: Item) : List<Item> = items.asList(isArrayImmutable = true)


fun <Item> List (@Skippable vararg itemGetters: () -> Item) : List<Item> {
	return itemGetters.copyConvertToList { itemGetter -> itemGetter() }
}

inline fun <Item> List (
	@Skippable initItem0 : () -> Item
) : List<Item> {
	val editableList = EditableList<Item>()
	skippable { editableList.add(initItem0()) }
	editableList.freeze()
	return editableList
}

inline fun <Item> List (
	@Skippable initItem0 : () -> Item,
	@Skippable initItem1 : () -> Item
) : List<Item> {
	val editableList = EditableList<Item>()
	skippable { editableList.add(initItem0()) }
	skippable { editableList.add(initItem1()) }
	editableList.freeze()
	return editableList
}

inline fun <Item> List (
	@Skippable initItem0 : () -> Item,
	@Skippable initItem1 : () -> Item,
	@Skippable initItem2 : () -> Item
) : List<Item> {
	val editableList = EditableList<Item>()
	skippable { editableList.add(initItem0()) }
	skippable { editableList.add(initItem1()) }
	skippable { editableList.add(initItem2()) }
	editableList.freeze()
	return editableList
}

inline fun <Item> List (
	getItem1 : () -> Item,
	getItem2 : () -> Item,
	getItem3 : () -> Item,
	getItem4 : () -> Item,
) : List<Item> {
	val editableList = EditableList<Item>()
	skippable { editableList.add(getItem1()) }
	skippable { editableList.add(getItem2()) }
	skippable { editableList.add(getItem3()) }
	skippable { editableList.add(getItem4()) }
	editableList.freeze()
	return editableList
}

inline fun <Item> ListOrNull (
	getItem1 : () -> Item,
	getItem2 : () -> Item
) : List<Item>? {
	val list = List(getItem1, getItem2)
	return if (list.isEmpty) null else list
}

inline fun <Item> ListOrNull (
	getItem1 : () -> Item,
	getItem2 : () -> Item,
	getItem3 : () -> Item
) : List<Item>? {
	val list = List(getItem1, getItem2, getItem3)
	return if (list.isEmpty) null else list
}


inline fun <Item> List (
	length : Int64,
	@Skippable initItem : (index: Int64) -> Item
) : List<Item> {
	val editableList = EditableList<Item>()
	repeat(length) { index ->
		skippable {
			editableList.add(
				initItem(index)
			)
		}
	}
	editableList.freeze()
	return editableList
}


inline fun <Item> List (
	length : Int32,
	@Skippable initItem : (index: Int32) -> Item
) : List<Item> {
	val editableList = EditableList<Item>()
	repeat(length) { index ->
		skippable {
			editableList.add(
				initItem(index)
			)
		}
	}
	return editableList
}

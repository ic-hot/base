package ic.struct.list.editable.fromklist


import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.editable.BaseEditableList


class EditableListFromKotlinMutableList<Item> (

	private val mutableList : MutableList<Item>

) : BaseEditableList<Item>() {


	override val length get() = mutableList.size.asInt64


	override fun implementGet (index: Int64) = mutableList[index.asInt32]

	override fun implementSet (index: Int64, item: Item) {
		mutableList[index.asInt32] = item
	}

	override fun implementInsert (index: Int64, item: Item) {
		mutableList.add(index.asInt32, item)
	}

	override fun implementRemove (index: Int64) : Item {
		return mutableList.removeAt(index.asInt32)
	}


	override fun shuffle() = mutableList.shuffle()


}
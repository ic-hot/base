package ic.struct.list.editable;


import ic.math.annotations.Positive
import ic.base.annotations.ToOverride
import ic.base.annotations.Valid
import ic.base.loop.breakableRepeat
import ic.base.primitives.int64.Int64
import ic.ifaces.adder.Adder
import ic.ifaces.emptiable.Emptiable
import ic.struct.array.Array
import ic.base.throwables.IndexOutOfBoundsException
import ic.base.throwables.EmptyException


interface EditableList<Item> : Array<Item>, Adder<Item>, Emptiable {


	// Insert:

	@Throws(IndexOutOfBoundsException::class)
	fun insertOrThrowIndexOutOfBounds (index: Int64, item: Item)

	fun insert (index: Int64, item: Item) {
		try {
			insertOrThrowIndexOutOfBounds(index, item)
		} catch (t: IndexOutOfBoundsException) {
			throw IndexOutOfBoundsException.Runtime(length, index)
		}
	}


	// Remove:

	@Throws(IndexOutOfBoundsException::class)
	fun removeOrThrowIndexOutOfBounds (@Positive index: Int64) : Item

	fun remove (@Positive index: Int64) : Item {
		try {
			return removeOrThrowIndexOutOfBounds(index)
		} catch (t: IndexOutOfBoundsException) {
			throw IndexOutOfBoundsException.Runtime(length, index)
		}
	}

	@Throws(EmptyException::class)
	fun removeLastOrThrowEmpty() {
		val length = length
		if (length <= 0) throw EmptyException
		remove(length - 1)
	}

	fun removeLast() {
		try {
			removeLastOrThrowEmpty()
		} catch (_: EmptyException) {
			throw RuntimeException()
		}
	}

	fun getAndRemove (index: Int64) : Item {
		val item = get(index)
		remove(index)
		return item
	}


	// Adder implementation:

	override fun add (item: Item) = insert(length, item)


	// Emptiable implementation:

	@ToOverride
	override fun empty() {
		breakableRepeat (length) { remove(0) }
	}


	fun shuffle() { // TODO
		throw NotImplementedError()
	}


	fun trim (maxSize: Int) {
		while (length > maxSize) removeLast()
	}


}

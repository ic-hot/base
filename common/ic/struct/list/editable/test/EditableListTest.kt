package ic.struct.list.editable.test


import ic.struct.list.editable.EditableList
import ic.test.Test
import ic.text.TextOutput


internal abstract class EditableListTest : Test() {


	protected abstract val nextEditableList : EditableList<Any?>


	protected abstract fun TextOutput.runTest (editableList: EditableList<Any?>)


	override fun TextOutput.runTest() {

		runTest(nextEditableList)

	}


}
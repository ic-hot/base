package ic.struct.list.editable.test


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.test.Test
import ic.test.TestSuite


internal abstract class EditableListImplementationTests : TestSuite() {


	protected abstract val nextEditableList : EditableList<Any?>


	override fun initTests() : List<Test> {
		val suite = this
		return List(
			object : NewListIsEmpty() {
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToTailRemoveFromHead() {
				override val itemsCount get() = 4L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToHeadRemoveFromTail() {
				override val itemsCount get() = 4L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToTailRemoveFromHead() {
				override val itemsCount get() = 8L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToHeadRemoveFromTail() {
				override val itemsCount get() = 8L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToTailRemoveFromHead() {
				override val itemsCount get() = 16L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToHeadRemoveFromTail() {
				override val itemsCount get() = 16L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToTailRemoveFromHead() {
				override val itemsCount get() = 256L
				override val nextEditableList get() = suite.nextEditableList
			},
			object : InsertToHeadRemoveFromTail() {
				override val itemsCount get() = 256L
				override val nextEditableList get() = suite.nextEditableList
			}
		)
	}


}
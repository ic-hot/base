package ic.struct.list.editable.test


import ic.base.assert.assert
import ic.base.loop.breakableRepeat
import ic.base.primitives.int64.Int64
import ic.struct.list.editable.EditableList
import ic.text.TextOutput


internal abstract class InsertToHeadRemoveFromTail : EditableListTest() {

	protected abstract val itemsCount : Int64

	override fun TextOutput.runTest (editableList: EditableList<Any?>) {

		breakableRepeat(itemsCount) { index ->

			editableList.insert(0, "item$index")

			val newLength = index + 1

			assert { editableList.length == newLength }

			breakableRepeat(newLength) { i ->
				assert { editableList[i] == "item${ newLength - 1 - i }" }
			}

		}

		breakableRepeat(itemsCount) { index ->

			val removeIndex = itemsCount - 1 - index

			editableList.remove(removeIndex)

			val newLength = itemsCount - 1 - index

			assert { editableList.length == newLength }

			breakableRepeat(newLength) { i ->
				assert { editableList[i] == "item${ itemsCount - 1 - i }" }
			}

		}

	}

}
package ic.struct.list.editable.test


import ic.base.assert.assert
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.length.isEmpty
import ic.text.TextOutput


internal abstract class NewListIsEmpty : EditableListTest() {

	override fun TextOutput.runTest (editableList: EditableList<Any?>) {

		assert { editableList.isEmpty }

	}

}
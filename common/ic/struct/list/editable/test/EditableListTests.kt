package ic.struct.list.editable.test


import ic.struct.list.List
import ic.struct.list.editable.default.DefaultEditableList
import ic.test.Test
import ic.test.TestSuite


internal class EditableListTests : TestSuite() {


	override fun initTests() = List<Test>(

		object : EditableListImplementationTests() {
			override val nextEditableList get() = DefaultEditableList<Any?>()
		}

	)


}
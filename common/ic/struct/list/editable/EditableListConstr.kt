package ic.struct.list.editable


import ic.struct.list.editable.default.DefaultEditableList


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> EditableList() : EditableList<Item> = DefaultEditableList()

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> EditableList (vararg items: Item) : EditableList<Item> {
	val list = EditableList<Item>()
	items.forEach { list.add(it) }
	return list
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> EditableList (items: Iterable<Item>) : EditableList<Item> {
	val list = EditableList<Item>()
	items.forEach { list.add(it) }
	return list
}

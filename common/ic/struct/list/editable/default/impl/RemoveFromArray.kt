package ic.struct.list.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.list.editable.default.DefaultEditableList


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Item>
	DefaultEditableList<Item>.removeFromArray (index: Int32)
{

	val array = this.array!!

	var i : Int32 = index
	while (i < lengthField - 1) {
		array[i] = array[i + 1]
		i++
	}

	array[lengthField - 1] = null

}
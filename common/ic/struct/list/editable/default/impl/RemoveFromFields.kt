package ic.struct.list.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.list.editable.default.DefaultEditableList


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Item>
	DefaultEditableList<Item>.removeFromFields (index: Int32)
{

	if (index < 1) item0 = item1
	if (index < 2) item1 = item2
	if (index < 3) item2 = item3
	if (index < 4) item3 = item4
	if (index < 5) item4 = item5
	if (index < 6) item5 = item6
	if (index < 7) item6 = item7
	               item7 = null

}
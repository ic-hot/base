package ic.struct.list.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.list.editable.default.DefaultEditableList


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Item>
	DefaultEditableList<Item>.setIntoFields (index: Int32, item: Item)
{

	if (index < 4) {
		if (index < 2) {
			if (index < 1) {
				item0 = item
			} else {
				item1 = item
			}
		} else {
			if (index < 3) {
				item2 = item
			} else {
				item3 = item
			}
		}
	} else {
		if (index < 6) {
			if (index < 5) {
				item4 = item
			} else {
				item5 = item
			}
		} else {
			if (index < 7) {
				item6 = item
			} else {
				item7 = item
			}
		}
	}

}
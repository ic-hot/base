package ic.struct.list.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.list.editable.default.DefaultEditableList


@Suppress(
	"NOTHING_TO_INLINE",
	"UNCHECKED_CAST"
)
internal inline fun <Item> DefaultEditableList<Item>.getFromFields (index: Int32) : Item = (

	if (index < 4) {
		if (index < 2) {
			if (index < 1) {
				item0
			} else {
				item1
			}
		} else {
			if (index < 3) {
				item2
			} else {
				item3
			}
		}
	} else {
		if (index < 6) {
			if (index < 5) {
				item4
			} else {
				item5
			}
		} else {
			if (index < 7) {
				item6
			} else {
				item7
			}
		}
	}

) as Item
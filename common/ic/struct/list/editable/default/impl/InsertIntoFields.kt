package ic.struct.list.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.list.editable.default.DefaultEditableList


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Item>
	DefaultEditableList<Item>.insertIntoFields (index: Int32, item: Item)
{

	if (index < 4) {
		if (index < 2) {
			if (index < 1) { // index: 0

				if (length > 0) {
					if (length > 1) {
						if (length > 2) {
							if (length > 3) {
								if (length > 4) {
									if (length > 5) {
										if (length > 6) {
											item7 = item6
										}
										item6 = item5
									}
									item5 = item4
								}
								item4 = item3
							}
							item3 = item2
						}
						item2 = item1
					}
					item1 = item0
				}

				item0 = item

			} else { // index: 1

				if (length > 1) {
					if (length > 2) {
						if (length > 3) {
							if (length > 4) {
								if (length > 5) {
									if (length > 6) {
										item7 = item6
									}
									item6 = item5
								}
								item5 = item4
							}
							item4 = item3
						}
						item3 = item2
					}
					item2 = item1
				}

				item1 = item

			}
		} else {
			if (index < 3) { // index: 2

				if (length > 2) {
					if (length > 3) {
						if (length > 4) {
							if (length > 5) {
								if (length > 6) {
									item7 = item6
								}
								item6 = item5
							}
							item5 = item4
						}
						item4 = item3
					}
					item3 = item2
				}

				item2 = item

			} else { // index: 3

				if (length > 3) {
					if (length > 4) {
						if (length > 5) {
							if (length > 6) {
								item7 = item6
							}
							item6 = item5
						}
						item5 = item4
					}
					item4 = item3
				}

				item3 = item

			}
		}
	} else {
		if (index < 6) {
			if (index < 5) { // index: 4

				if (length > 4) {
					if (length > 5) {
						if (length > 6) {
							item7 = item6
						}
						item6 = item5
					}
					item5 = item4
				}

				item4 = item

			} else { // index: 5

				if (length > 5) {
					if (length > 6) {
						item7 = item6
					}
					item6 = item5
				}

				item5 = item

			}
		} else {
			if (index < 7) { // index: 6

				if (length > 6) {
					item7 = item6
				}

				item6 = item

			} else { // index: 7

				item7 = item

			}
		}
	}

}
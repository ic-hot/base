package ic.struct.list.editable.default


import ic.base.annotations.Valid
import ic.base.arrays.Array
import ic.base.arrays.ext.copy
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.length
import ic.base.arrays.ext.set
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.editable.BaseEditableList
import ic.struct.list.editable.default.impl.*
import kotlin.jvm.JvmField


open class DefaultEditableList<Item> : BaseEditableList<Item>() {


	internal @JvmField var lengthField : Int32 = 0

	internal @JvmField var item0 : Any? = null
	internal @JvmField var item1 : Any? = null
	internal @JvmField var item2 : Any? = null
	internal @JvmField var item3 : Any? = null
	internal @JvmField var item4 : Any? = null
	internal @JvmField var item5 : Any? = null
	internal @JvmField var item6 : Any? = null
	internal @JvmField var item7 : Any? = null

	internal @JvmField var array : Array<Any?>? = null


	override val length get() = lengthField.asInt64


	override fun implementGet (@Valid index: Int64) : Item {
		val array = this.array
		if (array == null) {
			return getFromFields(index.asInt32)
		} else {
			@Suppress("UNCHECKED_CAST")
			return array[index] as Item
		}
	}


	override fun implementSet (@Valid index: Int64, item: Item) {
		val array = this.array
		if (array == null) {
			setIntoFields(index.asInt32, item)
		} else {
			array[index] = item
		}
	}


	override fun implementInsert (@Valid index: Int64, item: Item) {
		val length = lengthField
		var array = this.array
		if (array == null) {
			if (length < 8) {
				insertIntoFields(index.asInt32, item)
			} else {
				val newArray = Array(
					item0, item1, item2, item3, item4, item5, item6, item7,
					null,  null,  null,  null,  null,  null,  null,  null
				)
				var i : Int32 = length
				while (i > index) {
					newArray[i] = newArray[i - 1]
					i--
				}
				newArray[index] = item
				this.array = newArray
				item0 = null
				item1 = null
				item2 = null
				item3 = null
				item4 = null
				item5 = null
				item6 = null
				item7 = null
			}
		} else {
			if (array.length == length) {
				array = array.copy(array.length * 2)
				this.array = array
			}
			var i : Int32 = length
			while (i > index) {
				array[i] = array[i - 1]
				i--
			}
			array[index] = item
		}
		lengthField = length + 1
	}


	override fun implementRemove (@Valid index: Int64) : Item {
		val item = implementGet(index)
		if (array == null) {
			removeFromFields(index.asInt32)
		} else {
			removeFromArray(index.asInt32)
		}
		lengthField--
		return item
	}


}
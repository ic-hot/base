package ic.struct.list.editable


import ic.base.primitives.int64.Int64


fun <Item> EditableList<Item>.removeAll (predicate: (Item) -> Boolean) {

	var index : Int64 = 0

	while (index < length) {

		if (predicate(get(index))) {
			remove(index)
		} else {
			index++
		}

	}

}
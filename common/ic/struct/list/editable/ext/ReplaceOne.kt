package ic.struct.list.editable.ext


import ic.base.loop.breakableRepeat
import ic.base.throwables.NotExistsException
import ic.struct.array.ext.set
import ic.struct.list.editable.EditableList


@Throws(NotExistsException::class)
inline fun <Item> EditableList<Item>.replaceOneOrThrowNotExists (

	predicate : (Item) -> Boolean,

	replace : (Item) -> Item

) {
	breakableRepeat(length) { index ->
		val item = get(index)
		if (predicate(item)) {
			set(index, replace(item))
			return
		}
	}
	throw NotExistsException
}


inline fun <Item> EditableList<Item>.replaceOneIfExists (
	predicate : (Item) -> Boolean,
	replace : (Item) -> Item
) {
	try {
		replaceOneOrThrowNotExists(predicate = predicate, replace = replace)
	} catch (_: NotExistsException) {}
}


@Throws(NotExistsException::class)
inline fun <Item> EditableList<Item>.replaceOne (

	predicate : (Item) -> Boolean,

	replace : (Item) -> Item

) {
	try {
		replaceOneOrThrowNotExists(predicate = predicate, replace = replace)
	} catch (_: NotExistsException) {
		throw RuntimeException()
	}
}
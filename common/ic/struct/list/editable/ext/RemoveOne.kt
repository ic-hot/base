package ic.struct.list.editable.ext


import ic.base.primitives.int64.Int64
import ic.base.throwables.NotExistsException
import ic.struct.list.editable.EditableList


inline fun <Item> EditableList<Item>.removeOneOrThrowNotExists (predicate: (Item) -> Boolean) {
	val length = length
	var index : Int64 = 0
	while (index < length) {
		if (predicate(get(index))) {
			remove(index)
			return
		}
		index++
	}
	throw NotExistsException
}


inline fun <Item> EditableList<Item>.removeOneIfExists (predicate: (Item) -> Boolean) {
	try {
		removeOneOrThrowNotExists(predicate)
	} catch (_: NotExistsException) {}
}


inline fun <Item> EditableList<Item>.removeOne (predicate: (Item) -> Boolean) {
	try {
		removeOneOrThrowNotExists(predicate)
	} catch (_: NotExistsException) {
		throw RuntimeException()
	}
}
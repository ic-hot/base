package ic.struct.list.editable.ext


import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


fun <Item> EditableList<Item>.copy() : EditableList<Item> {
	val copy = EditableList<Item>()
	forEach { item ->
		copy.add(item)
	}
	return copy
}
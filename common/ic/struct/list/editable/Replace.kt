package ic.struct.list.editable


import ic.struct.array.ext.set
import ic.struct.list.ext.get


fun <Item> EditableList<Item>.replaceAll (predicate: (Item) -> Boolean, replacer: (Item) -> Item) {

	for (index in 0 until length) {
		val item = get(index)
		if (predicate(item)) {
			set(index, replacer(item))
		}
	}

}
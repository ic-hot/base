package ic.struct.list.editable


import ic.base.annotations.Valid
import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException
import ic.ifaces.immut.FrozenException
import ic.math.annotations.Positive
import ic.struct.array.BaseArray


abstract class BaseEditableList<Item> : BaseArray<Item>(), EditableList<Item> {


	protected abstract fun implementInsert (@Valid @Positive index: Int64, item: Item)

	protected abstract fun implementRemove (@Valid @Positive index: Int64) : Item


	@Throws(IndexOutOfBoundsException::class)
	protected fun throwIndexOutOfBoundsIfInsertionIndexIsInvalid (index: Int64) {
		val isIndexValid = when {
			index < 0 -> false
			index <= length -> true
			else -> false
		}
		if (!isIndexValid) throw IndexOutOfBoundsException
	}


	@Throws(IndexOutOfBoundsException::class)
	override fun insertOrThrowIndexOutOfBounds (index: Int64, item: Item) {
		if (isFrozen) throw FrozenException()
		throwIndexOutOfBoundsIfInsertionIndexIsInvalid(index)
		implementInsert(index, item)
	}


	@Throws(IndexOutOfBoundsException::class)
	override fun removeOrThrowIndexOutOfBounds (index: Int64) : Item {
		if (isFrozen) throw FrozenException()
		throwIndexOutOfBoundsIfIndexIsInvalid(index)
		return implementRemove(index)
	}


}
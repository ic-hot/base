package ic.struct.list.ext


import ic.struct.list.List
import ic.struct.list.join.JoinList


val <Item> List<List<Item>>.joined : List<Item> get() = JoinList(children = this)
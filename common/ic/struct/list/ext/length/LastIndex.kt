package ic.struct.list.ext.length


import ic.base.annotations.Throws
import ic.base.assert.assert
import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List


inline val List<*>.lastIndexOrThrowEmpty : Int64
	@Throws(EmptyException::class)
	get() {
		val length = this.length
		if (length == 0L) {
			throw EmptyException
		} else {
			assert { length > 0 }
			return length - 1
		}
	}
;


inline val List<*>.lastIndex : Int64 get() {
	try {
		return lastIndexOrThrowEmpty
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
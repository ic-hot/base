package ic.struct.list.ext.length


import ic.base.annotations.Throws
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.get


inline val <Item> List<Item>.lastOrThrowEmpty : Item
	@Throws(EmptyException::class)
	get() {
		return get(lastIndexOrThrowEmpty)
	}
;

inline val <Item> List<Item>.last : Item get() {
	try {
		return lastOrThrowEmpty
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}


inline val <Item> List<Item>.lastOrNull : Item? get() {
	try {
		return lastOrThrowEmpty
	} catch (_: EmptyException) {
		return null
	}
}
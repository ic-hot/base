package ic.struct.list.ext.length


import ic.struct.list.List


inline val <Item> List<Item>.orNullIfEmpty get() = if (isEmpty) null else this
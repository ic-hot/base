package ic.struct.list.ext.length


import ic.struct.list.List


inline val List<*>?.isNullOrEmpty get() = this == null || this.isEmpty

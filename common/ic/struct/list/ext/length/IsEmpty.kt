package ic.struct.list.ext.length


import ic.struct.list.List


inline val List<*>.isEmpty get() = length == 0L


inline val List<*>?.isNotEmpty get() = this != null && !isEmpty
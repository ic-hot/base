package ic.struct.list.ext


import ic.struct.list.List
import ic.struct.list.empty.EmptyList


inline val List.Companion.Empty get() = EmptyList
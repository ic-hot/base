package ic.struct.list.ext


import ic.base.arrays.Array
import ic.base.arrays.ext.set
import ic.base.objects.ext.asInt64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEachIndexed
import ic.struct.list.fromarray.ListFromArray


fun <Item> List<Item>.copySet (index: Int64, item: Item) : List<Item> {

	val array : Array<Any?> = Array<Any?>(length = length)

	forEachIndexed { i, oldItem ->
		if (i == index) {
			array[i] = item
		} else {
			array[i] = oldItem
		}
	}

	return ListFromArray(array, isArrayImmutable = true)

}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.copySet (index: Int32, item: Item) : List<Item> {
	return copySet(
		index = index.asInt64,
		item = item
	)
}
package ic.struct.list.ext.cache


import ic.struct.list.List
import ic.struct.list.cache.dynamic.DynamicCacheConvertListWithItemsInBetween


inline fun <Item, NewItem: Any> (() -> List<Item>).cacheConvert (
	crossinline initItemInBetween : () -> NewItem,
	crossinline convertItem : (() -> Item) -> NewItem
) : List<NewItem> {

	val getSourceList = this

	return object : DynamicCacheConvertListWithItemsInBetween<NewItem, Item>() {

		override val sourceList get() = getSourceList()

		override fun initItemInBetween() : NewItem {
			return initItemInBetween()
		}

		override fun convertItem (getSourceItem: () -> Item): NewItem {
			return convertItem(getSourceItem)
		}

	}

}
package ic.struct.list.ext.cache


import ic.struct.list.List
import ic.struct.list.cache.dynamic.DynamicCacheConvertList
import ic.struct.list.cache.dynamic.DynamicCacheConvertList.ItemHolder


inline fun <Item, NewItem> (() -> List<Item>).cacheConvert (
	crossinline convertItem : ItemHolder<NewItem, Item>.() -> NewItem
) : List<NewItem> {

	val getSourceList = this

	return object : DynamicCacheConvertList<NewItem, Item>() {

		override val sourceList get() = getSourceList()

		override fun ItemHolder<NewItem, Item>.convertItem() : NewItem {
			return convertItem()
		}

	}

}
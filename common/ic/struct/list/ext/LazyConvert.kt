package ic.struct.list.ext


import ic.struct.list.ConvertList
import ic.struct.list.List


inline fun <Item, NewItem> List<Item>.lazyConvert (
	crossinline convertItem : (Item) -> NewItem
) : List<NewItem> {

	return ConvertList(
		sourceList = this,
		convertItem = convertItem
	)

}
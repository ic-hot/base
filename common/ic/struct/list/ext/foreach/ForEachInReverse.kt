package ic.struct.list.ext.foreach


import ic.base.loop.repeat
import ic.struct.list.List


inline fun <Item> List<Item>.forEachInReverse (
	action : (item: Item) -> Unit
) {

	val length = this.length

	repeat(length) { index ->

		action(
			this[length - 1 - index]
		)

	}

}
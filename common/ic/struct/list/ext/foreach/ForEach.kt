package ic.struct.list.ext.foreach


import ic.base.loop.repeat
import ic.struct.list.List


inline fun <Item> List<Item>.forEach (

	action : (Item) -> Unit

) {

	repeat(length) { index ->

		val item = this[index]
		action(item)

	}

}


@Deprecated("Use forEach")
inline fun <Item> List<Item>.nonBreakableForEach (
	action : (Item) -> Unit
) {
	forEach(action =action)
}
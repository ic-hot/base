package ic.struct.list.ext.foreach


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asInt32
import ic.funs.effect.Effect
import ic.struct.list.List


inline fun <Item> List<Item>.forEachIndexAsInt32 (effect: Effect<Int32>) {
	repeat(length.asInt32, effect)
}
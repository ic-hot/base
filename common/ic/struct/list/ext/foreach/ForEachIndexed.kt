package ic.struct.list.ext.foreach


import ic.base.loop.repeat
import ic.base.primitives.int64.Int64
import ic.ifaces.action.Action2
import ic.struct.list.List


inline fun <Item> List<Item>.forEachIndexed (
	action : (index: Int64, item: Item) -> Unit
) {
	repeat(length) { index ->
		action(index, this[index])
	}
}


fun <Item> List<Item>.forEachIndexed (action: Action2<Int64, Item>) {
	forEachIndexed { index, item -> action.run(index, item) }
}
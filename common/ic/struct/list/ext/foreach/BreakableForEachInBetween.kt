package ic.struct.list.ext.foreach


import ic.struct.list.List


@Deprecated("To remove")
inline fun <Item> List<Item>.breakableForEach (
	onBreak : () -> Unit = {},
	doInBetween : () -> Unit,
	action : (Item) -> Unit
) {

	breakableForEachIndexed(onBreak = onBreak) { index, item ->

		if (index > 0) {
			doInBetween()
		}

		action(item)
		
	}

} 
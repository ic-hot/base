package ic.struct.list.ext.foreach


import ic.base.loop.breakableRepeat
import ic.base.primitives.int64.Int64
import ic.ifaces.action.Action2
import ic.struct.list.List


@Deprecated("Use forEachIndexed")
inline fun <Item> List<Item>.breakableForEachIndexed (
	onBreak : () -> Unit = {},
	action : (index: Int64, item: Item) -> Unit
) {
	breakableRepeat(length, onBreak = onBreak) { index ->
		action(index, this[index])
	}
}


@Deprecated("Use forEachIndexed")
fun <Item> List<Item>.breakableForEachIndexed (action: Action2<Int64, Item>) {
	breakableForEachIndexed { index, item -> action.run(index, item) }
}
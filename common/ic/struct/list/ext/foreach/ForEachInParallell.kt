package ic.struct.list.ext.foreach


import ic.base.primitives.int32.Int32
import ic.struct.list.List


inline fun <Item> List<Item>.forEachInParallel (

	maxThreadsCount : Int32 = Int32.MAX_VALUE,

	crossinline action: (Item) -> Unit

) {

	forEachInParallelIndexed(maxThreadsCount = maxThreadsCount) { _, item ->
		action(item)
	}

}
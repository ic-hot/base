package ic.struct.list.ext.foreach


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.parallel.funs.doinbeam.doInBeamBlocking
import ic.struct.list.List
import ic.struct.list.ext.length.isEmpty


inline fun <Item> List<Item>.forEachInParallelIndexed (

	maxThreadsCount : Int32 = Int32.MAX_VALUE,

	crossinline action: (index: Int64, Item) -> Unit

) {

	if (isEmpty) return

	val thisList = this

	doInBeamBlocking(
		actionsCount = length,
		maxThreadsCount = maxThreadsCount
	) { index ->
		action(
			index,
			thisList[index]
		)
	}

}
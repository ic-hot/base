package ic.struct.list.ext.foreach


import ic.base.loop.breakableRepeat
import ic.struct.list.List


@Deprecated("Do not use")
inline fun <Item> List<Item>.breakableForEach (
	onBreak : () -> Unit,
	action : (Item) -> Unit
) {
	breakableRepeat(length, onBreak = onBreak) { index ->
		val item = this[index]
		action(item)
	}
}


@Deprecated("Do not use")
inline fun <Item> List<Item>.breakableForEach (action : (Item) -> Unit) {
	breakableForEach(onBreak = {}, action = action)
}
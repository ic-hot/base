package ic.struct.list.ext.copy


import ic.base.assert.assert
import ic.base.escape.skip.skippable
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.base.throwables.Skip
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEachIndexed


inline fun <OldItem, NewItem> List<OldItem>.copySplitIntoFixedSizePortions (

	portionSize : Int32,

	crossinline convertItem : (globalIndex: Int64, OldItem) -> NewItem,

	fillGapsOrSkip : () -> NewItem = { throw Skip }

) : List<List<NewItem>> {

	assert { portionSize > 0 }

	val portions = EditableList<List<NewItem>>()

	var currentPortion : EditableList<NewItem>? = null

	breakableForEachIndexed { index, item ->

		if (currentPortion == null) {
			currentPortion = EditableList()
		}

		currentPortion!!.add(convertItem(index, item))

		if (currentPortion!!.length.asInt32 == portionSize) {

			portions.add(currentPortion!!)

			currentPortion = null

		}

	}

	if (currentPortion != null) {

		skippable {
			while (currentPortion!!.length < portionSize) {
				currentPortion!!.add(fillGapsOrSkip())
			}
		}

		portions.add(currentPortion!!)

	}

	return portions

}


inline fun
	<Item, OldItem: Item, NewItem: Item>
	List<OldItem>.copySplitIntoFixedSizePortions (
	portionSize : Int32,
	fillGapsOrSkip : () -> NewItem = { throw Skip }
)
	: List<List<Item>>
{
	return copySplitIntoFixedSizePortions(
		portionSize = portionSize,
		convertItem = { _, item -> item },
		fillGapsOrSkip = fillGapsOrSkip
	)
}
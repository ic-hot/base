package ic.struct.list.ext.copy


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


fun <Item> List<out FiniteSet<out Item>>.copyUnion() : FiniteSet<Item> {

	val result = EditableSet<Item>()

	breakableForEach { set ->

		set.breakableForEach { item ->

			result.addIfNotExists(item)

		}

	}

	return result

}
package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.reduce.find.none


inline fun <Item> List<Item>.copyRemoveDuplicates (
	areEqual : (Item, Item) -> Boolean = { a, b -> a == b }
) : List<Item> {
	val result = EditableList<Item>()
	forEach { item ->
		if (result.none { areEqual(it, item) }) {
			result.add(item)
		}
	}
	return result
}
package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEachIndexed


fun <Item, OldItem: Item, NewItem: Item> List<OldItem>.copy (
	initItemInBetween : () -> NewItem
) : List<Item> {
	val result = EditableList<Item>()
	breakableForEachIndexed { index, item ->
		if (index > 0) {
			result.add(initItemInBetween())
		}
		result.add(item)
	}
	return result
}
package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEach


inline fun <Item> List<Item>.copyReplace (

	condition : (Item) -> Boolean,

	newItem : Item

) : List<Item> {

	val result = EditableList<Item>()
	breakableForEach { item ->
		if (condition(item)) {
			result.add(newItem)
		} else {
			result.add(item)
		}
	}
	return result

}
package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.length.isNotEmpty


inline fun

	<OldItem, NewItem, ConvertedItem: NewItem, ItemInBetween: NewItem>

	List<OldItem>.copyConvert (

		initItemInBetween : () -> ItemInBetween,

		@Skippable convertItem : (OldItem) -> ConvertedItem

	)

	: List<NewItem>

{

	val result = EditableList<NewItem>()

	forEach { item ->

		if (result.isNotEmpty) {
			result.add(
				initItemInBetween()
			)
		}

		skippable {

			result.add(
				convertItem(item)
			)

		}

	}

	return result

}
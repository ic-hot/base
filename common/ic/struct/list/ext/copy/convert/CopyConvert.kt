package ic.struct.list.ext.copy.convert


import ic.base.escape.breakable.Breakable
import ic.base.escape.breakable.breakable
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


inline fun <Item, NewItem> List<Item>.copyConvert (

	@Skippable @Breakable convertItem : (Item) -> NewItem

) : List<NewItem> {

	val result = EditableList<NewItem>()

	breakable {

		forEach { item ->

			skippable {

				result.add(
					convertItem(item)
				)

			}

		}

	}
	
	return result

}
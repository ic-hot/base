package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.ext.copy.toArray
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


inline fun <Item, reified NewItem> List<Item>.copyConvertToArray (

	@Skippable convertItem : (Item) -> NewItem

) : Array<NewItem> {

	val result = EditableList<NewItem>()

	forEach { item ->
		skippable {
			result.add(
				convertItem(item)
			)
		}
	}

	return result.toArray()

}
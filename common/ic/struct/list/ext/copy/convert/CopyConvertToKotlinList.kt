package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.struct.list.List


inline fun <Item, NewItem> List<Item>.copyConvertToKotlinList (

	@Skippable convertItem : (Item) -> NewItem

) : kotlin.collections.List<NewItem> {

	return copyConvertToKotlinMutableList(convertItem = convertItem)

}
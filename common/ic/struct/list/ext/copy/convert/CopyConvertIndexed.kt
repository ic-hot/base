package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEachIndexed


inline fun <Item, NewItem> List<Item>.copyConvertIndexed (

	@Skippable crossinline convertItem : (index: Int64, Item) -> NewItem

) : List<NewItem> {

	val result = EditableList<NewItem>()

	forEachIndexed { index, item ->
		skippable {
			result.add(
				convertItem(index, item)
			)
		}
	}

	return result

}
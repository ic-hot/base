package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.collection.Collection
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEach


inline fun <Item, NewItem> List<Item>.copyConvertToCollection (

	@Skippable convertItem : (Item) -> NewItem

) : Collection<NewItem> {

	val result = EditableList<NewItem>()

	breakableForEach { item ->

		skippable {

			result.add(
				convertItem(item)
			)

		}

	}

	return result

}
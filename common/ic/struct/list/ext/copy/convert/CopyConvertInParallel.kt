package ic.struct.list.ext.copy.convert


import ic.base.primitives.int32.Int32
import ic.parallel.funs.doinbeam.doInBeamBlocking
import ic.struct.array.Array
import ic.struct.array.ext.set
import ic.struct.list.List


inline fun <OldItem, NewItem> List<OldItem>.copyConvertInParallel (

	maxThreadsCount : Int32 = Int32.MAX_VALUE,

	crossinline convertItem : (OldItem) -> NewItem

) : List<NewItem> {

	val oldList = this

	val length = this.length

	when {
		length <= 0 -> return List()
		length == 1L -> return List(convertItem(this[0]))
	}

	val newList = Array<NewItem>(length = length)

	doInBeamBlocking(
		actionsCount = length,
		maxThreadsCount = maxThreadsCount
	) { index ->

		val oldItem = oldList[index]

		val newItem = convertItem(oldItem)

		newList[index] = newItem

	}

	return newList

}
package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


inline fun <Item, NewItem> List<Item>.copyConvertIndexed (

	initItemInBetween : () -> NewItem,

	@Skippable crossinline convertItem : (index: Int64, Item) -> NewItem

) : List<NewItem> {

	val result = EditableList<NewItem>()

	var index : Int64 = 0

	forEach { item ->
		skippable {
			val newItem = convertItem(index, item)
			if (index > 0) {
				result.add(
					initItemInBetween()
				)
			}
			result.add(newItem)
			index++
		}
	}

	return result

}
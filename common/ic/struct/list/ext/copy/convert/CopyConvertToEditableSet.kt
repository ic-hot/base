package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


inline fun <Item, NewItem> List<Item>.copyConvertToEditableSet (

	@Skippable convertItem : (Item) -> NewItem

) : EditableSet<NewItem> {
	val result = EditableSet<NewItem>()
	forEach { item ->
		skippable {
			result.addIfNotExists(
				convertItem(item)
			)
		}
	}
	return result
}
package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


inline fun <Item, NewItem> List<Item>.copyConvertToKotlinMutableList (

	@Skippable convertItem : (Item) -> NewItem

) : kotlin.collections.MutableList<NewItem> {

	val result = mutableListOf<NewItem>()

	forEach { item ->

		skippable {

			result.add(
				convertItem(item)
			)

		}

	}

	return result

}
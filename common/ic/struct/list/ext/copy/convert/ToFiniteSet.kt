package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


inline fun <Item, NewItem> List<Item>.copyConvertToFiniteSet (

	@Skippable convertItem : (Item) -> NewItem

) : FiniteSet<NewItem> {
	val result = EditableSet<NewItem>()
	forEach { item ->
		skippable {
			result.addIfNotExists(
				convertItem(item)
			)
		}
	}
	return result
}
package ic.struct.list.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.nonBreakableForEach


inline fun <Item, NewItem> List<Item>.copyConvertToEditableList (

	@Skippable convertItem : (Item) -> NewItem

) : EditableList<NewItem> {
	val result = EditableList<NewItem>()
	nonBreakableForEach { item ->
		skippable {
			result.add(
				convertItem(item)
			)
		}
	}
	return result
}
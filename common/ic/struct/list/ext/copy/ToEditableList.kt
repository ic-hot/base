package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


fun <Item> List<Item>.copyToEditableList() : EditableList<Item> {

	val result = EditableList<Item>()

	forEach { item ->

		result.add(item)

	}

	return result

}


fun <Item> List<Item>.copyToEditableListIfNotAlready() : EditableList<Item> {
	if (this is EditableList) {
		return this
	} else {
		return copyToEditableList()
	}
}
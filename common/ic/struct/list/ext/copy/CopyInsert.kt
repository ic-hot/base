package ic.struct.list.ext.copy


import ic.base.assert.assert
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEachIndexed
import ic.struct.list.ext.length.isEmpty


fun <Item> List<Item>.copyInsert (index: Int64, item: Item) : List<Item> {

	if (isEmpty) {
		assert { index == 0L }
		return List(item)
	}

	val result = EditableList<Item>()

	breakableForEachIndexed { i, existingItem ->
		if (i == index) {
			result.add(item)
		}
		result.add(existingItem)
	}

	return result

}
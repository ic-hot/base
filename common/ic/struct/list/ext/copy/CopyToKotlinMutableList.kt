package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


fun <Item> List<Item>.copyToKotlinMutableList() : kotlin.collections.MutableList<Item> {
	val result = mutableListOf<Item>()
	breakableForEach { item ->
		result.add(item)
	}
	return result
}
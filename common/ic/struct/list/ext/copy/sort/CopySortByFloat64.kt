package ic.struct.list.ext.copy.sort


import ic.base.kcollections.ext.copy.copyToList
import ic.base.primitives.float64.Float64
import ic.struct.list.ext.copy.copyToKotlinMutableList
import ic.struct.list.List


inline fun <Item> List<Item>.copySortByFloat64 (
	crossinline itemToFloat64 : (Item) -> Float64
) : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sortWith { a, b -> itemToFloat64(a) compareTo itemToFloat64(b) }
	return kotlinList.copyToList()
}
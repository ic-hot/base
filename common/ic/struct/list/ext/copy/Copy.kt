package ic.struct.list.ext.copy


import ic.base.arrays.Array
import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


fun <Item> List<Item>.copy() : List<Item> {
	return ListFromArray(
		array = Array<Any?>(length) { this[it] },
		isArrayImmutable = true
	)
}
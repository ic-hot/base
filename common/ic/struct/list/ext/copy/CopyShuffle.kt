package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


fun <Item> List<Item>.copyShuffle() : List<Item> {

    val array = toUntypedArray()

    array.shuffle()

    return ListFromArray(array, isArrayImmutable = true)

}
package ic.struct.list.ext.copy


import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.copyToKotlinList() : kotlin.collections.List<Item> {
	return copyToKotlinMutableList()
}
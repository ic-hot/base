package ic.struct.list.ext.copy


import ic.base.compare.Comparison
import ic.base.kcollections.ext.copy.copyToList
import ic.struct.collection.ext.copy.copyToKotlinMutableList
import ic.struct.list.List


fun <Item: Comparable<Item>> List<Item>.copySort() : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sort()
	return kotlinList.copyToList()
}


inline fun <Item> List<Item>.copySort (
	crossinline compareItems : (Item, Item) -> Comparison
) : List<Item> {
	val kotlinList = copyToKotlinMutableList()
	kotlinList.sortWith { a, b -> compareItems(a, b) }
	return kotlinList.copyToList()
}
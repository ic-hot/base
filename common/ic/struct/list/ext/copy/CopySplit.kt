package ic.struct.list.ext.copy


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item> List<Item>.copySplit (

	crossinline areInSameGroup : (Item, Item) -> Boolean

) : List<List<Item>> {

	val groups = EditableList<List<Item>>()

	lateinit var currentGroup : EditableList<Item>

	var atLeastOneItem : Boolean = false

	var isFirstItem : Boolean = true

	var previousItemOrNull : Item? = null

	breakableForEach { item ->

		if (isFirstItem) {

			currentGroup = EditableList()

			atLeastOneItem = true

		} else {

			@Suppress("UNCHECKED_CAST")
			val previousItem = previousItemOrNull as Item

			if (!areInSameGroup(previousItem, item)) {

				groups.add(currentGroup)
				currentGroup = EditableList()

			}

		}

		currentGroup.add(item)

		isFirstItem = false
		previousItemOrNull = item

	}

	if (atLeastOneItem) {
		groups.add(currentGroup)
	}

	return groups

}
package ic.struct.list.ext.copy


import ic.base.primitives.int64.Int64
import ic.struct.list.List


fun <Item> List<Item>.copyRange (startIndex: Int64, endIndex: Int64) : List<Item> {

	return List(length = endIndex - startIndex) { index ->
		this[startIndex + index]
	}

}
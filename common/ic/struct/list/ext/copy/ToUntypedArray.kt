package ic.struct.list.ext.copy


import ic.base.arrays.Array
import ic.struct.list.List
import ic.struct.list.ext.get


fun <Item> List<Item>.toUntypedArray() : Array<Any?> {

	return Array(length) { index -> this[index] }

}
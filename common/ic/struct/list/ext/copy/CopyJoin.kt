package ic.struct.list.ext.copy


import kotlin.jvm.JvmName

import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


@JvmName("copyJoinListOfLists")
fun <Item> List<List<Item>>.copyJoin() : List<Item> {
	val result = EditableList<Item>()
	forEach { child ->
		child.forEach { item ->
			result.add(item)
		}
	}
	return result
}


@JvmName("copyJoinListOfCollections")
fun <Item> List<Collection<Item>>.copyJoin() : List<Item> {
	val result = EditableList<Item>()
	forEach { child ->
		child.forEach { item ->
			result.add(item)
		}
	}
	return result
}
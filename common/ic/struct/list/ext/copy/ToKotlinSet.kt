package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


fun <Item> List<Item>.toKotlinSet() : kotlin.collections.Set<Item> {
	val result = mutableSetOf<Item>()
	breakableForEach { item ->
		result.add(item)
	}
	return result
}
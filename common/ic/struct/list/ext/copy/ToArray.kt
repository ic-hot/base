package ic.struct.list.ext.copy


import ic.base.arrays.*
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.list.List


inline fun <reified Item> List<Item>.toArray() : Array<Item> {
	return Array(length) { index -> this[index] }
}

fun List<Int32>.toArray() : Int32Array {
	return Int32Array(length) { index -> this[index] }
}

fun List<Int64>.toArray() : Int64Array {
	return Int64Array(length) { index -> this[index] }
}

fun List<Float32>.toArray() : Float32Array {
	return Float32Array(length) { index -> this[index] }
}

fun List<Float64>.toArray() : Float64Array {
	return Float64Array(length) { index -> this[index] }
}
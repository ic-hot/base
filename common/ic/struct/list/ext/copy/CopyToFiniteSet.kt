package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


fun <Item> List<Item>.copyToFiniteSet() : FiniteSet<Item> {
	val result = EditableSet<Item>()
	forEach { item ->
		result.addIfNotExists(item)
	}
	return result
}
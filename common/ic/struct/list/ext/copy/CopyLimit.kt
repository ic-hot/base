package ic.struct.list.ext.copy


import ic.base.escape.breakable.Break
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEachIndexed


fun <Item> List<Item>.copyLimit (length: Int64) : List<Item> {

	val result = EditableList<Item>()

	breakableForEachIndexed { index, item ->

		if (index < length) {

			result.add(item)

		} else {

			Break()

		}

	}

	return result

}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.copyLimit (length: Int32) : List<Item> {
	return copyLimit(length = length.asInt64)
}
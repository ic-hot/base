package ic.struct.list.ext.copy


import ic.struct.collection.Collection
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.breakableForEach


fun <Item> List<Item>.copyToCollection() : Collection<Item> {

	val result = EditableList<Item>()

	breakableForEach { item ->

		result.add(item)

	}

	return result

}
package ic.struct.list.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach


inline fun <Item> List<Item>.copyRemoveAll (condition: (Item) -> Boolean) : List<Item> {
	val result = EditableList<Item>()
	forEach { item ->
		if (!condition(item)) {
			result.add(item)
		}
	}
	return result
}
package ic.struct.list.ext


import ic.base.primitives.int64.Int64
import ic.base.throwables.NotExistsException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEachIndexed


@Throws(NotExistsException::class)
inline fun <Item> List<Item>.findIndexOrThrowNotExists (predicate: (Item) -> Boolean) : Int64 {
	breakableForEachIndexed { index, item ->
		if (predicate(item)) return index
	}
	throw NotExistsException
}


inline fun <Item> List<Item>.findIndexOrNull (predicate: (Item) -> Boolean) : Int64? {
	try {
		return findIndexOrThrowNotExists(predicate)
	} catch (t: NotExistsException) {
		return null
	}
}


fun <Item> List<Item>.findIndex (predicate: (Item) -> Boolean) : Int64 {
	try {
		return findIndexOrThrowNotExists(predicate)
	} catch (t: NotExistsException) {
		throw RuntimeException()
	}
}

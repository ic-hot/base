package ic.struct.list.ext


import ic.base.primitives.int64.Int64

import ic.struct.list.List


inline val List<*>.lastIndex : Int64 get() = length - 1
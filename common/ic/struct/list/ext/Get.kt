package ic.struct.list.ext


import ic.base.escape.skip.skip
import ic.base.escape.skip.Skippable
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException
import ic.struct.list.List


inline fun <Item> List<Item>.get (index: Int64, or: () -> Item) : Item {
	try {
		return getOrThrowIndexOutOfBounds(index)
	} catch (_: IndexOutOfBoundsException) {
		return or()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.getOrNull (index: Int64) : Item? {
	try {
		return getOrThrowIndexOutOfBounds(index)
	} catch (_: IndexOutOfBoundsException) {
		return null
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.getOrNull (index: Int32) : Item? {
	try {
		return getOrThrowIndexOutOfBounds(index.asInt64)
	} catch (t: IndexOutOfBoundsException) {
		return null
	}
}


@Suppress("NOTHING_TO_INLINE")
@Skippable
inline fun <Item> List<Item>.getOrSkip (index: Int64) : Item {
	try {
		return getOrThrowIndexOutOfBounds(index)
	} catch (_: IndexOutOfBoundsException) {
		skip
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.getOrNull (index: Int64?) : Item? {
	return getOrNull(index = index ?: return null)
}
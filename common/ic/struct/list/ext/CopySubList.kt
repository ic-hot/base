package ic.struct.list.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.List


fun <Item> List<Item>.copySubList (from: Int64, to: Int64) : List<Item> {

	return List(length = to - from) { index ->
		this[index + from]
	}

}


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.copySubList (from: Int32, to: Int64) : List<Item> {
	return copySubList(from = from.asInt64, to = to)
}
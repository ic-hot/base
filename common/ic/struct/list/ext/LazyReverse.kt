@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.list.ext


import ic.struct.list.List
import ic.struct.list.reverse.ReverseList


inline fun <Item> List<Item>.lazyReverse() : List<Item> = ReverseList(this)
package ic.struct.list.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt64OrThrowEmpty (
	@Skippable itemToInt64 : (Item) -> Int64
) : Int64 {
	var maxInt64 : Int64 = Int64.MIN_VALUE
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		skippable {
			val int64 = itemToInt64(item)
			if (int64 > maxInt64) maxInt64 = int64
			atLeastOneItem = true
		}
	}
	if (atLeastOneItem) {
		return maxInt64
	} else {
		throw EmptyException
	}
}


inline fun <Item> List<Item>?.maxInt64 (
	or: Int64,
	@Skippable itemToInt64 : (Item) -> Int64
) : Int64 {
	if (this == null) return or
	try {
		return maxInt64OrThrowEmpty(itemToInt64)
	} catch (t: EmptyException) {
		return or
	}
}
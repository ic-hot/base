package ic.struct.list.ext


import ic.struct.list.List


fun <Item> List<Item>.copyReverse() : List<Item> {
	val length = this.length
	return List(length = length) { index ->
		this[length - 1 - index]
	}
}
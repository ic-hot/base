package ic.struct.list.ext


import ic.base.primitives.int32.Int32
import ic.struct.list.List


fun <Item, Result> List<Item>.reduce (

	startIndex : Int32 = 0,

	or : () -> Result,

	action : (Item, next: () -> Result) -> Result,

) : Result {

	if (startIndex < length) {

		return action(
			this[startIndex],
			{
				reduce(
					startIndex = startIndex + 1,
					or = or,
					action = action
				)
			}
		)

	} else {

		return or()

	}

}
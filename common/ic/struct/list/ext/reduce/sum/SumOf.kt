package ic.struct.list.ext.reduce.sum


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.sumOf (itemToInt32: (Item) -> Int32) : Int32 {
	var sum : Int32 = 0
	forEach { item ->
		sum += itemToInt32(item)
	}
	return sum
}

@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.sumOf (itemToInt64: (Item) -> Int64) : Int64 {
	var sum : Int64 = 0
	forEach { item ->
		sum += itemToInt64(item)
	}
	return sum
}

@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.sumOf (itemToFloat32: (Item) -> Float32) : Float32 {
	var sum : Float32 = 0F
	forEach { item ->
		sum += itemToFloat32(item)
	}
	return sum
}

@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.sumOf (itemToFloat64: (Item) -> Float64) : Float64 {
	var sum : Float64 = 0.0
	forEach { item ->
		sum += itemToFloat64(item)
	}
	return sum
}
package ic.struct.list.ext.reduce.sum


import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


val List<Int32>.sum : Int32 get() {
	var sum : Int32 = 0
	forEach { item ->
		sum += item
	}
	return sum
}
package ic.struct.list.ext.reduce.avg


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.avgOfOrThrow (itemToFloat64: (Item) -> Float64) : Float64 {
	var sum : Float64 = 0.0
	var count : Int64 = 0
	forEach { item ->
		sum += itemToFloat64(item)
		count++
	}
	if (count > 0) {
		return sum / count
	} else {
		throw EmptyException
	}
}


inline fun <Item> List<Item>.avgOf (itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return avgOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

inline fun <Item> List<Item>.avgOf (or: Float64, itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return avgOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return or
	}
}

inline fun <Item> List<Item>.avgOfOrNull (itemToFloat64: (Item) -> Float64) : Float64? {
	try {
		return avgOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return null
	}
}
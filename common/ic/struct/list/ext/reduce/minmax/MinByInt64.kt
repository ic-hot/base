package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach

import kotlin.experimental.ExperimentalTypeInference


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.minByOrThrow (itemToValue: (Item) -> Int64) : Item {
	var minValue : Int64 = Int64.MAX_VALUE
	var minItem : Item? = null
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val value = itemToValue(item)
		if (atLeastOneItem) {
			if (value < minValue) {
				minItem = item
				minValue = value
			}
		} else {
			atLeastOneItem = true
			minItem = item
			minValue = value
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return minItem as Item
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minBy (itemToValue: (Item) -> Int64) : Item {
	try {
		return minByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minByOrNull (itemToValue: (Item) -> Int64) : Item? {
	try {
		return minByOrThrow(itemToValue)
	} catch (_: EmptyException) {
		return null
	}
}
package ic.struct.list.ext.reduce.minmax


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float32.Float32
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.minOfOrThrow (itemToFloat32: (Item) -> Float32) : Float32 {
	var minFloat32 : Float32 = Float32.POSITIVE_INFINITY
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float32 = itemToFloat32(item)
		if (atLeastOneItem) {
			if (float32 < minFloat32) {
				minFloat32 = float32
			}
		} else {
			atLeastOneItem = true
			minFloat32 = float32
		}
	}
	if (atLeastOneItem) {
		return minFloat32
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.minOf (itemToFloat32: (Item) -> Float32) : Float32 {
	try {
		return minOfOrThrow(itemToFloat32)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEachIndexed


@Throws(EmptyException::class)
inline fun <Item> List<Item>.indexOfMinOrThrow (itemToValue: (Item) -> Float64) : Int64 {
	var minValue : Float64 = Float64.POSITIVE_INFINITY
	var result : Int64 = 0
	var atLeastOne : Boolean = false
	forEachIndexed { index, item ->
		val value = itemToValue(item)
		if (atLeastOne) {
			if (value < minValue) {
				result = index
				minValue = value
			}
		} else {
			atLeastOne = true
			result = index
			minValue = value
		}
	}
	if (atLeastOne) {
		return result
	} else {
		throw EmptyException
	}
}

inline fun <Item> List<Item>.indexOfMin (itemToValue: (Item) -> Float64) : Int64 {
	try {
		return indexOfMinOrThrow(itemToValue)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
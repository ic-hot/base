package ic.struct.list.ext.reduce.minmax


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float32.Float32
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.minByOrThrow (itemToFloat32: (Item) -> Float32) : Item {
	var minFloat32 : Float32 = Float32.POSITIVE_INFINITY
	var minItem : Item? = null
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float32 = itemToFloat32(item)
		if (atLeastOneItem) {
			if (float32 < minFloat32) {
				minItem = item
				minFloat32 = float32
			}
		} else {
			atLeastOneItem = true
			minItem = item
			minFloat32 = float32
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return minItem as Item
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minBy (itemToFloat32: (Item) -> Float32) : Item {
	try {
		return minByOrThrow(itemToFloat32)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
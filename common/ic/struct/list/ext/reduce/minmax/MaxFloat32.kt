package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.float32.Float32
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxFloat32OrThrow (itemToFloat32: (Item) -> Float32) : Float32 {
	var maxFloat32 : Float32 = Float32.NEGATIVE_INFINITY
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float32 = itemToFloat32(item)
		if (atLeastOneItem) {
			if (float32 > maxFloat32) {
				maxFloat32 = float32
			}
		} else {
			atLeastOneItem = true
			maxFloat32 = float32
		}
	}
	if (atLeastOneItem) {
		return maxFloat32
	} else {
		throw EmptyException
	}
}


inline fun <Item> List<Item>.maxFloat32 (itemToFloat32: (Item) -> Float32) : Float32 {
	try {
		return maxFloat32OrThrow(itemToFloat32)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

inline fun <Item> List<Item>.maxFloat32OrNull (itemToFloat32: (Item) -> Float32) : Float32? {
	try {
		return maxFloat32OrThrow(itemToFloat32)
	} catch (_: EmptyException) {
		return null
	}
}
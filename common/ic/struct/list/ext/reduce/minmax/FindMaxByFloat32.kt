package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.float32.Float32
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.findMaxByFloat32OrThrowEmpty (
	itemToFloat32 : (Item) -> Float32
) : Item {
	var maxFloat32 : Float32 = Float32.NEGATIVE_INFINITY
	var maxItem : Item? = null
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		val float32 = itemToFloat32(item)
		if (atLeastOneItem) {
			if (float32 > maxFloat32) {
				maxItem = item
				maxFloat32 = float32
			}
		} else {
			atLeastOneItem = true
			maxItem = item
			maxFloat32 = float32
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return maxItem as Item
	} else {
		throw EmptyException
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.findMaxByFloat32 (itemToFloat32: (Item) -> Float32) : Item {
	try {
		return findMaxByFloat32OrThrowEmpty(itemToFloat32)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
package ic.struct.list.ext.reduce.minmax


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float64.Float64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxOfOrThrow (itemToFloat64: (Item) -> Float64) : Float64 {
	var maxFloat64 : Float64 = Float64.NEGATIVE_INFINITY
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float64 = itemToFloat64(item)
		if (atLeastOneItem) {
			if (float64 > maxFloat64) {
				maxFloat64 = float64
			}
		} else {
			atLeastOneItem = true
			maxFloat64 = float64
		}
	}
	if (atLeastOneItem) {
		return maxFloat64
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.maxOf (itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return maxOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}

inline fun <Item> List<Item>.maxOf (or: Float64, itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return maxOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return or
	}
}

inline fun <Item> List<Item>.maxOfOrNull (itemToFloat64: (Item) -> Float64) : Float64? {
	try {
		return maxOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return null
	}
}
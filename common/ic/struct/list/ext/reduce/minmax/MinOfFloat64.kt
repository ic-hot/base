package ic.struct.list.ext.reduce.minmax


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.float64.Float64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
@Throws(EmptyException::class)
inline fun <Item> List<Item>.minOfOrThrow (itemToFloat64: (Item) -> Float64) : Float64 {
	var minFloat64 : Float64 = Float64.POSITIVE_INFINITY
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float64 = itemToFloat64(item)
		if (atLeastOneItem) {
			if (float64 < minFloat64) {
				minFloat64 = float64
			}
		} else {
			atLeastOneItem = true
			minFloat64 = float64
		}
	}
	if (atLeastOneItem) {
		return minFloat64
	} else {
		throw EmptyException
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minOf (itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return minOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minOf (or: Float64, itemToFloat64: (Item) -> Float64) : Float64 {
	try {
		return minOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return or
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> List<Item>.minOfOrNull (itemToFloat64: (Item) -> Float64) : Float64? {
	try {
		return minOfOrThrow(itemToFloat64)
	} catch (_: EmptyException) {
		return null
	}
}
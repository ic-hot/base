package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.int64.Int64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt64OrThrowEmpty (itemToInt64: (Item) -> Int64) : Int64 {
	var maxInt64 : Int64 = Int64.MIN_VALUE
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		val int64 = itemToInt64(item)
		if (atLeastOneItem) {
			if (int64 > maxInt64) {
				maxInt64 = int64
			}
		} else {
			atLeastOneItem = true
			maxInt64 = int64
		}
	}
	if (atLeastOneItem) {
		return maxInt64
	} else {
		throw EmptyException
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt64 (itemToInt64: (Item) -> Int64) : Int64 {
	try {
		return maxInt64OrThrowEmpty(itemToInt64)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt64 (
	or : Int64,
	itemToInt64 : (Item) -> Int64
) : Int64 {
	try {
		return maxInt64OrThrowEmpty(itemToInt64)
	} catch (_: EmptyException) {
		return or
	}
}
package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.float64.Float64
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.findMaxByFloat64OrThrowEmpty (
	itemToFloat64 : (Item) -> Float64
) : Item {
	var maxFloat64 : Float64 = Float64.NEGATIVE_INFINITY
	var maxItem : Item? = null
	var atLeastOneItem : Boolean = false
	forEach { item ->
		val float32 = itemToFloat64(item)
		if (atLeastOneItem) {
			if (float32 > maxFloat64) {
				maxItem = item
				maxFloat64 = float32
			}
		} else {
			atLeastOneItem = true
			maxItem = item
			maxFloat64 = float32
		}
	}
	if (atLeastOneItem) {
		@Suppress("UNCHECKED_CAST")
		return maxItem as Item
	} else {
		throw EmptyException
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.findMaxByFloat64 (itemToFloat64: (Item) -> Float64) : Item {
	try {
		return findMaxByFloat64OrThrowEmpty(itemToFloat64)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
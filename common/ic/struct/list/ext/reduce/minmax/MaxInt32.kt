package ic.struct.list.ext.reduce.minmax


import ic.base.primitives.int32.Int32
import ic.base.throwables.EmptyException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt32OrThrowEmpty (itemToInt32: (Item) -> Int32) : Int32 {
	var maxInt32 : Int32 = Int32.MIN_VALUE
	var atLeastOneItem : Boolean = false
	breakableForEach { item ->
		val int32 = itemToInt32(item)
		if (atLeastOneItem) {
			if (int32 > maxInt32) {
				maxInt32 = int32
			}
		} else {
			atLeastOneItem = true
			maxInt32 = int32
		}
	}
	if (atLeastOneItem) {
		return maxInt32
	} else {
		throw EmptyException
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt32 (itemToInt32: (Item) -> Int32) : Int32 {
	try {
		return maxInt32OrThrowEmpty(itemToInt32)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}


@Throws(EmptyException::class)
inline fun <Item> List<Item>.maxInt32 (
	or : Int32,
	itemToInt32 : (Item) -> Int32
) : Int32 {
	try {
		return maxInt32OrThrowEmpty(itemToInt32)
	} catch (_: EmptyException) {
		return or
	}
}
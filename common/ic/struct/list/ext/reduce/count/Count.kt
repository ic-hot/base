package ic.struct.list.ext.reduce.count


import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


inline fun <Item> List<Item>.count (condition: (Item) -> Boolean) : Int64 {
	var result : Int64 = 0
	forEach { item ->
		if (condition(item)) {
			result++
		}
	}
	return result
}
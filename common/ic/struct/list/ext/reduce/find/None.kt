package ic.struct.list.ext.reduce.find


import ic.struct.list.List


inline fun <Item> List<Item>.none (
	condition : (Item) -> Boolean
) : Boolean {
	return !atLeastOne(condition)
}
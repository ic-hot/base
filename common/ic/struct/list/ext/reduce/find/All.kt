package ic.struct.list.ext.reduce.find


import ic.struct.list.List


inline fun <Item> List<Item>.all (
	condition : (Item) -> Boolean
) : Boolean {
	return none { !condition(it) }
}
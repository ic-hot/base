package ic.struct.list.ext.reduce.find


import ic.base.primitives.int32.Int32
import ic.struct.list.List


inline fun <Item> List<Item>.atLeastOneInRange (

	from : Int32, to : Int32,

	predicate : (Item) -> Boolean

) : Boolean {

	var index : Int = from

	while (index < to) {

		if (predicate(this[index])) return true

		index++

	}; return false

}
package ic.struct.list.ext.reduce.find


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skip
import ic.base.throwables.NotExistsException
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@Throws(NotExistsException::class)
inline fun <Item> List<Item>.findOrThrow (condition: (Item) -> Boolean) : Item {
	forEach { item ->
		if (condition(item)) {
			return item
		}
	}
	throw NotExistsException
}


inline fun <Item> List<Item>.find (condition: (Item) -> Boolean) : Item {
	try {
		return findOrThrow(condition)
	} catch (_: NotExistsException) {
		throw RuntimeException()
	}
}


inline fun <Item> List<Item>.findOrNull (condition: (Item) -> Boolean) : Item? {
	try {
		return findOrThrow(condition)
	} catch (_: NotExistsException) {
		return null
	}
}


@Skippable
inline fun <Item> List<Item>.findOrSkip (condition: (Item) -> Boolean) : Item {
	try {
		return findOrThrow(condition)
	} catch (_: NotExistsException) {
		skip
	}
}


inline fun <Item> List<Item>.find (
	or : () -> Item,
	condition : (Item) -> Boolean
) : Item {
	try {
		return findOrThrow(condition)
	} catch (_: NotExistsException) {
		return or()
	}
}

inline fun <Item> List<Item>.find (
	or : Item,
	condition : (Item) -> Boolean
) : Item {
	try {
		return findOrThrow(condition)
	} catch (_: NotExistsException) {
		return or
	}
}
package ic.struct.list.ext.reduce.find


import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


inline infix fun <Item> List<Item>.atLeastOne (
	condition : (Item) -> Boolean
) : Boolean {
	forEach { item ->
		if (condition(item)) {
			return true
		}
	}
	return false
}

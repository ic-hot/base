package ic.struct.list.ext


import ic.base.arrays.Array
import ic.ifaces.hascount.ext.isEmpty
import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


operator fun
	<Item, Item1: Item, Item2: Item>
	List<Item1>.plus (other: List<Item2>) : List<Item>
{
	if (other.isEmpty) return this
	if (other.isEmpty) return other
	val thisLength = length
	return ListFromArray(
		Array<Any?>(thisLength + other.length) { index ->
			if (index < thisLength) {
				this[index]
			} else {
				other[index - thisLength]
			}
		},
		isArrayImmutable = true
	)
}


operator fun <Item> List<Item>.plus (item: Item) : List<Item> {
	val thisLength = length
	return ListFromArray(
		Array<Any?>(thisLength + 1) { index ->
			if (index < thisLength) {
				this[index]
			} else {
				item
			}
		},
		isArrayImmutable = true
	)
}
package ic.struct.list.ext


import kotlin.jvm.JvmName

import ic.base.escape.skip.Skippable
import ic.base.throwables.IndexOutOfBoundsException
import ic.base.throwables.EmptyException
import ic.struct.list.List


@Deprecated("Use firstOrThrow")
@Throws(EmptyException::class)
inline fun <Item> List<Item>.getFirstOrThrowEmpty() : Item {
	try {
		return getOrThrowIndexOutOfBounds(0)
	} catch (_: IndexOutOfBoundsException) {
		throw EmptyException
	}
}

inline val <Item> List<Item>.firstOrThrow : Item
	// @Throws(EmptyException::class)
	get() {
		try {
			return getOrThrowIndexOutOfBounds(0)
		} catch (_: IndexOutOfBoundsException) {
			throw EmptyException
		}
	}
;


@Deprecated("Use first")
@JvmName("getFirstFun")
inline fun <Item> List<Item>.getFirst() = get(0)

inline val <Item> List<Item>.first get() = get(0)


@Deprecated("Use firstOrNull")
@JvmName("getFirstOrNullFun")
inline fun <Item> List<Item>.getFirstOrNull() = getOrNull(0L)

inline val <Item> List<Item>.firstOrNull get() = getOrNull(0L)

@Skippable
inline fun <Item> List<Item>.getFirstOrSkip() = getOrSkip(0L)
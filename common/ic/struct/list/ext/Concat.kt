package ic.struct.list.ext


import ic.funs.effect.andDoInBetween
import ic.base.primitives.character.Character
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.text.TextBuffer


inline fun <Item> List<Item>.concat (
	separator : String = "",
	crossinline itemToString : (Item) -> String
) : String {
	val textBuffer = TextBuffer()
	forEach(
		{ item : Item ->
			textBuffer.write(itemToString(item))
		}
		.andDoInBetween {
			textBuffer.write(separator)
		}
	)
	return textBuffer.toString()
}


inline fun <Item> List<Item>.concat (
	separator : Character,
	crossinline itemToString : (Item) -> String
) : String {
	return concat(separator = separator.toString(), itemToString = itemToString)
}


fun List<String>.concat (separator: String = "") = concat(separator = separator) { it }

fun List<String>.concat (separator: Character) = concat(separator = separator) { it }
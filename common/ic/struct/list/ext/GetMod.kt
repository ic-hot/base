package ic.struct.list.ext


import ic.base.primitives.int64.Int64
import ic.math.funs.mod
import ic.struct.list.List


fun <Item> List<Item>.getMod (index: Int64) : Item {
	return get(index mod length)
}
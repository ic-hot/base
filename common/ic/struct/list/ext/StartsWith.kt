package ic.struct.list.ext


import ic.base.loop.breakableRepeat
import ic.struct.list.List


infix fun <Item> List<Item>.startsWith (start: List<Item>) : Boolean {
	val startLength = start.length
	if (length < startLength) return false
	breakableRepeat(startLength) { index ->
		if (this[index] != start[index]) return false
	}
	return true
}
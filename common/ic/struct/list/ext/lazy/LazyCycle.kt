package ic.struct.list.ext.lazy


import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.cycle.CycleList


fun <Item> List<Item>.lazyCycle (loopsCount: Int64) : List<Item> {

	return CycleList(this, loopsCount)

}
package ic.struct.list.ext.lazy


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.List
import ic.struct.list.limit.LimitList


fun <Item> List<Item>.lazyLimit (limit: Int64) = run {
	LimitList(sourceList = this, limit = limit)
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> List<Item>.lazyLimit (limit: Int32) = run {
	lazyLimit(limit = limit.asInt64)
}
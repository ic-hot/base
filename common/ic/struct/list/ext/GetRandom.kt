package ic.struct.list.ext


import ic.base.throwables.EmptyException
import ic.math.rand.randomInt64
import ic.struct.list.List


@Throws(EmptyException::class)
fun <Item> List<Item>.getRandomOrThrowEmpty() : Item {

	val length = this.length

	if (length <= 0) {
		throw EmptyException
	}

	return this[
		randomInt64(length)
	]

}
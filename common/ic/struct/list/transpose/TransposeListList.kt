@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")


package ic.struct.list.transpose


import ic.base.objects.ext.isImmutable
import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.list.ext.maxInt64


class TransposeListList<Item> (

	private val sourceListList: List<List<Item>>

) : BaseList<List<Item?>>() {

	override val isListImmutable get() = sourceListList.isImmutable

	override fun implementGet (rowIndex: Int64): List<Item?> {

		return object : BaseList<Item?>() {

			override val length : Int64 get() = sourceListList.length

			override val isListImmutable get() = sourceListList.isImmutable

			override fun implementGet (columnIndex: Int64) : Item? {
				return try {
					sourceListList
					.getOrThrowIndexOutOfBounds(columnIndex)
					.getOrThrowIndexOutOfBounds(rowIndex)
				} catch (indexOutOfBoundsException: IndexOutOfBoundsException) {
					null
				}
			}

		}

	}

	override val length : Int64 get() = maxInt64(or = 0) { it.length }

}
package ic.struct.list


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.throwables.*
import ic.ifaces.hascount.HasCount
import ic.struct.sequence.Sequence


interface List<out Item> : Sequence<Item>, HasCount {


	val length : Int64


	@Throws(IndexOutOfBoundsException::class)
	fun getOrThrowIndexOutOfBounds (index: Int64) : Item


	// These functions are here for Swift interop compatibility:

	operator fun get (index: Int64) : Item {
		try {
			return getOrThrowIndexOutOfBounds(index)
		} catch (_: IndexOutOfBoundsException) {
			throw IndexOutOfBoundsException.Runtime(length = length, index = index)
		}
	}

	operator fun get (index: Int32) : Item {
		return get(index.asInt64)
	}


	companion object


}

package ic.struct.list.fromarray


import ic.base.arrays.Float32Array
import ic.base.arrays.Float64Array
import ic.base.arrays.Int64Array


@Suppress("NOTHING_TO_INLINE")
inline fun ListFromArray (array: Int64Array, isArrayImmutable: Boolean) = ListFromInt64Array(array, isArrayImmutable = isArrayImmutable)


@Suppress("NOTHING_TO_INLINE")
inline fun ListFromArray (array: Float64Array, isArrayImmutable: Boolean) = ListFromFloat64Array(array, isArrayImmutable = isArrayImmutable)


@Suppress("NOTHING_TO_INLINE")
inline fun ListFromArray (array: Float32Array, isArrayImmutable: Boolean) = ListFromFloat32Array(array, isArrayImmutable = isArrayImmutable)
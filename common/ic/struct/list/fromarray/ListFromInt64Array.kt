package ic.struct.list.fromarray


import ic.base.arrays.Int64Array
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.length
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList


class ListFromInt64Array (

	private val array : Int64Array,

	private val isArrayImmutable : Boolean

) : BaseList<Int64>() {

	override val length get() = array.length.asInt64

	override val isListImmutable get() = isImmutable

	override fun implementGet (index: Int64) : Int64 {
		return array[index]
	}

}
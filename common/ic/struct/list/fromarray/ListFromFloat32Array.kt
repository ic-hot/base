package ic.struct.list.fromarray


import ic.base.arrays.Float32Array
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.length
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList


class ListFromFloat32Array (

	private val array : Float32Array,

	private val isArrayImmutable : Boolean

) : BaseList<Float32>() {

	override val isListImmutable get() = isArrayImmutable

	override val length get() = array.length.asInt64

	override fun implementGet (index: Int64) : Float32 {
		return array[index]
	}

}
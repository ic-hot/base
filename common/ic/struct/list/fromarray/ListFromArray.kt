package ic.struct.list.fromarray


import ic.base.arrays.ext.length
import ic.base.arrays.ext.get.get
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList


class ListFromArray<Item> (

	private val array : Array<*>,

	private val isArrayImmutable : Boolean

) : BaseList<Item>() {

	override val isListImmutable get() = isArrayImmutable

	override val length get() = array.length.asInt64

	override fun implementGet (index: Int64) : Item {
		@Suppress("UNCHECKED_CAST")
		return array[index] as Item
	}

}
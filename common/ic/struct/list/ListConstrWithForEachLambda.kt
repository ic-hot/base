package ic.struct.list


import ic.struct.list.editable.EditableList


inline fun <Item> List (forEach: ((Item) -> Unit) -> Unit) : List<Item> {
	val list = EditableList<Item>()
	forEach { item ->
		list.add(item)
	}
	list.freeze()
	return list
}
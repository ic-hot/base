package ic.struct.list


import ic.struct.list.fromklist.ListFromKotlinList


fun <Item> List<Item>.copyRemoveLast() : List<Item> {
	val kotlinList = ArrayList<Item>()
	for (i in 0 until length - 1) {
		kotlinList.add(this[i])
	}
	return ListFromKotlinList(kotlinList, isKotlinListImmutable = true)
}
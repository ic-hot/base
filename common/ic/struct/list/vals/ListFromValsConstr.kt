package ic.struct.list.vals


import ic.struct.list.List
import ic.struct.list.ext.reduce.find.atLeastOne
import ic.struct.value.Val
import ic.struct.value.skip.Skippable


inline fun <Item: Any> ListFromVals (

	vals : List<Val<out Item>>,

	isItemsSkippable : Boolean = vals.atLeastOne { it is Skippable },

	crossinline initItemInBetween : () -> Item

) : ListFromVals<Item> {

	return object : ListFromVals<Item>() {

		override val vals get() = vals

		override val isItemsSkippable get() = isItemsSkippable

		override val hasItemsInBetween get() = true

		override fun initItemInBetween() = initItemInBetween()

		override val isListImmutable get() = false

	}

}
package ic.struct.list.vals


import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.list.ext.reduce.count.count
import ic.struct.value.Val
import ic.util.cache.RamCache


abstract class ListFromVals<Item: Any> : BaseList<Item>() {


	protected abstract val vals : List<Val<out Item>>

	protected abstract val isItemsSkippable : Boolean

	protected abstract val hasItemsInBetween : Boolean

	protected abstract fun initItemInBetween() : Item


	override val length : Int64 get() {
		var length : Int64 = (
			if (isItemsSkippable) {
				vals.count { v ->
					skippable(
						onSkip = { false }
					) {
						v.value
						true
					}
				}
			} else {
				vals.length
			}
		)
		if (hasItemsInBetween) {
			if (length > 1) {
				length += (length - 1)
			}
		}
		return length
	}


	private val itemsInBetweenCache = RamCache<Int64, Item>()


	override fun implementGet (index: Int64) : Item {
		val itemIndex : Int64 = (
			if (hasItemsInBetween) {
				if (index % 2 == 0L) {
					index / 2
				} else {
					val inBetweenIndex = index / 2
					return itemsInBetweenCache.getOr(key = inBetweenIndex) {
						initItemInBetween()
					}
				}
			} else {
				index
			}
		)
		if (isItemsSkippable) {
			var itemI : Int64 = 0
			var valI : Int64 = 0
			var item : Item? = null
			while(itemI <= itemIndex) {
				skippable {
					item = vals[valI].value
					itemI++
				}
				valI++
			}
			return item as Item
		} else {
			return vals[itemIndex].value
		}
	}


}
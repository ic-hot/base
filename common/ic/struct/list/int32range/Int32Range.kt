package ic.struct.list.int32range


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.BaseList


abstract class Int32Range : BaseList<Int>() {

	protected abstract val from : Int32
	protected abstract val to   : Int32

	protected abstract val isRangeImmutable : Boolean

	override val isListImmutable get() = isRangeImmutable

	override val length: Int64 get() = (to - from).asInt64

	override fun implementGet (index: Int64) : Int {
		return (from + index).asInt32
	}

}
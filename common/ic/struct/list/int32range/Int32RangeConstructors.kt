@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.list.int32range


import ic.base.primitives.int32.Int32


inline fun Int32Range (length: Int32) : Int32Range {

	return object : Int32Range() {

		override val from 	get() = 0
		override val to 	get() = length

		override val isRangeImmutable get() = true

	}

}
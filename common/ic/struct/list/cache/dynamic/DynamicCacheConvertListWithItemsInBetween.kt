package ic.struct.list.cache.dynamic


import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.map.editable.EditableMap


abstract class DynamicCacheConvertListWithItemsInBetween
	<Item: Any, SourceItem>
	: BaseList<Item>()
{


	protected abstract val sourceList : List<SourceItem>

	protected abstract fun convertItem (getSourceItem : () -> SourceItem) : Item

	protected abstract fun initItemInBetween() : Item


	private inner class ItemHolder (
		var sourceItem : SourceItem
	) : () -> SourceItem {

		override fun invoke() = sourceItem

		val item = convertItem(this)

	}

	private val cache = EditableMap<SourceItem, ItemHolder>()

	private val itemsInBetweenCache = EditableMap<Int64, Item>()


	override val length : Int64 get()  {
		val sourceLength = sourceList.length
		if (sourceLength == 0L) {
			return 0L
		} else {
			return sourceLength + sourceLength - 1
		}
	}

	override fun implementGet (index: Int64) : Item {

		if (index % 2 == 0L) {

			val sourceIndex = index / 2
			val sourceItem = sourceList[sourceIndex]
			val itemHolder = run {
				cache[sourceItem]
				?.also { it.sourceItem = sourceItem }
				?: ItemHolder(sourceItem)
				.also { cache[sourceItem] = it }
			}
			return itemHolder.item

		} else {

			val inBetweenIndex = index / 2

			return (
				itemsInBetweenCache[inBetweenIndex]
				?: initItemInBetween()
				.also { itemsInBetweenCache[inBetweenIndex] = it }
			)

		}


	}


	final override val isListImmutable get() = false


}
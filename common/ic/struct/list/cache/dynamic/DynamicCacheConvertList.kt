package ic.struct.list.cache.dynamic


import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.map.editable.EditableMap


abstract class DynamicCacheConvertList<Item, SourceItem> : BaseList<Item>() {


	protected abstract val sourceList : List<SourceItem>


	protected abstract fun ItemHolder<Item, SourceItem>.convertItem () : Item


	class ItemHolder<Item, SourceItem>
		internal constructor (
			list : DynamicCacheConvertList<Item, SourceItem>,
			sourceItem : SourceItem
		)
	{

		var sourceItem : SourceItem = sourceItem
			internal set
		;

		val item = list.run {
			convertItem()
		}

	}

	private val cache = EditableMap<SourceItem, ItemHolder<Item, SourceItem>>()


	override val length get() = sourceList.length

	override fun implementGet (index: Int64) : Item {
		val sourceItem = sourceList[index]
		val itemHolder = run {
			cache[sourceItem]
			?.also { it.sourceItem = sourceItem }
			?: ItemHolder(list = this, sourceItem = sourceItem)
			.also { cache[sourceItem] = it }
		}
		return itemHolder.item
	}


	final override val isListImmutable get() = false


}
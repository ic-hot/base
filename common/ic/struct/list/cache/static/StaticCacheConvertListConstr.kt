package ic.struct.list.cache.static


import ic.struct.list.List


inline fun <Item: Any, SourceItem> CacheConvertList (

	crossinline getSourceList : () -> List<SourceItem>,

	crossinline convertItem : (SourceItem) -> Item

) : StaticCacheConvertList<Item, SourceItem> {

	return object : StaticCacheConvertList<Item, SourceItem>() {

		override val isSourceListImmutable get() = false

		override val sourceList get() = getSourceList()

		override fun convertItem (sourceItem: SourceItem) = convertItem(sourceItem)

	}

}


inline fun <Item: Any, SourceItem> CacheConvertList (

	sourceList : List<SourceItem>,

	crossinline convertItem : (SourceItem) -> Item

) : StaticCacheConvertList<Item, SourceItem> {

	return object : StaticCacheConvertList<Item, SourceItem>() {

		override val isSourceListImmutable get() = true

		override val sourceList get() = sourceList

		override fun convertItem (sourceItem: SourceItem) = convertItem(sourceItem)

	}

}


inline fun <Item: Any, SourceItem> CacheConvertList (

	sourceList : List<SourceItem>,

	crossinline initItemInBetween : () -> Item,

	crossinline convertItem : (SourceItem) -> Item

) : StaticCacheConvertListWithItemsInBetween<Item, SourceItem> {

	return object : StaticCacheConvertListWithItemsInBetween<Item, SourceItem>() {

		override val isListImmutable get() = true

		override val sourceList get() = sourceList

		override fun initItemInBetween() = initItemInBetween()

		override fun convertItem (sourceItem: SourceItem) = convertItem(sourceItem)

	}

}


@Suppress("FunctionName")
inline fun <Item: Any, SourceItem> CacheConvertList (

	crossinline getSourceList : () -> List<SourceItem>,

	crossinline initItemInBetween : () -> Item,

	crossinline convertItem : (SourceItem) -> Item

) : StaticCacheConvertListWithItemsInBetween<Item, SourceItem> {

	return object : StaticCacheConvertListWithItemsInBetween<Item, SourceItem>() {

		override val isListImmutable get() = false

		override val sourceList get() = getSourceList()

		override fun initItemInBetween() = initItemInBetween()

		override fun convertItem (sourceItem: SourceItem) = convertItem(sourceItem)

	}

}
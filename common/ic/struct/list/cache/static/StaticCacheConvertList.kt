package ic.struct.list.cache.static


import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.map.editable.EditableMap


abstract class StaticCacheConvertList<Item: Any, SourceItem> : BaseList<Item>() {


	protected abstract val sourceList : List<SourceItem>

	protected abstract val isSourceListImmutable : Boolean

	protected abstract fun convertItem (sourceItem: SourceItem) : Item


	private val cache = EditableMap<SourceItem, Item>()


	override val length get() = sourceList.length

	override fun implementGet (index: Int64) : Item {
		val sourceItem = sourceList[index]
		return cache[sourceItem] ?: convertItem(sourceItem).also { cache[sourceItem] = it }
	}


	final override val isListImmutable get() = isSourceListImmutable


}
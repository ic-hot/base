package ic.struct.list.cached


import ic.base.primitives.int64.Int64


inline fun <Item: Any> CachedList (

	crossinline getItemsCount : () -> Int64,

	crossinline initItemInBetween : () -> Item,

	crossinline initItem : (Int64) -> Item

) : CachedList<Item> {

	return object : CachedList<Item>(
		hasItemsInBetween = true
	) {

		override val itemsCount get() = getItemsCount()

		override val isItemsCountImmutable get() = false

		override fun initItem (index: Int64) : Item {
			return initItem(index)
		}

		override fun initItemInBetween() : Item {
			return initItemInBetween()
		}

	}

}
package ic.struct.list.cached


import ic.base.arrays.Array
import ic.base.arrays.ext.copy
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.length
import ic.base.arrays.ext.set
import ic.base.assert.assert
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList


abstract class CachedList<Item: Any> (

	private val hasItemsInBetween : Boolean

) : BaseList<Item>() {


	protected abstract val itemsCount : Int64

	protected abstract val isItemsCountImmutable : Boolean


	protected abstract fun initItem (index: Int64) : Item

	protected abstract fun initItemInBetween() : Item

	override val isListImmutable get() = isItemsCountImmutable


	override val length : Int64 get() {
		val itemsCount = this.itemsCount
		assert { itemsCount >= 0 }
		if (hasItemsInBetween) {
			if (itemsCount < 2) {
				return itemsCount
			} else {
				return itemsCount + itemsCount - 1
			}
		} else {
			return itemsCount
		}
	}


	private var items : Array<Any?>? = null

	private var itemsInBetween : Array<Any?>? = null

	override fun implementGet (index: Int64) : Item {
		if (hasItemsInBetween) {
			if (index % 2 == 0L) {
				return getItem(index / 2)
			} else {
				return getItemInBetween(index / 2)
			}
		} else {
			return getItem(index)
		}
	}

	private fun getItem (index: Int64) : Item {
		val itemsCount = this.itemsCount
		if (items == null) {
			items = Array(length = itemsCount)
		} else {
			if (items!!.length < itemsCount) {
				items = items!!.copy(length = itemsCount)
			}
		}
		val item = items!![index] ?: initItem(index).also { items!![index] = it }
		@Suppress("UNCHECKED_CAST")
		return item as Item
	}

	private fun getItemInBetween (index: Int64) : Item {
		val itemsCount = this.itemsCount
		if (itemsInBetween == null) {
			itemsInBetween = Array(length = itemsCount)
		} else {
			if (itemsInBetween!!.length < itemsCount) {
				itemsInBetween = itemsInBetween!!.copy(length = itemsCount)
			}
		}
		val item = (
			itemsInBetween!![index]
			?: initItemInBetween().also { itemsInBetween!![index] = it }
		)
		@Suppress("UNCHECKED_CAST")
		return item as Item
	}


}
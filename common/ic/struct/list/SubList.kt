package ic.struct.list


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


class SubList<Item>(

	private val sourceList : List<Item>,

	private val start : Int64,

	private val end : Int64

) : BaseList<Item>() {


	override val isListImmutable get() = false


	override val length get() = end - start


	override fun implementGet(index: Int64): Item {
		return sourceList[start + index]
	}


	constructor(
		sourceList: List<Item>,
		start : Int32,
		end : Int32
	) : this (
		sourceList = sourceList,
		start = start.asInt64,
		end = end.asInt64
	)


}

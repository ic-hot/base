@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.list.join


import ic.base.arrays.ext.asList
import ic.struct.list.List


inline fun <Item> JoinList (
	crossinline getChildren : () -> List<List<Item>?>
) : JoinList<Item> {
	return object : JoinList<Item>() {
		override val children get() = getChildren()
		override val isChildrenImmutable get() = false
	}
}


inline fun <Item> JoinList (
	children : List<List<Item>?>
) : JoinList<Item> {
	return object : JoinList<Item>() {
		override val children get() = children
		override val isChildrenImmutable get() = true
	}
}


inline fun <Item> JoinList (
	vararg children : List<Item>?
) : JoinList<Item> {
	return object : JoinList<Item>() {
		override val children = children.asList(isArrayImmutable = true)
		override val isChildrenImmutable get() = true
	}
}
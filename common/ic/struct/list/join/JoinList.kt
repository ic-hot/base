@file:Suppress("NAME_SHADOWING")


package ic.struct.list.join


import ic.math.annotations.Positive
import ic.base.annotations.Valid
import ic.base.objects.ext.isImmutable
import ic.base.primitives.int64.Int64
import ic.base.throwables.InconsistentDataException
import ic.base.throwables.IndexOutOfBoundsException
import ic.struct.list.BaseList
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


abstract class JoinList<Item> : BaseList<Item>() {


	protected abstract val children : List<List<Item>?>

	protected abstract val isChildrenImmutable : Boolean

	override val isListImmutable get() = isChildrenImmutable && children.isImmutable


	override val length : Int64 get() {
		var length : Int64 = 0
		children.forEach { child ->
			if (child != null) {
				length += child.length
			}
		}
		return length
	}


	override fun implementGet (@Valid @Positive index: Int64) : Item {
		var index = index
		children.forEach { child ->
			if (child != null) {
				try {
					return child.getOrThrowIndexOutOfBounds(index)
				} catch (indexOutOfBoundsException: IndexOutOfBoundsException) {
					index -= child.length
				}
			}
		}
		throw InconsistentDataException.Error()
	}


}

package ic.struct.list.limit


import ic.base.primitives.int64.Int64
import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> LimitList (
	sourceList : List<Item>,
	limit : Int64
) : LimitList<Item> {
	return object : LimitList<Item>() {
		override val sourceList get() = sourceList
		override val isSourceListImmutable get() = true
		override val limit get() = limit
		override val isLimitImmutable get() = true
	}
}
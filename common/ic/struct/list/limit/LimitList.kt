package ic.struct.list.limit


import kotlin.math.min

import ic.base.objects.ext.isImmutable
import ic.base.primitives.int64.Int64
import ic.struct.list.BaseList
import ic.struct.list.List


abstract class LimitList <Item> : BaseList<Item>() {


	protected abstract val sourceList : List<Item>

	protected abstract val isSourceListImmutable : Boolean

	protected abstract val limit : Int64

	protected abstract val isLimitImmutable : Boolean


	override val length get() = min(sourceList.length, limit)

	override fun implementGet (index: Int64) : Item {
		return sourceList[index]
	}


	override val isListImmutable get() = run {
		isLimitImmutable && isSourceListImmutable && sourceList.isImmutable
	}


}


import "ll.bool.Branch"
import "ll.fun.Function"
import "ll.fun.Invoke"
import "ll.math.Compare"
import "ll.math.Sum"

import "ic.struct.list.List"


addToList function { List list, NewItem

	Result list {

		Length + [ List.Length, 1 ]

		item { ThisIndex Index
			Result branch [
				< [ Index, List.Length ] -> invoke List.item { Index ThisIndex }
				Else -> NewItem
			]
		}

	}

}


import "ll.Void"

void {

	import "ll.compiletime.Assert"

	void { // Test add to empty list:
		EmptyList list { Length 0 }
		NewList invoke addToList { List EmptyList, NewItem "A" }
		assert = [ NewList.Length, 1 ]
		assert = [
			invoke NewList.item { Index 0 },
			"A"
		]
	}

}
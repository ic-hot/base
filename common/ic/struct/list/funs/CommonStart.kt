package ic.struct.list.funs


import ic.base.escape.breakable.Break
import ic.base.loop.breakableRepeat
import ic.base.primitives.int32.Int32
import ic.math.funs.min
import ic.struct.list.List
import ic.struct.list.editable.EditableList


fun
	<Item, AItem: Item, BItem: Item>
	commonStart (a: List<AItem>, b: List<BItem>) : List<Item>
{
	val commonStart = EditableList<Item>()
	breakableRepeat(
		times = min(a.length, b.length)
	) { index ->
		val aItem = a[index]
		val bItem = b[index]
		if (aItem != bItem) Break()
		commonStart.add(aItem)
	}
	return commonStart
}


fun commonStartLength (a: List<*>, b: List<*>) : Int32 {
	var commonStartLength = 0
	breakableRepeat(
		times = min(a.length, b.length)
	) { index ->
		val aItem = a[index]
		val bItem = b[index]
		if (aItem != bItem) Break()
		commonStartLength++
	}
	return commonStartLength
}
package ic.struct.list.funs


import ic.base.escape.breakable.Break
import ic.base.loop.breakableRepeat
import ic.base.primitives.int32.Int32
import ic.math.funs.min
import ic.struct.list.List


fun commonEndLength (a: List<*>, b: List<*>) : Int32 {
	val aLength = a.length
	val bLength = b.length
	var commonEndLength = 0
	breakableRepeat(
		times = min(aLength, bLength)
	) { index ->
		val aItem = a[aLength - 1 - index]
		val bItem = b[bLength - 1 - index]
		if (aItem != bItem) Break()
		commonEndLength++
	}
	return commonEndLength
}
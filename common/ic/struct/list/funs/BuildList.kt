package ic.struct.list.funs


import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item> buildList (action: EditableList<Item>.() -> Unit) : List<Item> {
	val list = EditableList<Item>()
	list.action()
	list.freeze()
	return list
}
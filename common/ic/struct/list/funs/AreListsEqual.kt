package ic.struct.list.funs


import ic.base.loop.repeat
import ic.struct.list.List


fun areListsEqual (list1: List<*>, list2: List<*>) : Boolean {
	if (list1 === list2) return true
	val length = list1.length
	if (list2.length != length) return false
	repeat(length) { index ->
		if (list1[index] != list2[index]) return false
	}
	return true
}
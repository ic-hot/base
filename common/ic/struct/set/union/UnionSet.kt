package ic.struct.set.union


import ic.struct.collection.Collection
import ic.funs.effector.find.atLeastOne
import ic.struct.set.Set


abstract class UnionSet<Item> : Set<Item> {


	protected abstract val sourceSets : Collection<Set<Item>>


	override fun contains (item: Item) : Boolean {

		return sourceSets.atLeastOne { it.contains(item) }

	}


}
package ic.struct.set.test


import ic.struct.list.List
import ic.struct.set.editable.test.EditableSetTests
import ic.test.TestSuite


internal class SetTests : TestSuite() {

	override fun initTests() = List(

		EditableSetTests()

	)

}
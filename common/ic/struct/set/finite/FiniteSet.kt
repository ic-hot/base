package ic.struct.set.finite


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.Set
import ic.struct.set.editable.EditableSet


interface FiniteSet<Item> : Set<Item>, Collection<Item> {


	companion object


}

@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.set.finite.subsets


import ic.struct.set.finite.FiniteSet


inline fun <Item> Subsets (
	crossinline getFullSet : () -> FiniteSet<Item>,
	crossinline includesEmptySet : () -> Boolean,
	crossinline includesFullSet : () -> Boolean
) : Subsets<Item> {
	return object : Subsets<Item>() {
		override val fullSet get() = getFullSet()
		override val includesEmptySet get() = includesEmptySet()
		override val includesFullSet get() = includesFullSet()
	}
}


inline fun <Item> Subsets (
	fullSet : FiniteSet<Item>,
	includesEmptySet : Boolean,
	includesFullSet : Boolean
) = Subsets(
	getFullSet = { fullSet },
	includesEmptySet = { includesEmptySet },
	includesFullSet = { includesFullSet }
)
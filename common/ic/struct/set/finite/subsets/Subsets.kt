package ic.struct.set.finite.subsets


import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.funs.effector.find.all
import ic.ifaces.action.action1.Action1
import ic.ifaces.hascount.HasCount
import ic.math.funs.pow.pow
import ic.struct.collection.ext.copy.copyToList
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.count.isEmpty
import ic.struct.list.ext.foreach.forEachIndexed
import ic.struct.set.finite.FiniteSet
import ic.struct.set.finite.BaseFiniteSet
import ic.struct.set.editable.EditableSet


abstract class Subsets<Item> : BaseFiniteSet<FiniteSet<Item>>(), HasCount {


	protected abstract val fullSet : FiniteSet<Item>

	protected abstract val includesEmptySet : Boolean
	protected abstract val includesFullSet  : Boolean


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun contains (subset: FiniteSet<Item>) : Boolean {
		val fullSet = this.fullSet
		if (!includesEmptySet) {
			if (subset.isEmpty) return false
		}
		if (!includesFullSet) {
			if (subset == fullSet) return false
		}
		return subset.all { it in fullSet }
	}


	override val count : Int64 get() {
		val fullSetSize = fullSet.count
		if (fullSetSize < 1) return 0
		var count = 2 pow fullSetSize
		if (!includesEmptySet) count--
		if (!includesFullSet) count--
		return count
	}


	override fun invoke (action: (FiniteSet<Item>) -> Unit) {
		val items = fullSet.copyToList()
		val from = if (includesEmptySet) 0 else 1
		var to = 2 pow items.length
		if (!includesFullSet) to--
		var subsetIndex = from
		while (subsetIndex < to) {
			val subset = EditableSet<Item>()
			items.forEachIndexed { itemIndex, item ->
				val toInclude = ((subsetIndex shr itemIndex.asInt32) and 1) != 0
				if (toInclude) subset.add(item)
			}
			action(subset)
			subsetIndex++
		}
	}


}
package ic.struct.set.finite


import ic.struct.collection.ext.foreach.forEach


abstract class SubtractFiniteSet<Item> : BaseFiniteSet<Item>() {


	protected abstract val a : FiniteSet<Item>
	protected abstract val b : FiniteSet<Item>


	override fun invoke (action: (Item) -> Unit) {
		a.forEach {
			if (!b.contains(it)) action(it)
		}
	}


	override fun contains (item: Item) : Boolean {
		return a.contains(item) && !b.contains(item)
	}


}
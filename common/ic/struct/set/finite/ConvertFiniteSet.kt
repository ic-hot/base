package ic.struct.set.finite


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.collection.ext.foreach.forEach


abstract class ConvertFiniteSet<Item, SourceItem> : BaseFiniteSet<Item>() {


	protected abstract val sourceFiniteSet : FiniteSet<SourceItem>


	protected abstract @Skippable fun convertItem (sourceItem: SourceItem) : Item

	protected abstract @Skippable fun convertItemBack (item: Item) : SourceItem


	override fun contains (item: Item) : Boolean {
		skippable {
			return sourceFiniteSet.contains(
				convertItemBack(item)
			)
		}
		return false
	}

	override fun invoke (action: (Item) -> Unit) {
		sourceFiniteSet.forEach { sourceItem ->
			skippable {
				action(
					convertItem(sourceItem)
				)
			}
		}
	}


}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.set.finite.union


import ic.struct.collection.Collection
import ic.struct.list.fromarray.ListFromArray
import ic.struct.set.finite.FiniteSet


inline fun <Item> UnionFiniteSet (
	crossinline getChildren : () -> Collection<FiniteSet<Item>>
) : UnionFiniteSet<Item> {
	return object : UnionFiniteSet<Item>() {
		override val children get() = getChildren()
	}
}


inline fun <Item> UnionFiniteSet (
	children : Collection<FiniteSet<Item>>
) = UnionFiniteSet(
	getChildren = { children }
)


inline fun <Item> UnionFiniteSet (vararg children : FiniteSet<Item>) : UnionFiniteSet<Item> {
	return UnionFiniteSet(
		ListFromArray(children, isArrayImmutable = true)
	)
}
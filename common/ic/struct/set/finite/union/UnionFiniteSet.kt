package ic.struct.set.finite.union


import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.toArray
import ic.struct.collection.ext.foreach.forEach
import ic.funs.effector.find.atLeastOne
import ic.struct.set.finite.BaseFiniteSet
import ic.struct.set.finite.FiniteSet


abstract class UnionFiniteSet<Item> : BaseFiniteSet<Item>() {


	protected abstract val children : Collection<FiniteSet<Item>>


	override fun invoke (action: (Item) -> Unit) {
		val childrenArray = children.toArray()
		childrenArray.forEachIndexed { index, child ->
			child.forEach childItemsIteration@{
				for (i in 0 until index) {
					if (childrenArray[i].contains(it)) return@childItemsIteration
				}
				action(it)
			}
		}
	}


	override fun contains (item: Item) = children.atLeastOne { it.contains(item) }


}
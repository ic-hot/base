package ic.struct.set.finite.removeone


import ic.struct.collection.ext.foreach.forEach
import ic.struct.set.finite.BaseFiniteSet
import ic.struct.set.finite.FiniteSet


abstract class RemoveOneFiniteSet<Item> : BaseFiniteSet<Item>() {


	protected abstract val sourceFiniteSet : FiniteSet<Item>

	protected abstract val itemToRemove : Item


	override fun invoke (action: (Item) -> Unit) {

		sourceFiniteSet.forEach { item ->

			if (item != itemToRemove) {
				action(item)
			}

		}

	}


	override fun contains (item: Item): Boolean {

		if (item == itemToRemove) return false

		return sourceFiniteSet.contains(item)

	}


}
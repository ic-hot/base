@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.set.finite.removeone


import ic.struct.set.finite.FiniteSet


inline fun <Item> RemoveOneFiniteSet (

	sourceFiniteSet : FiniteSet<Item>,

	itemToRemove : Item

) : RemoveOneFiniteSet<Item> {

	return object : RemoveOneFiniteSet<Item>() {

		override val sourceFiniteSet get() = sourceFiniteSet

		override val itemToRemove get() = itemToRemove

	}

}
package ic.struct.set.finite.ext.copy


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


inline fun <Item> FiniteSet<Item>.copyFilter (

	crossinline condition : (Item) -> Boolean

) : FiniteSet<Item> {

	val result = EditableSet<Item>()

	breakableForEach { item ->
		if (condition(item)) {
			result.add(item)
		}
	}

	return result

}
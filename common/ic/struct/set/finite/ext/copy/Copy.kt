package ic.struct.set.finite.ext.copy


import ic.struct.collection.ext.foreach.forEach
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


fun <Item> FiniteSet<Item>.copy() : FiniteSet<Item> {

	val result = EditableSet<Item>()

	forEach { item ->
		result.add(item)
	}

	return result

}
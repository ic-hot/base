package ic.struct.set.finite.ext


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


operator fun <Item> FiniteSet<Item>.plus (other: Collection<Item>?) : FiniteSet<Item> {

	if (other == null) return this

	val unionEditableSet = EditableSet<Item>()

	this.breakableForEach { item ->
		unionEditableSet.addIfNotExists(item)
	}

	other.breakableForEach { item ->
		unionEditableSet.addIfNotExists(item)
	}

	return unionEditableSet

}


operator fun <Item> FiniteSet<Item>.plus (item: Item) : FiniteSet<Item> {

	val unionEditableSet = EditableSet<Item>()

	this.breakableForEach {
		unionEditableSet.addIfNotExists(it)
	}

	unionEditableSet.addIfNotExists(item)

	return unionEditableSet

}
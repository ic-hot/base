package ic.struct.set.finite.ext


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


fun <Item> FiniteSet<Item>.copyFlip (item: Item) : FiniteSet<Item> {

	val editableSet = EditableSet<Item>()

	breakableForEach { editableSet.add(it) }

	if (this.contains(item)) {
		editableSet.removeIfExists(item)
	} else {
		editableSet.addIfNotExists(item)
	}

	return editableSet

}
package ic.struct.set.finite.ext


import ic.base.throwables.NotExistsException
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


@Throws(NotExistsException::class)
fun <Item> FiniteSet<Item>.copyRemoveOrThrowNotExists (item: Item) : FiniteSet<Item> {

	val editableSet = EditableSet<Item>()

	breakableForEach { editableSet.add(it) }

	editableSet.removeOrThrowNotExists(item)

	return editableSet

}


fun <Item> FiniteSet<Item>.copyRemoveIfExists (item: Item) : FiniteSet<Item> {

	val editableSet = EditableSet<Item>()

	breakableForEach { editableSet.add(it) }

	editableSet.removeIfExists(item)

	return editableSet

}
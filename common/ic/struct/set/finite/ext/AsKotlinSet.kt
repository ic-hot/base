package ic.struct.set.finite.ext


import ic.base.primitives.int64.ext.asInt32
import ic.struct.collection.ext.copy.copyToKotlinList
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.count.isEmpty
import ic.struct.set.finite.FiniteSet


inline val <Item> FiniteSet<Item>.asKotlinSet : kotlin.collections.Set<Item> get() {

	val thisFiniteSet = this

	return object : kotlin.collections.Set<Item> {
		override val size get() = thisFiniteSet.count.asInt32
		override fun isEmpty() = thisFiniteSet.isEmpty
		override fun contains (element: Item) = thisFiniteSet.contains(element)
		override fun containsAll (elements: kotlin.collections.Collection<Item>) : Boolean {
			for (element in elements) {
				if (!thisFiniteSet.contains(element)) return false
			}; return true
		}
		override fun iterator() = thisFiniteSet.copyToKotlinList().iterator()
	}

}
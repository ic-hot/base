package ic.struct.set.finite.ext


import ic.struct.set.finite.FiniteSet
import ic.struct.set.finite.empty.EmptyFiniteSet


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> FiniteSet.Companion.Empty() : FiniteSet<Item> {

	@Suppress("UNCHECKED_CAST")
	return EmptyFiniteSet as FiniteSet<Item>

}
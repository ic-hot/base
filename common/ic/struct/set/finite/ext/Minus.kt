package ic.struct.set.finite.ext


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


operator fun <Item> FiniteSet<Item>.minus (other: FiniteSet<Item>?) : FiniteSet<Item> {

	if (other == null) return this

	val result = EditableSet<Item>()

	breakableForEach { item ->
		if (item !in other) {
			result.add(item)
		}
	}

	return result

}
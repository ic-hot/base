package ic.struct.set.finite.ext


import ic.struct.collection.ext.count.isEmpty
import ic.struct.set.finite.FiniteSet


inline val <Item> FiniteSet<Item>.orNullIfEmpty get() = if (isEmpty) null else this
package ic.struct.set.finite.ext


import ic.struct.set.finite.FiniteSet
import ic.struct.set.finite.subsets.Subsets


inline val <Item> FiniteSet<Item>.exclusiveNonEmptySubsets : FiniteSet<FiniteSet<Item>> get() {
	return Subsets(fullSet = this, includesEmptySet = false, includesFullSet = false)
}

inline val <Item> FiniteSet<Item>.exclusiveSubsets : FiniteSet<FiniteSet<Item>> get() {
	return Subsets(fullSet = this, includesEmptySet = true, includesFullSet = false)
}
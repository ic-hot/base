package ic.struct.set.finite.ext


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


fun <Item> FiniteSet<Item>.copyAddOrThrowAlreadyExists (item: Item) : FiniteSet<Item> {
	val editableSet = EditableSet<Item>()
	breakableForEach { editableSet.add(it) }
	editableSet.addOrThrowAlreadyExists(item)
	return editableSet
}


fun <Item> FiniteSet<Item>.copyAddIfNotExists (item: Item) : FiniteSet<Item> {
	val editableSet = EditableSet<Item>()
	breakableForEach { editableSet.add(it) }
	editableSet.addIfNotExists(item)
	return editableSet
}
package ic.struct.set.finite.empty


import ic.ifaces.hascount.HasCount
import ic.struct.set.finite.BaseFiniteSet


object EmptyFiniteSet : BaseFiniteSet<Any?>(), HasCount {

	override val count get() = 0L

	override fun contains (item: Any?) = false

	override fun invoke (action: (Any?) -> Unit) {}

}
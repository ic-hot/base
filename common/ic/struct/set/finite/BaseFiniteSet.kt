package ic.struct.set.finite


import ic.struct.collection.BaseCollection
import ic.struct.collection.ext.reduce.sum.sumByInt32
import ic.struct.set.finite.funs.areFiniteSetsEqual


abstract class BaseFiniteSet<Item> : BaseCollection<Item>(), FiniteSet<Item> {


	override fun hashCode() : Int {
		return sumByInt32 { it.hashCode() }
	}

	override fun equals (other: Any?): Boolean {
		if (other is FiniteSet<*>) {
			return areFiniteSetsEqual(this, other)
		} else {
			return false
		}
	}


}
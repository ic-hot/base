package ic.struct.set.finite


import ic.struct.collection.ext.foreach.forEach


abstract class FilterFiniteSet<Item> : BaseFiniteSet<Item>() {


	protected abstract val sourceFiniteSet : FiniteSet<Item>

	protected abstract fun filter (item: Item ): Boolean


	override fun invoke (action: (Item) -> Unit) {
		sourceFiniteSet.forEach {
			if (filter(it)) action(it)
		}
	}

	override fun contains (item: Item) : Boolean {
		return if (filter(item)) sourceFiniteSet.contains(item) else false
	}


}
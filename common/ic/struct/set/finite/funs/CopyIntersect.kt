package ic.struct.set.finite.funs


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


fun <Item> copyIntersect (a: FiniteSet<Item>?, b: FiniteSet<Item>?) : FiniteSet<Item> {

	if (a == null) return FiniteSet()
	if (b == null) return FiniteSet()

	val intersection = EditableSet<Item>()

	a.breakableForEach { item ->
		if (item !in b) return@breakableForEach
		intersection.add(item)
	}

	return intersection

}
package ic.struct.set.finite.funs


import ic.funs.effector.find.all
import ic.struct.set.finite.FiniteSet


fun areFiniteSetsEqual (a: FiniteSet<*>, b: FiniteSet<*>) : Boolean {

	@Suppress("UNCHECKED_CAST")
	a as FiniteSet<Any?>

	@Suppress("UNCHECKED_CAST")
	b as FiniteSet<Any?>

	return a.all { it in b } && b.all { it in a }

}
package ic.struct.set.finite.fromkset


import ic.struct.set.finite.BaseFiniteSet


abstract class FiniteSetFromKotlinSet<Item> : BaseFiniteSet<Item>() {


	abstract val kotlinSet : kotlin.collections.Set<Item>


	override fun invoke (action: (Item) -> Unit) = kotlinSet.forEach(action)

	override fun contains (item: Item) = kotlinSet.contains(item)


}
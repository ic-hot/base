@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.set.finite.fromkset


inline fun <Item> FiniteSetFromKotlinSet (

	kotlinSet : kotlin.collections.Set<Item>

) : FiniteSetFromKotlinSet<Item> {

	return object : FiniteSetFromKotlinSet<Item>() {

		override val kotlinSet get() = kotlinSet

	}

}
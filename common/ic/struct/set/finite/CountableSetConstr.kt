@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.set.finite


import ic.base.escape.skip.skippable
import ic.struct.set.finite.empty.EmptyFiniteSet
import ic.struct.set.editable.EditableSet


inline fun <Item> FiniteSet() : FiniteSet<Item> {
	@Suppress("UNCHECKED_CAST")
	return EmptyFiniteSet as FiniteSet<Item>
}

inline fun <Item> FiniteSet (vararg items: Item) : FiniteSet<Item> = EditableSet(*items)

inline fun <Item> FiniteSet (items: Iterable<Item>) : FiniteSet<Item> = EditableSet(items)

inline fun <Item> FiniteSet (getItem1 : () -> Item) : FiniteSet<Item> {
	val editableSet = EditableSet<Item>()
	skippable {
		editableSet.add(
			getItem1()
		)
	}
	return editableSet
}


inline fun <Item> FiniteSet (

	crossinline contains : (Item) -> Boolean,

	crossinline forEach : ((Item) -> Unit) -> Unit

) : FiniteSet<Item> {

	return object : BaseFiniteSet<Item>() {

		override fun contains (item: Item) = contains(item)

		override fun invoke(action: (Item) -> Unit) = forEach(action)

	}

}
package ic.struct.set.ext


import ic.funs.effector.find.all
import ic.struct.set.Set
import ic.struct.set.finite.FiniteSet


operator fun <Item> Set<Item>.contains (subset: FiniteSet<Item>) : Boolean {

	return subset.all { it in this }

}
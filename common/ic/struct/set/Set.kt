package ic.struct.set


import ic.struct.collection.Collection
import ic.funs.effector.find.atLeastOne


interface Set<Item> {


	infix operator fun contains (item: Item) : Boolean

	
	fun containsAnyOf (items: Collection<Item>) : Boolean {
		return items.atLeastOne { item -> contains(item) }
	}


	class Empty<Item> : Set<Item> { override fun contains (item: Item) = false }

	class Universe<Item> : Set<Item> { override fun contains (item: Item) = true }


}

package ic.struct.set.editable.default


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.ifaces.action.action1.Action1
import ic.ifaces.hascount.HasCount
import ic.struct.set.editable.BaseEditableSet
import ic.struct.set.editable.default.impl.*
import kotlin.jvm.JvmField


class DefaultEditableSet<Item> : BaseEditableSet<Item>(), HasCount {


	internal @JvmField var containsNull : Boolean = false

	internal @JvmField var item0 : Any? = null
	internal @JvmField var item1 : Any? = null
	internal @JvmField var item2 : Any? = null
	internal @JvmField var item3 : Any? = null
	internal @JvmField var item4 : Any? = null
	internal @JvmField var item5 : Any? = null
	internal @JvmField var item6 : Any? = null
	internal @JvmField var item7 : Any? = null

	internal @JvmField var grid : Array<Any?>? = null

	internal @JvmField var bucketsCount : Int32 = initialBucketsCount

	private var countField : Int64 = 0


	override fun contains (item: Item) : Boolean {
		when {
			item == null -> return containsNull
			grid == null -> return containsInItems(item)
			else         -> return containsInGrid(item)
		}
	}

	@Throws(AlreadyExistsException::class)
	override fun addOrThrowAlreadyExists (item: Item) {
		if (item == null) {
			if (containsNull) {
				throw AlreadyExistsException
			} else {
				containsNull = true
			}
		} else {
			if (grid == null) {
				addToItemsOrThrowAlreadyExists(item)
			} else {
				addToGridOrThrowAlreadyExists(item)
			}
		}
		countField++
	}

	@Throws(NotExistsException::class)
	override fun removeOrThrowNotExists (item: Item) {
		if (item == null) {
			if (containsNull) {
				containsNull = false
			} else {
				throw NotExistsException
			}
		} else {
			if (grid == null) {
				removeFromItemsOrThrowNotExists(item)
			} else {
				removeFromGridOrThrowNotExists(item)
			}
		}
		countField--
	}

	override fun invoke (action: (Item) -> Unit) {
		if (containsNull) {
			@Suppress("UNCHECKED_CAST")
			action(null as Item)
		}
		if (grid == null) {
			forEachInItems { action(it) }
		} else {
			forEachInGrid { action(it) }
		}
	}

	override val count get() = countField

	override fun empty() {
		containsNull = false
		if (grid == null) {
			emptyItems()
		} else {
			grid = null
		}
		countField = 0
	}


}
package ic.struct.set.editable.default.impl


import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.containsInItems (item: Item) : Boolean {

	return (

		item0 == item ||
		item1 == item ||
		item2 == item ||
		item3 == item ||
		item4 == item ||
		item5 == item ||
		item6 == item ||
		item7 == item

	)

}
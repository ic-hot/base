package ic.struct.set.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.base.throwables.AlreadyExistsException
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
@Throws(AlreadyExistsException::class)
internal fun <Item> DefaultEditableSet<Item>.addToGridOrThrowAlreadyExists (item: Item) {

	val bucketIndex = getBucketIndex(item)

	var emptyIndexInBucket : Int32 = -1

	forEachInBucket(bucketIndex) { indexInBucket, itemFromBucket ->

		if (itemFromBucket == null) {
			if (emptyIndexInBucket < 0) {
				emptyIndexInBucket = indexInBucket
			}
		}

		if (itemFromBucket == item) throw AlreadyExistsException

	}

	if (emptyIndexInBucket < 0) {

		expandGrid()
		addToGrid(item)

	} else {

		setIntoBucket(bucketIndex, emptyIndexInBucket, item)

	}

}
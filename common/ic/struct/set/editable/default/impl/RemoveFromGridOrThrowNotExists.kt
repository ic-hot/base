package ic.struct.set.editable.default.impl


import ic.base.throwables.NotExistsException
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
@Throws(NotExistsException::class)
internal inline fun <Item> DefaultEditableSet<Item>.removeFromGridOrThrowNotExists (
	item : Item
) {

	val bucketIndex = getBucketIndex(item)

	forEachInBucket(bucketIndex) { indexInBucket, itemFromBucket ->

		if (itemFromBucket == item) {
			setIntoBucket(bucketIndex, indexInBucket, null)
			return
		}

	}

	throw NotExistsException

}
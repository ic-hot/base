package ic.struct.set.editable.default.impl


import ic.base.arrays.Array
import ic.base.throwables.AlreadyExistsException
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE",
	"UNCHECKED_CAST"
)
@Throws(AlreadyExistsException::class)
internal inline fun <Item> DefaultEditableSet<Item>.addToItemsOrThrowAlreadyExists (
	item : Item
) {

	if (containsInItems(item)) throw AlreadyExistsException

	when {

		item0 == null -> item0 = item
		item1 == null -> item1 = item
		item2 == null -> item2 = item
		item3 == null -> item3 = item
		item4 == null -> item4 = item
		item5 == null -> item5 = item
		item6 == null -> item6 = item
		item7 == null -> item7 = item

		else -> {

			initGrid()

			forEachInItems { addToGrid(it) }

			emptyItems()

			addToGrid(item)

		}

	}

}
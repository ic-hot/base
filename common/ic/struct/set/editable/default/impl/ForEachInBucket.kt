package ic.struct.set.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.set.editable.default.DefaultEditableSet


internal inline fun <Item> DefaultEditableSet<Item>.forEachInBucket (

	bucketIndex : Int32,

	action : (indexInBucket: Int32, itemFromBucket: Any?) -> Unit

) {

	val grid = this.grid!!

	val bucketStartIndex = bucketIndex * bucketSize

	var indexInBucket : Int32 = 0

	while (indexInBucket < bucketSize) {

		val indexInGrid = bucketStartIndex + indexInBucket

		val itemFromBucket = grid[indexInGrid]

		action(indexInBucket, itemFromBucket)

		indexInBucket++

	}

}
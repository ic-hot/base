package ic.struct.set.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.setIntoBucket (
	bucketIndex: Int32, indexInBucket: Int32, item: Any?
) {

	val bucketStartIndex = bucketIndex * bucketSize

	val indexInGrid = bucketStartIndex + indexInBucket

	grid!![indexInGrid] = item

}
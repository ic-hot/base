package ic.struct.set.editable.default.impl


import ic.base.arrays.Array
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.initGrid() {

	grid = Array(length = bucketsCount * bucketSize)

}
package ic.struct.set.editable.default.impl


import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"UNCHECKED_CAST"
)
internal inline fun <Item> DefaultEditableSet<Item>.forEachInItems (action: (Item) -> Unit) {

	item0?.let { action(it as Item) }
	item1?.let { action(it as Item) }
	item2?.let { action(it as Item) }
	item3?.let { action(it as Item) }
	item4?.let { action(it as Item) }
	item5?.let { action(it as Item) }
	item6?.let { action(it as Item) }
	item7?.let { action(it as Item) }

}
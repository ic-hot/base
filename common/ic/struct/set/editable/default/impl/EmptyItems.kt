package ic.struct.set.editable.default.impl


import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.emptyItems() {

	item0 = null
	item1 = null
	item2 = null
	item3 = null
	item4 = null
	item5 = null
	item6 = null
	item7 = null

}
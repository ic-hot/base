package ic.struct.set.editable.default.impl


import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal fun <Item> DefaultEditableSet<Item>.addToGrid (item: Item) {

	val bucketIndex = getBucketIndex(item)

	forEachInBucket(bucketIndex) { indexInBucket, itemFromBucket ->

		if (itemFromBucket == null) {
			setIntoBucket(bucketIndex, indexInBucket, item)
			return
		}

	}

	expandGrid()

	addToGrid(item)

}
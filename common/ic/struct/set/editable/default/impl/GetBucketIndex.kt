package ic.struct.set.editable.default.impl


import kotlin.math.absoluteValue

import ic.base.primitives.int32.Int32
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.getBucketIndex (item: Item) : Int32 {

	return item.hashCode().absoluteValue % bucketsCount

}
package ic.struct.set.editable.default.impl


import ic.base.arrays.ext.breakableForEach
import ic.struct.set.editable.default.DefaultEditableSet


internal inline fun <Item> DefaultEditableSet<Item>.forEachInGrid (

	grid : Array<Any?> = this.grid!!,

	action : (Item) -> Unit

) {

	grid.breakableForEach { item ->

		if (item != null) {

			@Suppress("UNCHECKED_CAST")
			action(item as Item)

		}

	}

}
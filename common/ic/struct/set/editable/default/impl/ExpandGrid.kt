package ic.struct.set.editable.default.impl


import ic.base.arrays.Array
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.expandGrid() {

	val oldGrid         = grid!!
	val oldBucketsCount = bucketsCount

	bucketsCount = oldBucketsCount * 2
	grid = Array(bucketsCount * bucketSize)

	forEachInGrid(oldGrid) { addToGrid(it) }

}
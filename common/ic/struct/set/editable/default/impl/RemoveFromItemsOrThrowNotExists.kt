package ic.struct.set.editable.default.impl


import ic.base.throwables.NotExistsException
import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
@Throws(NotExistsException::class)
internal inline fun <Item> DefaultEditableSet<Item>.removeFromItemsOrThrowNotExists (
	item : Item
) {

	when {

		item0 == item -> item0 = null
		item1 == item -> item1 = null
		item2 == item -> item2 = null
		item3 == item -> item3 = null
		item4 == item -> item4 = null
		item5 == item -> item5 = null
		item6 == item -> item6 = null
		item7 == item -> item7 = null

		else -> throw NotExistsException

	}

}
package ic.struct.set.editable.default.impl


import ic.struct.set.editable.default.DefaultEditableSet


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Item> DefaultEditableSet<Item>.containsInGrid (item: Item) : Boolean {

	val bucketIndex = getBucketIndex(item)

	forEachInBucket(bucketIndex) { _, itemFromBucket ->

		if (itemFromBucket == item) return true

	}

	return false

}
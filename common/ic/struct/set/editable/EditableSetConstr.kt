@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.set.editable


import ic.base.arrays.ext.breakableForEach
import ic.struct.set.editable.default.DefaultEditableSet


inline fun <Item> EditableSet() : EditableSet<Item> = DefaultEditableSet()

inline fun <Item> EditableSet (iterable: Iterable<Item>) = EditableSet<Item>().apply {
	iterable.forEach { addIfNotExists(it) }
}

inline fun <Item> EditableSet (vararg items: Item) = EditableSet<Item>().apply {
	items.breakableForEach { addIfNotExists(it) }
}
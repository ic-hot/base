package ic.struct.set.editable


import ic.base.annotations.Repeat
import ic.ifaces.adder.SetAdder
import ic.ifaces.emptiable.Emptiable
import ic.ifaces.remover.Remover1
import ic.struct.set.finite.FiniteSet


interface EditableSet<Item> : FiniteSet<Item>, SetAdder<Item>, Remover1<Item>, Emptiable {

	
	fun setContains (item: Item, contains: Boolean) {
		if (contains) {
			addIfNotExists(item)
		} else {
			removeOrThrowNotExists(item)
		}
	}


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	@Repeat
	override fun removeOrThrowNotExists (item: Item)


}
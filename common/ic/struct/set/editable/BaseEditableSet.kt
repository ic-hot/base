package ic.struct.set.editable


import ic.struct.set.finite.BaseFiniteSet


abstract class BaseEditableSet<Item> : BaseFiniteSet<Item>(), EditableSet<Item> {



}
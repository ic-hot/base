package ic.struct.set.editable.ext


import ic.struct.set.editable.EditableSet


fun <Item> EditableSet<Item>.flip (item: Item) {

	if (this contains item) {
		remove(item)
	} else {
		add(item)
	}

}
package ic.struct.set.editable.ext


import ic.struct.set.editable.EditableSet


inline fun <Item> Item.processAndAddIfNotAlreadyIn (set: EditableSet<Item>, process: (Item) -> Unit) {

	if (this in set) return

	set.add(this)

	process(this)

}
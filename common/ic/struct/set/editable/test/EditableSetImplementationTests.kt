package ic.struct.set.editable.test


import ic.struct.list.List
import ic.struct.set.editable.EditableSet
import ic.test.Test
import ic.test.TestSuite


abstract class EditableSetImplementationTests : TestSuite() {


	protected abstract val setImplementationName : String

	protected abstract val nextEditableSet : EditableSet<Any?>


	override fun initTests() : List<Test> {
		val suite = this
		return List(

			object : NewSetIsEmpty() {
				override val setImplementationName get() = suite.setImplementationName
				override val nextEditableSet get() = suite.nextEditableSet
			},

			object : AddAndRemoveTest() {
				override val setImplementationName get() = suite.setImplementationName
				override val nextEditableSet get() = suite.nextEditableSet
				override val itemsCount get() = 1024L
			}

		)
	}


}
package ic.struct.set.editable.test


import ic.struct.list.List
import ic.struct.set.editable.default.DefaultEditableSet
import ic.test.Test
import ic.test.TestSuite


internal class EditableSetTests : TestSuite() {


	override fun initTests() = List<Test>(

		object : EditableSetImplementationTests() {
			override val setImplementationName get() = "DefaultEditableSet"
			override val nextEditableSet get() = DefaultEditableSet<Any?>()
		}

	)


}
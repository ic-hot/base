package ic.struct.set.editable.test


import ic.base.assert.assert
import ic.struct.collection.ext.count.isEmpty
import ic.struct.set.editable.EditableSet
import ic.text.TextOutput


internal abstract class NewSetIsEmpty : EditableSetTest() {

	override fun TextOutput.runTest (editableSet: EditableSet<Any?>) {

		assert { editableSet.isEmpty }

	}

}
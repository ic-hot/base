package ic.struct.set.editable.test


import ic.struct.set.editable.EditableSet
import ic.test.Test
import ic.text.TextOutput


internal abstract class EditableSetTest : Test() {


	protected abstract val setImplementationName : String

	protected abstract val nextEditableSet : EditableSet<Any?>


	protected abstract fun TextOutput.runTest (editableSet: EditableSet<Any?>)


	override fun TextOutput.runTest() {

		runTest(nextEditableSet)

	}


}
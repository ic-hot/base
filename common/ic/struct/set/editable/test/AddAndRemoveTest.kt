package ic.struct.set.editable.test


import ic.base.assert.assert
import ic.base.loop.breakableRepeat
import ic.base.primitives.int64.Int64
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.struct.collection.ext.count.count
import ic.struct.set.editable.EditableSet
import ic.text.TextOutput


internal abstract class AddAndRemoveTest : EditableSetTest() {


	override val testName get() = (
		"AddAndRemoveTest(" +
			"setImplementation: $setImplementationName, " +
			"itemsCount: $itemsCount" +
		")"
	)


	protected abstract val itemsCount : Int64


	override fun TextOutput.runTest (editableSet: EditableSet<Any?>) {

		breakableRepeat(itemsCount) { index ->

			editableSet.add("item$index")

			assert { editableSet.count == index + 1 }

			breakableRepeat(itemsCount) { i ->

				if (i <= index) {

					assert { "item$i" in editableSet }

					try {
						editableSet.addOrThrowAlreadyExists("item$i")
						assert { false }
					} catch (t: AlreadyExistsException) {}

				} else {

					assert { "item$i" !in editableSet }

					try {
						editableSet.removeOrThrowNotExists("item$i")
						assert { false }
					} catch (t: NotExistsException) {}

				}

			}

		}

		breakableRepeat(itemsCount) { index ->

			editableSet.remove("item$index")

			assert { editableSet.count == itemsCount - 1 - index }

			breakableRepeat(itemsCount) { i ->

				if (i <= index) {

					assert { "item$i" !in editableSet }

					try {
						editableSet.removeOrThrowNotExists("item$i")
						assert { false }
					} catch (t: NotExistsException) {}

				} else {

					assert { "item$i" in editableSet }

					try {
						editableSet.addOrThrowAlreadyExists("item$i")
						assert { false }
					} catch (t: AlreadyExistsException) {}

				}

			}

		}

	}


}
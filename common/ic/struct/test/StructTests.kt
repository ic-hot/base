package ic.struct.test


import ic.struct.list.List
import ic.struct.list.test.ListTests
import ic.struct.map.test.MapTests
import ic.struct.set.test.SetTests
import ic.test.TestSuite


internal class StructTests : TestSuite() {

	override fun initTests() = List(

		ListTests(),

		SetTests(),

		MapTests()

	)

}
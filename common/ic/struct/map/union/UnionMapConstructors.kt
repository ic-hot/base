@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.map.union


import ic.struct.collection.Collection
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.Map


inline fun <Key, Value> UnionMap (

	sourceMaps : Collection<Map<Key, out Value>>

) : UnionMap<Key, Value> {

	return object : UnionMap<Key, Value>() {

		override val sourceMaps get() = sourceMaps

	}

}


inline fun <Key, Value> UnionMap (

	vararg sourceMaps : Map<Key, out Value>

) : UnionMap<Key, Value> {

	return UnionMap(
		sourceMaps = ListFromArray(sourceMaps, isArrayImmutable = true)
	)

}
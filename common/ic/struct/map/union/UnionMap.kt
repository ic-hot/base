package ic.struct.map.union


import ic.base.escape.breakable.Break
import ic.struct.collection.Collection
import ic.struct.collection.convert.ConvertCollection
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.Map
import ic.struct.set.Set
import ic.struct.set.union.UnionSet


abstract class UnionMap<Key, Value> : Map<Key, Value> {


	protected abstract val sourceMaps : Collection<Map<Key, out Value>>


	override val keys = object : UnionSet<Key>() {

		override val sourceSets = object : ConvertCollection<Set<Key>, Map<Key, out Value>>() {

			override val sourceCollection get() = sourceMaps

			override fun convertItem (sourceItem: Map<Key, out Value>) = sourceItem.keys

		}

	}


	override fun get (key: Key) : Value {

		var value : Value? = null

		sourceMaps.breakableForEach { sourceMap ->

			value = sourceMap[key]

			if (value != null) Break()

		}

		@Suppress("UNCHECKED_CAST")
		return value as Value

	}


}
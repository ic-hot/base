@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE", "SortModifiers")


package ic.struct.map

import ic.ifaces.getter.getter1.Getter1
import ic.struct.set.Set


interface Map<Key, Value> : Getter1<Value, Key> {


	val keys : Set<Key>


	override operator fun get (key: Key) : Value


}

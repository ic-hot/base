package ic.struct.map.complete


import ic.struct.map.Map
import ic.struct.set.Set


interface CompleteMap<Key, Value> : Map<Key, Value> {

	override val keys get() = Set.Universe<Key>()

}
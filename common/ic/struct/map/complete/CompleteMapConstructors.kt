@file:Suppress("FunctionName")


package ic.struct.map.complete


inline fun <Key, Value> CompleteMap (

	crossinline keyToValue : (Key) -> Value

) : CompleteMap<Key, Value> {

	return object : CompleteMap<Key, Value> {

		override fun get (key: Key) : Value {
			return keyToValue(key)
		}

	}

}
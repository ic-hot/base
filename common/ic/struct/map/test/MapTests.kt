package ic.struct.map.test


import ic.struct.list.List
import ic.struct.map.editable.default.DefaultEditableMap
import ic.test.Test
import ic.test.TestSuite


class MapTests : TestSuite() {

	override fun initTests() = List<Test>(

		object : EditableMapImplementationTests() {
			override val mapImplementationName get() = "DefaultEditableMap"
			override val nextEditableMap get() = DefaultEditableMap<Any?, Any>()
		}

	)

}
package ic.struct.map.test


import ic.struct.map.editable.EditableMap
import ic.test.Test
import ic.text.TextOutput


internal abstract class EditableMapTest : Test() {


	protected abstract val mapImplementationName : String

	protected abstract val nextEditableMap : EditableMap<Any?, Any>


	protected abstract fun TextOutput.runTest (editableMap: EditableMap<Any?, Any>)


	override fun TextOutput.runTest() {

		runTest(nextEditableMap)

	}


}
package ic.struct.map.test


import ic.base.assert.assert
import ic.ifaces.hascount.ext.isEmpty
import ic.struct.map.editable.EditableMap
import ic.text.TextOutput


internal abstract class NullKey : EditableMapTest() {


	override val testName get() = (
		"NullKey(" +
			"mapImplementation: $mapImplementationName" +
		")"
	)


	override fun TextOutput.runTest (editableMap: EditableMap<Any?, Any>) {

		editableMap[null] = "nullValue"

		assert { editableMap.count == 1L }

		assert { editableMap[null] == "nullValue" }

		editableMap[null] = null

		assert { editableMap.isEmpty }

		assert { editableMap[null] == null }

	}


}
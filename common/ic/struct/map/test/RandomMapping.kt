package ic.struct.map.test


import ic.base.assert.assert
import ic.base.loop.breakableRepeat
import ic.base.primitives.int64.Int64
import ic.struct.map.editable.EditableMap
import ic.text.TextOutput


internal abstract class RandomMapping : EditableMapTest() {


	protected abstract val itemsCount : Int64


	override val testName get() = (
		"${ RandomMapping::class.qualifiedName }(" +
			"mapImplementation: $mapImplementationName" +
		")"
	)


	override fun TextOutput.runTest (editableMap: EditableMap<Any?, Any>) {

		breakableRepeat(itemsCount) { index ->

			editableMap["key$index"] = "value$index"

			assert { editableMap.count == index + 1 }

			breakableRepeat(itemsCount) { i ->

				if (i <= index) {

					assert { editableMap["key$i"] == "value$i" }

				} else {

					assert { editableMap["key$i"] == null }

				}

			}

		}

		breakableRepeat(itemsCount) { index ->

			editableMap["key$index"] = "newValue$index"

			assert { editableMap.count == itemsCount }

			breakableRepeat(itemsCount) { i ->

				if (i <= index) {

					assert { editableMap["key$i"] == "newValue$i" }

				} else {

					assert { editableMap["key$i"] == "value$i" }

				}

			}

		}

		breakableRepeat(itemsCount) { index ->

			editableMap["key$index"] = null

			assert { editableMap.count == itemsCount - 1 - index }

			breakableRepeat(itemsCount) { i ->

				if (i <= index) {

					assert { editableMap["key$i"] == null }

				} else {

					assert { editableMap["key$i"] == "newValue$i" }

				}

			}

		}

	}


}
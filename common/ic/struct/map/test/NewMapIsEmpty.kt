package ic.struct.map.test


import ic.base.assert.assert
import ic.ifaces.hascount.ext.isEmpty
import ic.struct.map.editable.EditableMap
import ic.text.TextOutput


internal abstract class NewMapIsEmpty : EditableMapTest() {


	override val testName get() = (
		"NewMapIsEmpty(" +
			"mapImplementation: $mapImplementationName" +
		")"
	)


	override fun TextOutput.runTest (editableMap: EditableMap<Any?, Any>) {

		assert { editableMap.isEmpty }

	}


}
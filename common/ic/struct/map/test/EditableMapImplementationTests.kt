package ic.struct.map.test


import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.map.editable.EditableMap
import ic.test.Test
import ic.test.TestSuite


internal abstract class EditableMapImplementationTests : TestSuite() {


	protected abstract val mapImplementationName : String

	protected abstract val nextEditableMap : EditableMap<Any?, Any>


	override fun initTests() : List<Test> {
		val suite = this
		return List(

			object : NewMapIsEmpty() {
				override val mapImplementationName get() = suite.mapImplementationName
				override val nextEditableMap get() = suite.nextEditableMap
			},

			object : NullKey() {
				override val mapImplementationName get() = suite.mapImplementationName
				override val nextEditableMap get() = suite.nextEditableMap
			},

			object : RandomMapping() {
				override val itemsCount get() = 256L
				override val mapImplementationName get() = suite.mapImplementationName
				override val nextEditableMap get() = suite.nextEditableMap
			}

		)
	}


}
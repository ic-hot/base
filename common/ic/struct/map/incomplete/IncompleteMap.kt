package ic.struct.map.incomplete


import ic.struct.map.Map


interface IncompleteMap<Key, Value: Any> : Map<Key, Value?> {



}
package ic.struct.map.incomplete.ext


import ic.struct.map.incomplete.IncompleteMap


@Suppress("NOTHING_TO_INLINE")
inline infix fun <Key> IncompleteMap<Key, *>.containsKey (key: Key) : Boolean {

	return this[key] != null

}
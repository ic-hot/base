package ic.struct.map.ephemeral


import ic.base.primitives.int64.Int64
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.foreach.forEach
import ic.struct.map.editable.BaseEditableMap
import ic.struct.map.editable.EditableMap
import ic.struct.map.ephemeral.impl.cleanUpMinimumFrequencyInMs
import ic.struct.map.finite.ext.foreach.forEach
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet
import ic.struct.value.Val
import ic.struct.value.ephemeral.Ephemeral
import ic.system.funs.getCurrentEpochTimeMs


abstract class EphemeralValuesMap<Key, Value: Any> : BaseEditableMap<Key, Value>() {


	protected abstract fun createEphemeralValue (value: Value) : Ephemeral<Value>


	private val map = EditableMap<Key, Val<Value?>>()


	override fun get (key: Key) : Value? {
		cleanUpIfNeeded()
		return map[key]?.value
	}

	override fun set (key: Key, value: Value?) {
		cleanUpIfNeeded()
		map[key] = value?.let { createEphemeralValue(value) }
	}

	// TODO Make lazy
	override val keys : FiniteSet<Key> get() = EditableSet<Key>().also {
		cleanUpIfNeeded()
		map.forEach { key, ephemeralValue ->
			val value = ephemeralValue.value
			if (value != null) {
				it.addIfNotExists(key)
			}
		}
	}

	override val count get() = keys.count

	override fun empty() {
		map.empty()
		lastCleanUpTimeEpochMs = getCurrentEpochTimeMs()
	}


	private var lastCleanUpTimeEpochMs : Int64 = getCurrentEpochTimeMs()

	private fun cleanUpIfNeeded() {
		if (getCurrentEpochTimeMs() - lastCleanUpTimeEpochMs < cleanUpMinimumFrequencyInMs) {
			return
		}
		map.keys.forEach { key ->
			val ephemeralValue = map[key]!!
			if (ephemeralValue.value == null) {
				map[key] = null
			}
		}
		lastCleanUpTimeEpochMs = getCurrentEpochTimeMs()
	}


}

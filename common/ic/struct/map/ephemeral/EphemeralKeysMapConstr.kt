package ic.struct.map.ephemeral


import ic.struct.value.ephemeral.Ephemeral


inline fun <Key, Value: Any> EphemeralKeysMap (

	crossinline createEphemeralKey : (Any) -> Ephemeral<Any>

) : EphemeralKeysMap<Key, Value> {

	return object : EphemeralKeysMap<Key, Value>() {

		override fun createEphemeralKey (key: Any) = createEphemeralKey(key)

	}

}
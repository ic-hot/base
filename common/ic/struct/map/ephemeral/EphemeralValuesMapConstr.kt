package ic.struct.map.ephemeral


import ic.struct.value.ephemeral.Ephemeral


inline fun <Key, Value: Any> EphemeralValuesMap (

	crossinline createEphemeralValue : (Value) -> Ephemeral<Value>

) : EphemeralValuesMap<Key, Value> {

	return object : EphemeralValuesMap<Key, Value>() {

		override fun createEphemeralValue (value: Value) = createEphemeralValue(value)

	}

}
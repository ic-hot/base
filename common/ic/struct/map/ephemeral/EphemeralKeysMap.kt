@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.map.ephemeral


import ic.base.primitives.int64.Int64
import ic.ifaces.hascount.HasCount
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.ext.foreach.breakableForEach
import ic.struct.map.editable.BaseEditableMap
import ic.struct.map.editable.default.DefaultEditableMap
import ic.struct.map.editable.default.ext.findKeyOrNull
import ic.struct.map.ephemeral.impl.cleanUpMinimumFrequencyInMs
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet
import ic.struct.value.ephemeral.Ephemeral
import ic.system.funs.getCurrentEpochTimeMs


abstract class EphemeralKeysMap <Key, Value: Any> : BaseEditableMap<Key, Value>(), HasCount {


	protected abstract fun createEphemeralKey (key: Any) : Ephemeral<Any>


	private var nullValue : Value? = null

	private val map = DefaultEditableMap<Ephemeral<Any>, Value>()


	override fun get (key: Key) : Value? {
		cleanUpIfNeeded()
		if (key == null) {
			return nullValue
		} else {
			val hashCode = key.hashCode()
			val ephemeralKey = map.findKeyOrNull(hashCode = hashCode) { it.value == key }
			if (ephemeralKey == null) {
				return null
			} else {
				return map[ephemeralKey]
			}
		}
	}

	override fun set (key: Key, value: Value?) {
		cleanUpIfNeeded()
		if (key == null) {
			nullValue = key
		} else {
			val hashCode = key.hashCode()
			val ephemeralKey = map.findKeyOrNull(hashCode = hashCode) { it.value == key }
			if (ephemeralKey == null) {
				if (value != null) {
					map[createEphemeralKey(key)] = value
				}
			} else {
				map[ephemeralKey] = value
			}
		}
	}

	private inline fun implementForEach (crossinline action: (Key, Value) -> Unit) {
		cleanUpIfNeeded()
		nullValue?.let {
			@Suppress("UNCHECKED_CAST")
			action(null as Key, it)
		}
		map.breakableForEach { ephemeralKey, value ->
			val key = ephemeralKey.value
			if (key != null) {
				@Suppress("UNCHECKED_CAST")
				action(key as Key, value)
			}
		}
	}

	// TODO Make lazy
	override val keys : FiniteSet<Key> get() = EditableSet<Key>().also {
		implementForEach { key, _ ->
			it.add(key)
		}
	}

	override val count : Int64 get() {
		cleanUpIfNeeded()
		var count = map.count
		if (nullValue != null) count++
		return count
	}

	override fun empty() {
		nullValue = null
		map.empty()
	}


	private var lastCleanUpTimeEpochMs : Int64 = getCurrentEpochTimeMs()

	private fun cleanUpIfNeeded() {
		if (getCurrentEpochTimeMs() - lastCleanUpTimeEpochMs < cleanUpMinimumFrequencyInMs) {
			return
		}
		map.keys.breakableForEach { ephemeralKey ->
			if (ephemeralKey.value == null) {
				map[ephemeralKey] = null
			}
		}
		lastCleanUpTimeEpochMs = getCurrentEpochTimeMs()
	}


}

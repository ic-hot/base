package ic.struct.map.editable


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress("NOTHING_TO_INLINE")
inline fun <Key, Value: Any> EditableMap() : EditableMap<Key, Value> = DefaultEditableMap()
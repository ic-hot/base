package ic.struct.map.editable


import ic.struct.map.finite.BaseFiniteMap


abstract class BaseEditableMap<Key, Value: Any>
	: BaseFiniteMap<Key, Value>()
	, EditableMap<Key, Value>
{

}
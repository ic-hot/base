package ic.struct.map.editable.test


import ic.struct.list.List
import ic.struct.map.editable.default.DefaultEditableMap
import ic.struct.map.test.EditableMapImplementationTests
import ic.test.Test
import ic.test.TestSuite


class EditableMapTests : TestSuite() {


	override fun initTests() = List<Test>(

		object : EditableMapImplementationTests() {
			override val mapImplementationName get() = "DefaultEditableMap"
			override val nextEditableMap get() = DefaultEditableMap<Any?, Any>()
		}

	)


}
package ic.struct.map.editable.ext.copy


import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.copy.copyToEditableMap


@Suppress("NOTHING_TO_INLINE")
inline fun <Key, Value: Any> EditableMap<Key, Value>.copy () = copyToEditableMap()
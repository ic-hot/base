package ic.struct.map.editable.ext


import ic.struct.map.editable.EditableMap
import ic.struct.map.editable.sync.SynchronizedMap


inline val <Key, Value: Any> EditableMap<Key, Value>.synchronized get() = (

	SynchronizedMap(this)

)
package ic.struct.map.editable.sync


import ic.struct.map.editable.EditableMap


inline fun <Key, Value: Any> SynchronizedMap (

	sourceEditableMap : EditableMap<Key, Value>

) : SynchronizedMap<Key, Value> {

	return object : SynchronizedMap<Key, Value>() {

		override val sourceEditableMap get() = sourceEditableMap

	}

}
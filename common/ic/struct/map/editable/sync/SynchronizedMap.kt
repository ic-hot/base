package ic.struct.map.editable.sync


import ic.ifaces.mutable.Mutable
import ic.ifaces.mutable.ext.synchronized
import ic.parallel.mutex.Mutex
import ic.struct.map.editable.BaseEditableMap
import ic.struct.map.editable.EditableMap


abstract class SynchronizedMap<Key, Value: Any> : BaseEditableMap<Key, Value>(), Mutable {


	protected abstract val sourceEditableMap : EditableMap<Key, Value>


	override val mutex = Mutex()


	override val keys get() = synchronized { sourceEditableMap.keys }

	override val count get() = synchronized { sourceEditableMap.count }

	override fun get (key: Key) : Value? {
		synchronized {
			return sourceEditableMap.get(key)
		}
	}

	override fun set (key: Key, value: Value?) {
		synchronized {
			sourceEditableMap.set(key, value)
		}
	}


	override fun empty() {
		synchronized {
			sourceEditableMap.empty()
		}
	}


}
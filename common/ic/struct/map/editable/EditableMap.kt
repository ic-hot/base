package ic.struct.map.editable


import ic.ifaces.emptiable.Emptiable
import ic.ifaces.gettersetter.gettersetter1.GetterSetter1
import ic.struct.map.finite.FiniteMap


@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
interface EditableMap<Key, Value: Any>
	: FiniteMap<Key, Value>
	, GetterSetter1<Value?, Key>
	, Emptiable
{


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	operator override fun set (key: Key, value: Value?)


}

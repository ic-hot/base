package ic.struct.map.editable.fromkmap


import ic.base.kcollections.toEditableSet
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.ifaces.hascount.HasCount
import ic.struct.map.editable.BaseEditableMap
import ic.struct.set.finite.FiniteSet


class EditableMapFromMutableMap <Key, Value: Any> (

	private val mutableMap : MutableMap<Key, Value>

) : BaseEditableMap<Key, Value>(), HasCount {

	private var nullValue : Value? = null

	override val count : Int64 get() {
		if (nullValue == null) {
			return mutableMap.size.asInt64
		} else {
			return mutableMap.size.asInt64 + 1
		}
	}

	// TODO make lazy
	override val keys : FiniteSet<Key> get() {
		if (nullValue == null) {
			return FiniteSet(mutableMap.keys)
		} else {
			return mutableMap.keys.toEditableSet().apply {
				@Suppress("UNCHECKED_CAST")
				add(null as Key)
			}
		}
	}

	override fun empty() {
		mutableMap.clear()
		nullValue = null
	}

	override fun get (key: Key) : Value? {
		if (key == null) {
			return nullValue
		} else {
			return mutableMap[key]
		}
	}

	override fun set (key: Key, value: Value?) {
		if (key == null) {
			nullValue = value
		} else {
			if (value == null) {
				mutableMap.remove(key)
			} else {
				mutableMap[key] = value
			}
		}
	}

}
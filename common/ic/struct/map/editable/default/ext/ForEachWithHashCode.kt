package ic.struct.map.editable.default.ext


import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap
import ic.struct.map.editable.default.impl.forEachInBucket
import ic.struct.map.editable.default.impl.forEachInItems
import ic.struct.map.editable.default.impl.getBucketIndex


@Suppress(
	"UNCHECKED_CAST"
)
internal inline fun <Key, Value: Any>
	DefaultEditableMap<Key, Value>.forEach (
		hashCode : Int32,
		action : (Key, Value) -> Unit
	)
{

	if (hashCode == null.hashCode()) {
		nullValue?.let {
			action(null as Key, it)
		}
	}

	if (grid == null) {

		forEachInItems(action)

	} else {

		val bucketIndex = getBucketIndex(hashCode)

		forEachInBucket(bucketIndex) { _, key, value ->

			if (key != null) {
				action(key as Key, value!!)
			}

		}

	}

}
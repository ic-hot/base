package ic.struct.map.editable.default.ext


import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap


fun <Key, Value: Any>
	DefaultEditableMap<Key, Value>.findKeyOrNull (
		hashCode : Int32,
		predicate : (Key) -> Boolean
	) : Key?
{

	forEach(hashCode = hashCode) { key, _ ->

		if (predicate(key)) {
			return key
		}

	}

	return null

}
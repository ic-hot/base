package ic.struct.map.editable.default


import kotlin.jvm.JvmField

import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.ifaces.hascount.HasCount
import ic.struct.map.editable.BaseEditableMap
import ic.struct.map.editable.default.impl.*
import ic.struct.map.editable.default.impl.getFromGrid
import ic.struct.map.editable.default.impl.getFromItems
import ic.struct.map.editable.default.impl.initialBucketsCount
import ic.struct.map.editable.default.impl.setIntoItems
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


class DefaultEditableMap<Key, Value: Any> : BaseEditableMap<Key, Value>(), HasCount {


	internal @JvmField var nullValue : Value? = null

	internal @JvmField var key0 : Any? = null
	internal @JvmField var key1 : Any? = null
	internal @JvmField var key2 : Any? = null
	internal @JvmField var key3 : Any? = null
	internal @JvmField var key4 : Any? = null
	internal @JvmField var key5 : Any? = null
	internal @JvmField var key6 : Any? = null
	internal @JvmField var key7 : Any? = null

	internal @JvmField var value0 : Any? = null
	internal @JvmField var value1 : Any? = null
	internal @JvmField var value2 : Any? = null
	internal @JvmField var value3 : Any? = null
	internal @JvmField var value4 : Any? = null
	internal @JvmField var value5 : Any? = null
	internal @JvmField var value6 : Any? = null
	internal @JvmField var value7 : Any? = null

	internal @JvmField var grid : Array<Any?>? = null

	internal @JvmField var bucketsCount : Int32 = initialBucketsCount

	internal var countField : Int64 = 0


	override fun get (key: Key) : Value? {
		when {
			key == null  -> return nullValue
			grid == null -> return getFromItems(key)
			else         -> return getFromGrid(key)
		}
	}

	override fun set (key: Key, value: Value?) {
		when {
			key == null -> {
				if (nullValue == null) {
					if (value != null) {
						countField++
					}
				} else {
					if (value == null) {
						countField--
					}
				}
				nullValue = value
			}
			grid == null -> setIntoItems(key, value)
			else         -> setIntoGrid(key, value)
		}
	}

	private inline fun forEach (action: (Key, Value) -> Unit) {
		nullValue?.let {
			@Suppress("UNCHECKED_CAST")
			action(null as Key, it)
		}
		if (grid == null) {
			forEachInItems { key, value -> action(key, value) }
		} else {
			forEachInGrid { key, value -> action(key, value) }
		}
	}

	// TODO make lazy
	override val keys : FiniteSet<Key> get() = EditableSet<Key>().also {
		forEach { key, _ ->
			it.add(key)
		}
	}

	override val count get() = countField

	override fun empty() {
		nullValue = null
		if (grid == null) {
			emptyItems()
		} else {
			grid = null
		}
		countField = 0
	}


}
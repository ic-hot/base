package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


internal fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.emptyItems()
{

	key0 = null
	key1 = null
	key2 = null
	key3 = null
	key4 = null
	key5 = null
	key6 = null
	key7 = null

	value0 = null
	value1 = null
	value2 = null
	value3 = null
	value4 = null
	value5 = null
	value6 = null
	value7 = null

}
package ic.struct.map.editable.default.impl


import kotlin.math.absoluteValue

import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.getBucketIndex (hashCode: Int32) : Int32
{

	return hashCode.absoluteValue % bucketsCount

}


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.getBucketIndex (key: Key) : Int32
{

	return getBucketIndex(hashCode = key.hashCode())

}
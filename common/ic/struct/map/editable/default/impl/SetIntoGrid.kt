package ic.struct.map.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.setIntoGrid (key: Key, value: Value?)
{

	val bucketIndex = getBucketIndex(key)

	if (value == null) {

		forEachInBucket(bucketIndex) { indexInBucket, keyFromBucket, _ ->

			if (keyFromBucket == key) {
				setIntoBucket(bucketIndex, indexInBucket, null, null)
				countField--
				return
			}

		}

	} else {

		var emptyIndexInBucket : Int32 = -1

		forEachInBucket(bucketIndex) { indexInBucket, keyFromBucket, _ ->

			if (keyFromBucket == null) {
				if (emptyIndexInBucket < 0) {
					emptyIndexInBucket = indexInBucket
				}
			}

			if (keyFromBucket == key) {
				setIntoBucket(bucketIndex, indexInBucket, key, value)
				return
			}

		}

		if (emptyIndexInBucket < 0) {

			expandGrid()
			addIntoGrid(key, value)

		} else {

			setIntoBucket(bucketIndex, emptyIndexInBucket, key, value)

		}

		countField++

	}

}
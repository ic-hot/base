package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Key, Value: Any> DefaultEditableMap<Key, Value>.expandGrid() {

	val oldGrid         = grid!!
	val oldBucketsCount = bucketsCount

	bucketsCount = oldBucketsCount * 2
	initGrid()

	forEachInGrid(oldGrid) { key, value -> addIntoGrid(key, value) }

}
package ic.struct.map.editable.default.impl


internal const val pairSize = 2

internal const val pairsInBucketCount = 8

internal const val bucketSize = pairSize * pairsInBucketCount

internal const val initialBucketsCount = 8
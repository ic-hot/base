package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.addIntoGrid (key: Key, value: Value)
{

	val bucketIndex = getBucketIndex(key)

	forEachInBucket(bucketIndex) { indexInBucket, keyFromBucket, _ ->

		if (keyFromBucket == null) {
			setIntoBucket(bucketIndex, indexInBucket, key, value)
			return
		}

	}

	expandGrid()

	addIntoGrid(key, value)

}
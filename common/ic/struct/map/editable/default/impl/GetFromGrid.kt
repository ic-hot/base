package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE", "UNCHECKED_CAST"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.getFromGrid (key: Key) : Value?
{

	val bucketIndex = getBucketIndex(key)

	forEachInBucket(bucketIndex) { _, keyFromBucket, valueFromBucket ->

		if (keyFromBucket == key) return valueFromBucket

	}

	return null

}
package ic.struct.map.editable.default.impl


import ic.base.arrays.Array
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun <Key, Value: Any> DefaultEditableMap<Key, Value>.initGrid() {

	grid = Array(bucketsCount * bucketSize)

}
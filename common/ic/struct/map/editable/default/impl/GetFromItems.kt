package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE", "UNCHECKED_CAST"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.getFromItems (key: Key) : Value?
{

	return when {

		key0 == key -> value0
		key1 == key -> value1
		key2 == key -> value2
		key3 == key -> value3
		key4 == key -> value4
		key5 == key -> value5
		key6 == key -> value6
		key7 == key -> value7

		else -> null

	} as Value?

}
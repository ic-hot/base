package ic.struct.map.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"UNCHECKED_CAST"
)
internal inline fun <Key, Value: Any> DefaultEditableMap<Key, Value>.forEachInBucket (

	bucketIndex : Int32,

	action : (indexInBucket: Int32, keyFromBucket: Any?, valueFromBucket: Value?) -> Unit

) {

	val grid = this.grid!!

	val bucketStartIndex = bucketIndex * bucketSize

	var indexInBucket : Int32 = 0

	while (indexInBucket < bucketSize) {

		val indexInGrid = bucketStartIndex + indexInBucket

		val keyFromBucket   = grid[indexInGrid]
		val valueFromBucket = grid[indexInGrid + 1]

		action(indexInBucket, keyFromBucket, valueFromBucket as Value?)

		indexInBucket += pairSize

	}

}
package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.setIntoItems (key: Key, value: Value?)
{

	if (value == null) {

		when {
			key0 == key -> { key0 = null; value0 = null; countField-- }
			key1 == key -> { key1 = null; value1 = null; countField-- }
			key2 == key -> { key2 = null; value2 = null; countField-- }
			key3 == key -> { key3 = null; value3 = null; countField-- }
			key4 == key -> { key4 = null; value4 = null; countField-- }
			key5 == key -> { key5 = null; value5 = null; countField-- }
			key6 == key -> { key6 = null; value6 = null; countField-- }
			key7 == key -> { key7 = null; value7 = null; countField-- }
		}

	} else {

		when {

			key0 == key -> value0 = value
			key1 == key -> value1 = value
			key2 == key -> value2 = value
			key3 == key -> value3 = value
			key4 == key -> value4 = value
			key5 == key -> value5 = value
			key6 == key -> value6 = value
			key7 == key -> value7 = value

			else -> {

				when {

					key0 == null -> { key0 = key; value0 = value; }
					key1 == null -> { key1 = key; value1 = value; }
					key2 == null -> { key2 = key; value2 = value; }
					key3 == null -> { key3 = key; value3 = value; }
					key4 == null -> { key4 = key; value4 = value; }
					key5 == null -> { key5 = key; value5 = value; }
					key6 == null -> { key6 = key; value6 = value; }
					key7 == null -> { key7 = key; value7 = value; }

					else -> {
						initGrid()
						forEachInItems { k, v -> addIntoGrid(k, v) }
						emptyItems()
						addIntoGrid(key, value)
					}

				}

				countField++

			}

		}

	}

}
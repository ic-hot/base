package ic.struct.map.editable.default.impl


import ic.struct.map.editable.default.DefaultEditableMap


@Suppress("UNCHECKED_CAST")
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.forEachInItems (
		action : (Key, Value) -> Unit
	)
{

	key0?.let { action(it as Key, value0 as Value) }
	key1?.let { action(it as Key, value1 as Value) }
	key2?.let { action(it as Key, value2 as Value) }
	key3?.let { action(it as Key, value3 as Value) }
	key4?.let { action(it as Key, value4 as Value) }
	key5?.let { action(it as Key, value5 as Value) }
	key6?.let { action(it as Key, value6 as Value) }
	key7?.let { action(it as Key, value7 as Value) }

}
package ic.struct.map.editable.default.impl


import ic.base.arrays.ext.length
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"UNCHECKED_CAST"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.forEachInGrid (
		grid : Array<Any?> = this.grid!!,
		action : (Key, Value) -> Unit
	)
{

	val gridLength = grid.length

	var indexInGrid = 0

	while (indexInGrid < gridLength) {

		val key = grid[indexInGrid]

		if (key != null) {

			val value = grid[indexInGrid + 1]

			action(key as Key, value as Value)

		}

		indexInGrid += pairSize

	}

}
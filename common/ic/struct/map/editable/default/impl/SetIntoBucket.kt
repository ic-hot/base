package ic.struct.map.editable.default.impl


import ic.base.primitives.int32.Int32
import ic.struct.map.editable.default.DefaultEditableMap


@Suppress(
	"NOTHING_TO_INLINE"
)
internal inline fun
	<Key, Value: Any>
	DefaultEditableMap<Key, Value>.setIntoBucket (
		bucketIndex: Int32, indexInBucket: Int32, key: Key?, value: Value?
	)
{

	val grid = grid!!

	val bucketStartIndex = bucketIndex * bucketSize

	val indexInGrid = bucketStartIndex + indexInBucket

	grid[indexInGrid]     = key
	grid[indexInGrid + 1] = value

}
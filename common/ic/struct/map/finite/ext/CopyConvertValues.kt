package ic.struct.map.finite.ext


import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap


fun
	<Key, Value: Any, SourceValue: Any>
	FiniteMap<Key, SourceValue>.copyConvertValues (
		convertValue : (SourceValue) -> Value
	)
	: FiniteMap<Key, Value>
{
	val editableMap = EditableMap<Key, Value>()
	synchronizedIfMutable {
		keys.breakableForEach { key ->
			editableMap[key] = convertValue(this[key]!!)
		}
	}
	return editableMap
}
package ic.struct.map.finite.ext


import ic.struct.collection.Collection
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.union.UnionFiniteMap


@Suppress("NOTHING_TO_INLINE")
inline fun
	<Key, Value: Any>
	Collection<FiniteMap<Key, Value>>.lazyUnion ()
	: FiniteMap<Key, Value>
{
	return UnionFiniteMap(this)
}
package ic.struct.map.finite.ext


import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.collection.Collection
import ic.struct.list.editable.EditableList
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.breakableForEach


fun <Key, Value: Any> FiniteMap<Key, Value>.getValues() : Collection<Value> {

	val values = EditableList<Value>()

	synchronizedIfMutable {

		breakableForEach { _, value ->

			values.add(value)

		}

	}

	return values

}
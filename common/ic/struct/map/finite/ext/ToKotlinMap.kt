package ic.struct.map.finite.ext


import ic.struct.map.finite.FiniteMap


fun <Key, Value: Any> FiniteMap<Key, Value>.toKotlinMap()

	: kotlin.collections.Map<Key, Value>

	= toMutableMap()

;
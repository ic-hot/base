package ic.struct.map.finite.ext.copy


import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach


fun <Key, Value: Any> FiniteMap<Key, Value>.copyToEditableMap () = run {
	val copy = EditableMap<Key, Value>()
	forEach { key, value ->
		copy[key] = value
	}
	copy
}
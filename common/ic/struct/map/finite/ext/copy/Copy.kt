package ic.struct.map.finite.ext.copy


import ic.struct.map.finite.FiniteMap


@Suppress("NOTHING_TO_INLINE")
inline fun
	<Key, Value: Any>
	FiniteMap<Key, Value>.copy () : FiniteMap<Key, Value>
	= copyToEditableMap()
	.apply {
		// TODO freeze()
	}
;


package ic.struct.map.finite.ext


import ic.base.arrays.ext.breakableForEach
import ic.struct.map.finite.FiniteMap


fun <Key, Value: Any> FiniteMap<Key, Value>.copySet (
	key: Key, value: Value?
) : FiniteMap<Key, Value> {
	val editableMap = copyToEditableMap()
	editableMap[key] = value
	return editableMap
}

fun <Key, Value: Any> FiniteMap<Key, Value>.copySet (
	vararg keyValuePairs: Pair<Key, Value>
) : FiniteMap<Key, Value> {
	val editableMap = copyToEditableMap()
	keyValuePairs.breakableForEach { (key, value) -> editableMap[key] = value }
	return editableMap
}
package ic.struct.map.finite.ext


import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.empty.EmptyFiniteMap


@Suppress("NOTHING_TO_INLINE")
inline fun <Key, Value: Any> FiniteMap.Companion.Empty() : FiniteMap<Key, Value> {

	@Suppress("UNCHECKED_CAST")
	return EmptyFiniteMap as FiniteMap<Key, Value>

}
package ic.struct.map.finite.ext


import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.FiniteMap


fun <Key, Value: Any> FiniteMap<Key, Value>.toMutableMap() : MutableMap<Key, Value> {

	val mutableMap = mutableMapOf<Key, Value>()

	keys.breakableForEach { key ->
		mutableMap[key] = this[key] ?: return@breakableForEach
	}

	return mutableMap

}
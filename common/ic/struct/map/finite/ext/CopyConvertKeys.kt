package ic.struct.map.finite.ext


import ic.base.escape.skip.skippable
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap


fun
	<OldKey, NewKey, Value: Any>
	FiniteMap<OldKey, Value>.copyConvertKeys (
		convertKeyOrSkip : (OldKey) -> NewKey
	)
	: FiniteMap<NewKey, Value>
{
	val editableMap = EditableMap<NewKey, Value>()
	synchronizedIfMutable {
		keys.breakableForEach { oldKey ->
			skippable {
				val newKey = convertKeyOrSkip(oldKey)
				editableMap[newKey] = this[oldKey]
			}
		}
	}
	return editableMap
}
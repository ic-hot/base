package ic.struct.map.finite.ext


import ic.struct.collection.Collection
import ic.struct.collection.mapvalues.MapValues
import ic.struct.map.finite.FiniteMap


val <Value: Any> FiniteMap<*, Value>.values : Collection<Value> get() {

	return MapValues(this)

}
package ic.struct.map.finite.ext.reduce.minmax


import ic.base.compare.Comparison
import ic.base.throwables.EmptyException
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach


@Throws(EmptyException::class)
inline fun <Key, Value: Any> FiniteMap<Key, Value>.findMinKeyOrThrowEmpty (

	crossinline compare : (aKey: Key, aValue: Value, bKey: Key, bValue: Value) -> Comparison

) : Key {

	         var minKey   : Key? = null
	lateinit var minValue : Value

	var isAtLeastOne : Boolean = false

	forEach { key, value ->

		if (isAtLeastOne) {

			minKey   = key
			minValue = value

			isAtLeastOne = true

		} else {

			@Suppress("UNCHECKED_CAST")
			if (compare(key, value, minKey as Key, minValue) < 0) {
				minKey   = key
				minValue = value
			}

		}

	}

	if (isAtLeastOne) {

		@Suppress("UNCHECKED_CAST")
		return minKey as Key

	} else {

		throw EmptyException

	}

}


inline fun <Key, Value: Any> FiniteMap<Key, Value>.findMinKey (
	crossinline compare : (aKey: Key, aValue: Value, bKey: Key, bValue: Value) -> Comparison
) : Key {
	try {
		return findMinKeyOrThrowEmpty(compare)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
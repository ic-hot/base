package ic.struct.map.finite.ext.reduce.minmax


import ic.base.compare.Comparison
import ic.base.throwables.EmptyException
import ic.base.tuples.funs.Tuple
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.nonBreakableForEach


@Throws(EmptyException::class)
inline fun <Key, Value: Any> FiniteMap<Key, Value>.findMinOrThrowEmpty (

	crossinline compare : (aKey: Key, aValue: Value, bKey: Key, bValue: Value) -> Comparison

) : Pair<Key, Value> {

	         var minKey   : Key? = null
	lateinit var minValue : Value

	var isAtLeastOne : Boolean = false

	nonBreakableForEach { key, value ->

		if (isAtLeastOne) {

			minKey   = key
			minValue = value

			isAtLeastOne = true

		} else {

			@Suppress("UNCHECKED_CAST")
			if (compare(key, value, minKey as Key, minValue) < 0) {
				minKey   = key
				minValue = value
			}

		}

	}

	if (isAtLeastOne) {

		@Suppress("UNCHECKED_CAST")
		return Tuple(minKey as Key, minValue)

	} else {

		throw EmptyException

	}

}


inline fun <Key, Value: Any> FiniteMap<Key, Value>.findMin (
	crossinline compare : (aKey: Key, aValue: Value, bKey: Key, bValue: Value) -> Comparison
) : Pair<Key, Value> {
	try {
		return findMinOrThrowEmpty(compare)
	} catch (_: EmptyException) {
		throw RuntimeException()
	}
}
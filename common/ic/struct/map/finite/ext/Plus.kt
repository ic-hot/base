package ic.struct.map.finite.ext


import ic.ifaces.hascount.ext.isEmpty
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.foreach.forEach


operator fun <Key, Value: Any>
	FiniteMap<Key, Value>.plus (other: FiniteMap<Key, Value>) : FiniteMap<Key, Value>
{
	if (this.isEmpty) return other
	if (other.isEmpty) return this
	val newMap = EditableMap<Key, Value>()
	this .forEach { key, value -> newMap[key] = value }
	other.forEach { key, value -> newMap[key] = value }
	return newMap
}


operator fun <Key, Value: Any>
	FiniteMap<Key, Value>.plus (keyValuePair: Pair<Key, Value?>) : FiniteMap<Key, Value>
{
	val (newKey, newValue) = keyValuePair
	if (newValue == null) {
		return this
	} else {
		val newMap = EditableMap<Key, Value>()
		forEach { key, value -> newMap[key] = value }
		newMap[newKey] = newValue
		return newMap
	}
}
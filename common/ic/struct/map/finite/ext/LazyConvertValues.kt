package ic.struct.map.finite.ext


import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.convertvalues.ConvertValuesFiniteMap


inline fun <Key, Value: Any, NewValue: Any> FiniteMap<Key, Value>.lazyConvertValues (

	crossinline convertValue : (Value) -> NewValue

) : FiniteMap<Key, NewValue> {

	return ConvertValuesFiniteMap(
		sourceFiniteMap = this,
		convertValue = convertValue
	)

}
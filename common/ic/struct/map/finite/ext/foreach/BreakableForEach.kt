package ic.struct.map.finite.ext.foreach


import ic.base.kfunctions.DoNothing
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.FiniteMap


@Deprecated("To remove")
inline fun <Key, Value: Any> FiniteMap<Key, Value>.breakableForEach (

	noinline onBreak: () -> Unit = DoNothing,

	crossinline action : (key: Key, value: Value) -> Unit

) {

	keys.breakableForEach(onBreak = onBreak) { key ->

		val value = this[key]!!

		action(key, value)

	}

}
package ic.struct.map.finite.ext.foreach


import ic.struct.collection.ext.foreach.forEach
import ic.struct.map.finite.FiniteMap


inline fun <Key, Value: Any> FiniteMap<Key, Value>.forEach (

	crossinline action : (key: Key, value: Value) -> Unit

) {

	keys.forEach { key ->

		val value = this[key]!!

		action(key, value)

	}

}


@Deprecated("Use forEach")
inline fun <Key, Value: Any> FiniteMap<Key, Value>.nonBreakableForEach (
	crossinline action : (key: Key, value: Value) -> Unit
) {
	forEach(action = action)
}
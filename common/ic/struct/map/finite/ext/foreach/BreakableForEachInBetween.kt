package ic.struct.map.finite.ext.foreach


import ic.base.kfunctions.DoNothing
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.map.finite.FiniteMap


@Deprecated("To remove")
inline fun <Key, Value: Any> FiniteMap<Key, Value>.breakableForEach (

	noinline onBreak: () -> Unit = DoNothing,

	crossinline doInBetween : () -> Unit,

	crossinline action : (key: Key, value: Value) -> Unit

) {

	var atLeastOne : Boolean = false

	keys.breakableForEach(onBreak = onBreak) { key ->

		if (atLeastOne) {
			doInBetween()
		} else {
			atLeastOne = true
		}

		val value = this[key]!!

		action(key, value)

	}

}
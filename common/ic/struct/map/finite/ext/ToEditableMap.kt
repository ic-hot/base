package ic.struct.map.finite.ext


import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.foreach.breakableForEach


fun <Key, Value: Any> FiniteMap<Key, Value>.copyToEditableMap() : EditableMap<Key, Value> {

	val editableMap = EditableMap<Key, Value>()

	breakableForEach { key, value ->
		editableMap[key] = value
	}

	return editableMap

}


@Suppress("NOTHING_TO_INLINE")
inline fun <Key, Value: Any>
	FiniteMap<Key, Value>.toEditableMapIfNotAlready() : EditableMap<Key, Value>
{

	if (this is EditableMap) {

		return this

	} else {

		return copyToEditableMap()

	}

}
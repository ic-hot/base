@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.map.finite


import ic.base.escape.skip.skippable
import ic.ifaces.getter.getter1.Getter1
import ic.struct.map.editable.EditableMap
import ic.struct.map.finite.ext.Empty


inline fun <Key, Value: Any> FiniteMap() : FiniteMap<Key, Value> = FiniteMap.Empty()


inline fun <Key, Value: Any>
	FiniteMap (
		build: EditableMap<Key, Value>.() -> Unit
	) : FiniteMap<Key, Value>
{
	return EditableMap<Key, Value>().apply {
		build()
		// TODO freeze()
	}
}


fun <Key, Value: Any> FiniteMap (
	vararg keyValuePairs : Pair<Key, Value?>
) : FiniteMap<Key, Value> {
	return EditableMap<Key, Value>().apply {
		keyValuePairs.forEach { (key, value) ->
			this[key] = value
		}
	}
}

fun <Key, Value: Any> FiniteMap (
	keys : Iterable<Key>,
	keyToValue : Getter1<Value, Key>
) : FiniteMap<Key, Value> {
	return EditableMap<Key, Value>().apply {
		keys.forEach { key ->
			skippable {
				this[key] = keyToValue.get(key)
			}
		}
	}
}

inline fun <Key, Value: Any> FiniteMap (
	keys : Iterable<Key>,
	crossinline keyToValue : (Key) -> Value
) : FiniteMap<Key, Value> {
	return EditableMap<Key, Value>().apply {
		keys.forEach { key ->
			skippable {
				this[key] = keyToValue(key)
			}
		}
	}
}


inline fun <Generator, Key, Value: Any> FiniteMap (
	generators : Iterable<Generator>,
	crossinline generateKey : (Generator) -> Key,
	crossinline generateValue : (Generator, Key) -> Value
) : FiniteMap<Key, Value> {
	return EditableMap<Key, Value>().apply {
		generators.forEach { generator ->
			skippable {
				val key = generateKey(generator)
				this[key] = generateValue(generator, key)
			}
		}
	}
}
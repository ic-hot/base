package ic.struct.map.finite


import ic.ifaces.hascount.HasCount
import ic.struct.collection.Collection
import ic.struct.map.incomplete.IncompleteMap
import ic.struct.set.finite.FiniteSet


interface FiniteMap<Key, Value: Any>
	: IncompleteMap<Key, Value>, HasCount
	, Collection<Pair<Key, Value>>
{


	override val keys : FiniteSet<Key>


	companion object


}

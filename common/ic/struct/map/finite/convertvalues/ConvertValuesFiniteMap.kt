package ic.struct.map.finite.convertvalues


import ic.struct.map.finite.BaseFiniteMap
import ic.struct.map.finite.FiniteMap


abstract class ConvertValuesFiniteMap<Key, Value: Any, SourceValue: Any>
	: BaseFiniteMap<Key, Value>()
{


	protected abstract val sourceFiniteMap : FiniteMap<Key, SourceValue>

	protected abstract fun convertValue (sourceValue: SourceValue) : Value


	override val keys get() = sourceFiniteMap.keys

	override val count get() = sourceFiniteMap.count


	override fun get (key: Key) : Value? {
		val sourceValue = sourceFiniteMap[key]
		if (sourceValue == null) return null
		return convertValue(sourceValue)
	}


}
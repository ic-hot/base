@file:Suppress("FunctionName")


package ic.struct.map.finite.convertvalues


import ic.struct.map.finite.FiniteMap


inline fun <Key, Value: Any, SourceValue: Any> ConvertValuesFiniteMap (

	sourceFiniteMap : FiniteMap<Key, SourceValue>,

	crossinline convertValue : (SourceValue) -> Value

) : ConvertValuesFiniteMap<Key, Value, SourceValue> {

	return object : ConvertValuesFiniteMap<Key, Value, SourceValue>() {

		override val sourceFiniteMap get() = sourceFiniteMap

		override fun convertValue (sourceValue: SourceValue) = convertValue(sourceValue)

	}

}
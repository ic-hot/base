package ic.struct.map.finite.union


import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.struct.collection.Collection
import ic.struct.collection.convert.ConvertCollection
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.foreach.forEach
import ic.struct.map.finite.BaseFiniteMap
import ic.struct.map.finite.FiniteMap
import ic.struct.set.finite.union.UnionFiniteSet


abstract class UnionFiniteMap<Key, Value: Any> : BaseFiniteMap<Key, Value>() {


	protected abstract val children : Collection<FiniteMap<Key, Value>>


	override val keys = UnionFiniteSet(
		children = ConvertCollection({ children }) { it.keys }
	)

	override val count get() = keys.count


	override fun get (key: Key) : Value? {
		var result : Value? = null
		skippable {
			children.forEach { child ->
				val value = child[key]
				if (value != null) {
					result = value
					skip
				}
			}
		}
		return result
	}


}
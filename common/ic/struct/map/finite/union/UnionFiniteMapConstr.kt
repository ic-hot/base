@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.map.finite.union


import ic.struct.collection.Collection
import ic.struct.list.fromarray.ListFromArray
import ic.struct.map.finite.FiniteMap


inline fun <Key, Value: Any> UnionFiniteMap (

	crossinline getChildren : () -> Collection<FiniteMap<Key, Value>>

) : UnionFiniteMap<Key, Value> {

	return object : UnionFiniteMap<Key, Value>() {

		override val children get() = getChildren()

	}

}


inline fun <Key, Value: Any> UnionFiniteMap (

	children : Collection<FiniteMap<Key, Value>>

) = UnionFiniteMap(
	getChildren = { children }
)


inline fun <Key, Value: Any> UnionFiniteMap (

	vararg children : FiniteMap<Key, Value>

) = UnionFiniteMap(
	children = ListFromArray<FiniteMap<Key, Value>>(children, isArrayImmutable = true)
)
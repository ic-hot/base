package ic.struct.map.finite


import ic.base.primitives.int32.Int32
import ic.base.strings.String
import ic.funs.effect.Effect
import ic.funs.effect.andDoInBetween
import ic.funs.effector.find.all
import ic.funs.effector.sum.sum
import ic.funs.fun0.Fun0
import ic.struct.collection.ext.foreach.forEach
import ic.struct.map.finite.ext.foreach.forEach
import ic.struct.set.finite.BaseFiniteSet


abstract class BaseFiniteMap<Key, Value: Any>
	: BaseFiniteSet<Pair<Key, Value>>()
	, FiniteMap<Key, Value>
{


	// BaseFiniteSet impl:

	override fun invoke (effect: Effect<Pair<Key, Value>>) {
		forEach { key, value ->
			effect(Pair(key, value))
		}
	}

	override fun contains (item: Pair<Key, Value>) : Boolean {
		return this[item.first] == item.second
	}


	override fun equals (other: Any?) : Boolean {
		if (other is FiniteMap<*, *>) {
			@Suppress("UNCHECKED_CAST")
			other as FiniteMap<Key, Value>
			val thisKeys = this.keys
			val otherKeys = other.keys
			if (thisKeys == otherKeys) {
				return thisKeys.all { key ->
					this[key] == other[key]
				}
			} else {
				return false
			}
		} else {
			return false
		}
	}

	override fun hashCode() : Int32 {
		return keys.sum { it.hashCode() }
	}


	override fun toString() : String {
		return String {
			write("[ ")
			keys.forEach(
				{ key : Key ->
					val value = get(key)
					write("$key: $value")
				}
				.andDoInBetween {
					write(", ")
				}
			)
			write(" ]")
		}
	}


}
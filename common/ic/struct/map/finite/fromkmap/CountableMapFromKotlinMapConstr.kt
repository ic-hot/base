@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.map.finite.fromkmap


inline fun <Key, Value: Any> FiniteMapFromKotlinMap (

	kotlinMap : kotlin.collections.Map<Key, Value>

) : FiniteMapFromKotlinMap<Key, Value> {

	return object : FiniteMapFromKotlinMap<Key, Value>() {

		override val kotlinMap get() = kotlinMap

	}

}
package ic.struct.map.finite.fromkmap


import ic.base.primitives.int32.ext.asInt64
import ic.struct.map.finite.BaseFiniteMap
import ic.struct.set.finite.FiniteSet
import ic.struct.set.finite.fromkset.FiniteSetFromKotlinSet


abstract class FiniteMapFromKotlinMap<Key, Value: Any> : BaseFiniteMap<Key, Value>() {


	protected abstract val kotlinMap : kotlin.collections.Map<Key, Value>


	override val keys : FiniteSet<Key> get() = FiniteSetFromKotlinSet(kotlinMap.keys)

	override val count get() = kotlinMap.size.asInt64


	override fun get (key: Key) = kotlinMap[key]


}
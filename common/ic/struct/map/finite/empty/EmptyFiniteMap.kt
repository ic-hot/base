package ic.struct.map.finite.empty


import ic.struct.map.finite.BaseFiniteMap
import ic.struct.set.finite.empty.EmptyFiniteSet


object EmptyFiniteMap : BaseFiniteMap<Any?, Nothing>() {

	override val keys get() = EmptyFiniteSet

	override val count get() = 0L

	override fun get (key: Any?) = null

}
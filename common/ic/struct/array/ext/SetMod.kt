package ic.struct.array.ext


import ic.base.primitives.int64.Int64
import ic.math.funs.mod
import ic.struct.array.Array


fun <Item> Array<Item>.setMod (index: Int64, item: Item) {
	return set(index mod length, item)
}
package ic.struct.array.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException
import ic.struct.array.Array


operator fun <Item> Array<Item>.set (index: Int64, value: Item) {
	try {
		setOrThrow(index, value)
	} catch (_: IndexOutOfBoundsException) {
		throw RuntimeException("length: $length, index: $index")
	}
}


operator fun <Item> Array<Item>.set (index: Int32, value: Item) {
	try {
		setOrThrow(index.asInt64, value)
	} catch (_: IndexOutOfBoundsException) {
		throw RuntimeException("length: $length, index: $index")
	}
}
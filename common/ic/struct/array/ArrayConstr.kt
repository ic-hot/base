@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.array


import ic.base.primitives.int64.Int64


inline fun <Item> Array (length: Int64) : Array<Item> {

	return DefaultArray(length = length)

}
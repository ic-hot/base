package ic.struct.array


import ic.base.annotations.Valid
import ic.base.primitives.int64.Int64
import ic.ifaces.immut.FrozenException
import ic.math.annotations.PositiveOrZero
import ic.struct.list.BaseList


abstract class BaseArray<Item> : BaseList<Item>(), Array<Item> {


	final override val isListImmutable get() = isFrozen


	final override var isFrozen : Boolean = false; private set

	override fun freeze() {
		if (isFrozen) throw FrozenException()
		isFrozen = true
	}


	protected abstract fun implementSet (@Valid @PositiveOrZero index: Int64, item: Item)


	override fun setOrThrow (index: Int64, value: Item) {
		if (isFrozen) throw FrozenException()
		throwIndexOutOfBoundsIfIndexIsInvalid(index)
		implementSet(index, value)
	}


}
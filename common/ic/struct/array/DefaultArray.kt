package ic.struct.array


import ic.base.arrays.ext.length
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.set
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


class DefaultArray<Item> (

	private val items : kotlin.Array<Any?>

) : BaseArray<Item>() {


	override val length get() = items.length.asInt64

	override fun implementGet (index: Int64) : Item {
		@Suppress("UNCHECKED_CAST")
		return items[index] as Item
	}

	override fun implementSet (index: Int64, item: Item) = items.set(index, item)


	constructor (length: Int64) : this (
		items = ic.base.arrays.Array(length)
	)

	constructor (length: Int32) : this (length.asInt64)


}
package ic.struct.array


import ic.base.primitives.int64.Int64
import ic.base.throwables.IndexOutOfBoundsException
import ic.ifaces.immut.Freezable
import ic.struct.list.List


interface Array<Item> : List<Item>, Freezable {


	@Throws(IndexOutOfBoundsException::class)
	fun setOrThrow (index: Int64, value: Item)


}

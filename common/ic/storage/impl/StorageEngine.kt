package ic.storage.impl


import ic.storage.fs.local.impl.LocalFsEngine


internal interface StorageEngine {


	val localFsEngine : LocalFsEngine


}
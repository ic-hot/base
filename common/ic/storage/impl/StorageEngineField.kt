package ic.storage.impl


import ic.base.platform.basePlatform


internal inline val storageEngine get() = basePlatform.storageEngine
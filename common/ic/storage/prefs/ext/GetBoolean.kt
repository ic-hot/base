package ic.storage.prefs.ext


import ic.storage.prefs.Prefs


@Suppress("NOTHING_TO_INLINE")
inline fun Prefs.getBoolean (key: String, or: Boolean) = getBooleanOrNull(key) ?: or

inline fun Prefs.getBoolean (key: String, or: () -> Boolean) = getBooleanOrNull(key) ?: or()
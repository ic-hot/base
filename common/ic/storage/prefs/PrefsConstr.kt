package ic.storage.prefs


import ic.app.impl.appEngine


fun Prefs (prefsName: String) : Prefs {

	return appEngine.createPrefs(prefsName = prefsName)

}
package ic.storage.prefs


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


interface Prefs {

	operator fun set (key: String, value: Any?)

	fun getBooleanOrNull (key: String) : Boolean?

	fun getStringOrNull (key: String) : String?

	fun getInt32OrNull (key: String) : Int32?

	fun getInt64OrNull (key: String) : Int64?

	fun getFloat64OrNull (key: String) : Float64?


}
package ic.storage.fs.local.impl


import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.File
import ic.storage.fs.FsEntry


interface LocalFsEngine {


	@Throws(NotExistsException::class)
	fun getEntryOrThrowNotExists (path: String) : FsEntry


	@Throws(AlreadyExistsException::class)
	fun createFileOrThrowAlreadyExists (path: String) : File


	@Throws(AlreadyExistsException::class)
	fun createFolderOrThrowAlreadyExists (path: String) : Directory


}
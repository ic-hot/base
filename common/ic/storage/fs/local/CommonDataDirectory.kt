package ic.storage.fs.local


import ic.app.impl.appEngine
import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


val commonDataDirectory by Cached {

	appEngine.initCommonDataDirectory()

}
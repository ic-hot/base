package ic.storage.fs.local.ext


import ic.base.throwables.AlreadyExistsException
import ic.storage.fs.Directory
import ic.storage.fs.FsEntry


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.createSymbolicLink (name: String, linkPath: String) {
	try {
		createSymbolicLinkOrThrowAlreadyExists(name = name, linkPath = linkPath)
	} catch (t: AlreadyExistsException) {
		throw AlreadyExistsException.Runtime()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.createSymbolicLinkIfExists (name: String, linkPath: String) {
	if (!FsEntry.exists(this, linkPath)) return
	createSymbolicLink(name = name, linkPath = linkPath)
}
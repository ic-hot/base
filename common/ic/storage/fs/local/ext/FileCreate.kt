package ic.storage.fs.local.ext


import ic.base.platform.basePlatform
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.WrongTypeException
import ic.storage.fs.Directory
import ic.storage.fs.File


@Throws(AlreadyExistsException::class)
fun File.Companion.createOrThrowAlreadyExists (path: String) : File {
	try {
		return basePlatform.storageEngine.localFsEngine.createFileOrThrowAlreadyExists(path)
	} catch (_: WrongTypeException) {
		throw RuntimeException("path: $path")
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun File.Companion.create (path: String) : File {
	try {
		return createOrThrowAlreadyExists(path = path)
	} catch (_: AlreadyExistsException) {
		throw RuntimeException("path: $path")
	}
}


@Throws(AlreadyExistsException::class)
fun File.Companion.createOrThrowAlreadyExists (
	baseDirectory: Directory, relativePath: String
) : File {
	return createOrThrowAlreadyExists(
		path = baseDirectory.absolutizePath(relativePath)
	)
}


@Suppress("NOTHING_TO_INLINE")
inline fun File.Companion.create (baseDirectory: Directory, relativePath: String) : File {
	try {
		return createOrThrowAlreadyExists(
			baseDirectory = baseDirectory, relativePath = relativePath
		)
	} catch (_: AlreadyExistsException) {
		throw RuntimeException("baseDirectory: $baseDirectory, relativePath: $relativePath")
	}
}
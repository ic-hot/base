package ic.storage.fs.local.ext


import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.FsEntry


fun FsEntry.Companion.exists (path: String) : Boolean {
	try {
		getExistingOrThrow(path)
		return true
	} catch (_: NotExistsException) {
		return false
	}
}


fun FsEntry.Companion.exists (baseDirectory: Directory, relativePath: String) : Boolean {
	return exists(
		path = baseDirectory.absolutizePath(relativePath)
	)
}
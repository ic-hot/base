package ic.storage.fs.local.ext


import ic.base.platform.basePlatform
import ic.base.throwables.AlreadyExistsException
import ic.storage.fs.Directory


@Throws(AlreadyExistsException::class)
fun Directory.Companion.createOrThrowAlreadyExists (path: String) : Directory {
	return basePlatform.storageEngine.localFsEngine.createFolderOrThrowAlreadyExists(path)
}

@Throws(AlreadyExistsException::class)
fun Directory.Companion.createOrThrowAlreadyExists (
	baseDirectory : Directory, relativePath : String
) : Directory {
	return createOrThrowAlreadyExists(
		path = baseDirectory.absolutizePath(relativePath)
	)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.Companion.create (path: String) : Directory {
	try {
		return createOrThrowAlreadyExists(path)
	} catch (_: AlreadyExistsException) {
		throw RuntimeException("path: $path")
	}
}

fun Directory.Companion.create (baseDirectory: Directory, relativePath: String) : Directory {
	return create(
		path = baseDirectory.absolutizePath(relativePath)
	)
}


fun Directory.Companion.createIfNotExists (path: String) : Directory {
	return getExistingOrNull(path) ?: create(path)
}

fun Directory.Companion.createIfNotExists (baseDirectory: Directory, relativePath: String) : Directory {
	return Directory.createIfNotExists(
		path = baseDirectory.absolutizePath(relativePath)
	)
}
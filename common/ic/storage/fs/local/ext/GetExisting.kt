package ic.storage.fs.local.ext


import ic.base.platform.basePlatform
import ic.base.throwables.NotExistsException
import ic.storage.fs.File
import ic.storage.fs.Directory
import ic.storage.fs.FsEntry
import ic.storage.fs.ext.recursive.recursiveGetEntry


@Throws(NotExistsException::class)
fun FsEntry.Companion.getExistingOrThrow (path: String) : FsEntry {
	return basePlatform.storageEngine.localFsEngine.getEntryOrThrowNotExists(path)
}

@Throws(NotExistsException::class)
fun File.Companion.getExistingOrThrow (path: String) : File {
	return FsEntry.getExistingOrThrow(path) as File
}

@Throws(NotExistsException::class)
fun Directory.Companion.getExistingOrThrow (path: String) : Directory {
	return FsEntry.getExistingOrThrow(path) as Directory
}


@Throws(NotExistsException::class)
fun FsEntry.Companion.getExistingOrThrow (base: Directory, path: String) : FsEntry {
	return base.recursiveGetEntry(path)
}

@Throws(NotExistsException::class)
fun File.Companion.getExistingOrThrow (base: Directory, path: String) : File {
	return FsEntry.getExistingOrThrow(base, path) as File
}

@Throws(NotExistsException::class)
fun Directory.Companion.getExistingOrThrow (base: Directory, path: String) : Directory {
	return FsEntry.getExistingOrThrow(base, path) as Directory
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.Companion.getExisting (path: String) : Directory {
	try {
		return getExistingOrThrow(path)
	} catch (_: NotExistsException) {
		throw RuntimeException("path: $path")
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun Directory.Companion.getExisting (base: Directory, path: String) : Directory {
	try {
		return getExistingOrThrow(base, path)
	} catch (_: NotExistsException) {
		throw RuntimeException("path: $path")
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun Directory.Companion.getExistingOrNull (path: String) : Directory? {
	try {
		return getExistingOrThrow(path)
	} catch (_: NotExistsException) {
		return null
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun File.Companion.getExisting (base: Directory, path: String) : File {
	try {
		return getExistingOrThrow(base, path)
	} catch (_: NotExistsException) {
		throw RuntimeException("path: $path")
	}
}
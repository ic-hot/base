package ic.storage.fs.local


import ic.app.impl.appEngine


val privateFolder by lazy {

	appEngine.initPrivateFolder()

}
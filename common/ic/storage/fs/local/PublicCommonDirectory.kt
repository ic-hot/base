package ic.storage.fs.local


import ic.app.impl.appEngine


val commonPublicDirectory by lazy {

	appEngine.initCommonPublicFolder()

}
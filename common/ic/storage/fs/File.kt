package ic.storage.fs


import ic.base.primitives.int64.Int64
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput


interface File : FsEntry {


	val length : Int64


	fun openInput() : ByteInput

	fun openOutput() : ByteOutput


	companion object


}
package ic.storage.fs


import ic.base.primitives.int64.Int64
import ic.util.time.Time


interface FsEntry {


	val parent : Directory?

	val name : String

	val modifiedAt : Time

	val absolutePath : String

	val sizeInBytes : Int64

	val freeSpaceInBytes : Int64


	fun remove()


	companion object


}
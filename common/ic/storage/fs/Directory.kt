package ic.storage.fs


import ic.base.annotations.Throws
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.IoException
import ic.base.throwables.NotExistsException
import ic.ifaces.emptiable.Emptiable
import ic.struct.collection.Collection
import ic.struct.set.finite.FiniteSet


interface Directory : FsEntry, Emptiable {


	@Throws(IoException::class)
	fun getItemsNames() : FiniteSet<String>

	@Throws(IoException::class)
	fun getItems() : Collection<FsEntry>

	@Throws(IoException::class, NotExistsException::class)
	fun getItemOrThrowNotExists (name: String) : FsEntry


	@Throws(IoException::class, AlreadyExistsException::class)
	fun createFileOrThrowAlreadyExists (name: String) : File


	@Throws(IoException::class, AlreadyExistsException::class)
	fun createFolderOrThrowAlreadyExists (name: String) : Directory


	@Throws(IoException::class)
	fun copyHereReplacing (name: String, item: FsEntry)

	@Throws(IoException::class)
	fun moveHereReplacing (name: String, item: FsEntry)


	fun absolutizePath (relativePath: String) : String
	fun relativizePath (absolutePath: String) : String


	@Throws(AlreadyExistsException::class)
	fun createSymbolicLinkOrThrowAlreadyExists (name: String, linkPath: String)


	companion object


}
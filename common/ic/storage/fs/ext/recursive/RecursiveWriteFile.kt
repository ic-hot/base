package ic.storage.fs.ext.recursive


import ic.base.annotations.NotEmpty
import ic.base.assert.assert
import ic.storage.fs.Directory
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.ext.writeFileReplacing
import ic.stream.output.ByteOutput
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEachIndexed
import ic.struct.list.ext.length.isNotEmpty


inline fun Directory.recursiveWriteFileReplacing (
	@NotEmpty path : List<String>,
	write : ByteOutput.() -> Unit
) {
	assert { path.isNotEmpty }
	var directory : Directory = this
	path.breakableForEachIndexed { index, name ->
		if (index < path.length - 1) {
			directory = directory.createFolderIfNotExists(name)
		} else {
			directory.writeFileReplacing(name = name, write = write)
		}
	}
}
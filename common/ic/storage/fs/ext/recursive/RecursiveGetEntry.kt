package ic.storage.fs.ext.recursive


import ic.base.primitives.character.Character
import ic.base.strings.ext.split
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.File
import ic.storage.fs.FsEntry
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


@Throws(NotExistsException::class)
fun Directory.recursiveGetEntryOrThrowNotExists (path: List<String>) : FsEntry {
	var entry : FsEntry = this
	path.forEach { name ->
		val directory = entry as? Directory ?: throw NotExistsException
		entry = directory.getItemOrThrowNotExists(name)
	}
	return entry
}

fun Directory.recursiveGetEntryOrNull (path: List<String>) : FsEntry? {
	try {
		return recursiveGetEntryOrThrowNotExists(path = path)
	} catch (t: NotExistsException) {
		return null
	}
}

fun Directory.recursiveGetEntry (path: List<String>) : FsEntry {
	try {
		return recursiveGetEntryOrThrowNotExists(path = path)
	} catch (t: NotExistsException) {
		throw RuntimeException()
	}
}


@Throws(NotExistsException::class)
fun Directory.recursiveGetEntryOrThrowNotExists (
	path : String, separator : Character = '/'
) : FsEntry {
	return recursiveGetEntryOrThrowNotExists(
		path = path.split(separator = separator)
	)
}

@Throws(NotExistsException::class)
fun Directory.recursiveGetEntry (
	path : String, separator : Character = '/'
) : FsEntry {
	try {
		return recursiveGetEntryOrThrowNotExists(
			path = path.split(separator = separator)
		)
	} catch (t: NotExistsException) {
		throw RuntimeException()
	}
}


@Throws(NotExistsException::class)
fun Directory.recursiveGetFileOrThrowNotExists (
	path : String, separator : Character = '/'
) : File {
	return recursiveGetEntryOrThrowNotExists(path = path, separator = separator)
	as File
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.recursiveGetFile (
	path : String, separator : Character = '/'
) : File {
	try {
		return recursiveGetFileOrThrowNotExists(path = path, separator = separator)
	} catch (t: NotExistsException) {
		throw RuntimeException()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.recursiveGetFileOrNull (
	path : String, separator : Character = '/'
) : File? {
	try {
		return recursiveGetFileOrThrowNotExists(path = path, separator = separator)
	} catch (t: NotExistsException) {
		return null
	}
}
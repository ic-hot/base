package ic.storage.fs.ext.recursive


import ic.storage.fs.Directory
import ic.struct.list.List
import ic.struct.list.editable.EditableList


fun Directory.recursiveGetAllRelativePaths (
	toIncludeFiles   : Boolean = true,
	toIncludeFolders : Boolean = true
) : List<String> {
	val paths = EditableList<String>()
	recursiveForEachRelativePath(
		toIncludeFiles = toIncludeFiles,
		toIncludeFolders = toIncludeFolders
	) { path ->
		paths.add(path)
	}
	return paths
}
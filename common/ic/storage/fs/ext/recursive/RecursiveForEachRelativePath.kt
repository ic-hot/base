package ic.storage.fs.ext.recursive


import ic.storage.fs.Directory
import ic.storage.fs.ext.getRelativePath


inline fun Directory.recursiveForEachRelativePath (

	toIncludeFiles   : Boolean = true,
	toIncludeFolders : Boolean = true,

	crossinline action : (path: String) -> Unit

) {

	recursiveForEachEntry { entry ->
		if (
			(toIncludeFolders && entry is Directory) ||
			(toIncludeFiles && entry !is Directory)
		)
		action(
			getRelativePath(of = entry)
		)
	}

}
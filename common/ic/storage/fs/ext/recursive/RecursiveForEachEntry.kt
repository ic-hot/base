package ic.storage.fs.ext.recursive


import ic.storage.fs.Directory
import ic.storage.fs.FsEntry
import ic.struct.collection.ext.foreach.forEach


fun Directory.recursiveForEachEntry (action: (FsEntry) -> Unit) {

	getItems().forEach { item ->

		when (item) {

			is Directory -> {
				action(item)
				item.recursiveForEachEntry(action)
			}

			else -> action(item)

		}

	}

}
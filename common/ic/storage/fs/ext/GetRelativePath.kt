package ic.storage.fs.ext


import ic.storage.fs.Directory
import ic.storage.fs.FsEntry


fun Directory.getRelativePath (of: FsEntry) : String {

	return relativizePath(absolutePath = of.absolutePath)

}
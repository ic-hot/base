package ic.storage.fs.ext


import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.File
import ic.stream.output.ByteOutput
import ic.stream.sequence.ByteSequence


fun File.write (bytes: ByteSequence) {

	val output = openOutput()

	try {

		output.write(bytes)
		output.close()

	} catch (t: Throwable) {
		output.cancel()
		throw t
	}

}


inline fun File.write (action: ByteOutput.() -> Unit) {
	openOutput().runAndCloseOrCancel(action)
}
package ic.storage.fs.ext


import ic.base.throwables.IoException
import ic.base.throwables.NotExistsException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.Directory
import ic.stream.input.ByteInput


@Throws(NotExistsException::class, IoException::class)
inline fun <Result> Directory.readFileOrThrowNotExists (
	name : String,
	read : ByteInput.() -> Result
) : Result {

	return getFileOrThrow(name).openInput().runAndCloseOrCancel(read)

}


@Throws(IoException::class)
inline fun <Result> Directory.readFileOrNull (
	name : String,
	read : ByteInput.() -> Result
) : Result? {
	try {
		return readFileOrThrowNotExists(name, read)
	} catch (_: NotExistsException) {
		return null
	}
}
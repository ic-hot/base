package ic.storage.fs.ext


import ic.base.throwables.IoException
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.stream.sequence.ByteSequence


@Suppress("NOTHING_TO_INLINE")
@Throws(NotExistsException::class, IoException::class)
inline fun Directory.readFileOrThrowNotExists (name: String) : ByteSequence {
	return getFileOrThrow(name).read()
}


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun Directory.readFileOrNull (name: String) : ByteSequence? {
	try {
		return readFileOrThrowNotExists(name)
	} catch (t: NotExistsException) {
		return null
	}
}
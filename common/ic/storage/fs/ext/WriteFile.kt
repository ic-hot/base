package ic.storage.fs.ext


import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.IoException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.Directory
import ic.stream.output.ByteOutput
import ic.stream.output.ext.write
import ic.stream.sequence.ByteSequence


@Throws(IoException::class, AlreadyExistsException::class)
inline fun Directory.writeFileOrThrowAlreadyExists (name: String, write: ByteOutput.() -> Unit) {
	createFileOrThrowAlreadyExists(name).openOutput().runAndCloseOrCancel(write)
}


@Throws(IoException::class)
inline fun Directory.writeFileIfNotExists (name: String, write: ByteOutput.() -> Unit) {
	try {
		writeFileOrThrowAlreadyExists(name, write)
	} catch (_: AlreadyExistsException) {}
}

@Throws(IoException::class)
fun Directory.writeFileIfNotExists (name: String, bytes: ByteSequence) {
	writeFileIfNotExists(name) { write(bytes) }
}

@Throws(IoException::class)
fun Directory.writeFileIfNotExists (name: String, bytes: ByteArray) {
	writeFileIfNotExists(name) { write(bytes) }
}


@Throws(IoException::class)
inline fun Directory.writeFileReplacing (name: String, write: ByteOutput.() -> Unit) {
	createFileReplacing(name).openOutput().runAndCloseOrCancel(write)
}

@Throws(IoException::class)
fun Directory.writeFileReplacing (name: String, bytes: ByteSequence) {
	writeFileReplacing(name) { write(bytes) }
}
package ic.storage.fs.ext


import ic.base.throwables.IoException
import ic.storage.fs.FsEntry


@Throws(IoException::class)
fun FsEntry.rename (newName: String) {

	parent!!.renameItem(name, newName)

}
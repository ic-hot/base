package ic.storage.fs.ext


import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory


fun Directory.containsItem (name: String) : Boolean {

	try {
		getItemOrThrowNotExists(name)
		return true
	} catch (_: NotExistsException) {
		return false
	}

}
package ic.storage.fs.ext


import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.File
import ic.stream.input.ByteInput


inline fun <Result> File.read (action: ByteInput.() -> Result) : Result {
	return openInput().runAndCloseOrCancel(action)
}
package ic.storage.fs.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skip
import ic.base.throwables.NotExistsException
import ic.base.throwables.WrongTypeException
import ic.storage.fs.Directory


@Suppress("NOTHING_TO_INLINE")
@Throws(NotExistsException::class)
inline fun Directory.getFolderOrThrow (name: String) : Directory {
	val entry = getItemOrThrowNotExists(name)
	if (entry is Directory) {
		return entry
	} else {
		throw WrongTypeException.Runtime(
			message = "${ entry.absolutePath } is not a directory"
		)
	}
}


@Suppress("NOTHING_TO_INLINE")
@Skippable
inline fun Directory.getFolder (name: String) : Directory {
	try {
		return getFolderOrThrow(name)
	} catch (_: NotExistsException) {
		throw RuntimeException(
			"baseFolder: $absolutePath, name: $name"
		)
	}
}


@Suppress("NOTHING_TO_INLINE")
@Skippable
inline fun Directory.getFolderOrSkip (name: String) : Directory {
	try {
		return getFolderOrThrow(name)
	} catch (t: NotExistsException) {
		skip
	}
}
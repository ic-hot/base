package ic.storage.fs.ext


import ic.base.escape.skip.skip
import ic.base.escape.skip.Skippable
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.File


@Suppress("NOTHING_TO_INLINE")
@Throws(NotExistsException::class)
inline fun Directory.getFileOrThrow (name: String) : File {
	return getItemOrThrowNotExists(name) as File
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.getFile (name: String) : File {
	try {
		return getFileOrThrow(name)
	} catch (t: NotExistsException) {
		throw RuntimeException()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.getFileOrNull (name: String) : File? {
	try {
		return getFileOrThrow(name)
	} catch (t: NotExistsException) {
		return null
	}
}


@Suppress("NOTHING_TO_INLINE")
@Skippable
inline fun Directory.getFileOrSkip (name: String) : File {
	try {
		return getFileOrThrow(name)
	} catch (t: NotExistsException) {
		skip
	}
}
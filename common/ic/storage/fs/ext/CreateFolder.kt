package ic.storage.fs.ext


import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.IoException
import ic.storage.fs.Directory


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun Directory.createFolder (name: String) : Directory {
	try {
		return createFolderOrThrowAlreadyExists(name)
	} catch (_: AlreadyExistsException) {
		throw RuntimeException("name: $name")
	}
}


@Suppress("NOTHING_TO_INLINE")
@Throws(IoException::class)
inline fun Directory.createFolderIfNotExists (name: String) : Directory {
	val item = getItemOrNull(name)
	if (item == null) {
		return createFolder(name)
	} else {
		return item as Directory
	}
}
package ic.storage.fs.ext


import ic.base.throwables.IoException
import ic.storage.fs.File
import ic.stream.input.ext.read
import ic.stream.sequence.ByteSequence


@Throws(IoException::class)
fun File.read() : ByteSequence {

	val input = openInput()

	try {

		return input.read()

	} finally {

		input.close()

	}

}
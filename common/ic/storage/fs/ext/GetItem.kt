package ic.storage.fs.ext


import ic.base.escape.skip.skip
import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory
import ic.storage.fs.FsEntry


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.getItem (name: String) : FsEntry {
	try {
		return getItemOrThrowNotExists(name)
	} catch (_: NotExistsException) {
		throw RuntimeException("name: $name")
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.getItemOrNull (name: String) : FsEntry? {
	try {
		return getItemOrThrowNotExists(name)
	} catch (_: NotExistsException) {
		return null
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.getItemOrSkip (name: String) : FsEntry {
	try {
		return getItemOrThrowNotExists(name)
	} catch (_: NotExistsException) {
		skip
	}
}
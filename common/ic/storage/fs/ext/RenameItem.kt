package ic.storage.fs.ext


import ic.storage.fs.Directory


fun Directory.renameItem (oldName: String, newName: String) {

	moveHereReplacing(
		name = newName,
		item = getItem(oldName)
	)

}
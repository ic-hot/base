package ic.storage.fs.ext


import ic.base.throwables.AlreadyExistsException
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.storage.fs.File
import ic.storage.fs.Directory


fun Directory.createFile (name: String) : File {
	try {
		return createFileOrThrowAlreadyExists(name)
	} catch (_: AlreadyExistsException) {
		throw AlreadyExistsException.Runtime()
	}
}


fun Directory.createFileIfNotExists (name: String) : File {
	synchronizedIfMutable {
		val item = getItemOrNull(name)
		if (item == null) {
			return createFile(name)
		} else {
			return item as File
		}
	}
}


fun Directory.createFileReplacing (name: String) : File {
	synchronizedIfMutable {
		val item = getItemOrNull(name)
		when (item) {
			null -> {
				return createFile(name)
			}
			is File -> {
				return item
			}
			else -> {
				item.remove()
				return createFile(name)
			}
		}
	}
}
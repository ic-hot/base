package ic.storage.fs.ext


import ic.base.throwables.NotExistsException
import ic.storage.fs.Directory


@Throws(NotExistsException::class)
fun Directory.removeOrThrowNotExists (name: String) {

	getItemOrThrowNotExists(name).remove()

}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.remove (name: String) {
	try {
		return removeOrThrowNotExists(name)
	} catch (_: NotExistsException) {
		throw RuntimeException("name: $name")
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Directory.removeIfExists (name: String) {
	try {
		return removeOrThrowNotExists(name)
	} catch (_: NotExistsException) {}
}
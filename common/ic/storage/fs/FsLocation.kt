package ic.storage.fs


data class FsLocation (

	val baseDirectory : Directory,

	val relativePath : String

) {

	val absolutePath : String get() = baseDirectory.absolutizePath(relativePath)

}
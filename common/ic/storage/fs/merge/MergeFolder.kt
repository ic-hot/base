package ic.storage.fs.merge


import ic.base.annotations.Throws
import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.throwables.NotExistsException
import ic.base.throwables.NotSupportedException
import ic.storage.fs.File
import ic.storage.fs.FsEntry
import ic.storage.fs.Directory
import ic.storage.fs.ext.getItem
import ic.storage.fs.ext.getItemOrNull
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyUnion
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.length.isEmpty


abstract class MergeFolder : Directory {


	override val absolutePath get() = throw NotImplementedError()


	protected abstract val children : Collection<Directory>


	override fun getItemsNames() = children.copyConvert { it.getItemsNames() }.copyUnion()


	override fun getItems() : Collection<FsEntry> {
		return getItemsNames().copyConvert { getItem(it) }
	}


	@Throws(NotExistsException::class)
	override fun getItemOrThrowNotExists (name: String) : FsEntry {
		val childrenItemsAsDirectories = EditableList<Directory>()
		var childItemAsNonDirectory : FsEntry? = null
		skippable {
			children.forEach { child ->
				val childItem = child.getItemOrNull(name)
				when (childItem) {
					null -> {}
					is Directory -> {
						childrenItemsAsDirectories.add(childItem)
					}
					else -> {
						childItemAsNonDirectory = childItem
						skip
					}
				}
			}
		}
		childItemAsNonDirectory?.let { return it }
		if (childrenItemsAsDirectories.isEmpty) {
			throw NotExistsException
		} else {
			return MergeFolder(childrenItemsAsDirectories)
		}
	}


	override val parent get() = null

	override val name : String get() = throw NotSupportedException.Runtime()

	override val modifiedAt get() = throw NotSupportedException.Runtime()

	override fun remove() {
		throw NotSupportedException.Runtime()
	}


	override fun createFileOrThrowAlreadyExists(name: String): File {
		throw NotSupportedException.Runtime()
	}

	override fun createFolderOrThrowAlreadyExists(name: String): Directory {
		throw NotSupportedException.Runtime()
	}


	override fun copyHereReplacing (name: String, item: FsEntry) {
		throw NotSupportedException.Runtime()
	}

	override fun moveHereReplacing (name: String, item: FsEntry) {
		throw NotSupportedException.Runtime()
	}

	override fun absolutizePath (relativePath: String): String {
		throw NotSupportedException.Runtime()
	}

	override fun relativizePath (absolutePath: String): String {
		throw NotSupportedException.Runtime()
	}

	override fun createSymbolicLinkOrThrowAlreadyExists(name: String, linkPath: String) {
		throw NotSupportedException.Runtime()
	}

	override fun empty() {
		throw NotSupportedException.Runtime()
	}


	override val sizeInBytes get() = throw NotSupportedException.Runtime()

	override val freeSpaceInBytes get() = throw NotSupportedException.Runtime()


}
package ic.storage.fs.merge


import ic.storage.fs.Directory
import ic.struct.collection.Collection


@Suppress("NOTHING_TO_INLINE")
inline fun MergeFolder (

	children : Collection<Directory>

) : MergeFolder {

	return object : MergeFolder() {

		override val children get() = children

	}

}
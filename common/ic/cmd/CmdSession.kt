package ic.cmd


import ic.ifaces.lifecycle.closeable.Closeable
import ic.text.Text


interface CmdSession : Closeable {

	fun executeCommand (command: String) : Text

}
package ic.cmd.bash


fun nohup (command: String) = "nohup bash -c ${ bashSingleQuote(command) } &> /dev/null &"
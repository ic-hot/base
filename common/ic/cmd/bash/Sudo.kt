package ic.cmd.bash


import ic.base.strings.ext.trimmed
import ic.ifaces.lifecycle.closeable.Closeable
import ic.text.Text
import ic.text.ext.copyReplaceAll


class Sudo (

	private val sourceBashSession : BashSession,
	private val getSudoPassword : () -> String

)  : BashSession(), Closeable {


	private var isRoot : Boolean? = null

	private var password : String? = null


	override var workdirPath
		get() = sourceBashSession.workdirPath
		set(value) { sourceBashSession.workdirPath = value }
	;


	override fun implementExecuteScript (script: String) : Text {
		if (isRoot == null) {
			isRoot = sourceBashSession.executeCommand("echo \$USER").toString().trimmed == "root"
		}
		if (isRoot!!) return sourceBashSession.executeScript(script)
		if (password == null) {
			password = getSudoPassword()
		}
		val rawResponse = sourceBashSession.executeScript(
			"echo ${ bashSingleQuote(password!!) } | " +
			"sudo -S -p '$(passwordPrompt)' bash -c ${ bashSingleQuote(script) }"
		)
		return rawResponse.copyReplaceAll("$(passwordPrompt)" to "")
	}


	override fun close() {
		sourceBashSession.close()
	}


}

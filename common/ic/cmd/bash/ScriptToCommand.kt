package ic.cmd.bash


import ic.text.Text


fun bashScriptToCommand (script : Text) = "bash -c ${ bashSingleQuote(script) }"

fun bashScriptToCommand (script : String) = "bash -c ${ bashSingleQuote(script) }"


fun scriptPathToCommand (scriptPath : String) = "bash $scriptPath"
package ic.cmd.bash


import ic.base.strings.ext.asText
import ic.base.strings.ext.replaceAll
import ic.text.Text


fun bashPureSingleQuote (string: String) = (
	"'" +
	string.replaceAll(
		"\\" to "\\\\",
		"\n" to "\\n",
		"'"  to "\\'"
	) +
	"'"
)


@Suppress("NOTHING_TO_INLINE")
inline fun bashPureSingleQuote (text: Text) = bashPureSingleQuote(text.toString()).asText


fun bashSingleQuote (string: String) = "\$" + bashPureSingleQuote(string)


@Suppress("NOTHING_TO_INLINE")
inline fun bashSingleQuote (text: Text) = bashSingleQuote(text.toString()).asText
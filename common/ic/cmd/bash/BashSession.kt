package ic.cmd.bash


import ic.cmd.CmdSession
import ic.text.Text
import ic.text.ext.Empty


abstract class BashSession : CmdSession {


	abstract var workdirPath : String


	protected abstract fun implementExecuteScript (script: String) : Text


	fun executeScript (script: String) : Text {
		return implementExecuteScript(
			"cd $workdirPath; $script"
		)
	}


	override fun executeCommand (command: String) : Text {

		when {

			command.startsWith("cd ") -> {
				val cdPath = command.substring("cd ".length).trim()
				if (
					cdPath.startsWith("/") ||
					cdPath.startsWith("~")
				) {
					workdirPath = cdPath
				} else {
					workdirPath += "/$cdPath"
				}
				return Text.Empty
			}

			command.trim() == "cd" -> {
				workdirPath = "~"
				return Text.Empty
			}

			else -> return executeScript(command)

		}

	}


}
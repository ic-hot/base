package ic.text.tests


import ic.base.assert.assert
import ic.test.Test
import ic.text.TextOutput
import ic.text.Text
import ic.text.ext.FromString


class TextFromStringTest : Test() {


	override fun TextOutput.runTest() {

		val string = "This is valid string."

		val text = Text.FromString(string)

		assert { text.toString() == string }

	}


}
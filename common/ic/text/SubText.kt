package ic.text


import ic.base.throwables.End
import ic.text.input.TextInput


class SubText (val sourceText: Text, val start: Int, val end: Int) : BaseText() {

	override fun newIterator() = object : TextInput {

		val sourceTextIterator = sourceText.newIterator()

		init {
			try {
				sourceTextIterator.skip(start)
			} catch (end : End) {}
		}

		var index = start

		override fun getNextCharacterOrThrowEnd() : Char {
			if (index >= end) throw End
			val char = sourceTextIterator.getNextCharacterOrThrowEnd()
			index++
			return char
		}

	}

}
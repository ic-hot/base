package ic.text.pattern


interface TextPattern {


	sealed class MatchStatus {
		object DoesNotMatch 	: MatchStatus()
		object MatchesBeginning : MatchStatus()
		object MatchesFully 	: MatchStatus()
		object BeginningMatches : MatchStatus()
	}


	fun checkString (string: String) : MatchStatus


}
package ic.text.pattern


import ic.base.primitives.int32.Int32
import ic.text.Text
import ic.text.TextBuffer


fun Text.findOrNull (

	pattern : TextPattern,

	startWithIndex : Int32 = 0

) : SearchInTextResult? {

	val iterator = newIterator()

	iterator.skip(startWithIndex)

	var index : Int32 = 0

	val textBuffer = TextBuffer()

	var result : SearchInTextResult? = null

	while (true) {

		val character = iterator.getNextCharacterOrNull()

		if (character == null) return result

		textBuffer.putChar(character)

		val subString = textBuffer.toString()

		when (
			pattern.checkString(subString)
		) {
			TextPattern.MatchStatus.DoesNotMatch -> {
				result = null
				textBuffer.empty()
			}
			TextPattern.MatchStatus.MatchesBeginning -> {
				result = null
			}
			TextPattern.MatchStatus.MatchesFully -> {
				result = SearchInTextResult(
					startIndex = index,
					subString = subString
				)
			}
			TextPattern.MatchStatus.BeginningMatches -> {
				if (result != null) return result
			}
		}

		index++

	}

}
package ic.text.pattern


import ic.base.primitives.int32.Int32


class SearchInTextResult (

	val startIndex : Int32,

	val subString : String

)
@file:Suppress("FunctionName")


package ic.text.pattern


inline fun TextPattern (crossinline checkString : (String) -> TextPattern.MatchStatus) : TextPattern {

	return object : TextPattern {

		override fun checkString (string: String) = checkString(string)

	}

}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.text


import ic.text.ext.FromString


inline fun Text (string: String) : Text = Text.FromString(string)
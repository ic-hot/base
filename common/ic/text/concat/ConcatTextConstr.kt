package ic.text.concat


import ic.struct.list.List
import ic.struct.sequence.Sequence
import ic.struct.sequence.from.array.SequenceFromArray
import ic.text.Text


@Suppress("NOTHING_TO_INLINE")
inline fun ConcatText (

	parts : Sequence<out Text>,

	separator : String = ""

) : ConcatText {

	return object : ConcatText() {

		override val parts get() = parts

		override val separator get() = separator

	}

}


@Suppress("NOTHING_TO_INLINE")
inline fun ConcatText (

	vararg parts : Text

) = (
	ConcatText(
		parts = SequenceFromArray(parts)
	)
)
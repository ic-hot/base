package ic.text.concat


import ic.struct.sequence.Sequence
import ic.base.throwables.End
import ic.text.BaseText
import ic.text.Text
import ic.text.input.TextInput


abstract class ConcatText : BaseText() {


	protected abstract val parts : Sequence<out Text>

	protected abstract val separator : String


	private sealed class IteratorState {
		object Initial : IteratorState()
		class Part (val partIterator: TextInput) : IteratorState()
		class Divider (val nextPart: Text) : IteratorState() {
			var dividerIndex : Int = 0
		}
	}


	override fun newIterator() = object : TextInput {

		private val partsIterator = parts.newIterator()

		private var state : IteratorState = IteratorState.Initial

		@Throws(End::class)
		override fun getNextCharacterOrThrowEnd() : Char {
			when (val state = this.state) {
				is IteratorState.Initial -> {
					this.state = IteratorState.Part(
						partsIterator.getNextOrThrowEnd().newIterator()
					)
					return getNextCharacterOrThrowEnd()
				}
				is IteratorState.Part -> {
					try {
						return state.partIterator.getNextCharacterOrThrowEnd()
					} catch (endOfPart: End) {
						this.state = IteratorState.Divider(
							partsIterator.getNextOrThrowEnd()
						)
						return getNextCharacterOrThrowEnd()
					}
				}
				is IteratorState.Divider -> {
					if (state.dividerIndex < separator.length) {
						return separator[state.dividerIndex++]
					} else {
						this.state = IteratorState.Part(state.nextPart.newIterator())
						return getNextCharacterOrThrowEnd()
					}
				}
			}
		}

	}


}

@file:Suppress("NOTHING_TO_INLINE")


package ic.text.ext


import ic.util.text.charset.Charset
import ic.util.text.charset.Charset.Companion.Utf8
import ic.text.Text
import ic.util.text.charset.ext.textToByteSequence


inline fun Text.toByteSequence (charset: Charset = Utf8) = charset.textToByteSequence(this)
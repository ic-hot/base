@file:Suppress("NOTHING_TO_INLINE")


package ic.text.ext


import ic.base.strings.ext.replaceAll
import ic.text.Text
import ic.base.strings.ext.asText
import ic.struct.map.finite.FiniteMap


inline fun Text.copyReplaceAll (vararg mappings: Pair<String, String>) : Text {
	return toString().replaceAll(*mappings).asText
}

inline fun Text.copyReplaceAll (map: FiniteMap<String, String>) : Text {
	return toString().replaceAll(map).asText
}

inline fun Text.copyReplaceAll (map: Iterable<Pair<String, String>>) : Text {
	return toString().replaceAll(map).asText
}
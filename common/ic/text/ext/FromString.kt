@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.text.ext


import ic.text.Text
import ic.text.fromstr.TextFromString


inline fun Text.Companion.FromString (string: String) = TextFromString(string)
package ic.text.ext


import ic.text.Text
import ic.text.empty.EmptyText


inline val Text.Companion.Empty get() = EmptyText
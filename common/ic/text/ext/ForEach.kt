package ic.text.ext


import ic.base.loop.breakableLoop
import ic.base.primitives.character.Character
import ic.base.throwables.End
import ic.text.Text


inline fun Text.forEach (action: (Character) -> Unit) {
	val iterator = newIterator()
	try {
		breakableLoop {
			action(
				iterator.getNextCharacterOrThrowEnd()
			)
		}
	} catch (end: End) {}
}
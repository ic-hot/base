package ic.text

import ic.base.strings.ext.asText


fun Any?.toText() : Text = when(this) {
	is Text -> this
	is String -> asText
	else -> toString().asText
}
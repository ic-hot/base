@file:Suppress("NOTHING_TO_INLINE")


package ic.text.fromstr


inline fun TextFromString (string: String) : TextFromString {

	return object : TextFromString() {

		override val string get() = string

	}

}
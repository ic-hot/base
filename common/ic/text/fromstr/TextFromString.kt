package ic.text.fromstr


import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.text.BaseText
import ic.text.input.TextInput


abstract class TextFromString : BaseText() {


	protected abstract val string : String


	override val length get() = string.length

	override fun newIterator() = object : TextInput {
		private var index = 0
		@Throws(End::class)
		override fun getNextCharacterOrThrowEnd(): Char {
			if (index >= string.length) throw End
			return string[index++]
		}
	}


	override fun toString() = string


}
@file:Suppress("CovariantEquals")


package ic.text


import ic.base.annotations.ToOverride
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.text.concat.ConcatText
import ic.text.input.TextInput


interface Text {


	fun newIterator() : TextInput


	@ToOverride
	val length : Int32 get() {
		var counter = 0
		val iterator = newIterator()
		try {
			while (true) {
				iterator.getNextCharacterOrThrowEnd()
				counter++
			}
		} catch (end: End) { return counter }
	}

	val isEmpty get() = length == 0

	val isNotEmpty get() = !isEmpty


	infix fun equals (string: String) 	= toString() == string
	infix fun equals (text: Text) 		= toString() == text.toString()

	infix fun startsWith (subtext: Text) 		= toString().startsWith(subtext.toString())
	infix fun startsWith (substring: String) 	= toString().startsWith(substring)
	infix fun endsWith (subtext: Text)		= toString().endsWith(subtext.toString())
	infix fun endsWith (substring: String)	= toString().endsWith(substring)

	operator fun plus (text: Text) = ConcatText(this, text)
	operator fun contains (subtext: Text) = toString().contains(subtext.toString())
	operator fun contains (string: String) = toString().contains(string)
	operator fun contains (char : Char) = toString().contains(char)


	companion object


}
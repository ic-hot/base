package ic.text


import ic.base.loop.breakableLoop
import ic.base.primitives.int32.Int32
import ic.base.throwables.End


abstract class BaseText : Text {

	override fun toString() : String {
		val iterator = newIterator()
		val stringBuilder = StringBuilder()
		try {
			breakableLoop {
				stringBuilder.append(
					iterator.getNextCharacterOrThrowEnd()
				)
			}
		} catch (end: End) {}
		return stringBuilder.toString()
	}

	override fun equals (other: Any?) : Boolean {
		if (other is Text) {
			return toString() == other.toString()
		} else {
			return false
		}
	}

	override fun hashCode() : Int32 {
		return toString().hashCode()
	}

}
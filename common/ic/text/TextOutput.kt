package ic.text


import ic.base.loop.loop
import ic.base.primitives.character.Character
import ic.base.throwables.End
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.ifaces.writer.Writer


interface TextOutput : Writer<Text> {

	fun putChar (character: Character)

	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun write (text: Text) {
		val textIterator = text.newIterator()
		try {
			loop {
				putChar(textIterator.getNextCharacterOrThrowEnd())
			}
		} catch (_: End) {}
	}

	fun write (string: String) {
		string.forEach(::putChar)
	}

	fun writeLine (text: Text) {
		synchronizedIfMutable {
			write(text)
			putChar('\n')
		}
	}

	fun writeLine (string: String) {
		synchronizedIfMutable {
			write(string)
			putChar('\n')
		}
	}

	fun writeNewLine() {
		putChar('\n')
	}

}

package ic.text.empty


import ic.base.annotations.AlwaysThrows
import ic.base.primitives.character.Character
import ic.base.throwables.End
import ic.text.BaseText
import ic.text.input.TextInput


object EmptyText : BaseText() {

	override fun newIterator() = object : TextInput {

		@AlwaysThrows(End::class)
		override fun getNextCharacterOrThrowEnd() : Character {
			throw End
		}

	}

	override fun toString() = ""

}
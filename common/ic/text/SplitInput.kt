package ic.text


import ic.base.annotations.Throws
import ic.base.throwables.End
import ic.ifaces.iterator.Iterator
import ic.text.input.TextInput


open class SplitInput (

	private val textInput: TextInput,

	private val divider: Char

) : Iterator<Text> {

	protected open val toSkipEmpty : Boolean get() = false

	private var textBuffer : TextBuffer? = null

	private var isEnd = false

	@Throws(End::class)
	override fun getNextOrThrowEnd() : Text {
		if (isEnd) throw End
		while (true) {
			if (textBuffer == null) textBuffer = TextBuffer()
			try {
				val c = textInput.getNextCharacterOrThrowEnd()
				if (c == divider) {
					val textBuffer = textBuffer
					this.textBuffer = null
					if (toSkipEmpty && textBuffer!!.isEmpty) continue
					return textBuffer!!
				}
				textBuffer!!.putChar(c)
			} catch (end: End) {
				this.isEnd = true
				val textBuffer = textBuffer
				this.textBuffer = null
				if (toSkipEmpty && textBuffer!!.isEmpty) continue
				return textBuffer!!
			}
		}
	}

}
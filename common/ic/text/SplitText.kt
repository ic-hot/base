package ic.text


import ic.ifaces.iterator.Iterator
import ic.struct.sequence.BaseSequence


open class SplitText (private val text: Text, private val separator: Char) : BaseSequence<Text>() {

	protected open val toSkipEmpty : Boolean get() = false

	override fun newIterator() : Iterator<Text> {
		return object : SplitInput(text.newIterator(), separator) {
			override val toSkipEmpty get() = this@SplitText.toSkipEmpty
		}
	}

	constructor (string: String, divider: Char) : this(Text(string), divider)

}
package ic.text


import ic.base.primitives.character.Character
import ic.ifaces.emptiable.Emptiable
import ic.text.ext.FromString
import ic.text.input.TextInput


class TextBuffer : BaseText, TextOutput, Emptiable {


	private val stringBuilder = StringBuilder()


	override val length get() = stringBuilder.length


	override fun toString() = stringBuilder.toString()


	override fun empty() {
		stringBuilder.clear()
	}


	override fun newIterator() : TextInput {
		return Text.FromString(stringBuilder.toString()).newIterator()
	}


	override fun putChar (character: Character) {
		stringBuilder.append(character)
	}

	override fun write (string: String) {
		stringBuilder.append(string)
	}


	constructor()

	constructor (character: Character) {
		putChar(character)
	}


}
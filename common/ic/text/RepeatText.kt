package ic.text


import ic.base.throwables.End
import ic.text.ext.FromString
import ic.text.input.TextInput


class RepeatText : BaseText {


	private val pattern : Text

	private val times : Int


	override fun newIterator() =  object : TextInput {
		var patternIterator : TextInput = pattern.newIterator()
		var counter = 0
		override fun getNextCharacterOrThrowEnd() : Char {
			while (true) {
				if (counter >= times) throw End
				try {
					return patternIterator.getNextCharacterOrThrowEnd()
				} catch (end : End) {
					patternIterator = pattern.newIterator()
					counter++
				}
			}
		}
	}


	constructor(
		pattern : Text,
		times : Int
	) {
		this.pattern = pattern
		this.times = times
	}

	constructor(
		pattern : String,
		times : Int
	) : this (
		Text.FromString(pattern),
		times
	)

	constructor(
		pattern : Char,
		times : Int
	) : this (
		pattern.toString(),
		times
	)


}
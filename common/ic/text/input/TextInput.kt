package ic.text.input


import ic.base.loop.loop
import ic.base.primitives.character.Character
import ic.base.primitives.character.inLowerCase
import ic.base.throwables.NotExistsException
import ic.base.throwables.End
import ic.text.Text
import ic.text.TextBuffer


interface TextInput {


	@Throws(End::class)
	fun getNextCharacterOrThrowEnd() : Character

	fun getNextCharacterOrNull() : Character? {
		return try {
			getNextCharacterOrThrowEnd()
		} catch (t: End) {
			null
		}
	}


	@Throws(End::class)
	fun safeRead (amount: Int): Text {
		val textBuffer = TextBuffer()
		for (i in 0 until amount) {
			textBuffer.putChar(getNextCharacterOrThrowEnd())
		}
		return textBuffer
	}

	fun read() : Text {
		val textBuffer = TextBuffer()
		try {
			loop {
				textBuffer.putChar(getNextCharacterOrThrowEnd())
			}
		} catch (end: End) {}
		return textBuffer
	}

	fun readToString() : String {
		val textBuffer = TextBuffer()
		try {
			loop {
				textBuffer.putChar(getNextCharacterOrThrowEnd())
			}
		} catch (end: End) {}
		return textBuffer.toString()
	}

	fun read (amount: Int) : Text {
		val textBuffer = TextBuffer()
		try {
			repeat (amount) {
				textBuffer.putChar(getNextCharacterOrThrowEnd())
			}
		} catch (end: End) {}
		return textBuffer
	}

	fun skip (amount: Int) {
		try {
			repeat (amount) {
				getNextCharacterOrThrowEnd()
			}
		} catch (end: End) {}
	}


	// Read till:

	private fun implementReadTill(
		string : String,
		ignoreCase : Boolean,
		onNotFound : () -> Unit
	) : Text {
		val textBuffer = TextBuffer()
		var stringIndex = 0
		try {
			while (true) {
				val c = getNextCharacterOrThrowEnd()
				val equal: Boolean
				run {
					if (ignoreCase) {
						equal = c.inLowerCase == string[stringIndex].inLowerCase
					} else {
						equal = c == string[stringIndex]
					}
				}
				if (equal) {
					stringIndex++
					if (stringIndex >= string.length) {
						break
					}
				} else {
					for (i in 0 until stringIndex) {
						textBuffer.putChar(string[i])
					}
					stringIndex = 0
					textBuffer.putChar(c)
				}
			}
		} catch (end: End) { onNotFound() }
		return textBuffer
	}

	fun readTill (string: String) = implementReadTill(string, false) {}

	fun readTill (c: Char) = readTill(c.toString())

	fun readLineAsText() = readTill('\n')

	@Throws(NotExistsException::class)
	fun safeReadTill (string: String) = implementReadTill (string, false) { throw NotExistsException }

	@Throws(NotExistsException::class)
	fun safeReadTillIgnoreCase (string: String) = implementReadTill(string, true) { throw NotExistsException }

	@Throws(NotExistsException::class)
	fun safeReadTill (c: Char) = safeReadTill(c.toString())

	@Throws(NotExistsException::class)
	fun safeReadLine() = safeReadTill('\n')


	object Empty : TextInput {
		override fun getNextCharacterOrThrowEnd() = throw End
	}


}

package ic.math.funs


import ic.base.assert.assert
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.math.annotations.Positive


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int64.divCeil (@Positive divisor: Int64) : Int64 {
	assert { divisor > 0 }
	if (this < 0) {
		return this / divisor
	} else {
		var result = this / divisor
		if (result * divisor != this) {
			result++
		}
		return result
	}
}


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int64.divCeil (@Positive divisor: Int32) : Int64 {
	return divCeil(divisor = divisor.asInt64)
}


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int32.divCeil (@Positive divisor: Int32) : Int32 {
	return this.asInt64.divCeil(divisor = divisor.asInt64).asInt32
}
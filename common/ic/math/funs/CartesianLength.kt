package ic.math.funs


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64
import ic.math.ext.squared
import ic.math.sqrt
import ic.math.square


@Suppress("NOTHING_TO_INLINE")
inline fun cartesian (x: Int64, y: Int64) = sqrt(square(x) + square(y))

@Suppress("NOTHING_TO_INLINE")
inline fun cartesian (x: Float32, y: Float32) = sqrt(square(x) + square(y))

@Suppress("NOTHING_TO_INLINE")
inline fun cartesian (x: Float64, y: Float64) = sqrt(x.squared + y.squared)

@Suppress("NOTHING_TO_INLINE")
inline fun cartesian (x: Float64, y: Float64, z: Float64) = sqrt(square(x) + square(y) + square(z))
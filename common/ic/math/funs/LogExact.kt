package ic.math.funs


import ic.base.assert.assert
import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.WrongValueException


@Throws(WrongValueException::class)
fun logExactOrThrowWrongValue (base: Int32, x: Int32) : Int32 {
	assert { base > 1 }
	assert { x > 0 }
	var result = 0
	var powerResult : Int32 = 1
	loop {
		when {
			x < powerResult -> throw WrongValueException
			x > powerResult -> {}
			x == powerResult -> return result
		}
		powerResult *= base
		result++
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun logExact (base: Int32, x: Int32) : Int32 {
	try {
		return logExactOrThrowWrongValue(base, x)
	} catch (_: WrongValueException) {
		throw WrongValueException.Runtime("base: $base, x: $x")
	}
}

package ic.math.funs


import ic.base.assert.assert
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Int32, b: Int32) = if (a < b) a else b

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Int64, b: Int64) = if (a < b) a else b

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Float32, b: Float32) = if (a < b) a else b

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Float64, b: Float64) = if (a < b) a else b

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Int32, b: Int64) = min(a.asInt64, b)

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Int32, b: Int32, c: Int32) : Int32 {
	return if (a < b) {
		if (a < c) a else c
	} else {
		if (b < c) b else c
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun min (a: Float64, b: Float64, c: Float64) : Float64 {
	return if (a < b) {
		if (a < c) a else c
	} else {
		if (b < c) b else c
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun min (vararg values: Float64) : Float64 {
	assert { values.isNotEmpty() }
	var min : Float64 = Float64.POSITIVE_INFINITY
	for (value in values) {
		if (value < min) {
			min = value
		}
	}
	return min
}
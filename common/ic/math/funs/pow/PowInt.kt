package ic.math.funs.pow


import ic.base.assert.assert
import ic.base.loop.repeat
import ic.base.primitives.bytes.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int8.Int8


infix fun Int32.pow (power: Int32) : Int32 {
	assert { power >= 0 }
	var result : Int32 = 1
	repeat(power) {
		result *= this
	}
	return result
}

@Suppress("NOTHING_TO_INLINE")
inline infix fun Int32.pow (power: Int8) : Int32 {
	return pow(power.asInt32)
}

infix fun Int32.pow (power: Int64) : Int64 {
	assert { power >= 0 }
	var result : Int64 = 1
	repeat(power) {
		result *= this
	}
	return result
}

infix fun Int64.pow (power: Int32) : Int64 {
	assert { power >= 0 }
	var result : Int64 = 1
	repeat(power) {
		result *= this
	}
	return result
}

infix fun Int64.pow (power: Int64) : Int64 {
	assert { power >= 0 }
	var result : Int64 = 1
	repeat(power) {
		result *= this
	}
	return result
}

package ic.math.funs.pow


import ic.base.primitives.float32.Float32
import kotlin.math.pow

import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline infix fun Float32.pow (power: Int32) = this.pow(power)

@Suppress("NOTHING_TO_INLINE")
inline infix fun Float64.pow (power: Float64) = this.pow(power)

@Suppress("NOTHING_TO_INLINE")
inline infix fun Float64.pow (power: Int32) = this.pow(power)


@Suppress("NOTHING_TO_INLINE")
inline infix fun Float64.pow (power: Int64) = this pow power.asInt32
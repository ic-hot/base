@file:Suppress("NOTHING_TO_INLINE")


package ic.math.funs


import ic.base.primitives.float32.Float32
import ic.math.Pi
import ic.math.denormalize


inline fun cosOfNormalizedFullPeriod (x: Float32) : Float32 = cos(x.denormalize(0, Pi * 2))
@file:Suppress("NOTHING_TO_INLINE")


package ic.math.funs


import ic.base.primitives.float32.Float32
import ic.math.Pi
import ic.math.denormalize
import ic.math.sin


inline fun sinOfNormalizedFullPeriod (x: Float32) : Float32 = sin(x.denormalize(0, Pi * 2))
package ic.math.funs


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import kotlin.math.absoluteValue
import kotlin.math.exp
import kotlin.math.ln

import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.math.funs.pow.pow


private val almostZero = 1.0 / (2 pow 16)

private val almostOne = 1.0 - 1.0 / (2 pow 24)


fun norm (value: Float64) : Float64 {
	if (value.absoluteValue <= almostZero) return value
	if (value < 0) {
		return -norm(-value)
	} else {
		return 1 - exp(-value)
	}
}

fun denorm (value: Float64) : Float64 {
	if (value.absoluteValue <= almostZero) return value
	if (value < 0) {
		return -denorm(-value)
	} else {
		return -ln(1 - value)
	}
}

fun safeDenorm (value: Float64) : Float64 {
	return denorm(value * almostOne)
}


@Suppress("NOTHING_TO_INLINE")
inline fun norm (value: Float32) = norm(value.asFloat64).asFloat32
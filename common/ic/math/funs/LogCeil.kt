package ic.math.funs


import ic.base.assert.assert
import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


fun logCeil (base: Int32, x: Int64) : Int32 {
	assert { base > 1 }
	assert { x >= 0 }
	var result = 0
	var powerResult : Int32 = 1
	loop {
		if (x < powerResult) {
			return result
		}
		powerResult *= base
		result++
	}
}
package ic.math.funs


import ic.base.assert.assert
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.math.annotations.Positive


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int64.divFloor (@Positive divisor: Int64) : Int64 {
	assert { divisor > 0 }
	if (this < 0) {
		var result = this / divisor
		if (result * divisor != this) {
			result--
		}
		return result
	} else {
		return this / divisor
	}
}


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int64.divFloor (@Positive divisor: Int32) : Int64 {
	return divFloor(divisor = divisor.asInt64)
}
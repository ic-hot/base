package ic.math.funs


import ic.base.arrays.ext.isNotEmpty
import ic.base.assert.assert
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int64.Int64


inline fun max (a: Int32, b: Int32) = if (a > b) a else b

inline fun max (a: Int64, b: Int64) = if (a > b) a else b

inline fun max (a: Float32, b: Float32) = if (a > b) a else b

inline fun max (a: Int32, b: Float32) = max(a.asFloat32, b)

inline fun max (a: Float64, b: Float64) = if (a > b) a else b


inline fun max (a: Int32, b: Int32, c: Int32) : Int32{
	if (a > b) {
		if (a > c) {
			return a
		} else {
			return c
		}
	} else {
		if (b > c) {
			return b
		} else {
			return c
		}
	}
}


inline fun max (vararg values: Float32) : Float32 {
	assert { values.isNotEmpty }
	var max : Float32 = Float32.NEGATIVE_INFINITY
	for (value in values) {
		if (value > max) {
			max = value
		}
	}
	return max
}
package ic.math.funs


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32


@Suppress("NOTHING_TO_INLINE")
inline fun cos (x: Float64) : Float64 = kotlin.math.cos(x)


@Suppress("NOTHING_TO_INLINE")
inline fun cos (x: Float32) : Float32 = cos(x.asFloat64).asFloat32
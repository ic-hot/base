@file:Suppress("NOTHING_TO_INLINE")


package ic.math.funs


import ic.base.primitives.float64.Float64
import ic.math.Pi


inline fun degreesToRadians (degrees: Float64) : Float64 {

	return degrees * Pi / 180

}
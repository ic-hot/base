@file:OptIn(ExperimentalTypeInference::class)


package ic.math.funs


import kotlin.experimental.ExperimentalTypeInference

import ic.base.loop.repeat
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


@OverloadResolutionByLambdaReturnType
inline fun sum (numbersCount: Int64, getNumber: (index: Int64) -> Int64) : Int64 {
	var sum : Int64 = 0
	repeat(numbersCount) { index ->
		sum += getNumber(index)
	}
	return sum
}

@OverloadResolutionByLambdaReturnType
inline fun sum (numbersCount: Int64, getNumber: (index: Int64) -> Float64) : Float64 {
	var sum : Float64 = 0.0
	repeat(numbersCount) { index ->
		sum += getNumber(index)
	}
	return sum
}


fun sum (numbers: Iterable<Int32>) : Int32 {
	var sum : Int32 = 0
	numbers.forEach {
		sum += it
	}
	return sum
}
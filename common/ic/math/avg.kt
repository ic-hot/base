package ic.math


import ic.base.primitives.float32.Float32


fun avg (a: Float32, b: Float32) : Float32 {
	return (a + b) / 2
}
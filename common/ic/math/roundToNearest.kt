package ic.math


import kotlin.math.absoluteValue

import ic.base.primitives.float64.Float64


fun Float64.roundToNearest (a: Float64, b: Float64) : Float64 {

	if ((this - a).absoluteValue <= (this - b).absoluteValue) {
		return a
	} else {
		return b
	}

}
package ic.math.annotations


@MustBeDocumented
@Target(
	AnnotationTarget.FIELD,
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.VALUE_PARAMETER,
	AnnotationTarget.LOCAL_VARIABLE,
	AnnotationTarget.PROPERTY
)
annotation class BetweenInt(vararg val value: Int)

package ic.math.annotations


import ic.base.primitives.float64.Float64


@MustBeDocumented
@Target(
	AnnotationTarget.FIELD,
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.VALUE_PARAMETER,
	AnnotationTarget.LOCAL_VARIABLE,
	AnnotationTarget.PROPERTY
)
annotation class BetweenFloat64 (vararg val value: Float64)

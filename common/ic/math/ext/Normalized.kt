package ic.math.ext


import ic.base.primitives.int8.Int8


inline val Int8.normalized : Int8 get() = if (this < 0) Int8(-1) else Int8(1)
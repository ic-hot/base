package ic.math.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32


inline val Int32.squared get() = this * this

inline val Float32.squared get() = this * this

inline val Float64.squared get() = this * this
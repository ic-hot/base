@file:Suppress("NOTHING_TO_INLINE", "ConvertTwoComparisonsToRangeCheck")


package ic.math.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.ext.asFloat32


inline fun Int.isInRange (from: Int, to: Int) : Boolean = this >= from && this < to

inline fun Float32.isInRange (from: Float32, to: Float32) : Boolean = this >= from && this < to

inline fun Float32.isInRange (from: Int, to: Int) : Boolean = isInRange(from.asFloat32, to.asFloat32)


inline fun Float64.isInRange (from: Float64, to: Float64) : Boolean = this >= from && this < to
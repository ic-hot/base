package ic.math.ext


import kotlin.math.round
import kotlin.math.roundToInt
import kotlin.math.roundToLong

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline val Float32.round : Float32 get() = round(this)

inline val Float32.roundAsInt32 : Int32 get() = roundToInt()

inline val Float32.roundAsInt64 : Int64 get() = roundToLong()
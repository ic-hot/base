package ic.math.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.float64.ext.isNaN
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat32
import ic.base.primitives.int64.ext.asFloat64


inline val Float64.ceilToInt64 : Int64 get() {
	if (this.isNaN) throw RuntimeException("value: $this")
	if (this >= 0) {
		val negativeValue = -this
		val negativeFloored = negativeValue.asInt64
		if (negativeFloored.asFloat64 == negativeValue) {
			return -negativeFloored
		} else {
			return -negativeFloored + 1
		}
	} else {
		return this.asInt64
	}
}

inline val Float32.ceilToInt64 : Int64 get() = this.asFloat64.ceilToInt64

inline val Float32.ceil : Float32 get() = this.asFloat64.ceilToInt64.asFloat32
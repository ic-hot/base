package ic.math.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat64


@Suppress("NOTHING_TO_INLINE")
inline fun Float64.log (n: Float64) = kotlin.math.log(n, this)

@Suppress("NOTHING_TO_INLINE")
inline fun Float64.log (n: Int32) = this.log(n.asFloat64)
package ic.math.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int32.divAsFloat32 (d: Int32) = this.asFloat32 / d.asFloat32
package ic.math.ext


import kotlin.math.sqrt

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat64


inline val Float32.sqrt get() = sqrt(this)

inline val Int32.sqrt get() = sqrt(this.asFloat64)

inline val Int64.sqrt get() = sqrt(this.asFloat64)
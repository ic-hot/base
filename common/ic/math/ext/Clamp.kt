package ic.math.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.ext.asFloat64
import ic.base.primitives.int64.ext.asInt32
import ic.base.primitives.int64.ext.asFloat32


@Suppress("NOTHING_TO_INLINE")
inline fun Int32.clamp (from: Int32, to: Int32 = Int32.MAX_VALUE) : Int32 {
	if (this < from) return from
	if (this >= to)  return to - 1
	else return this
}

@Suppress("NOTHING_TO_INLINE")
inline fun Int32.clamp (from: Int32, to: Int64) = clamp(from, to.asInt32)

@Suppress("NOTHING_TO_INLINE")
inline fun Int64.clamp (from: Int64, to: Int64 = Int64.MAX_VALUE) : Int64 {
	if (this < from) return from
	if (this >= to)  return to - 1
	else return this
}

@Suppress("NOTHING_TO_INLINE")
inline fun Float64.clamp (from: Float64, to: Float64) : Float64 {
	if (this < from) return from
	if (this > to)   return to
	else return this
}
@Suppress("NOTHING_TO_INLINE")
inline fun Float64.clamp (from: Int32, to: Float64) = clamp(from.asFloat64, to)


@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (min: Float32, max: Float32 = Float32.POSITIVE_INFINITY) : Float32 {
	if (this < min) return min
	if (this > max) return max
	else return this
}

@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (from: Int32, to: Float32 = Float32.POSITIVE_INFINITY)
	= clamp (from.asFloat32, to)
;

@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (from: Float32, to: Int32) = clamp (from, to.asFloat32)

@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (from: Int32, to: Int32) = clamp(from.asFloat32, to.asFloat32)

@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (from: Int32, to: Int64) = clamp(from.asFloat32, to.asFloat32)

@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp (from: Int32, to: Float64) = clamp(from.asFloat32, to.asFloat32)


@Suppress("NOTHING_TO_INLINE")
inline fun Float32.clamp() = clamp(0F, 1F)
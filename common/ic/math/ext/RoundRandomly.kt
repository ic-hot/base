package ic.math.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.math.rand.randomBoolean


fun Float64.roundRandomly() : Int64 {

	if (this < 0) {
		return -((-this).roundRandomly())
	}

	val floored = this.asInt64

	val remainder = this - floored

	val toAddOne = randomBoolean(probability = remainder)

	if (toAddOne) {
		return floored + 1
	} else {
		return floored
	}

}
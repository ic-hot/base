@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64


inline fun square (value: Float64) = value * value

inline fun square (value: Float32) = value * value

inline fun square (value: Int64) = value * value
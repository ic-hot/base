package ic.math.circle.funs


import kotlin.math.absoluteValue

import ic.base.primitives.bytes.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt8
import ic.base.primitives.int8.Int8


fun diffInCircle (a: Int32, b: Int32, from: Int32, to: Int32) : Int32 {
	var diff = a - b
	val circleLength = to - from
	if (diff.absoluteValue > circleLength / 2) {
		if (diff < 0) {
			diff += circleLength
		} else {
			diff -= circleLength
		}
	}
	return diff
}


@Suppress("NOTHING_TO_INLINE")
inline fun diffInCircle (a: Int8, b: Int8, from: Int32, to: Int32) : Int8 {
	return diffInCircle(a.asInt32, b.asInt32, from = from, to = to).asInt8
}
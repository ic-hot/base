package ic.math.circle.ext


import ic.base.primitives.bytes.ext.asInt32
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.ext.asFloat64
import ic.base.primitives.int32.ext.asInt8
import ic.base.primitives.int64.Int64
import ic.base.primitives.int8.Int8


fun Int32.circle (from: Int32, to: Int32) : Int32 {
	val circleLength = to - from
	var value = this
	while (value < from) {
		value += circleLength
	}
	while (value >= to) {
		value -= circleLength
	}
	return value
}


fun Int64.circle (from: Int64, to: Int64) : Int64 {
	val circleLength = to - from
	var value = this
	while (value < from) {
		value += circleLength
	}
	while (value >= to) {
		value -= circleLength
	}
	return value
}


@Suppress("NOTHING_TO_INLINE")
inline fun Int8.circle (from: Int32, to: Int32) : Int8 {
	return this.asInt32.circle(from, to).asInt8
}


fun Float32.circle (from: Float32, to: Float32) : Float32 {
	val circleLength = to - from
	var value = this
	while (value < from) {
		value += circleLength
	}
	while (value >= to) {
		value -= circleLength
	}
	return value
}


@Suppress("NOTHING_TO_INLINE")
inline fun Float32.circle (from: Int32, to: Int32) : Float32 {
	return circle(from = from.asFloat32, to = to.asFloat32)
}


fun Float64.circle (from: Float64, to: Float64) : Float64 {
	val circleLength = to - from
	var value = this
	while (value < from) {
		value += circleLength
	}
	while (value >= to) {
		value -= circleLength
	}
	return value
}


@Suppress("NOTHING_TO_INLINE")
inline fun Float64.circle (from: Int32, to: Int32) : Float64 {
	return circle(from = from.asFloat64, to = to.asFloat64)
}

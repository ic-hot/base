package ic.math.circle.ext


import kotlin.math.absoluteValue

import ic.base.primitives.int64.Int64


fun Int64.uncircleToNearest (

	nearest : Int64,

	from : Int64, to : Int64

) : Int64 {

	val circleLength = to - from

	if (circleLength < 2) return nearest

	var value : Int64 = this

	while (value < nearest) {
		value += circleLength
	}

	while (value > nearest) {
		value -= circleLength
	}

	val previousValue = value - circleLength
	val nextValue     = value + circleLength

	val distance = (nearest - value).absoluteValue

	when {
		(nearest - previousValue).absoluteValue < distance -> return previousValue
		(nearest - nextValue).absoluteValue     < distance -> return nextValue
		else -> return value
	}

}
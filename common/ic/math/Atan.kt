@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import kotlin.math.atan2

import ic.base.primitives.float64.Float64


inline fun atan (x: Float64, y: Float64) : Float64 {
	return atan2(y, x)
}

inline fun atan (x: Float64, y: Float64, periodStart: Float64) : Float64 {
	var result = atan(x, y)
	while (result < periodStart) result += Tau
	while (result >= periodStart + Tau) result -= Tau
	return result
}
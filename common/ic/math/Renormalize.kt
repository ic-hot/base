@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import ic.base.primitives.float32.Float32


inline fun Float32.renormalize (oldStart: Float32, oldEnd: Float32, newStart: Float32, newEnd: Float32) = (
	(this - oldStart) / (oldEnd - oldStart) * (newEnd - newStart) + newStart
)
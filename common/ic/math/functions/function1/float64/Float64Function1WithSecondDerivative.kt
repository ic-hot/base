package ic.math.functions.function1.float64


import ic.base.primitives.float64.Float64


interface Float64Function1WithSecondDerivative : Float64Function1WithDerivative {

	fun getSecondDerivative (arg: Float64) : Float64

	override val derivative : Float64Function1WithDerivative

}
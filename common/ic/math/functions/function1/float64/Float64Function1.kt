package ic.math.functions.function1.float64


import ic.base.primitives.float64.Float64


interface Float64Function1 {

	fun getValue (arg: Float64) : Float64

}
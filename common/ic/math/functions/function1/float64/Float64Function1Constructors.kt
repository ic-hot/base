package ic.math.functions.function1.float64


import ic.base.primitives.float64.Float64


inline fun Float64Function1 (crossinline getValue : (Float64) -> Float64) : Float64Function1 {

	return object : Float64Function1 {

		override fun getValue (arg: Float64) = getValue.invoke(arg)

	}

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.math.functions.function1.float64.ext


import ic.base.primitives.float64.Float64
import ic.math.functions.function1.float64.Float64Function1


operator fun Float64Function1.invoke (arg: Float64) : Float64 = getValue(arg)
@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import kotlin.math.roundToLong

import ic.base.primitives.float64.Float64



inline fun round (value: Float64) = value.roundToLong()
package ic.math.map


import ic.base.primitives.int32.ext.asUInt64
import ic.base.primitives.uint64.UInt64


private val Zero  = 0.asUInt64
private val One   = 1.asUInt64
private val Two   = 2.asUInt64
private val Three = 3.asUInt64
private val Four  = 4.asUInt64


fun map2dTo1d (x: UInt64, y: UInt64) : UInt64 = when {

	x == Zero && y == Zero -> Zero
	x == One  && y == Zero -> One
	x == Zero && y == One  -> Two
	x == One  && y == One  -> Three

	else -> map2dTo1d(x / Two, y / Two) * Four + map2dTo1d(x % Two, y % Two)

}
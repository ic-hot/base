package ic.math.map


import ic.base.primitives.int32.ext.asUInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asUInt64
import ic.base.primitives.uint64.UInt64


private val One = 1.asUInt64
private val Two = 2.asUInt64


fun mapSignedToUnsigned (a: Int64) : UInt64 = (

	if (a < 0) {
		(-(a + 1)).asUInt64 * Two + One
	} else {
		a.asUInt64 * Two
	}

)
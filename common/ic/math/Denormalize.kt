package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.ext.asFloat64


inline fun Float32.denormalize (from0: Float32, from1: Float32) : Float32 {
	return this * (from1 - from0) + from0
}


inline fun Float32.denormalize (from0: Int, from1: Int) = denormalize(from0.asFloat32, from1.asFloat32)

inline fun Float32.denormalize (from0: Int, from1: Float32) = denormalize(from0.asFloat32, from1)

inline fun Float32.denormalize (from0: Float32, from1: Int) = denormalize(from0, from1.asFloat32)

inline fun Float32.denormalize (from0: Int32, from1: Float64) = denormalize(from0.asFloat32, from1.asFloat32)

inline fun Float32.denormalize (from0: Float64, from1: Int32) = denormalize(from0.asFloat32, from1.asFloat32)


inline fun Float64.denormalize (from0: Float64, from1: Float64) : Float64 {
	return this * (from1 - from0) + from0
}

inline fun Float64.denormalize (from0: Int32, from1: Int32) : Float64 {
	return denormalize(from0.asFloat64, from1.asFloat64)
}

inline fun Float64.denormalize (from0: Int32, from1: Float64) : Float64 {
	return denormalize(from0.asFloat64, from1)
}

inline fun Float32.denormalize (from0: Float64, from1: Float64) : Float64 {
	return this * (from1 - from0) + from0
}

inline fun Float64.denormalize (from0: Float64, from1: Int32) : Float64 {
	return denormalize(from0, from1.asFloat64)
}

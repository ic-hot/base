@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat64

import kotlin.math.pow


inline fun Float64.pow (power: Float64) = this.pow(power)

inline fun Float64.pow (power: Int32) = this.pow(power.asFloat64)

inline fun Float32.pow (power: Float64) = this.asFloat64.pow(power).asFloat32
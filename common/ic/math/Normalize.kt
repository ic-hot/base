@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.ext.asFloat64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat64


inline fun Float32.normalize (to0: Float32, to1: Float32) : Float32 {
	if (to1 == to0) return 0F
	return (this - to0) / (to1 - to0)
}

inline fun Float32.normalize (to0: Int32, to1: Int32) 		= normalize(to0.asFloat32, to1.asFloat32)
inline fun Float32.normalize (to0: Float32, to1: Int32) 	= normalize(to0, to1.asFloat32)
inline fun Float32.normalize (to0: Int32, to1: Float32) 	= normalize(to0.asFloat32, to1)
inline fun Float32.normalize (to0: Float64, to1: Float64) 	= normalize(to0.asFloat32, to1.asFloat32)
inline fun Float32.normalize (to0: Int32, to1: Float64) 	= normalize(to0.asFloat32, to1.asFloat32)
inline fun Float32.normalize (to0: Float64, to1: Int32) 	= normalize(to0.asFloat32, to1.asFloat32)


inline fun Float64.normalize (to0: Float64, to1: Float64) : Float64 {
	if (to1 == to0) return 0.0
	return (this - to0) / (to1 - to0)
}

inline fun Float64.normalize (to0: Int32, to1: Int32) : Float64 {
	return normalize(to0.asFloat64, to1.asFloat64)
}

inline fun Float64.normalize (to0: Int32, to1: Int64) : Float64 {
	return normalize(to0.asFloat64, to1.asFloat64)
}

inline fun Float64.normalize (to0: Int32, to1: Float64) : Float64 {
	return normalize(to0.asFloat64, to1)
}

inline fun Float64.normalize (to0: Float64, to1: Int32) : Float64 {
	return normalize(to0, to1.asFloat64)
}


inline fun Int32.normalize (to0: Int32, to1: Int32) = this.asFloat32.normalize(to0.asFloat32, to1.asFloat32)
package ic.math


import ic.base.primitives.float32.Float32


fun Float32.interpolateLinearly (vararg points : Pair<Float32, Float32>) : Float32 {
	var i = 0
	while (i < points.size) {
		if (this >= points[i].first) {
			if (i == points.size - 1) {
				return points[i].second
			} else {
				if (this < points[i + 1].first) {
					return normalize(points[i].first, points[i + 1].first).denormalize(points[i].second, points[i + 1].second)
				}
			}
		}
		i++
	}; return points[0].second
}
package ic.math


import ic.base.primitives.float32.Float32
import kotlin.math.absoluteValue


inline fun Float32.roundToClosest (vararg values: Float32) : Float32 {
	var closestValue : Float32 = 0F
	var closestDistance : Float32 = Float32.MAX_VALUE
	values.forEach { value ->
		val distance = (value - this).absoluteValue
		if (distance < closestDistance) {
			closestValue = value
			closestDistance = distance
		}
	}
	return closestValue
}

inline fun Float32.roundToClosest (vararg values: Int) : Int {
	var closestValue : Int = 0
	var closestDistance : Float32 = Float32.MAX_VALUE
	values.forEach { value ->
		val distance = (value - this).absoluteValue
		if (distance < closestDistance) {
			closestValue = value
			closestDistance = distance
		}
	}
	return closestValue
}
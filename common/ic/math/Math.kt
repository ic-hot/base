@file:Suppress("NOTHING_TO_INLINE")


package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64


inline fun sin (x: Float64) : Float64 = kotlin.math.sin(x)

fun sin (x: Float32) : Float32 = kotlin.math.sin(x)

fun getAngle (angle1: Double, angle2: Double) : Double {
	var result = angle2 - angle1
	while (result < -Pi) result += Tau
	while (result > Pi) result -= Tau
	return result
}

fun getAngle (angle1: Float, angle2: Float) : Float {
	return getAngle(
		angle1.toDouble(),
		angle2.toDouble()
	).toFloat()
}
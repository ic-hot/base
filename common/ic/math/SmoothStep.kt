package ic.math


import ic.math.annotations.BetweenFloat64
import ic.base.primitives.float64.Float64


@BetweenFloat64(0.0, 1.0)
fun smoothStep (a: Float64) : Float64 {
	if (a <= 0.0) return 0.0
	if (a >= 1.0) return 1.0
	return 3.0 * a * a - 2.0 * a * a * a
}
package ic.math.rand


import ic.base.primitives.int32.Int32
import ic.base.primitives.int8.Int8


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> getRandom (a: Item, b: Item) : Item {
	if (randomBoolean()) {
		return a
	} else {
		return b
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> getRandom (a: Item, b: Item, c: Item) : Item {
	when (randomInt32(3)) {
		0 -> return a
		1 -> return b
		2 -> return c
		else -> throw Error()
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> getRandom (a: Item, b: Item, c: Item, d: Item) : Item {
	when (randomInt32(4)) {
		0 -> return a
		1 -> return b
		2 -> return c
		3 -> return d
		else -> throw Error()
	}
}

fun <Item> getRandom (vararg items: Item) : Item {
	return items[randomInt32(items.size)]
}

fun <Item> getRandom (seed: String, vararg items: Item) : Item {
	return items[Random(seed).getNextInt32(items.size)]
}



@Suppress("NOTHING_TO_INLINE")
inline fun getRandom (item1: Int8, item2: Int8) : Int8 {
	if (randomBoolean()) {
		return item1
	} else {
		return item2
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun getRandom (item1: Int32, item2: Int32) : Int32 {
	if (randomBoolean()) {
		return item1
	} else {
		return item2
	}
}
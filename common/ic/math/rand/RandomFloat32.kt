package ic.math.rand


import ic.base.primitives.float32.Float32


val randomFloat32 : Float32 get() = Random.Global.getNextFloat32()

@Suppress("NOTHING_TO_INLINE")
inline fun randomFloat32 (below: Float32) = randomFloat32 * below

@Suppress("NOTHING_TO_INLINE")
inline fun randomFloat32 (from: Float32, to: Float32) = from + randomFloat32 * (to - from)
package ic.math.rand


import ic.base.primitives.float64.Float64


val randomFloat64 : Float64 get() = Random.Global.getNextFloat64()

@Suppress("NOTHING_TO_INLINE")
inline fun randomFloat64 (below: Float64) = randomFloat64 * below

@Suppress("NOTHING_TO_INLINE")
inline fun randomFloat64 (from: Float64, to: Float64) = from + randomFloat64 * (to - from)
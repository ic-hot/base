@file:Suppress("NOTHING_TO_INLINE")


package ic.math.rand


import kotlin.jvm.JvmField

import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int32.ext.asInt8
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.base.primitives.int8.Int8
import ic.math.denormalize
import ic.system.funs.getCurrentEpochTimeMs


class Random {


	val kotlinRandom: kotlin.random.Random


	fun getNextByteArray (length: Int) : ByteArray {
		val byteArray = ByteArray(length)
		kotlinRandom.nextBytes(byteArray)
		return byteArray
	}

	fun getNextInt8() : Int8 = getNextInt32().asInt8

	fun getNextInt32() : Int = kotlinRandom.nextInt()

	fun getNextInt32 (from: Int32, to: Int32) : Int32 = kotlinRandom.nextInt(from, to)

	fun getNextInt64() : Long = kotlinRandom.nextLong()


	fun getNextInt32 (below: Int): Int {
		return kotlinRandom.nextInt(below)
	}

	fun getNextInt64 (limit: Int64) : Int64 {
		return kotlinRandom.nextInt(limit.asInt32).asInt64
	}


	inline fun getNextFloat32 () : Float32 = kotlinRandom.nextFloat()

	inline fun getNextFloat32 (limit: Float32) = getNextFloat32().denormalize(0, limit)

	inline fun getNextFloat32 (from: Float32, to: Float32) = getNextFloat32().denormalize(from, to)


	inline fun getNextFloat64() : Float64 = kotlinRandom.nextDouble()

	inline fun getNextFloat64 (limit: Float64) : Float64 = getNextFloat64().denormalize(0, limit)

	inline fun getNextFloat64 (from: Float64, to: Float64) : Float64 = getNextFloat64().denormalize(from, to)


	inline fun nextBoolean() : Boolean {
		return kotlinRandom.nextBoolean()
	}

	inline fun nextBoolean (probability: Float32) : Boolean {
		if (probability <= 0) return false
		if (probability >= 1) return true
		return kotlinRandom.nextFloat() < probability
	}

	inline fun nextBoolean (probability: Float64) : Boolean {
		return kotlinRandom.nextDouble() < probability
	}


	constructor (seed: Long) {
		kotlinRandom = kotlin.random.Random(seed)
	}

	constructor (seed: String) {
		kotlinRandom = kotlin.random.Random(seed.hashCode().toLong())
	}

	constructor() {
		kotlinRandom = kotlin.random.Random(getCurrentEpochTimeMs())
	}

	companion object {

		@JvmField
		val Global = Random()

	}


}

package ic.math.rand


fun getRandomByteArray (length: Int) = Random.Global.getNextByteArray(length)
package ic.math.rand


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64


@Suppress("NOTHING_TO_INLINE")
inline fun randomBoolean () = Random.Global.nextBoolean()

@Suppress("NOTHING_TO_INLINE")
inline fun randomBoolean (probability: Float32) = Random.Global.nextBoolean(probability)

@Suppress("NOTHING_TO_INLINE")
inline fun randomBoolean (probability: Float64) = Random.Global.nextBoolean(probability)


@Suppress("NOTHING_TO_INLINE")
inline fun randomBoolean (seed: String, probability: Float64) = Random(seed).nextBoolean(probability)

@Suppress("NOTHING_TO_INLINE")
inline fun randomBoolean (seed: String, probability: Float32 = .5F) = Random(seed).nextBoolean(probability)
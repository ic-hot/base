package ic.math.rand


inline fun <Result> random (
	action1 : () -> Result,
	action2 : () -> Result
) : Result {
	return if (randomBoolean()) {
		action1()
	} else {
		action2()
	}
}


inline fun <Result> random (
	action1 : () -> Result,
	action2 : () -> Result,
	action3 : () -> Result
) : Result {
	return when (randomInt32(below = 3)) {
		0 -> action1()
		1 -> action2()
		2 -> action3()
		else -> throw RuntimeException()
	}
}


inline fun <Result> random (
	action1 : () -> Result,
	action2 : () -> Result,
	action3 : () -> Result,
	action4 : () -> Result
) : Result {
	return when (randomInt32(4)) {
		0 -> action1()
		1 -> action2()
		2 -> action3()
		3 -> action4()
		else -> throw RuntimeException()
	}
}
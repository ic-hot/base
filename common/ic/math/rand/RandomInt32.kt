package ic.math.rand


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32() = Random.Global.getNextInt32()

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32 (below: Int32) = Random.Global.getNextInt32(below)

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32 (below: Int64) = randomInt32(below = below.asInt32)

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32 (from: Int32, to: Int32) = Random.Global.getNextInt32(from = from, to = to)


@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32 (seed: String, limit: Int32) = Random(seed).getNextInt32(limit)

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt32 (seed: String, from: Int32, to: Int32) : Int32 {
	return Random(seed).getNextInt32(from, to)
}
package ic.math.rand


import ic.base.primitives.int64.Int64


fun randomInt64 () = Random.Global.getNextInt64()

fun randomInt64 (below: Int64) = Random.Global.getNextInt64(below)

fun randomInt64 (seed: String, limit: Int64) = Random(seed).getNextInt64(limit)

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt64 (from: Int64, to: Int64) : Int64 {
	return from + randomInt64(below = to - from)
}

@Suppress("NOTHING_TO_INLINE")
inline fun randomInt64 (seed: String, from: Int64, to: Int64) : Int64 {
	return from + randomInt64(seed = seed, limit = to - from)
}
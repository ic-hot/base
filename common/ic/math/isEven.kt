package ic.math


import ic.base.primitives.int64.Int64


inline val Int64.isEven : Boolean get() = this % 2 == 0L
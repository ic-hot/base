package ic.math


inline val Pi get() = kotlin.math.PI

inline val Tau get() = Pi * 2

inline val E get() = kotlin.math.E
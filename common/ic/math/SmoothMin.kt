package ic.math


import ic.base.primitives.float64.Float64
import ic.base.throwables.EmptyException
import ic.math.ext.clamp


fun smoothMin (
	a : Float64,
	b : Float64,
	smoothRadius : Float64
) : Float64 {
	val h = (0.5 + 0.5 * (b - a) / smoothRadius).clamp(0.0, 1.0)
	return h.denormalize(b, a) - smoothRadius * h * (1.0 - h)
}


@Throws(EmptyException::class)
inline fun <Item> smoothMinOrThrowEmpty (
	items : Iterable<Item>,
	crossinline itemToFloat64 : (Item) -> Float64,
	smoothRadius : Float64
) : Float64 {
	var atLeastOne : Boolean = false
	var minF : Float64 = 0.0
	items.forEach { item ->
		if (atLeastOne) {
			val f = itemToFloat64(item)
			minF = ic.math.smoothMin(f, minF, smoothRadius)
		} else {
			atLeastOne = true
			minF = itemToFloat64(item)
		}
	}
	if (atLeastOne) {
		return minF
	} else {
		throw EmptyException
	}
}


inline fun <Item> smoothMin (
	items : Iterable<Item>,
	crossinline itemToFloat64 : (Item) -> Float64,
	smoothRadius : Float64
) : Float64 {
	try {
		return smoothMinOrThrowEmpty(
			items = items,
			itemToFloat64 = itemToFloat64,
			smoothRadius = smoothRadius
		)
	} catch (empty: EmptyException) { throw EmptyException.Runtime(empty) }
}
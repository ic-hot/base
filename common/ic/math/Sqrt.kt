package ic.math


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat64


@Suppress("NOTHING_TO_INLINE")
inline fun sqrt (value: Float32) : Float32 = kotlin.math.sqrt(value)

@Suppress("NOTHING_TO_INLINE")
inline fun sqrt (value: Float64) : Float64 = kotlin.math.sqrt(value)

@Suppress("NOTHING_TO_INLINE")
inline fun sqrt (value: Int64) : Float64 = sqrt(value.asFloat64)
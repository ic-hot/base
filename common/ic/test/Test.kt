package ic.test


import ic.base.reflect.ext.className
import ic.text.TextOutput


abstract class Test {


	protected open val testName : String get() = className


	protected abstract fun TextOutput.runTest()


	fun run (output: TextOutput) {

		output.run {

			output.writeLine("Test $testName started...")

			try {

				runTest()

				writeLine("Test $testName passed.")

			} catch (t: Throwable) {

				writeLine("Test $testName failed:")

				throw t

			}

		}

	}


}
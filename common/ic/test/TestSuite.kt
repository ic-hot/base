package ic.test


import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach
import ic.text.TextOutput


abstract class TestSuite : Test() {


	protected abstract fun initTests() : List<out Test>


	protected open fun TextOutput.beforeRun() {}

	protected open fun TextOutput.afterRun() {}


	override fun TextOutput.runTest() {

		beforeRun()

		initTests().breakableForEach { test ->

			test.run(output = this)

		}

		afterRun()

	}


}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.test


import ic.struct.list.List


inline fun TestSuite (vararg tests: Test) : TestSuite {

	val testsList = List(*tests)

	return object : TestSuite() {

		override fun initTests() = testsList

	}

}
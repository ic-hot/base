package ic.test


import ic.base.test.BaseTests
import ic.math.test.MathTests
import ic.struct.list.List
import ic.struct.test.StructTests
import ic.text.tests.TextTests
import ic.util.tests.UtilTests


internal class BasePackageTests : TestSuite() {


	override fun initTests() = List(

		BaseTests(),

		MathTests(),

		TextTests(),

		UtilTests(),

		StructTests(),

	)


}
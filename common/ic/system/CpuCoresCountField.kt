package ic.system


import ic.base.primitives.int32.Int32


val cpuCoresCount : Int32 get() = ic.base.platform.basePlatform.systemEngine.getCpuCoresCount()
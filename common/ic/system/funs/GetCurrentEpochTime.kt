package ic.system.funs


import ic.base.primitives.int64.Int64
import ic.system.impl.systemEngine


fun getCurrentEpochTimeMs() : Int64 = systemEngine.getCurrentEpochTimeMs()

@Suppress("NOTHING_TO_INLINE")
inline fun getCurrentEpochTimeS() = getCurrentEpochTimeMs() / 1000
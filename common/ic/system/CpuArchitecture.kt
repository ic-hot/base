package ic.system


import ic.struct.list.List


sealed class CpuArchitecture {

	abstract val name : String

	object X86_64 : CpuArchitecture() {
		override val name get() = "x86_64"
	}

	/*object AArch64 : CpuArchitecture(
		unixName = "aarch64"
	)*/

	companion object {

		val all : List<CpuArchitecture> get() = List(
			X86_64,
			//AArch64
		)

		val runtime get() = ic.base.platform.basePlatform.systemEngine.getCpuArchitecture()

	}

}
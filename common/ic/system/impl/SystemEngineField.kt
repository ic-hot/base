package ic.system.impl


import ic.base.platform.basePlatform


internal inline val systemEngine get() = basePlatform.systemEngine
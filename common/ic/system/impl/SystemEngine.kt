package ic.system.impl


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.system.CpuArchitecture
import ic.system.ostype.OsType


internal abstract class SystemEngine {

	abstract fun getOsType() : OsType

	abstract fun getCurrentEpochTimeMs() : Int64

	abstract fun getCpuArchitecture() : CpuArchitecture

	abstract fun getCpuCoresCount() : Int32

}
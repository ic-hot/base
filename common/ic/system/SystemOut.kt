package ic.system


import ic.base.primitives.character.Character
import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.Mutex
import ic.text.TextOutput


object SystemOut : TextOutput, Mutable {


	override val mutex = Mutex()


	override fun putChar (character: Character) {

		if (character == '\t') {
			print("    ")
			return
		}

		print(character)

	}


}
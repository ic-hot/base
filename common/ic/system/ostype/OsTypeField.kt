package ic.system.ostype


import ic.struct.value.ext.getValue
import ic.struct.value.cached.Cached
import ic.system.impl.systemEngine


val osType : OsType by Cached {

	systemEngine.getOsType()

}
package ic.system.ostype


sealed class OsType {

	sealed class Linux : OsType() {

		object RedHatBased : Linux()

		object DebianBased : Linux()

		object ArchBased : Linux()

		object Other : Linux()

	}

	object Android : OsType()

	object Ios : OsType()

}
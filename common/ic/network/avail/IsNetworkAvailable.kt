package ic.network.avail


inline val isNetworkAvailable get() = networkType != null
package ic.network.avail


import ic.base.platform.basePlatform
import ic.design.task.Task


fun listenNetworkAvailability (
	callback : NetworkAvailabilityCallback,
	toCallAtOnce : Boolean
) : Task {
	return basePlatform.networkEngine.listenNetworkAvailability(
		toCallAtOnce = toCallAtOnce,
		callback = callback
	)
}


inline fun listenNetworkAvailability (
	toCallAtOnce : Boolean,
	crossinline onConnectionEstablished : (NetworkType) -> Unit,
	crossinline onConnectionLost : () -> Unit
) : Task {
	return listenNetworkAvailability(
		toCallAtOnce = toCallAtOnce,
		callback = NetworkAvailabilityCallback(
			onConnectionEstablished = onConnectionEstablished,
			onConnectionLost = onConnectionLost
		)
	)
}
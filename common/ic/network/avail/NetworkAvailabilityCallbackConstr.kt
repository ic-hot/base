package ic.network.avail


inline fun NetworkAvailabilityCallback (

	crossinline onConnectionEstablished : (NetworkType) -> Unit,

	crossinline onConnectionLost : () -> Unit

) : NetworkAvailabilityCallback {

	return object : NetworkAvailabilityCallback {

		override fun onConnectionEstablished (networkType: NetworkType) {
			onConnectionEstablished(networkType)
		}

		override fun onConnectionLost() {
			onConnectionLost()
		}

	}

}
package ic.network.avail


sealed class NetworkType {

	object Mobile : NetworkType()

	object WiFi : NetworkType()

	object Wired : NetworkType()

}

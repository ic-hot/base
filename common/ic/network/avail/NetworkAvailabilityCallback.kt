package ic.network.avail


interface NetworkAvailabilityCallback {

	fun onConnectionEstablished (networkType: NetworkType)

	fun onConnectionLost()

}
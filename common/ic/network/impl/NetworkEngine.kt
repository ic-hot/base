package ic.network.impl


import ic.design.task.Task
import ic.network.avail.NetworkAvailabilityCallback
import ic.network.avail.NetworkType
import ic.network.http.impl.HttpClient


internal interface NetworkEngine {


	val networkType : NetworkType?


	fun listenNetworkAvailability (
		toCallAtOnce : Boolean,
		callback : NetworkAvailabilityCallback
	) : Task


	val httpClient : HttpClient


}
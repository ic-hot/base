package ic.network.socket


import ic.math.annotations.BetweenInt
import ic.base.primitives.int32.Int32


data class SocketAddress (

	val host : String,

	@BetweenInt(0, 65537)
	val port : Int32

)
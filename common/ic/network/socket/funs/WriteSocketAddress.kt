package ic.network.socket.funs


import ic.network.socket.SocketAddress
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeInt32
import ic.stream.output.ext.writeString


fun ByteOutput.writeSocketAddress (socketAddress: SocketAddress) {

	writeString(socketAddress.host)

	writeInt32(socketAddress.port)

}
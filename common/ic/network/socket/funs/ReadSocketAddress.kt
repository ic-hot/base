package ic.network.socket.funs


import ic.network.socket.SocketAddress
import ic.stream.input.ByteInput
import ic.stream.input.ext.readInt32
import ic.stream.input.ext.readString


fun ByteInput.readSocketAddress() : SocketAddress {

	return SocketAddress(
		host = readString(),
		port = readInt32()
	)

}
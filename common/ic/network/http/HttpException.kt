package ic.network.http


import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse
import ic.util.text.charset.ext.bytesToString


class HttpException : Exception {

	val request 	: HttpRequest
	val response 	: HttpResponse

	constructor (request: HttpRequest, response: HttpResponse) : super(
		"\n" +
		"request: $request\n" +
		"response: $response"
	) {
		this.request = request
		this.response = response
	}

	constructor (request: HttpRequest, response: HttpResponse, cause: Throwable?) : super(
		"\n" +
		"request: $request\n" +
		"response: $response",
		cause
	) {
		this.request = request
		this.response = response
	}

	class Runtime : RuntimeException {

		val request : HttpRequest
		val response: HttpResponse

		constructor (request: HttpRequest, response: HttpResponse) : super(
			response.charset.bytesToString(response.body)
		) {
			this.request = request
			this.response = response
		}

		constructor (httpException: HttpException) : super(
			httpException.response.charset.bytesToString(httpException.response.body),
			httpException
		) {
			request = httpException.request
			response = httpException.response
		}

	}

}
package ic.network.http.job


import ic.network.http.response.HttpResponse


sealed class HttpRequestFailure {


	object ConnectionFailure : HttpRequestFailure()


	class HttpError (

		val response : HttpResponse

	) : HttpRequestFailure() {

		override fun toString() = "HttpError ( response: $response )"

	}


}
package ic.network.http.job


import ic.base.primitives.int32.Int32
import ic.util.log.LogLevel


class SendHttpRequestParams (

	val connectTimeoutMs : Int32 = 16384,

	val readTimeoutMs : Int32 = 16384,

	val toValidateSslCertificate : Boolean = true,

	val successLogLevel 			: LogLevel = LogLevel.Debug,
	val connectionFailureLogLevel 	: LogLevel = LogLevel.Debug,
	val httpErrorLogLevel 			: LogLevel = LogLevel.Warning

) {

	companion object {

		val DEFAULT = SendHttpRequestParams()

	}

}
package ic.network.http.job


import ic.base.primitives.int32.Int32
import ic.network.http.request.HttpRequest
import ic.util.log.LogLevel


class SendHttpRequestInput (

	val request : HttpRequest,

	val connectTimeoutMs 	: Int32 = 16384,
	val readTimeoutMs 		: Int32 = 16384,

	val toValidateSslCertificate : Boolean = true,

	val connectionFailureLogLevel 	: LogLevel = LogLevel.Debug,
	val httpErrorLogLevel 			: LogLevel = LogLevel.Debug,
	val successLogLevel				: LogLevel = LogLevel.Debug

)
package ic.network.http.auth.bearer


import ic.text.input.TextInput
import ic.base.throwables.UnableToParseException
import ic.network.http.auth.HttpAuthorization


class BearerHttpAuthorization (

	val bearer : String

) : HttpAuthorization {


	override val asHeaderString get() = "Bearer $bearer"


	companion object {

		@Throws(UnableToParseException::class)
		internal fun parseOrThrowUnableToParse (iterator: TextInput) : BearerHttpAuthorization {
			return BearerHttpAuthorization(iterator.readToString())
		}

	}


}
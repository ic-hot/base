package ic.network.http.auth


interface HttpAuthorization {

	val asHeaderString : String

	companion object

}
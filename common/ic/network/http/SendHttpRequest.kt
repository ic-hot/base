package ic.network.http


import ic.base.annotations.Blocking
import ic.base.primitives.int32.Int32
import ic.base.reflect.ext.className
import ic.base.throwables.IoException
import ic.ifaces.mutable.ext.synchronized
import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse
import ic.network.http.response.HttpResponse.Companion.STATUS_PERMANENT_REDIRECT
import ic.util.log.LogLevel
import ic.util.log.globalLogger
import ic.util.log.log


@Blocking
@Throws(IoException::class, HttpException::class)
fun sendHttpRequest (

	request : HttpRequest,

	connectTimeoutMs : Int32 = 16384,
	readTimeoutMs    : Int32 = 16384,

	toIgnoreTrustCertificate : Boolean = false,

	successLogLevel 			: LogLevel = LogLevel.Debug,
	connectionFailureLogLevel 	: LogLevel = LogLevel.Info,
	httpErrorLogLevel 			: LogLevel = LogLevel.Warning,

) : HttpResponse {

	if (connectTimeoutMs <= 0) throw IoException
	if (readTimeoutMs    <= 0) throw IoException

	val response = try {

		ic.base.platform.basePlatform.networkEngine.httpClient.sendHttpRequest(
			request 					= request,
			connectTimeoutMs 			= connectTimeoutMs,
			readTimeoutMs 				= readTimeoutMs,
			toIgnoreTrustCertificate 	= toIgnoreTrustCertificate
		)

	} catch (_: IoException) {

		log(connectionFailureLogLevel, "SendHttpRequest") {
			"ConnectionFailure\n" +
			"request: $request\n"
		}

		throw IoException

	} catch (e: HttpException) {

		val response = e.response

		if (response.statusCode == STATUS_PERMANENT_REDIRECT) {
			val location = response.headers["Location"]
			if (location != null) {
				return sendHttpRequest(
					request = request.copy(urlString = location),
					connectTimeoutMs = connectTimeoutMs,
					readTimeoutMs = readTimeoutMs,
					toIgnoreTrustCertificate = toIgnoreTrustCertificate,
					successLogLevel = successLogLevel,
					connectionFailureLogLevel = connectionFailureLogLevel,
					httpErrorLogLevel = httpErrorLogLevel
				)
			}
		}

		globalLogger.synchronized {
			log(httpErrorLogLevel, "SendHttpRequest") {
				"HttpException\n" +
				"request: $request\n"
			}
			log(httpErrorLogLevel, "SendHttpRequest") {
				"HttpException\n" +
				"response: ${ e.response }"
			}
		}

		throw e

	}

	log(successLogLevel, "SendHttpRequest") {
		"Success\n" +
		"request: $request\n" +
		"response: $response"
	}

	return response

}
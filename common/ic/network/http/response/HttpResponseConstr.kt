@file:Suppress("NOTHING_TO_INLINE")


package ic.network.http.response


import ic.base.primitives.int32.Int32
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.util.text.charset.Charset
import ic.util.mimetype.MimeType


inline fun HttpResponse (

	statusCode : Int32 = HttpResponse.STATUS_OK,

	contentType : MimeType? = MimeType.Html,

	charset : Charset = Charset.defaultHttp,

	headers : FiniteMap<String, String> = FiniteMap(),

	body : ByteSequence = EmptyByteSequence

) : HttpResponse {

	return ConstantHttpResponse(
		statusCode = statusCode,
		contentType = contentType,
		charset = charset,
		headers = headers,
		body = body
	)

}
package ic.network.http.response


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.util.text.charset.Charset
import ic.util.mimetype.MimeType
import ic.util.text.charset.ext.bytesToString


abstract class HttpResponse {

	open val statusCode : Int get() = STATUS_OK

	open val statusMessage : String get() {
		val status = statusCode
		return when (status) {
			STATUS_OK 					-> "200 OK"
			STATUS_NO_CONTENT			-> "204 No Content"
			STATUS_MOVED_PERMANENTLY 	-> "301 Moved Permanently"
			else 						-> status.toString()
		}
	}

	open val contentType : MimeType? get() = MimeType.Html

	open val charset : Charset get() = Charset.defaultHttp

	open val headers : FiniteMap<String, String> get() = FiniteMap()

	open val body : ByteSequence get() = EmptyByteSequence

	override fun toString() = "HttpResponse { " +
		"statusCode: $statusCode, " +
		"headers: $headers, " +
		"body: $bodyAsString " +
	"}"

	val bodyAsString : String get() = charset.bytesToString(body)

	companion object {

		const val STATUS_OK = 200
		const val STATUS_NO_CONTENT = 204
		const val STATUS_MOVED_PERMANENTLY = 301
		const val STATUS_PERMANENT_REDIRECT = 308
		const val STATUS_BAD_REQUEST = 400
		const val STATUS_UNAUTHORIZED = 401
		const val STATUS_FORBIDDEN = 403
		const val STATUS_NOT_FOUND = 404
		const val STATUS_SERVICE_UNAVAILABLE = 503

	}

}

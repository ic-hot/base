package ic.network.http.response


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.util.mimetype.MimeType
import ic.util.text.charset.Charset


data class ConstantHttpResponse (

	override val statusCode : Int = STATUS_OK,

	override val contentType : MimeType? = MimeType.Html,

	override val charset : Charset = Charset.defaultHttp,

	override val headers : FiniteMap<String, String> = FiniteMap(),

	override val body : ByteSequence = EmptyByteSequence

) : HttpResponse() {

	override fun toString() = super.toString()

}
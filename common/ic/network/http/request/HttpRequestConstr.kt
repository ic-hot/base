@file:Suppress("FunctionName")


package ic.network.http.request


import ic.network.http.auth.HttpAuthorization
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.plus
import ic.util.mimetype.MimeType
import ic.util.url.Url


inline fun HttpRequest (
	method : String = "GET",
	urlString : String,
	contentType : MimeType? = null,
	authorization : HttpAuthorization? = null,
	accept : MimeType? = null,
	headers : FiniteMap<String, String> = FiniteMap(),
	body : ByteSequence = EmptyByteSequence
) : HttpRequest {
	var modifiedHeaders = headers
	if (contentType != null) {
		modifiedHeaders += "Content-Type" to contentType.name
	}
	if (authorization != null) {
		modifiedHeaders += "Authorization" to authorization.asHeaderString
	}
	if (accept != null) {
		modifiedHeaders += "Accept" to accept.name
	}
	return HttpRequest(
		method = method,
		urlString = urlString,
		headers = modifiedHeaders,
		body = body
	)
}


inline fun HttpRequest (
	method : String = "GET",
	url : Url,
	authorization : HttpAuthorization? = null,
	accept : MimeType? = null,
	headers : FiniteMap<String, String> = FiniteMap(),
	contentType : MimeType? = null,
	body : ByteSequence = EmptyByteSequence
) : HttpRequest {
	return HttpRequest(
		method = method,
		urlString = url.toString(),
		authorization = authorization,
		accept = accept,
		contentType = contentType,
		headers = headers,
		body = body
	)
}


inline fun HttpRequest (
	method : String = "GET",
	baseUrl : String,
	urlParams : FiniteMap<String, String>,
	authorization : HttpAuthorization? = null,
	accept : MimeType? = null,
	contentType : MimeType? = null,
	headers : FiniteMap<String, String> = FiniteMap(),
	body : ByteSequence = EmptyByteSequence
) : HttpRequest {
	return HttpRequest(
		method = method,
		url = Url.fromBaseUrlAndParams(baseUrl, urlParams),
		authorization = authorization,
		headers = headers,
		accept = accept,
		contentType = contentType,
		body = body
	)
}
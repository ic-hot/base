package ic.network.http.request


import ic.base.arrays.bytes.ext.toString
import ic.base.arrays.ext.length
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.struct.map.finite.FiniteMap
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.bytesToString


data class HttpRequest (

	val method : String = "GET",

	val urlString : String,

	val headers : FiniteMap<String, String>,

 	val body : ByteSequence

) {

	val bodyAsString get() = Charset.defaultHttp.bytesToString(body)

	override fun toString() : String {
		val bodyAsArray = body.toByteArray()
		val bodyAsString = (
			if (bodyAsArray.length < 16384) {
				bodyAsArray.toString(charset = Charset.defaultHttp)
			} else {
				"<LargeBinary>"
			}
		)
		return "HttpRequest { " +
			"method: $method, " +
			"urlString: $urlString, " +
			"headers: $headers, " +
			"body: $bodyAsString " +
		"}"
	}

}
package ic.network.http.impl


import ic.base.annotations.Blocking
import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.network.http.HttpException
import ic.network.http.request.HttpRequest
import ic.network.http.response.HttpResponse


internal interface HttpClient {

	@Blocking
	@Throws(IoException::class, HttpException::class)
	fun sendHttpRequest (
		request 					: HttpRequest,
		connectTimeoutMs			: Int32,
		readTimeoutMs				: Int32,
		toIgnoreTrustCertificate : Boolean
	) : HttpResponse

}
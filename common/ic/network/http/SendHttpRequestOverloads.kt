package ic.network.http


import ic.base.throwables.IoException
import ic.network.http.auth.HttpAuthorization
import ic.network.http.request.HttpRequest
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.empty.EmptyByteSequence
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.Empty
import ic.util.log.LogLevel
import ic.util.mimetype.MimeType
import ic.util.url.Url


@Throws(IoException::class, HttpException::class)
fun sendHttpRequest (
	method : String = "GET",
	url : Url,
	headers : FiniteMap<String, String> = FiniteMap.Empty(),
	contentType : MimeType? = null,
	body : ByteSequence = EmptyByteSequence,
	connectTimeoutMs : Int = 16384,
	readTimeoutMs    : Int = 16384,
	toIgnoreTrustCertificate : Boolean = false
) = sendHttpRequest(
	request = HttpRequest(
		method = method,
		url = url,
		headers = headers,
		contentType = contentType,
		body = body
	),
	connectTimeoutMs = connectTimeoutMs,
	readTimeoutMs = readTimeoutMs,
	toIgnoreTrustCertificate = toIgnoreTrustCertificate
)


@Throws(IoException::class, HttpException::class)
fun sendHttpRequest (
	method : String = "GET",
	baseUrl : String, urlParams : FiniteMap<String, String>,
	headers : FiniteMap<String, String> = FiniteMap.Empty(),
	authorization : HttpAuthorization? = null,
	contentType : MimeType? = null,
	body : ByteSequence = EmptyByteSequence,
	connectTimeoutMs : Int = 16384,
	readTimeoutMs    : Int = 16384,
	toIgnoreTrustCertificate : Boolean = false,
	successLogLevel 			: LogLevel = LogLevel.Debug,
	connectionFailureLogLevel 	: LogLevel = LogLevel.Info,
	httpErrorLogLevel 			: LogLevel = LogLevel.Warning
) = sendHttpRequest(
	request = HttpRequest(
		method = method,
		baseUrl = baseUrl, urlParams = urlParams,
		authorization = authorization,
		headers = headers,
		contentType = contentType,
		body = body
	),
	connectTimeoutMs = connectTimeoutMs,
	readTimeoutMs = readTimeoutMs,
	toIgnoreTrustCertificate = toIgnoreTrustCertificate,
	successLogLevel = successLogLevel,
	connectionFailureLogLevel = connectionFailureLogLevel,
	httpErrorLogLevel = httpErrorLogLevel
)


@Throws(IoException::class, HttpException::class)
fun sendHttpRequest (
	urlString : String,
	method : String = "GET",
	contentType : MimeType? = null,
	accept : MimeType? = null,
	headers : FiniteMap<String, String> = FiniteMap.Empty(),
	body : ByteSequence = EmptyByteSequence,
	connectTimeoutMs : Int = 16384,
	readTimeoutMs    : Int = 16384,
	toIgnoreTrustCertificate : Boolean = false,
	successLogLevel           : LogLevel = LogLevel.Debug,
	connectionFailureLogLevel : LogLevel = LogLevel.Debug,
	httpErrorLogLevel         : LogLevel = LogLevel.Warning,
) = sendHttpRequest(
	request = HttpRequest(
		method = method,
		urlString = urlString,
		contentType = contentType,
		accept = accept,
		headers = headers,
		body = body
	),
	connectTimeoutMs = connectTimeoutMs,
	readTimeoutMs = readTimeoutMs,
	toIgnoreTrustCertificate = toIgnoreTrustCertificate,
	successLogLevel = successLogLevel,
	connectionFailureLogLevel = connectionFailureLogLevel,
	httpErrorLogLevel = httpErrorLogLevel
)
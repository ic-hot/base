package ic.parallel.annotations


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.CONSTRUCTOR,
	AnnotationTarget.VALUE_PARAMETER
)
annotation class BackgroundThread (val value: String = "")
package ic.parallel.beam


import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.loop.repeat
import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.funs.min
import ic.parallel.beam.impl.BeamThread
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.parallel.thread.impl.StopThread
import ic.service.ext.startBlocking
import ic.service.nonblockingstart.NonBlockingStartService
import ic.struct.collection.ext.copy.copy
import ic.struct.collection.ext.copy.copyFilter
import ic.struct.collection.ext.count.isEmpty
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


abstract class Beam : NonBlockingStartService(isReusable = false) {


	private inline val thisBeam get() = this


	protected abstract fun BeamThread.toDoInParallel (actionIndex: Int64)

	protected abstract val maxThreadsCount : Int32

	protected open val maxActionsCount : Int64 get() = Int64.MAX_VALUE


	private val beamMutex = Mutex()

	private val threads = EditableSet<BeamThread>()

	private var nextActionIndex : Int64 = 0


	override fun implementStartNonBlocking() {
		val maxActionsCount = this.maxActionsCount
		val maxThreadsCount = min(this.maxThreadsCount, maxActionsCount)
		if (maxThreadsCount == 0L) {
			notifyFinished()
			return
		}
		beamMutex.synchronized {
			repeat(maxThreadsCount) {
				val thread = BeamThread {
					try {
						skippable {
							loop {
								if (this.toStop) skip
								val actionIndex = beamMutex.synchronized { nextActionIndex++ }
								if (actionIndex >= maxActionsCount) skip
								toDoInParallel(actionIndex = actionIndex)
							}
						}
					} finally {
						val isBeamFinished = beamMutex.synchronized {
							threads.remove(this)
							threads.isEmpty
						}
						if (isBeamFinished) {
							thisBeam.notifyFinished()
						}
					}
				}
				beamMutex.synchronized {
					threads.add(thread)
				}
				thread.startBlocking()
			}
		}
	}


	internal fun stopAllThreadsNonBlocking (except: BeamThread? = null) {
		val threads = beamMutex.synchronized {
			this.threads.copyFilter { it != except }
		}
		threads.forEach { thread ->
			thread.stopNonBlockingIfNeeded()
		}
	}


	override fun implementStopNonBlocking() {
		stopAllThreadsNonBlocking()
	}


	override fun implementWaitFor() {
		try {
			val threads = beamMutex.synchronized { this.threads.copy() }
			threads.forEach { thread ->
				thread.waitFor()
			}
		} catch (_: StopThread) {
			val threads = beamMutex.synchronized { this.threads.copy() }
			threads.forEach { thread ->
				thread.stopNonBlockingIfNeeded()
			}
			throw StopThread
		}
	}


}
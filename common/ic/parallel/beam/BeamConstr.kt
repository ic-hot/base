@file:Suppress("FunctionName")


package ic.parallel.beam


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.parallel.beam.impl.BeamThread


inline fun Beam (

	actionsCount : Int64 = Int64.MAX_VALUE,

	maxThreadsCount : Int32,

	crossinline toDoInParallel : BeamThread.(actionIndex: Int64) -> Unit

) : Beam {

	return object : Beam() {

		override val maxThreadsCount get() = maxThreadsCount

		override val maxActionsCount get() = actionsCount

		override fun BeamThread.toDoInParallel (actionIndex: Int64) = toDoInParallel(actionIndex)

	}

}


inline fun Beam (

	maxActionsCount : Int32,

	maxThreadsCount : Int32,

	crossinline toDoInParallel : BeamThread.(actionIndex: Int32) -> Unit

) = (
	Beam(
		actionsCount = maxActionsCount.asInt64,
		maxThreadsCount = maxThreadsCount,
		toDoInParallel = { actionIndex ->
			toDoInParallel(actionIndex.asInt32)
		}
	)
)
#ifndef ic_parallel_beam_impl_ThreadInBeam_h_included
#define ic_parallel_beam_impl_ThreadInBeam_h_included


#include <pthread.h>


#include "../../../base/primitives/int32/Int32.h"
#include "../Beam.h"
#include "../../thread/Thread.h"


struct ThreadInBeam {
	Beam* beam;
	Int32 threadIndex;
	Thread thread;
};

typedef struct ThreadInBeam ThreadInBeam;


#endif
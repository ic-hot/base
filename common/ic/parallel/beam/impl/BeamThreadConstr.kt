package ic.parallel.beam.impl


import ic.parallel.beam.Beam


internal inline fun Beam.BeamThread (

	crossinline action : BeamThread.() -> Unit

) : BeamThread {

	return object : BeamThread(beam = this) {

		override fun toDoInBackground() {
			action()
		}

	}

}
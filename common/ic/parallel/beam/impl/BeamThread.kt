package ic.parallel.beam.impl


import ic.parallel.beam.Beam
import ic.parallel.thread.Thread


abstract class BeamThread

	internal constructor (private val beam: Beam)

	: Thread()

{


	internal var toStop : Boolean = false


	fun stopOtherThreadsInBeamNonBlocking() {

		beam.stopAllThreadsNonBlocking(except = this)

	}


	fun stopAllThreadsInBeamNonBlocking() {

		beam.stopAllThreadsNonBlocking()

	}


	override fun implementStopNonBlocking() {
		toStop = true
		super.implementStopNonBlocking()
	}


}
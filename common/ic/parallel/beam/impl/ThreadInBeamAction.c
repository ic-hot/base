#ifndef ic_parallel_beam_impl_ThreadInBeamAction_c_included
#define ic_parallel_beam_impl_ThreadInBeamAction_c_included


#include "../../../base/objects/Ref.h"
#include "../../../base/primitives/bool/Boolean.h"
#include "../../../base/primitives/int64/Int64.h"
#include "../../../base/primitives/void/Void.h"
#include "../../mutex/funs/ReleaseMutex.c"
#include "../../mutex/funs/SeizeMutex.c"
#include "../Beam.h"
#include "ThreadInBeam.h"


Void threadInBeamAction (ThreadInBeam* threadInBeam) {

	Beam* beam = (*threadInBeam).beam;

	Void (*action)(Int64, Ref) 	= (*beam).action;
	Ref arg 					= (*beam).arg;
	Int64 actionsCount 			= (*beam).actionsCount;

	Mutex* mutex = &( (*beam).mutex );

	Int64 actionIndex;

	while (TRUE) {

		seizeMutex(mutex);
        actionIndex = (*beam).nextActionIndex++;
        releaseMutex(mutex);

        if (actionIndex >= actionsCount) return;

        (*action)(actionIndex, arg);

	}

}


#endif
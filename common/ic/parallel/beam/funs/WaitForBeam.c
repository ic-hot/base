#ifndef ic_parallel_beam_funs_WaitForBeam_c_included
#define ic_parallel_beam_funs_WaitForBeam_c_included


#include <stdlib.h>

#include "../../../base/primitives/int32/Int32.h"
#include "../Beam.h"
#include "../impl/ThreadInBeam.h"
#include "../../mutex/funs/CloseMutex.c"
#include "../../thread/funs/WaitForThread.c"


Void waitForBeam (Beam* beam) {

	Int32 threadsCount = (*beam).threadsCount;

	ThreadInBeam* threads = (*beam).threads;

	Mutex* mutex = &( (*beam).mutex );

	for (Int32 threadIndex = 0; threadIndex < threadsCount; threadIndex++) {

		waitForThread(
			&( threads[threadIndex].thread )
		);

	}

	free(threads);

	closeMutex(mutex);

}


#endif
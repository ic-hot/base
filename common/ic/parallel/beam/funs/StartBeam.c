#ifndef ic_parallel_beam_funs_StartBeam_c_included
#define ic_parallel_beam_funs_StartBeam_c_included


#include <stdlib.h>

#include "../../../base/primitives/int32/Int32.h"
#include "../Beam.h"
#include "../impl/ThreadInBeam.h"
#include "../impl/ThreadInBeamAction.c"
#include "../../mutex/funs/OpenMutex.c"
#include "../../thread/funs/RunInBackground.c"


Void startBeam (Beam* beam) {

	Int32 threadsCount = (*beam).threadsCount;

	Mutex* mutex = &( (*beam).mutex );

	ThreadInBeam* threads = malloc(
		sizeof(ThreadInBeam) * threadsCount
	);

	(*beam).threads = threads;

	openMutex(mutex);

	for (Int32 threadIndex = 0; threadIndex < threadsCount; threadIndex++) {

		threads[threadIndex].beam = beam;
		threads[threadIndex].threadIndex = threadIndex;

		threads[threadIndex].thread = runInBackground(
			&threadInBeamAction,
			&( threads[threadIndex] )
		);

	}

}


#endif
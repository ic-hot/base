package ic.parallel.thread


import ic.parallel.impl.parallelEngine
import ic.service.nonblockingstart.NonBlockingStartService


abstract class Thread : NonBlockingStartService(isReusable = false) {


	protected abstract fun toDoInBackground()


	internal fun runBlocking() {
		try {
			toDoInBackground()
		} finally {
			notifyFinished()
		}
	}


	@Suppress("LeakingThis")
	private val implementation = parallelEngine.createThreadImplementation(thread = this)

	val isCurrentThread get() = implementation.isCurrentThread

	override fun implementStartNonBlocking() {
		implementation.startNonBlocking()
	}

	override fun implementStopNonBlocking() {
		implementation.stopNonBlocking()
	}

	override fun implementWaitFor() {
		implementation.waitFor()
	}


}

@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.parallel.thread


inline fun Thread (

	crossinline action : () -> Unit

) : Thread {

	return object : Thread() {

		override fun toDoInBackground() {
			action()
		}

	}

}
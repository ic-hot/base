#ifndef ic_parallel_thread_Thread_h_included
#define ic_parallel_thread_Thread_h_included


#include <pthread.h>


struct Thread {
	pthread_t id;
};

typedef struct Thread Thread;


#endif
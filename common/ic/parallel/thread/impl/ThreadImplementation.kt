package ic.parallel.thread.impl


import ic.parallel.thread.Thread


abstract class ThreadImplementation (

	protected val thread : Thread

) {

	abstract val isCurrentThread : Boolean

	abstract fun startNonBlocking()

	abstract fun stopNonBlocking()

	abstract fun waitFor()

}
#ifndef ic_parallel_thread_funs_RunInBackground_c_included
#define ic_parallel_thread_funs_RunInBackground_c_included


#include <pthread.h>


#include "../../../base/objects/Ref.h"
#include "../../../base/primitives/void/Void.h"
#include "../Thread.h"


Thread runInBackground (
	Void (*action)(Ref),
	Ref arg
) {
	Thread thread;
	pthread_create( &(thread.id), NULL, action, arg );
	return thread;
}


#endif
#ifndef ic_parallel_thread_funs_WaitForThread_c_included
#define ic_parallel_thread_funs_WaitForThread_c_included


#include <pthread.h>

#include "../../../base/primitives/void/Void.h"
#include "../Thread.h"


Void waitForThread (Thread* thread) {

	pthread_join((*thread).id, NULL);

}


#endif
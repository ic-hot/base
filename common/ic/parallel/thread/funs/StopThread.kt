package ic.parallel.thread.funs


import ic.parallel.thread.impl.StopThread


@Deprecated("Use stopThread")
@Suppress("FunctionName")
fun StopThread() : Nothing = throw StopThread


val stopThread : Nothing get() = throw StopThread
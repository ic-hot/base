package ic.parallel.mutex.log


import ic.base.primitives.int64.ext.asFixedSizeHexString
import ic.math.rand.randomInt64
import ic.parallel.mutex.Mutex
import ic.util.log.LogLevel
import ic.util.log.Logger
import ic.util.log.StackTrace
import ic.util.log.globalLogger


class LogMutex (

	private val name : String = randomInt64().asFixedSizeHexString,

	private val logger : Logger = globalLogger,

	private val logLevel : LogLevel = LogLevel.Debug,

	private val toEnableStackTrace : Boolean = false

) : Mutex {


	private val mutex = Mutex()


	override fun seize() {
		val seizeId = randomInt64().asFixedSizeHexString
		if (toEnableStackTrace) {
			logger.log(logLevel, "Mutex") { "$name Seize $seizeId\n$StackTrace" }
		} else {
			logger.log(logLevel, "Mutex") { "$name Seize $seizeId" }
		}
		mutex.seize()
		logger.log(logLevel, "Mutex") { "$name Seize $seizeId Success" }
	}


	override fun release() {
		logger.log(logLevel, "Mutex") { "$name Release" }
		mutex.release()
	}

}
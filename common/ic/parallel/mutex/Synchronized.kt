package ic.parallel.mutex


import kotlin.jvm.JvmName


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.CONSTRUCTOR,
	AnnotationTarget.VALUE_PARAMETER
)
annotation class Synchronized


@JvmName("synchronizedAsMutexExtension")
inline fun <Result> Mutex.synchronized (

	@Synchronized
	block : () -> Result

) : Result {

	seize()
	return try {
		block()
	} finally {
		release()
	}

}


@JvmName("synchronizedAsMutexExtension")
inline fun <Result> Mutex.synchronized (

	beforeSeize  : () -> Unit,
	afterSeize   : () -> Unit,
	onRelease    : () -> Unit,

	@Synchronized
	block : () -> Result

) : Result {

	beforeSeize()
	seize()
	afterSeize()
	return try {
		block()
	} finally {
		onRelease()
		release()
	}

}


inline fun <Result> synchronized (mutex: Mutex, @Synchronized block : () -> Result) : Result {
	return mutex.synchronized(block)
}


inline fun <Result> synchronized (
	mutex1 : Mutex,
	mutex2 : Mutex,
	@Synchronized block : () -> Result
) : Result {
	return mutex1.synchronized {
		mutex2.synchronized(block)
	}
}
package ic.parallel.mutex


interface Mutex {

	fun seize()

	fun release()

}
package ic.parallel.mutex


import ic.parallel.impl.parallelEngine


fun Mutex() : Mutex = parallelEngine.createMutex()
#ifndef ic_parallel_mutex_Mutex_h_included
#define ic_parallel_mutex_Mutex_h_included


#include <pthread.h>


struct Mutex {
	pthread_mutex_t pthreadMutex;
};

typedef struct Mutex Mutex;


#endif
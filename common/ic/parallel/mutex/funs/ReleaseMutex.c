#ifndef ic_parallel_mutex_funs_ReleaseMutex_c_included
#define ic_parallel_mutex_funs_ReleaseMutex_c_included


#include <pthread.h>

#include "../../../base/primitives/void/Void.h"
#include "../Mutex.h"


Void releaseMutex (Mutex* mutex) {

	pthread_mutex_unlock(
		&(
			(*mutex).pthreadMutex
		)
	);

}


#endif
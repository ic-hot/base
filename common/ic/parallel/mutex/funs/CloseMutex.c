#ifndef ic_parallel_mutex_funs_CloseMutex_c_included
#define ic_parallel_mutex_funs_CloseMutex_c_included


#include <pthread.h>

#include "../../../base/primitives/void/Void.h"
#include "../Mutex.h"


Void closeMutex (Mutex* mutex) {

	pthread_mutex_destroy(
		&(
			(*mutex).pthreadMutex
		)
	);

}


#endif
#ifndef ic_parallel_mutex_funs_OpenMutex_c_included
#define ic_parallel_mutex_funs_OpenMutex_c_included


#include <pthread.h>

#include "../../../base/primitives/void/Void.h"
#include "../Mutex.h"


Void openMutex (Mutex* mutex) {

	pthread_mutex_init(
		&(
			(*mutex).pthreadMutex
		),
		NULL
	);

}


#endif
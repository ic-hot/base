#ifndef ic_parallel_mutex_funs_SeizeMutex_c_included
#define ic_parallel_mutex_funs_SeizeMutex_c_included


#include <pthread.h>

#include "../../../base/primitives/void/Void.h"
#include "../Mutex.h"


Void seizeMutex (Mutex* mutex) {

	pthread_mutex_lock(
		&(
			(*mutex).pthreadMutex
		)
	);

}


#endif
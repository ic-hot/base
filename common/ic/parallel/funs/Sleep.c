#ifndef ic_parallel_funs_Sleep_c_included
#define ic_parallel_funs_Sleep_c_included


#include <unistd.h>

#include "../../base/primitives/void/Void.h"
#include "../../base/primitives/int64/Int64.h"


Void sleepMs (Int64 ms) {

	usleep(ms * 1000);

}


#endif
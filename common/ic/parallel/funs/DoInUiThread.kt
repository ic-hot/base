package ic.parallel.funs


import ic.base.platform.basePlatform
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable


fun doInUiThread (action: Action) : Cancelable {
	return basePlatform.parallelEngine.doInUiThread(action)
}


inline fun doInUiThread (crossinline action: () -> Unit) : Cancelable {
	return doInUiThread(
		Action(action)
	)
}


@Deprecated("Use doInUiThread")
inline fun doCallback (crossinline action : () -> Unit) = doInUiThread(action)


@Deprecated("Use doInUiThread")
inline fun callback (crossinline action : () -> Unit) = doInUiThread(action)
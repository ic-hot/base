package ic.parallel.funs


import ic.parallel.thread.Thread
import ic.service.ext.startBlocking


inline fun doInBackground (
	crossinline action : () -> Unit
) : Thread {
	return Thread(
		action = action
	).apply { startBlocking() }
}
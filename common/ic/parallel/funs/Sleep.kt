package ic.parallel.funs


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.system.funs.getCurrentEpochTimeMs
import ic.util.time.duration.Duration


fun sleep (durationMs: Int64) {
	ic.base.platform.basePlatform.parallelEngine.sleep(durationMs = durationMs)
}


@Suppress("NOTHING_TO_INLINE")
inline fun sleep (durationMs: Int32) = sleep(durationMs = durationMs.asInt64)


@Suppress("NOTHING_TO_INLINE")
inline fun sleep (duration: Duration) = sleep(durationMs = duration.inMs)


inline fun sleepWhile (
	stepDurationMs : Int64 = 16,
	condition : () -> Boolean
) {
	while (condition()) {
		sleep(durationMs = stepDurationMs)
	}
}

inline fun sleepTill (stepDurationMs : Int64 = 16, condition: () -> Boolean) {
	sleepWhile(stepDurationMs = stepDurationMs) {
		!condition()
	}
}


inline fun sleepWhile (
	stepDurationMs : Int64 = 16,
	timeoutMs : Int64,
	onTimeout : () -> Unit = {},
	onFinish : () -> Unit = {},
	condition : () -> Boolean
) {
	val deadline = getCurrentEpochTimeMs() + timeoutMs
	while (condition()) {
		if (getCurrentEpochTimeMs() >= deadline) {
			onTimeout()
			return
		}
		sleep(durationMs = stepDurationMs)
	}
	onFinish()
}


inline fun sleepTill (
	stepDurationMs : Int64 = 16,
	timeoutMs : Int64,
	onTimeout : () -> Unit,
	onFinish : () -> Unit,
	condition : () -> Boolean
) {
	sleepWhile(
		stepDurationMs = stepDurationMs,
		timeoutMs = timeoutMs,
		onTimeout = onTimeout,
		onFinish = onFinish
	) {
		!condition()
	}
}
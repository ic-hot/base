package ic.parallel.funs.doinparallel


import ic.parallel.thread.Thread
import ic.service.ext.startBlocking
import ic.struct.array.Array
import ic.struct.array.ext.set
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvertToListIndexed
import ic.struct.collection.ext.count.count
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach


fun <Result> doInParallel (actions: Collection<() -> Result>) : List<Result> {
	var thrown : Throwable? = null
	val results = Array<Result>(length = actions.count)
	val threads = actions.copyConvertToListIndexed { index, action: () -> Result ->
		Thread {
			try {
				results[index] = action()
			} catch (t: Throwable) {
				if (thrown == null) thrown = t
			}
		}
	}
	threads.forEach { it.startBlocking() }
	threads.forEach { it.waitFor() }
	if (thrown != null) throw thrown!!
	return results
}

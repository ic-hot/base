package ic.parallel.funs.doinparallel


import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.parallel.thread.Thread
import ic.parallel.thread.impl.StopThread
import ic.service.ext.startNonBlocking


fun <Result1, Result2, Result3> doInParallel (
	action1 : () -> Result1,
	action2 : () -> Result2,
	action3 : () -> Result3
) : Triple<Result1, Result2, Result3> {
	var result1 : Result1? = null
	var result2 : Result2? = null
	var result3 : Result3? = null
	var thrown : Throwable? = null
	val thread2 = Thread {
		try {
			result2 = action2()
		} catch (t: Throwable) {
			thrown = t
		}
	}
	val thread3 = Thread {
		try {
			result3 = action3()
		} catch (t: Throwable) {
			thrown = t
		}
	}
	thread2.startNonBlocking()
	thread3.startNonBlocking()
	try {
		result1 = action1()
	} catch (_: StopThread) {
		thread2.stopNonBlockingIfNeeded()
		thread3.stopNonBlockingIfNeeded()
	} catch (t: Throwable) {
		thrown = t
	}
	thread2.waitFor()
	thread3.waitFor()
	if (thrown == null) {
		@Suppress("UNCHECKED_CAST")
		return Triple(result1 as Result1, result2 as Result2, result3 as Result3)
	} else {
		throw thrown!!
	}
}
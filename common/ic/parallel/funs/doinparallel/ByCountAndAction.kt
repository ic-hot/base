package ic.parallel.funs.doinparallel


import ic.base.assert.assert
import ic.base.annotations.Blocking
import ic.base.primitives.int32.Int32
import ic.parallel.funs.doinbeam.doInBeamBlocking
import ic.struct.array.DefaultArray
import ic.struct.array.ext.set
import ic.struct.list.List


@Blocking
inline fun <Result> doInParallel (

	count : Int32,

	crossinline action : (threadIndex: Int32) -> Result

) : List<Result> {

	assert { count >= 0 }

	when (count) {
		0 -> {
			return List()
		}
		1 -> {
			return List(
				action(0)
			)
		}
		else -> {
			val results = DefaultArray<Result>(count)
			doInBeamBlocking(
				actionsCount = count,
				maxThreadsCount = count
			) { actionIndex ->
				results[actionIndex] = action(actionIndex)
			}
			results.freeze()
			return results
		}
	}

}
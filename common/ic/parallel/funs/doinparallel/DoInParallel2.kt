package ic.parallel.funs.doinparallel


import ic.base.assert.assert
import ic.ifaces.stoppable.ext.stopNonBlocking
import ic.parallel.thread.Thread
import ic.parallel.thread.impl.StopThread
import ic.service.ext.startBlocking


fun <Result1, Result2> doInParallel (

	action1 : () -> Result1,
	action2 : () -> Result2

) : Pair<Result1, Result2> {

	var result1 : Result1? = null
	var result2 : Result2? = null
	var isResult1Loaded : Boolean = false
	var isResult2Loaded : Boolean = false
	var thrownException : Exception? = null

	val thread2 = Thread {
		try {
			result2 = action2()
			isResult2Loaded = true
		} catch (e: Exception) {
			thrownException = e
		}
	}
	thread2.startBlocking()

	try {
		result1 = action1()
		isResult1Loaded = true
	} catch (e: Exception) {
		thrownException = e
	} catch (_: StopThread) {
		thread2.stopNonBlocking()
		thread2.waitFor()
		throw StopThread
	}

	thread2.waitFor()

	if (thrownException == null) {
		assert { isResult1Loaded && isResult2Loaded }
		@Suppress("UNCHECKED_CAST")
		return Pair(result1 as Result1, result2 as Result2)
	} else {
		throw thrownException!!
	}

}
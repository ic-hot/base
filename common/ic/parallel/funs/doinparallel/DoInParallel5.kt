package ic.parallel.funs.doinparallel


import ic.base.tuples.Tuple5
import ic.parallel.thread.Thread
import ic.service.ext.startBlocking


inline fun <Result1, Result2, Result3, Result4, Result5> doInParallel (
	crossinline action1 : () -> Result1,
	crossinline action2 : () -> Result2,
	crossinline action3 : () -> Result3,
	crossinline action4 : () -> Result4,
	crossinline action5 : () -> Result5
) : Tuple5<Result1, Result2, Result3, Result4, Result5> {
	var result1 : Result1? = null
	var result2 : Result2? = null
	var result3 : Result3? = null
	var result4 : Result4? = null
	var result5 : Result5? = null
	var thrown : Throwable? = null
	val thread1 = Thread {
		try {
			result1 = action1()
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	val thread2 = Thread {
		try {
			result2 = action2()
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	val thread3 = Thread {
		try {
			result3 = action3()
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	val thread4 = Thread {
		try {
			result4 = action4()
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	val thread5 = Thread {
		try {
			result5 = action5()
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	thread1.startBlocking()
	thread2.startBlocking()
	thread3.startBlocking()
	thread4.startBlocking()
	thread5.startBlocking()
	thread1.waitFor()
	thread2.waitFor()
	thread3.waitFor()
	thread4.waitFor()
	thread5.waitFor()
	if (thrown == null) {
		@Suppress("UNCHECKED_CAST")
		return Tuple5(
			result1 as Result1,
			result2 as Result2,
			result3 as Result3,
			result4 as Result4,
			result5 as Result5
		)
	} else {
		throw thrown!!
	}
}
package ic.parallel.funs


import ic.design.task.Task
import ic.design.task.scope.CloseableTaskScope
import ic.design.task.scope.TaskScope
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded


inline fun doInBackgroundAsTask (

	crossinline onCancel : () -> Unit = {},

	crossinline action : TaskScope.() -> Unit

) : Task {

	val taskScope = CloseableTaskScope()

	val thread = doInBackground {
		taskScope.run {
			action()
		}
	}

	return Task(
		cancel = {
			thread.stopNonBlockingIfNeeded()
			taskScope.close()
			onCancel()
		}
	)

}
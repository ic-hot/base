package ic.parallel.funs


import ic.base.primitives.int64.Int64
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.parallel.impl.parallelEngine
import ic.util.time.duration.Duration


fun schedule (

	delayMs : Int64,

	action : Action

) : Cancelable? = parallelEngine.schedule(
	action = action,
	delayMs = delayMs
)


inline fun schedule (

	delayMs : Int64,

	crossinline action : () -> Unit

) = schedule(
	delayMs = delayMs,
	action = Action(action)
)


inline fun schedule (

	delay : Duration,

	crossinline action : () -> Unit

) = schedule(
	delayMs = delay.inMs,
	action = Action(action)
)
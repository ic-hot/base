package ic.parallel.funs.blocking


import ic.base.annotations.Blocking
import ic.parallel.funs.sleepTill


@Blocking
inline fun <Result> blocking (

	crossinline action : BlockingScope<Result>.() -> Unit

) : Result {

	var isDone : Boolean = false

	var returned : Result?    = null
	var thrown   : Throwable? = null

	object : BlockingScope<Result> {

		override fun notifyReturn (result: Result) {
			returned = result
			isDone = true
		}

		override fun notifyThrow (throwable: Throwable) {
			thrown = throwable
			isDone = true
		}

		init {
			action()
		}

	}

	sleepTill { isDone }

	if (thrown != null) {
		throw thrown!!
	} else {
		@Suppress("UNCHECKED_CAST")
		return returned as Result
	}

}
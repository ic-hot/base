package ic.parallel.funs.blocking


interface BlockingScope<Result> {

	fun notifyReturn (result: Result)

	fun notifyThrow (throwable: Throwable)

}
package ic.parallel.funs.doinbeam


import ic.base.annotations.Blocking
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.parallel.beam.Beam
import ic.parallel.beam.impl.BeamThread
import ic.service.ext.startBlocking


@Blocking
inline fun doInBeamBlocking (
	actionsCount : Int64,
	maxThreadsCount : Int32,
	crossinline action : BeamThread.(actionIndex: Int64) -> Unit
) {
	if (actionsCount <= 0) return
	var thrown : Throwable? = null
	val beam = Beam(
		actionsCount = actionsCount,
		maxThreadsCount = maxThreadsCount
	) { actionIndex ->
		try {
			action(actionIndex)
		} catch (t: Throwable) {
			if (thrown == null) thrown = t
		}
	}
	beam.startBlocking()
	beam.waitFor()
	if (thrown != null) throw thrown!!
}


@Blocking
inline fun doInBeamBlocking (
	actionsCount : Int32,
	maxThreadsCount : Int32,
	crossinline action : BeamThread.(actionIndex: Int32) -> Unit
) {
	doInBeamBlocking(
		actionsCount = actionsCount.asInt64,
		maxThreadsCount = maxThreadsCount,
		action = { actionIndex: Int64 -> action(actionIndex.asInt32) }
	)
}
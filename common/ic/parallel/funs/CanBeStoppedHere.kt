package ic.parallel.funs


import ic.parallel.impl.parallelEngine


@Suppress("FunctionName")
@Deprecated("To remove")
fun CanBeStoppedHere() {
	parallelEngine.canBeStoppedHere()
}


val canBeStoppedHere : Unit get() {

	parallelEngine.canBeStoppedHere()

}
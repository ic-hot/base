package ic.parallel.funs


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.parallel.beam.Beam
import ic.parallel.thread.Thread
import ic.service.ext.startNonBlocking


inline fun doInParallelInBackground (

	actionsCount : Int64 = Int64.MAX_VALUE,

	maxThreadsCount : Int32 = Int32.MAX_VALUE,

	crossinline action : Thread.(index: Int64) -> Unit

) : Beam {

	return Beam(
		actionsCount  = actionsCount,
		maxThreadsCount = maxThreadsCount,
		toDoInParallel = action
	).apply {
		startNonBlocking()
	}

}
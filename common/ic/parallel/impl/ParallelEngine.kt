package ic.parallel.impl


import ic.base.annotations.ToOverride
import ic.base.primitives.int64.Int64
import ic.design.task.executor.ExecutorTask
import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.executor.BackgroundExecutor
import ic.ifaces.executor.Executor
import ic.parallel.mutex.Mutex
import ic.parallel.thread.Thread
import ic.parallel.thread.impl.ThreadImplementation


internal interface ParallelEngine {

	fun sleep (durationMs: Int64)

	fun createThreadImplementation (thread: Thread) : ThreadImplementation

	fun createMutex() : Mutex

	val defaultTaskExecutor : Executor get() = BackgroundExecutor

	val defaultCallbackExecutor : Executor

	fun doInUiThread (action: Action) : Cancelable

	@ToOverride
	fun schedule (action: Action, delayMs: Int64) : Cancelable? = ExecutorTask(
		run = { sleep(delayMs) },
		onSuccess = {
			action.run()
		}
	)

	fun canBeStoppedHere()

}
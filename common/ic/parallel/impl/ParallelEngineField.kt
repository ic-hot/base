package ic.parallel.impl


import ic.base.platform.basePlatform


internal inline val parallelEngine get() = basePlatform.parallelEngine
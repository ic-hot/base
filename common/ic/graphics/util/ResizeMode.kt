package ic.graphics.util


import ic.base.primitives.int64.Int64


sealed class ResizeMode {


	object LeaveAsIs : ResizeMode() {

		override fun toString() : String {
			return "ResizeMode.LeaveAsIs"
		}

	}


	data class MaxWidth (val maxWidth: Int64) : ResizeMode() {

		override fun toString() : String {
			return "ResizeMode.MaxWidth($maxWidth)"
		}

	}


	data class MaxHeight (val maxHeight: Int64) : ResizeMode() {

		override fun toString() : String {
			return "ResizeMode.MaxHeight($maxHeight)"
		}

	}


	data class MaxDimension (val maxDimension: Int64) : ResizeMode() {

		override fun toString() : String {
			return "ResizeMode.MaxDimension($maxDimension)"
		}

	}


}
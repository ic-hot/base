package ic.ifaces.sizeable


interface Sizeable<Size> {

	val size: Size

}

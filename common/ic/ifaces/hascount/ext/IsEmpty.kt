package ic.ifaces.hascount.ext


import ic.ifaces.hascount.HasCount


inline val HasCount.isEmpty : Boolean get() = count == 0L

inline val HasCount?.isNullOrEmpty : Boolean get() {
	if (this == null) return true
	return isEmpty
}

inline val HasCount?.isNotEmpty : Boolean get() = !isNullOrEmpty
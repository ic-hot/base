package ic.ifaces.hascount.ext


import ic.ifaces.hascount.HasCount


inline val HasCount.orNullIfEmpty get() = if (isEmpty) null else this
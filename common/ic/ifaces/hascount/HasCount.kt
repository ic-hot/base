package ic.ifaces.hascount


import ic.base.primitives.int64.Int64


interface HasCount {

	val count : Int64

}

package ic.ifaces.scrollable.phase


import ic.base.primitives.float32.Float32


interface PhaseScrollable {

	fun setScrollPhase (phase: Float32)

}
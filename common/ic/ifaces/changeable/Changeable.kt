package ic.ifaces.changeable


import ic.util.event.Event


interface Changeable {

	val onChangedEvent: Event

}

package ic.ifaces.id


abstract class Identifiable<Id> {

	abstract val id : Id

	override fun equals (other: Any?) : Boolean {
		if (other is Identifiable<*>) {
			return this.id == other.id
		} else {
			return false
		}
	}

	override fun hashCode() : Int {
		return id.hashCode()
	}

}
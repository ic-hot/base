package ic.ifaces.id


import ic.base.primitives.int64.Int64


interface HasInt64Id {

	val id : Int64

}
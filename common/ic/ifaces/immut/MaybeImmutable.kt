package ic.ifaces.immut


interface MaybeImmutable {

	val isImmutable : Boolean

}
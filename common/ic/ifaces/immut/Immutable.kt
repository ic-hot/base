package ic.ifaces.immut


interface Immutable : MaybeImmutable {

	override val isImmutable get() = true

}
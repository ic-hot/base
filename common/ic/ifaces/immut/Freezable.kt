package ic.ifaces.immut


interface Freezable {


	val isFrozen : Boolean


	fun freeze()


}
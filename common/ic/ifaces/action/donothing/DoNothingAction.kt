package ic.ifaces.action.donothing


import ic.base.annotations.DoesNothing
import ic.ifaces.action.Action


object DoNothingAction : Action {

	@DoesNothing
	override fun run() {}

}
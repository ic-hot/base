package ic.ifaces.action.action1


interface Action1<in Arg> {

	fun run (arg: Arg)

	companion object

}

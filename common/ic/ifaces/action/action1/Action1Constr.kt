@file:Suppress("FunctionName")


package ic.ifaces.action.action1


inline fun <Arg> Action1 (crossinline function: (Arg) -> Unit) : Action1<Arg> {

	return object : Action1<Arg> {

		override fun run (arg: Arg) = function(arg)

	}

}
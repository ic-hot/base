package ic.ifaces.action.action1.between


import ic.ifaces.action.action1.Action1
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


abstract class BetweenAction1<Arg> : Action1<Arg> {


	protected abstract fun implementRun (arg: Arg)

	protected abstract fun runInBetween()


	private val mutex = Mutex()

	private var toCallDoInBetween : Boolean = false


	override fun run (arg: Arg) = mutex.synchronized {

		if (toCallDoInBetween) {
			runInBetween()
		} else {
			toCallDoInBetween = true
		}

		implementRun(arg)

	}


}
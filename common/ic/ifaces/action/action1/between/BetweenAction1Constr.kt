@file:Suppress("FunctionName")


package ic.ifaces.action.action1.between


@Deprecated("Use .andDoInBetween")
inline fun <Arg> BetweenAction1 (

	crossinline runInBetween : () -> Unit,

	crossinline run : (Arg) -> Unit

) : BetweenAction1<Arg> {

	return object : BetweenAction1<Arg>() {

		override fun implementRun (arg: Arg) = run(arg)

		override fun runInBetween() = runInBetween()

	}

}
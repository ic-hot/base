package ic.ifaces.action.action1.ext


import ic.ifaces.action.action1.Action1
import ic.ifaces.action.action1.donothing.DoNothingAction1


inline val Action1.Companion.DoNothing get() = DoNothingAction1
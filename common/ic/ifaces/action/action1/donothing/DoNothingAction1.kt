package ic.ifaces.action.action1.donothing


import ic.base.annotations.DoesNothing
import ic.ifaces.action.action1.Action1


object DoNothingAction1 : Action1<Any?> {

	@DoesNothing
	override fun run (arg: Any?) {}

}
@file:Suppress("FunctionName")


package ic.ifaces.action


inline fun Action (crossinline action: () -> Unit) : Action {

	return object : Action {

		override fun run() = action()

	}

}
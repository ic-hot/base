package ic.ifaces.action.ext


import ic.ifaces.action.Action
import ic.ifaces.action.donothing.DoNothingAction


inline val Action.Companion.DoNothing get() = DoNothingAction
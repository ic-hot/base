@file:Suppress("FunctionName")


package ic.ifaces.action


inline fun <Arg1, Arg2> Action2 (

	crossinline function: (Arg1, Arg2) -> Unit

) : Action2<Arg1, Arg2> {

	return object : Action2<Arg1, Arg2> {

		override fun run (arg1: Arg1, arg2: Arg2) = function(arg1, arg2)

	}

}
package ic.ifaces.action.between


import ic.ifaces.action.Action2
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


abstract class BetweenAction2<Arg1, Arg2> : Action2<Arg1, Arg2> {


	protected abstract fun implementRun (arg1: Arg1, arg2: Arg2)

	protected abstract fun runInBetween()


	private val lock = Mutex()

	private var toCallDoInBetween : Boolean = false


	override fun run (arg1: Arg1, arg2: Arg2) = lock.synchronized {

		if (toCallDoInBetween) {
			runInBetween()
		} else {
			toCallDoInBetween = true
		}

		implementRun(arg1, arg2)

	}


}
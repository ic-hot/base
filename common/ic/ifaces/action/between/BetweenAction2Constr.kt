@file:Suppress("FunctionName")


package ic.ifaces.action.between


inline fun <Arg1, Arg2> BetweenAction2 (

	crossinline runInBetween : () -> Unit,

	crossinline run : (Arg1, Arg2) -> Unit

) : BetweenAction2<Arg1, Arg2> {

	return object : BetweenAction2<Arg1, Arg2>() {

		override fun implementRun (arg1: Arg1, arg2: Arg2) = run(arg1, arg2)

		override fun runInBetween() = runInBetween()

	}

}
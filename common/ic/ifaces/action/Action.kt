package ic.ifaces.action


interface Action {

	fun run()

	companion object

}

@file:Suppress("DEPRECATION")


package ic.ifaces.comparable


import ic.struct.order.OrderRelation


@Deprecated("Use normal Kotlin comparable")
interface Comparable<Other> {

	fun compareTo (other: Other) : OrderRelation

}
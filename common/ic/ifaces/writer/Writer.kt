package ic.ifaces.writer


interface Writer<Type> {

	fun write (value: Type)

}

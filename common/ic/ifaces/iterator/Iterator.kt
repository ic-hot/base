package ic.ifaces.iterator


import ic.base.loop.repeat
import ic.base.primitives.int32.Int32
import ic.base.throwables.End


interface Iterator<out Item> {


	@Throws(End::class)
	fun getNextOrThrowEnd() : Item


	@Throws(End::class)
	fun skip (amount: Int32) {
		repeat(amount) {
			getNextOrThrowEnd()
		}
	}


}

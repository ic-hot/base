package ic.ifaces.remover


import ic.base.throwables.NotExistsException


interface Remover {

	@Throws(NotExistsException::class)
	fun removeOrThrowNotExists()

	fun remove() {
		try {
			removeOrThrowNotExists()
		} catch (t: NotExistsException) {
			throw RuntimeException()
		}
	}

	fun removeIfExists() {
		try {
			removeOrThrowNotExists()
		} catch (notExists: NotExistsException) {}
	}

}

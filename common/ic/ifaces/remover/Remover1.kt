package ic.ifaces.remover


import ic.base.throwables.NotExistsException


interface Remover1<Arg> {


	@Throws(NotExistsException::class)
	fun removeOrThrowNotExists (arg: Arg)

	
	fun remove (item: Arg) {
		try {
			removeOrThrowNotExists(item)
		} catch (notExists: NotExistsException) {
			throw RuntimeException()
		}
	}

	
	fun removeIfExists (item: Arg) {
		try {
			removeOrThrowNotExists(item)
		} catch (_: NotExistsException) {}
	}


}

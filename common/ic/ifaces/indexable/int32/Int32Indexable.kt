package ic.ifaces.indexable.int32


import ic.base.primitives.int32.Int32


interface Int32Indexable {

	val index : Int32

}
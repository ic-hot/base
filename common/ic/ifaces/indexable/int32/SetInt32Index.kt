package ic.ifaces.indexable.int32


import ic.base.primitives.int32.Int32


interface SetInt32Index : Int32Indexable {

	override var index: Int32

}
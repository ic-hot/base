package ic.ifaces.mutable.funs


import ic.ifaces.mutable.Mutable
import ic.ifaces.mutable.ext.synchronizedIfMutable
import ic.parallel.mutex.synchronized


inline fun <Result> synchronized (a: Mutable, b: Mutable, block : () -> Result) : Result {
	return synchronized(a.mutex, b.mutex, block)
}


inline fun <Result> synchronizedIfMutable (a: Any, block : () -> Result) : Result {
	return a.synchronizedIfMutable(block)
}


inline fun <Result> synchronizedIfMutable (a: Any, b: Any, block : () -> Result) : Result {
	return if (a is Mutable) {
		if (b is Mutable) {
			synchronized(a.mutex, b.mutex, block)
		} else {
			synchronized(a.mutex, block)
		}
	} else {
		if (b is Mutable) {
			synchronized(b.mutex, block)
		} else {
			block()
		}
	}

}
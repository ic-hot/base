package ic.ifaces.mutable


import ic.parallel.mutex.Mutex


interface Mutable {

	val mutex : Mutex

}
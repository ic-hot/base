package ic.ifaces.mutable.ext


import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.synchronized


inline fun <Result> Mutable.synchronized (block : () -> Result) : Result {
	return mutex.synchronized(block)
}

inline fun <Result> Any?.synchronizedIfMutable (block : () -> Result) : Result {
	if (this is Mutable) {
		return synchronized(block)
	} else {
		return block()
	}
}
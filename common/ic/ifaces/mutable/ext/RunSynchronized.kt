package ic.ifaces.mutable.ext


import ic.ifaces.mutable.Mutable


inline fun

	<Receiver: Mutable, Result>

	Receiver.runSynchronized (action: Receiver.() -> Result)

	: Result

{

	synchronized {

		return run(action)

	}

}
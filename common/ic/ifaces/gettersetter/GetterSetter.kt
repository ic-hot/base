package ic.ifaces.gettersetter


import ic.ifaces.getter.Getter
import ic.ifaces.setter.Setter


interface GetterSetter<Value> : Getter<Value>, Setter<Value> {



}
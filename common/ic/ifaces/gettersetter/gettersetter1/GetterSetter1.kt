package ic.ifaces.gettersetter.gettersetter1


import ic.ifaces.getter.getter1.Getter1
import ic.ifaces.setter.Setter1


interface GetterSetter1<Value, Arg> : Getter1<Value, Arg>, Setter1<Value, Arg> {



}
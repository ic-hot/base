package ic.ifaces.gettersetter.gettersetter1.ext.untyped


import ic.ifaces.gettersetter.gettersetter1.GetterSetter1
import ic.ifaces.mutable.ext.synchronizedIfMutable


@Suppress("UNCHECKED_CAST")
inline fun

	<Bound, Result: Bound, Arg>

	GetterSetter1<Bound, Arg>.getOrCreateIfNull (

		key : Arg,

		ifNotExists : () -> Result

	)

	: Result

{

	synchronizedIfMutable {

		val existingValue = get(key)

		if (existingValue != null) return existingValue as Result

		val newValue = ifNotExists()
		set(key, newValue)
		return newValue

	}

}
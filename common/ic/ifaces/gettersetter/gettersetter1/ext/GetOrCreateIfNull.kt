package ic.ifaces.gettersetter.gettersetter1.ext


import ic.ifaces.gettersetter.gettersetter1.GetterSetter1
import ic.ifaces.mutable.ext.synchronizedIfMutable


inline fun

	<Value: Any, Arg>

	GetterSetter1<Value?, Arg>.getOrCreateIfNull (

		key : Arg,

		ifNotExists : () -> Value

	)

	: Value

{

	synchronizedIfMutable {

		val existingValue = get(key)

		if (existingValue != null) return existingValue

		val newValue = ifNotExists()
		set(key, newValue)
		return newValue

	}

}
package ic.ifaces.condition


inline fun Condition (crossinline function : () -> Boolean) : Condition {
	return object : Condition {
		override val booleanValue = function()
	}
}
@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.condition.not


import ic.ifaces.condition.Condition


inline fun NotCondition (

	sourceCondition : Condition

) : NotCondition {

	return object : NotCondition() {

		override val sourceCondition get() = sourceCondition

	}

}
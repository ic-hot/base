package ic.ifaces.condition.not


import ic.ifaces.condition.Condition


abstract class NotCondition : Condition {

	protected abstract val sourceCondition : Condition

	override val booleanValue get() = !sourceCondition.booleanValue

}
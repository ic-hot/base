@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.condition.ext

import ic.ifaces.condition.Condition
import ic.ifaces.condition.not.NotCondition


inline operator fun Condition.not() : Condition = NotCondition(this)
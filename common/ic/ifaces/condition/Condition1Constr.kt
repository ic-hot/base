@file:Suppress("FunctionName")


package ic.ifaces.condition


inline fun <Arg> Condition1 (crossinline lambda: (Arg) -> Boolean) : Condition1<Arg> {
	return object : Condition1<Arg> {
		override fun getBooleanValue (arg: Arg) = lambda(arg)
	}
}
package ic.ifaces.condition



interface Condition1<Arg> {

	fun getBooleanValue (arg: Arg) : Boolean

	class False<Arg> : Condition1<Arg> {
		override fun getBooleanValue (arg: Arg) = false
	}

	class True<Arg> : Condition1<Arg> {
		override fun getBooleanValue (arg: Arg) = true
	}

}
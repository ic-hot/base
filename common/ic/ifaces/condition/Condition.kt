package ic.ifaces.condition


interface Condition {

	val booleanValue : Boolean

	class False : Condition { override val booleanValue get() = false }

	class True : Condition { override val booleanValue get() = true }

}

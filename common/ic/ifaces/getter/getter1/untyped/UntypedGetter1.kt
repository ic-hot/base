package ic.ifaces.getter.getter1.untyped


interface UntypedGetter1<Arg> {

	fun <Value> get (arg: Arg) : Value

}
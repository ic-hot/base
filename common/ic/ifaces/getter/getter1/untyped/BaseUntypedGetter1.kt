package ic.ifaces.getter.getter1.untyped


abstract class BaseUntypedGetter1<Arg> : UntypedGetter1<Arg> {


	protected abstract fun implementGet (arg: Arg) : Any?


	@Suppress("UNCHECKED_CAST")
	override fun <Value> get (arg: Arg) : Value {
		return implementGet(arg) as Value
	}
	

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.getter.getter1.untyped.ext


import ic.base.throwables.NotExistsException
import ic.ifaces.getter.getter1.untyped.UntypedGetter1


@Throws(NotExistsException::class)
inline fun <Value: Any, Arg> UntypedGetter1<Arg>.getOrThrowNotExists (arg: Arg) : Value {
	return get(arg) ?: throw NotExistsException
}
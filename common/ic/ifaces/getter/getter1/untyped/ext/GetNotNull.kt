@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.getter.getter1.untyped.ext


import ic.base.arrays.ext.forEach
import ic.base.throwables.NotExistsException
import ic.ifaces.getter.getter1.untyped.UntypedGetter1


inline fun <Value: Any, Arg> UntypedGetter1<Arg>.getNotNull (arg: Arg) : Value {
	return get(arg)!!
}

inline fun <Value: Any, Arg> UntypedGetter1<Arg>.getNotNull (vararg args: Arg) : Value {
	args.forEach { arg ->
		try {
			return getOrThrowNotExists(arg)
		} catch (t: NotExistsException) {}
	}
	throw RuntimeException()
}
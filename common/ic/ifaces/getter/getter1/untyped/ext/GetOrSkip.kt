@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.getter.getter1.untyped.ext


import ic.base.throwables.Skip
import ic.ifaces.getter.getter1.untyped.UntypedGetter1


@Throws(Skip::class)
inline fun <Value: Any, Arg> UntypedGetter1<Arg>.getOrSkip (arg: Arg) : Value {
	return get(arg) ?: throw Skip
}
@file:Suppress("FunctionName")


package ic.ifaces.getter.getter1


inline fun <Value, Arg> Getter1 (

	crossinline lambda: (Arg) -> Value

) : Getter1<Value, Arg> {

	return object : Getter1<Value, Arg> {

		override fun get (arg: Arg) = lambda(arg)

	}

}
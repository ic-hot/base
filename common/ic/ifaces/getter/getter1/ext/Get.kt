@file:Suppress("NOTHING_TO_INLINE")


package ic.ifaces.getter.getter1.ext


import ic.base.objects.ext.orSkip
import ic.base.throwables.NotExistsException
import ic.ifaces.getter.getter1.Getter1


@Deprecated("Use get(arg).orThrowNotExists")
@Throws(NotExistsException::class)
inline fun <Value: Any, Arg> Getter1<Value?, Arg>.getOrThrowNotExists (arg: Arg) : Value {
	return get(arg) ?: throw NotExistsException
}


@Deprecated("Use get(arg).orSkip")
inline fun <Value: Any, Arg> Getter1<Value?, Arg>.getOrSkip (arg: Arg) = get(arg).orSkip


@Deprecated("Use get(arg) ?: ifNull()")
inline fun <Value: Any, Arg> Getter1<Value?, Arg>.getOr (arg: Arg, ifNull : () -> Value) = get(arg) ?: ifNull()
package ic.ifaces.getter.getter1.selector


import ic.base.throwables.NotSupportedException
import ic.struct.list.List
import ic.struct.list.ext.get


abstract class Selector1<Value, Arg> : SelectGetter1<Value, Arg> {


	protected abstract val selectGetters : List<(Arg) -> Value>


	@Throws(NotSupportedException::class)
	override fun getOrThrowNotSupported (arg: Arg) : Value {

		val selectGetters = selectGetters

		val selectGettersCount = selectGetters.length

		var selectGetterIndex = 0

		while (selectGetterIndex < selectGettersCount) {

			try {
				return selectGetters[selectGetterIndex](arg)
			} catch (t: NotSupportedException) {}

			selectGetterIndex++

		}; throw NotSupportedException

	}


}
package ic.ifaces.getter.getter1.selector


import ic.base.throwables.NotSupportedException
import ic.ifaces.getter.getter1.Getter1


interface SelectGetter1<Value, Arg> : Getter1<Value, Arg> {


	@Throws(NotSupportedException::class)
	fun getOrThrowNotSupported (arg: Arg) : Value


	override fun get(arg: Arg) : Value {
		try {
			return getOrThrowNotSupported (arg)
		} catch (t: NotSupportedException) {
			throw NotSupportedException.Runtime(t)
		}
	}


}
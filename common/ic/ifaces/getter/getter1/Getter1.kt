package ic.ifaces.getter.getter1


interface Getter1<Value, Arg> {

	fun get (arg: Arg) : Value

}
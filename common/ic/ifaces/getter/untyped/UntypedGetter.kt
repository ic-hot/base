package ic.ifaces.getter.untyped


interface UntypedGetter {

	fun <Value> get() : Value

}
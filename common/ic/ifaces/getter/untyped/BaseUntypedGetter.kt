package ic.ifaces.getter.untyped


abstract class BaseUntypedGetter : UntypedGetter {


	protected abstract fun implementGet() : Any?


	@Suppress("UNCHECKED_CAST")
	override fun <Value> get() : Value {
		return implementGet() as Value
	}


}
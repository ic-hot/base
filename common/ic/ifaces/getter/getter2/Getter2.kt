package ic.ifaces.getter.getter2


interface Getter2<Value, Arg1, Arg2> {

	fun get (arg1: Arg1, arg2: Arg2) : Value

}
package ic.ifaces.getter.abstr


abstract class BaseAbstractGetter {

	protected abstract fun implementGet() : Any?

}
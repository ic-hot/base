package ic.ifaces.getter


interface Getter<Value> {

	fun get() : Value

}
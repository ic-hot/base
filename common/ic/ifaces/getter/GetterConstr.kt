@file:Suppress("FunctionName")


package ic.ifaces.getter


inline fun <Value> Getter (

	crossinline lambda: () -> Value

) : Getter<Value> {

	return object : Getter<Value> {

		override fun get() = lambda()

	}

}
@file:Suppress("FunctionName")


package ic.ifaces.cancelable


inline fun Cancelable (crossinline cancel : () -> Unit) : Cancelable = object : Cancelable {

	override fun cancel() = cancel()

}
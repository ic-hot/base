package ic.ifaces.cancelable


interface Cancelable {

	fun cancel()

}
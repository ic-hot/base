package ic.ifaces.executor


import ic.ifaces.action.Action
import ic.ifaces.stoppable.Stoppable


interface Executor {


	fun execute (action: Action) : Stoppable?


}
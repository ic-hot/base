package ic.ifaces.executor


val defaultTaskExecutor 	get() = ic.base.platform.basePlatform.parallelEngine.defaultTaskExecutor
val defaultCallbackExecutor get() = ic.base.platform.basePlatform.parallelEngine.defaultCallbackExecutor
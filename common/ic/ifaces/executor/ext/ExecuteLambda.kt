package ic.ifaces.executor.ext


import ic.ifaces.action.Action
import ic.ifaces.executor.Executor
import ic.ifaces.stoppable.Stoppable


inline fun Executor.execute (crossinline action: () -> Unit) : Stoppable? {

	return execute(

		Action(action)

	)

}
package ic.ifaces.executor


import ic.ifaces.action.Action
import ic.ifaces.stoppable.Stoppable
import ic.parallel.funs.doInBackground


object BackgroundExecutor : Executor {

	override fun execute (action: Action) : Stoppable {

		return doInBackground {
			action.run()
		}

	}

}
package ic.ifaces.executor


import ic.ifaces.action.Action
import ic.ifaces.stoppable.Stoppable


object DirectExecutor : Executor {

	override fun execute (action: Action) : Stoppable? {
		action.run()
		return null
	}

}
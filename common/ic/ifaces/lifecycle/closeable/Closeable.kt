package ic.ifaces.lifecycle.closeable


interface Closeable {

	fun close()

}
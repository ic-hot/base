package ic.ifaces.lifecycle.closeable


inline fun Closeable (

	crossinline close : () -> Unit

) : Closeable {

	return object : Closeable {

		override fun close() = close()

	}

}
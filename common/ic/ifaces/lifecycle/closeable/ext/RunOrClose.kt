package ic.ifaces.lifecycle.closeable.ext


import ic.ifaces.lifecycle.closeable.Closeable


inline fun

	<Receiver: Closeable?, Result>

	Receiver.runOrClose (action: Receiver.() -> Result) : Result

{

	try {

		return action()

	} catch (t: Throwable) {

		this?.close()

		throw t

	}

}
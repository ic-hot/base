package ic.ifaces.lifecycle.closeable.ext


import ic.ifaces.cancelable.Cancelable
import ic.ifaces.lifecycle.closeable.Closeable


inline fun

	<Receiver: Closeable?, Result>

	Receiver.runAndCloseOrCancel (action: Receiver.() -> Result) : Result

{

	val result = try {

		action()

	} catch (t: Throwable) {

		if (this is Cancelable) {
			cancel()
		} else {
			this?.close()
		}

		throw t

	}

	this?.close()

	return result

}
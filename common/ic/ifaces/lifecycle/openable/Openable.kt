package ic.ifaces.lifecycle.openable


interface Openable {


	fun open()


}
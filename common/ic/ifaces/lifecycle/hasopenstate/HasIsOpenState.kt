package ic.ifaces.lifecycle.hasopenstate


interface HasIsOpenState {

	val isOpen : Boolean

}
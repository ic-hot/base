package ic.ifaces.pausable


interface Pausable {

	fun resume()

	fun pause()

}
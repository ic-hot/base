package ic.ifaces.adder


import ic.base.throwables.AlreadyExistsException


interface SetAdder<Item> : Adder<Item> {

	@Throws(AlreadyExistsException::class)
	fun addOrThrowAlreadyExists(item: Item)

	override fun add (item: Item) {
		try {
			addOrThrowAlreadyExists(item)
		} catch (_: AlreadyExistsException) {
			throw RuntimeException("item: $item")
		}
	}

	fun addIfNotExists (item: Item) {
		try {
			addOrThrowAlreadyExists(item)
		} catch (_: AlreadyExistsException) {}
	}

}
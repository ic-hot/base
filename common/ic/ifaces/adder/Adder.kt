package ic.ifaces.adder


interface Adder<Item> {


	infix fun add (item: Item)


}

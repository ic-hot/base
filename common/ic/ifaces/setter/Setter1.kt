package ic.ifaces.setter


interface Setter1<Type, Arg> {

	fun set (arg: Arg, value: Type)

}

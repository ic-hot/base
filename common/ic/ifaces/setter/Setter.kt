package ic.ifaces.setter


interface Setter<Value> {

	fun set (value: Value)

}
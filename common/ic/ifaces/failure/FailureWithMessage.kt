package ic.ifaces.failure


interface FailureWithMessage : Failure {

	val message : String

}
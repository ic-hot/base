package ic.ifaces.failure


interface IoFailure : Failure {

	object IoFailure : ic.ifaces.failure.IoFailure

	companion object {

		val IO_FAILURE = IoFailure

	}

}
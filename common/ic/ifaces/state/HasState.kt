package ic.ifaces.state


interface HasState<State> {

	val state : State

}
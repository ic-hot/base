package ic.ifaces.state


interface HasSettableState <State> : HasState<State> {

	override var state: State

}
package ic.ifaces.removable


import ic.base.throwables.NotExistsException


interface Removable {

	@Throws(NotExistsException::class)
	fun removeOrThrowNotExists()

	fun remove() {
		try {
			removeOrThrowNotExists()
		} catch (t: NotExistsException) {
			throw RuntimeException()
		}
	}

	fun removeIfExists() {
		try {
			removeOrThrowNotExists()
		} catch (notExists: NotExistsException) {}
	}

}

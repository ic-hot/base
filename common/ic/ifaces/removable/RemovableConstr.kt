package ic.ifaces.removable


import ic.base.throwables.NotExistsException


inline fun Removable (

	crossinline removeOrThrowNotExists : () -> Unit

) : Removable {

	return object : Removable {

		@Throws(NotExistsException::class)
		override fun removeOrThrowNotExists() = removeOrThrowNotExists()

	}

}
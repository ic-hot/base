package ic.ifaces.stoppable


import ic.base.throwables.NotNeededException


interface Stoppable {

	@Throws(NotNeededException::class)
	fun stopNonBlockingOrThrowNotNeeded()

	fun waitFor()

}
package ic.ifaces.stoppable


inline fun Stoppable (

	crossinline stopNonBlockingOrThrowNotNeeded : () -> Unit,

	crossinline waitFor : () -> Unit

) : Stoppable {

	return object : Stoppable {

		override fun stopNonBlockingOrThrowNotNeeded() = stopNonBlockingOrThrowNotNeeded()

		override fun waitFor() = waitFor()

	}

}
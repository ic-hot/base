package ic.ifaces.stoppable.ext


import ic.base.throwables.NotNeededException
import ic.ifaces.stoppable.Stoppable


@Suppress("NOTHING_TO_INLINE")
inline fun Stoppable.stopNonBlocking() {
	try {
		stopNonBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {
		throw RuntimeException()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Stoppable.stopNonBlockingIfNeeded() {
	try {
		stopNonBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {}
}
package ic.ifaces.stoppable.ext


import ic.base.throwables.NotNeededException
import ic.ifaces.stoppable.Stoppable


@Throws(NotNeededException::class)
fun Stoppable.stopBlockingOrThrowNotNeeded() {
	stopNonBlockingOrThrowNotNeeded()
	waitFor()
}


@Suppress("NOTHING_TO_INLINE")
inline fun Stoppable.stopBlocking() {
	try {
		stopBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {
		throw RuntimeException()
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Stoppable.stopBlockingIfNeeded() {
	try {
		stopBlockingOrThrowNotNeeded()
	} catch (_: NotNeededException) {}
}
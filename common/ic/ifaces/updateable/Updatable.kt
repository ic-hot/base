package ic.ifaces.updateable


interface Updatable {

	fun update()

}
package ic.ifaces.withargs


interface WithArgs<Args> {

	val args : Args

}
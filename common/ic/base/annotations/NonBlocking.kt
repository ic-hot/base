package ic.base.annotations


import kotlin.annotation.AnnotationTarget.*


@MustBeDocumented
@Target(
	FUNCTION,
	PROPERTY_GETTER,
	PROPERTY_SETTER,
	CONSTRUCTOR,
	VALUE_PARAMETER
)
annotation class NonBlocking
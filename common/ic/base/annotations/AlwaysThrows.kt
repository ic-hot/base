package ic.base.annotations


import kotlin.reflect.KClass


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.CONSTRUCTOR
)
annotation class AlwaysThrows (val value: KClass<out Throwable> = RuntimeException::class)
package ic.base.annotations


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.CONSTRUCTOR,
	AnnotationTarget.FIELD,
	AnnotationTarget.ANNOTATION_CLASS,
	AnnotationTarget.CLASS
)
annotation class UsedByReflect
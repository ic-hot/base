package ic.base.annotations


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.CONSTRUCTOR,
	AnnotationTarget.CLASS
)
annotation class RootOnly
package ic.base.annotations


import kotlin.reflect.KClass


@MustBeDocumented
@Target(
	AnnotationTarget.CONSTRUCTOR,
	AnnotationTarget.FUNCTION,
	AnnotationTarget.PROPERTY_GETTER,
	AnnotationTarget.PROPERTY_SETTER,
	AnnotationTarget.PROPERTY,
	AnnotationTarget.VALUE_PARAMETER
)
annotation class Throws (vararg val throwablesClasses: KClass<out Throwable>)
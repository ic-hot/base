package ic.base.test


import ic.base.primitives.tests.PrimitivesTests
import ic.struct.list.List
import ic.test.TestSuite


class BaseTests : TestSuite() {

	override fun initTests() = List(

		PrimitivesTests()

	)

}
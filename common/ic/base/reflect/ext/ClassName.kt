package ic.base.reflect.ext


inline val Any?.className : String get() {

	if (this == null) return "null"

	return this::class.qualifiedName ?: "Unknown"

}



inline val Any?.simpleClassName : String get() {

	if (this == null) return "null"

	return this::class.simpleName ?: "Unknown"

}
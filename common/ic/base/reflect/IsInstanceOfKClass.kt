package ic.base.reflect


import kotlin.reflect.KClass


@Suppress("NOTHING_TO_INLINE")
inline infix fun Any?.isInstanceOf (type: KClass<*>) : Boolean {

	if (this == null) return false

	return type.isInstance(this)

}
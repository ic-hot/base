package ic.base.throwables


class FailureException (val failure : Any?) : Exception() {

	class Runtime (val failure: Any?) : RuntimeException()

}
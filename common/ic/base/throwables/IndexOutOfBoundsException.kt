package ic.base.throwables


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


object IndexOutOfBoundsException : Exception() {


	class Runtime (

		val length : Int64,
		val index  : Int64,

		message : String = "Can't access item $index of List that has length $length"

	) : RuntimeException(message) {

		constructor(
			length : Int32,
			index  : Int32,
			message : String = "Can't access item $index of List that has length $length"
		) : this(
			length = length.asInt64,
			index  = index.asInt64,
			message = message
		)

		constructor(
			length : Int32,
			index  : Int64,
			message : String = "Can't access item $index of List that has length $length"
		) : this(
			length = length.asInt64,
			index  = index,
			message = message
		)

	}


}
package ic.base.throwables


object End : Throwable() {


	@Suppress("FunctionName", "NOTHING_TO_INLINE")
	inline fun End() : Nothing = throw End


	class Runtime : RuntimeException {

		constructor () 					: super ()
		constructor (cause: Throwable) 	: super (cause)

	}


}
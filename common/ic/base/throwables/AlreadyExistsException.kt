package ic.base.throwables


object AlreadyExistsException : Exception() {

	class Runtime : RuntimeException {
		constructor() : super()
		constructor(message: String?) : super(message)
		constructor(cause: Throwable?) : super(cause)
	}

}
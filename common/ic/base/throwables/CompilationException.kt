package ic.base.throwables


class CompilationException (

	message : String

) : Exception (message) {

	class Runtime : RuntimeException {

		constructor () 									: super ()
		constructor (message: String) 					: super (message)
		constructor (message: String, cause: Throwable) : super (message, cause)

	}

}
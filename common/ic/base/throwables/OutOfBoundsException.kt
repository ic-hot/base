package ic.base.throwables


object OutOfBoundsException : Exception() {


	class Runtime : RuntimeException {

		constructor (message: String) 					: super(message)
		constructor (cause: Throwable) 					: super(cause)
		constructor (message: String, cause: Throwable) : super(message, cause)

	}


}
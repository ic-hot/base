package ic.base.throwables


object OutOfLimitException : Exception() {


	class Runtime : RuntimeException {

		constructor() 					: super()
		constructor(message: String) 	: super(message)

	}


}
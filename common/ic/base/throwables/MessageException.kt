package ic.base.throwables


open class MessageException (message: String) : Exception(message) {

	override val message get() = super.message!!

}
package ic.base.throwables



object EmptyException : Exception() {

	class Runtime : RuntimeException {
		constructor() : super()
		constructor(message: String?) : super(message)
		constructor(cause: Throwable?) : super(cause)
	}

}
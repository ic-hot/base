package ic.base.throwables



object NotSupportedException : Exception() {

	class Runtime : RuntimeException {
		constructor() : super()
		constructor(message: String?) : super(message)
		constructor(cause: Throwable?) : super(cause)
	}

	class Error : kotlin.Error {
		constructor() : super()
		constructor(message: String?) : super(message)
		constructor(cause: Throwable?) : super(cause)
	}

}
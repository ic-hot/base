package ic.base.throwables


object WrongCallException : Exception() {

	class Runtime : RuntimeException {
		constructor() : super()
		constructor(message: String?) : super(message)
	}

	class Error : kotlin.Error {
		constructor() : super()
		constructor(message: String?) : super(message)
		constructor(cause: Throwable?) : super(cause)
	}

}
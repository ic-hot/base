package ic.base.throwables


class UnableToParseException : Exception {


	constructor () 									: super()
	constructor (message: String?) 					: super(message)
	constructor (cause: Throwable) 					: super(cause)
	constructor (message: String, cause: Throwable) : super(message, cause)


	class Runtime : RuntimeException {

		constructor () 									: super()
		constructor (message: String?) 					: super(message)
		constructor (cause: Throwable) 					: super(cause)
		constructor (message: String, cause: Throwable) : super(message, cause)

	}


}
package ic.base.throwables


object OverflowException : Exception() {


	class Runtime : RuntimeException {

		constructor() : super()

		constructor(message: String) : super(message)

	}


}
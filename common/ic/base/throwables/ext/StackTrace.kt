package ic.base.throwables.ext


import ic.base.strings.ext.asText


inline val Throwable.stackTraceAsString : String get() = (

	"\n" +

	message.let {
		if (it == null) {
			""
		} else {
			it + "\n"
		}
	} +

	stackTraceToString()

)


inline val Throwable.stackTraceAsText get() = stackTraceAsString.asText
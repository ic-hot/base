@file:Suppress("NOTHING_TO_INLINE")


package ic.base.throwables.ext


import ic.base.throwables.FailureException


inline fun <Failure> Throwable.asFailure() : Failure {
	if (this is FailureException) {
		@Suppress("UNCHECKED_CAST")
		return failure as Failure
	} else {
		throw this
	}
}
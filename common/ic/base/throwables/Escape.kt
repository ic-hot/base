@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.throwables


object Escape : RuntimeException() {

	inline fun Escape() { throw Escape }

}

package ic.base.kcollections

import ic.base.kcollections.ext.isEmpty


inline val <Item> kotlin.collections.Collection<Item>.isNotEmpty : Boolean get() = !isEmpty
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections.ext


inline fun <Item> Iterable<Item>.toKotlinList() : MutableList<Item> {

	return toMutableList()

}
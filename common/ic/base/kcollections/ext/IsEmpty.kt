package ic.base.kcollections.ext


inline val <Item> kotlin.collections.Collection<Item>.isEmpty : Boolean get() = isEmpty()
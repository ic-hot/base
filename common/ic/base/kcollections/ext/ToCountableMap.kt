package ic.base.kcollections.ext


import ic.struct.map.finite.FiniteMap


fun
	<Key, Value: Any>
	kotlin.collections.Map<Key, Value>.toFiniteMap()
	: FiniteMap<Key, Value>
	= toEditableMap()
;
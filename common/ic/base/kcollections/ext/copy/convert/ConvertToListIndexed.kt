package ic.base.kcollections.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item, NewItem> Iterable<Item>.copyConvertToListIndexed (

	@Skippable convertItemOrSkip : (index: Int32, Item) -> NewItem

) : List<NewItem> {
	val list = EditableList<NewItem>()
	var index : Int32 = 0
	forEach {
		skippable {
			list.add(
				convertItemOrSkip(index++, it)
			)
		}
	}
	return list
}
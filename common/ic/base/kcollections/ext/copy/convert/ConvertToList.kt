package ic.base.kcollections.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <Item, NewItem> Iterable<Item>.copyConvertToList (

	@Skippable convertItemOrSkip : (Item) -> NewItem

) : List<NewItem> {
	val list = EditableList<NewItem>()
	forEach {
		skippable {
			list.add(convertItemOrSkip(it))
		}
	}
	return list
}
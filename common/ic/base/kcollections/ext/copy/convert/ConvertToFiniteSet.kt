package ic.base.kcollections.ext.copy.convert


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


inline fun <Item, NewItem> Iterable<Item>.copyConvertToFiniteSet (

	@Skippable convertItemOrSkip : (Item) -> NewItem

) : FiniteSet<NewItem> {

	val set = EditableSet<NewItem>()

	forEach {
		skippable {
			set.addIfNotExists(
				convertItemOrSkip(it)
			)
		}
	}

	return set

}
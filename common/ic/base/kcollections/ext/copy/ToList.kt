package ic.base.kcollections.ext.copy


import ic.struct.list.List
import ic.struct.list.editable.EditableList


fun <Item> Iterable<Item>.copyToList() : List<Item> {
	val list = EditableList<Item>()
	forEach { list.add(it) }
	return list
}
package ic.base.kcollections.ext.copy


import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


fun <Item> Iterable<Item>.copyToFiniteSet() : FiniteSet<Item> {
	val result = EditableSet<Item>()
	forEach { item ->
		result.addIfNotExists(item)
	}
	return result
}
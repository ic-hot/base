package ic.base.kcollections.ext


inline val
	<Item>
	kotlin.collections.Collection<Item>?.isNullOrEmpty : Boolean

	get() = this == null || isEmpty

;
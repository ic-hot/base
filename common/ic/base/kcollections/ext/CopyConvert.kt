package ic.base.kcollections.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable


inline fun
	<OldItem, NewItem>
	Iterable<OldItem>.copyConvert (
		@Skippable convertItemOrSkip : (OldItem) -> NewItem
	)
	: List<NewItem>
{
	val newItems = mutableListOf<NewItem>()
	forEach { oldItem ->
		skippable {
			newItems.add(
				convertItemOrSkip(oldItem)
			)
		}
	}
	return newItems
}
package ic.base.kcollections.ext


inline val
	<Item>
	kotlin.collections.Collection<Item>.orNullIfEmpty : kotlin.collections.Collection<Item>?
	get() {
		if (isEmpty) {
			return null
		} else {
			return this
		}
	}
;
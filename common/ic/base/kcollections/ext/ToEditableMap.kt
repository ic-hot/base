package ic.base.kcollections.ext


import ic.struct.map.editable.EditableMap


fun
	<Key, Value: Any>
	kotlin.collections.Map<Key, Value>.toEditableMap()
	: EditableMap<Key, Value>
{

	val editableMap = EditableMap<Key, Value>()

	forEach { entry ->
		editableMap[entry.key] = entry.value
	}

	return editableMap

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections


inline fun <reified Item> kotlin.collections.Collection<Item>.toArray() : Array<Item> = toTypedArray()

inline fun <reified Item> kotlin.collections.Iterable<Item>.toArray() : Array<Item> = toList().toArray()
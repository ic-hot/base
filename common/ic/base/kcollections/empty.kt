@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections


inline fun <Item> kotlin.collections.MutableCollection<Item>.empty() = clear()
package ic.base.kcollections


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asInt32
import ic.struct.list.List
import ic.struct.list.SubList
import ic.struct.list.ext.reduce.find.atLeastOne
import ic.struct.list.ext.length.isEmpty


class KotlinListFromList<Item> (

	private val list : List<Item>

) : kotlin.collections.List<Item> {

	override fun get (index: Int) = list.get(index)

	override val size get() = list.length.asInt32

	override fun isEmpty() = list.isEmpty

	override fun iterator() = listIterator()

	override fun contains (element: Item) = list.atLeastOne { it == element }

	override fun containsAll (elements: kotlin.collections.Collection<Item>) : Boolean {
		for (element in elements) {
			if (!contains(element)) return false
		}; return true
	}

	override fun indexOf (element: Item) : Int {
		val length = length
		for (index in 0 until length) {
			if (get(index) == element) return index
		}; return -1
	}

	override fun lastIndexOf (element: Item) : Int {
		for (index in length - 1 downTo 0) {
			if (get(index) == element) return index
		}; return -1
	}

	override fun listIterator (
		@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE") startingFromIndex : Int32
	) : ListIterator<Item> {
		return object : ListIterator<Item> {
			val length = list.length
			var index = 0
			override fun hasNext() = index < length
			override fun hasPrevious() = index > 0
			override fun next() = get(index++)
			override fun previous() = get(index--)
			override fun nextIndex() = index + 1
			override fun previousIndex() = index - 1
		}
	}

	override fun listIterator() = listIterator(0)

	override fun subList (fromIndex: Int, toIndex: Int) : kotlin.collections.List<Item> {
		return KotlinListFromList(
			SubList(list, fromIndex, toIndex)
		)
	}

}
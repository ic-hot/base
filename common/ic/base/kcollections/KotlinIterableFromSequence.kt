package ic.base.kcollections


import ic.struct.sequence.Sequence
import ic.base.throwables.End


class KotlinIterableFromSequence<Item> (

	private val sequence : Sequence<Item>

) : kotlin.collections.Iterable<Item> {

	override fun iterator() = object : kotlin.collections.Iterator<Item> {

		val iterator = sequence.newIterator()

		var hasNext : Boolean = false
		var next : Item? = null

		fun updateValues() = try {
			next = iterator.getNextOrThrowEnd()
			hasNext = true
		} catch (end: End) {
			next = null
			hasNext = false
		}

		init { updateValues() }

		override fun hasNext() = hasNext

		override fun next() : Item {
			val next = this.next
			updateValues()
			@Suppress("UNCHECKED_CAST")
			return next as Item
		}

	}

}
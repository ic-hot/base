package ic.base.kcollections


import ic.base.primitives.float64.Float64


inline fun <Item> Iterable<Item>.sumByFloat64 (itemToFloat64 : (Item) -> Float64) : Float64 = this.sumOf(itemToFloat64)
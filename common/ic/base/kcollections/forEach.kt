@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections


import ic.ifaces.action.action1.Action1


inline fun <Item> Iterable<Item>.forEach (action: Action1<Item>) {

	forEach { action.run(it) }

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


inline fun <Item, NewItem> kotlin.collections.Iterable<Item>.toFiniteSet (
	@Skippable convertItem : (Item) -> NewItem
) : FiniteSet<NewItem> {
	val result = EditableSet<NewItem>()
	forEach { item ->
		skippable {
			result.add(
				convertItem(item)
			)
		}
	}
	return result
}


inline fun <Item> kotlin.collections.Iterable<Item>.toFiniteSet() : FiniteSet<Item> {
	return toFiniteSet { it }
}
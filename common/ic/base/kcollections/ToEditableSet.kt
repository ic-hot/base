@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections


import ic.struct.set.editable.EditableSet


inline fun <Item> kotlin.collections.Iterable<Item>.toEditableSet() : EditableSet<Item> {

	val editableSet = EditableSet<Item>()

	forEach { editableSet.add(it) }

	return editableSet

}
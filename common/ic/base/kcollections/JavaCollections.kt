@file:Suppress("NOTHING_TO_INLINE")


package ic.base.kcollections

import ic.struct.set.finite.FiniteSet
import ic.base.throwables.NotExistsException
import ic.struct.set.editable.EditableSet


inline fun <Item> Iterable<Item>.forkAdd (item: Item) : kotlin.collections.List<Item> {
	val mutableList = toMutableList()
	mutableList.add(item)
	return mutableList
}


inline fun <Item> Iterable<Item>.forkRemove (noinline predicate: (Item) -> Boolean) : kotlin.collections.List<Item> {
	val mutableList = toMutableList()
	mutableList.removeAll(predicate)
	return mutableList
}


inline fun <Item> Iterable<Item>.atLeastOne (predicate: (Item) -> Boolean) : Boolean {
	forEach { item -> if (predicate(item)) return true }; return false
}


inline fun <Item> kotlin.collections.List<Item>.findIndex (predicate: (Item) -> Boolean) : Int {
	for (i in 0 until size) {
		if (predicate(this[i])) {
			return i
		}
	}; throw RuntimeException()
}


inline fun <Item> MutableList<Item>.removeOne (predicate: (Item) -> Boolean) {
	for (i in 0 until size) {
		if (predicate(this[i])) {
			removeAt(i)
			return
		}
	}
}


inline fun <Key, Value> MutableMap<Key, Value>.createIfNull (
	key: Key, createItem: () -> Value
) : Value {
	val oldValue = this[key]
	if (oldValue == null) {
		val newValue = createItem()
		this[key] = newValue
		return newValue
	} else {
		return oldValue
	}
}

inline fun <Item> Iterator<Item>.toFiniteSet () : FiniteSet<Item> {
	val editableSet = EditableSet<Item>()
	forEach { editableSet.add(it) }
	return editableSet
}
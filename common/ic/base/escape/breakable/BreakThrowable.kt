package ic.base.escape.breakable


@Deprecated("To remove")
object Break : Throwable() {

	@Suppress("NOTHING_TO_INLINE")
	@Deprecated("Use ic.base.escape.breakable.Break()")
	inline fun Break() : Nothing {
		@Suppress("DEPRECATION")
		throw Break
	}
	
}
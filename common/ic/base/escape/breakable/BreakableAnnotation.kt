package ic.base.escape.breakable


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.VALUE_PARAMETER
)
annotation class Breakable
package ic.base.escape.breakable



@Deprecated("To remove")
inline fun breakable (

	onBreak : () -> Unit = {},

	@Breakable action : () -> Unit

) {

	try {
		action()
	} catch (@Suppress("DEPRECATION") _: Break) {
		onBreak()
	}
	
}
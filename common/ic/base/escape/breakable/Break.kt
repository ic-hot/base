package ic.base.escape.breakable


@Suppress("NOTHING_TO_INLINE", "FunctionName")
@Deprecated("To remove")
inline fun Break() : Nothing {
	@Suppress("DEPRECATION")
	throw Break
}
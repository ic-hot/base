package ic.base.escape


class Escape : Throwable() {

	@Suppress("NOTHING_TO_INLINE")
	inline operator fun invoke() : Nothing = throw this

}
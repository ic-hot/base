package ic.base.escape.skip


inline fun skippable (

	action: () -> Unit

) {

	try {
		return action()
	} catch (_: Skip) {}
	
}


inline fun <Result> skippable (

	onSkip: () -> Result,

	action: () -> Result

) : Result {

	try {
		return action()
	} catch (_: Skip) {
		return onSkip()
	}

}
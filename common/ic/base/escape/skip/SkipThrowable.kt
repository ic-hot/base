package ic.base.escape.skip


object Skip : Throwable() {

	@Suppress("NOTHING_TO_INLINE")
	@Deprecated("Use ic.base.escape.Skip()")
	inline fun Skip() : Nothing = throw Skip
	
}
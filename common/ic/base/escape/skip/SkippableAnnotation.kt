package ic.base.escape.skip


@MustBeDocumented
@Target(
	AnnotationTarget.FUNCTION,
	AnnotationTarget.VALUE_PARAMETER,
	AnnotationTarget.PROPERTY
)
annotation class Skippable
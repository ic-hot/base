package ic.base.escape.skip


@Suppress("NOTHING_TO_INLINE")
@Deprecated("Use skip", ReplaceWith("skip"))
inline fun Skip() : Nothing = throw Skip


inline val skip : Nothing get() = throw Skip
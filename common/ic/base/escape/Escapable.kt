package ic.base.escape


inline fun escapable (

	action : (escape: Escape) -> Unit

) {

	val escape = Escape()

	try {
		return action(escape)
	} catch (t: Throwable) {
		if (t !== escape) {
			throw t
		}
	}

}
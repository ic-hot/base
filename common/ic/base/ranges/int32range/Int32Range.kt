package ic.base.ranges.int32range


import ic.base.primitives.int32.Int32


interface Int32Range {

	val start : Int32

	val end : Int32

}
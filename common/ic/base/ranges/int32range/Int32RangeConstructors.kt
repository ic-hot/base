@file:Suppress("NOTHING_TO_INLINE")


package ic.base.ranges.int32range


import ic.base.primitives.int32.Int32


inline fun Int32Range (

	start	: Int32,
	end 	: Int32

) : Int32Range {

	return ConstantInt32Range(start, end)

}
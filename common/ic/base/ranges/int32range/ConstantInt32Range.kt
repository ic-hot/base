package ic.base.ranges.int32range


import ic.base.primitives.int32.Int32


class ConstantInt32Range (

	override val start	: Int32,
	override val end	: Int32

) : Int32Range
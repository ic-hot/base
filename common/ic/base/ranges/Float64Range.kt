package ic.base.ranges


import ic.base.primitives.float64.Float64


class Float64Range (

	val from : Float64,

	val to : Float64

) {

	override fun toString() = "Float64Range ( from: $from, to: $to )"

}
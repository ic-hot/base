package ic.base.objects.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


val Any.isPrimitive get() = when (this) {

	is Boolean -> true

	is Int32 -> true

	is Int64 -> true

	is Float32 -> true

	is Float64 -> true

	else -> false

}
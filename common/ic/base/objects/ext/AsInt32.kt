package ic.base.objects.ext


import ic.base.annotations.Throws
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@Throws(NotSupportedException::class)
val Any?.asInt32OrThrow : Int32 get() = when (this) {
	is Int32 -> this
	is Int64 -> this.asInt32
	is Float32 -> this.asInt32
	is Float64 -> this.asInt32
	else -> throw NotSupportedException
}


inline val Any?.asInt32 : Int32 get() {
	try {
		return asInt32OrThrow
	} catch (_: NotSupportedException) {
		throw NotSupportedException.Runtime("this.className: ${ this.className }")
	}
}


inline val Any?.asInt32OrNull : Int32? get() {
	try {
		return asInt32OrThrow
	} catch (_: NotSupportedException) {
		return null
	}
}
@file:Suppress("DEPRECATION")


package ic.base.objects.ext


import ic.base.escape.breakable.Break


inline val <Value> Value?.orBreak : Value get() = if (this == null) Break() else this
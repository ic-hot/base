package ic.base.objects.ext


import ic.ifaces.immut.MaybeImmutable


val Any?.isImmutable : Boolean get() = when {

	this == null -> true

	this is MaybeImmutable -> isImmutable

	isPrimitive -> true

	this is String -> true

	else -> false

}
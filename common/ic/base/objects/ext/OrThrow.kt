@file:Suppress("NOTHING_TO_INLINE")


package ic.base.objects.ext


inline infix fun <Value: Any> Value?.orThrow (throwable: Throwable) : Value {

	return this ?: throw throwable

}
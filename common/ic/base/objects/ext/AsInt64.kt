package ic.base.objects.ext


import ic.base.annotations.Throws
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skip
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@Throws(NotSupportedException::class)
val Any?.asInt64OrThrow : Int64 get() = when (this) {
	is Int32 -> this.asInt64
	is Int64 -> this
	is Float32 -> this.asInt64
	is Float64 -> this.asInt64
	else -> throw NotSupportedException
}


val Any?.asInt64 : Int64 get() {
	try {
		return asInt64OrThrow
	} catch (_: NotSupportedException) {
		throw RuntimeException("className: ${ this.className }")
	}
}


inline val Any?.asInt64OrNull : Int64? get() {
	try {
		return asInt64OrThrow
	} catch (_: NotSupportedException) {
		return null
	}
}


@Skippable
inline val Any?.asInt64OrSkip : Int64 get() {
	try {
		return asInt64OrThrow
	} catch (_: NotSupportedException) {
		skip
	}
}

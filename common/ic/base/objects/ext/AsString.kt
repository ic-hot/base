package ic.base.objects.ext


inline val Any?.asStringOrNull : String? get() = run {
	when (this) {
		is String -> this
		else      -> null
	}
}
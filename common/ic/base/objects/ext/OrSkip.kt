package ic.base.objects.ext


import ic.base.escape.skip.skip


inline val <Value: Any> Value?.orSkip : Value get() = this ?: skip
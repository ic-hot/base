package ic.base.objects.ext


import ic.base.annotations.Throws
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat64
import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException


@Throws(NotSupportedException::class)
val Any?.asFloat64OrThrow : Float64 get() = when (this) {
	is Int32 -> this.asFloat64
	is Int64 -> this.asFloat64
	is Float32 -> this.asFloat64
	is Float64 -> this
	else -> throw NotSupportedException
}


inline val Any?.asFloat64 : Float64 get() {
	try {
		return asFloat64OrThrow
	} catch (_: NotSupportedException) {
		throw NotSupportedException.Runtime("this.className: ${ this.className }")
	}
}


inline val Any?.asFloat64OrNull : Float64? get() {
	try {
		return asFloat64OrThrow
	} catch (_: NotSupportedException) {
		return null
	}
}
package ic.base.assert


import ic.app.tier.Tier
import ic.app.tier.tier


inline fun assert (getMessage: () -> String?, condition: () -> Boolean) {

	if (tier === Tier.Production) return

	if (!condition()) {
		throw AssertionError(
			getMessage()
		)
	}
	
}


inline fun assert (message: String? = null, condition: () -> Boolean) {
	assert(getMessage = { message }, condition = condition)
}
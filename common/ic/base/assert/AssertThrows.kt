package ic.base.assert


import kotlin.reflect.KClass

import ic.base.reflect.isInstanceOf


inline fun assertThrows (
	throwableClass1 : KClass<out Throwable>,
	block : () -> Unit
) {
	try {
		block()
		throw AssertionError()
	} catch (t: Throwable) {
		when {
			t isInstanceOf throwableClass1 -> {}
			else -> throw t
		}
	}
}
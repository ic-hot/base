package ic.base.assert


import ic.base.reflect.isInstanceOf
import kotlin.reflect.KClass


inline fun <Result> assertDoesNotThrow (
	throwableClass1 : KClass<out Throwable>,
	block : () -> Result
) : Result {
	try {
		return block()
	} catch (t: Throwable) {
		when {
			t isInstanceOf throwableClass1 -> throw AssertionError()
			else -> throw t
		}
	}
}

inline fun <Result> assertDoesNotThrow (
	throwableClass1 : KClass<out Throwable>,
	throwableClass2 : KClass<out Throwable>,
	block : () -> Result
) : Result {
	try {
		return block()
	} catch (t: Throwable) {
		when {
			t isInstanceOf throwableClass1 -> throw AssertionError()
			t isInstanceOf throwableClass2 -> throw AssertionError()
			else -> throw t
		}
	}
}
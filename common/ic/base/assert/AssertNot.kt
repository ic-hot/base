package ic.base.assert


inline fun assertNot (getMessage: () -> String?, condition: () -> Boolean) {

	assert(getMessage = getMessage) { !condition() }
	
}


inline fun assertNot (message: String? = null, condition: () -> Boolean) {
	assertNot(getMessage = { message }, condition = condition)
}


import "ll.fun.Function"
import "ll.fun.Invoke"

import "ic.base.arrays.ext.AsList"
import "ic.struct.list.funs.Add"
import "ic.struct.list.ext.AsArray"


addToArray function { Array array, NewItem

	List array.AsList

	NewList invoke addToList { List addToArray.List, NewItem addToArray.NewItem }

	Result NewList.AsArray

}
package ic.base.arrays.funs


import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32


fun areEqual (a: ByteArray, b: ByteArray) : Boolean {

	if (a.length != b.length) return false

	var i : Int32 = 0

	while (i < a.length) {

		if (a[i] != b[i]) return false

		i++

	}

	return true

}
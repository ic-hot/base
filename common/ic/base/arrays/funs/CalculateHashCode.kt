package ic.base.arrays.funs


import ic.base.primitives.int32.Int32


fun calculateHashCodeOf (of: ByteArray) : Int32 {

	var hashCode : Int32 = 0

	of.forEach { byte ->

		hashCode *= 31

		hashCode += byte

	}

	return hashCode

}
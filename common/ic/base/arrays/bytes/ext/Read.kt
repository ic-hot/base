package ic.base.arrays.bytes.ext


import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.input.ByteInput
import ic.stream.input.frombytearray.ByteInputFromByteArray


inline fun ByteArray.read (
	action: ByteInput.() -> Unit
) {
	ByteInputFromByteArray(this).runAndCloseOrCancel(action)
}
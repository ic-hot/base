package ic.base.arrays.bytes.ext.copy


import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.math.funs.min


fun ByteArray.copyLimit (limit: Int32) : ByteArray {

	return copyRange(
		from = 0,
		to = min(length, limit)
	)

}
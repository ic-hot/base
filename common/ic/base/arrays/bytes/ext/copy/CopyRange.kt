package ic.base.arrays.bytes.ext.copy


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.copyRange (from: Int32, to: Int32) : ByteArray {

	return copyOfRange(from, to)

}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.copyRange (from: Int64, to: Int64) : ByteArray {

	return copyOfRange(from.asInt32, to.asInt32)

}
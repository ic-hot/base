package ic.base.arrays.bytes.ext


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray


inline val ByteArray.asByteSequence : ByteSequence get() {

	return ByteSequenceFromByteArray(this)

}
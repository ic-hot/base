package ic.base.arrays.bytes.ext.reduce


import ic.base.arrays.ext.length
import ic.base.loop.repeat


inline fun ByteArray.atLeastOne (condition : (Byte) -> Boolean) : Boolean {
	repeat(length) { index ->
		if (condition(this[index])) return true
	}
	return false
}
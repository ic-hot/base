package ic.base.arrays.bytes.ext.reduce


inline fun ByteArray.none (condition : (Byte) -> Boolean) : Boolean {
	return !atLeastOne(condition)
}
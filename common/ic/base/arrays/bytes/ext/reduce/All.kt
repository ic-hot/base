package ic.base.arrays.bytes.ext.reduce


inline fun ByteArray.all (condition : (Byte) -> Boolean) : Boolean {
	return none { !condition(it) }
}
package ic.base.arrays.bytes.ext


import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline operator fun ByteArray.get (index: Int64) = get(index.asInt32)
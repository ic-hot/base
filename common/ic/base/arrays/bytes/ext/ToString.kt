package ic.base.arrays.bytes.ext


import ic.util.text.charset.Charset
import ic.util.text.charset.impl.charsetsEngine


fun ByteArray.toString (charset: Charset) : String {

	return charsetsEngine.byteArrayToString(this, charset)

}
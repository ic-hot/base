package ic.base.arrays.bytes.funs


import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ext.toByteArray


@Throws(UnableToParseException::class)
fun parseByteArrayFromHexStringOrThrow (string: String) : ByteArray {

	return parseByteSequenceFromHexStringOrThrow(string).toByteArray()

}
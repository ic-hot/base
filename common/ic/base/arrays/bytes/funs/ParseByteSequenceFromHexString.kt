package ic.base.arrays.bytes.funs


import ic.base.primitives.bytes.funs.parseByteFromHexStringOrThrowUnableToParse
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.getSubstring
import ic.base.throwables.UnableToParseException
import ic.stream.buffer.ByteBuffer
import ic.stream.sequence.ByteSequence


@Throws(UnableToParseException::class)
fun parseByteSequenceFromHexStringOrThrow (string: String) : ByteSequence {

	val byteBuffer = ByteBuffer()

	var indexInString : Int32 = 0

	while (indexInString < string.length) {

		val substringLength = (
			if (indexInString == 0) {
				if (string.length % 2 == 0) {
					2
				} else {
					1
				}
			} else {
				2
			}
		)

		val substring = string.getSubstring(indexInString, indexInString + substringLength)

		byteBuffer.putByte(
			parseByteFromHexStringOrThrowUnableToParse(substring)
		)

		indexInString += substringLength

	}

	return byteBuffer

}
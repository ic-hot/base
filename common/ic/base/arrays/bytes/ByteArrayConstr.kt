package ic.base.arrays.bytes


import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray() : ByteArray = EmptyByteArray


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray (vararg items: Byte) : ByteArray = items

@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray (length: Int32) : ByteArray {
	if (length == 0) return EmptyByteArray
	return kotlin.ByteArray(length)
}

@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray (length: Int64) = ByteArray(length = length.asInt32)

inline fun ByteArray (length: Int32, generateItem: (index: Int32) -> Byte) : ByteArray {
	val byteArray = kotlin.ByteArray(length)
	var index : Int32 = 0
	while (index < length) {
		byteArray[index] = generateItem(index)
		index++
	}
	return byteArray
}

@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray (vararg items: Int32) : ByteArray {
	return ByteArray(items.length) { index ->
		items[index].toByte()
	}
}
package ic.base.arrays.bytes


import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.buffer.ByteBuffer
import ic.stream.output.ByteOutput


inline fun ByteArray (

	write : ByteOutput.() -> Unit

) : ByteArray {

	return ByteBuffer().apply { runAndCloseOrCancel(write) }.toByteArray()

}
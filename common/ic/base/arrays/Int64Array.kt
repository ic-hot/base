@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.arrays


import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


typealias Int64Array = LongArray

inline fun Int64Array (vararg items: Int64) : Int64Array = items

inline fun Int64Array (length: Int64, initItem : (index: Int64) -> Int64) : Int64Array {
	return LongArray(size = length.asInt32) { index ->
		initItem(index.asInt64)
	}
}
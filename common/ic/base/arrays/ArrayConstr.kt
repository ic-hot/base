@file:Suppress("FunctionName")


package ic.base.arrays


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


inline fun <reified Item> Array (length: Int32) : Array<Item?> {
	return arrayOfNulls(length)
}

inline fun <reified Item> Array (length: Int64) : Array<Item?> {
	return Array(length.asInt32)
}


inline fun <reified Item> Array (
	length : Int64,
	initItem : (index: Int64) -> Item
) : Array<Item> {
	return Array(length.asInt32) { index ->
		initItem(index.asInt64)
	}
}


inline fun <reified Item> Array (vararg items: Item) : Array<Item> {
	@Suppress("UNCHECKED_CAST")
	return items as Array<Item>
}

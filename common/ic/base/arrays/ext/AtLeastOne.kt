package ic.base.arrays.ext


inline fun <Item> Array<Item>.atLeastOne (condition: (Item) -> Boolean) : Boolean {
	forEach { item ->
		if (condition(item)) return true
	}
	return false
}


inline fun ByteArray.atLeastOne (condition: (Byte) -> Boolean) : Boolean {
	forEach { item ->
		if (condition(item)) return true
	}
	return false
}
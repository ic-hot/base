package ic.base.arrays.ext.copy


import ic.base.arrays.ext.copy.convert.copyConvertToFiniteSet
import ic.struct.set.finite.FiniteSet


fun <Item> Array<out Item>.copyToFiniteSet() : FiniteSet<Item> {
	return copyConvertToFiniteSet { it }
}
package ic.base.arrays.ext.copy.convert


import ic.base.arrays.ext.forEach
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <SourceItem, NewItem> Array<SourceItem>.copyConvertToList (

	@Skippable convertItem : (SourceItem) -> NewItem

) : List<NewItem> {

	val editableList = EditableList<NewItem>()

	forEach { oldItem ->
		skippable {
			editableList.add(
				convertItem(oldItem)
			)
		}
	}

	return editableList

}
package ic.base.arrays.ext.copy.convert


import ic.base.arrays.ext.forEach
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.struct.set.editable.EditableSet
import ic.struct.set.finite.FiniteSet


inline fun <SourceItem, NewItem> Array<SourceItem>.copyConvertToFiniteSet (

	@Skippable convertItem : (SourceItem) -> NewItem

) : FiniteSet<NewItem> {

	val result = EditableSet<NewItem>()

	forEach { oldItem ->
		skippable {
			result.addIfNotExists(
				convertItem(oldItem)
			)
		}
	}

	return result

}

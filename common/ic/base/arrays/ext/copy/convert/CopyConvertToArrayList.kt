package ic.base.arrays.ext.copy.convert


import ic.base.arrays.ext.forEach
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable


inline fun <SourceItem, NewItem> Array<SourceItem>.copyConvertToArrayList (

	@Skippable convertItem : (SourceItem) -> NewItem

) : ArrayList<NewItem> {

	val arrayList = ArrayList<NewItem>()

	forEach { oldItem ->
		skippable {
			arrayList.add(
				convertItem(oldItem)
			)
		}
	}

	return arrayList

}
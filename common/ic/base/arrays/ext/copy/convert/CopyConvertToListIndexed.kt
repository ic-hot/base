package ic.base.arrays.ext.copy.convert


import ic.base.arrays.ext.forEach
import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.editable.EditableList


inline fun <SourceItem, NewItem> Array<SourceItem>.copyConvertToListIndexed (

	@Skippable convertItem : (index: Int32, SourceItem) -> NewItem

) : List<NewItem> {
	var index : Int32 = 0
	val result = EditableList<NewItem>()
	forEach { oldItem ->
		skippable {
			result.add(
				convertItem(index, oldItem)
			)
			index++
		}
	}
	result.freeze()
	return result
}
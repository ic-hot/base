package ic.base.arrays.ext.copy.convert


import ic.base.escape.skip.skippable
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.copy.toArray


inline fun <SourceItem, reified Item> Array<SourceItem>.copyConvert (

	convertItem : (SourceItem) -> Item

) : Array<Item> {

	val editableList = EditableList<Item>()
	for (sourceItem in this) {
		skippable {
			editableList.add(
				convertItem(sourceItem)
			)
		}
	}
	return editableList.toArray()

}
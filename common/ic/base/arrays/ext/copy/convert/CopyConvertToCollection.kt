package ic.base.arrays.ext.copy.convert


import ic.base.arrays.ext.copy.copyToList
import ic.base.escape.skip.Skippable
import ic.struct.collection.Collection


inline fun <SourceItem, NewItem> Array<SourceItem>.copyConvertToCollection (

	@Skippable convertItem : (SourceItem) -> NewItem

) : Collection<NewItem> {

	return copyConvertToList(convertItem)

}
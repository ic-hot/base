package ic.base.arrays.ext.copy


import ic.struct.collection.Collection


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Array<out Item>.copyToCollection() : Collection<Item> {
	return copyToList()
}
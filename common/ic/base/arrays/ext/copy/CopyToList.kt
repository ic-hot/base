package ic.base.arrays.ext.copy


import ic.base.arrays.ext.copy.convert.copyConvertToList
import ic.struct.list.List


fun <Item> Array<out Item>.copyToList() : List<Item> {
	return copyConvertToList { it }
}
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.write (startIndex: Int32, bytes: ByteArray) {
	bytes.copyInto(this, destinationOffset = startIndex)
}
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.setRange (startIndex: Int32, subArray: ByteArray) {

	subArray.forEachIndexed { index, byte ->
		this[startIndex + index] = byte
	}

}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.setRange (startIndex: Int64, subArray: ByteArray) {
	setRange(
		startIndex = startIndex.asInt32,
		subArray = subArray
	)
}
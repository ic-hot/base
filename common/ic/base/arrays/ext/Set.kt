package ic.base.arrays.ext


import ic.base.arrays.Int32Array
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline operator fun <Item> Array<Item>.set (index: Int64, value: Item) {
	this[index.asInt32] = value
}


@Suppress("NOTHING_TO_INLINE")
inline operator fun ByteArray.set (index: Int64, value: Byte) {
	this[index.asInt32] = value
}


@Suppress("NOTHING_TO_INLINE")
inline operator fun Int32Array.set (index: Int64, value: Int32) {
	this[index.asInt32] = value
}
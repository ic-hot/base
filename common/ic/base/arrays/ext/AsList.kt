package ic.base.arrays.ext


import ic.struct.list.List
import ic.struct.list.fromarray.ListFromArray


inline val <Item> Array<Item>.asList : List<Item> get() {
	return ListFromArray(this, isArrayImmutable = true)
}

@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Array<Item>.asList (isArrayImmutable: Boolean) : List<Item> {
	return ListFromArray(this, isArrayImmutable = isArrayImmutable)
}
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32


inline fun <Item> Array<Item>.minInt32 (itemToInt32: (Item) -> Int32) : Int32 {

	var minInt32 : Int32 = Int32.MAX_VALUE

	var index : Int32 = 0

	while (index < length) {

		val int32 = itemToInt32(this[index])

		if (int32 < minInt32) minInt32 = int32

		index++

	}

	return minInt32

}
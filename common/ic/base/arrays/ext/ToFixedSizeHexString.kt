package ic.base.arrays.ext


import ic.base.primitives.bytes.ext.asFixedSizeHexString


fun ByteArray.toFixedSizeHexString() : String {

	return buildString {
		forEach { byte: Byte ->
			append(
				byte.asFixedSizeHexString
			)
		}
	}

}
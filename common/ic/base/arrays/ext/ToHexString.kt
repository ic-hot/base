package ic.base.arrays.ext


import ic.base.primitives.bytes.ext.asFixedSizeHexString
import ic.text.TextBuffer


fun ByteArray.toHexString() : String {

	val result = TextBuffer()
	forEach { byte ->
		result.write(
			byte.asFixedSizeHexString
		)
	}
	return result.toString()

}
package ic.base.arrays.ext


import ic.base.arrays.Float32Array
import ic.base.arrays.Float64Array
import ic.base.arrays.Int32Array
import ic.base.arrays.Int64Array
import ic.base.primitives.int32.Int32


inline val <Item> Array<Item>.length : Int32 get() = size

inline val ByteArray.length : Int32 get() = size

inline val Int32Array.length : Int32 get() = size

inline val Int64Array.length : Int32 get() = size

inline val Float32Array.length : Int32 get() = size

inline val Float64Array.length : Int32 get() = size


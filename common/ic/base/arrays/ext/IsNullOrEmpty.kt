package ic.base.arrays.ext


inline val <Item> Array<Item>?.isNullOrEmpty : Boolean get() = this == null || isEmpty
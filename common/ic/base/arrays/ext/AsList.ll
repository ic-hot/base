

import "ll.bool.Branch"
import "ll.bool.Equals"

import "ic.struct.list.List"


array {

	AsList list {
		Length array.Length
		item {
			Result branch [
				= [ Index, 0 ] -> array.0
				= [ Index, 1 ] -> array.1
				= [ Index, 2 ] -> array.2
				= [ Index, 3 ] -> array.3
				= [ Index, 4 ] -> array.4
				= [ Index, 5 ] -> array.5
				= [ Index, 6 ] -> array.6
				= [ Index, 7 ] -> array.7
			]
		}
	}
	
}
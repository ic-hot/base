package ic.base.arrays.ext


import ic.base.arrays.Int64Array
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


inline fun <OldItem, NewItem> Array<OldItem>.toFiniteSet (
	convertItem : (OldItem) -> NewItem
) : FiniteSet<NewItem> {
	val editableSet = EditableSet<NewItem>()
	forEach { oldItem ->
		skippable {
			editableSet.addIfNotExists(
				convertItem(oldItem)
			)
		}
	}
	return editableSet
}


fun <Item> Array<Item>.toFiniteSet() = toFiniteSet { it }


inline fun <NewItem> Int64Array.toFiniteSet (
	convertItem : (Int64) -> NewItem
) : FiniteSet<NewItem> {
	val editableSet = EditableSet<NewItem>()
	forEach { oldItem ->
		skippable {
			editableSet.addIfNotExists(
				convertItem(oldItem)
			)
		}
	}
	return editableSet
}


fun Int64Array.toFiniteSet() = toFiniteSet { it }
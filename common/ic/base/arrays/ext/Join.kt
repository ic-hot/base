@file:Suppress("NOTHING_TO_INLINE")


package ic.base.arrays.ext


import ic.base.primitives.character.Character


inline fun Array<String>.join (separator: String = "") : String {

	return joinToString(separator = separator)

}


inline fun Array<String>.join (separator: Character) : String {
	return join(
		separator = separator.toString()
	)
}
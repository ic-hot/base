@file:Suppress("NOTHING_TO_INLINE")


package ic.base.arrays.ext


inline fun <Item> Array<Item>.getLast() = this[lastIndex]
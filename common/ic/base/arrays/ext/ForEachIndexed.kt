package ic.base.arrays.ext


import ic.base.loop.repeat
import ic.base.primitives.int32.Int32


inline fun <Item> Array<Item>.forEachIndexed (action: (Int32, Item) -> Unit) {

	repeat(length) { index ->

		action( index, this[index] )

	}

}
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32


inline fun <Item> Array<Item>.maxInt32 (itemToInt32: (Item) -> Int32) : Int32 {

	var maxInt32 : Int32 = Int32.MIN_VALUE

	var index : Int32 = 0

	while (index < length) {

		val int32 = itemToInt32(this[index])

		if (int32 > maxInt32) maxInt32 = int32

		index++

	}

	return maxInt32

}
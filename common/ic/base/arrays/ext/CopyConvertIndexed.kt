package ic.base.arrays.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.loop.repeat
import ic.base.primitives.int32.Int32


inline fun <SourceItem, reified Item> Array<SourceItem>.copyConvertIndexed (

	@Skippable convertItem : (index: Int32, SourceItem) -> Item

) : Array<Item> {

	val javaList = ArrayList<Item>()

	repeat(length) { index ->

		skippable {

			javaList.add(
				convertItem(index, this[index])
			)

		}

	}

	return javaList.toTypedArray()

}
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun <reified Item> Array<Item>.copy (length: Int32) = copyOf(length)

@Suppress("NOTHING_TO_INLINE")
inline fun <reified Item> Array<Item>.copy (length: Int64) = copyOf(length.asInt32)


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.copy (length: Int32 = this.length) = copyOf(newSize = length)

@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.copy (length: Int64) = copyOf(newSize = length.asInt32)
package ic.base.arrays.ext


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.clear() {

	fill(0)

}
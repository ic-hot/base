package ic.base.arrays.ext


import ic.math.rand.randomInt32


fun <Item> Array<Item>.getRandom (seed: String) : Item {

	return this [ randomInt32(seed = seed, limit = length) ]

}
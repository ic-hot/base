@file:Suppress("DEPRECATION")

package ic.base.arrays.ext


import ic.base.primitives.int32.Int32
import ic.base.throwables.IndexOutOfBoundsException


@Deprecated("Use ic.base.arrays.ext.copy.copyRange")
inline fun <reified Item> Array<Item>.copyRange (from: Int32, to: Int32) : Array<Item> {

	return copyOfRange(from, to)

}


@Deprecated("Use ic.base.arrays.bytes.ext.copy.copyRange")
@Suppress("NOTHING_TO_INLINE")
@Throws(IndexOutOfBoundsException::class)
inline fun ByteArray.copyRangeOrThrowIndexOutOfBounds (
	fromIndex : Int32, toIndex : Int32
) : ByteArray {
	when {
		fromIndex < 0 || fromIndex >= length -> throw IndexOutOfBoundsException
		toIndex   < 0 || toIndex   >= length -> throw IndexOutOfBoundsException
		toIndex < fromIndex -> throw IndexOutOfBoundsException
	}
	return copyOfRange(fromIndex, toIndex)
}

@Deprecated("Use ic.base.arrays.bytes.ext.copy.copyRange")
inline fun ByteArray.copyRange (
	fromIndex : Int32, toIndex : Int32, or : () -> ByteArray
) : ByteArray {
	try {
		return copyRangeOrThrowIndexOutOfBounds(fromIndex = fromIndex, toIndex = toIndex)
	} catch (t: IndexOutOfBoundsException) {
		return or()
	}
}

@Deprecated("Use ic.base.arrays.bytes.ext.copy.copyRange")
@Suppress("NOTHING_TO_INLINE")
@Throws(IndexOutOfBoundsException::class)
inline fun ByteArray.copyRange (
	fromIndex : Int32, toIndex : Int32, orThrow : Throwable
) : ByteArray {
	try {
		return copyRangeOrThrowIndexOutOfBounds(fromIndex = fromIndex, toIndex = toIndex)
	} catch (t: IndexOutOfBoundsException) {
		throw orThrow
	}
}

@Deprecated("Use ic.base.arrays.bytes.ext.copy.copyRange")
@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.copyRange (fromIndex: Int32, toIndex: Int32) : ByteArray {
	when {
		fromIndex < 0 || fromIndex >= length -> {
			throw IndexOutOfBoundsException.Runtime(
				length = length,
				index = fromIndex
			)
		}
		toIndex < 0 || toIndex >= length -> {
			throw IndexOutOfBoundsException.Runtime(
				length = length,
				index = toIndex
			)
		}
		toIndex < fromIndex -> {
			throw IndexOutOfBoundsException.Runtime(
				length = length,
				index = toIndex,
				message = "Invalid range fromIndex: $fromIndex toIndex: $toIndex"
			)
		}
	}
	return copyOfRange(fromIndex, toIndex)
}
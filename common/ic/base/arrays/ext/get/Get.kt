package ic.base.arrays.ext.get


import ic.base.arrays.Float32Array
import ic.base.arrays.Float64Array
import ic.base.arrays.Int32Array
import ic.base.arrays.Int64Array
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline operator fun <Item> Array<Item>.get (index: Int64) = get(index.asInt32)


@Suppress("NOTHING_TO_INLINE")
inline operator fun ByteArray.get (index: Int64) = get(index.asInt32)


@Suppress("NOTHING_TO_INLINE")
inline operator fun Int32Array.get (index: Int64) = get(index.asInt32)

@Suppress("NOTHING_TO_INLINE")
inline operator fun Int64Array.get (index: Int64) = get(index.asInt32)


@Suppress("NOTHING_TO_INLINE")
inline operator fun Float32Array.get (index: Int64) = get(index.asInt32)

@Suppress("NOTHING_TO_INLINE")
inline operator fun Float64Array.get (index: Int64) = get(index.asInt32)
package ic.base.arrays.ext.get


import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Array<Item>.getOrNull (index: Int64) = getOrNull(index.asInt32)

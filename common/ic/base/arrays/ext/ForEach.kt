package ic.base.arrays.ext


import ic.base.loop.breakableRepeat
import ic.ifaces.action.action1.Action1


inline fun <Item> Array<Item>.forEach (
	action : (Item) -> Unit
) {
	repeat(length) { index ->
		action(
			this[index]
		)
	}
}


@Deprecated("To remove break")
fun <Item> Array<Item>.breakableForEach (action: Action1<Item>) {
	@Suppress("DEPRECATION")
	breakableRepeat (length) { index ->
		action.run(
			this[index]
		)
	}
}


@Deprecated("To remove break")
inline fun <Item> Array<Item>.breakableForEach (
	onBreak : () -> Unit = {},
	action : (Item) -> Unit
) {
	@Suppress("DEPRECATION")
	breakableRepeat(
		length,
		onBreak = onBreak
	) { index ->
		action(
			this[index]
		)
	}
}
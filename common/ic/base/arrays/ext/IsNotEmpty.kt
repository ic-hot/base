package ic.base.arrays.ext


import ic.base.arrays.Float32Array
import ic.base.arrays.Int32Array


inline val Array<*>.isNotEmpty : Boolean get() = !isEmpty

inline val ByteArray.isNotEmpty : Boolean get() = !isEmpty

inline val Int32Array.isNotEmpty : Boolean get() = !isEmpty

inline val Float32Array.isNotEmpty : Boolean get() = !isEmpty
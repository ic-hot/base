package ic.base.arrays.ext


import ic.base.loop.repeatInReverse
import ic.base.primitives.int32.ext.asByte


fun ByteArray.incrementAsInt() {

	repeatInReverse(length) { byteIndex ->

		this[byteIndex]++

		if (this[byteIndex] != 0.asByte) {
			return
		}

	}

}
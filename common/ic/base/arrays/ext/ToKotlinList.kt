package ic.base.arrays.ext


inline fun <OldItem, NewItem> Array<OldItem>.toKotlinList (

	crossinline convertItem : (OldItem) -> NewItem

) : List<NewItem> {

	val mutableList = mutableListOf<NewItem>()

	forEach { oldItem ->
		mutableList.add(
			convertItem(oldItem)
		)
	}

	return mutableList

}
package ic.base.arrays.ext


inline fun <reified Item> Array<out Item>.copyFilter (

	predicate : (Item) -> Boolean

) : Array<Item> {

	val javaList = ArrayList<Item>()

	forEach { item ->

		if (predicate(item)) javaList.add(item)

	}

	return javaList.toTypedArray()

}
package ic.base.arrays.ext


import ic.base.arrays.Float32Array
import ic.base.arrays.Int32Array


inline val Array<*>.isEmpty : Boolean get() = isEmpty()

inline val ByteArray.isEmpty : Boolean get() = isEmpty()

inline val Int32Array.isEmpty : Boolean get() = isEmpty()

inline val Float32Array.isEmpty : Boolean get() = isEmpty()
package ic.base.arrays.ext


import ic.base.primitives.int32.Int32


inline fun <Item> Array<Item>.findOrNull (predicate : (Item) -> Boolean) : Item? {

	var index : Int32 = 0

	while (index < length) {

		val item = this[index]

		if (predicate(item)) {
			return item
		}

		index++

	}

	return null

}
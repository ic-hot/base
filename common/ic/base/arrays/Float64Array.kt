package ic.base.arrays

import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


typealias Float64Array = DoubleArray


inline fun Float64Array (length: Int64, initItem : (index: Int64) -> Float64) : Float64Array {
	return DoubleArray(size = length.asInt32) { index ->
		initItem(index.asInt64)
	}
}
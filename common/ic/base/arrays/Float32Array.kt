package ic.base.arrays


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


typealias Float32Array = FloatArray

inline fun Float32Array (length: Int64, initItem : (index: Int64) -> Float32) : Float32Array {
	return FloatArray(size = length.asInt32) { index ->
		initItem(index.asInt64)
	}
}
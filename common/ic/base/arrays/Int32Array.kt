@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.arrays


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


typealias Int32Array = IntArray

inline fun Int32Array (length: Int64) : Int32Array = IntArray(size = length.asInt32)

inline fun Int32Array (vararg items: Int32) : Int32Array = items

inline fun Int32Array (length: Int64, initItem : (index: Int64) -> Int32) : Int32Array {
	return IntArray(size = length.asInt32) { index ->
		initItem(index.asInt64)
	}
}
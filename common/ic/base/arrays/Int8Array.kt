@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.arrays


import ic.base.primitives.int8.Int8


typealias Int8Array = ByteArray

inline fun Int8Array (vararg items: Int8) : Int8Array = items
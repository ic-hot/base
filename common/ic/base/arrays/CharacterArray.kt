@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.arrays


import ic.base.primitives.character.Character


typealias CharacterArray = CharArray

inline fun CharacterArray (vararg items: Character) : CharArray = items
package ic.base


fun <Item> Array<Item>.contains (predicate: (Item) -> Boolean) : Boolean {
	for (item in this) {
		if (predicate(item)) return true
	}; return false
}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.float32


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32


typealias Float32 = Float


inline fun Float32 (int32: Int32) : Float32 = int32.toFloat()

inline fun Float32 (float64: Float64) : Float32 = float64.toFloat()
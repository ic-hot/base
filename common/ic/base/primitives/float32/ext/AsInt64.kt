package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64


inline val Float32.asInt64 : Int64 get() = toLong()
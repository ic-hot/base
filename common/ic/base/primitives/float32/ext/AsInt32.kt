package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


inline val Float32.asInt32 : Int32 get() = toInt()
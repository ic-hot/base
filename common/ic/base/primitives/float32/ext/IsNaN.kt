package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32


inline val Float32.isNaN : Boolean get() = isNaN()
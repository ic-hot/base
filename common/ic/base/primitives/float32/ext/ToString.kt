package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32
import ic.util.locale.Locale
import ic.util.locale.currentLocale
import ic.util.locale.impl.localeEngine


fun Float32.toString (

	pattern : String,

	locale : Locale = currentLocale,

	groupingSeparator : Char = ' '

) : String {

	return localeEngine.formatFloat64(
		float64 = this.asFloat64,
		pattern = pattern,
		locale = locale,
		groupingSeparator = groupingSeparator
	)

}
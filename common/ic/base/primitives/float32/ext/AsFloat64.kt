package ic.base.primitives.float32.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64


inline val Float32.asFloat64 : Float64 get() = toDouble()
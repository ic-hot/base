package ic.base.primitives.bool


fun or (vararg args: Boolean) : Boolean {

	for (arg in args) {

		if (arg) return true

	}

	return false

}


fun or (vararg args: () -> Boolean) : Boolean {

	for (arg in args) {

		if (arg()) return true

	}

	return false

}
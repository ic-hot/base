package ic.base.primitives.bool.ext


inline val Boolean.asFloat32 get() = if (this) 1F else 0F
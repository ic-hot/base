@file:Suppress("NOTHING_TO_INLINE")


package ic.base.primitives.bool.ext


inline fun <Result> Boolean.then (then: Result, orElse: Result) : Result {

	return if (this) {
		then
	} else {
		orElse
	}

}
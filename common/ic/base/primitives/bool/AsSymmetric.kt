package ic.base.primitives.bool


inline val Boolean.asSymmetricFloat64 get() = booleanToSymmetricFloat64(this)

inline val Boolean.asSymmetricFloat32 get() = booleanToSymmetricFloat32(this)
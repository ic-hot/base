package ic.base.primitives.bool


fun and (vararg args: Boolean) : Boolean {

	for (arg in args) {

		if (!arg) return false

	}

	return true

}
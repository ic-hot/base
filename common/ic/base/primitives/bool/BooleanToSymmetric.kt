@file:Suppress("NOTHING_TO_INLINE")


package ic.base.primitives.bool


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64


inline fun booleanToSymmetricFloat64 (b: Boolean) : Float64 {
	if (b) {
		return 1.0
	} else {
		return -1.0
	}
}


inline fun booleanToSymmetricFloat32 (b: Boolean) : Float32 {
	if (b) {
		return 1.0F
	} else {
		return -1.0F
	}
}
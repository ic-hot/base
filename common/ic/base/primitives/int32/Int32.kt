@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.int32


import ic.base.primitives.int64.Int64


typealias Int32 = Int


inline fun Int32 (int64: Int64) : Int32 = int64.toInt()


package ic.base.primitives.int32.ext


import ic.base.primitives.bytes.ext.asFixedSizeHexString
import ic.base.primitives.int32.Int32


val Int32.asFixedSizeHexString : String get() {
	val stringBuilder = StringBuilder()
	toByteArray().forEach { byte ->
		stringBuilder.append(byte.asFixedSizeHexString)
	}
	return stringBuilder.toString()
}
package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline val Int32.asInt64 : Int64 get() = toLong()
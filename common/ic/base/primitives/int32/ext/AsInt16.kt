package ic.base.primitives.int32.ext


import ic.base.primitives.int16.Int16
import ic.base.primitives.int32.Int32


inline val Int32.asInt16 : Int16 get() = toShort()
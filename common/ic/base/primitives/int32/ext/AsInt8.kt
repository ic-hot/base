package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int8.Int8


inline val Int32.asInt8 : Int8 get() = toByte()
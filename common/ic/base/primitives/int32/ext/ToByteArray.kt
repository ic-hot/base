package ic.base.primitives.int32.ext


import ic.base.arrays.bytes.ByteArray
import ic.base.primitives.int32.Int32


fun Int32.toByteArray() = ByteArray(

	(this ushr 24).asByte,
	(this ushr 16).asByte,
	(this ushr 8 ).asByte,
	(this ushr 0 ).asByte

)
package ic.base.primitives.int32.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32


inline val Int32.asFloat64 : Float64 get() = toDouble()
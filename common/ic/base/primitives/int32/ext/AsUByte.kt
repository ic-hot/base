package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32


inline val Int32.asUByte : UByte get() = toUByte()
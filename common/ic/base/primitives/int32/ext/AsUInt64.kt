package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.uint64.UInt64


inline val Int32.asUInt64 : UInt64 get() = toULong()
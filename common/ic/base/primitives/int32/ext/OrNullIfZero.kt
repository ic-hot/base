package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32


inline val Int32.orNullIfZero get() = if (this == 0) null else this
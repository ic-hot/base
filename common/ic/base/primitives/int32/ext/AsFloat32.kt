package ic.base.primitives.int32.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


inline val Int32.asFloat32 : Float32 get() = toFloat()
package ic.base.primitives.int32.ext


import ic.base.primitives.float64.ext.toString
import ic.base.primitives.int32.Int32
import ic.util.locale.Locale


fun Int32.toString (

	pattern : String,

	locale : Locale = Locale.American,

	groupingSeparator : Char = ' '

) : String {

	return this.asFloat64.toString(
		pattern = pattern,
		locale = locale,
		groupingSeparator = groupingSeparator
	)

}
package ic.base.primitives.int32


import ic.base.primitives.bytes.ext.asInt32


@Suppress("NOTHING_TO_INLINE")
inline fun Int32 (

	byte0 : Byte,
	byte1 : Byte,
	byte2 : Byte,
	byte3 : Byte

) : Int32 = (

	(byte0.asInt32 and 0xff shl 24) or
	(byte1.asInt32 and 0xff shl 16) or
	(byte2.asInt32 and 0xff shl 8 ) or
	(byte3.asInt32 and 0xff shl 0 )

)
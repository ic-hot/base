@file:Suppress("DEPRECATION")


package ic.base.primitives.int32


import ic.struct.order.OrderRelation


@Deprecated("Use normal Int32 comparison")
inline val Int32.asOrderRelation : OrderRelation get() {

	return when {

		this < 0 -> OrderRelation.Less

		this == 0 -> OrderRelation.Equals

		else -> OrderRelation.Greater

	}

}
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.int8


import ic.base.primitives.int32.Int32


typealias Int8 = Byte


inline fun Int8 (int32: Int32) : Int8 = int32.toByte()
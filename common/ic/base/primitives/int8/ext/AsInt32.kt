package ic.base.primitives.int8.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int8.Int8


inline val Int8.asInt32 : Int32 get() = toInt()
package ic.base.primitives.int8


import ic.base.primitives.float64.Float64


inline val Int8.asFloat64 : Float64 get() = toDouble()
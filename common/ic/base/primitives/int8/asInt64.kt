package ic.base.primitives.int8


import ic.base.primitives.int64.Int64


inline val Int8.asInt64 : Int64 get() = toLong()
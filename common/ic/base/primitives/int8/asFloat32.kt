package ic.base.primitives.int8


import ic.base.primitives.float32.Float32


inline val Int8.asFloat32 : Float32 get() = toFloat()
package ic.base.primitives.int8


import ic.base.primitives.int16.Int16


inline val Int8.asInt16 : Int16 get() = toShort()
package ic.base.primitives.int64


import ic.base.arrays.ext.length
import ic.base.assert.assert
import ic.base.primitives.int8.asInt64


@Suppress("NOTHING_TO_INLINE")
inline fun Int64 (

	byte0 : Byte,
	byte1 : Byte,
	byte2 : Byte,
	byte3 : Byte,
	byte4 : Byte,
	byte5 : Byte,
	byte6 : Byte,
	byte7 : Byte

) : Int64 = (

	(byte0.asInt64 and 0xffL shl 56) or
	(byte1.asInt64 and 0xffL shl 48) or
	(byte2.asInt64 and 0xffL shl 40) or
	(byte3.asInt64 and 0xffL shl 32) or
	(byte4.asInt64 and 0xffL shl 24) or
	(byte5.asInt64 and 0xffL shl 16) or
	(byte6.asInt64 and 0xffL shl 8 ) or
	(byte7.asInt64 and 0xffL shl 0 )

)


@Suppress("NOTHING_TO_INLINE")
inline fun Int64 (bytes: ByteArray) : Int64 {
	assert { bytes.length == 8 }
	return Int64(
		bytes[0],
		bytes[1],
		bytes[2],
		bytes[3],
		bytes[4],
		bytes[5],
		bytes[6],
		bytes[7]
	)
}
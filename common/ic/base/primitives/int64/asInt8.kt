package ic.base.primitives.int64


import ic.base.primitives.int8.Int8


inline val Int64.asInt8 : Int8 get() = toByte()
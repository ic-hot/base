@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.int64


import ic.base.primitives.int32.Int32


typealias Int64 = Long


inline fun Int64 (int32: Int32) : Int64 = int32.toLong()
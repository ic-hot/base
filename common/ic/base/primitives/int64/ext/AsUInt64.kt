package ic.base.primitives.int64.ext


import ic.base.primitives.int64.Int64
import ic.base.primitives.uint64.UInt64


inline val Int64.asUInt64 : UInt64 get() = toULong()
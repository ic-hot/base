package ic.base.primitives.int64.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


@Suppress("NOTHING_TO_INLINE")
inline infix fun Int64.equals (int32: Int32) = this == int32.asInt64
package ic.base.primitives.int64.ext


import ic.base.primitives.int64.Int64


inline val Int64.asByte get() = toByte()
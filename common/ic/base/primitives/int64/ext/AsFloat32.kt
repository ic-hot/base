package ic.base.primitives.int64.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64


inline val Int64.asFloat32 : Float32 get() = toFloat()
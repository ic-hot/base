package ic.base.primitives.int64.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64


inline val Int64.asFloat64 : Float64 get() = toDouble()
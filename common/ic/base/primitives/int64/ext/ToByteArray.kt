package ic.base.primitives.int64.ext


import ic.base.arrays.bytes.ByteArray
import ic.base.primitives.int64.Int64


@Suppress("NOTHING_TO_INLINE")
inline fun Int64.toByteArray() = ByteArray(

	(this ushr 56).asByte,
	(this ushr 48).asByte,
	(this ushr 40).asByte,
	(this ushr 32).asByte,
	(this ushr 24).asByte,
	(this ushr 16).asByte,
	(this ushr 8 ).asByte,
	(this ushr 0 ).asByte

)

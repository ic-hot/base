package ic.base.primitives.int64.ext


import ic.base.primitives.int64.Int64
import ic.util.text.numbers.hex.int64ToFixedSizeHexString


inline val Int64.asFixedSizeHexString get() = int64ToFixedSizeHexString(this)
package ic.base.primitives.int64.ext


import ic.base.primitives.float64.ext.toString
import ic.base.primitives.int64.Int64
import ic.util.locale.Locale


fun Int64.toString (

	pattern : String,

	locale : Locale = Locale.American,

	groupingSeparator : Char = ' '

) : String {

	return this.asFloat64.toString(
		pattern = pattern,
		locale = locale,
		groupingSeparator = groupingSeparator
	)

}
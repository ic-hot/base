package ic.base.primitives.int64.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline val Int64.asInt32 : Int32 get() = toInt()
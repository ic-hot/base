package ic.base.primitives.int64


import ic.base.primitives.int16.Int16


inline val Int64.asInt16 : Int16 get() = toShort()
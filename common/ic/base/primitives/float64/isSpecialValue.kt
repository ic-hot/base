package ic.base.primitives.float64

import ic.base.primitives.float64.ext.isNaN


inline val Float64.isSpecialValue : Boolean get() {

	if (isNaN) return true

	if (this == Float64.POSITIVE_INFINITY) return true
	if (this == Float64.NEGATIVE_INFINITY) return true

	else return false

}
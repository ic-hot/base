@file:Suppress("NOTHING_TO_INLINE")


package ic.base.primitives.float64


inline operator fun Float64?.plus (other: Float64) : Float64 {

	if (this == null) {
		return other
	} else {
		return this + other
	}

}


inline operator fun Float64?.plus (other: Float64?) : Float64? {

	return when {

		this != null && other != null -> this + other

		this != null && other == null -> this

		this == null && other != null -> other

		else -> null

	}

}
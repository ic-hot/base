package ic.base.primitives.float64


import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int64.ext.asFloat64


inline val Float64.isInteger : Boolean get() {

	return asInt64.asFloat64 == this

}
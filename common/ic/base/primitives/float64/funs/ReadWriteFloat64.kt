package ic.base.primitives.float64.funs


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.rawBitsAsInt64
import ic.base.primitives.int64.ext.rawBitsAsFloat64
import ic.base.throwables.End
import ic.stream.input.ByteInput
import ic.stream.input.ext.readInt64OrThrowEnd
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeInt64


@Suppress("NOTHING_TO_INLINE")
inline fun ByteOutput.writeFloat64 (float64: Float64) {
	writeInt64(float64.rawBitsAsInt64)
}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteInput.readFloat64OrThrowEnd() : Float64 {
	return readInt64OrThrowEnd().rawBitsAsFloat64
}

@Suppress("NOTHING_TO_INLINE")
inline fun ByteInput.readFloat64() : Float64 {
	try {
		return readFloat64OrThrowEnd()
	} catch (_: End) {
		throw RuntimeException()
	}
}
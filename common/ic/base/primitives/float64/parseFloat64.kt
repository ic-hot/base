@file:Suppress("NOTHING_TO_INLINE")


package ic.base.primitives.float64


inline fun parseFloat64 (string: String) : Float64 = string.toDouble()
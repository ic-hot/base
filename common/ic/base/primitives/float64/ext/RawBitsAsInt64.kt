package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.Int64


inline val Float64.rawBitsAsInt64 : Int64 get() {

	return toRawBits()

}
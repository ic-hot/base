package ic.base.primitives.float64.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64


inline val Float64.asFloat32 : Float32 get() = toFloat()
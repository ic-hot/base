package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.ext.toByteArray


@Suppress("NOTHING_TO_INLINE")
inline fun Float64.toByteArray() = toRawBits().toByteArray()
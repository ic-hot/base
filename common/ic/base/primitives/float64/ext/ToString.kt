package ic.base.primitives.float64.ext


import ic.base.platform.basePlatform
import ic.base.primitives.float64.Float64
import ic.util.locale.Locale


fun Float64.toString (

	pattern : String,

	locale : Locale = Locale.American,

	groupingSeparator : Char = ' '

) : String {

	return basePlatform.localeEngine.formatFloat64(
		float64 = this,
		pattern = pattern,
		locale = locale,
		groupingSeparator = groupingSeparator
	)

}
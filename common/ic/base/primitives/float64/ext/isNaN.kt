package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64


inline val Float64.isNaN get() = isNaN()

inline val Float64.isNotNaN get() = !isNaN
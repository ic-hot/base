package ic.base.primitives.float64.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32


inline val Float64.asInt32 : Int32 get() = toInt()
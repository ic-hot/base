package ic.base.primitives.float64


import ic.base.arrays.ext.length
import ic.base.assert.assert
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.rawBitsAsFloat64


@Suppress("NOTHING_TO_INLINE")
inline fun Float64 (
	byte0 : Byte,
	byte1 : Byte,
	byte2 : Byte,
	byte3 : Byte,
	byte4 : Byte,
	byte5 : Byte,
	byte6 : Byte,
	byte7 : Byte
) : Float64 {
	return Int64(byte0, byte1, byte2, byte3, byte4, byte5, byte6, byte7).rawBitsAsFloat64
}


@Suppress("NOTHING_TO_INLINE")
inline fun Float64 (bytes: ByteArray) : Float64 {
	assert { bytes.length == 8 }
	return Float64(
		bytes[0],
		bytes[1],
		bytes[2],
		bytes[3],
		bytes[4],
		bytes[5],
		bytes[6],
		bytes[7]
	)
}
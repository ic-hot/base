package ic.base.primitives.ubyte.ext


import ic.base.primitives.int32.Int32


inline val UByte.asInt32 : Int32 get() = toInt()
package ic.base.primitives.ubyte.ext


inline val UByte.asByte : Byte get() = toByte()
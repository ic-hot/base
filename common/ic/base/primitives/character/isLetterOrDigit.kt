package ic.base.primitives.character


inline val Character.isLetterOrDigit get() = isLetterOrDigit()
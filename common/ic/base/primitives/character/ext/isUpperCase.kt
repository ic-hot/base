package ic.base.primitives.character.ext


import ic.base.primitives.character.Character


inline val Character.isUpperCase : Boolean get() = isUpperCase()
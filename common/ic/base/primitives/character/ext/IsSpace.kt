package ic.base.primitives.character.ext


import ic.base.primitives.character.Character


inline val Character.isSpace : Boolean get() {
	return when (this) {
		' ' 	-> true
		'\t' 	-> true
		else -> false
	}
}


inline val Character.isNotSpace : Boolean get() = !isSpace
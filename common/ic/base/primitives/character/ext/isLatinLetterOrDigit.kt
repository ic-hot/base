package ic.base.primitives.character.ext


import ic.base.primitives.character.Character
import ic.base.primitives.character.isLatinLetter


inline val Character.isLatinLetterOrDigit : Boolean get() = isLatinLetter || isDigit

inline val Character.isNotLatinLetterOrDigit : Boolean get() = !isLatinLetterOrDigit
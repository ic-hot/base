package ic.base.primitives.character.ext


import ic.base.primitives.character.Character


inline val Character.isSpaceOrNewline : Boolean get() {
	return when (this) {
		' ' 	-> true
		'\t' 	-> true
		'\n' 	-> true
		'\r' 	-> true
		else -> false
	}
}


inline val Character.isNotSpaceOrNewline : Boolean get() = !isSpaceOrNewline
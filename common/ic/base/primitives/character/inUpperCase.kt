package ic.base.primitives.character


inline val Character.inUpperCase : Character get() = uppercaseChar()
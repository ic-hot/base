package ic.base.primitives.character


inline val Character.isLatinLetter : Boolean get() = this in 'A'..'Z' || this in 'a'..'z'
package ic.base.primitives.character


inline val Character.inLowerCase : Character get() = lowercaseChar()
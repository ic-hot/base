package ic.base.primitives.character


inline val Character.asString : String get() = toString()
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.bytes


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asByte


inline fun Byte (int32: Int32) = int32.asByte
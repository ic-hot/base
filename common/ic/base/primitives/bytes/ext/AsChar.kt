package ic.base.primitives.bytes.ext


inline val Byte.asChar : Char get() {

    return Char(code = this.asInt32)

}
package ic.base.primitives.bytes.ext


import ic.base.primitives.int32.Int32


inline val Byte.asInt32 : Int32 get() {

    var int32 = this.toInt()

    if (int32 < 0) {
        int32 += 256
    }

    return int32

}
package ic.base.primitives.bytes.ext


inline val Byte.asUByte : UByte get() = toUByte()
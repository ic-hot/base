package ic.base.primitives.bytes.ext


val Byte.asFixedSizeHexString : String get() {

	val int32 = this.asInt32

	var string = int32.toString(radix = 16)

	while (string.length < 2) {
		string = "0" + string
	}

	return string

}
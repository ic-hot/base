package ic.base.primitives.bytes.funs


import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun parseByteFromHexStringOrThrowUnableToParse (string: String) : Byte {

	try {
		return string.toInt(16).asByte
	} catch (_: NumberFormatException) {
		throw UnableToParseException()
	}

}
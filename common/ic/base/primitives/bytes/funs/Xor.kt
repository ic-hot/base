package ic.base.primitives.bytes.funs


import kotlin.experimental.xor

import ic.base.primitives.int32.Int32


inline fun xor (
	count: Int32,
	getByte : (index: Int32) -> Byte
) : Byte {
	var result : Byte = 0
	repeat(count) { index ->
		result = result xor getByte(index)
	}
	return result
}
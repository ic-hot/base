package ic.base.primitives.int16


import ic.base.primitives.int64.Int64


inline val Int16.asInt64 : Int64 get() = toLong()
@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.base.primitives.int16


import ic.base.primitives.int32.Int32


typealias Int16 = Short


inline fun Int16 (int32: Int32) : Int16 = int32.toShort()
package ic.base.primitives.int16


import ic.base.primitives.int8.Int8


inline val Int16.asInt8 : Int8 get() = toByte()
package ic.base.primitives.int16


import ic.base.primitives.float32.Float32


inline val Int16.asFloat32 : Float32 get() = toFloat()
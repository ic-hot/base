package ic.base.primitives.int16.ext


import ic.base.primitives.bytes.ext.asFixedSizeHexString
import ic.base.primitives.int16.Int16


val Int16.asFixedSizeHexString : String get() {
	val stringBuilder = StringBuilder()
	toByteArray().forEach { byte ->
		stringBuilder.append(byte.asFixedSizeHexString)
	}
	return stringBuilder.toString()
}
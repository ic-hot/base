package ic.base.primitives.int16.ext


import ic.base.primitives.int16.Int16
import ic.base.primitives.int32.Int32


inline val Int16.asInt32 : Int32 get() = toInt()
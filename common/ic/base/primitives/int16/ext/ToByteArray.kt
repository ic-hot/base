package ic.base.primitives.int16.ext


import ic.base.arrays.bytes.ByteArray
import ic.base.primitives.int16.Int16
import ic.base.primitives.int32.ext.asByte


fun Int16.toByteArray() : ByteArray {

	val int32 = this.asInt32

	return ByteArray(

		(int32 ushr 8).asByte,
		(int32 ushr 0).asByte

	)

}
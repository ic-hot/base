package ic.base.primitives.int16


import ic.base.primitives.float64.Float64


inline val Int16.asFloat64 : Float64 get() = toDouble()
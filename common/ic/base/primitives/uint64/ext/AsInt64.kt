package ic.base.primitives.uint64.ext


import ic.base.primitives.int64.Int64
import ic.base.primitives.uint64.UInt64


inline val UInt64.asInt64 : Int64 get() = toLong()
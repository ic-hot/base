package ic.base.platform


import ic.app.impl.AppEngine
import ic.network.impl.NetworkEngine
import ic.parallel.impl.ParallelEngine
import ic.storage.impl.StorageEngine
import ic.struct.value.ephemeral.impl.EphemeralEngine
import ic.system.impl.SystemEngine
import ic.util.analytics.impl.AnalyticsEngine
import ic.util.crypto.impl.CryptoEngine
import ic.util.locale.impl.LocaleEngine
import ic.util.log.Logger
import ic.util.log.SystemOutLogger
import ic.util.text.charset.impl.CharsetsEngine


internal abstract class BasePlatform {


	abstract val platformType : PlatformType


	abstract val analyticsEngine    : AnalyticsEngine
	abstract val appEngine			: AppEngine
	abstract val systemEngine 		: SystemEngine
	abstract val parallelEngine 	: ParallelEngine
	abstract val ephemeralEngine 	: EphemeralEngine
	abstract val charsetsEngine 	: CharsetsEngine
	abstract val localeEngine		: LocaleEngine
	abstract val networkEngine 		: NetworkEngine
	abstract val storageEngine		: StorageEngine
	abstract val cryptoEngine       : CryptoEngine


	open val globalLogger : Logger get() = SystemOutLogger


}
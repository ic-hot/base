package ic.base.platform


sealed class PlatformType {


	sealed class Jvm : PlatformType() {

		object Pure : Jvm()

		object Ic : Jvm()

		object Android : Jvm()

	}


	object Ios : PlatformType()


}
package ic.base.platform


import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


val platformType by Cached { basePlatform.platformType }
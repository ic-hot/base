package ic.base.tuples


data class Tuple4 <Item1, Item2, Item3, Item4> (

	val item1 : Item1,
	val item2 : Item2,
	val item3 : Item3,
	val item4 : Item4

)
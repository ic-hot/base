package ic.base.tuples.funs


@Suppress("NOTHING_TO_INLINE")
inline fun <A> Tuple (a: A) = a

@Suppress("NOTHING_TO_INLINE")
inline fun <A, B> Tuple (a: A, b: B) = Pair(a, b)



@Suppress("NOTHING_TO_INLINE")
inline fun <A, B, C> Tuple (a: A, b: B, c: C) = Triple(a, b, c)
package ic.base.tuples.ext


import ic.struct.list.List
import ic.struct.list.ext.plus


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Pair<List<Item>, List<Item>>.copyJoin() = first + second
package ic.base.tuples.ext


import ic.struct.list.Lazy
import ic.struct.list.List


val <Item, A: Item, B: Item> Pair<A, B>.asList get() = List.Lazy(
	getLength = { 2 },
	getItem = { index ->
		when (index) {
			0L -> first
			1L -> second
			else -> throw Error()
		}
	}
)
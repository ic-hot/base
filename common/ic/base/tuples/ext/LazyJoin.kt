package ic.base.tuples.ext


import ic.struct.list.List
import ic.struct.list.join.JoinList


@Suppress("NOTHING_TO_INLINE")
inline fun <Item> Pair<List<Item>, List<Item>>.lazyJoin() = JoinList(first, second)
package ic.base.compare


import ic.base.primitives.int32.Int32


typealias Comparison = Int32


const val ALessThanB    : Comparison = -1
const val AGreaterThanB : Comparison =  1
const val AEqualsB      : Comparison =  0
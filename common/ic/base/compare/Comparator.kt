package ic.base.compare


typealias Comparator <Item> = (Item, Item) -> Comparison
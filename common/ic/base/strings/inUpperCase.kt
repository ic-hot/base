package ic.base.strings


inline val String.inUpperCase : String get() = uppercase()
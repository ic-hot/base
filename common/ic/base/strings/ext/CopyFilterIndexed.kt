package ic.base.strings.ext


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.text.TextBuffer


inline fun String.copyFilterIndexed (

	predicate: (index: Int32, Character) -> Boolean

) : String {

	val textBuffer = TextBuffer()

	forEachIndexed { index, character ->

		if (predicate(index, character)) {
			textBuffer.putChar(character)
		}

	}

	return textBuffer.toString()

}
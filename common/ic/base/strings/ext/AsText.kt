package ic.base.strings.ext


import ic.text.Text
import ic.text.ext.FromString


inline val String.asText get() = Text.FromString(this)
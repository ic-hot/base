package ic.base.strings.ext.find


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.util.code.clike.quote.doubleQuote


@Suppress("NOTHING_TO_INLINE")
inline fun String.findLastIndexOf (character: Character) : Int32 {
	val index = lastIndexOf(character)
	if (index < 0) {
		throw RuntimeException(
			"string: ${ doubleQuote(this) }, character: '$character'"
		)
	}
	return index
}
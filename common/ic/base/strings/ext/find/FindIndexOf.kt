package ic.base.strings.ext.find


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.base.throwables.NotExistsException


@Throws(NotExistsException::class)
fun String.findIndexOfOrThrowNotExists (substring: String, startIndex: Int32 = 0) : Int32 {
	val index = indexOf(string = substring, startIndex = startIndex)
	if (index < 0) {
		throw NotExistsException
	}
	return index
}


@Throws(NotExistsException::class)
fun String.findIndexOfOrThrowNotExists (character: Character) : Int32 {
	val index = indexOf(character)
	if (index < 0) {
		throw NotExistsException
	}
	return index
}
package ic.base.strings.ext


import ic.base.primitives.character.inLowerCase


inline val String.decapitalized : String get() {

	if (isEmpty) return this

	return this[0].inLowerCase + getSubstring(1, length)

}
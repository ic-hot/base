package ic.base.strings.ext


import ic.base.primitives.character.Character

import kotlin.text.endsWith


@Suppress("NOTHING_TO_INLINE")
inline fun String?.endsWith (suffix: String, ignoreCase: Boolean = false) : Boolean {

	if (this == null) return false

	return endsWith(suffix = suffix, ignoreCase = ignoreCase)

}


@Suppress("NOTHING_TO_INLINE")
inline infix fun String?.endsWith (suffix: String) = endsWith(suffix, ignoreCase = false)


@Suppress("NOTHING_TO_INLINE")
inline fun String?.endsWith (suffix: Character, ignoreCase: Boolean = false) : Boolean {

	if (this == null) return false

	return endsWith(char = suffix, ignoreCase = ignoreCase)

}


@Suppress("NOTHING_TO_INLINE")
inline infix fun String?.endsWith (suffix: Character) = endsWith(suffix, ignoreCase = false)
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext


import ic.base.primitives.int32.Int32


inline fun String.limit (maxLength: Int32) : String {

	if (length <= maxLength) return this

	return substring(0, maxLength)

}
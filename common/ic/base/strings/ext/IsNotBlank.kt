package ic.base.strings.ext


inline val String?.isNotBlank get() = !isNullOrBlank
package ic.base.strings.ext


import ic.base.primitives.character.ext.isUpperCase


inline val String.isCapitalized : Boolean get() {
	if (isEmpty) return false
	return this[0].isUpperCase
}
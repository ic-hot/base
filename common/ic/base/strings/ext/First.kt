package ic.base.strings.ext


inline val String.first get() = first()
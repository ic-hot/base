package ic.base.strings.ext


import ic.base.primitives.int32.Int32


inline fun String?.lengthOr (ifNull : () -> Int32) : Int32 {
	if (this == null) {
		return ifNull()
	} else {
		return length
	}
}
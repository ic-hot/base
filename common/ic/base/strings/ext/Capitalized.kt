package ic.base.strings.ext


import ic.base.primitives.character.inUpperCase


inline val String.capitalized : String get() {

	if (isEmpty) return this

	return this[0].inUpperCase + getSubstring(1, length)

}
package ic.base.strings.ext


import ic.base.objects.ext.orSkip


inline val String?.orSkipIfBlank : String get() = orNullIfBlank.orSkip
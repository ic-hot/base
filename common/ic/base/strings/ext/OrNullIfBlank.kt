package ic.base.strings.ext


inline val String?.orNullIfBlank : String? get() {

	if (this == null) {
		return null
	} else {
		if (this.isBlank) {
			return null
		} else {
			return this
		}
	}

}
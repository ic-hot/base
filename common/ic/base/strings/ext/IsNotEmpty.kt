package ic.base.strings.ext


inline val String.isNotEmpty get() = !isEmpty
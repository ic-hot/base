package ic.base.strings.ext


@Suppress("NOTHING_TO_INLINE")
inline operator infix fun String?.contains (substring: String) : Boolean {

	if (this == null) return false

	return this.contains(substring, ignoreCase = false)

}
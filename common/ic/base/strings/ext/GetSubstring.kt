package ic.base.strings.ext


import ic.base.primitives.int32.Int32


@Deprecated("To remove")
fun String.getSubstring (startIndex: Int32, endIndex: Int32) : String {

	return substring(startIndex, endIndex)

}
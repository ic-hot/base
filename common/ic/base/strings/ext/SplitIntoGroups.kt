package ic.base.strings.ext


import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.text.TextBuffer


fun String.splitIntoGroups (

	groupsLengths : List<Int32>,

	toEndWithEmptyGroup : Boolean

) : List<String> {

	val groups = EditableList<String>()

	var groupIndex : Int32 = 0
	var groupBuffer : TextBuffer? = null

	var characterIndex : Int32 = 0

	var isAllGroupsFilled : Boolean = false
	var isAllCharactersProcessed : Boolean = false

	fun getGroupBufferLength() : Int32 {
		return groupBuffer?.length ?: 0
	}

	while (true) {

		if (characterIndex >= length) isAllCharactersProcessed = true

		if (groupIndex >= groupsLengths.length) isAllGroupsFilled = true

		if (isAllCharactersProcessed || isAllGroupsFilled) break

		val groupLength = groupsLengths[groupIndex]

		if (getGroupBufferLength() < groupLength) {

			if (groupBuffer == null) groupBuffer = TextBuffer()
			groupBuffer.putChar(
				this[characterIndex]
			)
			characterIndex++

		}

		if (getGroupBufferLength() == groupLength) {

			groups.add(
				groupBuffer?.toString() ?: ""
			)
			groupBuffer = null
			groupIndex++

		}

	}

	if (!isAllGroupsFilled) {

		if (groupBuffer == null) {
			if (toEndWithEmptyGroup) {
				groups.add("")
			}
		} else {
			groups.add(
				groupBuffer.toString()
			)
		}

	}

	return groups

}
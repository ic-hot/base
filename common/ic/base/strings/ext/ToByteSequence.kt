package ic.base.strings.ext


import ic.stream.sequence.ByteSequence
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.stringToByteSequence


fun String.toByteSequence (charset: Charset) : ByteSequence {

	return charset.stringToByteSequence(this)

}
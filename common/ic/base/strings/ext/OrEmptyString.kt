package ic.base.strings.ext


inline val String?.orEmptyString get() = this ?: ""
package ic.base.strings.ext


import ic.base.primitives.character.Character
import ic.base.primitives.character.inLowerCase


fun String?.startsWith (prefix: String, toIgnoreCase: Boolean = false) : Boolean {
	if (this == null) return false
	return startsWith(prefix = prefix, ignoreCase = toIgnoreCase)
}

fun String?.startsWith (prefix: Character, toIgnoreCase: Boolean = false) : Boolean {
	if (this == null) return false
	if (isEmpty) return false
	if (toIgnoreCase) {
		return this[0].inLowerCase == prefix.inLowerCase
	} else {
		return this[0] == prefix
	}
}


infix fun String?.startsWith (prefix: String) = startsWith(prefix = prefix, toIgnoreCase = false)

infix fun String?.startsWith (prefix: Character) = startsWith(prefix = prefix, toIgnoreCase = false)
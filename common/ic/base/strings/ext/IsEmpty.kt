package ic.base.strings.ext


inline val String.isEmpty get() = isEmpty()
package ic.base.strings.ext


inline val String.isBlank get() = isBlank()
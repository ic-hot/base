package ic.base.strings.ext.parse


import ic.base.primitives.bytes.funs.parseByteFromHexStringOrThrowUnableToParse
import ic.base.throwables.UnableToParseException


@Deprecated("Use parseByteFromHexStringOrThrowUnableToParse")
@Suppress("NOTHING_TO_INLINE")
@Throws(UnableToParseException::class)
inline fun String.parseHexByteOrThrowUnableToParse() : Byte {

	return parseByteFromHexStringOrThrowUnableToParse(this)

}
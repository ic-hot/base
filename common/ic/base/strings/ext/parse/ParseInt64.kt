@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext.parse


import ic.base.primitives.int64.Int64
import ic.base.throwables.UnableToParseException
import ic.util.text.numbers.parseInt64OrThrowUnableToParse


@Throws(UnableToParseException::class)
inline fun String.parseInt64OrThrowUnableToParse() : Int64 {
	return parseInt64OrThrowUnableToParse(this)
}


inline fun String.parseInt64() : Int64 {
	try {
		return parseInt64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("string: $this")
	}
}


inline fun String.parseInt64OrNull() : Int64? {
	try {
		return parseInt64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		return null
	}
}

inline fun String.parseInt64Or (isUnableToParse: () -> Int64) : Int64 {
	try {
		return parseInt64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		return isUnableToParse()
	}
}



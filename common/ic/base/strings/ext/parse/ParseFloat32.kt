package ic.base.strings.ext.parse


import ic.base.primitives.float32.Float32
import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun String.parseFloat32OrThrowUnableToParse() : Float32 {
	try {
		return toFloat()
	} catch (_: NumberFormatException) {
		throw UnableToParseException()
	}
}

fun String.parseFloat32() : Float32 {
	try {
		return parseFloat32OrThrowUnableToParse()
	} catch (_: UnableToParseException) {
		throw RuntimeException("string: $this")
	}
}

fun String.parseFloat32OrNull() : Float32? {
	try {
		return parseFloat32OrThrowUnableToParse()
	} catch (_: UnableToParseException) {
		return null
	}
}

inline fun String.parseFloat32Or (ifUnableToParse : () -> Float32) : Float32 {
	try {
		return parseFloat32OrThrowUnableToParse()
	} catch (_: UnableToParseException) {
		return ifUnableToParse()
	}
}
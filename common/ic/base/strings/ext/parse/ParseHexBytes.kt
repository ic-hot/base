package ic.base.strings.ext.parse


import ic.base.arrays.bytes.funs.parseByteSequenceFromHexStringOrThrow
import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray


@Deprecated("Use parseByteSequenceFromHexStringOrThrowUnableToParse")
@Suppress("NOTHING_TO_INLINE")
@Throws(UnableToParseException::class)
inline fun String.parseHexByteSequenceOrThrowUnableToParse() : ByteSequence {
	return parseByteSequenceFromHexStringOrThrow(this)
}


@Deprecated("Use parseByteArrayFromHexString")
@Suppress("NOTHING_TO_INLINE")
inline fun String.parseHexByteArray() : ByteArray {
	try {
		return parseByteSequenceFromHexStringOrThrow(this).toByteArray()
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime()
	}
}
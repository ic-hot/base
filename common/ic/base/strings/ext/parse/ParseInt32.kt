@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext.parse


import ic.base.primitives.int32.Int32
import ic.base.throwables.UnableToParseException
import ic.util.text.numbers.parseInt32OrThrowUnableToParse


@Throws(UnableToParseException::class)
inline fun String.parseInt32OrThrowUnableToParse() = parseInt32OrThrowUnableToParse(this)

inline fun String.parseInt32() : Int32 {
	try {
		return parseInt32OrThrowUnableToParse(this)
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("string: $this")
	}
}


inline fun String.parseInt32OrNull() : Int32? {
	try {
		return parseInt32OrThrowUnableToParse(this)
	} catch (t: UnableToParseException) {
		return null
	}
}


inline fun String.parseInt32 (
	or : Int32
) : Int32 {
	try {
		return parseInt32OrThrowUnableToParse(this)
	} catch (t: UnableToParseException) {
		return or
	}
}


inline fun String.parseInt32 (
	or : () -> Int32
) : Int32 {
	try {
		return parseInt32OrThrowUnableToParse(this)
	} catch (t: UnableToParseException) {
		return or()
	}
}

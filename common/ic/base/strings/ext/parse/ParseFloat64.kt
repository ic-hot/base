@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext.parse


import ic.base.primitives.float64.Float64
import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
inline fun String.parseFloat64OrThrowUnableToParse() : Float64 {
	try {
		return toDouble()
	} catch (t: NumberFormatException) {
		throw UnableToParseException()
	}
}

inline fun String.parseFloat64() : Float64 {
	try {
		return parseFloat64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("string: $this")
	}
}

inline fun String.parseFloat64OrNull() : Float64? {
	try {
		return parseFloat64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		return null
	}
}

inline fun String.parseFloat64Or (ifUnableToParse : () -> Float64) : Float64 {
	try {
		return parseFloat64OrThrowUnableToParse()
	} catch (t: UnableToParseException) {
		return ifUnableToParse()
	}
}
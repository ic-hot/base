package ic.base.strings.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skippable
import ic.base.primitives.character.Character
import ic.text.TextBuffer


inline fun String.filter (
	@Skippable predicate: (Character) -> Boolean
) : String {
	val textBuffer = TextBuffer()
	forEach { character ->
		skippable {
			if (predicate(character)) {
				textBuffer.putChar(character)
			}
		}
	}
	return textBuffer.toString()
}


@Deprecated("Use filter(Predicate)")
inline fun String.copyFilter (predicate: (Character) -> Boolean) : String = filter(predicate)
package ic.base.strings.ext


import kotlin.text.split

import ic.base.kcollections.ext.copy.copyToList
import ic.base.primitives.character.Character
import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE")
inline fun String.splitIntoArray (separator: Character) : Array<String> {
	return split(separator).toTypedArray()
}


@Suppress("NOTHING_TO_INLINE")
inline fun String.split (separator: Character) : List<String> {
	return split(separator).copyToList()
}


@Suppress("NOTHING_TO_INLINE")
inline fun String.split (separator: String) : List<String> {
	return split(separator).copyToList()
}
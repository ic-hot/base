package ic.base.strings.ext


inline val String?.isNullOrBlank : Boolean get() {

	if (this == null) return true

	else return isBlank

}
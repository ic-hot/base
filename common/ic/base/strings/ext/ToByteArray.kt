@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings.ext


import ic.util.text.charset.Charset
import ic.util.text.charset.ext.stringToByteArray


inline fun String.toByteArray (charset: Charset) : ByteArray {
	return charset.stringToByteArray(this)
}
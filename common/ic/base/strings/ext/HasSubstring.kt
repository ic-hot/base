package ic.base.strings.ext


import ic.base.loop.all
import ic.base.primitives.int32.Int32


fun String.hasSubstring (substring: String, at: Int32) = run {
	all(substring.length) { i ->
		this[at + i] == substring[i]
	}
}
package ic.base.strings.ext


import kotlin.jvm.JvmName

import ic.base.arrays.ext.forEach
import ic.base.primitives.character.Character
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach


fun String.replaceAll (from: String, to: String) : String {
	return replace(from, to)
}

fun String.replaceAll (from: Character, to: Character) : String {
	return replace(from, to)
}

@JvmName("replaceAllSubstring")
fun String.replaceAll (vararg mappings: Pair<String, String>) : String {
	var string = this
	mappings.forEach { (from, to) ->
		string = string.replace(from, to)
	}
	return string
}


fun String.replaceAll (mappings: Iterable<Pair<String, String>>) : String {
	var string = this
	mappings.forEach { (from, to) ->
		string = string.replace(from, to)
	}
	return string
}


fun String.replaceAll (map: FiniteMap<String, String>) : String {
	var string = this
	map.forEach { from, to ->
		string = string.replace(from, to)
	}
	return string
}


@JvmName("replaceAllCharacters")
fun String.replaceAll (vararg mappings: Pair<Character, Character>) : String {
	var string = this
	mappings.forEach { (from, to) ->
		string = string.replace(from, to)
	}
	return string
}
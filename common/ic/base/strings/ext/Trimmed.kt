package ic.base.strings.ext


inline val String.trimmed get() = trim()
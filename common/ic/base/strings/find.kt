@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings


import ic.base.primitives.int32.Int32
import ic.text.Text
import ic.text.pattern.SearchInTextResult
import ic.text.pattern.TextPattern
import ic.text.pattern.findOrNull


inline fun String.findOrNull (

	pattern : TextPattern,

	startWithIndex : Int32 = 0

) : SearchInTextResult? {

	return Text(this).findOrNull(
		startWithIndex = startWithIndex,
		pattern = pattern
	)

}
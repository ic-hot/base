package ic.base.strings


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32


inline fun String.none (predicate: (character: Character) -> Boolean) : Boolean = !atLeastOne(predicate)


inline fun String.noneIndexed (

	predicate : (index: Int32, character: Character) -> Boolean

) : Boolean = !atLeastOneIndexed(predicate)
@file:Suppress("NOTHING_TO_INLINE")


package ic.base.strings


import ic.base.primitives.character.inUpperCase
import ic.base.strings.ext.getSubstring
import ic.base.strings.ext.isEmpty


inline fun String.copyCapitalize() : String {

	if (isEmpty) return this

	return this[0].inUpperCase + getSubstring(1, length)

}
#ifndef ic_base_strings_funs_join_JoinStrings4_c_included
#define ic_base_strings_funs_join_JoinStrings4_c_included


#include "../../../primitives/int32/Int32.h"
#include "../../String.h"
#include "../../buffer/StringBuffer.h"
#include "../../buffer/funs/InitStringBuffer.c"
#include "../../buffer/funs/PutCharacterIntoStringBuffer.c"


String joinStrings4 (String a, String b, String c, String d, GarbageCollector* garbageCollectorRef) {

	StringBuffer stringBuffer;
	initStringBuffer(&stringBuffer, garbageCollectorRef);

	for (Int32 i = 0; a[i] != 0; i++) {
		putCharacterIntoStringBuffer(&stringBuffer, a[i], garbageCollectorRef);
	}

	for (Int32 i = 0; b[i] != 0; i++) {
    	putCharacterIntoStringBuffer(&stringBuffer, b[i], garbageCollectorRef);
    }

    for (Int32 i = 0; c[i] != 0; i++) {
        putCharacterIntoStringBuffer(&stringBuffer, c[i], garbageCollectorRef);
    }

    for (Int32 i = 0; d[i] != 0; i++) {
        putCharacterIntoStringBuffer(&stringBuffer, d[i], garbageCollectorRef);
    }

    return stringBuffer.string;

}


#endif
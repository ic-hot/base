#ifndef ic_base_strings_funs_join_JoinStrings2_c_included
#define ic_base_strings_funs_join_JoinStrings2_c_included


#include "../../../primitives/int32/Int32.h"
#include "../../String.h"
#include "../../buffer/StringBuffer.h"
#include "../../buffer/funs/InitStringBuffer.c"
#include "../../buffer/funs/PutCharacterIntoStringBuffer.c"


String joinStrings2 (String a, String b, GarbageCollector* garbageCollectorRef) {

	StringBuffer stringBuffer;
	initStringBuffer(&stringBuffer, garbageCollectorRef);

	for (Int32 i = 0; a[i] != 0; i++) {
		putCharacterIntoStringBuffer(&stringBuffer, a[i], garbageCollectorRef);
	}

	for (Int32 i = 0; b[i] != 0; i++) {
    	putCharacterIntoStringBuffer(&stringBuffer, b[i], garbageCollectorRef);
    }

    return stringBuffer.string;

}


#endif
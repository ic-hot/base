package ic.base.strings.funs


import ic.base.arrays.ext.forEach
import ic.funs.effect.andDoInBetween
import ic.base.primitives.character.Character
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.text.TextBuffer


fun concat (strings: Array<String>, separator : String = "") : String {
	val textBuffer = TextBuffer()
	strings.forEach(
		{ string: String ->
			textBuffer.write(string)
		}
		.andDoInBetween {
			textBuffer.write(separator)
		}
	)
	return textBuffer.toString()
}


@Suppress("NOTHING_TO_INLINE")
inline fun concat (strings: Array<String>, separator : Character) : String {
	return concat(
		strings = strings,
		separator = separator.toString()
	)
}

fun concat (strings: List<String>, separator : String = "") : String {
	val textBuffer = TextBuffer()
	strings.forEach(
		{ string: String ->
			textBuffer.write(string)
		}
		.andDoInBetween {
			textBuffer.write(separator)
		}
	)
	return textBuffer.toString()
}


@Suppress("NOTHING_TO_INLINE")
inline fun concat (strings: List<String>, separator : Character) : String {
	return concat(
		strings = strings,
		separator = separator.toString()
	)
}
#ifndef ic_base_strings_funs_AreStringsEqual_c_included
#define ic_base_strings_funs_AreStringsEqual_c_included


#include "../../primitives/bool/Boolean.h"
#include "../../primitives/int32/Int32.h"
#include "../String.h"


Boolean areStringsEqual (String a, String b) {

	for (Int32 i = 0; a[i] != 0 || b[i] != 0; i++) {

		if (a[i] != b[i]) return FALSE;

	} return TRUE;

}


#endif
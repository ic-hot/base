#ifndef ic_base_strings_buffer_StringBuffer_h_included
#define ic_base_strings_buffer_StringBuffer_h_included


#include "../../primitives/int32/Int32.h"
#include "../String.h"


struct StringBuffer {
	String string;
	Int32 capacity;
	Int32 length;
};

typedef struct StringBuffer StringBuffer;


#endif
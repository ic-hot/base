#ifndef ic_base_strings_buffer_funs_PutCharacterIntoStringBuffer_c_included
#define ic_base_strings_buffer_funs_PutCharacterIntoStringBuffer_c_included


#include "../../../primitives/character/Character.h"
#include "../../../primitives/int32/Int32.h"
#include "../../../primitives/void/Void.h"
#include "../StringBuffer.h"
#include "../../../../memory/gc/GarbageCollector.h"
#include "../../../../memory/gc/funs/AllocateMemory.c"


Void putCharacterIntoStringBuffer (StringBuffer* stringBufferRef, Character character, GarbageCollector* garbageCollectorRef) {

	Int32 length = (*stringBufferRef).length;

	if (length > (*stringBufferRef).capacity - 2) {
		String oldString = (*stringBufferRef).string;
		Int32 newCapacity = (*stringBufferRef).capacity * 2;
		String newString = allocateMemory(sizeof(Character) * newCapacity, garbageCollectorRef);
		for (Int32 i = 0; i < length; i++) {
			newString[i] = oldString[i];
		}
		newString[length] = 0;
		(*stringBufferRef).string = newString;
		(*stringBufferRef).capacity = newCapacity;
	}

	String string = (*stringBufferRef).string;

	string[length++] = character;
	string[length] = 0;

	(*stringBufferRef).length = length;

}


#endif
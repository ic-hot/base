#ifndef ic_base_strings_buffer_funs_InitStringBuffer_c_included
#define ic_base_strings_buffer_funs_InitStringBuffer_c_included


#include "../../../primitives/character/Character.h"
#include "../../../primitives/int32/Int32.h"
#include "../../../primitives/void/Void.h"
#include "../StringBuffer.h"
#include "../../../../memory/gc/GarbageCollector.h"
#include "../../../../memory/gc/funs/AllocateMemory.c"


const Int32 initialStringBufferCapacity = 64;


Void initStringBuffer (StringBuffer* stringBufferRef, GarbageCollector* garbageCollectorRef) {

	(*stringBufferRef).string = allocateMemory(sizeof(Character) * initialStringBufferCapacity, garbageCollectorRef);
	(*stringBufferRef).string[0] = 0;

	(*stringBufferRef).capacity = initialStringBufferCapacity;
	(*stringBufferRef).length = 0;

}


#endif
package ic.base.strings


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32


inline fun String.atLeastOne (predicate: (character: Character) -> Boolean) : Boolean {

	var index : Int32 = 0

	while (index < length) {

		if (predicate(this[index])) {
			return true
		}

		index++

	}

	return false

}


inline fun String.atLeastOneIndexed (predicate: (index: Int, character: Character) -> Boolean) : Boolean {

	var index : Int32 = 0

	while (index < length) {

		if (predicate(index, this[index])) {
			return true
		}

		index++

	}

	return false

}
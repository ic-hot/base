package ic.base.strings


import ic.base.primitives.character.Character


inline fun String.copyRemoveAll (predicate : (Character) -> Boolean) : String {

	val buffer = StringBuilder()

	forEach { char ->
		if (!predicate(char)) {
			buffer.append(char)
		}
	}

	return buffer.toString()

}
package ic.base.strings


inline val String.inLowerCase : String get() = lowercase()
package ic.base.strings


import ic.text.TextBuffer
import ic.text.TextOutput


inline fun String (write: TextOutput.() -> Unit) : String {
	val textBuffer = TextBuffer()
	textBuffer.run(write)
	return textBuffer.toString()
}
package ic.base.strings


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32


inline fun String.all (predicate: (character: Character) -> Boolean) : Boolean = none { character -> !predicate(character) }


inline fun String.allIndexed (

	predicate : (index: Int32, character: Character) -> Boolean

) : Boolean {

	return noneIndexed { index, character -> !predicate(index, character) }

}
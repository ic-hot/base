package ic.base.kfunctions.ext


import kotlin.reflect.KProperty


inline operator fun <Value> (() -> Value).getValue (
	thisRef : Any?, property : KProperty<*>
) : Value {
	return invoke()
}
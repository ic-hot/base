@file:Suppress("ObjectLiteralToLambda")


package ic.base.kfunctions


val <Item> ((a: Item, b: Item) -> Boolean).asComparator : kotlin.Comparator<Item> get() {

	val thisFunction = this

	return object : kotlin.Comparator<Item> {

		override fun compare (a: Item, b: Item) : Int {

			return if (thisFunction(a, b)) -1 else 1

		}

	}

}
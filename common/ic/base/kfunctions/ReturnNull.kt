@file:Suppress("FunctionName")


package ic.base.kfunctions


object ReturnNull : () -> Nothing? {

	override fun invoke () = null

}



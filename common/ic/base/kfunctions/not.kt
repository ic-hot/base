package ic.base.kfunctions


operator fun (() -> Boolean).not() : () -> Boolean {

	return { !this() }

}
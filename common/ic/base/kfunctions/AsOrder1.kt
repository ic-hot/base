@file:Suppress("ObjectLiteralToLambda", "DEPRECATION")


package ic.base.kfunctions

import ic.struct.order.OrderRelation
import ic.struct.order.order1.Order1


@Deprecated("Use normal Int32 comparison")
inline val <Item> ((b: Item) -> Boolean).asOrder1 : Order1<Item> get() {

	val thisFunction = this

	return object : Order1<Item> {
		override fun compare (b: Item) : OrderRelation {
			return if (thisFunction(b)) {
				OrderRelation.Less
			} else {
				OrderRelation.Greater
			}
		}
	}

}
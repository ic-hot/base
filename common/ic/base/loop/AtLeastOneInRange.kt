package ic.base.loop


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline fun atLeastOneInRange (
	from : Int32, to : Int32,
	condition : (Int32) -> Boolean
) : Boolean {
	nonBreakableForRange(from = from, to = to) {
		if (condition(it)) return true
	}
	return false
}


inline fun atLeastOneInRange (
	from : Int64, to : Int64,
	condition : (Int64) -> Boolean
) : Boolean {
	nonBreakableForRange(from = from, to = to) {
		if (condition(it)) return true
	}
	return false
}
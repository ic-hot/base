package ic.base.loop


import ic.base.primitives.int32.Int32


inline fun repeatInReverse (
	times : Int32,
	action : (index: Int32) -> Unit
) {
	repeat(times) { index ->
		action(times - 1 - index)
	}
}
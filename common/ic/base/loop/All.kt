package ic.base.loop


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline fun all (count: Int32, condition: (index: Int32) -> Boolean) : Boolean {
	repeat(count) { index ->
		if (!condition(index)) return false
	}
	return true
}


inline fun all (count: Int64, condition: (index: Int64) -> Boolean) : Boolean {
	repeat(count) { index ->
		if (!condition(index)) return false
	}
	return true
}
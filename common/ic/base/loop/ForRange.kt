package ic.base.loop


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


inline fun nonBreakableForRange (from: Int32, to: Int32, action: (Int32) -> Unit) {
	var index : Int32 = from
	while (index < to) {
		action(index)
		index++
	}
}


inline fun nonBreakableForRange (from: Int64, to: Int64, action: (Int64) -> Unit) {
	var index : Int64 = from
	while (index < to) {
		action(index)
		index++
	}
}


inline fun nonBreakableForRange (from: Int64, to: Int32, action: (Int64) -> Unit) {
	nonBreakableForRange(from = from, to = to.asInt64, action = action)
}
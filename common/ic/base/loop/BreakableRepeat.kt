package ic.base.loop


import ic.base.escape.breakable.Breakable
import ic.base.escape.breakable.breakable
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


@Deprecated("To remove break")
inline fun breakableRepeat (
	times : Int32,
	onBreak : () -> Unit = {},
	@Breakable action : (index: Int32) -> Unit
) {
	var index : Int32 = 0
	@Suppress("DEPRECATION")
	breakable(onBreak = onBreak) {
		while (index < times) {
			action(index)
			index++
		}
	}
}


@Deprecated("To remove break")
inline fun breakableRepeat (
	times : Int64,
	onBreak : () -> Unit = {},
	@Breakable action : (index: Int64) -> Unit
) {
	var index : Int64 = 0
	@Suppress("DEPRECATION")
	breakable(onBreak = onBreak) {
		while (index < times) {
			action(index)
			index++
		}
	}
}
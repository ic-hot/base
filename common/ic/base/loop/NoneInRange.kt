package ic.base.loop


import ic.base.primitives.int64.Int64


inline fun noneInRange (
	from : Int64, to : Int64,
	condition : (Int64) -> Boolean
) : Boolean {
	return !atLeastOneInRange(from, to, condition)
}
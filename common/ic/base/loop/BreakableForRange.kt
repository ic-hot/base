@file:Suppress("DEPRECATION")


package ic.base.loop


import ic.base.escape.breakable.Breakable
import ic.base.escape.breakable.breakable
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


@Deprecated("To remove")
inline fun breakableForRange (from: Int32, to: Int32, @Breakable action: (Int32) -> Unit) {
	var index : Int32 = from
	breakable {
		while (index < to) {
			action(index)
			index++
		}
	}
}


@Deprecated("To remove")
inline fun breakableForRange (from: Int64, to: Int64, @Breakable action: (Int64) -> Unit) {
	var index : Int64 = from
	breakable {
		while (index < to) {
			action(index)
			index++
		}
	}
}
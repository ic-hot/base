package ic.base.loop


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline fun repeat (
	times : Int64,
	action : (index: Int64) -> Unit
) {
	var index : Int64 = 0
	while (index < times) {
		action(index)
		index++
	}
}


@Deprecated("Use repeat")
inline fun nonBreakableRepeat (
	times : Int32,
	action : (index: Int32) -> Unit
) {
	repeat(times = times, action = action)
}


@Deprecated("Use repeat")
inline fun nonBreakableRepeat (
	times : Int64,
	action : (index: Int64) -> Unit
) {
	repeat(times = times, action = action)
}
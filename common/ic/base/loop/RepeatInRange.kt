package ic.base.loop


import ic.base.escape.breakable.Breakable
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


@Deprecated("Use forRange")
inline fun repeatInRange (
	from : Int32, to : Int32,
	@Breakable action : (index: Int32) -> Unit
) {
	@Suppress("DEPRECATION")
	breakableRepeat(times = to - from) { index ->
		action(from + index)
	}
}


@Deprecated("Use forRange")
inline fun repeatInRange (
	from : Int64, to : Int64,
	@Breakable action : (index: Int64) -> Unit
) {
	@Suppress("DEPRECATION")
	breakableRepeat(times = to - from) { index ->
		action(from + index)
	}
}
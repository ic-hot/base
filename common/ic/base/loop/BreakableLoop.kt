@file:Suppress("DEPRECATION")

package ic.base.loop


import ic.base.escape.breakable.Breakable
import ic.base.escape.breakable.breakable
import ic.ifaces.action.Action


@Deprecated("To remove")
inline fun breakableLoop (

	onBreak	: () -> Unit = {},

	@Breakable action : () -> Unit

) {

	breakable (onBreak = onBreak) {
		while (true) {
			action()
		}
	}

}


@Deprecated("To remove")
fun breakableLoop (@Breakable action : Action) {

	breakableLoop {

		action.run()

	}

}
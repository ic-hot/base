package ic.base.loop


import ic.ifaces.action.Action


inline fun loop (

	action	: () -> Unit

) : Nothing {

	while (true) {
		action()
	}

}


fun loop (action : Action) : Nothing {

	loop {

		action.run()

	}

}
package ic.base.loop


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


inline fun count (count: Int32, condition: (index: Int32) -> Boolean) : Int32 {
	var result : Int32 = 0
	repeat (count) { index ->
		if (condition(index)) result++
	}
	return result
}

inline fun count (count: Int64, condition: (index: Int64) -> Boolean) : Int64 {
	var result : Int64 = 0
	repeat (count) { index ->
		if (condition(index)) result++
	}
	return result
}
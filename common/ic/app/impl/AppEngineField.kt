package ic.app.impl


import ic.base.platform.basePlatform


internal inline val appEngine : AppEngine get() = basePlatform.appEngine
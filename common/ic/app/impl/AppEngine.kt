package ic.app.impl


import ic.app.tier.Tier
import ic.base.annotations.CalledOnce
import ic.storage.fs.Directory
import ic.storage.prefs.Prefs
import ic.app.res.impl.Resources


abstract class AppEngine {


	abstract fun getTierString() : String?

	abstract fun getDefaultTier() : Tier


	abstract val resources : Resources


	@CalledOnce
	abstract fun initPrivateFolder() : Directory

	@CalledOnce
	abstract fun initCommonDataDirectory() : Directory

	@CalledOnce
	abstract fun initCommonPublicFolder() : Directory


	abstract fun createPrefs (prefsName: String) : Prefs


}
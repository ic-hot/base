package ic.app.tier


import ic.app.impl.appEngine


private var cached : String? = null

var tierString : String?
	get() {
		return cached ?: appEngine.getTierString().also { cached = it }
	}
	set(value) {
		cached = value
	}
;
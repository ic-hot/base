package ic.app.tier


sealed class Tier {

	object Debug : Tier()

	object Development : Tier()

	object Stage : Tier()

	object Production : Tier()

}
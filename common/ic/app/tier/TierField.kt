package ic.app.tier


import ic.app.impl.appEngine
import ic.app.runtimeAppOrNull


private var cached : Tier? = null


var tier : Tier
	get() = (
		cached ?: run {
			runtimeAppOrNull?.thisTier ?:
			when (tierString) {
				"debug" -> Tier.Debug
				"dev"   -> Tier.Development
				"stage" -> Tier.Development
				"prod"  -> Tier.Production
				else -> appEngine.getDefaultTier()
			}
		}.also { cached = it }
	)
	set(value) {
		cached = value
	}
;


val tierMayChange get() = run {
	cached = null
}
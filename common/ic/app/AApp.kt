package ic.app


import ic.base.annotations.CallOnce
import ic.base.annotations.CalledOnce
import ic.app.tier.Tier
import ic.ifaces.stoppable.Stoppable


abstract class AApp : Stoppable {


	protected open val tier : Tier? get() = null

	internal val thisTier get() = tier


	@CalledOnce
	protected abstract fun implRun (args: String)

	@CallOnce
	fun run() {
		implRun(appArgs)
	}


	protected open fun implStopNonBlocking() {}

	override fun stopNonBlockingOrThrowNotNeeded() {
		implStopNonBlocking()
	}


	protected open fun implWaitFor() {}

	override fun waitFor() {
		implWaitFor()
	}


}
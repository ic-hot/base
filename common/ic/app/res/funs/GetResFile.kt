package ic.app.res.funs


import ic.base.throwables.NotExistsException
import ic.app.res.impl.resources
import ic.stream.sequence.ByteSequence


@Throws(NotExistsException::class)
fun getResFileOrThrow (name: String) : ByteSequence {
	return resources.getFileOrThrow(name)
}


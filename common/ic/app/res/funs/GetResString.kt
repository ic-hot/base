package ic.app.res.funs


import ic.base.strings.ext.replaceAll
import ic.app.res.impl.resources


fun getResStringOrNull (path: String) = resources.getStringOrNull(path)

@Suppress("NOTHING_TO_INLINE")
inline fun getResString (path: String) = getResStringOrNull(path)!!

@Suppress("NOTHING_TO_INLINE")
inline fun getResString (path: String, vararg mappings: Pair<String, String>) : String {
	return getResString(path).replaceAll(mappings = mappings)
}

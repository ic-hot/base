package ic.app.res.impl


import ic.app.impl.appEngine


internal inline val resources get() = appEngine.resources
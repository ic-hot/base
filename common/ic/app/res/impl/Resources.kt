package ic.app.res.impl


import ic.base.throwables.NotExistsException
import ic.stream.sequence.ByteSequence


interface Resources {

	@Throws(NotExistsException::class)
	fun getFileOrThrow (path: String) : ByteSequence

	fun getStringOrNull (path: String) : String?

}
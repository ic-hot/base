package ic.app


import ic.app.tier.tierMayChange
import ic.base.annotations.CallOnce


var runtimeAppOrNull : App? = null
	private set
;


inline val runtimeApp get() = runtimeAppOrNull!!


@CallOnce
fun setRuntimeApp (app: App) {
	runtimeAppOrNull = app
	tierMayChange
}
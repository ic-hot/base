package ic.design.status


sealed interface Status<out Success, out Failure> {

	object InProgress : Status<Nothing, Nothing>

	sealed interface Finished<Success, Failure> : Status<Success, Failure>

	data class Failed<Failure> (
		val failure : Failure
	) : Finished<Nothing, Failure>

	data class Success<Success> (
		val data : Success
	) : Finished<Success, Nothing>

}
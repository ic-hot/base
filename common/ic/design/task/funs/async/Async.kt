package ic.design.task.funs.async


import ic.design.task.Job
import ic.design.task.scope.ext.doInUiThread
import ic.ifaces.cancelable.Cancelable
import ic.parallel.annotations.BackgroundThread
import ic.parallel.annotations.UiThread
import ic.parallel.funs.doInBackgroundAsTask


@UiThread
inline fun
	<Result>
	async (
	initialValue : Result,
		@BackgroundThread crossinline doInBackground : () -> Result,
		@UiThread crossinline callback : (Result) -> Unit
	)
	: Cancelable
{
	callback(initialValue)
	return doInBackgroundAsTask {
		val result = doInBackground()
		doInUiThread {
			callback(result)
		}
	}
}


@UiThread
inline fun
	<Result>
	async (
		initialValue : Result,
		@BackgroundThread crossinline doInBackground : () -> Result
	)
	: Job<Result>
	= { callback : (Result) -> Unit ->
		callback(initialValue)
		doInBackgroundAsTask {
			val result = doInBackground()
			doInUiThread {
				callback(result)
			}
		}
	}
;
package ic.design.task.funs.async


import ic.design.status.Status
import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable
import ic.parallel.annotations.BackgroundThread
import ic.parallel.annotations.UiThread


@UiThread
inline fun
	<Data, Failure>
	async (
		@BackgroundThread crossinline doInBackground : () -> Status<Data, Failure>,
		@UiThread crossinline callback : (Status<Data, Failure>) -> Unit
	)
	: Cancelable
{
	return async(
		initialValue = Status.InProgress,
		doInBackground = doInBackground,
		callback = callback
	)
}


@UiThread
inline fun
	<Data, Failure>
	async (
		@BackgroundThread crossinline doInBackground : () -> Status<Data, Failure>
	)
	: Job<Status<Data, Failure>>
	= async(
		initialValue = Status.InProgress,
		doInBackground = doInBackground,
	)
;
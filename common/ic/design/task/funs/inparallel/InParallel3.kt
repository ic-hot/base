package ic.design.task.funs.inparallel


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable


inline fun
	<Result1, Result2, Result3>
	InParallel (
		crossinline job1 : Job<Result1>,
		crossinline job2 : Job<Result2>,
		crossinline job3 : Job<Result3>
	)
	: Job<Triple<Result1, Result2, Result3>>
	= { onResult ->
		var isResult1Ready : Boolean = false
		var isResult2Ready : Boolean = false
		var isResult3Ready : Boolean = false
		var result1 : Result1? = null
		var result2 : Result2? = null
		var result3 : Result3? = null
		val task1 = job1 {
			result1 = it
			isResult1Ready = true
			if (isResult2Ready && isResult3Ready) {
				@Suppress("UNCHECKED_CAST")
				onResult(
					Triple(
						result1 as Result1,
						result2 as Result2,
						result3 as Result3
					)
				)
			}
		}
		val task2 = job2 {
			result2 = it
			isResult2Ready = true
			if (isResult1Ready && isResult3Ready) {
				@Suppress("UNCHECKED_CAST")
				onResult(
					Triple(
						result1 as Result1,
						result2 as Result2,
						result3 as Result3
					)
				)
			}
		}
		val task3 = job3 {
			result3 = it
			isResult3Ready = true
			if (isResult1Ready && isResult2Ready) {
				@Suppress("UNCHECKED_CAST")
				onResult(
					Triple(
						result1 as Result1,
						result2 as Result2,
						result3 as Result3
					)
				)
			}
		}
		Cancelable {
			task1?.cancel()
			task2?.cancel()
			task3?.cancel()
		}
	}
;
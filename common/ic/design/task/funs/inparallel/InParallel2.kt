package ic.design.task.funs.inparallel


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable


inline fun
	<Result1, Result2>
	InParallel (
		crossinline job1 : Job<Result1>,
		crossinline job2 : Job<Result2>
	)
	: Job<Pair<Result1, Result2>>
	= { onResult ->
		var isResult1Ready : Boolean = false
		var isResult2Ready : Boolean = false
		var result1 : Result1? = null
		var result2 : Result2? = null
		val task1 = job1 {
			result1 = it
			isResult1Ready = true
			if (isResult2Ready) {
				@Suppress("UNCHECKED_CAST")
				onResult(
					Pair(
						result1 as Result1,
						result2 as Result2
					)
				)
			}
		}
		val task2 = job2 {
			result2 = it
			isResult2Ready = true
			if (isResult1Ready) {
				@Suppress("UNCHECKED_CAST")
				onResult(
					Pair(
						result1 as Result1,
						result2 as Result2
					)
				)
			}
		}
		Cancelable {
			task1?.cancel()
			task2?.cancel()
		}
	}
;
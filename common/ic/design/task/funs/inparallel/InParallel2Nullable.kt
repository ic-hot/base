package ic.design.task.funs.inparallel


import ic.ifaces.cancelable.Cancelable


inline fun
	<Result1, Result2>
	InParallel
	(
		subscribe1 : ((Result1) -> Unit) -> Cancelable?,
		subscribe2 : ((Result2) -> Unit) -> Cancelable?,
		crossinline onResult : (Result1, Result2) -> Unit
	)
	: Cancelable?
{
	var isResult1Ready : Boolean = false
	var isResult2Ready : Boolean = false
	var result1 : Result1? = null
	var result2 : Result2? = null
	val task1 = subscribe1 {
		result1 = it
		isResult1Ready = true
		if (isResult2Ready) {
			@Suppress("UNCHECKED_CAST")
			onResult(
				result1 as Result1,
				result2 as Result2
			)
		}
	}
	val task2 = subscribe2 {
		result2 = it
		isResult2Ready = true
		if (isResult1Ready) {
			@Suppress("UNCHECKED_CAST")
			onResult(
				result1 as Result1,
				result2 as Result2
			)
		}
	}
	if (task1 == null && task2 == null) {
		return null
	} else {
		return Cancelable {
			task1?.cancel()
			task2?.cancel()
		}
	}
}
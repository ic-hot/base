package ic.design.task.funs.pipe


import ic.design.task.Job
import ic.funs.effect.ChangesOnly
import ic.ifaces.cancelable.Cancelable


inline fun
	<First, Second, Third>
	Pipe (
		crossinline loadFirst : ((First) -> Unit) -> Cancelable?,
		crossinline loadSecond : (First) -> ((Second) -> Unit) -> Cancelable?,
		crossinline loadThird : (First, Second) -> ((Third) -> Unit) -> Cancelable?
	)
	: Job<Third>
	= { watcher ->
		@Suppress("NAME_SHADOWING")
		val watcher = ChangesOnly(watcher)
		var secondTask : Cancelable? = null
		var thirdTask : Cancelable? = null
		var firstTask : Cancelable? = loadFirst { first ->
			thirdTask?.cancel()
			thirdTask = null
			secondTask?.cancel()
			secondTask = (loadSecond(first)) { second ->
				thirdTask?.cancel()
				thirdTask = (loadThird(first, second)) { third ->
					watcher(third)
				}
			}
		}
		if (firstTask == null && secondTask == null && thirdTask == null) {
			null
		} else {
			Cancelable {
				firstTask?.cancel()
				firstTask = null
				secondTask?.cancel()
				secondTask = null
				thirdTask?.cancel()
				thirdTask = null
			}
		}
	}
;
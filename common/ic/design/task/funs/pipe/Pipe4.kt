package ic.design.task.funs.pipe


import ic.design.task.Job
import ic.funs.effect.ChangesOnly
import ic.ifaces.cancelable.Cancelable


inline fun
	<First, Second, Third, Fourth>
	Pipe (
		crossinline loadFirst : ((First) -> Unit) -> Cancelable?,
		crossinline loadSecond : (First) -> ((Second) -> Unit) -> Cancelable?,
		crossinline loadThird : (First, Second) -> ((Third) -> Unit) -> Cancelable?,
		crossinline loadFourth : (First, Second, Third) -> ((Fourth) -> Unit) -> Cancelable?
	)
	: Job<Fourth>
	= { watcher ->
		@Suppress("NAME_SHADOWING")
		val watcher = ChangesOnly(watcher)
		var secondTask : Cancelable? = null
		var thirdTask : Cancelable? = null
		var fourthTask : Cancelable? = null
		var firstTask : Cancelable? = loadFirst { first ->
			thirdTask?.cancel()
			thirdTask = null
			fourthTask?.cancel()
			fourthTask = null
			secondTask?.cancel()
			secondTask = (loadSecond(first)) { second ->
				fourthTask?.cancel()
				fourthTask = null
				thirdTask?.cancel()
				thirdTask = (loadThird(first, second)) { third ->
					fourthTask?.cancel()
					fourthTask = (loadFourth(first, second, third)) { fourth ->
						watcher(fourth)
					}
				}
			}
		}
		if (firstTask == null && secondTask == null && thirdTask == null) {
			null
		} else {
			Cancelable {
				firstTask?.cancel()
				firstTask = null
				secondTask?.cancel()
				secondTask = null
				thirdTask?.cancel()
				thirdTask = null
				fourthTask?.cancel()
				fourthTask = null
			}
		}
	}
;
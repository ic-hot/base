package ic.design.task.funs.pipe


import ic.design.task.Job
import ic.funs.effect.ChangesOnly
import ic.ifaces.cancelable.Cancelable


inline fun
	<First, Second>
	Pipe (
		crossinline loadFirst : Job<First>,
		crossinline loadSecond : (First) -> Job<Second>
	)
	: Job<Second>
	= { watcher ->
		@Suppress("NAME_SHADOWING")
		val watcher = ChangesOnly(watcher)
		var secondTask : Cancelable? = null
		var firstTask : Cancelable? = loadFirst { first ->
			secondTask?.cancel()
			secondTask = (loadSecond(first)) { second ->
				watcher(second)
			}
		}
		if (firstTask == null && secondTask == null) {
			null
		} else {
			Cancelable {
				firstTask?.cancel()
				firstTask = null
				secondTask?.cancel()
				secondTask = null
			}
		}
	}
;
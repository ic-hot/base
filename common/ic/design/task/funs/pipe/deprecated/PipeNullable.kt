package ic.design.task.funs.pipe.deprecated


import ic.funs.effect.ChangesOnly
import ic.ifaces.cancelable.Cancelable


inline fun
	<First, Second>
	pipe (
		loadFirst : ((First) -> Unit) -> Cancelable?,
		crossinline loadSecond : (First, (Second) -> Unit) -> Cancelable?,
		crossinline onResult : (Second) -> Unit
	)
	: Cancelable?
{
	val thisWatcher = ChangesOnly(onResult)
	var secondTask : Cancelable? = null
	var firstTask : Cancelable? = loadFirst { first ->
		secondTask?.cancel()
		secondTask = loadSecond(first) { second ->
			thisWatcher(second)
		}
	}
	if (firstTask == null && secondTask == null) {
		return null
	} else {
		return Cancelable {
			firstTask?.cancel()
			firstTask = null
			secondTask?.cancel()
			secondTask = null
		}
	}
}
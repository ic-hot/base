package ic.design.task.funs


import ic.design.task.Task
import ic.design.task.callback.failure.JobCallbackWithFailure
import ic.design.task.failure.JobWithFailure
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


inline fun <Output1, Output2, Output3, Output4, Failure1, Failure2, Failure3, Failure4> startInParallel (

	startTask1 : (JobCallbackWithFailure<Output1, Failure1>) -> Task?,
	startTask2 : (JobCallbackWithFailure<Output2, Failure2>) -> Task?,
	startTask3 : (JobCallbackWithFailure<Output3, Failure3>) -> Task?,
	startTask4 : (JobCallbackWithFailure<Output4, Failure4>) -> Task?,

	crossinline onFinish : () -> Unit,
	crossinline onFailure1 : (Failure1) -> Unit,
	crossinline onFailure2 : (Failure2) -> Unit,
	crossinline onFailure3 : (Failure3) -> Unit,
	crossinline onFailure4 : (Failure4) -> Unit,
	crossinline onSuccess : (Output1, Output2, Output3, Output4) -> Unit

) : Task? {

	val mutex = Mutex()

	var task1 : Task? = null
	var task2 : Task? = null
	var task3 : Task? = null
	var task4 : Task? = null

	var isTask1Success : Boolean = false
	var isTask2Success : Boolean = false
	var isTask3Success : Boolean = false
	var isTask4Success : Boolean = false

	var output1 : Output1? = null
	var output2 : Output2? = null
	var output3 : Output3? = null
	var output4 : Output4? = null

	mutex.synchronized {

		task1 = startTask1(
			JobCallbackWithFailure(
				onFailure = { failure ->
					mutex.synchronized {
						task1 = null
						task2?.cancel()
						task2 = null
						task3?.cancel()
						task3 = null
						task4?.cancel()
						task4 = null
					}
					onFinish()
					onFailure1(failure)
				},
				onSuccess = handlingSuccess1@{ output ->
					mutex.synchronized {
						task1 = null
						isTask1Success = true
						output1 = output
						if (!isTask2Success || !isTask3Success || !isTask4Success) return@handlingSuccess1
					}
					onFinish()
					@Suppress("UNCHECKED_CAST")
					onSuccess(output1 as Output1, output2 as Output2, output3 as Output3, output4 as Output4)
				}
			)
		)

		task2 = startTask2(
			JobCallbackWithFailure(
				onFailure = { failure ->
					mutex.synchronized {
						task2 = null
						task1?.cancel()
						task1 = null
						task3?.cancel()
						task3 = null
						task4?.cancel()
						task4 = null
					}
					onFinish()
					onFailure2(failure)
				},
				onSuccess = handlingSuccess2@{ output ->
					mutex.synchronized {
						task2 = null
						isTask2Success = true
						output2 = output
						if (!isTask1Success || !isTask3Success || !isTask4Success) return@handlingSuccess2
					}
					onFinish()
					@Suppress("UNCHECKED_CAST")
					onSuccess(output1 as Output1, output2 as Output2, output3 as Output3, output4 as Output4)
				}
			)
		)

		task3 = startTask3(
			JobCallbackWithFailure(
				onFailure = { failure ->
					mutex.synchronized {
						task3 = null
						task1?.cancel()
						task1 = null
						task2?.cancel()
						task2 = null
						task4?.cancel()
						task4 = null
					}
					onFinish()
					onFailure3(failure)
				},
				onSuccess = handlingSuccess3@{ output ->
					mutex.synchronized {
						task3 = null
						isTask3Success = true
						output3 = output
						if (!isTask1Success || !isTask2Success || !isTask4Success) return@handlingSuccess3
					}
					onFinish()
					@Suppress("UNCHECKED_CAST")
					onSuccess(output1 as Output1, output2 as Output2, output3 as Output3, output4 as Output4)
				}
			)
		)

		task4 = startTask4(
			JobCallbackWithFailure(
				onFailure = { failure ->
					mutex.synchronized {
						task4 = null
						task1?.cancel()
						task1 = null
						task2?.cancel()
						task2 = null
						task3?.cancel()
						task3 = null
					}
					onFinish()
					onFailure4(failure)
				},
				onSuccess = handlingSuccess4@{ output ->
					mutex.synchronized {
						task4 = null
						isTask4Success = true
						output4 = output
						if (!isTask1Success || !isTask2Success || !isTask3Success) return@handlingSuccess4
					}
					onFinish()
					@Suppress("UNCHECKED_CAST")
					onSuccess(output1 as Output1, output2 as Output2, output3 as Output3, output4 as Output4)
				}
			)
		)

	}

	if (task1 == null && task2 == null && task3 == null && task4 == null) {
		return null
	} else {
		return Task(
			cancel = {
				mutex.synchronized {
					task1?.cancel()
					task1 = null
					task2?.cancel()
					task2 = null
					task3?.cancel()
					task3 = null
					task4?.cancel()
					task4 = null
				}
			}
		)
	}

}

inline fun <Output1, Output2, Output3, Output4, Failure> startInParallel (
	startTask1 : (JobCallbackWithFailure<Output1, Failure>) -> Task?,
	startTask2 : (JobCallbackWithFailure<Output2, Failure>) -> Task?,
	startTask3 : (JobCallbackWithFailure<Output3, Failure>) -> Task?,
	startTask4 : (JobCallbackWithFailure<Output4, Failure>) -> Task?,
	crossinline onFinish : () -> Unit,
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output1, Output2, Output3, Output4) -> Unit
) = startInParallel(
	startTask1 = startTask1,
	startTask2 = startTask2,
	startTask3 = startTask3,
	startTask4 = startTask4,
	onFinish = onFinish,
	onFailure1 = onFailure,
	onFailure2 = onFailure,
	onFailure3 = onFailure,
	onFailure4 = onFailure,
	onSuccess = onSuccess
)

inline fun <Input1, Input2, Input3, Input4, Output1, Output2, Output3, Output4, Failure1, Failure2, Failure3, Failure4> startInParallel (

	job1 : JobWithFailure<Input1, Output1, Failure1>,
	job2 : JobWithFailure<Input2, Output2, Failure2>,
	job3 : JobWithFailure<Input3, Output3, Failure3>,
	job4 : JobWithFailure<Input4, Output4, Failure4>,

	input1 : Input1,
	input2 : Input2,
	input3 : Input3,
	input4 : Input4,

	crossinline onFinish : () -> Unit,
	crossinline onFailure1 : (Failure1) -> Unit,
	crossinline onFailure2 : (Failure2) -> Unit,
	crossinline onFailure3 : (Failure3) -> Unit,
	crossinline onFailure4 : (Failure4) -> Unit,
	crossinline onSuccess : (Output1, Output2, Output3, Output4) -> Unit

) = startInParallel (
	startTask1 = { callback ->
		job1.start(input1, callback)
	},
	startTask2 = { callback ->
		job2.start(input2, callback)
	},
	startTask3 = { callback ->
		job3.start(input3, callback)
	},
	startTask4 = { callback ->
		job4.start(input4, callback)
	},
	onFinish = onFinish,
	onFailure1 = onFailure1,
	onFailure2 = onFailure2,
	onFailure3 = onFailure3,
	onFailure4 = onFailure4,
	onSuccess = onSuccess
)
package ic.design.task.funs.silence


import ic.ifaces.cancelable.Cancelable


inline fun <Status> SilenceInsteadOfCancel (
	crossinline f : ((Status) -> Unit) -> Cancelable,
	crossinline callback : (Status) -> Unit
) : Cancelable {
	var isSilenced : Boolean = false
	f { status ->
		if (!isSilenced) {
			callback(status)
		}
	}
	return Cancelable {
		isSilenced = true
	}
}


inline fun
	<Status>
	(((Status) -> Unit) -> Cancelable).silenceInsteadOfCancel (
		crossinline callback : (Status) -> Unit
	)
	= SilenceInsteadOfCancel(this, callback = callback)
;
package ic.design.task.funs.silence


import ic.ifaces.cancelable.Cancelable


inline fun <Status> SilenceInsteadOfCancel (
	crossinline f : ((Status) -> Unit) -> Cancelable?,
	crossinline callback : (Status) -> Unit
) : Cancelable? {
	var isSilenced : Boolean = false
	val task = f { status ->
		if (!isSilenced) {
			callback(status)
		}
	}
	if (task == null) {
		return null
	} else {
		return Cancelable {
			isSilenced = true
		}
	}
}


inline fun
	<Status>
	(((Status) -> Unit) -> Cancelable?).silenceInsteadOfCancel (
		crossinline callback : (Status) -> Unit
	)
	= SilenceInsteadOfCancel(this, callback = callback)
;
@file:Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")


package ic.design.task.operations


import ic.design.task.failure.JobWithFailure
import ic.design.task.Task
import ic.design.task.failure.progress.ext.start
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


inline fun
	<
		FirstJobInput, FirstJobOutput, FirstJobFailure,
		SecondJobInput, SecondJobOutput, SecondJobFailure
	>
	startJobsInPipe
(
	input1 : FirstJobInput,
	job1 : JobWithFailure<FirstJobInput, FirstJobOutput, FirstJobFailure>,
	crossinline onFailure1 : (FirstJobFailure) -> Unit,
	crossinline getInput2 : (FirstJobOutput) -> SecondJobInput,
	job2 : JobWithFailure<SecondJobInput, SecondJobOutput, SecondJobFailure>,
	crossinline onFailure2 : (SecondJobFailure) -> Unit,
	crossinline onFinish : () -> Unit = {},
	crossinline onSuccess : (SecondJobOutput) -> Unit
) : Task? {
	val lock = Mutex()
	var currentTask : Task? = null
	var isCanceled : Boolean = false
	lock.synchronized {
		val task1 = job1.start(
			input = input1,
			onFinish = {},
			onFailure = handlingFirstJobFailure@{ failure ->
				lock.synchronized {
					currentTask = null
					if (isCanceled) return@handlingFirstJobFailure
				}
				onFinish()
				onFailure1(failure)
			}
		) handlingFirstJobSuccess@{ firstJobOutput ->
			lock.synchronized {
				if (isCanceled) return@handlingFirstJobSuccess
				currentTask = job2.start(
					input = getInput2(firstJobOutput),
					onFinish = {},
					onFailure = handlingSecondJobFailure@{ failure ->
						lock.synchronized {
							currentTask = null
							if (isCanceled) return@handlingSecondJobFailure
						}
						onFinish()
						onFailure2(failure)
					}
				) handlingSecondJobSuccess@{ secondJobOutput ->
					lock.synchronized {
						currentTask = null
						if (isCanceled) return@handlingSecondJobSuccess
					}
					onFinish()
					onSuccess(secondJobOutput)
				}
			}
		}
		if (task1 != null) currentTask = task1
		if (currentTask == null) {
			return null
		} else {
			return Task(
				cancel = {
					lock.synchronized {
						isCanceled = true
						currentTask?.cancel()
						currentTask = null
					}
				}
			)
		}
	}
}


inline fun <FirstJobInput, SecondJobOutput, SecondJobFailure> startJobsInPipe (
	firstJobInput : FirstJobInput,
	firstJob : JobWithFailure<FirstJobInput, Unit, Unit>,
	crossinline onFirstJobFailure : () -> Unit,
	secondJob : JobWithFailure<Unit, SecondJobOutput, SecondJobFailure>,
	crossinline onSecondJobFailure : (SecondJobFailure) -> Unit,
	crossinline onFinish : () -> Unit = {},
	crossinline onSuccess : (SecondJobOutput) -> Unit
) = startJobsInPipe(
	input1 = firstJobInput,
	job1 = firstJob,
	onFailure1 = { onFirstJobFailure() },
	getInput2 = { Unit },
	job2 = secondJob,
	onFailure2 = onSecondJobFailure,
	onFinish = onFinish,
	onSuccess = onSuccess
)
package ic.design.task.scope


import ic.ifaces.cancelable.Cancelable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.struct.collection.ext.copy.copy
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


abstract class BaseTaskScope : TaskScope {


	private val mutex = Mutex()

	private val tasks = EditableSet<Cancelable>()


	final override fun notifyTaskStarted (task: Cancelable) = mutex.synchronized {
		tasks.addIfNotExists(task)
	}


	final override fun notifyTaskFinished (task: Cancelable) = mutex.synchronized {
		tasks.removeIfExists(task)
	}

	final override fun cancelTasks() = mutex.synchronized {
		tasks.copy().forEach { it.cancel() }
		tasks.empty()
	}



}
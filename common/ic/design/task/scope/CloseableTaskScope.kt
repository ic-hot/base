package ic.design.task.scope


import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.lifecycle.hasopenstate.HasIsOpenState


open class CloseableTaskScope : BaseTaskScope(), HasIsOpenState, Closeable {

	final override var isOpen : Boolean = true
		private set
	;

	override fun close() {
		isOpen = false
		cancelTasks()
	}

}
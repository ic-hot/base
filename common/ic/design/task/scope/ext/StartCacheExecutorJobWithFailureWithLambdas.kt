package ic.design.task.scope.ext


import ic.design.task.Task
import ic.design.task.cache.executor.failure.CacheExecutorJobWithFailure
import ic.design.task.cache.executor.failure.ext.start
import ic.design.task.scope.TaskScope
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.util.cache.Cache
import ic.util.load.LoadMode


inline fun <Input, Output, Failure> TaskScope.startCacheExecutorJobWithFailure (
	job : CacheExecutorJobWithFailure<Input, Output, Failure>,
	input : Input,
	cache : Cache<Input, Output>,
	loadMode : LoadMode = LoadMode.CacheFirst.Default,
	executor : Executor = defaultTaskExecutor,
	callbackExecutor : Executor = defaultCallbackExecutor,
	crossinline onFinish : () -> Unit = {},
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit
) {
	var task : Task? = null
	task = job.start(
		input = input,
		cache = cache, loadMode = loadMode,
		executor = executor, callbackExecutor = callbackExecutor,
		onFinish = {
			task?.let { notifyTaskFinished(it) }
			onFinish()
		},
		onFailure = onFailure,
		onSuccess = onSuccess
	)
	task?.let { notifyTaskStarted(it) }
}
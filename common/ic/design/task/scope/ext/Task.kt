package ic.design.task.scope.ext


import ic.design.task.scope.TaskScope
import ic.ifaces.cancelable.Cancelable


inline fun TaskScope.inScope (createTask: () -> Cancelable?) {

	if (!isOpen) return

	val task = createTask()

	if (task != null) {
		notifyTaskStarted(task)
	}

}
package ic.design.task.scope.ext


import ic.design.task.scope.TaskScope
import ic.ifaces.cancelable.Cancelable
import ic.parallel.funs.sleepTill


inline fun TaskScope.doInUiThread (crossinline action : () -> Unit) {
	if (!isOpen) return
	var isNotified : Boolean = false
	lateinit var task : Cancelable
	task = ic.parallel.funs.doInUiThread {
		sleepTill { isNotified }
		notifyTaskFinished(task)
		if (isOpen) {
			action()
		}
	}
	notifyTaskStarted(task)
	isNotified = true
}


@Deprecated("Use doInUiThread")
inline fun TaskScope.doCallback (crossinline action : () -> Unit) = doInUiThread(action)

@Deprecated("Use doInUiThread")
inline fun TaskScope.callback (crossinline action : () -> Unit) = doInUiThread(action)
package ic.design.task.scope.ext


import ic.design.task.Task
import ic.design.task.scope.TaskScope
import ic.parallel.funs.doInBackgroundAsTask


inline fun TaskScope.doInBackground (

	crossinline action : TaskScope.() -> Unit

) {

	val thisTaskScope = this

	lateinit var task : Task

	task = doInBackgroundAsTask {
		try {
			action()
		} finally {
			ic.parallel.funs.doInUiThread {
				thisTaskScope.notifyTaskFinished(task)
			}
		}
	}.also {
		thisTaskScope.notifyTaskStarted(it)
	}

}


@Deprecated("Use doInBackground()")
inline fun TaskScope.inBackground (
	crossinline action : TaskScope.() -> Unit
) = doInBackground(action)
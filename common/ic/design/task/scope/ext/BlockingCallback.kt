package ic.design.task.scope.ext


import ic.base.annotations.Blocking
import ic.design.task.scope.TaskScope
import ic.parallel.funs.blocking.blocking
import ic.parallel.thread.funs.stopThread


@Blocking
inline fun <Result> TaskScope.blockingCallback (

	crossinline action : () -> Result

) : Result {

	return blocking {

		doInUiThread {

			try {

				if (isOpen) {
					notifyReturn(
						action()
					)
				} else {
					stopThread
				}

			} catch (thrown: Throwable) {
				notifyThrow(
					thrown
				)
			}

		}

	}

}
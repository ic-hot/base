package ic.design.task.scope.ext


import ic.design.task.scope.TaskScope
import ic.network.avail.NetworkAvailabilityCallback
import ic.network.avail.NetworkType


@Suppress("NOTHING_TO_INLINE")
inline fun TaskScope.listenNetworkAvailability (
	callback : NetworkAvailabilityCallback,
    toCallAtOnce : Boolean
) {
	notifyTaskStarted(
		ic.network.avail.listenNetworkAvailability(
			callback = callback,
			toCallAtOnce = toCallAtOnce
		)
	)
}


inline fun TaskScope.listenNetworkAvailability (
	toCallAtOnce : Boolean,
	crossinline onConnectionEstablished : (NetworkType) -> Unit,
	crossinline onConnectionLost : () -> Unit
) {
	notifyTaskStarted(
		ic.network.avail.listenNetworkAvailability(
			toCallAtOnce = toCallAtOnce,
			onConnectionEstablished = onConnectionEstablished,
			onConnectionLost = onConnectionLost
		)
	)
}
package ic.design.task.scope


import ic.ifaces.cancelable.Cancelable
import ic.ifaces.lifecycle.hasopenstate.HasIsOpenState


interface TaskScope : HasIsOpenState {

	fun notifyTaskStarted (task: Cancelable)

	fun notifyTaskFinished (task: Cancelable)

	fun cancelTasks()

}
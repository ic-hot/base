package ic.design.task.executor.failure


import ic.design.task.callback.progress.ProgressCallback
import ic.design.task.executor.failure.progress.BaseExecutorJobWithFailureAndProgress


abstract class BaseExecutorJobWithFailure<Input, Output, Failure>
	: BaseExecutorJobWithFailureAndProgress<Input, Output, Failure, Unit>()
{


	@Throws(Throwable::class)
	abstract fun run (input: Input) : Output


	@Throws(Throwable::class)
	final override fun run (input: Input, progressCallback: ProgressCallback<Unit>) : Output {
		return run(input = input)
	}


}
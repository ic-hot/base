package ic.design.task.executor.failure.ext


import ic.design.task.Task
import ic.design.task.executor.failure.ExecutorJobWithFailure
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor


inline fun <Input, Failure> ExecutorJobWithFailure<Input, Unit, Failure>.start (
	input : Input,
	executor : Executor = defaultTaskExecutor, callbackExecutor : Executor = defaultCallbackExecutor,
	crossinline onFinish : () -> Unit,
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : () -> Unit
) : Task? {
	return start(
		input = input,
		executor = executor, callbackExecutor = callbackExecutor,
		onFinish = onFinish,
		onFailure = onFailure,
		onSuccess = { _ -> onSuccess() }
	)
}
package ic.design.task.executor.failure.ext


import ic.design.task.Task
import ic.design.task.executor.ExecutorJobInput
import ic.design.task.executor.failure.ExecutorJobWithFailure
import ic.design.task.failure.progress.ext.start
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor


inline fun <Input, Output, Failure> ExecutorJobWithFailure<Input, Output, Failure>.start (
	input : Input,
	executor : Executor = defaultTaskExecutor, callbackExecutor : Executor = defaultCallbackExecutor,
	crossinline onFinish : () -> Unit,
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit
) : Task? {
	return start(
		input = ExecutorJobInput(
			executor = executor, callbackExecutor = callbackExecutor,
			input = input
		),
		onFinish = onFinish,
		onFailure = onFailure,
		onSuccess = onSuccess
	)
}
@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.executor.failure.ext


import ic.design.task.Task
import ic.design.task.callback.failure.JobCallbackWithFailure
import ic.design.task.executor.ExecutorJobInput
import ic.design.task.executor.failure.ExecutorJobWithFailure
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor


inline fun <Input, Output, Failure> ExecutorJobWithFailure<Input, Output, Failure>.start (
	input : Input,
	executor : Executor = defaultTaskExecutor, callbackExecutor : Executor = defaultCallbackExecutor,
	callback : JobCallbackWithFailure<in Output, in Failure>
) : Task? {
	return start(
		input = ExecutorJobInput(
			executor = executor, callbackExecutor = callbackExecutor,
			input = input
		),
		callback = callback
	)
}
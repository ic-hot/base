package ic.design.task.executor.failure.progress


import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.design.task.executor.ExecutorJobInput


typealias ExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress> =
	JobWithFailureAndProgress<ExecutorJobInput<Input>, Output, Failure, Progress>
;
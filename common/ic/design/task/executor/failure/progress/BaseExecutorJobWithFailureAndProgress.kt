package ic.design.task.executor.failure.progress


import ic.design.task.Task
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.callback.progress.ProgressCallback
import ic.design.task.executor.ExecutorJobInput
import ic.design.task.executor.progress.ExecutorTask


abstract class BaseExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>
	: ExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>
{


	@Throws(Throwable::class)
	abstract fun run (input: Input, progressCallback: ProgressCallback<Progress>) : Output


	abstract fun getFailure (throwable: Throwable) : Failure


	override fun start(
		input : ExecutorJobInput<Input>,
		callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>
	) : Task? {

		return ExecutorTask(
			executor = input.executor,
			callbackExecutor = input.callbackExecutor,
			run = { progressCallback : ProgressCallback<Progress> ->
				run(
					input = input.input,
					progressCallback = progressCallback
				)
			},
			isProgressEnabled = { false },
			onProgress = { callback.notifyProgress(it) },
			onThrown = { throwable ->
				callback.notifyFailure(
					getFailure(throwable)
				)
			},
			onSuccess = { callback.notifySuccess(it) }
		)

	}


}
@file:Suppress("PackageDirectoryMismatch")


package ic.design.task.executor.failure.progress

import ic.design.task.callback.progress.ProgressCallback


inline fun <Input, Output, Failure, Progress> ExecutorJobWithFailureAndProgress (

	crossinline run : (Input, ProgressCallback<Progress>) -> Output,

	crossinline getFailure : (Throwable) -> Failure

) : ExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress> {

	return object : BaseExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>() {

		override fun run (input: Input, progressCallback: ProgressCallback<Progress>): Output {
			return run(input, progressCallback)
		}

		override fun getFailure (throwable: Throwable): Failure {
			return getFailure(throwable)
		}

	}

}
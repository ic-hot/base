package ic.design.task.executor.failure


import ic.design.task.executor.failure.progress.ExecutorJobWithFailureAndProgress


typealias ExecutorJobWithFailure<Input, Output, Failure> = ExecutorJobWithFailureAndProgress<Input, Output, Failure, Unit>

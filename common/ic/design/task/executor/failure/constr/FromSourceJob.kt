@file:Suppress("PackageDirectoryMismatch")


package ic.design.task.executor.failure


import ic.base.throwables.FailureException
import ic.design.task.failure.JobWithFailure
import ic.design.task.failure.progress.ext.run


fun <Input, Output, Failure> ExecutorJobWithFailure (

	sourceJob : JobWithFailure<Input, Output, Failure>

) = ExecutorJobWithFailure(

	run = { input: Input ->

		sourceJob.run(input)

	},

	getFailure = { thrown ->

		if (thrown is FailureException) {
			@Suppress("UNCHECKED_CAST")
			thrown.failure as Failure
		} else {
			throw thrown
		}

	}

)
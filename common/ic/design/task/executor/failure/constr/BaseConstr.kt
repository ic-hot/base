@file:Suppress("PackageDirectoryMismatch")


package ic.design.task.executor.failure


inline fun <Input, Output, Failure> ExecutorJobWithFailure (

	crossinline run : (Input) -> Output,

	crossinline getFailure : (Throwable) -> Failure

) : ExecutorJobWithFailure<Input, Output, Failure> {

	return object : BaseExecutorJobWithFailure<Input, Output, Failure>() {

		@Throws(Throwable::class)
		override fun run (input: Input) = run(input)

		override fun getFailure (throwable: Throwable) = getFailure(throwable)

	}

}
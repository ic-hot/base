package ic.design.task.executor


import ic.ifaces.executor.Executor


data class ExecutorJobInput<Input>(

	val executor : Executor,

	val callbackExecutor : Executor,

	val input : Input

)
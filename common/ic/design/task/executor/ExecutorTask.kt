package ic.design.task.executor


import ic.design.task.Task
import ic.design.task.executor.progress.ExecutorTask
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor


inline fun <Result> ExecutorTask (

	executor 			: Executor = defaultTaskExecutor,
	callbackExecutor 	: Executor = defaultCallbackExecutor,

	crossinline run : () -> Result,

	crossinline onFinish : () -> Unit = {},
	crossinline onThrown : (Throwable) -> Unit = { throw it },

	crossinline onSuccess : (Result) -> Unit

) : Task? {

	return ExecutorTask<Result, Nothing>(
		executor = executor,
		callbackExecutor = callbackExecutor,
		isProgressEnabled = { false },
		onProgress = {},
		run = { run() },
		onFinish = onFinish,
		onThrown = onThrown,
		onSuccess = onSuccess
	)

}


inline fun ExecutorTask (

	executor 			: Executor = defaultTaskExecutor,
	callbackExecutor 	: Executor = defaultCallbackExecutor,

	crossinline run : () -> Unit,

	crossinline onFinish : () -> Unit = {},
	crossinline onThrown : (Throwable) -> Unit = { throw it },

	crossinline onSuccess : () -> Unit

) : Task? {

	return ExecutorTask(
		executor = executor,
		callbackExecutor = callbackExecutor,
		run = { run() },
		onFinish = onFinish,
		onThrown = onThrown,
		onSuccess = { _ -> onSuccess() }
	)

}
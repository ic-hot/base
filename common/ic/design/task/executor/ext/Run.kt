package ic.design.task.executor.ext


import ic.design.task.executor.ExecutorJobInput
import ic.design.task.executor.failure.BaseExecutorJobWithFailure
import ic.design.task.executor.failure.ExecutorJobWithFailure
import ic.design.task.failure.progress.ext.run
import ic.ifaces.executor.DirectExecutor


@Suppress("NOTHING_TO_INLINE")
inline fun <Input, Output, Failure> ExecutorJobWithFailure<Input, Output, Failure>.run (

	input : Input

) : Output {

	if (this is BaseExecutorJobWithFailure) {

		return run(input = input)

	} else {

		return run(
			input = ExecutorJobInput(
				input = input,
				executor = DirectExecutor,
				callbackExecutor = DirectExecutor
			)
		)

	}

}
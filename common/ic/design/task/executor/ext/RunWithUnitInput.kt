package ic.design.task.executor.ext


import ic.design.task.executor.failure.ExecutorJobWithFailure


@Suppress("NOTHING_TO_INLINE")
inline fun <Output, Failure> ExecutorJobWithFailure<Unit, Output, Failure>.run() : Output {

	return run(input = Unit)

}
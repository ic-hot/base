package ic.design.task.executor.progress


import ic.design.task.executor.failure.progress.ExecutorJobWithFailureAndProgress


typealias ExecutorJobWithProgress<Input, Output, Progress>
	= ExecutorJobWithFailureAndProgress<Input, Output, Nothing, Progress>
;


package ic.design.task.executor.progress


import ic.design.task.executor.failure.progress.BaseExecutorJobWithFailureAndProgress


abstract class BaseExecutorJobWithProgress<Input, Output, Progress>
	: BaseExecutorJobWithFailureAndProgress<Input, Output, Nothing, Progress>()
{

	override fun getFailure (throwable: Throwable) = throw throwable

}

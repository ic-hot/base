@file:Suppress("FunctionName")


package ic.design.task.executor.progress


import ic.design.task.callback.progress.ProgressCallback
import ic.design.task.Task
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.ifaces.executor.ext.execute
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.util.log.logV


inline fun <Result, Progress> ExecutorTask (

	executor 			: Executor = defaultTaskExecutor,
	callbackExecutor 	: Executor = defaultCallbackExecutor,

	crossinline run : (ProgressCallback<Progress>) -> Result,

	crossinline isProgressEnabled : () -> Boolean = { true },
	crossinline onProgress : (Progress) -> Unit,

	crossinline onFinish : () -> Unit = {},
	crossinline onThrown : (Throwable) -> Unit = { throw it },
	crossinline onSuccess : (Result) -> Unit

) : Task? {

	val mutex = Mutex()

	logV("ExecutorTask") { "${ mutex.hashCode() } start" }

	var isCanceled : Boolean = false
	var isFinished : Boolean = false

	val progressCallback = ProgressCallback<Progress>(isProgressEnabled = isProgressEnabled) { progress ->
		callbackExecutor.execute {
			mutex.synchronized {
				logV("ExecutorTask") { "${ mutex.hashCode() } attemptOnProgressCallback" }
				if (!isCanceled) {
					logV("ExecutorTask") { "${ mutex.hashCode() } onProgressCallback" }
					onProgress(progress)
				}
			}
		}
	}

	val stoppable = executor.execute executingTask@{

		val output = try {

			run(progressCallback)

		} catch (thrown: Throwable) {

			callbackExecutor.execute {
				logV("ExecutorTask") { "${ mutex.hashCode() } attemptOnThrownCallback" }
				mutex.synchronized {
					if (!isCanceled) {
						logV("ExecutorTask") { "${ mutex.hashCode() } onThrownCallback" }
						isFinished = true
						onFinish()
						onThrown(thrown)
					}
				}
			}

			return@executingTask

		}

		callbackExecutor.execute {
			logV("ExecutorTask") { "${ mutex.hashCode() } attemptOnSuccessCallback" }
			mutex.synchronized {
				if (!isCanceled) {
					logV("ExecutorTask") { "${ mutex.hashCode() } onSuccessCallback" }
					isFinished = true
					onFinish()
					onSuccess(output)
				}
			}
		}

	}

	if (isFinished) {
		return null
	} else {
		return Task(
			cancel = {
				mutex.synchronized {
					logV("ExecutorTask") { "${ mutex.hashCode() } attemptCancel" }
					if (!isCanceled) {
						logV("ExecutorTask") { "${ mutex.hashCode() } cancel" }
						isCanceled = true
						stoppable?.stopNonBlockingIfNeeded()
					}
				}
			}
		)
	}

}
package ic.design.task.progress


import ic.design.task.failure.progress.JobWithFailureAndProgress


typealias JobWithProgress<Input, Output, Progress> = JobWithFailureAndProgress<Input, Output, Nothing, Progress>
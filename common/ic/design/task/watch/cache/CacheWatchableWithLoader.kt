package ic.design.task.watch.cache


import ic.base.throwables.NotExistsException

import ic.ifaces.cancelable.Cancelable


abstract class CacheWatchableWithLoader <Value> : CacheWatchable<Value>() {


	protected abstract fun load (watcher: (Value) -> Unit) : Cancelable?

	protected abstract fun isFinished (value: Value) : Boolean

	protected abstract fun isSuccess (value: Value) : Boolean


	private var loadTask : Cancelable? = null


	override fun watch (watcher: (Value) -> Unit) : Cancelable {
		return super.watch(watcher).also {
			try {
				val cachedValue = getOrThrow()
				if (!isSuccess(cachedValue)) {
					startLoadingIfNotAlready()
				}
			} catch (_: NotExistsException) {
				startLoadingIfNotAlready()
			}
		}
	}

	private fun startLoadingIfNotAlready () {
		if (loadTask == null) {
			loadTask = load { value ->
				if (isFinished(value)) {
					loadTask = null
				}
				set(value)
			}
		}
	}


	fun reload () {
		loadTask?.cancel()
		loadTask = null
		startLoadingIfNotAlready()
	}


}
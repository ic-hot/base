package ic.design.task.watch.cache


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable


inline fun <Value> CacheWatchable (

	crossinline load : Job<Value>,

	crossinline isFinished : (Value) -> Boolean = { false },
	crossinline isSuccess  : (Value) -> Boolean = { true  }

) : CacheWatchableWithLoader<Value> {

	return object : CacheWatchableWithLoader<Value>() {

		override fun load (watcher: (Value) -> Unit) : Cancelable? {
			return load(watcher)
		}

		override fun isFinished (value: Value) : Boolean {
			return isFinished(value)
		}

		override fun isSuccess (value: Value) : Boolean {
			return isSuccess(value)
		}

	}

}
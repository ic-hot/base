package ic.design.task.watch.cache


import kotlin.jvm.JvmName

import ic.base.throwables.NotExistsException
import ic.design.task.watch.BaseWatchable
import ic.ifaces.cancelable.Cancelable


open class CacheWatchable<Value> : BaseWatchable<Value>() {


	private var hasCachedValue : Boolean = false

	private var cachedValue : Value? = null


	override fun watch (watcher: (Value) -> Unit) : Cancelable {
		return super.watch(watcher).also {
			try {
				val cachedValue = getOrThrow()
				watcher(cachedValue)
			} catch (_: NotExistsException) {}
		}
	}


	@Throws(NotExistsException::class)
	fun getOrThrow () : Value {
		if (hasCachedValue) {
			@Suppress("UNCHECKED_CAST")
			return cachedValue as Value
		} else {
			throw NotExistsException
		}
	}


	fun set (value: Value) {
		if (hasCachedValue) {
			if (value != cachedValue) {
				cachedValue = value
				notify(value)
			}
		} else {
			cachedValue = value
			hasCachedValue = true
			notify(value)
		}
	}


	@JvmName("notifyWatchable")
	fun notify () {
		try {
			val cachedValue = getOrThrow()
			notify(cachedValue)
		} catch (_: NotExistsException) {}
	}


	fun clear () {
		hasCachedValue = false
		cachedValue = null
	}


}
package ic.design.task.watch


import ic.ifaces.cancelable.Cancelable
import ic.struct.collection.ext.copy.copy
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


open class BaseWatchable <Value> : Watchable<Value> {

	private val watchers = EditableSet<(Value) -> Unit>()

	override fun watch (watcher: (Value) -> Unit) : Cancelable {
		watchers.add(watcher)
		return Cancelable {
			watchers.remove(watcher)
		}
	}

	fun notify (value: Value) {
		watchers.copy().forEach { it(value) }
	}

}
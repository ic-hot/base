package ic.design.task.watch


import ic.design.task.Job
import ic.funs.effect.Effect
import ic.ifaces.cancelable.Cancelable


interface Watchable <Value> : Job<Value> {


	fun watch (watcher: Effect<Value>) : Cancelable


	override fun invoke (watcher: Effect<Value>) = watch(watcher)


}
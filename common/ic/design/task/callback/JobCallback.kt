package ic.design.task.callback


import ic.design.task.callback.failure.JobCallbackWithFailure
import ic.design.task.callback.progress.JobCallbackWithProgress


interface JobCallback<Output> : JobCallbackWithFailure<Output, Nothing>, JobCallbackWithProgress<Output, Unit> {



}
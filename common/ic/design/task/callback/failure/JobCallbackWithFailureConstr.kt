@file:Suppress("FunctionName")


package ic.design.task.callback.failure


inline fun <SuccessOutput, FailureOutput> JobCallbackWithFailure (

	crossinline onFinish : () -> Unit = {},

	crossinline onFailure : (FailureOutput) -> Unit,
	crossinline onSuccess : (SuccessOutput) -> Unit

) : JobCallbackWithFailure<SuccessOutput, FailureOutput> {

	return object : BaseJobCallbackWithFailure<SuccessOutput, FailureOutput>() {

		override fun onFinish() = onFinish()

		override fun onFailure (failure: FailureOutput) = onFailure(failure)

		override fun onSuccess (output: SuccessOutput) = onSuccess(output)

	}

}
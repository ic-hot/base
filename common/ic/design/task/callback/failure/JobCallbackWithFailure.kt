package ic.design.task.callback.failure


import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress


typealias JobCallbackWithFailure<Output, Failure> = JobCallbackWithFailureAndProgress<Output, Failure, Unit>
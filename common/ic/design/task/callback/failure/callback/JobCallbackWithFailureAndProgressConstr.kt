@file:Suppress("FunctionName")


package ic.design.task.callback.failure.callback


inline fun <Output, Failure, Progress> JobCallbackWithFailureAndProgress (

	crossinline isProgressEnabled : () -> Boolean = { true },
	crossinline onProgress : (Progress) -> Unit,

	crossinline onFinish : () -> Unit,

	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit

) : JobCallbackWithFailureAndProgress<Output, Failure, Progress> {

	return object : BaseJobCallbackWithFailureAndProgress<Output, Failure, Progress>() {

		override val isProgressEnabled get() = isProgressEnabled()

		override fun onProgress (progress: Progress) = onProgress(progress)

		override fun onFinish() = onFinish()

		override fun onFailure (failure: Failure) = onFailure(failure)

		override fun onSuccess (output: Output) = onSuccess(output)

	}

}
package ic.design.task.callback.failure.callback


import ic.design.task.callback.progress.ProgressCallback


interface JobCallbackWithFailureAndProgress<Output, Failure, Progress> : ProgressCallback<Progress> {


	fun notifyFailure (failure: Failure)

	fun notifySuccess (output: Output)


}
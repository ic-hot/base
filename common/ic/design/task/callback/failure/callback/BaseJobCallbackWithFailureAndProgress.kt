package ic.design.task.callback.failure.callback


import ic.design.task.callback.progress.BaseProgressCallback


abstract class BaseJobCallbackWithFailureAndProgress<Output, Failure, Progress>
	: BaseProgressCallback<Progress>()
	, JobCallbackWithFailureAndProgress<Output, Failure, Progress>
{


	protected open fun onFinish() {}

	protected abstract fun onFailure (failure: Failure)

	protected abstract fun onSuccess (output: Output)


	final override fun notifyFailure (failure: Failure) {
		onFinish()
		onFailure(failure)
	}

	final override fun notifySuccess (output: Output) {
		onFinish()
		onSuccess(output)
	}


}
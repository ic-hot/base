package ic.design.task.callback.failure


import ic.design.task.callback.failure.callback.BaseJobCallbackWithFailureAndProgress


abstract class BaseJobCallbackWithFailure<Output, Failure>
	: BaseJobCallbackWithFailureAndProgress<Output, Failure, Unit>()
	, JobCallbackWithFailure<Output, Failure>
{


	override val isProgressEnabled get() = false

	final override fun onProgress (progress: Unit) {}


}
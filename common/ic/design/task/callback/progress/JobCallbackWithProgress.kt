package ic.design.task.callback.progress


import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress


interface JobCallbackWithProgress<Output, Progress> : JobCallbackWithFailureAndProgress<Output, Nothing, Progress> {


}
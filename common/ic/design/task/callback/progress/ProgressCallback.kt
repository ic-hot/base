package ic.design.task.callback.progress


interface ProgressCallback<Progress> {


	val isProgressEnabled : Boolean

	fun notifyProgress (progress: Progress)


}
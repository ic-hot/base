package ic.design.task.callback.progress


import ic.base.throwables.WrongCallException
import ic.design.task.callback.failure.callback.BaseJobCallbackWithFailureAndProgress


abstract class BaseJobCallbackWithProgress<Output, Progress>
	: BaseJobCallbackWithFailureAndProgress<Output, Nothing, Progress>()
	, JobCallbackWithProgress<Output, Progress>
{


	final override fun onFailure (failure: Nothing) = throw WrongCallException.Error()


}
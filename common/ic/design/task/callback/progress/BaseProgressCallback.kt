package ic.design.task.callback.progress


abstract class BaseProgressCallback<Progress> : ProgressCallback<Progress> {


	protected abstract fun onProgress (progress: Progress)


	final override fun notifyProgress (progress: Progress) {
		onProgress(progress)
	}


}
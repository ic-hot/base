@file:Suppress("FunctionName")


package ic.design.task.callback.progress


inline fun <Progress> ProgressCallback (

	crossinline isProgressEnabled : () -> Boolean = { true },

	crossinline onProgress : (Progress) -> Unit

) : ProgressCallback<Progress> {

	return object : BaseProgressCallback<Progress>() {

		override val isProgressEnabled get() = isProgressEnabled()

		override fun onProgress (progress: Progress) = onProgress(progress)

	}

}
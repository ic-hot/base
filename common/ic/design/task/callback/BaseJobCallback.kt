package ic.design.task.callback


import ic.base.throwables.WrongCallException
import ic.design.task.callback.failure.callback.BaseJobCallbackWithFailureAndProgress


abstract class BaseJobCallback<Output> : BaseJobCallbackWithFailureAndProgress<Output, Nothing, Unit>(), JobCallback<Output> {

	override fun onFailure (failure: Nothing) = throw WrongCallException.Error()

	override val isProgressEnabled get() = false

	override fun onProgress (progress: Unit) {}

}
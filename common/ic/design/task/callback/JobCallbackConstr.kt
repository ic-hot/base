package ic.design.task.callback


inline fun <Output> JobCallback (

	crossinline onFinish : () -> Unit = {},

	crossinline onSuccess : (Output) -> Unit

) : JobCallback<Output> {

	return object : BaseJobCallback<Output>() {

		override fun onFinish() = onFinish()

		override fun onSuccess (output: Output) = onSuccess(output)

	}

}
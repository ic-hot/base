@file:Suppress("FunctionName")


package ic.design.task.cache


import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.design.task.Task
import ic.design.task.cache.impl.*
import ic.design.task.cache.impl.CacheStatus
import ic.design.task.cache.impl.startWithCacheFirst
import ic.design.task.cache.impl.startWithRemoteFirst
import ic.design.task.cache.impl.startWithRemoteOnly
import ic.struct.map.editable.EditableMap
import ic.util.cache.Cache
import ic.util.load.LoadMode


inline fun <Input, Output, Failure, Progress, CacheKey> CacheTask (

	job : JobWithFailureAndProgress<Input, Output, Failure, Progress>,

	input : Input,

	cache : Cache<CacheKey, Output>,

	cacheStatuses : EditableMap<CacheKey, CacheStatus<Output, Failure, Progress>>,

	cacheKey : CacheKey,

	loadMode : LoadMode,

	getFailureByTimeout : () -> Failure,

	callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>

) : Task? {

	val task = when (loadMode) {

		is LoadMode.CacheOnly -> startWithCacheOnly(
			cache = cache,
			loadMode = loadMode,
			cacheKey = cacheKey,
			callback = callback,
			getFailureByTimeout = getFailureByTimeout
		)

		is LoadMode.CacheFirst -> startWithCacheFirst(
			job = job,
			cache = cache,
			cacheStatuses = cacheStatuses,
			input = input,
			loadMode = loadMode,
			callback = callback,
			cacheKey = cacheKey
		)

		is LoadMode.RemoteFirst -> startWithRemoteFirst(
			job = job,
			cache = cache,
			input = input,
			loadMode = loadMode,
			callback = callback,
			cacheKey = cacheKey
		)

		is LoadMode.RemoteOnly -> startWithRemoteOnly(
			job = job,
			cache = cache,
			input = input,
			loadMode = loadMode,
			callback = callback,
			cacheKey = cacheKey
		)

	}

	return task

}
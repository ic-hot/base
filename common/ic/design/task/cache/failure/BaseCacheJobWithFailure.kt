package ic.design.task.cache.failure


import ic.design.task.cache.failure.progress.BaseCacheJobWithFailureAndProgress


typealias BaseCacheJobWithFailure<Input, Output, Failure> = BaseCacheJobWithFailureAndProgress<Input, Output, Failure, Unit>
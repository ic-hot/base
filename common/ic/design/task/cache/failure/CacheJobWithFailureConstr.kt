package ic.design.task.cache.failure


import ic.design.task.failure.JobWithFailure


inline fun <Input, Output: Any, Failure> CacheJobWithFailure (

	sourceJob : JobWithFailure<Input, Output, Failure>,

	crossinline getFailureByTimeout : () -> Failure

) : CacheJobWithFailure<Input, Output, Failure> {

	return object : BaseCacheJobWithFailure<Input, Output, Failure>() {

		override val sourceJob get() = sourceJob

		override fun getFailureByTimeout() = getFailureByTimeout()

	}

}
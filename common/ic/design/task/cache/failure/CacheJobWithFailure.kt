package ic.design.task.cache.failure


import ic.design.task.cache.failure.progress.CacheJobWithFailureAndProgress


typealias CacheJobWithFailure<Input, Output, Failure> = CacheJobWithFailureAndProgress<Input, Output, Failure, Unit>
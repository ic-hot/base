package ic.design.task.cache.failure.progress


import ic.design.task.cache.CacheJobInput
import ic.design.task.failure.progress.JobWithFailureAndProgress


typealias CacheJobWithFailureAndProgress<Input, Output, Failure, Progress>
	= JobWithFailureAndProgress<CacheJobInput<Input, Output>, Output, Failure, Progress>
;
package ic.design.task.cache.failure.progress


import ic.design.task.Task
import ic.design.task.cache.CacheJobInput
import ic.design.task.cache.CacheTask
import ic.design.task.cache.impl.CacheStatus
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.struct.map.editable.EditableMap


abstract class BaseCacheJobWithFailureAndProgress<Input, Output, Failure, Progress>
	: CacheJobWithFailureAndProgress<Input, Output, Failure, Progress>
{


	protected abstract val sourceJob : JobWithFailureAndProgress<Input, Output, Failure, Progress>

	protected abstract fun getFailureByTimeout() : Failure


	private val cacheStatuses = EditableMap<Input, CacheStatus<Output, Failure, Progress>>()


	override fun start(
		input : CacheJobInput<Input, Output>,
		callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>
	) : Task? {

		return CacheTask(
			job = sourceJob,
			input = input.input,
			cache = input.cache,
			cacheStatuses = cacheStatuses,
			cacheKey = input.input,
			loadMode = input.loadMode,
			getFailureByTimeout = ::getFailureByTimeout,
			callback = callback
		)

	}


}
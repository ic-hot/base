package ic.design.task.cache.failure.progress


import ic.design.task.failure.progress.JobWithFailureAndProgress


inline fun <Input, Output, Failure, Progress> CacheJobWithFailureAndProgress (

	sourceJob : JobWithFailureAndProgress<Input, Output, Failure, Progress>,

	crossinline getFailureByTimeout : () -> Failure

) : CacheJobWithFailureAndProgress<Input, Output, Failure, Progress> {

	return object : BaseCacheJobWithFailureAndProgress<Input, Output, Failure, Progress>() {

		override val sourceJob get() = sourceJob

		override fun getFailureByTimeout() = getFailureByTimeout()

	}

}
@file:Suppress("PackageDirectoryMismatch")


package ic.design.task.cache.executor.failure


inline fun <Input, Output, Failure> CacheExecutorJobWithFailure (

	crossinline run : (Input) -> Output,

	crossinline getFailure : (Throwable) -> Failure,
	crossinline getFailureByTimeout : () -> Failure

) : CacheExecutorJobWithFailure<Input, Output, Failure> {

	return object : BaseCacheExecutorJobWithFailure<Input, Output, Failure>() {

		override fun run (input: Input) = run(input)

		override fun getFailure (throwable: Throwable) = getFailure(throwable)

		override fun getFailureByTimeout() = getFailureByTimeout()

	}

}
@file:Suppress("PackageDirectoryMismatch")


package ic.design.task.cache.executor.failure


import ic.design.task.executor.failure.BaseExecutorJobWithFailure
import ic.design.task.executor.failure.ExecutorJobWithFailure


inline fun <Input, Output, Failure> CacheJobWithFailure (

	sourceJob : ExecutorJobWithFailure<Input, Output, Failure>,

	crossinline getFailureByTimeout : () -> Failure

) : CacheExecutorJobWithFailure<Input, Output, Failure> {

	val baseExecutorJob = sourceJob as BaseExecutorJobWithFailure<Input, Output, Failure>

	return CacheExecutorJobWithFailure(
		run = baseExecutorJob::run,
		getFailure = baseExecutorJob::getFailure,
		getFailureByTimeout = getFailureByTimeout
	)

}
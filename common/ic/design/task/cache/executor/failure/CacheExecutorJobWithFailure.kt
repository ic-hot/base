package ic.design.task.cache.executor.failure


import ic.design.task.cache.executor.failure.progress.CacheExecutorJobWithFailureAndProgress


typealias CacheExecutorJobWithFailure<Input, Output, Failure>
	= CacheExecutorJobWithFailureAndProgress<Input, Output, Failure, Unit>
;
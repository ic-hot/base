package ic.design.task.cache.executor.failure.progress


import ic.design.task.cache.executor.CacheExecutorJobInput
import ic.design.task.failure.progress.JobWithFailureAndProgress


typealias CacheExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>
	= JobWithFailureAndProgress<CacheExecutorJobInput<Input, Output>, Output, Failure, Progress>
;
package ic.design.task.cache.executor.failure.progress


import ic.design.task.Task
import ic.design.task.cache.CacheJobInput
import ic.design.task.cache.executor.CacheExecutorJobInput
import ic.design.task.cache.failure.progress.CacheJobWithFailureAndProgress
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.callback.progress.ProgressCallback
import ic.design.task.executor.ExecutorJobInput
import ic.design.task.executor.failure.progress.ExecutorJobWithFailureAndProgress
import ic.util.cache.Cache
import ic.util.time.Time


abstract class BaseCacheExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>
	: CacheExecutorJobWithFailureAndProgress<Input, Output, Failure, Progress>
{


	@Throws(Throwable::class)
	protected abstract fun run (input: Input, progressCallback: ProgressCallback<Progress>) : Output

	protected abstract fun getFailure (throwable: Throwable) : Failure

	protected abstract fun getFailureByTimeout() : Failure


	private val cacheJobOfExecutorJob = CacheJobWithFailureAndProgress(
		ExecutorJobWithFailureAndProgress(
			run = ::run,
			getFailure = ::getFailure
		),
		getFailureByTimeout = ::getFailureByTimeout
	)


	private fun castCache (cache : Cache<Input, Output>) = object : Cache<ExecutorJobInput<Input>, Output>() {

		override fun implGetTimeOrThrow (key: ExecutorJobInput<Input>) : Time {
			return cache.getTimeOrThrow(key.input)
		}

		override fun implGetValueOrThrow(key: ExecutorJobInput<Input>) : Output {
			return cache.getOrThrow(key.input)
		}

		override fun implSet (key: ExecutorJobInput<Input>, value: Output) {
			cache[key.input] = value
		}

		override fun implIsDiscarded (key: ExecutorJobInput<Input>): Boolean {
			return cache.isDiscarded(key.input)
		}

		override fun implDiscard (key: ExecutorJobInput<Input>) {
			cache.discard(key.input)
		}

		override fun implEmpty() {
			cache.empty()
		}

		override fun implDiscardAll() {
			cache.discardAll()
		}

		override fun implRemove (key: ExecutorJobInput<Input>) {
			cache.remove(key.input)
		}

	}


	override fun start(
		input : CacheExecutorJobInput<Input, Output>,
		callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>
	) : Task? {

		return cacheJobOfExecutorJob.start(
			input = CacheJobInput(
				cache = castCache(input.cache),
				loadMode = input.loadMode,
				input = ExecutorJobInput(
					executor = input.executor, callbackExecutor = input.callbackExecutor,
					input = input.input
				)
			),
			callback = callback
		)

	}


}
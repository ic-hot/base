package ic.design.task.cache.executor.failure


import ic.design.task.cache.executor.failure.progress.BaseCacheExecutorJobWithFailureAndProgress
import ic.design.task.callback.progress.ProgressCallback


abstract class BaseCacheExecutorJobWithFailure <Input, Output, Failure>
	: BaseCacheExecutorJobWithFailureAndProgress<Input, Output, Failure, Unit>()
{


	protected abstract fun run (input: Input) : Output


	override fun run (input: Input, progressCallback: ProgressCallback<Unit>) : Output {
		return run(input)
	}


}
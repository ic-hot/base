package ic.design.task.cache.executor.failure.ext


import ic.design.task.Task
import ic.design.task.cache.executor.CacheExecutorJobInput
import ic.design.task.cache.executor.failure.CacheExecutorJobWithFailure
import ic.design.task.failure.progress.ext.start
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.util.cache.Cache
import ic.util.cache.getCommonRamCache
import ic.util.load.LoadMode


inline fun <Input, Output, Failure> CacheExecutorJobWithFailure<Input, Output, Failure>.start (
	input : Input,
	executor : Executor = defaultTaskExecutor,
	callbackExecutor : Executor = defaultCallbackExecutor,
	cache : Cache<Input, Output> = getCommonRamCache(),
	loadMode : LoadMode = LoadMode.CacheFirst.Default,
	crossinline onFinish : () -> Unit = {},
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit
) : Task? {
	return start(
		CacheExecutorJobInput(
			cache = cache,
			loadMode = loadMode,
			executor = executor, callbackExecutor = callbackExecutor,
			input = input
		),
		onFinish = onFinish,
		onFailure = onFailure,
		onSuccess = onSuccess
	)
}
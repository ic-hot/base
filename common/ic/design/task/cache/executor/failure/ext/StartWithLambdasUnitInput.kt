package ic.design.task.cache.executor.failure.ext


import ic.design.task.Task
import ic.design.task.cache.executor.failure.CacheExecutorJobWithFailure
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.util.cache.Cache
import ic.util.cache.getCommonRamCache
import ic.util.load.LoadMode


inline fun <Output: Any, Failure> CacheExecutorJobWithFailure<Unit, Output, Failure>.start (
	executor : Executor = defaultTaskExecutor,
	callbackExecutor : Executor = defaultCallbackExecutor,
	cache : Cache<Unit, Output> = getCommonRamCache(),
	loadMode : LoadMode = LoadMode.CacheFirst.Default,
	crossinline onFinish : () -> Unit = {},
	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit
) : Task? {
	return start(
		input = Unit,
		executor = executor, callbackExecutor = callbackExecutor,
		cache = cache,
		loadMode = loadMode,
		onFinish = onFinish,
		onFailure = onFailure,
		onSuccess = onSuccess
	)
}
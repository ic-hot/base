@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.cache.executor.failure.ext


import ic.base.throwables.FailureException
import ic.design.task.cache.executor.CacheExecutorJobInput
import ic.design.task.cache.executor.failure.CacheExecutorJobWithFailure
import ic.design.task.failure.progress.ext.run
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.util.cache.Cache
import ic.util.cache.getCommonRamCache
import ic.util.load.LoadMode


inline fun <Input, Output: Any, Failure> CacheExecutorJobWithFailure<Input, Output, Failure>.run (
	input : Input,
	executor : Executor = defaultTaskExecutor,
	callbackExecutor : Executor = defaultCallbackExecutor,
	cache : Cache<Input, Output> = getCommonRamCache(),
	loadMode : LoadMode = LoadMode.CacheFirst.Default,
	onFailure : (Failure) -> Output = { throw FailureException(it) }
) : Output {
	return run(
		CacheExecutorJobInput(
			cache = cache,
			loadMode = loadMode,
			executor = executor, callbackExecutor = callbackExecutor,
			input = input
		),
		onFailure = onFailure
	)
}
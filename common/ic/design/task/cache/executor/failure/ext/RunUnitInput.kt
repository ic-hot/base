@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.cache.executor.failure.ext


import ic.base.throwables.FailureException
import ic.design.task.cache.executor.failure.CacheExecutorJobWithFailure
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor
import ic.util.cache.Cache
import ic.util.cache.getCommonRamCache
import ic.util.load.LoadMode


inline fun <Output: Any, Failure> CacheExecutorJobWithFailure<Unit, Output, Failure>.run (
	executor : Executor = defaultTaskExecutor,
	callbackExecutor : Executor = defaultCallbackExecutor,
	cache : Cache<Unit, Output> = getCommonRamCache(),
	loadMode : LoadMode = LoadMode.CacheFirst.Default,
	onFailure : (Failure) -> Output = { throw FailureException(it) }
) : Output {
	return run(
		input = Unit,
		executor = executor, callbackExecutor = callbackExecutor,
		cache = cache,
		loadMode = loadMode,
		onFailure = onFailure
	)
}
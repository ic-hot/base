package ic.design.task.cache.executor


import ic.ifaces.executor.Executor
import ic.util.cache.Cache
import ic.util.load.LoadMode


data class CacheExecutorJobInput<Input, Output> (

	val cache : Cache<Input, Output>,

	val loadMode : LoadMode,

	val executor : Executor,

	val callbackExecutor : Executor,

	val input : Input

)
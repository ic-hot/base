@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.cache.impl


import ic.base.throwables.NotExistsException
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.design.task.Task
import ic.design.task.failure.progress.ext.start
import ic.util.cache.Cache
import ic.util.load.LoadMode
import ic.util.time.duration.ext.ms


inline fun <Output, Failure, Progress, SourceInput, CacheKey> startWithRemoteFirst (

	job : JobWithFailureAndProgress<SourceInput, Output, Failure, Progress>,

	cache : Cache<CacheKey, Output>,

	input : SourceInput,

	loadMode : LoadMode.RemoteFirst,

	callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>,

	cacheKey : CacheKey

) : Task? {

	return job.start(
		input = input,
		onFinish = {},
		onProgress = { progress -> callback.notifyProgress(progress) },
		onFailure = { failure ->
			try {
				val outputFromCache = cache.getOrThrow(
					key = cacheKey,
					maxAge = loadMode.expireDurationMs.ms
				)
				callback.notifySuccess(outputFromCache)
			} catch (t: NotExistsException) {
				callback.notifyFailure(failure)
			}
		},
		onSuccess = { output ->
			cache.set(cacheKey, output)
			callback.notifySuccess(output)
		}
	)

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.cache.impl


import ic.base.throwables.NotExistsException
import ic.ifaces.gettersetter.gettersetter1.ext.getOrCreateIfNull
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.design.task.Task
import ic.design.task.failure.progress.ext.start
import ic.ifaces.mutable.ext.synchronized
import ic.parallel.mutex.Mutex
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copy
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.collection.ext.foreach.forEach
import ic.struct.map.editable.EditableMap
import ic.util.cache.Cache
import ic.util.load.LoadMode
import ic.util.log.logV


inline fun <Input, Output, Failure, Progress, CacheKey> startWithCacheFirst (

	job : JobWithFailureAndProgress<Input, Output, Failure, Progress>,

	cache : Cache<CacheKey, Output>,

	cacheStatuses : EditableMap<CacheKey, CacheStatus<Output, Failure, Progress>>,

	input : Input,

	loadMode : LoadMode.CacheFirst,

	callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>,

	cacheKey : CacheKey

) : Task? {

	val mutex = Mutex()

	logV("StartWithCacheFirst") { "${ mutex.hashCode() } start" }

	val cacheStatus = cacheStatuses.getOrCreateIfNull(cacheKey) { CacheStatus() }

	cacheStatus.synchronized {

		try {
			val existingOutput = cache.getOrThrow(cacheKey, maxAge = loadMode.expireDuration)
			callback.notifySuccess(existingOutput)
			return null
		} catch (_: NotExistsException) {}

		cacheStatus.callbacks.add(callback)

		if (cacheStatus.currentTask == null) {

			cacheStatus.currentTask = job.start(
				input = input,
				onProgress = { progress -> callback.notifyProgress(progress) },
				onFailure = { failure ->
					logV("StartWithCacheFirst") { "${ mutex.hashCode() } onFailure" }
					lateinit var callbacks : Collection<JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>>
					cacheStatus.synchronized {
						cacheStatus.currentTask = null
						callbacks = cacheStatus.callbacks.copy()
						cacheStatus.callbacks.empty()
					}
					callbacks.forEach { it.notifyFailure(failure) }
				},
				onSuccess = { output ->
					logV("StartWithCacheFirst") { "${ mutex.hashCode() } onSuccess" }
					cache[cacheKey] = output
					lateinit var callbacks : Collection<JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>>
					cacheStatus.synchronized {
						cacheStatus.currentTask = null
						callbacks = cacheStatus.callbacks.copy()
						cacheStatus.callbacks.empty()
					}
					callbacks.forEach { it.notifySuccess(output) }
				}
			)

		}

	}

	return Task (
		cancel = {
			logV("StartWithCacheFirst") { "${ mutex.hashCode() } cancel" }
			cacheStatus.synchronized {
				cacheStatus.callbacks.removeIfExists(callback)
			}
		}
	)

}
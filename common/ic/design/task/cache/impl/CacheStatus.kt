package ic.design.task.cache.impl


import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.Task
import ic.ifaces.mutable.Mutable
import ic.parallel.mutex.Mutex
import ic.struct.set.editable.EditableSet


class CacheStatus<Output, Failure, Progress> : Mutable {

	override val mutex = Mutex()

	var currentTask : Task? = null

	val callbacks = (
		EditableSet<JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>>()
	)

	override fun toString() = "CacheStatus { currentTask: $currentTask, callbacks: $callbacks }"

}
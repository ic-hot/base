@file:Suppress("NOTHING_TO_INLINE")


package ic.design.task.cache.impl


import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.design.task.Task
import ic.design.task.failure.progress.ext.start
import ic.util.cache.Cache
import ic.util.load.LoadMode


inline fun <Output, Failure, Progress, SourceInput, CacheKey> startWithRemoteOnly (

	job : JobWithFailureAndProgress<SourceInput, Output, Failure, Progress>,

	cache : Cache<CacheKey, Output>,

	input : SourceInput,

	loadMode : LoadMode.RemoteOnly,

	callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>,

	cacheKey : CacheKey

) : Task? {

	return job.start(
		input = input,
		onProgress = { progress -> callback.notifyProgress(progress) },
		onFailure = { failure ->
			callback.notifyFailure(failure)
		},
		onSuccess = { output ->
			if (loadMode.toSaveToCache) {
				cache.set(cacheKey, output)
			}
			callback.notifySuccess(output)
		}
	)

}
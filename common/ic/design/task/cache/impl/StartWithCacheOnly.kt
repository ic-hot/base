package ic.design.task.cache.impl


import ic.base.throwables.NotExistsException
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.Task
import ic.util.cache.Cache
import ic.util.load.LoadMode
import ic.util.time.duration.ext.ms


inline fun <Output, Failure, Progress, CacheKey> startWithCacheOnly (

	cache : Cache<CacheKey, Output>,

	loadMode : LoadMode.CacheOnly,

	callback : JobCallbackWithFailureAndProgress<in Output, Failure, Progress>,

	cacheKey : CacheKey,

	getFailureByTimeout : () -> Failure

) : Task? {

	val cachedOutput = try {
		cache.getOrThrow(
			key = cacheKey,
			maxAge = loadMode.expireDurationMs.ms
		)
	} catch (t: NotExistsException) {
		callback.notifyFailure(getFailureByTimeout())
		return null
	}

	callback.notifySuccess(cachedOutput)
	return null

}
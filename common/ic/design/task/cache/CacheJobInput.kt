package ic.design.task.cache


import ic.util.cache.Cache
import ic.util.load.LoadMode


data class CacheJobInput<Input, Output> (

	val cache : Cache<Input, Output>,

	val loadMode : LoadMode,

	val input : Input

)
package ic.design.task.delay


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable
import ic.parallel.funs.schedule
import ic.util.time.duration.Duration


class JobDelayer (

	private val delay : Duration

) {

	private var task : Cancelable? = null

	operator fun <Result> invoke (job: Job<Result>) : Job<Result> = { callback ->
		task?.cancel()
		task = schedule(delay) {
			task = job(callback)
		}
		Cancelable {
			task?.cancel()
			task = null
		}
	}

}
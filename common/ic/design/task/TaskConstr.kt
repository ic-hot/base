package ic.design.task


import ic.design.task.executor.ExecutorTask
import ic.ifaces.executor.Executor
import ic.ifaces.executor.defaultCallbackExecutor
import ic.ifaces.executor.defaultTaskExecutor


inline fun Task (
	crossinline cancel : () -> Unit
) : Task {
	return object : Task {
		override fun cancel() = cancel()
	}
}


// ExecutorTask:

@Deprecated("Do not use ExecutorTask's")
inline fun <Result> Task (
	executor 			: Executor = defaultTaskExecutor,
	callbackExecutor 	: Executor = defaultCallbackExecutor,
	crossinline run : () -> Result,
	crossinline onFinish : () -> Unit = {},
	crossinline onThrown : (Throwable) -> Unit = { throw it },
	crossinline onSuccess : (Result) -> Unit
) = ExecutorTask(
	executor = executor,
	callbackExecutor = callbackExecutor,
	run = run,
	onFinish = onFinish,
	onThrown = onThrown,
	onSuccess = onSuccess
)
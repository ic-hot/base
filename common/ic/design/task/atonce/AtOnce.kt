package ic.design.task.atonce


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable


abstract class AtOnce <Result> : Job<Result> {

	protected abstract fun getResult () : Result

	override fun invoke (callback: (Result) -> Unit) : Cancelable? {
		callback(getResult())
		return null
	}

}
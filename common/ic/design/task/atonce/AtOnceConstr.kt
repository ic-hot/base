package ic.design.task.atonce


inline fun <Result> AtOnce (result: Result) : AtOnce<Result> {
	return object : AtOnce<Result>() {
		override fun getResult() = result
	}
}

inline fun <Result> AtOnce (crossinline getResult : () -> Result) : AtOnce<Result> {
	return object : AtOnce<Result>() {
		override fun getResult() = getResult()
	}
}
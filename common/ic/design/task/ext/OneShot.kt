package ic.design.task.ext


import ic.design.task.Job

import ic.ifaces.cancelable.Cancelable


inline fun
	<Result>
	Job<Result>.oneShot (
		crossinline isFinished : (Result) -> Boolean
	) : Job<Result>
	= { onResult ->
		var watchTask : Cancelable? = null
		var isFinishedAtOnce : Boolean = false
		watchTask = this { result ->
			onResult(result)
			if (isFinished(result)) {
				if (watchTask == null) {
					isFinishedAtOnce = true
				} else {
					watchTask!!.cancel()
					watchTask = null
				}
			}
		}
		if (isFinishedAtOnce) {
			if (watchTask != null) {
				watchTask!!.cancel()
				watchTask = null
			}
			null
		} else {
			Cancelable {
				watchTask?.cancel()
				watchTask = null
			}
		}
	}
;
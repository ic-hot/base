package ic.design.task.ext


import ic.design.task.Job
import ic.ifaces.cancelable.Cancelable
import ic.parallel.funs.doInUiThread
import ic.parallel.funs.sleepTill


inline fun <Result> Job<Result>.asBlocking (
	crossinline isFinished : (Result) -> Boolean,
	crossinline isSuccess  : (Result) -> Boolean = { true }
) = {
	var task : Cancelable? = null
	var hasFinishedResult : Boolean = false
	var finishedResult : Result? = null
	task = doInUiThread {
		task = this { result ->
			if (isFinished(result)) {
				task?.cancel()
				task = null
				hasFinishedResult = true
				finishedResult = result
			}
		}
	}
	try {
		sleepTill { hasFinishedResult }
	} finally {
		task?.cancel()
	}
	@Suppress("UNCHECKED_CAST")
	val result = finishedResult as Result
	if (isSuccess(result)) {
		result
	} else {
		throw RuntimeException("result: $result")
	}
}


inline val <Result> Job<Result>.asBlocking get() = (
	asBlocking(
		isFinished = { true }
	)
)
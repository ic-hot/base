package ic.design.task


import ic.funs.effect.Effect
import ic.ifaces.cancelable.Cancelable


typealias Job<Result> = (Effect<Result>) -> Cancelable?
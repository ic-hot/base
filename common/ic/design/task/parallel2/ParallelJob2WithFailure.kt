package ic.design.task.parallel2


import ic.design.task.Task
import ic.design.task.callback.failure.JobCallbackWithFailure
import ic.design.task.failure.JobWithFailure
import ic.design.task.funs.startInParallel


abstract class ParallelJob2WithFailure
	<Input, Output, Failure, Input1, Input2, Output1, Output2, Failure1, Failure2>
	: JobWithFailure<Input, Output, Failure>
{


	protected abstract val sourceJob1 : JobWithFailure<Input1, Output1, Failure1>
	protected abstract val sourceJob2 : JobWithFailure<Input2, Output2, Failure2>


	protected abstract fun inputToInput1 (input: Input) : Input1
	protected abstract fun inputToInput2 (input: Input) : Input2

	protected abstract fun outputsToOutput (output1: Output1, output2: Output2) : Output

	protected abstract fun failure1ToFailure (failure1: Failure1) : Failure
	protected abstract fun failure2ToFailure (failure2: Failure2) : Failure


	override fun start (input: Input, callback: JobCallbackWithFailure<in Output, in Failure>) : Task? {
		return startInParallel(
			job1 = sourceJob1,
			job2 = sourceJob2,
			input1 = inputToInput1(input),
			input2 = inputToInput2(input),
			onFinish = {},
			onFailure1 = { failure1 ->
				callback.notifyFailure(
					failure1ToFailure(failure1)
				)
			},
			onFailure2 = { failure2 ->
				callback.notifyFailure(
					failure2ToFailure(failure2)
				)
			}
		) { output1, output2 ->
			callback.notifySuccess(
				outputsToOutput(output1, output2)
			)
		}
	}


}
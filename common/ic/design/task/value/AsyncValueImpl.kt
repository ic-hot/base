package ic.design.task.value


import ic.funs.effect.changesOnly
import ic.ifaces.cancelable.Cancelable
import ic.parallel.annotations.UiThread
import ic.struct.collection.ext.copy.copy
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet


abstract class AsyncValueImpl <RawValue, Value> : AsyncValue<Value> {


	@UiThread
	protected abstract fun load (@UiThread callback: (RawValue) -> Unit) : Cancelable?

	@UiThread
	protected abstract fun isSuccessful (value: RawValue) : Boolean

	@UiThread
	protected abstract fun isFinished (value: RawValue) : Boolean

	@UiThread
	protected abstract fun modify (value: RawValue) : Value


	private var cachedRawValue : RawValue? = null
	private var hasCachedRawValue : Boolean = false

	private var cachedValue : Value? = null
	private var hasCachedValue : Boolean = false

	private var task : Cancelable? = null


	private val watchers = EditableSet<(Value) -> Unit>()


	private fun load () {
		task = load { rawValue ->
			if (isFinished(rawValue)) {
				task = null
			}
			if (isSuccessful(rawValue)) {
				cachedRawValue = rawValue
				hasCachedRawValue = true
			}
			val modifiedValue = modify(rawValue)
			watchers.copy().forEach { it(modifiedValue) }
		}
	}


	@UiThread
	override fun watch (@UiThread callback: (Value) -> Unit) : Cancelable {
		val watcher = callback.changesOnly()
		watchers.add(watcher)
		if (hasCachedValue) {
			@Suppress("UNCHECKED_CAST")
			val value = cachedValue as Value
			watcher(value)
		} else {
			if (hasCachedRawValue) {
				@Suppress("UNCHECKED_CAST")
				val rawValue = cachedRawValue as RawValue
				watcher(modify(rawValue))
			} else {
				if (task == null) {
					load()
				}
			}
		}
		return Cancelable {
			watchers.remove(watcher)
		}
	}


	override fun reload() {
		clear()
		load()
	}


	override fun set (value: Value) {
		cachedValue = value
		hasCachedValue = true
		watchers.copy().forEach { it(value) }
	}


	override fun notifyChanged () {
		if (!hasCachedRawValue) return
		@Suppress("UNCHECKED_CAST")
		val rawValue = cachedRawValue as RawValue
		val modifiedValue = modify(rawValue)
		watchers.copy().forEach { it(modifiedValue) }
	}


	override fun clear() {
		hasCachedRawValue = false
		cachedRawValue = null
		hasCachedValue = false
		cachedValue = null
		task?.cancel()
		task = null
	}


}
package ic.design.task.value


import ic.ifaces.cancelable.Cancelable
import ic.parallel.annotations.UiThread


interface AsyncValue <Value> {


	@UiThread
	fun watch (@UiThread callback: (Value) -> Unit) : Cancelable


	fun set (value: Value)

	fun notifyChanged ()

	fun reload ()

	fun clear()


}
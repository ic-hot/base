package ic.design.task.value


import ic.ifaces.cancelable.Cancelable


@Deprecated("To remove")
inline fun <RawValue, ModifiedValue> AsyncValue (

	crossinline load : ((RawValue) -> Unit) -> Cancelable? = { null },

	crossinline isSuccessful : (RawValue) -> Boolean = { true },

	crossinline isFinished : (RawValue) -> Boolean = { false },

	crossinline modify : (RawValue) -> ModifiedValue,

) : AsyncValue<ModifiedValue> {

	return object : AsyncValueImpl<RawValue, ModifiedValue>() {

		override fun isSuccessful (value: RawValue) : Boolean {
			return isSuccessful(value)
		}

		override fun isFinished (value: RawValue): Boolean {
			return isFinished(value)
		}

		override fun load (callback: (RawValue) -> Unit) : Cancelable? {
			return load(callback)
		}

		override fun modify (value: RawValue) : ModifiedValue {
			return modify(value)
		}

	}

}


@Deprecated("To remove")
inline fun <Value> AsyncValue (

	crossinline load : ((Value) -> Unit) -> Cancelable? = { null },

	crossinline isSuccessful : (Value) -> Boolean = { true },

	crossinline isFinished : (Value) -> Boolean = { false },

) : AsyncValue<Value> {

	return object : AsyncValueImpl<Value, Value>() {

		override fun isSuccessful (value: Value) : Boolean {
			return isSuccessful(value)
		}

		override fun isFinished (value: Value): Boolean {
			return isFinished(value)
		}

		override fun load (callback: (Value) -> Unit) : Cancelable? {
			return load(callback)
		}

		override fun modify (value: Value) = value

	}

}
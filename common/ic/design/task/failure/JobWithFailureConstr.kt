package ic.design.task.failure


import ic.design.task.Task
import ic.design.task.callback.failure.JobCallbackWithFailure


inline fun <Input, Output, Failure> JobWithFailure (

	crossinline start : (input: Input, callback: JobCallbackWithFailure<in Output, in Failure>) -> Task?

) : JobWithFailure<Input, Output, Failure> {

	return object : JobWithFailure<Input, Output, Failure> {

		override fun start (input: Input, callback: JobCallbackWithFailure<in Output, in Failure>) : Task? {
			return start(input, callback)
		}

	}

}
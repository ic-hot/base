package ic.design.task.failure.progress


import ic.design.task.Task
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress


inline fun <Input, Output, Failure, Progress> JobWithFailureAndProgress (

	crossinline start : (Input, JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>) -> Task?

) : JobWithFailureAndProgress<Input, Output, Failure, Progress> {

	return object : JobWithFailureAndProgress<Input, Output, Failure, Progress> {

		override fun start (
			input : Input, callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>
		) : Task? {
			return start(input, callback)
		}

	}

}
package ic.design.task.failure.progress


import ic.design.task.Task
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress


interface JobWithFailureAndProgress<Input, Output, Failure, Progress> {


	fun start (

		input : Input,

		callback : JobCallbackWithFailureAndProgress<in Output, in Failure, Progress>

	) : Task?


}
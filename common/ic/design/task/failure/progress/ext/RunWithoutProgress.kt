package ic.design.task.failure.progress.ext


import ic.base.throwables.FailureException
import ic.design.task.failure.progress.JobWithFailureAndProgress


inline fun
	<Input, Output, Failure, Progress>
	JobWithFailureAndProgress<Input, Output, Failure, Progress>
	.run (
		input : Input,
		onFailure : (Failure) -> Output = { throw FailureException(it) }
	)
	: Output
{
	return run(
		input = input,
		onFailure = onFailure,
		isProgressEnabled = { false },
		onProgress = {}
	)
}
package ic.design.task.failure.progress.ext


import ic.design.task.Task
import ic.design.task.callback.failure.callback.JobCallbackWithFailureAndProgress
import ic.design.task.failure.progress.JobWithFailureAndProgress


inline fun <Input, Output, Failure, Progress> JobWithFailureAndProgress<Input, Output, Failure, Progress>.start (

	input : Input,

	crossinline isProgressEnabled : () -> Boolean = { true },
	crossinline onProgress : (Progress) -> Unit,

	crossinline onFinish : () -> Unit = {},

	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit

) : Task? {

	val task = start(
		input = input,
		callback = JobCallbackWithFailureAndProgress(
			onFinish = onFinish,
			isProgressEnabled = isProgressEnabled,
			onProgress = onProgress,
			onFailure = onFailure,
			onSuccess = onSuccess
		)
	)

	return task

}
package ic.design.task.failure.progress.ext


import ic.base.throwables.InconsistentDataException
import ic.design.task.failure.progress.JobWithFailureAndProgress
import ic.parallel.funs.sleepWhile


sealed class TaskStatus {
	object InProgress : TaskStatus()
	class Success (val output: Any?) : TaskStatus()
	class Failure (val failure: Any?) : TaskStatus()
}


inline fun
	<Input, Output, Failure, Progress>
	JobWithFailureAndProgress<Input, Output, Failure, Progress>
	.run (
		input : Input,
		onFailure : (Failure) -> Output,
		crossinline isProgressEnabled : () -> Boolean = { true },
		crossinline onProgress : (Progress) -> Unit
	)
	: Output
{

	var taskStatus : TaskStatus = TaskStatus.InProgress

	start(
		input,
		onFailure = { failure ->
			taskStatus = TaskStatus.Failure(failure)
		},
		isProgressEnabled = isProgressEnabled,
		onProgress = onProgress
	) { output ->
		taskStatus = TaskStatus.Success(output)
	}

	sleepWhile { taskStatus is TaskStatus.InProgress }
	@Suppress("UNCHECKED_CAST")
	when (val finalTaskStatus = taskStatus) {
		is TaskStatus.Success -> {
			return finalTaskStatus.output as Output
		}
		is TaskStatus.Failure -> {
			return onFailure(finalTaskStatus.failure as Failure)
		}
		else -> throw InconsistentDataException.Error("finalTaskStatus: $finalTaskStatus")
	}

}
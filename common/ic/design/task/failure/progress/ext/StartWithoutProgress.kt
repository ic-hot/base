package ic.design.task.failure.progress.ext


import ic.design.task.Task
import ic.design.task.failure.progress.JobWithFailureAndProgress


inline fun <Input, Output, Failure, Progress> JobWithFailureAndProgress<Input, Output, Failure, Progress>.start (

	input : Input,

	crossinline onFinish : () -> Unit = {},

	crossinline onFailure : (Failure) -> Unit,
	crossinline onSuccess : (Output) -> Unit

) : Task? = start(
	input = input,
	isProgressEnabled = { false },
	onProgress = {},
	onFinish = onFinish,
	onFailure = onFailure,
	onSuccess = onSuccess
)
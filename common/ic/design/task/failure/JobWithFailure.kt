package ic.design.task.failure


import ic.design.task.failure.progress.JobWithFailureAndProgress


typealias JobWithFailure<Input, Output, Failure> = JobWithFailureAndProgress<Input, Output, Failure, Unit>
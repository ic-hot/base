package ic.design.failure.ext


import ic.design.failure.SuccessOrFailure


inline operator fun
	<Success, Failure, Result>
	SuccessOrFailure<Success, Failure>.invoke (
		ifSuccess : (Success) -> Result,
		ifFailure : (Failure) -> Result
	)
	: Result
= (
	when (this) {
		is SuccessOrFailure.Success -> ifSuccess(success)
		is SuccessOrFailure.Failure -> ifFailure(failure)
	}
)
package ic.design.failure


@Suppress("NOTHING_TO_INLINE")
inline fun <Success> success (success: Success) : SuccessOrFailure.Success<Success> {

	return SuccessOrFailure.Success(success)

}


val success = success(Unit)
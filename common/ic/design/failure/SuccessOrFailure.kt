package ic.design.failure


sealed class SuccessOrFailure<out Success, out Failure> {


	data class Success<out Success> (

		val success : Success

	) : SuccessOrFailure<Success, Nothing>()


	data class Failure<out Failure> (

		val failure : Failure

	) : SuccessOrFailure<Nothing, Failure>()


}
package ic.design.failure


fun <Failure> failure (failure: Failure) : SuccessOrFailure.Failure<Failure> {

	return SuccessOrFailure.Failure(failure)

}
package ic.design.sisi


import ic.parallel.annotations.BackgroundThread
import ic.parallel.annotations.UiThread


sealed class Action<Intent> {


	object DoNothing : Action<Nothing>()


	abstract class SideEffect : Action<Nothing>() {

		@UiThread
		abstract fun run()

	}


	abstract class Async<Intent> : Action<Intent>() {

		@BackgroundThread
		abstract fun getIntent() : Intent

	}


}
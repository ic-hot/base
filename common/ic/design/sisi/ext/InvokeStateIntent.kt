package ic.design.sisi.ext


import ic.design.sisi.SisiController
import ic.design.sisi.StateIntent
import ic.struct.list.ext.foreach.forEach


fun <State, Intent> SisiController<State, Intent>.invoke (

	stateIntent: StateIntent<State, Intent>

) {

	state = stateIntent.state

	stateIntent.intents.forEach { invoke(it) }

	stateIntent.actions.forEach { invoke(it) }

}
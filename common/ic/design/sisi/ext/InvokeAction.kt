package ic.design.sisi.ext


import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread

import ic.design.sisi.Action
import ic.design.sisi.SisiController


fun <Intent> SisiController<*, Intent>.invoke (action: Action<out Intent>) {

	when (action) {

		Action.DoNothing -> {}

		is Action.SideEffect -> {
			action.run()
		}

		is Action.Async -> {
			doInBackground {
				val intent = action.getIntent()
				doInUiThread {
					invoke(intent)
				}
			}
		}

	}

}
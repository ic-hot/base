package ic.design.sisi


import ic.design.sisi.ext.invoke
import ic.design.task.scope.TaskScope
import ic.ifaces.state.HasSettableState
import ic.parallel.annotations.UiThread


interface SisiController<State, Intent>

	: SisiInvokeScope<Intent>, TaskScope, HasSettableState<State>

{


	@UiThread
	fun onIntent (state: State, intent: Intent) : StateIntent<State, Intent>


	override fun invoke (intent: Intent) {
		invoke(
			onIntent(state, intent)
		)
	}


}
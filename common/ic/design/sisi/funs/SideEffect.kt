package ic.design.sisi.funs


import ic.design.sisi.Action
import ic.parallel.annotations.UiThread


inline fun sideEffect (

	@UiThread
	crossinline run : () -> Unit

) : Action.SideEffect {

	return object : Action.SideEffect() {

		@UiThread
		override fun run() {
			run()
		}

	}

}
package ic.design.sisi.funs

import ic.design.sisi.Action


inline val doNothing get() = Action.DoNothing
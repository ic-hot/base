package ic.design.sisi.funs


import ic.design.sisi.Action
import ic.parallel.annotations.BackgroundThread


inline fun <Intent> async (

	@BackgroundThread
	crossinline getIntent: () -> Intent

) : Action.Async<Intent> {

	return object : Action.Async<Intent>() {

		@BackgroundThread
		override fun getIntent() : Intent {
			return getIntent()
		}

	}

}
package ic.design.sisi.funs


import ic.design.sisi.Action
import ic.design.sisi.StateIntent
import ic.struct.list.ext.plus


@Suppress("NOTHING_TO_INLINE")
inline infix fun <State, Intent> StateIntent<State, Intent>.and (

	intent : Intent

) : StateIntent<State, Intent> {

	return StateIntent(
		state = state,
		intents = intents + intent,
		actions = actions
	)

}


@Suppress("NOTHING_TO_INLINE")
inline infix fun <State, Intent> StateIntent<State, Intent>.and (

	action : Action<out Intent>

) : StateIntent<State, Intent> {

	return StateIntent(
		state = state,
		intents = intents,
		actions = actions + action
	)

}
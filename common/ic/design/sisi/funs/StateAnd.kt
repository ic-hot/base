package ic.design.sisi.funs


import ic.design.sisi.Action
import ic.design.sisi.StateIntent
import ic.struct.list.List


@Suppress("NOTHING_TO_INLINE")
inline infix fun <State, Intent> State.and (

	intent : Intent

) : StateIntent<State, Intent> {

	return StateIntent(
		state = this,
		intents = List(intent),
		actions = List()
	)

}


@Suppress("NOTHING_TO_INLINE")
inline infix fun <State, Intent> State.and (

	action : Action<out Intent>

) : StateIntent<State, Intent> {

	return StateIntent(
		state = this,
		intents = List(),
		actions = List(action)
	)

}
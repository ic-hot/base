package ic.design.sisi


import ic.struct.list.List


data class StateIntent<out State, out Intent> (

	val state : State,

	val intents : List<Intent>,

	val actions : List<Action<out Intent>>

)
package ic.design.sisi


interface SisiInvokeScope<Intent> {


	fun invoke (intent: Intent)


}
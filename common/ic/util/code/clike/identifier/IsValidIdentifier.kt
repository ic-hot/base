package ic.util.code.clike.identifier


import ic.base.primitives.character.isLatinLetter
import ic.base.primitives.character.ext.isLatinLetterOrDigit
import ic.base.strings.ext.isEmpty


val String.isValidIdentifier : Boolean get() {

	if (this.isEmpty) return false

	var i = 0

	while (i < this.length) {

		val character = this[i]

		run checkingCharacter@{

			if (character == '_') return@checkingCharacter

			if (i == 0) {

				if (character.isLatinLetter) return@checkingCharacter

			} else {

				if (character.isLatinLetterOrDigit) return@checkingCharacter

			}

			return false

		}

		i++

	}

	return true

}
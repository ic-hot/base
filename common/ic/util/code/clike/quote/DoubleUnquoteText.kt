package ic.util.code.clike.quote


import ic.base.strings.ext.asText
import ic.base.throwables.WrongValueException
import ic.text.SubText
import ic.text.Text
import ic.text.ext.copyReplaceAll


fun DoubleUnquoteText (text: Text) : Text {

	if (!text.startsWith("\"")) {
		throw WrongValueException.Runtime("Text '$text' is not starts with '\"'")
	}
	if (!text.endsWith("\"")) {
		throw WrongValueException.Runtime("Text '$text' is not ends with '\"'")
	}

	val sourceText = SubText(text, 1, text.length - 1)

	return sourceText.copyReplaceAll(
		Pair("\\b", "\b"),
		Pair("\\r", "\r"),
		Pair("\\t", "\t"),
		Pair("\\n", "\n"),
		Pair("\\\"", "\""),
		Pair("\\\\", "\\")
	)

}


fun DoubleUnquoteText (string: String) = DoubleUnquoteText(text = string.asText)
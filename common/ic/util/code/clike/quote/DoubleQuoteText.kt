package ic.util.code.clike.quote


import ic.base.strings.ext.asText
import ic.text.concat.ConcatText
import ic.text.Text
import ic.text.ext.FromString
import ic.text.ext.copyReplaceAll


fun DoubleQuoteText (text: Text) = ConcatText(

	Text.FromString("\""),

	text.copyReplaceAll(
		Pair("\\", "\\\\"),
		Pair("\"", "\\\""),
		Pair("\n", "\\n"),
		Pair("\t", "\\t"),
		Pair("\r", "\\r"),
		Pair("\b", "\\b")
	),

	Text.FromString("\"")

)


@Suppress("NOTHING_TO_INLINE")
inline fun DoubleQuoteText (string: String) = DoubleQuoteText(string.asText)
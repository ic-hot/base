package ic.util.code.clike.quote


import ic.text.Text


fun doubleQuote (string: String) : String {

	return DoubleQuoteText(string).toString()

}


fun doubleQuote (text: Text) : Text {

	return DoubleQuoteText(text)

}
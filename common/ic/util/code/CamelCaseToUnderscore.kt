package ic.util.code


import ic.base.primitives.character.ext.isUpperCase
import ic.base.primitives.character.inLowerCase
import ic.text.TextBuffer


fun camelCaseToUnderscore (input: String) : String {

	val output = TextBuffer()

	input.forEachIndexed { index, character ->

		if (character.isUpperCase) {
			if (index == 0) {
				output.putChar(character.inLowerCase)
			} else {
				output.putChar('_')
				output.putChar(character.inLowerCase)
			}
		} else {
			output.putChar(character)
		}

	}

	return output.toString()

}
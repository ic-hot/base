package ic.util.log


import ic.ifaces.mutable.ext.synchronized
import ic.system.SystemOut


object SystemOutLogger : Logger() {


	override fun implementLog (level: LogLevel, tag: String, value: String) {
		SystemOut.synchronized {
			SystemOut.write(tag)
			SystemOut.putChar(' ')
			SystemOut.write(value)
			SystemOut.writeNewLine()
		}
	}


}
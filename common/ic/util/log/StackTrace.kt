package ic.util.log


class StackTraceThrowable : RuntimeException()

inline val StackTrace get() = StackTraceThrowable().stackTraceToString()
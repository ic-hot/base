package ic.util.log


sealed class LogLevel {

	object None 	: LogLevel()
	object Verbose 	: LogLevel()
	object Debug 	: LogLevel()
	object Info 	: LogLevel()
	object Warning 	: LogLevel()
	object Error 	: LogLevel()
	object Wtf 		: LogLevel()

}



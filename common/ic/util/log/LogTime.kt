package ic.util.log


import ic.system.funs.getCurrentEpochTimeMs
import ic.util.log.LogLevel.*


inline fun <Result> logTime (

	level : LogLevel,

	tag : String,

	getMessage : () -> Any?,

	action : () -> Result

) : Result {

	val startTimeMs = getCurrentEpochTimeMs()

	try {

		return action()

	} finally {

		val endTimeMs = getCurrentEpochTimeMs()

		val durationMs = endTimeMs - startTimeMs

		log(level, tag) {

			"${ getMessage() } : $durationMs ms"

		}

	}

}


inline fun <Result> logDTime (
	tag : String,
	getMessage : () -> Any?,
	action : () -> Result
) : Result {
	return logTime(level = Debug, tag = tag, getMessage = getMessage, action = action)
}
package ic.util.log


inline fun log (level: LogLevel, tag: String, getValue: () -> Any?) {
	globalLogger.log(level, tag, getValue)
}


inline fun logV (tag: String, getValue: () -> Any?) {
	globalLogger.logV(tag, getValue)
}

inline fun logD (tag: String, getValue: () -> Any?) {
	globalLogger.logD(tag, getValue)
}

fun logD (tag: String, value: Any?) {
	logD(tag) { value }
}

inline fun logI (tag: String, getValue: () -> Any?) {
	globalLogger.logI(tag, getValue)
}

inline fun logW (tag: String, getValue: () -> Any?) {
	globalLogger.logW(tag, getValue)
}

fun logW (tag: String, value: Any?) {
	logW(tag) { value }
}

inline fun logE (tag: String, getValue: () -> Any?) {
	globalLogger.logE(tag, getValue)
}

inline fun logWtf (tag: String, getValue: () -> Any?) {
	globalLogger.logWtf(tag, getValue)
}
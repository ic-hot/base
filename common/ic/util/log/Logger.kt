package ic.util.log


import ic.app.tier.Tier
import ic.app.tier.tier
import ic.base.throwables.ext.stackTraceAsString
import ic.ifaces.mutable.Mutable
import ic.ifaces.mutable.ext.synchronized
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.Synchronized


abstract class Logger : Mutable {


	override val mutex = Mutex()


	open fun isLogLevelEnabled (level: LogLevel) : Boolean {
		return when (tier) {
			Tier.Debug -> when (level) {
				LogLevel.Debug,
				LogLevel.Info,
				LogLevel.Warning,
				LogLevel.Error,
				LogLevel.Wtf -> true
				else -> false
			}
			Tier.Development -> when (level) {
				LogLevel.Info,
				LogLevel.Warning,
				LogLevel.Error,
				LogLevel.Wtf -> true
				else -> false
			}
			Tier.Stage -> when (level) {
				LogLevel.Warning,
				LogLevel.Error,
				LogLevel.Wtf -> true
				else -> false
			}
			Tier.Production -> when (level) {
				LogLevel.Warning,
				LogLevel.Error,
				LogLevel.Wtf -> true
				else -> false
			}
		}
	}


	@Synchronized
	abstract fun implementLog (level: LogLevel, tag: String, value: String)


	inline fun log (level: LogLevel, tag: String, getValue: () -> Any?) {
		if (isLogLevelEnabled(level)) {
			val value = getValue()
			val string = (
				if (value is Throwable) {
					value.stackTraceAsString
				} else {
					value.toString()
				}
			)
			synchronized {
				implementLog(level, tag, string)
			}
		}
	}


	inline fun logV (tag: String, getValue: () -> Any?) {
		log(LogLevel.Verbose, tag, getValue)
	}

	inline fun logD (tag: String, getValue: () -> Any?) {
		log(LogLevel.Debug, tag, getValue)
	}

	inline fun logI (tag: String, getValue: () -> Any?) {
		log(LogLevel.Info, tag, getValue)
	}

	inline fun logW (tag: String, getValue: () -> Any?) {
		log(LogLevel.Warning, tag, getValue)
	}

	inline fun logE (tag: String, getValue: () -> Any?) {
		log(LogLevel.Error, tag, getValue)
	}

	inline fun logWtf (tag: String, getValue: () -> Any?) {
		log(LogLevel.Wtf, tag, getValue)
	}


}
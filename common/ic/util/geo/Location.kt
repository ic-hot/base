package ic.util.geo


data class Location (

	val latitude : Double,

	val longitude : Double

) {

	companion object

}
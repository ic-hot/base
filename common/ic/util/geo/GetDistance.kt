package ic.util.geo


import kotlin.math.*

import ic.base.primitives.float64.ext.asFloat32
import ic.math.funs.degreesToRadians


private const val earthRadiusInM = 6371000F


fun getDistanceInM (a: Location, b: Location) : Float {

	val dLat = degreesToRadians(b.latitude - a.latitude)
	val dLng = degreesToRadians(b.longitude - a.longitude)

	val square = (
		(
			sin(dLat / 2) *
			sin(dLat / 2)
		) + (
			cos(
				degreesToRadians(a.latitude)
			) *
			cos(
				degreesToRadians(b.latitude)
			) *
			sin(dLng / 2) *
			sin(dLng / 2)
		)
	)

	return (
		earthRadiusInM *
		2 *
		atan2(
			sqrt(square),
			sqrt(1 - square)
		)
	).asFloat32

}


@Suppress("NOTHING_TO_INLINE")
inline fun getDistanceInKm (a: Location, b: Location) = getDistanceInM(a, b) / 1000
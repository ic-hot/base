package ic.util.geo


import ic.storage.prefs.Prefs
import ic.util.time.Time


private val prefs = Prefs("ic.util.geo.LastKnownLocation")


var defaultLocation : Location = Location(0.0, 0.0)


var lastKnownLocationTime : Time
	get() {
		return prefs.getInt64OrNull("time")?.let { Time(epochMs = it) } ?: Time.EpochStart
	}
	private set(value) {
		prefs["time"] = value.epochMs
	}
;


var lastKnownLocation : Location
	get() {
		return Location(
			latitude  = prefs.getFloat64OrNull("latitude")  ?: return defaultLocation,
			longitude = prefs.getFloat64OrNull("longitude") ?: return defaultLocation
		)
	}
	set(value) {
		prefs["latitude"]  = value.latitude
		prefs["longitude"] = value.longitude
	}
;
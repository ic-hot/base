package ic.util.crypto.hash


import ic.base.platform.basePlatform


fun sha256 (data: ByteArray) : ByteArray {
	return basePlatform.cryptoEngine.sha256(data)
}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteArray.getSha256() = sha256(this)
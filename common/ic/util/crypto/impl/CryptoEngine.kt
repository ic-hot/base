package ic.util.crypto.impl


internal abstract class CryptoEngine {

	abstract fun sha256 (data: ByteArray) : ByteArray

}
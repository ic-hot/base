
/*
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.html


import kotlin.jvm.JvmName

import ic.base.strings.ext.replaceAll


private sealed class State {

	object Initial : State()

	class InsideTag (

		val tagStartIndex : Int

	) : State()

	class InsideQuotation (

		val tagStartIndex : Int

	) : State()

}


fun htmlToPlainString (html: String, toPreservePlainTextNewlines: Boolean = false) : String {

	var string = html

	if (!toPreservePlainTextNewlines) {
		string = string.replaceAll(
			"\n" to " ",
			"\r" to ""
		)
	}

	string = string.replaceAll(
		"<p>" to "\n",
		"</p>" to "",
		"<br>" to "\n",
		"&nbsp;" to "\u00A0"
	)

	while (true) run pass@{
		var state : State = State.Initial
		var i = 0
		while (i < string.length) {
			val c = string[i]
			when (state) {
				is State.Initial -> {
					if (c == '<') {
						state = State.InsideTag(tagStartIndex = i)
					}
				}
				is State.InsideTag -> {
					if (c == '>') {
						string = string.substring(0, state.tagStartIndex) + string.substring(i + 1)
						return@pass
					}
					if (c == '"') {
						state = State.InsideQuotation(tagStartIndex = state.tagStartIndex)
					}
				}
				is State.InsideQuotation -> {
					if (c == '"') {
						state = State.InsideTag(tagStartIndex = state.tagStartIndex)
					}
				}
			}
			i++
		}
		return string
	}

}


@JvmName("parseNullableHtmlToPlainString")
inline fun htmlToPlainString (html: String?, toPreservePlainTextNewlines: Boolean = false) : String? {
	if (html == null) return null
	return htmlToPlainString(html, toPreservePlainTextNewlines = toPreservePlainTextNewlines)
}
 */
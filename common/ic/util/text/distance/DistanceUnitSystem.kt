package ic.util.text.distance


sealed class DistanceUnitSystem {


	abstract val id : Byte


	object Metric : DistanceUnitSystem() {
		override val id : Byte get() = 1
	}

	object Imperial : DistanceUnitSystem() {
		override val id : Byte get() = 2
	}


	companion object {

		val distanceUnitSystems get() = listOf(
			Metric,
			Imperial
		)

		fun byId (id: Byte) = distanceUnitSystems.find { it.id == id }!!

	}


}
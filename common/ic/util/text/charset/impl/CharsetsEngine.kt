package ic.util.text.charset.impl


import ic.base.throwables.UnableToParseException
import ic.util.text.charset.Charset


internal interface CharsetsEngine {

	fun stringToByteArray (string: String, charset: Charset) : ByteArray

	fun byteArrayToString (byteArray: ByteArray, charset: Charset) : String

	fun encodeUrl (string: String, charset: Charset) : String

	@Throws(UnableToParseException::class)
	fun decodeUrlOrThrowUnableToParse (string: String, charset: Charset) : String

}
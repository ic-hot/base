@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.charset.ext


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray
import ic.text.Text
import ic.util.text.charset.Charset


inline fun Charset.textToByteSequence (text: Text) : ByteSequence {

	return ByteSequenceFromByteArray(
		stringToByteArray(
			text.toString()
		)
	)

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.charset.ext


import ic.util.text.charset.Charset
import ic.util.text.charset.impl.charsetsEngine


fun Charset.stringToByteArray (string: String) : ByteArray {

	return charsetsEngine.stringToByteArray (string, this)

}
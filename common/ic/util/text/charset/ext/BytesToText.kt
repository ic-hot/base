@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.charset.ext


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.text.Text
import ic.base.strings.ext.asText
import ic.util.text.charset.Charset


inline fun Charset.bytesToText (byteArray: ByteArray) : Text {
	return bytesToString(byteArray).asText
}

inline fun Charset.bytesToText (byteSequence: ByteSequence) : Text {
	return bytesToString(byteSequence.toByteArray()).asText
}
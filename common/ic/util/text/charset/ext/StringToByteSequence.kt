@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.charset.ext


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray
import ic.util.text.charset.Charset


inline fun Charset.stringToByteSequence (string: String) : ByteSequence {

	return ByteSequenceFromByteArray(
		stringToByteArray(string)
	)

}
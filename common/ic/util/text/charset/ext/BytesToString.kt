package ic.util.text.charset.ext


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.util.text.charset.Charset
import ic.util.text.charset.impl.charsetsEngine


fun Charset.bytesToString (byteArray: ByteArray) : String {
	return charsetsEngine.byteArrayToString(byteArray, charset = this)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Charset.bytesToString (byteSequence: ByteSequence) : String {
	return bytesToString(byteSequence.toByteArray())
}
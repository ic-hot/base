package ic.util.text.charset


import ic.struct.list.List
import ic.struct.list.ext.reduce.find.findOrNull


class Charset

	private constructor (val jvmName: String, val httpName: String)

{


	companion object {

		val iso8859_1 = Charset(
			jvmName = "ISO-8859-1",
			httpName = "iso-8859-1"
		)

		val Utf8 = Charset(
			jvmName = "UTF-8",
			httpName = "utf-8"
		)

		val utf16 = Charset(
			jvmName = "UTF-16",
			httpName = "utf-16"
		)

		val windows1251 = Charset(
			jvmName = "windows-1251",
			httpName = "windows-1251"
		)

		val all get() = List(
			iso8859_1,
			Utf8,
			utf16,
			windows1251
		)

		fun byHttpNameOrNull (httpName: String?) : Charset? {
			if (httpName == null) return null
			return all.findOrNull { it.httpName == httpName }
		}

		val defaultUnix = Utf8
		val defaultHttp	= Utf8

	}


}
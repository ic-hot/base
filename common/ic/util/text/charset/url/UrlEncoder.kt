package ic.util.text.charset.url


import ic.base.throwables.UnableToParseException
import ic.util.text.charset.Charset
import ic.util.text.charset.impl.charsetsEngine


fun encodeUrl (string: String, charset: Charset = Charset.defaultHttp) : String {
	return charsetsEngine.encodeUrl(
		string = string,
		charset = charset
	)
}

@Throws(UnableToParseException::class)
fun decodeUrlOrThrowUnableToParse (
	string: String, charset: Charset = Charset.defaultHttp
) : String {
	return charsetsEngine.decodeUrlOrThrowUnableToParse(
		string = string,
		charset = charset
	)
}


@Suppress("NOTHING_TO_INLINE")
inline fun decodeUrl (
	string: String, charset: Charset = Charset.defaultHttp
) : String {
	try {
		return decodeUrlOrThrowUnableToParse(string = string, charset = charset)
	} catch (_: UnableToParseException) {
		throw RuntimeException("Can't URL-decode: $string")
	}
}
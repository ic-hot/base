package ic.util.text.email


import ic.base.throwables.UnableToParseException
import ic.util.email.EmailAddress


val String?.isValidEmail : Boolean get() {
	if (this == null) return false
	try {
		EmailAddress.parseOrThrowUnableToParse(this)
		return true
	} catch (t: UnableToParseException) {
		return false
	}
}
package ic.util.text.natural


sealed class GrammaticalCase {

	object Nominative : GrammaticalCase()

	object Genitive : GrammaticalCase()

}
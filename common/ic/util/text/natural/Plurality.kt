package ic.util.text.natural


import kotlin.math.absoluteValue

import ic.base.primitives.int32.Int32


sealed class Plurality {


	object One 	: Plurality()
	object Few 	: Plurality()
	object Many : Plurality()


	companion object {


		fun of (count: Int32) : Plurality {
			val n = count.absoluteValue
			return when {

				(n % 100) in 11 .. 14 -> Many

				(n % 10) == 1 		-> One
				(n % 10) in 2 .. 4 	-> Few

				else -> Many

			}
		}


	}


}
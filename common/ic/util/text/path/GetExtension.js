
let getExtensionOrNull = function(path) {

	if (!path) return null

	let lastIndexOfSlash = path.lastIndexOf('/')

    let lastIndexOfDot = path.lastIndexOf('.')

    if (lastIndexOfDot < 0) return null

    if (lastIndexOfDot < lastIndexOfSlash) return null

    return path.substring(lastIndexOfDot + 1, path.length)

}
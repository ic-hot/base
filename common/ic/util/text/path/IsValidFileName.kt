package ic.util.text.path


import ic.base.primitives.character.isLetterOrDigit
import ic.base.strings.all


val String.isValidFileName : Boolean get() = all { character ->

	character.isLetterOrDigit ||

	character == '_' ||

	character == '-' ||

	character == '.'

}
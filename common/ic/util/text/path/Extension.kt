package ic.util.text.path


import ic.base.strings.ext.getSubstring


val String?.extensionOrNull : String? get() {

	if (this == null) return null

	val lastIndexOfSlash = lastIndexOf('/')

	val lastIndexOfDot = lastIndexOf('.')

	if (lastIndexOfDot < 0) return null

	if (lastIndexOfDot < lastIndexOfSlash) return null

	return getSubstring(lastIndexOfDot + 1, length)

}
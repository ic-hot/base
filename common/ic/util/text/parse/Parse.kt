package ic.util.text.parse


import ic.base.loop.breakableLoop
import ic.text.input.TextInput
import ic.text.TextBuffer
import ic.base.escape.breakable.Break
import ic.base.throwables.End
import ic.base.throwables.UnableToParseException
import ic.text.TextOutput


@Throws(UnableToParseException::class)
fun <Expression> parseOrThrowUnableToParse (

	input : TextInput,

	initialState : ParserState<Expression>,

	errorOutput : TextOutput?

) : Expression {

	var state = initialState
	var char = ' '
	var currentLineIndex = 0
	val currentLine = TextBuffer()
	val currentLineSpace = TextBuffer()

	try {

		try {

			breakableLoop {

				char = input.getNextCharacterOrThrowEnd()

				if (char != '\n') {
					currentLine.putChar(char)
				}

				state = state.next(char)

				if (char == '\n') {
					currentLine.empty()
					currentLineIndex++
					currentLineSpace.empty()
				} else {
					currentLineSpace.putChar(
						if (char == '\t') '\t' else ' '
					)
				}

			}

		} catch (end : End) {}

		return state.finalize()

	} catch (thrown : Throwable) {

		if (char != '\n') {
			try {
				breakableLoop {
					val c = input.getNextCharacterOrThrowEnd()
					if (c == '\n') Break()
					currentLine.putChar(c)
				}
			} catch (end : End) {}
		}

		errorOutput?.writeLine(
			"\n" +
			"\n" +
			(
				if (thrown.message == null) "" else {
					"${ thrown.message }\n" +
					"\n"
				}
			) +
			"line ${ currentLineIndex + 1 }:\n" +
			"\n" +
			"$currentLine\n" +
			"$currentLineSpace^\n" +
			"\n" +
			"state: $state\n" +
			"char: '${
				if (char == '\n') "\\n" else char
			}'\n"
		)

		throw UnableToParseException()

	}

}
package ic.util.text.parse


import ic.base.primitives.character.Character
import ic.base.throwables.InvalidSyntaxException


abstract class ParserState<Expression> {


	@Throws(InvalidSyntaxException::class)
	abstract fun next (character: Character) : ParserState<Expression>

	abstract fun finalize() : Expression


}
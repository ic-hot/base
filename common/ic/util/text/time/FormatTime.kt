package ic.util.text.time


import ic.util.locale.Locale
import ic.util.locale.impl.localeEngine
import ic.util.time.Time


fun formatTime (

	time : Time,

	pattern : String,

	locale : Locale = Locale.current,
	toUseStandaloneMonth : Boolean = false

) : String {

	return localeEngine.formatTime(
		time = time,
		pattern = pattern,
		locale = locale,
		toUseStandaloneMonth = toUseStandaloneMonth
	)

}
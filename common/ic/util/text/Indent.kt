package ic.util.text


import ic.text.concat.ConcatText
import ic.text.RepeatText
import ic.text.Text
import ic.text.ext.FromString
import ic.text.ext.copyReplaceAll


fun IndentText (text: Text, level: Int) = ConcatText(

	RepeatText(Text.FromString("\t"), level),

	text.copyReplaceAll(
		"\n" to "\n" + RepeatText(Text.FromString("\t"), level)
	)

)
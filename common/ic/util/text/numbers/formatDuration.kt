package ic.util.text.numbers


fun formatDuration (durationInMillis : Long) : String {

	val min 	= (durationInMillis / 1000 / 60) % 60
	val hours 	= (durationInMillis / 1000 / 60 / 60)

	return if (hours == 0L) {
		"$min min"
	} else {
		if (min == 0L) {
			"$hours hr"
		} else {
			"$hours hr $min min"
		}
	}

}


fun formatDurationShort (durationInMillis : Long) : String {

	val min 	= (durationInMillis / 1000 / 60) % 60
	val hours 	= (durationInMillis / 1000 / 60 / 60)

	return if (hours == 0L) {
		"$min min"
	} else {
		"$hours hr"
	}

}
package ic.util.text.numbers.hex


import ic.base.primitives.int32.Int32
import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun parseInt32FromHexStringOrThrow (hexString: String) : Int32 {

	try {
		return hexString.toLong(radix = 16).toInt()
	} catch (_: NumberFormatException) {
		throw UnableToParseException()
	}

}
package ic.util.text.numbers.hex


import ic.base.primitives.int64.Int64


fun int64ToFixedSizeHexString (int64: Int64) : String {

	return int64.toULong().toString(radix = 16).padStart(16, '0')

}
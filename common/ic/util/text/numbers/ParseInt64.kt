package ic.util.text.numbers


import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
fun parseInt64OrThrowUnableToParse (string : String) : Long {
	try {
		return string.toLong()
	} catch (numberFormatException: NumberFormatException) {
		throw UnableToParseException()
	}
}
package ic.util.text.numbers


fun formatMemoryAmount (amount: Long) : String {

	if (amount < 1024L) 		return "$amount B"
	if (amount < 1048576L) 		return "${ amount / 1024L } Kb"
	if (amount < 1073741824L) 	return "${ amount / 1048576L } Mb"
	else						return "${ amount / 1073741824L } Gb"

}
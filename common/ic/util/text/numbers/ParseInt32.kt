@file:Suppress("NOTHING_TO_INLINE")


package ic.util.text.numbers


import ic.base.primitives.int32.Int32
import ic.base.throwables.UnableToParseException


@Throws(UnableToParseException::class)
inline fun parseInt32OrThrowUnableToParse (string: String) : Int32 {
	try {
		return string.toInt()
	} catch (numberFormatException: NumberFormatException) {
		throw UnableToParseException()
	}
}
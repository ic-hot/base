package ic.util.text.phonenumber


import ic.base.primitives.character.ext.isDigit
import ic.base.strings.ext.filter
import ic.base.strings.ext.splitIntoGroups
import ic.struct.list.List
import ic.struct.list.ext.concat


@Deprecated("To remove")
fun formatPhoneNumberWithSpaces (

	phoneNumber : String,

	oldPhoneNumber : String = phoneNumber,

	toEndWithSpace : Boolean = false

) : String {

	val digitsOnly = phoneNumber.filter { it.isDigit }

	if (digitsOnly.length > 12) return oldPhoneNumber

	return "+" + digitsOnly.splitIntoGroups(
		groupsLengths = List(3, 2, 3, 2, 2),
		toEndWithEmptyGroup = toEndWithSpace
	).concat(
		separator = ' '
	)

}
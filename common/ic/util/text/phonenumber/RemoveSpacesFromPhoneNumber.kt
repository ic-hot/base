package ic.util.text.phonenumber


import ic.base.primitives.character.ext.isDigit
import ic.base.strings.ext.filter
import ic.base.strings.ext.startsWith


fun removeSpacesFromPhoneNumber (phoneNumber: String) : String {

	if (phoneNumber startsWith '+') {

		return "+" + phoneNumber.filter { it.isDigit }

	} else {

		return phoneNumber.filter { it.isDigit }

	}

}
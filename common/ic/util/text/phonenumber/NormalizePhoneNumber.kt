package ic.util.text.phonenumber


import ic.base.primitives.character.ext.isDigit
import ic.base.strings.ext.endsWith
import ic.base.strings.ext.filter
import ic.base.strings.ext.getSubstring
import ic.base.strings.ext.startsWith


fun normalizePhoneNumber (

	value : String,

	oldValue : String = value,

	countryCode : String

) : String {

	var result = value.filter { it.isDigit }

	when {

		countryCode startsWith result -> {
			result = ""
		}

		result startsWith "0" && countryCode endsWith "0" -> {
			result = result.getSubstring(1, result.length)
		}

		result startsWith countryCode -> {
			result = result.getSubstring(countryCode.length, result.length)
		}

	}

	if (result.length > "998887766".length) {
		return oldValue
	}

	result = countryCode + result

	return "+" + result

}
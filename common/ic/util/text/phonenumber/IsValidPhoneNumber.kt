package ic.util.text.phonenumber


import ic.base.primitives.character.ext.isDigit
import ic.base.primitives.character.ext.isSpace
import ic.base.strings.atLeastOne
import ic.base.strings.ext.filter
import ic.base.strings.ext.getSubstring
import ic.base.strings.ext.startsWith
import ic.test.util.mocks.samplePhoneNumber


val String?.isValidPhoneNumber : Boolean get() {

	if (this == null) return false

	if (this startsWith '+') {

		val filtered = getSubstring(1, length).filter { !it.isSpace && it != '-' }

		when {

			filtered.atLeastOne { !it.isDigit } -> return false

			filtered.length != samplePhoneNumber.length - 1 -> return false

			else -> return true

		}

	} else {

		return false

	}

}
package ic.util.text.phonenumber


import ic.base.primitives.character.ext.isDigit
import ic.base.strings.ext.filter


@Deprecated("To remove")
fun normalizePhoneNumber (phoneNumber: String, defaultMask: String? = null) : String {

	val normalizedDefaultMask = defaultMask?.filter { it.isDigit }

	var normalizedPhoneNumber = phoneNumber.filter { it.isDigit }

	if (normalizedDefaultMask != null && normalizedPhoneNumber.length >= 9) {

		val phoneNumberWithoutMask = normalizedPhoneNumber.substring(
			normalizedPhoneNumber.length - 9,
			normalizedPhoneNumber.length
		)

		val existingMask = normalizedPhoneNumber.substring(
			0,
			normalizedPhoneNumber.length - 9
		)

		if (normalizedDefaultMask.endsWith(existingMask)) {
			normalizedPhoneNumber = normalizedDefaultMask + phoneNumberWithoutMask
		}

	}

	return '+' + normalizedPhoneNumber

}
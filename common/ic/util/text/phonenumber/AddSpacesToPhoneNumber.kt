package ic.util.text.phonenumber


import ic.base.strings.ext.splitIntoGroups
import ic.struct.list.ext.concat
import ic.struct.list.List


fun addSpacesToPhoneNumber (

	phoneNumber : String,

	toEndWithSpace : Boolean = false

) : String {

	return removeSpacesFromPhoneNumber(phoneNumber).splitIntoGroups(
		groupsLengths = List(4, 2, 3, 2, 2),
		toEndWithEmptyGroup = toEndWithSpace
	).concat(
		separator = ' '
	)

}
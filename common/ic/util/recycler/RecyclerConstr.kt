package ic.util.recycler


inline fun <Key, Value: Any> Recycler (

	crossinline generateValue : () -> Value,

	crossinline onSeize : (Key, Value) -> Unit,

	crossinline onRelease : (Value) -> Unit,

	crossinline onClose : (Value) -> Unit

) : Recycler<Key, Value> {

	return object : Recycler<Key, Value>() {

		override fun generateValue() = generateValue()

		override fun onSeize (key: Key, value: Value) = onSeize(key, value)

		override fun onRelease (value: Value) = onRelease(value)

		override fun onClose (value: Value) = onClose(value)

	}

}
@file:Suppress("SortModifiers")


package ic.util.recycler


import ic.ifaces.lifecycle.closeable.Closeable
import ic.ifaces.getter.getter1.Getter1
import ic.struct.collection.ext.count.isEmpty
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.collection.ext.reduce.any
import ic.struct.map.finite.FiniteMap
import ic.struct.map.editable.EditableMap
import ic.struct.set.editable.EditableSet


abstract class Recycler <Key, Value: Any> : Getter1<Value, Key>, Closeable {


	protected abstract fun generateValue () : Value

	protected open fun onSeize (key: Key, value: Value) {}

	protected open fun onRelease (value: Value) {}

	protected open fun onClose (value: Value) {}


	private val usedValues = EditableMap<Key, Value>()

	private val unusedValues = EditableSet<Value>()


	val used : FiniteMap<Key, Value> get() = usedValues


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	operator override fun get (key: Key) : Value {

		val usedValue = usedValues[key]

		if (usedValue != null) {
			return usedValue
		}

		if (unusedValues.isEmpty) {

			val newValue = generateValue()

			usedValues[key] = newValue

			onSeize(key, newValue)

			return newValue

		} else {

			val unusedValue = unusedValues.any

			unusedValues.remove(unusedValue)

			usedValues[key] = unusedValue

			onSeize(key, unusedValue)

			return unusedValue

		}

	}


	fun recycle (isInUse: (Key) -> Boolean) {
		usedValues.keys.breakableForEach { key ->
			if (!isInUse(key)) {
				val value = usedValues[key]!!
				onRelease(value)
				usedValues[key] = null
				unusedValues.add(value)
			}
		}
	}


	fun recycleAll() {
		recycle(
			isInUse = { false }
		)
	}


	override fun close() {
		usedValues.keys.breakableForEach { key ->
			val value = usedValues[key]!!
			onRelease(value)
			onClose(value)
		}
		usedValues.empty()
		unusedValues.breakableForEach { value -> onClose(value) }
		unusedValues.empty()
	}


}
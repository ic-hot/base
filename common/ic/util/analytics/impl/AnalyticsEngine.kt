package ic.util.analytics.impl


internal interface AnalyticsEngine {


	fun trackEvent (eventName: String, params: Array<out Pair<String, String?>>)


	fun trackScreenStart (screenName: String)

	fun trackScreenEnd (screenName: String)


}
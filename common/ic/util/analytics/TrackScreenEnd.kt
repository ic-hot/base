package ic.util.analytics


import ic.base.platform.basePlatform
import ic.util.log.logD


fun trackScreenEnd (screenName: String) {

	logD("trackScreenEnd") { "screenName: $screenName" }

	basePlatform.analyticsEngine.trackScreenEnd(screenName = screenName)

}
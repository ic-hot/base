package ic.util.analytics


import ic.base.platform.basePlatform
import ic.util.log.logD


fun trackEvent (eventName: String, vararg params: Pair<String, String?>) {

	logD("trackEvent") { "eventName: $eventName" }

	basePlatform.analyticsEngine.trackEvent(eventName = eventName, params = params)

}
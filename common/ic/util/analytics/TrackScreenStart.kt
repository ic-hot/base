package ic.util.analytics


import ic.base.platform.basePlatform
import ic.util.log.logD


fun trackScreenStart (screenName: String) {

	logD("trackScreenStart") { "screenName: $screenName" }

	basePlatform.analyticsEngine.trackScreenStart(screenName = screenName)

}
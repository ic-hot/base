package ic.util.velocitytracker


import ic.base.primitives.float32.Float32


internal class Point (

	val time : Float32,

	val position : Float32

)
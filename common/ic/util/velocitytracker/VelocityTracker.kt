package ic.util.velocitytracker


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.isNaN
import ic.struct.list.editable.EditableList
import ic.struct.list.editable.removeAll
import ic.struct.list.ext.reduce.minmax.findMaxByFloat32
import ic.struct.list.ext.reduce.minmax.minBy


class VelocityTracker (

	private val timeThreshold : Float32,

	private val maxSpeed : Float32

) {


	private val points = EditableList<Point>()


	private fun cleanUp (time : Float32) {
		points.removeAll { it.time < time - timeThreshold }
	}


	fun addPoint (time: Float32, position: Float32) {

		cleanUp(time = time)

		points.add(Point(time = time, position = position))

	}


	fun clear() {
		points.empty()
	}


	fun calculateVelocity (time: Float32) : Float32 {

		cleanUp(time = time)

		if (points.length < 2) return Float32(0)

		val earliestPoint = points.minBy { it.time }
		val latestPoint   = points.findMaxByFloat32 { it.time }

		val measuredVelocity = (latestPoint.position - earliestPoint.position) / (latestPoint.time - earliestPoint.time)

		val maxVelocityMagnitude = maxSpeed

		return when {
			measuredVelocity.isNaN -> Float32(0)
			measuredVelocity < -maxVelocityMagnitude -> -maxVelocityMagnitude
			measuredVelocity > maxVelocityMagnitude  -> maxVelocityMagnitude
			else -> measuredVelocity
		}

	}


}
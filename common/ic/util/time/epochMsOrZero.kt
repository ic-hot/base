package ic.util.time


import ic.base.primitives.int64.Int64


inline val Time?.epochMsOrZero : Int64 get() {

	if (this == null) return 0

	return epochMs

}
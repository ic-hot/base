@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time


import ic.base.primitives.int64.Int64
import ic.util.time.duration.Duration


operator fun Time.plus (ms: Int64) = Time(this.epochMs + ms)

operator fun Time.plus (duration: Duration) = Time(this.epochMs + duration.inMs)
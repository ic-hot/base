package ic.util.time.ext


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skip
import kotlin.jvm.JvmName

import ic.base.strings.ext.isNullOrBlank
import ic.base.throwables.UnableToParseException
import ic.util.locale.Locale
import ic.util.locale.impl.localeEngine
import ic.util.time.Time


@Throws(UnableToParseException::class)
fun Time.Companion.parseOrThrowUnableToParse (
	string : String,
	pattern : String,
	locale : Locale = Locale.British
) : Time {
	return localeEngine.parseTimeOrThrowUnableToParse(
		string = string,
		pattern = pattern,
		locale = locale
	)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parse (
	string : String,
	pattern : String,
	locale : Locale = Locale.British
) : Time {
	try {
		return parseOrThrowUnableToParse(
			string = string,
			pattern = pattern,
			locale = locale
		)
	} catch (_: UnableToParseException) {
		throw RuntimeException("string: $string")
	}
}


@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parseOrNull (
	string : String?,
	pattern : String,
	locale : Locale = Locale.British
) : Time? {
	if (string == null) return null
	try {
		return parseOrThrowUnableToParse(
			string = string,
			pattern = pattern,
			locale = locale
		)
	} catch (_: UnableToParseException) {
		return null
	}
}


@Suppress("NOTHING_TO_INLINE")
@Skippable
inline fun Time.Companion.parseOrSkip (
	string : String?,
	pattern : String,
	locale : Locale = Locale.British
) : Time {
	if (string == null) skip
	return try {
		parseOrThrowUnableToParse(
			string = string,
			pattern = pattern,
			locale = locale
		)
	} catch (_: UnableToParseException) {
		skip
	}
}


@JvmName("parseNullable")
@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parse (
	string : String?,
	pattern : String,
	locale : Locale = Locale.British
) : Time? {
	if (string.isNullOrBlank) return null
	return try {
		parseOrThrowUnableToParse(
			string = string!!,
			pattern = pattern,
			locale = locale
		)
	} catch (_: UnableToParseException) {
		throw RuntimeException("string: $string")
	}
}
package ic.util.time.ext


import ic.base.primitives.int32.Int32
import ic.util.locale.Locale
import ic.util.locale.impl.localeEngine
import ic.util.time.Time


fun Time.getYear (locale: Locale) : Int32 = localeEngine.getYear(time = this, locale = locale)

fun Time.getMonth (locale: Locale) : Int32 = localeEngine.getMonth(time = this, locale = locale)

fun Time.getDayOfMonth (locale: Locale) : Int32 = localeEngine.getDayOfMonth(time = this, locale = locale)


val Time.year : Int32 get() = localeEngine.getYear(time = this, locale = Locale.current)

val Time.month : Int32 get() = localeEngine.getMonth(time = this, locale = Locale.current)

val Time.dayOfMonth : Int32 get() = localeEngine.getDayOfMonth(time = this, locale = Locale.current)
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.ext


import ic.base.primitives.int64.Int64
import ic.util.time.Time
import ic.util.time.duration.Duration
import ic.util.time.duration.ext.inMsOrZero
import ic.util.time.epochMsOrZero


inline operator fun Time.minus (ms: Int64) = Time(epochMs = this.epochMs - ms)

inline operator fun Time.minus (time: Time?) = Duration(inMs = this.epochMs - time.epochMsOrZero)

inline operator fun Time.minus (duration: Duration?) = Time(this.epochMs - duration.inMsOrZero)
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.ext


import ic.util.locale.Locale
import ic.util.text.time.formatTime
import ic.util.time.Time


inline fun Time.toString (

	pattern : String,
	locale : Locale = Locale.current,
	toUseStandaloneMonth : Boolean = false

) : String {

	return formatTime(
		time = this,
		pattern = pattern,
		locale = locale,
		toUseStandaloneMonth = toUseStandaloneMonth
	)

}
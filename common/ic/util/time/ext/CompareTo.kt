@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.ext

import ic.util.time.Time


inline infix operator fun Time.compareTo (other: Time) : Int {
	return epochMs.compareTo(other.epochMs)
}
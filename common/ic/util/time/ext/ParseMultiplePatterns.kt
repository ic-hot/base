package ic.util.time.ext


import kotlin.jvm.JvmName

import ic.base.escape.breakable.Break
import ic.base.strings.ext.isNullOrBlank
import ic.base.throwables.UnableToParseException
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach
import ic.util.locale.Locale
import ic.util.locale.currentLocale
import ic.util.time.Time


@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parseOrThrowUnableToParse (
	string : String,
	patterns : List<String>,
	locale : Locale = currentLocale
) : Time {
	var result : Time? = null
	patterns.breakableForEach { pattern ->
		try {
			result = parseOrThrowUnableToParse(
				string = string,
				pattern = pattern,
				locale = locale
			)
			Break()
		} catch (_: UnableToParseException) {}
	}
	if (result == null) throw UnableToParseException()
	return result!!
}


@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parse (
	string : String,
	patterns : List<String>,
	locale : Locale = currentLocale
) : Time {
	return try {
		parseOrThrowUnableToParse(
			string = string,
			patterns = patterns,
			locale = locale
		)
	} catch (t: UnableToParseException) { throw UnableToParseException.Runtime(t) }
}


@Suppress("NOTHING_TO_INLINE")
@JvmName("parseNullable")
inline fun Time.Companion.parse (
	string : String?,
	patterns : List<String>,
	locale : Locale = currentLocale
) : Time? {
	if (string.isNullOrBlank) return null
	return try {
		parseOrThrowUnableToParse(
			string = string!!,
			patterns = patterns,
			locale = locale
		)
	} catch (t: UnableToParseException) { throw UnableToParseException.Runtime(t) }
}


@Suppress("NOTHING_TO_INLINE")
inline fun Time.Companion.parseOrNull (
	string : String?,
	patterns : List<String>,
	locale : Locale = currentLocale
) : Time? {
	if (string == null) return null
	return try {
		parseOrThrowUnableToParse(
			string = string,
			patterns = patterns,
			locale = locale
		)
	} catch (t: UnableToParseException) {
		return null
	}
}

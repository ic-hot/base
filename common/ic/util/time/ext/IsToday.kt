package ic.util.time.ext


import ic.util.time.Time
import ic.util.time.funs.areSameDay
import ic.util.time.funs.now


val Time.isToday : Boolean get() = areSameDay(now, this)
package ic.util.time.duration


import kotlin.jvm.JvmInline

import ic.base.primitives.int64.Int64


@JvmInline
value class Duration (

	val inMs : Int64

) {


	companion object {

		val Instantly = Duration(inMs = 0)

		val Forever = Duration(inMs = Int64.MAX_VALUE)

	}


}
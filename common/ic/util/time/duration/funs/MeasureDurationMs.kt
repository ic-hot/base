package ic.util.time.duration.funs


import ic.base.primitives.int64.Int64

import ic.system.funs.getCurrentEpochTimeMs


inline fun <Result> measureDurationMs (

	onMeasured : (Int64) -> Unit,

	block : () -> Result

) : Result {

	val start = getCurrentEpochTimeMs()

	try {

		return block()

	} finally {

		onMeasured(
			getCurrentEpochTimeMs() - start
		)

	}

}
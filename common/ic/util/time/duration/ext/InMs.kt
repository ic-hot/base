package ic.util.time.duration.ext


import ic.base.primitives.int64.Int64
import ic.util.time.duration.Duration


inline val Duration?.inMsOrZero : Int64 get() {
	if (this == null) {
		return 0
	} else {
		return inMs
	}
}
package ic.util.time.duration.ext


import ic.base.primitives.float64.Float64
import ic.util.time.duration.Duration


inline val Duration.inHours : Float64 get() = inMinutes / 60
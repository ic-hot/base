package ic.util.time.duration.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


inline val Int32.days get() = (this.asInt64 * 24).hours

inline val Int64.days get() = (this * 24).hours

inline val Float32.days get() = (this.asFloat64 * 24).hours

inline val Float64.days get() = (this * 24).hours
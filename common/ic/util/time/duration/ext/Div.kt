@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.duration.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asFloat32
import ic.util.time.duration.Duration


inline operator fun Duration.div (parts: Int32) = (this.inMs / parts).ms


inline operator fun Duration.div (duration: Duration) = (
	this.inMs.asFloat32 / duration.inMs.asFloat32
)
@file:Suppress("NOTHING_TO_INLINE")


package ic.util.time.duration.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.util.time.duration.Duration


inline operator fun Duration.compareTo (other: Duration) : Int32 {
	return inMs.compareTo(other.inMs)
}


inline operator fun Duration.compareTo (otherMs: Int64) : Int32 {
	return inMs.compareTo(otherMs)
}
package ic.util.time.duration.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


inline val Int32.sec get() = (this.asInt64 * 1000).ms

inline val Int64.sec get() = (this * 1000).ms

inline val Float32.sec get() = (this.asFloat64 * 1000).ms

inline val Float64.sec get() = (this * 1000).ms
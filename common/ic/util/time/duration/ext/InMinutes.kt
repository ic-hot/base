package ic.util.time.duration.ext


import ic.base.primitives.float64.Float64
import ic.util.time.duration.Duration


inline val Duration.inMinutes : Float64 get() = inSeconds / 60
package ic.util.time.duration.ext


import ic.base.primitives.float64.Float64
import ic.base.primitives.int64.ext.asFloat64
import ic.util.time.duration.Duration


inline val Duration.inSeconds : Float64 get() = inMs.asFloat64 / 1000
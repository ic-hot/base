package ic.util.time.duration.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt64
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.util.time.duration.Duration


inline val Int64.ms get() = Duration(inMs = this)

inline val Int32.ms get() = this.asInt64.ms

inline val Float32.ms get() = this.asInt64.ms

inline val Float64.ms get() = this.asInt64.ms

package ic.util.time.duration.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


inline val Int32.minutes get() = (this.asInt64 * 60).sec

inline val Int64.minutes get() = (this * 60).sec

inline val Float32.minutes get() = (this.asFloat64 * 60).sec

inline val Float64.minutes get() = (this * 60).sec
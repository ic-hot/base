package ic.util.time.funs


import ic.util.locale.Locale
import ic.util.time.Time
import ic.util.time.ext.getDayOfMonth
import ic.util.time.ext.getMonth
import ic.util.time.ext.getYear


fun areSameDay (a: Time, b: Time, locale: Locale = Locale.current) : Boolean = (
	a.getYear(locale)       == b.getYear(locale) &&
	a.getMonth(locale)      == b.getMonth(locale) &&
	a.getDayOfMonth(locale) == b.getDayOfMonth(locale)
)
package ic.util.time.funs


import ic.system.funs.getCurrentEpochTimeMs
import ic.util.time.Time


inline val now get() = Time(getCurrentEpochTimeMs())
package ic.util.time.funs


import ic.base.throwables.IoException
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeInt64
import ic.util.time.Time


@Throws(IoException::class)
fun ByteOutput.writeTime (time: Time) {

	writeInt64(time.epochMs)

}
package ic.util.time.funs


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.readInt64
import ic.util.time.Time


@Throws(IoException::class)
fun ByteInput.readTime() : Time {

	val epochMs = readInt64()

	return Time(epochMs = epochMs)

}
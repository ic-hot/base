package ic.util.time


import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asInt64


@Suppress("NOTHING_TO_INLINE")
inline fun Time (epochMs: Float64) = Time(epochMs = epochMs.asInt64)
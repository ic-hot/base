@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.util.time


import kotlin.jvm.JvmInline

import ic.base.primitives.int64.Int64


@JvmInline
value class Time (val epochMs: Int64) {


	companion object {

		val EpochStart = Time(epochMs = 0)

		val NegativeInfinity = Time(epochMs = Int64.MIN_VALUE)
		val PositiveInfinity = Time(epochMs = Int64.MAX_VALUE)

		inline fun Nullable (epochMs: Int64?) = if (epochMs == null) null else Time(epochMs)

	}


}
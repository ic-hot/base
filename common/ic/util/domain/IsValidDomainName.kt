package ic.util.domain


import ic.base.primitives.character.isLatinLetter
import ic.base.strings.all
import ic.base.strings.ext.isNotEmpty
import ic.base.strings.ext.split
import ic.struct.list.ext.reduce.find.all


val String.isValidDomainName : Boolean get() {

	val words = split(separator = '.')

	if (words.length < 2) return false

	return words.all { word ->
		word.isNotEmpty && word.all { it.isLatinLetter }
	}

}
package ic.util.load


import ic.base.primitives.int64.Int64
import ic.util.time.duration.Duration


sealed class LoadMode {


	class RemoteOnly (

		val toSaveToCache : Boolean

	) : LoadMode()


	class CacheOnly (

		val expireDurationMs : Int64 = Int64.MAX_VALUE

	) : LoadMode()


	class CacheFirst (

		val expireDuration : Duration = Duration.Forever

	) : LoadMode() {

		companion object {

			val Default = CacheFirst()

		}

	}


	class RemoteFirst (

		val expireDurationMs : Int64 = Int64.MAX_VALUE

	) : LoadMode() {

		companion object {

			val Default = RemoteFirst()

		}

	}


}
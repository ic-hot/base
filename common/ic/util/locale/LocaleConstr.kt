@file:Suppress("NOTHING_TO_INLINE")


package ic.util.locale


import ic.util.locale.impl.localeEngine


fun Locale (languageCode: String, countryCode: String) : Locale {

	return localeEngine.getLocale(
		languageCode = languageCode,
		countryCode = countryCode
	)

}
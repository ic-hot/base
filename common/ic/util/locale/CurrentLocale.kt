package ic.util.locale


import ic.base.platform.basePlatform
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


private val mutex by lazy { Mutex() }

private var currentLocaleField : Locale? = null


var currentLocale : Locale

	get() = mutex.synchronized {
		if (currentLocaleField == null) {
			currentLocaleField = basePlatform.localeEngine.getDefaultLocale()
		}
		currentLocaleField!!
	}

	set(value) = mutex.synchronized {
		currentLocaleField = value
	}

;
package ic.util.locale.impl


import ic.base.annotations.ToOverride
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.parse.parseInt32
import ic.base.throwables.UnableToParseException
import ic.util.locale.Locale
import ic.util.time.Time


internal interface LocaleEngine {


	fun getDefaultLocale() : Locale

	fun getLocale (languageCode: String, countryCode: String) : Locale


	fun formatFloat64 (
		float64 : Float64,
		pattern : String,
		locale : Locale,
		groupingSeparator : Char = ' '
	) : String


	fun formatTime (
		time : Time,
		pattern : String,
		locale : Locale,
		toUseStandaloneMonth : Boolean
	) : String

	@Throws(UnableToParseException::class)
	fun parseTimeOrThrowUnableToParse (
		string : String,
		pattern : String,
		locale : Locale
	) : Time


	@ToOverride
	fun getYear (time: Time, locale : Locale) : Int32 {
		return formatTime(time = time, locale = locale, pattern = "yyyy", toUseStandaloneMonth = false).parseInt32()
	}

	@ToOverride
	fun getMonth (time: Time, locale : Locale) : Int32 {
		return formatTime(time = time, locale = locale, pattern = "M", toUseStandaloneMonth = false).parseInt32() - 1
	}

	@ToOverride
	fun getDayOfMonth (time: Time, locale : Locale) : Int32 {
		return formatTime(time = time, locale = locale, pattern = "d", toUseStandaloneMonth = false).parseInt32()
	}


}
package ic.util.locale.impl


import ic.base.platform.basePlatform


internal inline val localeEngine get() = basePlatform.localeEngine
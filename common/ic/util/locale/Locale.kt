package ic.util.locale


interface Locale {

	val languageCode : String

	companion object {

		inline val current get() = currentLocale

		inline val American		get() = Locale("en", "US")
		inline val British		get() = Locale("en", "GB")
		inline val Russian 	    get() = Locale("ru", "RU")
		inline val Ukrainian 	get() = Locale("uk", "UA")

	}

}
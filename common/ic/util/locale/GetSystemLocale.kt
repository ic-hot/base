package ic.util.locale


val systemLocale : Locale get() = ic.base.platform.basePlatform.localeEngine.getDefaultLocale()
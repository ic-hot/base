package ic.util.email


import ic.base.kcollections.length
import ic.base.primitives.character.ext.isDigit
import ic.base.primitives.character.isLatinLetter
import ic.base.strings.all
import ic.base.strings.ext.isBlank
import ic.base.throwables.UnableToParseException
import ic.util.domain.isValidDomainName


class EmailAddress (

	val domain : String,

	val userName : String

) {

	override fun toString() = "$userName@$domain"

	companion object {
		
		private val String.isValidUserName : Boolean get() {
			if (isBlank) return false
			return all { character ->
				character.isLatinLetter ||
				character.isDigit ||
				character == '_' ||
				character == '.'
			}
		}

		@Throws(UnableToParseException::class)
		fun parseOrThrowUnableToParse (string: String) : EmailAddress {
			val split = string.split('@')
			if (split.length != 2) throw UnableToParseException()
			val userName = split[0]
			val domain = split[1]
			if (!userName.isValidUserName) throw UnableToParseException()
			if (!domain.isValidDomainName) throw UnableToParseException()
			return EmailAddress(
				userName = userName,
				domain = domain
			)
		}

		fun parse (string: String) : EmailAddress {
			try {
				return parseOrThrowUnableToParse(string)
			} catch (_: UnableToParseException) {
				throw RuntimeException(string)
			}
		}

	}

}
package ic.util.auth


import ic.struct.list.List
import ic.text.concat.ConcatText
import ic.text.Text
import ic.text.ext.FromString


class UrlAndAuth (

	val url: String,

	val auth: BasicAuth<String?, String?>

) {

	companion object {

		fun fromRawUrl (rawUrl: String) : UrlAndAuth {

			val rawUrlText = Text.FromString(rawUrl)

			val url: String
			val urlUserName: String?
			val urlPassword: String?

			if (rawUrlText.startsWith("https://")) {
				if (rawUrlText.contains('@')) {
					val urlIterator = rawUrlText.newIterator()
					urlIterator.read("https://".length)
					val credentials = urlIterator.readTill('@')
					if (credentials.contains(':')) {
						val credentialsIterator = credentials.newIterator()
						urlUserName = credentialsIterator.readTill(':').toString()
						urlPassword = credentialsIterator.read().toString()
					} else {
						urlUserName = credentials.toString()
						urlPassword = null
					}
					val restUrl = urlIterator.read()
					url = ConcatText(
						List(
							Text.FromString("https://"),
							restUrl
						)
					).toString()
				} else {
					url = rawUrl
					urlUserName = null
					urlPassword = null
				}
			} else {
				if (rawUrlText.contains('@')) {
					val urlIterator = rawUrlText.newIterator()
					val credentials = urlIterator.readTill('@')
					if (credentials.contains(':')) {
						val credentialsIterator = credentials.newIterator()
						urlUserName = credentialsIterator.readTill(':').toString()
						urlPassword = credentialsIterator.read().toString()
					} else {
						urlUserName = credentials.toString()
						urlPassword = null
					}
					val restUrl = urlIterator.read()
					url = restUrl.toString()
				} else {
					url = rawUrl
					urlUserName = null
					urlPassword = null
				}
			}

			return UrlAndAuth(
				url,
				BasicAuth(urlUserName, urlPassword)
			)

		}

	}

}
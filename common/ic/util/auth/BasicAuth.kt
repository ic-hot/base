package ic.util.auth


data class BasicAuth<Username, Password> (

	val username : Username,
	val password : Password

)
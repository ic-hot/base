package ic.util.event


import ic.ifaces.action.Action
import ic.ifaces.cancelable.Cancelable
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


abstract class Event private constructor() {


	protected val mutex = Mutex()

	protected val listeners: MutableSet<() -> Unit> = HashSet()


	fun watch (listener: () -> Unit): Cancelable {
		mutex.synchronized {
			listeners.add(listener)
		}
		return object : Cancelable {
			override fun cancel() {
				mutex.synchronized {
					listeners.remove(listener)
				}
			}
		}
	}


	class Trigger : Event(), Action {

		override fun run() {
			mutex.synchronized {
				for (listener in listeners.toTypedArray()) listener()
			}
		}

	}


}

@file:Suppress("NOTHING_TO_INLINE")


package ic.util.url


import ic.base.strings.ext.isNotEmpty
import ic.struct.map.finite.FiniteMap
import ic.base.throwables.UnableToParseException
import ic.ifaces.hascount.ext.isNotEmpty
import ic.struct.collection.ext.copy.copySortToList
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.map.editable.EditableMap
import ic.text.TextBuffer
import ic.util.text.charset.url.decodeUrlOrThrowUnableToParse
import ic.util.text.charset.url.encodeUrl


data class Url (

	val scheme : String,

	val domain : String,

	val path : String = "",

	val params : FiniteMap<String, String> = FiniteMap()

) {


	val baseUrlString : String get() {
		val textBuffer = TextBuffer()
		textBuffer.write(scheme)
		textBuffer.write("://")
		textBuffer.write(domain)
		if (path.isNotEmpty) {
			textBuffer.putChar('/')
			textBuffer.write(path)
		}
		return textBuffer.toString()
	}


	fun formatParams() = formatParams(params)


	override fun toString() : String {
		val textBuffer = TextBuffer()
		val baseUrlString = baseUrlString
		textBuffer.write(baseUrlString)
		if (params.isNotEmpty) {
			textBuffer.putChar(
				if (baseUrlString.contains('?')) '&' else '?'
			)
			textBuffer.write(formatParams())
		}
		return textBuffer.toString()
	}


	companion object {


		fun formatParams (params: FiniteMap<String, *>) : String {
			val textBuffer = TextBuffer()
			params.keys.copySortToList().breakableForEach(
				doInBetween = { textBuffer.putChar('&') }
			) { key ->
				textBuffer.write(encodeUrl(key))
				textBuffer.putChar('=')
				textBuffer.write(encodeUrl(params[key]!!.toString()))
			}
			return textBuffer.toString()
		}

		fun formatParams (vararg keyValuePairs: Pair<String, String>) : String {
			return formatParams(FiniteMap(*keyValuePairs))
		}


		@Throws(UnableToParseException::class)
		fun parseParamsOrThrowUnableToParse (paramsString: String) : FiniteMap<String, String> {
			if (paramsString.isEmpty()) return FiniteMap()
			val params = EditableMap<String, String>()
			for (paramString in paramsString.split('&')) {
				val paramSplit = paramString.split('=')
				if (paramSplit.size == 1) {
					params[paramSplit[0]] = ""
				} else if (paramSplit.size == 2) {
					params[paramSplit[0]] = decodeUrlOrThrowUnableToParse(paramSplit[1])
				} else throw UnableToParseException()
			}
			return params
		}

		inline fun parseParams (paramsString: String) : FiniteMap<String, String> {
			return try {
				parseParamsOrThrowUnableToParse(paramsString)
			} catch (t: UnableToParseException) { throw UnableToParseException.Runtime(t) }
		}


		@Throws(UnableToParseException::class)
		fun fromBaseUrlAndParamsOrThrowUnableToParse (
			baseUrl: String, params: FiniteMap<String, String>
		) : Url {
			val schemeEnd = baseUrl.indexOf("://")
			if (schemeEnd < 0) throw UnableToParseException()
			val scheme = baseUrl.substring(0, schemeEnd)
			val domainStart = schemeEnd + 3
			val domainEnd = baseUrl.indexOf('/', startIndex = domainStart)
			if (domainEnd < 0) return Url(
				scheme = scheme,
				domain = baseUrl.substring(domainStart),
				path = "",
				params = params
			)
			val domain = baseUrl.substring(domainStart, domainEnd)
			val pathStart = domainEnd + 1
			return Url(
				scheme = scheme,
				domain = domain,
				path = baseUrl.substring(pathStart),
				params = params
			)
		}

		inline fun fromBaseUrlAndParams (
			baseUrl: String, params: FiniteMap<String, String>
		) : Url {
			return try {
				fromBaseUrlAndParamsOrThrowUnableToParse(baseUrl = baseUrl, params = params)
			} catch (t: UnableToParseException) {
				throw RuntimeException("baseUrl: $baseUrl")
			}
		}


		@Throws(UnableToParseException::class)
		fun parseOrThrowUnableToParse (urlString: String) : Url {
			val baseUrlEnd = urlString.indexOf('?')
			if (baseUrlEnd < 0) {
				return fromBaseUrlAndParamsOrThrowUnableToParse(
					baseUrl = urlString,
					params = FiniteMap()
				)
			}
			val baseUrl = urlString.substring(0, baseUrlEnd)
			val paramsStart = baseUrlEnd + 1
			val params = parseParamsOrThrowUnableToParse(urlString.substring(paramsStart))
			return fromBaseUrlAndParamsOrThrowUnableToParse(
				baseUrl = baseUrl,
				params = params
			)
		}


		inline fun parse (urlString: String) : Url {
			return try {
				parseOrThrowUnableToParse(urlString)
			} catch (t: UnableToParseException) { throw UnableToParseException.Runtime(t) }
		}


		inline fun parseOrNull (urlString: String?) : Url? {
			if (urlString == null) return null
			return try {
				parseOrThrowUnableToParse(urlString)
			} catch (t: UnableToParseException) { return null }
		}


	}


}
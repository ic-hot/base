package ic.util.cache


import ic.base.throwables.NotExistsException
import ic.parallel.mutex.Synchronized
import ic.struct.map.ephemeral.EphemeralValuesMap
import ic.struct.map.finite.ext.foreach.forEach
import ic.struct.value.ephemeral.Soft
import ic.util.time.Time
import ic.util.time.funs.now


open class RamCache<Key, Value> : Cache<Key, Value>() {


	private inner class State (
		val value : Value
	) {
		val time = now
		var isDiscarded : Boolean = false
	}

	private val map = EphemeralValuesMap<Key, State>(::Soft)


	@Synchronized
	@Throws(NotExistsException::class)
	override fun implGetTimeOrThrow (key: Key) : Time {
		val state = map[key] ?: throw NotExistsException
		return state.time
	}

	@Synchronized
	@Throws(NotExistsException::class)
	override fun implGetValueOrThrow (key: Key) : Value {
		val state = map[key] ?: throw NotExistsException
		return state.value
	}

	@Synchronized
	override fun implSet (key: Key, value: Value) {
		map[key] = State(value)
	}

	@Synchronized
	override fun implEmpty() {
		map.empty()
	}

	@Synchronized
	override fun implRemove (key: Key) {
		map[key] = null
	}

	@Synchronized
	override fun implIsDiscarded (key: Key) : Boolean {
		val value = map[key]
		if (value == null) {
			return true
		} else {
			return value.isDiscarded
		}
	}

	@Synchronized
	override fun implDiscard (key: Key) {
		map[key]?.isDiscarded = true
	}

	@Synchronized
	override fun implDiscardAll() {
		map.forEach { _, value ->
			value.isDiscarded = true
		}
	}


}
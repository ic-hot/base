package ic.util.cache


private val commonRamCacheField by lazy { RamCache<Any?, Any>() }


fun <Key, Value> getCommonRamCache() : RamCache<Key, Value> {
	@Suppress("UNCHECKED_CAST")
	return commonRamCacheField as RamCache<Key, Value>
}
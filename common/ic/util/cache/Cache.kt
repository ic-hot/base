package ic.util.cache


import ic.base.throwables.NotExistsException
import ic.ifaces.emptiable.Emptiable
import ic.parallel.funs.doInBackground
import ic.parallel.funs.sleepWhile
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.Synchronized
import ic.parallel.mutex.synchronized
import ic.struct.set.editable.EditableSet
import ic.util.log.logW
import ic.util.time.Time
import ic.util.time.duration.Duration
import ic.util.time.duration.ext.compareTo
import ic.util.time.ext.minus
import ic.util.time.funs.now


abstract class Cache<Key, Value> : Emptiable {


	sealed class Directive {
		object Accept                   : Directive()
		object Deny                     : Directive()
		object AcceptButLoadInBackround : Directive()
		object AcceptOnlyIfFailure      : Directive()
	}


	interface Policy<in Key, in Value> {


		fun getDirective (key: Key, value: Value, isDiscarded: Boolean, time: Time) : Directive


		object Default : Policy<Any?, Any?> {

			override fun getDirective (
				key: Any?, value: Any?, isDiscarded: Boolean, time: Time
			) : Directive {
				if (isDiscarded) {
					return Directive.AcceptOnlyIfFailure
				} else {
					return Directive.Accept
				}
			}

		}


	}


	private val mutex = Mutex()


	@Synchronized
	@Throws(NotExistsException::class)
	protected abstract fun implGetTimeOrThrow (key: Key) : Time

	@Throws(NotExistsException::class)
	fun getTimeOrThrow (key: Key) : Time {
		mutex.synchronized {
			return implGetTimeOrThrow(key)
		}
	}


	@Synchronized
	@Throws(NotExistsException::class)
	protected abstract fun implGetValueOrThrow (key: Key) : Value

	@Throws(NotExistsException::class)
	fun getOrThrow (key: Key) : Value {
		mutex.synchronized {
			return implGetValueOrThrow(key)
		}
	}


	@Synchronized
	@Throws(NotExistsException::class)
	protected abstract fun implIsDiscarded (key: Key) : Boolean

	fun isDiscarded (key: Key) : Boolean {
		mutex.synchronized {
			return implIsDiscarded(key)
		}
	}


	@Throws(NotExistsException::class)
	fun getOrThrow (key: Key, maxAge: Duration) : Value {
		mutex.synchronized {
			val time = getTimeOrThrow(key)
			val age = now - time
			if (age < maxAge) {
				return implGetValueOrThrow(key)
			} else {
				throw NotExistsException
			}
		}
	}


	@Synchronized
	protected abstract fun implSet (key: Key, value: Value)

	operator fun set (key: Key, value: Value) {
		mutex.synchronized {
			implSet(key, value)
		}
	}


	@Synchronized
	protected abstract fun implRemove (key: Key)

	fun remove (key: Key) {
		mutex.synchronized {
			implRemove(key)
		}
	}

	@Synchronized
	protected abstract fun implDiscard (key: Key)

	fun discard (key: Key) {
		mutex.synchronized {
			implDiscard(key)
		}
	}


	@Synchronized
	protected abstract fun implEmpty()

	override fun empty() {
		mutex.synchronized {
			implEmpty()
		}
	}

	@Synchronized
	protected abstract fun implDiscardAll()

	fun discardAll() {
		mutex.synchronized {
			implDiscardAll()
		}
	}


	private val loadingKeys = EditableSet<Key>()

	@Synchronized
	private fun isLoading (key: Key) : Boolean {
		return loadingKeys contains key
	}

	@Synchronized
	private fun setLoading (key: Key, isLoading: Boolean) {
		loadingKeys.setContains(key, isLoading)
	}


	@Throws(Exception::class)
	private fun implLoad (key: Key, load: () -> Value) : Value {
		try {
			val value = load()
			mutex.synchronized {
				setLoading(key, false)
				implSet(key, value)
			}
			return value
		} catch (t: Throwable) {
			mutex.synchronized {
				setLoading(key, false)
			}
			throw t
		}
	}


	private fun loadInBackground (key: Key, load: () -> Value) {
		doInBackground {
			mutex.synchronized {
				if (isLoading(key)) {
					return@doInBackground
				} else {
					setLoading(key, true)
				}
			}
			try {
				implLoad(key = key, load = load)
			} catch (e: Exception) {
				logW("Uncaught") { e }
			}
		}
	}


	fun getOr (
		key : Key,
		policy : Policy<Key, Value> = Policy.Default,
		load : () -> Value
	) : Value {

		var useValueIfFailure : Boolean = false
		var valueIfFailure : Value? = null

		mutex.synchronized {
			try {
				val time = implGetTimeOrThrow(key)
				val value = implGetValueOrThrow(key)
				val isDiscarded = implIsDiscarded(key)
				val directive = policy.getDirective(
					key = key, value = value, isDiscarded = isDiscarded, time = time
				)
				when (directive) {
					Directive.Accept -> return value
					Directive.Deny -> {}
					Directive.AcceptButLoadInBackround -> {
						loadInBackground(key = key, load = load)
						return value
					}
					Directive.AcceptOnlyIfFailure -> {
						useValueIfFailure = true
						valueIfFailure = value
					}
				}
			} catch (_: NotExistsException) {}
		}

		val isAlreadyLoading = mutex.synchronized {
			isLoading(key).also {
				if (!it) {
					setLoading(key, true)
				}
			}
		}

		if (isAlreadyLoading) {

			sleepWhile {
				mutex.synchronized {
					isLoading(key)
				}
			}

			try {
				return getOrThrow(key)
			} catch (_: NotExistsException) {
				return getOr(key = key, policy = policy, load = load)
			}

		} else {

			try {
				return implLoad(key = key, load = load)
			} catch (e: Exception) {
				if (useValueIfFailure) {
					@Suppress("UNCHECKED_CAST")
					return valueIfFailure as Value
				} else {
					throw e
				}
			}

		}

	}


}
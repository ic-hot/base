package ic.util.mimetype


import ic.struct.list.List
import ic.struct.list.ext.reduce.find.findOrNull


class MimeType

	private constructor (
		val extension : String,
		val name : String
	)

{


	companion object {

		val TEXT 	= MimeType("txt", "text/plain")
		val Html 	= MimeType("html", "text/html")
		val CSS 	= MimeType("css", "text/css")
		val JS 		= MimeType("js", "text/javascript")
		val Json 	= MimeType("json", "application/json")
		val CSV 	= MimeType("csv", "text/csv")
		val PNG 	= MimeType("png", "image/png")
		val JPG 	= MimeType("jpg", "image/jpg")
		val JPEG 	= MimeType("jpeg", "image/jpeg")
		val WEBP 	= MimeType("webp", "image/webp")
		val GIF 	= MimeType("gif", "image/gif")
		val SVG 	= MimeType("svg", "image/svg+xml")
		val ICO 	= MimeType("ico", "image/image/x-icon")
		val TTF 	= MimeType("ttf", "font/ttf")
		val WOFF 	= MimeType("woff", "font/woff")
		val WOFF2 	= MimeType("woff2", "font/woff2")
		val MP3 	= MimeType("mp3", "audio/mpeg")
		val OGG 	= MimeType("ogg", "audio/ogg")
		val XML 	= MimeType("xml", "text/xml")
		val PDF 	= MimeType("pdf", "application/pdf")
		val SH 		= MimeType("sh", "text/x-shellscript")
		val JAR 	= MimeType("jar", "application/java-archive")

		val FormUrlEncoded = MimeType("jar", "application/x-www-form-urlencoded")

		val mimeTypes = List(
			TEXT, Html, CSS, JS, Json, CSV, PNG, JPG, JPEG, WEBP, GIF, SVG, ICO, TTF, WOFF, WOFF2,
			MP3, OGG, XML, PDF, SH, JAR, FormUrlEncoded
		)

		fun byNameOrNull (name: String?) : MimeType? {
			if (name == null) return null
			return mimeTypes.findOrNull { it.name == name }
		}

		fun byExtensionOrNull (extension: String?) : MimeType? {
			if (extension == null) return null
			return mimeTypes.findOrNull { it.extension == extension }
		}

	}


}
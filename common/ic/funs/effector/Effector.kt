package ic.funs.effector


import ic.funs.effect.Effect


typealias Effector <Arg> = Effect<Effect<Arg>>
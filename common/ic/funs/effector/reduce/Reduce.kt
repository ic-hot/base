package ic.funs.effector.reduce


import kotlin.jvm.JvmName

import ic.funs.effector.Effector
import ic.funs.fun0.Fun0


@JvmName("reduceWithConvertFirst")
inline fun <Item, Result> Effector<Item>.reduce (
	crossinline convertFirst : (first: Item) -> Result,
	or : Fun0<Result> = { throw RuntimeException() },
	crossinline fold : (Result, Item) -> Result
) : Result {
	var atLeastOne : Boolean = false
	var result : Result? = null
	this { item ->
		if (atLeastOne) {
			@Suppress("UNCHECKED_CAST")
			result = fold(result as Result, item)
		} else {
			result = convertFirst(item)
			atLeastOne = true
		}
	}
	if (atLeastOne) {
		@Suppress("UNCHECKED_CAST")
		return result as Result
	} else {
		return or()
	}
}


@JvmName("reduceWithInit")
inline fun <Item, Result> Effector<Item>.reduce (
	crossinline init : Fun0<Result>,
	or : Fun0<Result> = { throw RuntimeException() },
	crossinline fold : (Result, Item) -> Result
) : Result {
	return reduce(
		convertFirst = { first ->
			fold(init(), first)
		},
		or = or,
		fold = fold
	)
}


@JvmName("reduceWithConvert")
inline fun <Item, Result> Effector<Item>.reduce (
	crossinline convert : (first: Item) -> Result,
	or : Fun0<Result> = { throw RuntimeException() },
	crossinline fold : (Result, Result) -> Result
) : Result {
	return reduce(
		convertFirst = convert,
		or = or,
		fold = { aResult: Result, bItem: Item ->
			fold(aResult, convert(bItem))
		}
	)
}


@JvmName("reduceWithoutConvert")
inline fun <Item> Effector<Item>.reduce (
	or : Fun0<Item> = { throw RuntimeException() },
	crossinline fold : (Item, Item) -> Item
) : Item {
	return reduce(
		convertFirst = { it },
		or = or,
		fold = fold
	)
}
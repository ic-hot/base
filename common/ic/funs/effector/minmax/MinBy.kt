package ic.funs.effector.minmax


import kotlin.jvm.JvmName

import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.funs.effector.Effector
import ic.funs.fun0.Fun0


@JvmName("minByInt32")
inline fun <Item> Effector<Item>.minByInt32 (
	or : Fun0<Item> = { throw RuntimeException() },
	crossinline itemToValue : (Item) -> Int32
) : Item {
	return min(or = or) { a, b ->
		itemToValue(a) compareTo itemToValue(b)
	}
}


@JvmName("minByInt64")
inline fun <Item> Effector<Item>.minByInt64 (
	or : Fun0<Item> = { throw RuntimeException() },
	crossinline itemToValue : (Item) -> Int64
) : Item {
	return min(or = or) { a, b ->
		itemToValue(a) compareTo itemToValue(b)
	}
}
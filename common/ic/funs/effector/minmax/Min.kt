package ic.funs.effector.minmax


import ic.base.compare.Comparison
import ic.funs.effector.Effector
import ic.funs.fun0.Fun0


inline fun <Item> Effector<Item>.min (
	or : Fun0<Item> = { throw RuntimeException() },
	crossinline compare : (Item, Item) -> Comparison
) : Item {
	return max(or = or) { a, b ->
		-compare(a, b)
	}
}
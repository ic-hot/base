package ic.funs.effector.minmax


import ic.base.compare.Comparison
import ic.funs.effector.Effector
import ic.funs.effector.reduce.reduce
import ic.funs.fun0.Fun0


inline fun <Item> Effector<Item>.max (
	or : Fun0<Item> = { throw RuntimeException() },
	crossinline compare : (Item, Item) -> Comparison
) : Item {
	return reduce(or = or) { a, b ->
		if (compare(a, b) < 0) b else a
	}
}
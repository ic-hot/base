package ic.funs.effector.find


import kotlin.jvm.JvmName

import ic.funs.effector.Effector


@Suppress("JavaIoSerializableObjectMustHaveReadResolve")
object Found : Throwable()

inline fun <Item> atLeastOne (
	effector: Effector<Item>, crossinline condition: (Item) -> Boolean
) : Boolean {
	try {
		effector { item ->
			if (condition(item)) throw Found
		}
		return false
	} catch (_: Found) {
		return true
	}
}

@JvmName("atLeastOneExt")
inline fun <Item> Effector<Item>.atLeastOne (
	crossinline condition: (Item) -> Boolean
) : Boolean {
	return atLeastOne(this, condition)
}
package ic.funs.effector.find


import kotlin.jvm.JvmName

import ic.funs.effector.Effector


inline fun <Item> none (
	effector: Effector<Item>, crossinline condition: (Item) -> Boolean
) : Boolean {
	return !atLeastOne(effector, condition)
}

@JvmName("noneExt")
inline fun <Item> Effector<Item>.none (
	crossinline condition: (Item) -> Boolean
) : Boolean {
	return none(this, condition)
}
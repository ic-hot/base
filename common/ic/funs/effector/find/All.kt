package ic.funs.effector.find


import kotlin.jvm.JvmName

import ic.funs.cond.Condition
import ic.funs.cond.not
import ic.funs.effector.Effector


inline fun <Item> all (
	effector: Effector<Item>, crossinline condition: Condition<Item>
) : Boolean {
	return none(effector, not(condition))
}

@JvmName("allExt")
inline fun <Item> Effector<Item>.all (
	crossinline condition: (Item) -> Boolean
) : Boolean {
	return all(this, condition)
}
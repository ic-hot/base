package ic.funs.effector.find


import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.funs.effector.Effector


inline fun
	<Result, Item: Result, Failure: Result>
	Effector<Item>.find (
		or : () -> Failure = { throw RuntimeException() },
		crossinline condition : (Item) -> Boolean
	)
	: Result
{
	var result : Item? = null
	var atLeastOne : Boolean = false
	skippable {
		this { item ->
			if (condition(item)) {
				result = item
				atLeastOne = true
				skip
			}
		}
	}
	if (atLeastOne) {
		@Suppress("UNCHECKED_CAST")
		return result as Item
	} else {
		return or()
	}
}
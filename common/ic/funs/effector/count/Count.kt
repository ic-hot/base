package ic.funs.effector.count


import kotlin.jvm.JvmName

import ic.base.primitives.int64.Int64
import ic.funs.cond.Condition
import ic.funs.effector.Effector
import ic.ifaces.hascount.HasCount


inline fun <Item> count (
	effector : Effector<Item>,
	crossinline condition : Condition<Item>
) : Int64 {
	var count : Int64 = 0
	effector { item ->
		if (condition(item)) {
			count++
		}
	}
	return count
}

@JvmName("countExt")
inline fun <Item> Effector<Item>.count (crossinline condition : Condition<Item>) = run {
	count(this, condition)
}


fun <Item> count (effector: Effector<Item>) = run {
	if (effector is HasCount) {
		effector.count
	} else {
		count(effector) { true }
	}
}

inline val <Item> Effector<Item>.count get() = run {
	count(this)
}
package ic.funs.effector.count


import ic.funs.effector.Effector


inline val <Item> Effector<Item>.isEmpty get() = count == 0L
package ic.funs.effector.sum


import kotlin.experimental.ExperimentalTypeInference

import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.funs.effector.Effector
import ic.funs.effector.reduce.reduce
import ic.funs.fun0.Fun0


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Effector<Item>.sum (
	or : Fun0<Int32> = { 0 },
	crossinline itemToValue : (Item) -> Int32
) : Int32 {
	return reduce(
		init = { 0 },
		or = or
	) { a, b ->
		a + itemToValue(b)
	}
}


@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
inline fun <Item> Effector<Item>.sum (
	or : Fun0<Int64> = { 0 },
	crossinline itemToValue : (Item) -> Int64
) : Int64 {
	return reduce(
		init = { 0 },
		or = or
	) { a, b ->
		a + itemToValue(b)
	}
}
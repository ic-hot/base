package ic.funs.cond


inline fun <Arg> not (crossinline condition: Condition<Arg>) : Condition<Arg> {
	return { arg -> !condition(arg) }
}
package ic.funs.cond


typealias Condition<Arg> = (Arg) -> Boolean
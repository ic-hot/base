package ic.funs


@Suppress("NOTHING_TO_INLINE")
inline operator fun
	<Arg1, Arg2, Result>
	((Arg1, Arg2) -> Result).invoke (arg1: Arg1)
	: (Arg2) -> Result
	= { arg2 ->
		this(arg1, arg2)
	}
;
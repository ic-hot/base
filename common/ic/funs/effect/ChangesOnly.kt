package ic.funs.effect


inline fun
	<Arg>
	ChangesOnly (
		crossinline f : (Arg) -> Unit
	)
	: (Arg) -> Unit
{
	var isAlreadyCalled : Boolean = false
	var previousArg : Arg? = null
	return { arg ->
		if (isAlreadyCalled) {
			if (arg != previousArg) {
				previousArg = arg
				f(arg)
			}
		} else {
			isAlreadyCalled = true
			previousArg = arg
			f(arg)
		}
	}
}


inline fun
	<Arg>
	((Arg) -> Unit).changesOnly () : (Arg) -> Unit
{
	return ChangesOnly(this)
}
package ic.funs.effect


import ic.funs.effect.effect0.Effect0


inline fun <Arg> (Effect<Arg>).andDoInBetween (crossinline doInBetween: Effect0) = run {

	var hasInvokedAtLeastOnce : Boolean = false

	{ arg : Arg ->
		if (hasInvokedAtLeastOnce) {
			doInBetween()
		} else {
			hasInvokedAtLeastOnce = true
		}
		this(arg)
	}

}
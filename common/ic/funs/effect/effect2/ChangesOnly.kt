package ic.funs.effect.effect2


inline fun
	<Arg1, Arg2>
	ChangesOnly (
		crossinline f : (Arg1, Arg2) -> Unit
	)
	: (Arg1, Arg2) -> Unit
{
	var isAlreadyCalled : Boolean = false
	var previousArg1 : Arg1? = null
	var previousArg2 : Arg2? = null
	return { arg1, arg2 ->
		if (isAlreadyCalled) {
			if (arg1 != previousArg1 || arg2 != previousArg2) {
				previousArg1 = arg1
				previousArg2 = arg2
				f(arg1, arg2)
			}
		} else {
			isAlreadyCalled = true
			previousArg1 = arg1
			previousArg2 = arg2
			f(arg1, arg2)
		}
	}
}


inline fun
	<Arg1, Arg2>
	((Arg1, Arg2) -> Unit).changesOnly () : (Arg1, Arg2) -> Unit
{
	return ChangesOnly(this)
}
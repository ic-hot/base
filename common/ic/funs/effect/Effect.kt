package ic.funs.effect


typealias Effect <Arg> = (Arg) -> Unit